﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WSFargoLine.Proxy
{
    public class Localizador
    {
        private static log4net.ILog log = log4net.LogManager.GetLogger(typeof(Localizador));

        //private static readonly string usuarioServicioFL = "wsfefargoline";
        //private static readonly string claveServicioFL = "f3f@r0l1n3WS";

        public static ProxyFL.RespuestaServicio4 ObtenerServicioFL (int iIdDocumento, String sEstadoEnvio)
        //public static void ObtenerServicioPDF(int iIdDocumento, String sEstadoEnvio)
        {
            List<Int32> Comprobantes = new List<Int32>();
            Comprobantes.Add(iIdDocumento);
            ProxyFL.RespuestaServicio4 respuesta;


            ProxyFL.ServicioFLClient cliente = new ProxyFL.ServicioFLClient("FL");
            cliente.ClientCredentials.Windows.ClientCredential.UserName = "wsfefargoline";
            cliente.ClientCredentials.Windows.ClientCredential.Password = "f3f@r0l1n3WS";
            ProxyFL.IServicioFL IServicioFL = cliente;


            if (sEstadoEnvio == "1" || sEstadoEnvio == "2")
                respuesta = cliente.Transmision_GenerarPDFOficial(Comprobantes);
            else
                respuesta = cliente.Transmision_GenerarPDFInterno(Comprobantes);

            Byte[] bytesZip = respuesta.Bytes;
            String NombreZip = respuesta.DataCadena;

            if (NombreZip != null)
            {
                HttpContext.Current.Response.Clear();
                HttpContext.Current.Response.ClearHeaders();
                HttpContext.Current.Response.ClearContent();

                HttpContext.Current.Response.AddHeader("Content-Disposition", String.Format("attachment; filename={0}", NombreZip));
                HttpContext.Current.Response.ContentType = "application/x-zip-compressed";
                HttpContext.Current.Response.BinaryWrite(bytesZip);
                HttpContext.Current.Response.Flush();
                HttpContext.Current.Response.End();
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }


            log.Error(respuesta.DataCadena);

            return respuesta;

            //return IServicioFL;

            
        }
    }
}