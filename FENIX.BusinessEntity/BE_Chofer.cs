﻿using System;
using System.Collections.Generic;
using System.Text;
using FENIX.Common;

namespace FENIX.BusinessEntity
{
    public class BE_Chofer : Paginador
    {
        #region "Campos"
        String _Brevere;
        String _Dni;
        String _ApeNom;
        int? _IdChofer;
        #endregion

        #region "Propiedades"
        public int? iIdChofer
        {
            get { return _IdChofer; }
            set { _IdChofer = value; }
        }
        public String sApeNom
        {
            get { return _ApeNom; }
            set { _ApeNom = value; }
        }
        public String sDni
        {
            get { return _Dni; }
            set { _Dni = value; }
        }
        public String sBrevere
        {
            get { return _Brevere; }
            set { _Brevere = value; }
        }
        #endregion

    }
}
