﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FENIX.BusinessEntity
{
    public class BE_Menu
    {
        String _Descripcion;
        Int32? _PadreId;
        Int32? _Posicion;
        String _Icono;
        Int32? _Habilitado;
        String _Url;
        DateTime? _FechaCreacion;
        String _UsuarioCreacion;
        DateTime? _FechaModificacion;
        String _UsuarioModificacion;
        Int32? _MenuId;
        Int32? _BotonId;
        Int32? _GrupoId;
        String _BotonNombreObjeto;
        Int32? _Visible;


        public Int32? iVisible
        {
            get { return _Visible; }
            set { _Visible = value; }
        }


        public String sBotonNombreObjeto
        {
            get { return _BotonNombreObjeto; }
            set { _BotonNombreObjeto = value; }
        }

        public Int32? iGrupoId
        {
            get { return _GrupoId; }
            set { _GrupoId = value; }
        }

        public Int32? iBotonId
        {
            get { return _BotonId; }
            set { _BotonId = value; }
        }

        public Int32? iMenuId
        {
            get { return _MenuId; }
            set { _MenuId = value; }
        }

        public String sUsuarioModificacion
        {
            get { return _UsuarioModificacion; }
            set { _UsuarioModificacion = value; }
        }
        public DateTime? sFechaModificacion
        {
            get { return _FechaModificacion; }
            set { _FechaModificacion = value; }
        }
        public String sUsuarioCreacion
        {
            get { return _UsuarioCreacion; }
            set { _UsuarioCreacion = value; }
        }
        public DateTime? dtFechaCreacion
        {
            get { return _FechaCreacion; }
            set { _FechaCreacion = value; }
        }


        public String sUrl
        {
            get { return _Url; }
            set { _Url = value; }
        }

        public Int32? iHabilitado
        {
            get { return _Habilitado; }
            set { _Habilitado = value; }
        }

        public String sIcono
        {
            get { return _Icono; }
            set { _Icono = value; }
        }

        public String sDescripcion
        {
            get { return _Descripcion; }
            set { _Descripcion = value; }
        }

        public Int32? iPadreId
        {
            get { return _PadreId; }
            set { _PadreId = value; }
        }

        public Int32? iPosicion
        {
            get { return _Posicion; }
            set { _Posicion = value; }

        }
    }
}
