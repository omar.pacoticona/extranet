﻿using System;
using System.Collections.Generic;
using System.Text;
using FENIX.Common;

namespace FENIX.BusinessEntity
{
    public class BE_NaveViajeProvisional : Paginador
    {
        #region "Campos"

        int _IdNavViaPro = 0;
        int _IdVia = 0;

        String _Vch_AnoManifiesto = string.Empty;
        String _Vch_NumManifiesto = string.Empty;
        String _Vch_Nave = string.Empty;
        String _Vch_FechaTransmision = string.Empty;
        String _Vch_FechaLlegada = string.Empty;
        String _Vch_FechaDescarga = string.Empty;
        String _Vch_EmpTransporte = string.Empty;
        String _Vch_Usuario = string.Empty;
        String _Vch_NombrePc = string.Empty;

        #endregion


        #region "Propiedades"

        public int iIdNavViaPro
        {
            get { return _IdNavViaPro; }
            set { _IdNavViaPro = value; }
        }

        public int iIdVia
        {
            get { return _IdVia; }
            set { _IdVia = value; }
        }

        public String sAnoManifiesto
        {
            get { return _Vch_AnoManifiesto; }
            set { _Vch_AnoManifiesto = value; }
        }

        public String sNumManifiesto
        {
            get { return _Vch_NumManifiesto; }
            set { _Vch_NumManifiesto = value; }
        }

        public String sNave
        {
            get { return _Vch_Nave; }
            set { _Vch_Nave = value; }
        }

        public String sFechaTransmision
        {
            get { return _Vch_FechaTransmision; }
            set { _Vch_FechaTransmision = value; }
        }

        public String sFechaLlegada
        {
            get { return _Vch_FechaLlegada; }
            set { _Vch_FechaLlegada = value; }
        }

        public String sFechaDescarga
        {
            get { return _Vch_FechaDescarga; }
            set { _Vch_FechaDescarga = value; }
        }

        public String sEmpTransporte
        {
            get { return _Vch_EmpTransporte; }
            set { _Vch_EmpTransporte = value; }
        }

        public String sUsuario
        {
            get { return _Vch_Usuario; }
            set { _Vch_Usuario = value; }
        }

        public String sNombrePc
        {
            get { return _Vch_NombrePc; }
            set { _Vch_NombrePc = value; }
        }
        #endregion
    }
}
