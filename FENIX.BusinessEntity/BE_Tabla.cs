﻿using System;
using System.Collections.Generic;
using System.Text;
using FENIX.Common;
using System.Reflection;
namespace FENIX.BusinessEntity
{
    [Serializable]
    public class BE_Tabla : Paginador
    {
        #region "Campos"
        int _idTabla = 0;
        int? _idTablaPadre = null;
        int _IdValor = 0;
        string _sIdValor = string.Empty;
        string _ValVcr_Descripcion = string.Empty;
        String _ValChr_Estado = string.Empty;
        int? _IdCodigoSecundario = null;
        int? _IdNavVia = null;
        int? _IdOperacion = null;
        int? _IdMovimiento = null;
        String _sTipoBooking = string.Empty;

        String _sDescripcion1 = string.Empty;
        String _sDescripcion2 = string.Empty;
        String _sDescripcion3 = string.Empty;
        String _sDescripcion4 = string.Empty;
        String _sDescripcion5 = string.Empty;


        public string sBanco_Descripcion { get;set;}

        public string sIdBanco { get; set; }
        public string sIdRegimen { get; set; }
        public string sRegimen_Descripcion { get; set; }

        
            

        #endregion

        #region "Propiedades"
        public String sDescripcion5
        {
            get { return _sDescripcion5; }
            set { _sDescripcion5 = value; }
        }

        public String sDescripcion4
        {
            get { return _sDescripcion4; }
            set { _sDescripcion4 = value; }
        }

        public String sDescripcion3
        {
            get { return _sDescripcion3; }
            set { _sDescripcion3 = value; }
        }

        public String sDescripcion2
        {
            get { return _sDescripcion2; }
            set { _sDescripcion2 = value; }
        }

        public String sDescripcion1
        {
            get { return _sDescripcion1; }
            set { _sDescripcion1 = value; }
        }

        public int? IdMovimiento
        {
            get { return _IdMovimiento; }
            set { _IdMovimiento = value; }
        }
        public int? iIdOperacion
        {
            get { return _IdOperacion; }
            set { _IdOperacion = value; }
        }
        public int? iIdNavVia
        {
            get { return _IdNavVia; }
            set { _IdNavVia = value; }
        }

        public String sIdValor
        {
            get { return _sIdValor; }
            set { _sIdValor = value; }
        }
        public int iIdTabla
        {
            get { return _idTabla; }
            set { _idTabla = value; }
        }

        public int? iIdTablaPadre
        {
            get { return _idTablaPadre; }
            set { _idTablaPadre = value; }
        }

        public int iIdValor
        {
            get { return _IdValor; }
            set { _IdValor = value; }
        }

        public String sDescripcion
        {
            get { return _ValVcr_Descripcion; }
            set { _ValVcr_Descripcion = value; }
        }

        public String sTipoBooking
        {
            get { return _sTipoBooking; }
            set { _sTipoBooking = value; }
        }

        public String sEstado
        {
            get { return _ValChr_Estado; }
            set { _ValChr_Estado = value; }
        }


        public Int32? iCodigoSecundario
        {
            get { return _IdCodigoSecundario; }
            set { _IdCodigoSecundario = value; }
        }

        #endregion

    }
    //#region Metodos
    //[Serializable]
    //public class BE_TablaLis : List<BE_Tabla>
    //{
    //    public void Ordenar(string propertyName, direccionOrden Direction)
    //    {
    //        BE_TablaComparer dc = new BE_TablaComparer(propertyName, Direction);
    //        this.Sort(dc);            
    //    }
    //}

    //class BE_TablaComparer : IComparer<BE_Tabla>
    //{
    //    string _prop = "";
    //    direccionOrden _dir;

    //    public BE_TablaComparer(string propertyName, direccionOrden Direction)
    //    {
    //        _prop = propertyName;
    //        _dir = Direction;
    //    }

    //    public int Compare(BE_DocumentoOrigen x, BE_DocumentoOrigen y)
    //    {

    //        PropertyInfo propertyX = x.GetType().GetProperty(_prop);
    //        PropertyInfo propertyY = y.GetType().GetProperty(_prop);

    //        object px = propertyX.GetValue(x, null);
    //        object py = propertyY.GetValue(y, null);

    //        if (px == null && py == null)
    //        {
    //            return 0;
    //        }
    //        else if (px != null && py == null)
    //        {
    //            if (_dir == direccionOrden.Ascending)
    //            {
    //                return 1;
    //            }
    //            else
    //            {
    //                return -1;
    //            }
    //        }
    //        else if (px == null && py != null)
    //        {
    //            if (_dir == direccionOrden.Ascending)
    //            {
    //                return -1;
    //            }
    //            else
    //            {
    //                return 1;
    //            }
    //        }
    //        else if (px.GetType().GetInterface("IComparable") != null)
    //        {
    //            if (_dir == direccionOrden.Ascending)
    //            {
    //                return ((IComparable)px).CompareTo(py);
    //            }
    //            else
    //            {
    //                return ((IComparable)py).CompareTo(px);
    //            }
    //        }
    //        else
    //        {
    //            return 0;
    //        }
    //    }



    //}

    //#endregion
}
