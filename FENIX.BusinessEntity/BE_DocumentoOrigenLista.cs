﻿using System;
using System.Collections.Generic;
using System.Text;
using FENIX.BusinessEntity;
using System.Reflection;
namespace FENIX.BusinessEntity
{
    [Serializable]
    public class BE_DocumentoOrigenLista : BE_DocumentoOrigen
    {
        String _sAnoManifiesto = null;
        String _sNumManifiesto = null;


        String _sPuertoEmbarqueDes = null;
        String _PuertoOrigenDes = null;

        String _sTipOperacionDes = null;
        String _sLineaDes = null;

        String _NotificanteObs = null;
        String _RolProcedenciaDes = null;
        String _ProcedenciaDes = null;
        String _ConsignatarioObs = null;
        String _ClienteConsolidadorFa = null;
        String _ClienteFacturado = null;
        String _Volant_vch_Numero = null;
        DateTime? _Volant_dt_FechaUltimoEnvio = null;
        String _AgenciaAduanaVolante = null;

        String _PuertoFinalDes = null;
        String _PuertoDescargaDes = null;
        String _sEmbarcadorDes = null;

        String _sNaveDes = null;
        String _sViajeDes = null;
        String _sRumboDes = null;

        String _sPtoEmb_CodAduana;
        String _sPtoOri_CodAduana;
        String _sPtoFin_CodAduana;
        String _sPtoDes_CodAduana;


        public String sPtoDes_CodAduana
        {
            get { return _sPtoDes_CodAduana; }
            set { _sPtoDes_CodAduana = value; }
        }

        public String sPtoFin_CodAduana
        {
            get { return _sPtoFin_CodAduana; }
            set { _sPtoFin_CodAduana = value; }
        }

        public String sPtoOri_CodAduana
        {
            get { return _sPtoOri_CodAduana; }
            set { _sPtoOri_CodAduana = value; }
        }



        public String sPtoEmb_CodAduana
        {
            get { return _sPtoEmb_CodAduana; }
            set { _sPtoEmb_CodAduana = value; }
        }

        public String sRumboDes
        {
            get { return _sRumboDes; }
            set { _sRumboDes = value; }
        }

        public String sViajeDes
        {
            get { return _sViajeDes; }
            set { _sViajeDes = value; }
        }

        public String sNaveDes
        {
            get { return _sNaveDes; }
            set { _sNaveDes = value; }
        }

        public String sEmbarcadorDes
        {
            get { return _sEmbarcadorDes; }
            set { _sEmbarcadorDes = value; }
        }

        public String sPuertoDescargaDes
        {
            get { return _PuertoDescargaDes; }
            set { _PuertoDescargaDes = value; }
        }



        public String sPuertoFinalDes
        {
            get { return _PuertoFinalDes; }
            set { _PuertoFinalDes = value; }
        }


        public String sAgenciaAduanaVolante
        {
            get { return _AgenciaAduanaVolante; }
            set { _AgenciaAduanaVolante = value; }
        }

        public DateTime? sFechaUltimoEnvio
        {
            get { return _Volant_dt_FechaUltimoEnvio; }
            set { _Volant_dt_FechaUltimoEnvio = value; }
        }

        public String sVolantNumero
        {
            get { return _Volant_vch_Numero; }
            set { _Volant_vch_Numero = value; }
        }

        public String sClienteFacturado
        {
            get { return _ClienteFacturado; }
            set { _ClienteFacturado = value; }
        }

        public String sClienteConsolidadorFa
        {
            get { return _ClienteConsolidadorFa; }
            set { _ClienteConsolidadorFa = value; }
        }

        public String sConsignatarioObs
        {
            get { return _ConsignatarioObs; }
            set { _ConsignatarioObs = value; }
        }

        public String sProcedenciaDes
        {
            get { return _ProcedenciaDes; }
            set { _ProcedenciaDes = value; }
        }

        public String sRolProcedenciaDes
        {
            get { return _RolProcedenciaDes; }
            set { _RolProcedenciaDes = value; }
        }


        public String sNotificanteObs
        {
            get { return _NotificanteObs; }
            set { _NotificanteObs = value; }
        }


        public String sLineaDes
        {
            get { return _sLineaDes; }
            set { _sLineaDes = value; }
        }


        public String sTipOperacionDes
        {
            get { return _sTipOperacionDes; }
            set { _sTipOperacionDes = value; }
        }



        public String sPuertoOrigenDes
        {
            get { return _PuertoOrigenDes; }
            set { _PuertoOrigenDes = value; }
        }


        public String sPuertoEmbarqueDes
        {
            get { return _sPuertoEmbarqueDes; }
            set { _sPuertoEmbarqueDes = value; }
        }


        public String sNumManifiesto
        {
            get { return _sNumManifiesto; }
            set { _sNumManifiesto = value; }
        }

        public String sAnoManifiesto
        {
            get { return _sAnoManifiesto; }
            set { _sAnoManifiesto = value; }
        }
    }

    #region Metodos
    [Serializable]
    public class BE_DocumentoOrigenListaList : List<BE_DocumentoOrigenLista>
    {
        public void Ordenar(string propertyName, direccionOrden Direction)
        {
            BE_DocumentoOrigenListaComparer dc = new BE_DocumentoOrigenListaComparer(propertyName, Direction);
            this.Sort(dc);
        }
    }

    class BE_DocumentoOrigenListaComparer : IComparer<BE_DocumentoOrigenLista>
    {
        string _prop = "";
        direccionOrden _dir;

        public BE_DocumentoOrigenListaComparer(string propertyName, direccionOrden Direction)
        {
            _prop = propertyName;
            _dir = Direction;
        }

        public int Compare(BE_DocumentoOrigenLista x, BE_DocumentoOrigenLista y)
        {

            PropertyInfo propertyX = x.GetType().GetProperty(_prop);
            PropertyInfo propertyY = y.GetType().GetProperty(_prop);

            object px = propertyX.GetValue(x, null);
            object py = propertyY.GetValue(y, null);

            if (px == null && py == null)
            {
                return 0;
            }
            else if (px != null && py == null)
            {
                if (_dir == direccionOrden.Ascending)
                {
                    return 1;
                }
                else
                {
                    return -1;
                }
            }
            else if (px == null && py != null)
            {
                if (_dir == direccionOrden.Ascending)
                {
                    return -1;
                }
                else
                {
                    return 1;
                }
            }
            else if (px.GetType().GetInterface("IComparable") != null)
            {
                if (_dir == direccionOrden.Ascending)
                {
                    return ((IComparable)px).CompareTo(py);
                }
                else
                {
                    return ((IComparable)py).CompareTo(px);
                }
            }
            else
            {
                return 0;
            }
        }





    }


    [Serializable]
    public class BE_DocumentoOrigenDetalleLista : BE_DocumentoOrigenDetalle
    {
        public Int32 iIdDocOri { get; set; }
        public String sNumeroDO { get; set; }
        public Int32 ? iIdDocOriDet { get; set; }
        public Int32 ? iIdMovBalIngreso { get; set; }
        public Int32 ? iTicketIngreso { get; set; }
        public String sFechaIngreso { get; set; }
        public Int32 ? iIdMovBalSalida { get; set; }
        public Int32 ? iTicketSalida { get; set; }
        public String sFechaSalida { get; set; }
        public String sCarga { get; set; }
        public String sCliente { get; set; }
        
    }

    [Serializable]
    public class BE_DocumentoOrigenDetalleListaList : List<BE_DocumentoOrigenDetalleLista>
    {
        public void Ordenar(string propertyName, direccionOrden Direction)
        {
            BE_DocumentoOrigenListaDetalleComparer dc = new BE_DocumentoOrigenListaDetalleComparer(propertyName, Direction);
            this.Sort(dc);
        }
    }

    class BE_DocumentoOrigenListaDetalleComparer : IComparer<BE_DocumentoOrigenDetalleLista>
    {
        string _prop = "";
        direccionOrden _dir;

        public BE_DocumentoOrigenListaDetalleComparer(string propertyName, direccionOrden Direction)
        {
            _prop = propertyName;
            _dir = Direction;
        }

        public int Compare(BE_DocumentoOrigenDetalleLista x, BE_DocumentoOrigenDetalleLista y)
        {

            PropertyInfo propertyX = x.GetType().GetProperty(_prop);
            PropertyInfo propertyY = y.GetType().GetProperty(_prop);

            object px = propertyX.GetValue(x, null);
            object py = propertyY.GetValue(y, null);

            if (px == null && py == null)
            {
                return 0;
            }
            else if (px != null && py == null)
            {
                if (_dir == direccionOrden.Ascending)
                {
                    return 1;
                }
                else
                {
                    return -1;
                }
            }
            else if (px == null && py != null)
            {
                if (_dir == direccionOrden.Ascending)
                {
                    return -1;
                }
                else
                {
                    return 1;
                }
            }
            else if (px.GetType().GetInterface("IComparable") != null)
            {
                if (_dir == direccionOrden.Ascending)
                {
                    return ((IComparable)px).CompareTo(py);
                }
                else
                {
                    return ((IComparable)py).CompareTo(px);
                }
            }
            else
            {
                return 0;
            }
        }





    }
    #endregion
}
