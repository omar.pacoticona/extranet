﻿using System;
using System.Collections.Generic;
using System.Text;
using FENIX.Common;
using System.Reflection;

namespace FENIX.BusinessEntity
{
    [Serializable]
    public class BE_Indicador : Paginador
    {
        #region "Campos"
            int? _IdIndicador;
            int? _Anho;
            int? _Mes;
            int? _TipoIndicador;
            int? _Agrupacion;
            int? _ValorTotal;
            int? _DatoMostrar;

            String _Valor = null;
            String _Objetivo = null;
            String _Cumplimiento = null;
            String _NombreIndicador = null;
            String _TotalxItem = null;
            String _Porcentaje = null;
            String _Estado = null;
            String _Propietario = null;
            String _Descripcion = null;
            String _VerDetalle = null;
            String _Formula = null;
            String _Modulo = null;


            String _Semana1 = null;
            String _Semana2 = null;
            String _Semana3 = null;
            String _Semana4 = null;
            String _Semana5 = null;
            String _Semana6 = null;

            String _Mes1 = null;
            String _Mes2 = null;
            String _Mes3 = null;
            String _Mes4 = null;
            String _Mes5 = null;
            String _Mes6 = null;
            String _Mes7 = null;
            String _Mes8 = null;
            String _Mes9 = null;
            String _Mes10 = null;
            String _Mes11 = null;
            String _Mes12 = null;
            String _Mes13 = null;

            String _ObjetivoMes1 = null;
            String _ObjetivoMes2 = null;
            String _ObjetivoMes3 = null;
            String _ObjetivoMes4 = null;
            String _ObjetivoMes5 = null;
            String _ObjetivoMes6 = null;
            String _ObjetivoMes7 = null;
            String _ObjetivoMes8 = null;
            String _ObjetivoMes9 = null;
            String _ObjetivoMes10 = null;
            String _ObjetivoMes11 = null;
            String _ObjetivoMes12 = null;
            String _ObjetivoMes13 = null;

            String _Objetivo1 = null;
            String _Objetivo2 = null;
            String _Objetivo3 = null;
            String _Objetivo4 = null;
            String _Objetivo5 = null;
            String _Objetivo6 = null;
            String _Objetivo7 = null;
            String _Objetivo8 = null;
            String _Objetivo9 = null;
            String _Objetivo10 = null;
            String _Objetivo11 = null;
            String _Objetivo12 = null;
            String _Objetivo13 = null;

            Int32 _Item;

            Decimal? _dObjetivo = null;
            Decimal? _dMes1 = null;
            Decimal? _dMes2 = null;
            Decimal? _dMes3 = null;
            Decimal? _dMes4 = null;
            Decimal? _dMes5 = null;
            Decimal? _dMes6 = null;
            Decimal? _dMes7 = null;
            Decimal? _dMes8 = null;
            Decimal? _dMes9 = null;
            Decimal? _dMes10 = null;
            Decimal? _dMes11 = null;
            Decimal? _dMes12 = null;
            Decimal? _dMes13 = null;
            Decimal? _dMes = null;
            Decimal? _dMesPorc = null;

            Decimal? _dObjetivo1 = null;
            Decimal? _dObjetivo2 = null;
            Decimal? _dObjetivo3 = null;
            Decimal? _dObjetivo4 = null;
            Decimal? _dObjetivo5 = null;
            Decimal? _dObjetivo6 = null;
            Decimal? _dObjetivo7 = null;
            Decimal? _dObjetivo8 = null;
            Decimal? _dObjetivo9 = null;
            Decimal? _dObjetivo10 = null;
            Decimal? _dObjetivo11 = null;
            Decimal? _dObjetivo12 = null;
            Decimal? _dObjetivo13 = null;

            String _sNombreSerie = null;

            String _Color = null;
            String _Color2 = null;
            String _Color3 = null;
            String _Color4 = null;
            String _Color5 = null;
            String _Color6 = null;
            String _Color7 = null;
            
            String _Tooltip = null;
            String _Complemento = null;
        #endregion


        #region "Propiedades"
            public int? iIdIndicador
            {
                get { return _IdIndicador; }
                set { _IdIndicador = value; }
            }    
            public int? iAnho
            {
                get { return _Anho; }
                set { _Anho = value; }
            }
            public int? iMes
            {
                get { return _Mes; }
                set { _Mes = value; }
            }
            public int? iDatoMostrar
            {
                get { return _DatoMostrar; }
                set { _DatoMostrar = value; }
            }
            public int? iValorTotal
            {
                get { return _ValorTotal; }
                set { _ValorTotal = value; }
            }
            public int? iAgrupacion
            {
                get { return _Agrupacion; }
                set { _Agrupacion = value; }
            }
            public int? iTipoIndicador
            {
                get { return _TipoIndicador; }
                set { _TipoIndicador = value; }
            }

            public String sValor
            {
                get { return _Valor; }
                set { _Valor = value; }
            }
            public String sObjetivo
            {
                get { return _Objetivo; }
                set { _Objetivo = value; }
            }
            public String sCumplimiento
            {
                get { return _Cumplimiento; }
                set { _Cumplimiento = value; }
            }
            public String sNombreIndicador
            {
                get { return _NombreIndicador; }
                set { _NombreIndicador = value; }
            }
            public String sTotalItem
            {
                get { return _TotalxItem; }
                set { _TotalxItem = value; }
            }
            public String sPorcentaje
            {
                get { return _Porcentaje; }
                set { _Porcentaje = value; }
            }
            public String sEstado
            {
                get { return _Estado; }
                set { _Estado = value; }
            }
            public String sPropietario
            {
                get { return _Propietario; }
                set { _Propietario = value; }
            }
            public String sVerDetalle
            {
                get { return _VerDetalle; }
                set { _VerDetalle = value; }
            }
            public String sFormula
            {
                get { return _Formula; }
                set { _Formula = value; }
            }

            public String sColor
            {
                get { return _Color; }
                set { _Color = value; }
            }
            public String sColor2
            {
                get { return _Color2; }
                set { _Color2 = value; }
            }
            public String sColor3
            {
                get { return _Color3; }
                set { _Color3 = value; }
            }
            public String sColor4
            {
                get { return _Color4; }
                set { _Color4 = value; }
            }
            public String sColor5
            {
                get { return _Color5; }
                set { _Color5 = value; }
            }
            public String sColor6
            {
                get { return _Color6; }
                set { _Color6 = value; }
            }
            public String sColor7
            {
                get { return _Color7; }
                set { _Color7 = value; }
            }
          
            public String sTooltip
            {
                get { return _Tooltip; }
                set { _Tooltip = value; }
            }
            public String sComplemento
            {
                get { return _Complemento; }
                set { _Complemento = value; }
            }
            public String sDescripcion
            {
                get { return _Descripcion; }
                set { _Descripcion = value; }
            }
            public String sModulo
            {
                get { return _Modulo; }
                set { _Modulo = value; }
            }

            public String sSemana1
            {
                get { return _Semana1; }
                set { _Semana1 = value; }
            }
            public String sSemana2
            {
                get { return _Semana2; }
                set { _Semana2 = value; }
            }
            public String sSemana3
            {
                get { return _Semana3; }
                set { _Semana3 = value; }
            }
            public String sSemana4
            {
                get { return _Semana4; }
                set { _Semana4 = value; }
            }
            public String sSemana5
            {
                get { return _Semana5; }
                set { _Semana5 = value; }
            }
            public String sSemana6
            {
                get { return _Semana6; }
                set { _Semana6 = value; }
            }

            public String sMes1
            {
                get { return _Mes1; }
                set { _Mes1 = value; }
            }
            public String sMes2
            {
                get { return _Mes2; }
                set { _Mes2 = value; }
            }
            public String sMes3
            {
                get { return _Mes3; }
                set { _Mes3 = value; }
            }
            public String sMes4
            {
                get { return _Mes4; }
                set { _Mes4 = value; }
            }
            public String sMes5
            {
                get { return _Mes5; }
                set { _Mes5 = value; }
            }
            public String sMes6
            {
                get { return _Mes6; }
                set { _Mes6 = value; }
            }
            public String sMes7
            {
                get { return _Mes7; }
                set { _Mes7 = value; }
            }
            public String sMes8
            {
                get { return _Mes8; }
                set { _Mes8 = value; }
            }
            public String sMes9
            {
                get { return _Mes9; }
                set { _Mes9 = value; }
            }
            public String sMes10
            {
                get { return _Mes10; }
                set { _Mes10 = value; }
            }
            public String sMes11
            {
                get { return _Mes11; }
                set { _Mes11 = value; }
            }
            public String sMes12
            {
                get { return _Mes12; }
                set { _Mes12 = value; }
            }
            public String sMes13
            {
                get { return _Mes13; }
                set { _Mes13 = value; }
            }

            public String sObjetivoMes1
            {
                get { return _ObjetivoMes1; }
                set { _ObjetivoMes1 = value; }
            }
            public String sObjetivoMes2
            {
                get { return _ObjetivoMes2; }
                set { _ObjetivoMes2 = value; }
            }
            public String sObjetivoMes3
            {
                get { return _ObjetivoMes3; }
                set { _ObjetivoMes3 = value; }
            }
            public String sObjetivoMes4
            {
                get { return _ObjetivoMes4; }
                set { _ObjetivoMes4 = value; }
            }
            public String sObjetivoMes5
            {
                get { return _ObjetivoMes5; }
                set { _ObjetivoMes5 = value; }
            }
            public String sObjetivoMes6
            {
                get { return _ObjetivoMes6; }
                set { _ObjetivoMes6 = value; }
            }
            public String sObjetivoMes7
            {
                get { return _ObjetivoMes7; }
                set { _ObjetivoMes7 = value; }
            }
            public String sObjetivoMes8
            {
                get { return _ObjetivoMes8; }
                set { _ObjetivoMes8 = value; }
            }
            public String sObjetivoMes9
            {
                get { return _ObjetivoMes9; }
                set { _ObjetivoMes9 = value; }
            }
            public String sObjetivoMes10
            {
                get { return _ObjetivoMes10; }
                set { _ObjetivoMes10 = value; }
            }
            public String sObjetivoMes11
            {
                get { return _ObjetivoMes11; }
                set { _ObjetivoMes11 = value; }
            }
            public String sObjetivoMes12
            {
                get { return _ObjetivoMes12; }
                set { _ObjetivoMes12 = value; }
            }
            public String sObjetivoMes13
            {
                get { return _ObjetivoMes13; }
                set { _ObjetivoMes13 = value; }
            }

            public String sObjetivo1
            {
                get { return _Objetivo1; }
                set { _Objetivo1 = value; }
            }
            public String sObjetivo2
            {
                get { return _Objetivo2; }
                set { _Objetivo2 = value; }
            }
            public String sObjetivo3
            {
                get { return _Objetivo3; }
                set { _Objetivo3 = value; }
            }
            public String sObjetivo4
            {
                get { return _Objetivo4; }
                set { _Objetivo4 = value; }
            }
            public String sObjetivo5
            {
                get { return _Objetivo5; }
                set { _Objetivo5 = value; }
            }
            public String sObjetivo6
            {
                get { return _Objetivo6; }
                set { _Objetivo6 = value; }
            }
            public String sObjetivo7
            {
                get { return _Objetivo7; }
                set { _Objetivo7 = value; }
            }
            public String sObjetivo8
            {
                get { return _Objetivo8; }
                set { _Objetivo8 = value; }
            }
            public String sObjetivo9
            {
                get { return _Objetivo9; }
                set { _Objetivo9 = value; }
            }
            public String sObjetivo10
            {
                get { return _Objetivo10; }
                set { _Objetivo10 = value; }
            }
            public String sObjetivo11
            {
                get { return _Objetivo11; }
                set { _Objetivo11 = value; }
            }
            public String sObjetivo12
            {
                get { return _Objetivo12; }
                set { _Objetivo12 = value; }
            }
            public String sObjetivo13
            {
                get { return _Objetivo13; }
                set { _Objetivo13 = value; }
            }

            public Int32 iItem
            {
                get { return _Item; }
                set { _Item = value; }
            }

            public Decimal? dObjetivo
            {
                get { return _dObjetivo; }
                set { _dObjetivo = value; }
            }
            public Decimal? dMes1
            {
                get { return _dMes1; }
                set { _dMes1 = value; }
            }
            public Decimal? dMes2
            {
                get { return _dMes2; }
                set { _dMes2 = value; }
            }
            public Decimal? dMes3
            {
                get { return _dMes3; }
                set { _dMes3 = value; }
            }
            public Decimal? dMes4
            {
                get { return _dMes4; }
                set { _dMes4 = value; }
            }
            public Decimal? dMes5
            {
                get { return _dMes5; }
                set { _dMes5 = value; }
            }
            public Decimal? dMes6
            {
                get { return _dMes6; }
                set { _dMes6 = value; }
            }
            public Decimal? dMes7
            {
                get { return _dMes7; }
                set { _dMes7 = value; }
            }
            public Decimal? dMes8
            {
                get { return _dMes8; }
                set { _dMes8 = value; }
            }
            public Decimal? dMes9
            {
                get { return _dMes9; }
                set { _dMes9 = value; }
            }
            public Decimal? dMes10
            {
                get { return _dMes10; }
                set { _dMes10 = value; }
            }
            public Decimal? dMes11
            {
                get { return _dMes11; }
                set { _dMes11 = value; }
            }
            public Decimal? dMes12
            {
                get { return _dMes12; }
                set { _dMes12 = value; }
            }
            public Decimal? dMes13
            {
                get { return _dMes13; }
                set { _dMes13 = value; }
            }
            public Decimal? dMes
            {
                get { return _dMes; }
                set { _dMes = value; }
            }

            public Decimal? dObjetivo1
            {
                get { return _dObjetivo1; }
                set { _dObjetivo1 = value; }
            }
            public Decimal? dObjetivo2
            {
                get { return _dObjetivo2; }
                set { _dObjetivo2 = value; }
            }
            public Decimal? dObjetivo3
            {
                get { return _dObjetivo3; }
                set { _dObjetivo3 = value; }
            }
            public Decimal? dObjetivo4
            {
                get { return _dObjetivo4; }
                set { _dObjetivo4 = value; }
            }
            public Decimal? dObjetivo5
            {
                get { return _dObjetivo5; }
                set { _dObjetivo5 = value; }
            }
            public Decimal? dObjetivo6
            {
                get { return _dObjetivo6; }
                set { _dObjetivo6 = value; }
            }
             public Decimal? dObjetivo7
            {
                get { return _dObjetivo7; }
                set { _dObjetivo7 = value; }
            }

                public Decimal? dObjetivo8
            {
                get { return _dObjetivo8; }
                set { _dObjetivo8 = value; }
            }
            public Decimal? dObjetivo9
            {
                get { return _dObjetivo9; }
                set { _dObjetivo9 = value; }
            }
            public Decimal? dObjetivo10
            {
                get { return _dObjetivo10; }
                set { _dObjetivo10 = value; }
            }
            public Decimal? dObjetivo11
            {
                get { return _dObjetivo11; }
                set { _dObjetivo11 = value; }
            }
            public Decimal? dObjetivo12
            {
                get { return _dObjetivo12; }
                set { _dObjetivo12 = value; }
            }
            public Decimal? dObjetivo13
            {
                get { return _dObjetivo13; }
                set { _dObjetivo13 = value; }
            }

            


            public Decimal? dMesPorc
            {
                get { return _dMesPorc; }
                set { _dMesPorc = value; }
            }
            public String sNombreSerie
            {
                get { return _sNombreSerie; }
                set { _sNombreSerie = value; }
            }
        #endregion
    }
}
