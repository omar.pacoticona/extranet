﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FENIX.BusinessEntity
{
    [Serializable]
    public class BE_Refrendo
    {
        Int32 _IdExportador;
        Int32 _IdAgenciaAguanas;
        Int32 _IdNave;
        String _sExportador;
        String _sAgenciaAduanas;
        String _Nave;
        String _sViaje;
        String _sRumbo;
        Int32 _IdBooking;
        Int32? _idDocori;
        String _sBooking;
        String _sDocOri;
        Int32 _IdCliente;
        String _sNombreArchivo;
        String _sRutaArchivo;
        Int32 _iIdusuario;
        Int32 _iIdTipoDocumento;
        String _sUsuario;
        Int32 _iIdRefrendo;
        Int32 _iIdExportador;
        Int32 _iIdTipoPago;
        Int32? _iIdFormaPago;
        String _sIdBancoOrigen;
        String _sIdBancoDestino;
        Int32? _iIdMoneda;
        decimal? _dMonto;
        String _sNroOperacion;
        String _sComentarios;
        Int32 _iIdArchivoRefrendo;
        Int32 _iIdSolicitudRefr;
        Int32 _iIdSolicitudRefrDet;
        String _sSolicitudRefr;
        String _sMesIni;
        String _sMesFin;
        String _sSituacion;
        Int32 _NRegistros;
        Int32 _NPagina;
        Int32 _NtotalRegistros;
        String _sNroDua;
        String _sNombreSituacion;
        String _sContenedor;
        String _sTipoContenedor;
        String _sDimension;
        decimal? _dTara;
        decimal? _dPesoNeto;
        Int32 _iIdBookDetalle;
        Int32 _iIdDocOridet;
        String _sBultos;
        String _sComentariosUsuario;
        Int32 _iIdUsuarioAsignado;
        Int32 _iIdCondicionTransmitir;
        Int32 _iIdValor;
        String _sValor;
        String _sCanal;
        String _sPrecintoAduanas;
        String s_PrecintoLinea;
        String _dtCargaSeca;
        String _dtCargaRefrigerada;
        Int32 _iIdDocori;
        String _sRuc;
        String _sRazSocial;
        Int32? _iIdLiquidacion;
        Int32? _iIdCondicionCarga;
        Int32? _iConexionCarga;
        String _sClienteFacturar;
        String _sNave;
        Int32 _iIdDespachador;
        String _sDniDesp;
        String s_NombreDesp;

        public String sNombreDesp
        {
            get
            {
                return s_NombreDesp;
            }
            set
            {
                s_NombreDesp = value;
            }
        }

        public String sDniDesp
        {
            get
            {
                return _sDniDesp;
            }
            set
            {
                _sDniDesp = value;
            }
        }

        public Int32 iIdDespachador
        {
            get
            {
                return _iIdDespachador;
            }
            set
            {
                _iIdDespachador = value;
            }
        }

        public String sClienteFacturar
        {
            get
            {
                return _sClienteFacturar;
            }
            set
            {
                _sClienteFacturar = value;
            }
        }
        public String sNave
        {
            get
            {
                return _sNave;
            }
            set
            {
                _sNave = value;
            }
        }

        public Int32? iIConexionCarga
        {
            get
            {
                return _iConexionCarga;
            }
            set
            {
                _iConexionCarga = value;
            }
        }

        public Int32? iIdCondicionCarga
        {
            get
            {
                return _iIdCondicionCarga;
            }
            set
            {
                _iIdCondicionCarga = value;
            }
        }


        public Int32? iIdLiquidacion
        {
            get
            {
                return _iIdLiquidacion;
            }
            set
            {
                _iIdLiquidacion = value;
            }
        }



        public String sRazSocial
        {
            get
            {
                return _sRazSocial;
            }
            set
            {
                _sRazSocial = value;
            }
        }

        public String sRuc
        {
            get
            {
                return _sRuc;
            }
            set
            {
                _sRuc = value;
            }
        }

        public Int32 iIdDocori
        {
            get
            {
                return _iIdDocori;
            }
            set
            {
                _iIdDocori = value;
            }
        }

        public String dtCargaSeca
        {
            get
            {
                return _dtCargaSeca;
            }
            set
            {
                _dtCargaSeca = value;
            }
        }

        public String dtCargaRefrigerada
        {
            get
            {
                return _dtCargaRefrigerada;
            }
            set
            {
                _dtCargaRefrigerada = value;
            }
        }

        public String sPrecintoAduanas
        {
            get
            {
                return _sPrecintoAduanas;
            }
            set
            {
                _sPrecintoAduanas = value;
            }
        }

        public String sPrecintoLinea
        {
            get
            {
                return s_PrecintoLinea;
            }
            set
            {
                s_PrecintoLinea = value;
            }
        }


        public String sCanal
        {
            get
            {
                return _sCanal;
            }
            set
            {
                _sCanal = value;
            }
        }

        public String sValor
        {
            get
            {
                return _sValor;
            }
            set
            {
                _sValor = value;
            }
        }


        public Int32 iIdValor
        {
            get
            {
                return _iIdValor;
            }
            set
            {
                _iIdValor = value;
            }
        }

        public Int32 iIdCondicionTransmitir
        {
            get
            {
                return _iIdCondicionTransmitir;
            }
            set
            {
                _iIdCondicionTransmitir = value;
            }
        }

        public Int32 iIdUsuarioAsignado
        {
            get
            {
                return _iIdUsuarioAsignado;
            }
            set
            {
                _iIdUsuarioAsignado = value;
            }
        }

        public String sComentariosUsuario
        {
            get
            {
                return _sComentariosUsuario;
            }
            set
            {
                _sComentariosUsuario = value;
            }
        }


        public String sBultos
        {
            get
            {
                return _sBultos;
            }
            set
            {
                _sBultos = value;
            }
        }

        public Int32 iIdDocOridet
        {
            get
            {
                return _iIdDocOridet;
            }
            set
            {
                _iIdDocOridet = value;
            }
        }

        public Int32 iIdBookDetalle
        {
            get
            {
                return _iIdBookDetalle;
            }
            set
            {
                _iIdBookDetalle = value;
            }
        }

        public decimal? dPesoNeto
        {
            get
            {
                return _dPesoNeto;
            }
            set
            {
                _dPesoNeto = value;
            }
        }


        public decimal? dTara
        {
            get
            {
                return _dTara;
            }
            set
            {
                _dTara = value;
            }
        }



        public String sDimension
        {
            get
            {
                return _sDimension;
            }
            set
            {
                _sDimension = value;
            }
        }


        public String sTipoContenedor
        {
            get
            {
                return _sTipoContenedor;
            }
            set
            {
                _sTipoContenedor = value;
            }
        }

        public String sContenedor
        {
            get
            {
                return _sContenedor;
            }
            set
            {
                _sContenedor = value;
            }
        }

        public String sNombreSituacion
        {
            get
            {
                return _sNombreSituacion;
            }
            set
            {
                _sNombreSituacion = value;
            }
        }
        public String sNroDua
        {
            get
            {
                return _sNroDua;
            }
            set
            {
                _sNroDua = value;
            }
        }

        public Int32 NRegistros
        {
            get
            {
                return _NRegistros;
                
            }
            set
            {
                _NRegistros = value;
            }
        }

        public Int32 NPagina
        {
            get
            {
                return _NPagina;
            }
            set
            {
                _NPagina = value;
            }
        }

        public Int32 NtotalRegistros
        {
            get
            {
                return _NtotalRegistros;
            }
            set
            {
                _NtotalRegistros = value;
            }
        }

        public String sSituacion
        {
            get
            {
                return _sSituacion;
            }
            set
            {
                _sSituacion = value;
            }
        }

        public String sMesIni
        {
            get
            {
                return _sMesIni;
            }
            set
            {
                _sMesIni = value;
            }
        }

        public String sMesFin
        {
            get
            {
                return _sMesFin;
            }
            set
            {
                _sMesFin = value;
            }
        }

        public String sSolicitudRefr
        {
            get
            {
                return _sSolicitudRefr;
            }
            set
            {
                _sSolicitudRefr = value;
            }
        }

        public Int32 iIdSolicitudRefrDet
        {
            get
            {
                return _iIdSolicitudRefrDet;
            }
            set
            {
                _iIdSolicitudRefrDet = value;
            }
        }

        public Int32 iIdSolicitudRefr
        {
            get
            {
                return _iIdSolicitudRefr;
            }
            set
            {
                _iIdSolicitudRefr = value;
            }
        }

        public Int32 iIdArchivoRefrendo
        {
            get
            {
                return _iIdArchivoRefrendo;
            }
            set
            {
                _iIdArchivoRefrendo = value;
            }
        }

        public String sComentarios
        {
            get
            {
                return _sComentarios;
            }
            set
            {
                _sComentarios = value;
            }
        }


        public String sNroOperacion
        {
            get
            {
                return _sNroOperacion;
            }
            set
            {
                _sNroOperacion = value;
            }
        }
        public decimal? dMonto
        {
            get
            {
                return _dMonto;
            }
            set
            {
                _dMonto = value;
            }
        }

        public Int32? iIdMoneda
        {
            get
            {
                return _iIdMoneda;
            }
            set
            {
                _iIdMoneda = value;
            }
        }

        public String sIdBancoDestino
        {
            get
            {
                return _sIdBancoDestino;
            }
            set
            {
                _sIdBancoDestino = value;
            }
        }

        public String sIdBancoOrigen
        {
            get
            {
                return _sIdBancoOrigen;
            }
            set
            {
                _sIdBancoOrigen = value;
            }
        }

        public Int32? iIdFormaPago
        {
            get
            {
                return _iIdFormaPago;
            }
            set
            {
                _iIdFormaPago = value;
            }
        }

        public Int32 iIdTipoPago
        {
            get
            {
                return _iIdTipoPago;
            }
            set
            {
                _iIdTipoPago = value;
            }
        }

        public Int32 iIdExportador
        {
            get
            {
                return _iIdExportador;
            }
            set
            {
                _iIdExportador = value;
            }
        }

        public Int32 iIdRefrendo
        {
            get
            {
                return _iIdRefrendo;
            }
            set
            {
                _iIdRefrendo = value;
            }
        }
        public String sUsuario
        {
            get
            {
                return _sUsuario;
            }
            set
            {
                _sUsuario = value;
            }
        }

        public Int32 iIdTipoDocumento
        {
            get
            {
                return _iIdTipoDocumento;
            }
            set
            {
                _iIdTipoDocumento = value;
            }
        }

        public Int32 iIdusuario
        {
            get
            {
                return _iIdusuario;
            }
            set
            {
                _iIdusuario = value;
            }
        }

        public String sRutaArchivo
        {
            get
            {
                return _sRutaArchivo;
            }
            set
            {
                _sRutaArchivo = value;
            }
        }

        public String sNombreArchivo
        {
            get
            {
                return _sNombreArchivo;
            }
            set
            {
                _sNombreArchivo = value;
            }
        }

        public Int32 IdCliente
        {
            get
            {
                return _IdCliente;
            }
            set
            {
                _IdCliente = value;
            }
        }

        public String sDocOri
        {
            get
            {
                return _sDocOri;
            }
            set
            {
                _sDocOri = value;
            }
        }

        public String sBooking
        {
            get
            {
                return _sBooking;
            }
            set
            {
                _sBooking = value;
            }
        }

        public Int32? IidDocori
        {
            get
            {
                return _idDocori;
            }
            set
            {
                _idDocori = value;
            }
        }


        public Int32 IdBooking
        {
            get
            {
                return _IdBooking;
            }
            set
            {
                _IdBooking = value;
            }
        }

        public String sRumbo
        {
            get
            {
                return _sRumbo;
            }
            set
            {
                _sRumbo = value;
            }
        }

        public String sViaje
        {
            get
            {
                return _sViaje;
            }
            set
            {
                _sViaje = value;
            }
        }

        public String Nave
        {
            get
            {
                return _Nave;
            }
            set
            {
                _Nave = value;
            }
        }

        public String sAgenciaAduanas
        {
            get
            {
                return _sAgenciaAduanas;
            }
            set
            {
                _sAgenciaAduanas = value;
            }
        }

        public String sExportador
        {
            get
            {
                return _sExportador;
            }
            set
            {
                _sExportador = value;
            }
        }

        public Int32 IdNave
        {
            get
            {
                return _IdNave;
            }
            set
            {
                _IdNave = value;
            }
        }

        public Int32 IdAgenciaAguanas
        {
            get
            {
                return _IdAgenciaAguanas;
            }
            set
            {
                _IdAgenciaAguanas = value;
            }
        }


        public Int32 IdExportador
        {
            get
            {
                return _IdExportador;
            }
            set
            {
                _IdExportador = value;
            }
        }









    }

   
}
