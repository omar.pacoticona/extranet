﻿using System;
using System.Collections.Generic;
using System.Text;
using FENIX.BusinessEntity;
using System.Reflection;

namespace FENIX.BusinessEntity
{
    [Serializable]
    public class BE_PlacaChofer
    {
        int _iIdRegistro;
        String _sIdAutRet;
        String _sPlaca;
        String _sChofer;
        String _sDoc;
        String _sUsuario;
        String _sPC;
        String _sTipo;
        String _sTicket;
        String _sFSalida;
        String _sBrevete;
        int _iIdAutRet;

        public DateTime dtFechaRegistro { get; set; }
        public String sSituacion { get; set; }
        public string sIdAutRet
        {
            get
            {
                return _sIdAutRet;
            }

            set
            {
                _sIdAutRet = value;
            }
        }

        public string sBrevete
        {
            get
            {
                return _sBrevete;
            }

            set
            {
                _sBrevete = value;
            }
        }

        public string sTicket
        {
            get
            {
                return _sTicket;
            }

            set
            {
                _sTicket = value;
            }
        }

        public string sFSalida
        {
            get
            {
                return _sFSalida;
            }

            set
            {
                _sFSalida = value;
            }
        }

        public string sTipo
        {
            get
            {
                return _sTipo;
            }

            set
            {
                _sTipo = value;
            }
        }
        public int IIdRegistro
        {
            get
            {
                return _iIdRegistro;
            }

            set
            {
                _iIdRegistro = value;
            }
        }

        public string SPlaca
        {
            get
            {
                return _sPlaca;
            }

            set
            {
                _sPlaca = value;
            }
        }

        public string SChofer
        {
            get
            {
                return _sChofer;
            }

            set
            {
                _sChofer = value;
            }
        }

        public string SDoc
        {
            get
            {
                return _sDoc;
            }

            set
            {
                _sDoc = value;
            }
        }

        public string SUsuario
        {
            get
            {
                return _sUsuario;
            }

            set
            {
                _sUsuario = value;
            }
        }

        public string SPC
        {
            get
            {
                return _sPC;
            }

            set
            {
                _sPC = value;
            }
        }

        public int IIdAutRet
        {
            get
            {
                return _iIdAutRet;
            }

            set
            {
                _iIdAutRet = value;
            }
        }
    }

    [Serializable]
    public class BE_PlacaChoferList : List<BE_PlacaChofer>
    {
        public void Ordenar(string propertyName, direccionOrden Direction)
        {
            BE_PlacaChoferComparer dc = new BE_PlacaChoferComparer(propertyName, Direction);
            this.Sort(dc);
        }
    }

    class BE_PlacaChoferComparer : IComparer<BE_PlacaChofer>
    {
        string _prop = "";
        direccionOrden _dir;

        public BE_PlacaChoferComparer(string propertyName, direccionOrden Direction)
        {
            _prop = propertyName;
            _dir = Direction;
        }

        public int Compare(BE_PlacaChofer x, BE_PlacaChofer y)
        {

            PropertyInfo propertyX = x.GetType().GetProperty(_prop);
            PropertyInfo propertyY = y.GetType().GetProperty(_prop);

            object px = propertyX.GetValue(x, null);
            object py = propertyY.GetValue(y, null);

            if (px == null && py == null)
            {
                return 0;
            }
            else if (px != null && py == null)
            {
                if (_dir == direccionOrden.Ascending)
                {
                    return 1;
                }
                else
                {
                    return -1;
                }
            }
            else if (px == null && py != null)
            {
                if (_dir == direccionOrden.Ascending)
                {
                    return -1;
                }
                else
                {
                    return 1;
                }
            }
            else if (px.GetType().GetInterface("IComparable") != null)
            {
                if (_dir == direccionOrden.Ascending)
                {
                    return ((IComparable)px).CompareTo(py);
                }
                else
                {
                    return ((IComparable)py).CompareTo(px);
                }
            }
            else
            {
                return 0;
            }
        }

    }

}
