﻿using System;
using System.Collections.Generic;
using System.Text;
using FENIX.Common;
using System.Reflection;

namespace FENIX.BusinessEntity
{
    [Serializable]
    public class BE_Contenedor : Paginador
    {
        #region "Campos"

        int _IdContenedor = 0;
        int _IdLineaMaritima = 0;
        int? _IdTipoContenedor = 0;
        int? _IdDimension = 0;
        int _IdLineaNegocio = 0;
        Decimal _StoCon_dec_Temperatura = 0;
        String _StoCon_chr_Conexion;
        Decimal _StoCon_dec_BultosTotal = 0;

        Decimal _StoCon_dec_Payload = 0;
        decimal _vi_StoCon_dec_PesoNetoTotal = 0;
        decimal _vi_StoCon_dec_PesoBrutoTotal = 0;
        int _IdNaveViaje = 0;
        int _IdDocOri = 0;
        int _IdDocOriDet = 0;
        int _IdUnidadMedida = 0;
        int _iIdCondicionCnt = 0;
        String _Vch_Contenedor = string.Empty;
        Decimal _Dec_Tara = 0;
        Decimal _Dec_CargaMaxima = 0;
        String _Vch_Iso = string.Empty;
        Boolean _Bln_ShipperOwn = false;
        String _Vch_DesLineaMaritima = string.Empty;
        String _Vch_DesTipoContenedor = string.Empty;
        String _Vch_DesDimension = string.Empty;
        String _Precin_vch_Despachado;
        String _Precin_vch_Recibido;
        String _Precin_vch_Manifestado;

        String _Precin_vch_Linea;
        String _Precin_vch_Aduana;
        String _Precin_vch_Cliente;
        String _Precin_vch_Otros;
        String _Nave;
        String _Viaje;
        String _Rumbo;

        String _sSituacion;

        int? _iIdPrecinto;
        int? _IdStoCon;
        int? _IdOperacion;

        //Added for containers with list type
        Int32 _IdAutRet;
        DateTime _Vigencia;
        Int32 _Dia;
        String _sPresencia;
        Int32 _selCont;
        Int32 _IdAutRetDet;
        String _sUsuario;
        String _sNombrePC;


        #endregion


        #region "Propiedades"
        //container list  type
        public Int32 IdAutRet
        {
            get { return _IdAutRet; }
            set { _IdAutRet = value; }
        }
        public DateTime Vigencia
        {
            get { return _Vigencia; }
            set { _Vigencia = value; }
        }
        public Int32 Dia
        {
            get { return _Dia; }
            set { _Dia = value; }
        }
        public String sPresencia
        {
            get { return _sPresencia; }
            set { _sPresencia = value; }
        }
        public Int32 selCont
        {
            get { return _selCont; }
            set { _selCont = value; }
        }
        public Int32 IdAutRetDet
        {
            get { return _IdAutRetDet; }
            set { _IdAutRetDet = value; }
        }
        public String sUsuario
        {
            get { return _sUsuario; }
            set { _sUsuario = value; }
        }
        public String sNombrePC
        {
            get { return _sNombrePC; }
            set { _sNombrePC = value; }
        }
        //container list type
        public String sNave
        {
            get { return _Nave; }
            set { _Nave = value; }
        }

        public String sRumbo
        {
            get { return _Rumbo; }
            set { _Rumbo = value; }
        }
        public String sViaje
        {
            get { return _Viaje; }
            set { _Viaje = value; }
        }

        public int? iIdOperacion
        {
            get { return _IdOperacion; }
            set { _IdOperacion = value; }
        }
        public int? iIdPrecinto
        {
            get { return _iIdPrecinto; }
            set { _iIdPrecinto = value; }
        }

        public String sSituacion
        {
            get { return _sSituacion; }
            set { _sSituacion = value; }
        }

        public int iIdCondicionCnt
        {
            get { return _iIdCondicionCnt; }
            set { _iIdCondicionCnt = value; }
        }


        public int iIdUnidadMedida
        {
            get { return _IdUnidadMedida; }
            set { _IdUnidadMedida = value; }
        }

        public String sPrecinOtros
        {
            get { return _Precin_vch_Otros; }
            set { _Precin_vch_Otros = value; }
        }
        public String sPrecinCliente
        {
            get { return _Precin_vch_Cliente; }
            set { _Precin_vch_Cliente = value; }
        }
        public String sPrecinAduana
        {
            get { return _Precin_vch_Aduana; }
            set { _Precin_vch_Aduana = value; }
        }
        public String sPrecinLinea
        {
            get { return _Precin_vch_Linea; }
            set { _Precin_vch_Linea = value; }
        }

        public int? iIdStoCon
        {
            get { return _IdStoCon; }
            set { _IdStoCon = value; }
        }
        public String sPrecintoManifestado
        {
            get { return _Precin_vch_Manifestado; }
            set { _Precin_vch_Manifestado = value; }
        }
        public String sPrecintoRecibido
        {
            get { return _Precin_vch_Recibido; }
            set { _Precin_vch_Recibido = value; }
        }
        public String sPrecintoDespachado
        {
            get { return _Precin_vch_Despachado; }
            set { _Precin_vch_Despachado = value; }
        }

        public int iIdDocOriDet
        {
            get { return _IdDocOriDet; }
            set { _IdDocOriDet = value; }
        }

        public int iIdDocOri
        {
            get { return _IdDocOri; }
            set { _IdDocOri = value; }
        }

        public int iIdNaveViaje
        {
            get { return _IdNaveViaje; }
            set { _IdNaveViaje = value; }
        }

        public Decimal dPesoNetoTotal
        {
            get { return _vi_StoCon_dec_PesoNetoTotal; }
            set { _vi_StoCon_dec_PesoNetoTotal = value; }
        }


        public Decimal dPesoBrutoTotal
        {
            get { return _vi_StoCon_dec_PesoBrutoTotal; }
            set { _vi_StoCon_dec_PesoBrutoTotal = value; }
        }


        public Decimal dPayload
        {
            get { return _StoCon_dec_Payload; }
            set { _StoCon_dec_Payload = value; }
        }

        public Decimal dBultosTotal
        {
            get { return _StoCon_dec_BultosTotal; }
            set { _StoCon_dec_BultosTotal = value; }
        }
        public String sConexion
        {
            get { return _StoCon_chr_Conexion; }
            set { _StoCon_chr_Conexion = value; }
        }

        public Decimal dTemperatura
        {
            get { return _StoCon_dec_Temperatura; }
            set { _StoCon_dec_Temperatura = value; }
        }

        public int iIdContenedor
        {
            get { return _IdContenedor; }
            set { _IdContenedor = value; }
        }
        public int iIdLineaNegocio
        {
            get { return _IdLineaNegocio; }
            set { _IdLineaNegocio = value; }
        }
        public int iIdLineaMaritima
        {
            get { return _IdLineaMaritima; }
            set { _IdLineaMaritima = value; }
        }

        public int? iIdTipoContenedor
        {
            get { return _IdTipoContenedor; }
            set { _IdTipoContenedor = value; }
        }

        public int? iIdDimension
        {
            get { return _IdDimension; }
            set { _IdDimension = value; }
        }

        public String sContenedor
        {
            get { return _Vch_Contenedor; }
            set { _Vch_Contenedor = value; }
        }

        public Decimal nTara
        {
            get { return _Dec_Tara; }
            set { _Dec_Tara = value; }
        }

        public Decimal nCargaMaxima
        {
            get { return _Dec_CargaMaxima; }
            set { _Dec_CargaMaxima = value; }
        }

        public String sIso
        {
            get { return _Vch_Iso; }
            set { _Vch_Iso = value; }
        }

        public Boolean bShipperOwn
        {
            get { return _Bln_ShipperOwn; }
            set { _Bln_ShipperOwn = value; }
        }


        public String sDesLineaMaritima
        {
            get { return _Vch_DesLineaMaritima; }
            set { _Vch_DesLineaMaritima = value; }
        }

        public String sDesTipoContenedor
        {
            get { return _Vch_DesTipoContenedor; }
            set { _Vch_DesTipoContenedor = value; }
        }

        public String sDesDimension
        {
            get { return _Vch_DesDimension; }
            set { _Vch_DesDimension = value; }
        }

        #endregion
    }

    #region Metodos
    [Serializable]
    public class BE_ContenedorList : List<BE_Contenedor>
    {
        public void Ordenar(string propertyName, direccionOrden Direction)
        {
            BE_ContenedorComparer dc = new BE_ContenedorComparer(propertyName, Direction);
            this.Sort(dc);
        }
    }

    class BE_ContenedorComparer : IComparer<BE_Contenedor>
    {
        string _prop = "";
        direccionOrden _dir;

        public BE_ContenedorComparer(string propertyName, direccionOrden Direction)
        {
            _prop = propertyName;
            _dir = Direction;
        }

        public int Compare(BE_Contenedor x, BE_Contenedor y)
        {

            PropertyInfo propertyX = x.GetType().GetProperty(_prop);
            PropertyInfo propertyY = y.GetType().GetProperty(_prop);

            object px = propertyX.GetValue(x, null);
            object py = propertyY.GetValue(y, null);

            if (px == null && py == null)
            {
                return 0;
            }
            else if (px != null && py == null)
            {
                if (_dir == direccionOrden.Ascending)
                {
                    return 1;
                }
                else
                {
                    return -1;
                }
            }
            else if (px == null && py != null)
            {
                if (_dir == direccionOrden.Ascending)
                {
                    return -1;
                }
                else
                {
                    return 1;
                }
            }
            else if (px.GetType().GetInterface("IComparable") != null)
            {
                if (_dir == direccionOrden.Ascending)
                {
                    return ((IComparable)px).CompareTo(py);
                }
                else
                {
                    return ((IComparable)py).CompareTo(px);
                }
            }
            else
            {
                return 0;
            }
        }



    }

    #endregion
}
