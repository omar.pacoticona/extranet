﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FENIX.Common;
using System.Data.SqlTypes;
using System.Reflection;

namespace FENIX.BusinessEntity
{
    [Serializable]
    public class BE_eOrdenServicio
    {       
        public Int32 iIdOrdSer { get; set; }
        public Int32 iIdClienteAduana { get; set; }
        public Int32 iIdDocOri { get; set; }
        public Int32 iIdTipoOperacion { get; set; }
        public String sNumeroDO { get; set; }
        public Int32 iIdVolante { get; set; }
        public String sNumeroVO { get; set; }
    }

    [Serializable]
    public class BE_Servicio
    {
        public Int32 iIdServicio { get; set; }
        public String sServicio { get; set; }
    }

    [Serializable]
    public class BE_Operacion
    {
        public Int32 iIdTipoOperacion { get; set; }
        public String sTipoOperacion { get; set; }
    }

    [Serializable]
    public class BE_Detalle : Paginador
    {
        public Int32 iIdDocOri { get; set; }
        public String sNumeroDO { get; set; }
        public Int32 iItem { get; set; }
        public Int32 iIdDocOriDet { get; set; }
        public String sCarga { get; set; }
        public String sTamano { get; set; }
        public String sTipo { get; set; }
        public String sCondicFacturar { get; set; }
        public Decimal dBultos { get; set; }
        public Decimal dPeso { get; set; }
        public Int32 iVolante { get; set; }
    }

    [Serializable]
    public class BE_DetalleLis : List<BE_Detalle>
    {
        public void Ordenar(string propertyName, direccionOrden Direction)
        {
            BE_DetalleComparer dc = new BE_DetalleComparer(propertyName, Direction);
            this.Sort(dc);
        }
    }

    class BE_DetalleComparer : IComparer<BE_Detalle>
    {
        string _prop = "";
        direccionOrden _dir;

        public BE_DetalleComparer(string propertyName, direccionOrden Direction)
        {
            _prop = propertyName;
            _dir = Direction;
        }

        public int Compare(BE_Detalle x, BE_Detalle y)
        {

            PropertyInfo propertyX = x.GetType().GetProperty(_prop);
            PropertyInfo propertyY = y.GetType().GetProperty(_prop);

            object px = propertyX.GetValue(x, null);
            object py = propertyY.GetValue(y, null);

            if (px == null && py == null)
            {
                return 0;
            }
            else if (px != null && py == null)
            {
                if (_dir == direccionOrden.Ascending)
                {
                    return 1;
                }
                else
                {
                    return -1;
                }
            }
            else if (px == null && py != null)
            {
                if (_dir == direccionOrden.Ascending)
                {
                    return -1;
                }
                else
                {
                    return 1;
                }
            }
            else if (px.GetType().GetInterface("IComparable") != null)
            {
                if (_dir == direccionOrden.Ascending)
                {
                    return ((IComparable)px).CompareTo(py);
                }
                else
                {
                    return ((IComparable)py).CompareTo(px);
                }
            }
            else
            {
                return 0;
            }
        }
    }

    [Serializable]
    public class BE_ServicioAdicional
    {
        public Int32 iIdSuperNegocio { get; set; }
        public Int32 iIdOficina { get; set; }
        public Int32 iIdServicioOficinaLineaNegocio { get; set; }
        public String sServicio { get; set; }
        public String sEstado { get; set; }
        public Int32 iIdTipoServicio { get; set; }
        public String sTipoServicio { get; set; }
        public String sMandatorio { get; set; }
        public Int32 iIdCondicionCarga { get; set; }
        public Int32 iIdTipoOperacion { get; set; }
        public Int32 iIdZonaServicio { get; set; }
        public String sCodigoDetraccion { get; set; }
    }

    [Serializable]
    public class BE_ServicioAdicionalLis : List<BE_ServicioAdicional>
    {
        public void Ordenar(string propertyName, direccionOrden Direction)
        {
            BE_ServicioAdicionalComparer dc = new BE_ServicioAdicionalComparer(propertyName, Direction);
            this.Sort(dc);
        }
    }

    class BE_ServicioAdicionalComparer : IComparer<BE_ServicioAdicional>
    {
        string _prop = "";
        direccionOrden _dir;

        public BE_ServicioAdicionalComparer(string propertyName, direccionOrden Direction)
        {
            _prop = propertyName;
            _dir = Direction;
        }

        public int Compare(BE_ServicioAdicional x, BE_ServicioAdicional y)
        {

            PropertyInfo propertyX = x.GetType().GetProperty(_prop);
            PropertyInfo propertyY = y.GetType().GetProperty(_prop);

            object px = propertyX.GetValue(x, null);
            object py = propertyY.GetValue(y, null);

            if (px == null && py == null)
            {
                return 0;
            }
            else if (px != null && py == null)
            {
                if (_dir == direccionOrden.Ascending)
                {
                    return 1;
                }
                else
                {
                    return -1;
                }
            }
            else if (px == null && py != null)
            {
                if (_dir == direccionOrden.Ascending)
                {
                    return -1;
                }
                else
                {
                    return 1;
                }
            }
            else if (px.GetType().GetInterface("IComparable") != null)
            {
                if (_dir == direccionOrden.Ascending)
                {
                    return ((IComparable)px).CompareTo(py);
                }
                else
                {
                    return ((IComparable)py).CompareTo(px);
                }
            }
            else
            {
                return 0;
            }
        }
    }



}
