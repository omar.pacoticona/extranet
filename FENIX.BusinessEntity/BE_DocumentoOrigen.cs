﻿using System;
using System.Collections.Generic;
using System.Text;
using FENIX.Common;
using System.Reflection;

namespace FENIX.BusinessEntity
{
    [Serializable]
    public class BE_DocumentoOrigen : Paginador
    {
        #region "Campos"
        int? _IdCliente;
        int? _IdVolante;
        int? _BultoRecib;
        int? _PesoRecib;
        int? _IdOrdSer;
        int? _IdOrdSerDet;
        int? _IdDocOri;


        String _AnoManifiesto = null;
        String _NumManifiesto = null;
        String _Manifiesto = null;
        String _DocumentoMaster = null;
        String _DocumentoHijo = null;
        String _Contenedor = null;
        String _Volante = null;
        String _Condicion = null;
        String _CondicionCarga = null;
        String _TipoCtn = null;

        String _Orden = null;
        String _Cliente = null;
        String _Servicio = null;
        String _FechaSolicitud = null;
        String _FechaProgramacion = null;
        String _Comentario = null;

        String _Nave = null;
        String _Viaje = null;
        String _Rumbo = null;
        String _FechaEmision = null;
        String _PtoEmbarque = null;
        String _PtoFinal = null;
        String _FechaLlegada = null;
        String _FechaTermino = null;
        String _Operacion = null;
        String _LineaMaritima = null;
        String _AgenciaMaritima = null;
        String _Consignatario = null;
        String _AgenciaAduana = null;
        String _AgenciaCarga = null;
        String _Consolidador = null;
        String _Notificante = null;
        String _DetalleAduana = null;
        String _FecIngresoBL = null;
        String _TerminalPortuario = null;
        String _ObservacionAduana = null;
        String _EstadoTransmitido = null;
        String _Mercaderia = null;
        String _Tamano = null;
        String _FechaIngreso = null;
        String _PrecintoManifestado = null;
        String _PrecintoRecepcionado = null;
        String _TipoCarga = null;
        String _Observacion = null;
        String _UsuarioGenera = null;
        String _PesoInvLib = null;
        String _CliBloqueado = null;
        String _AgeBloqueado = null;
        String _ConsBloqueado = null;
        String _Sini = null;
        String _Reestiba = null;

        Decimal _BultoManif = 0;
        Decimal _PesoManif = 0;
        Decimal _BultoMalo = 0;
        Decimal _PesoMalo = 0;
        Decimal _BultoFaltante = 0;
        Decimal _PesoFaltante = 0;
        Decimal _BultoSobrante = 0;
        Decimal _PesoSobrante = 0;
        Decimal _DiceContener = 0;
        Decimal _EmpaqueRecibido = 0;
        Decimal _Tara = 0;
        Decimal _BultosRecib = 0;
        Decimal _BultosRecib2 = 0;
        Decimal _PesoRecibido = 0;
        Decimal _BultosRecCS = 0;
        Decimal _PesosRecCS = 0;
        Int32 _iTasaDscto;
        Decimal _dImpDscto;
        String _sAcepta;
        int? _iIdDscto;
        Int32 _iDiasAlmacen;
        Int32 _NtotalRegistros;


        /*Descarga de Documentos Operativos 11-11-2019*/
        public Int32? iIdNaveViaje { get; set; }
        public String sNumeroDO { get; set; }
        public String sNumeroDOMadre { get; set; }
        public DateTime? dtFechaEtaInicio { get; set; }
        public DateTime? dtFechaEtaFinal { get; set; }
        public String sRazonSocial { get; set; }
        public Int32 iIdTipoOperacion { get; set; }
        public String sNroOrden { get; set; }
        public Int32 iIdTerminal { get; set; }
        public String sEmbarcador { get; set; }
        public String sSada { get; set; }
        public String sObservaciones { get; set; }
        public String sCmbCondicion { get; set; }
        public Int32 iIdClienteConsignatario { get; set; }
        public Int32 iIdClienteProcedencia { get; set; }
        public String CodigoTerminal { get; set; }

        public int iIdNavVia { get; set; }
        public int iIdOperacion { get; set; }
        public int iIdRolCliente { get; set; }
        public int iIdAgenciaAduana { get; set; }
        public int iIdDocOriDet { get; set; }
        public string sCliente_Descripcion { get; set; }
        public string sAgenciaAdu_Descripcion { get; set; }
        public string sDniCliente { get; set; }
        public string sDniAgenciaAduana { get; set; }
        public int idliquidacion { get; set; }
        public DateTime dtTerminoDescarga { get; set; }

        public string sTamanoCnt { get; set; }
        public string sTipoCnt { get; set; }
        public string sCondicionCarga { get; set; }
        public string sCarga { get; set; }
        public string sEmbalaje { get; set; }
        public decimal dCantidadAutorizado { get; set; }
        public decimal dPesoAutorizado { get; set; }
        public decimal dVolumenAutorizado { get; set; }
        public string sChasis { get; set; }
        public int? iItem { get; set; }
        //public string sNumeroDo { get; set; }
        public string sDespachado { get; set; }
        public string sConAutorizacion { get; set; }

        public string sDni { get; set; }
        public string sRucAgencia { get; set; }
        public string sRuc { get; set; }
        public string sNombreDespachador { get; set; }
        public int IdDespachador { get; set; }
        public string sRetorno { get; set; }
        public decimal dtMontoLiquidacion { get; set; }
        public string sDua { get; set; }
        public string sMonto { get; set; }
        public int idAduana { get; set; }
        public string sDuaAnio { get; set; }
        public string sRegimen { get; set; }
        public string sDuaNumero { get; set; }
        public int IdSolicitudAre { get; set; }
        public string sFechCalculo { get; set; }
        
        #endregion



        #region "Propiedades"
        public Int32 NtotalRegistros
        {
            get { return _NtotalRegistros; }
            set { _NtotalRegistros = value; }
        }

        public int? iIdCliente
        {
            get { return _IdCliente; }
            set { _IdCliente = value; }
        }
        public int? iIdVolante
        {
            get { return _IdVolante; }
            set { _IdVolante = value; }
        }
        public int? iIdDocOri
        {
            get { return _IdDocOri; }
            set { _IdDocOri = value; }
        }
        public int? dBultoRecib
        {
            get { return _BultoRecib; }
            set { _BultoRecib = value; }
        }
        public int? dPesoRecib
        {
            get { return _PesoRecib; }
            set { _PesoRecib = value; }
        }
        public int? iIdOrdSer
        {
            get { return _IdOrdSer; }
            set { _IdOrdSer = value; }
        }
        public int? iIdOrdSerDet
        {
            get { return _IdOrdSerDet; }
            set { _IdOrdSerDet = value; }
        }


        public String sAnoManifiesto
        {
            get { return _AnoManifiesto; }
            set { _AnoManifiesto = value; }
        }
        public String sNumManifiesto
        {
            get { return _NumManifiesto; }
            set { _NumManifiesto = value; }
        }
        public String sManifiesto
        {
            get { return _Manifiesto; }
            set { _Manifiesto = value; }
        }
        public String sDocumentoMaster
        {
            get { return _DocumentoMaster; }
            set { _DocumentoMaster = value; }
        }
        public String sDocumentoHijo
        {
            get { return _DocumentoHijo; }
            set { _DocumentoHijo = value; }
        }

        public String sComentario
        {
            get { return _Comentario; }
            set { _Comentario = value; }
        }
        public String sContenedor
        {
            get { return _Contenedor; }
            set { _Contenedor = value; }
        }
        public String sVolante
        {
            get { return _Volante; }
            set { _Volante = value; }
        }
        public String sOrden
        {
            get { return _Orden; }
            set { _Orden = value; }
        }
        public String sCondicion
        {
            get { return _Condicion; }
            set { _Condicion = value; }
        }

        public String sCondicCarga
        {
            get { return _CondicionCarga; }
            set { _CondicionCarga = value; }
        }
        public String sTipoCtn
        {
            get { return _TipoCtn; }
            set { _TipoCtn = value; }
        }

        public String sCliente
        {
            get { return _Cliente; }
            set { _Cliente = value; }
        }
        public String sServicio
        {
            get { return _Servicio; }
            set { _Servicio = value; }
        }
        public String sFechaSolicitud
        {
            get { return _FechaSolicitud; }
            set { _FechaSolicitud = value; }
        }
        public String sFechaProgramacion
        {
            get { return _FechaProgramacion; }
            set { _FechaProgramacion = value; }
        }

        public String sNave
        {
            get { return _Nave; }
            set { _Nave = value; }
        }
        public String sViaje
        {
            get { return _Viaje; }
            set { _Viaje = value; }
        }
        public String sRumbo
        {
            get { return _Rumbo; }
            set { _Rumbo = value; }
        }
        public String sFechaEmision
        {
            get { return _FechaEmision; }
            set { _FechaEmision = value; }
        }
        public String sPtoEmbarque
        {
            get { return _PtoEmbarque; }
            set { _PtoEmbarque = value; }
        }
        public String sPtoFinal
        {
            get { return _PtoFinal; }
            set { _PtoFinal = value; }
        }
        public String sFechaLlegada
        {
            get { return _FechaLlegada; }
            set { _FechaLlegada = value; }
        }
        public String sFechaTermino
        {
            get { return _FechaTermino; }
            set { _FechaTermino = value; }
        }
        public String sOperacion
        {
            get { return _Operacion; }
            set { _Operacion = value; }
        }
        public String sLineaMaritima
        {
            get { return _LineaMaritima; }
            set { _LineaMaritima = value; }
        }
        public String sAgenciaMaritima
        {
            get { return _AgenciaMaritima; }
            set { _AgenciaMaritima = value; }
        }
        public String sConsignatario
        {
            get { return _Consignatario; }
            set { _Consignatario = value; }
        }
        public String sAgenciaCarga
        {
            get { return _AgenciaCarga; }
            set { _AgenciaCarga = value; }
        }
        public String sAgenciaAduana
        {
            get { return _AgenciaAduana; }
            set { _AgenciaAduana = value; }
        }
        public String sConsolidador
        {
            get { return _Consolidador; }
            set { _Consolidador = value; }
        }
        public String sNotificante
        {
            get { return _Notificante; }
            set { _Notificante = value; }
        }
        public String sDetalleAduana
        {
            get { return _DetalleAduana; }
            set { _DetalleAduana = value; }
        }
        public String sFecIngresoBL
        {
            get { return _FecIngresoBL; }
            set { _FecIngresoBL = value; }
        }

        public String sObservacion
        {
            get { return _Observacion; }
            set { _Observacion = value; }
        }
        public String sTerminalPortuario
        {
            get { return _TerminalPortuario; }
            set { _TerminalPortuario = value; }
        }
        public String sUsuarioGenera
        {
            get { return _UsuarioGenera; }
            set { _UsuarioGenera = value; }
        }
        public String sObservacionAduana
        {
            get { return _ObservacionAduana; }
            set { _ObservacionAduana = value; }
        }
        public String sEstadoTransmitido
        {
            get { return _EstadoTransmitido; }
            set { _EstadoTransmitido = value; }
        }
        public String sMercaderia
        {
            get { return _Mercaderia; }
            set { _Mercaderia = value; }
        }
        public String sTamano
        {
            get { return _Tamano; }
            set { _Tamano = value; }
        }
        public String sFechaIngreso
        {
            get { return _FechaIngreso; }
            set { _FechaIngreso = value; }
        }
        public String sPrecintoManifestado
        {
            get { return _PrecintoManifestado; }
            set { _PrecintoManifestado = value; }
        }
        public String sPrecintoRecepcionado
        {
            get { return _PrecintoRecepcionado; }
            set { _PrecintoRecepcionado = value; }
        }
        public String sTipoCarga
        {
            get { return _TipoCarga; }
            set { _TipoCarga = value; }
        }

        public String sPesoInvLib
        {
            get { return _PesoInvLib; }
            set { _PesoInvLib = value; }
        }

        public string sObservacionEir { get; set; }

        public String sCliBloqueado
        {
            get { return _CliBloqueado; }
            set { _CliBloqueado = value; }
        }
        public String sAgeBloqueado
        {
            get { return _AgeBloqueado; }
            set { _AgeBloqueado = value; }
        }
        public String sConsBloqueado
        {
            get { return _ConsBloqueado; }
            set { _ConsBloqueado = value; }
        }
        public String sSini
        {
            get { return _Sini; }
            set { _Sini = value; }
        }
        public String sReestiba
        {
            get { return _Reestiba; }
            set { _Reestiba = value; }
        }

        public Decimal dTara
        {
            get { return _Tara; }
            set { _Tara = value; }
        }
        public Decimal dBultosRecib
        {
            get { return _BultosRecib; }
            set { _BultosRecib = value; }
        }
        public Decimal dBultoRecib2
        {
            get { return _BultosRecib2; }
            set { _BultosRecib2 = value; }
        }
        public Decimal dPesoRecibido
        {
            get { return _PesoRecibido; }
            set { _PesoRecibido = value; }
        }
        public Decimal dBultoManif
        {
            get { return _BultoManif; }
            set { _BultoManif = value; }
        }
        public Decimal dPesoManif
        {
            get { return _PesoManif; }
            set { _PesoManif = value; }
        }
        public Decimal dBultoMalo
        {
            get { return _BultoMalo; }
            set { _BultoMalo = value; }
        }
        public Decimal dPesoMalo
        {
            get { return _PesoMalo; }
            set { _PesoMalo = value; }
        }
        public Decimal dBultoFaltante
        {
            get { return _BultoFaltante; }
            set { _BultoFaltante = value; }
        }
        public Decimal dPesoFaltante
        {
            get { return _PesoFaltante; }
            set { _PesoFaltante = value; }
        }
        public Decimal dBultoSobrante
        {
            get { return _BultoSobrante; }
            set { _BultoSobrante = value; }
        }
        public Decimal dPesoSobrante
        {
            get { return _PesoSobrante; }
            set { _PesoSobrante = value; }
        }
        public Decimal dDiceContener
        {
            get { return _DiceContener; }
            set { _DiceContener = value; }
        }
        public Decimal dEmpaqueRecibido
        {
            get { return _EmpaqueRecibido; }
            set { _EmpaqueRecibido = value; }
        }
        public Decimal dBultosRecCs
        {
            get { return _BultosRecCS; }
            set { _BultosRecCS = value; }
        }
        public Decimal dPesosRecCs
        {
            get { return _PesosRecCS; }
            set { _PesosRecCS = value; }
        }

        public Int32 iTasaDscto
        {
            get { return _iTasaDscto; }
            set { _iTasaDscto = value; }
        }

        public Decimal dImpDscto
        {
            get { return _dImpDscto; }
            set { _dImpDscto = value; }
        }

        public String sAcepta
        {
            get { return _sAcepta; }
            set { _sAcepta = value; }
        }

        public int? iIdDscto
        {
            get { return _iIdDscto; }
            set { _iIdDscto = value; }
        }

        public Int32 iDiasAlmacen
        {
            get { return _iDiasAlmacen; }
            set { _iDiasAlmacen = value; }
        }

        #endregion
    }
}
