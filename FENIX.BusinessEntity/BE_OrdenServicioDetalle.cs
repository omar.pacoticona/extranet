﻿using System;
using System.Collections.Generic;
using System.Text;
using FENIX.Common;
using System.Reflection;

namespace FENIX.BusinessEntity
{
    [Serializable]
    public class BE_OrdenServicioDetalle : Paginador
    {
        int _IdDocOriDet = 0;
        int _IdOrdSerDet = 0;
        int _IdCondicionCargaFacturar = 0;
        int _IdLiquidacion = 0;
        int _IdServicio = 0;
        int _IdDocOri = 0;
        int _IdOrdSer = 0;
        int _IdSituacion = 0;
        int _IdCargaRecibida = 0;
        int _IdEmbalajeRecibido = 0;
        int _IdTamanoCnt = 0;
        int _IdTipoCnt = 0;
        int _IdStoCon = 0;
        int _IdTipoOrdSerDet = 0;
        int _IdTipoServicio = 0;
        int _IdModalidad = 0;
        int _iMontoDinero = 0;
        int _iTipoOperacion = 0;
        int _iItem = 0;
        int _iIdTipoOperacion;
        int _IdCliente = 0;
        int _IdSocio = 0;
        int _IdServicioOficinaLineaNegocio = 0;
        int _IdOrigen = 0;

        string _DescripcionTipoCnt = string.Empty;
        string _Valorizado = string.Empty;
        string _Situacion = string.Empty;
        string _TamanoCnt = string.Empty;
        string _CondicionFacturar = string.Empty;
        string _Ser_Vch_Descripcion = string.Empty;
        string _OrdSerDet_vch_NumeroDO = string.Empty;
        string _OrdSerDet_vch_Contenedor = string.Empty;
        decimal _OrdSerDet_Num_CantidadSolicitado = 0;
        decimal _OrdSerDet_Num_PesoSolicitado = 0;
        decimal _OrdSerDet_Num_VolumenSolicitado = 0;
        decimal _OrdSerDet_Int_HoraSolicitado = 0;
        decimal _OrdSerDet_Num_CantidadAtendido = 0;
        decimal _OrdSerDet_Num_PesoAtendido = 0;
        decimal _OrdSerDet_Num_VolumenAtentido = 0;
        decimal _OrdSerDet_Int_HoraAtendido = 0;
        decimal _dMonto = 0;
        decimal _dDescuento = 0;
        decimal _dBultosRecibidos = 0;
        decimal _dVolumenRecibido = 0;
        decimal _dPesoRecibido = 0;
        string _Servicio = string.Empty;
        string _Observacion = string.Empty;
        string _Embalaje = string.Empty;
        string _Carga = string.Empty;
        string _OrdSerDet_Dt_FechaAtencion = string.Empty;
        string _OrdSerDet_Chr_Automatico = string.Empty;
        string _OrdSerDet_TipoOrdDescripcion = string.Empty;
        string _sFechaRegistro = string.Empty;
        string _sIdDocOriDet = string.Empty;

        string _Cliente = string.Empty;
        String _Socio = String.Empty;
        string _UsuarioRegistro = string.Empty;
        string _FechaCrea = string.Empty;
        String _EstaDespachado = String.Empty;

        String _sCondCarga = String.Empty;
        String _sChasis = String.Empty;
        String _sConsignatario = String.Empty;
        String _sNroOrden = String.Empty;
        DateTime? _dFechaIngreso;
        DateTime? _dFechaRetiro;
        int _iDiasAlmacenaje = 0;
        DateTime? _dFechaVencimiento;
        String _sNroAutorizacion = String.Empty;
        int _iIdMoneda = 0;
        string _sMoneda = string.Empty;

        String _Origen = String.Empty;
        String _HoraFinal = String.Empty;
        String _HoraInicio = String.Empty;
        String _sDescripcion = String.Empty;
        int _iCodigo = 0;


        public String sDescripcion
        {
            get { return _sDescripcion; }
            set { _sDescripcion = value; }
        }
        public int iCodigo
        {
            get { return _iCodigo; }
            set { _iCodigo = value; }
        }

        public String sNroAutorizacion
        {
            get { return _sNroAutorizacion; }
            set { _sNroAutorizacion = value; }
        }

        public String sCondCarga
        {
            get { return _sCondCarga; }
            set { _sCondCarga = value; }
        }


        public String sChasis
        {
            get { return _sChasis; }
            set { _sChasis = value; }
        }

        public String sConsignatario
        {
            get { return _sConsignatario; }
            set { _sConsignatario = value; }
        }

        public String sNroOrden
        {
            get { return _sNroOrden; }
            set { _sNroOrden = value; }
        }

        public DateTime? dtFechaIngreso
        {
            get { return _dFechaIngreso; }
            set { _dFechaIngreso = value; }
        }

        public DateTime? dtFechaRetiro
        {
            get { return _dFechaRetiro; }
            set { _dFechaRetiro = value; }
        }

        public int iDiasAlmacenaje
        {
            get { return _iDiasAlmacenaje; }
            set { _iDiasAlmacenaje = value; }
        }

        public DateTime? dtFechaVencimiento
        {
            get { return _dFechaVencimiento; }
            set { _dFechaVencimiento = value; }
        }

        public int iItem
        {
            get { return _iItem; }
            set { _iItem = value; }
        }

        public int iTipoOperacion
        {
            get { return _iTipoOperacion; }
            set { _iTipoOperacion = value; }
        }

        public int iIdTipoServicio
        {
            get { return _IdTipoServicio; }
            set { _IdTipoServicio = value; }
        }

        public int iIdModalidad
        {
            get { return _IdModalidad; }
            set { _IdModalidad = value; }
        }

        public string sIdDocOriDet
        {
            get { return _sIdDocOriDet; }
            set { _sIdDocOriDet = value; }
        }

        public string sFechaRegistro
        {
            get { return _sFechaRegistro; }
            set { _sFechaRegistro = value; }
        }

        public string sDescripcionTipoOrd
        {
            get { return _OrdSerDet_TipoOrdDescripcion; }
            set { _OrdSerDet_TipoOrdDescripcion = value; }
        }

        public int iIdTipoOrdSerDet
        {
            get { return _IdTipoOrdSerDet; }
            set { _IdTipoOrdSerDet = value; }
        }

        public string sValorizado
        {
            get { return _Valorizado; }
            set { _Valorizado = value; }
        }

        public string sObservacion
        {
            get { return _Observacion; }
            set { _Observacion = value; }
        }

        public string sCliente
        {
            get { return _Cliente; }
            set { _Cliente = value; }
        }

        public string sSocio
        {
            get { return _Socio; }
            set { _Socio = value; }
        }

        public string sUsuarioRegistro
        {
            get { return _UsuarioRegistro; }
            set { _UsuarioRegistro = value; }
        }

        public string sFechaCrea
        {
            get { return _FechaCrea; }
            set { _FechaCrea = value; }
        }




        public decimal dPesoRecibido
        {
            get { return _dPesoRecibido; }
            set { _dPesoRecibido = value; }
        }


        public decimal dVolumenRecibido
        {
            get { return _dVolumenRecibido; }
            set { _dVolumenRecibido = value; }
        }

        public string sDescripcionServicio
        {
            get { return _Servicio; }
            set { _Servicio = value; }
        }


        public decimal dBultosRecibidos
        {
            get { return _dBultosRecibidos; }
            set { _dBultosRecibidos = value; }
        }

        public String sEstaDespachado
        {
            get { return _EstaDespachado; }
            set { _EstaDespachado = value; }
        }

        public string sDescripcionCarga
        {
            get { return _Carga; }
            set { _Carga = value; }
        }

        public int iIdStoCon
        {
            get { return _IdStoCon; }
            set { _IdStoCon = value; }
        }
        public int iIdCliente
        {
            get { return _IdCliente; }
            set { _IdCliente = value; }
        }
        public int iIdSocio
        {
            get { return _IdSocio; }
            set { _IdSocio = value; }
        }

        public int iIdTipoCnt
        {
            get { return _IdTipoCnt; }
            set { _IdTipoCnt = value; }
        }


        public int iIdTamanoCnt
        {
            get { return _IdTamanoCnt; }
            set { _IdTamanoCnt = value; }
        }

        public int iIdEmbalajeRecibido
        {
            get { return _IdEmbalajeRecibido; }
            set { _IdEmbalajeRecibido = value; }
        }

        public int iIdOrigen
        {
            get { return _IdOrigen; }
            set { _IdOrigen = value; }
        }

        public string sDescripcionEmbalaje
        {
            get { return _Embalaje; }
            set { _Embalaje = value; }
        }


        public int iIdCargaRecibida
        {
            get { return _IdCargaRecibida; }
            set { _IdCargaRecibida = value; }
        }


        public decimal dDescuento
        {
            get { return _dDescuento; }
            set { _dDescuento = value; }
        }

        public decimal dMonto
        {
            get { return _dMonto; }
            set { _dMonto = value; }
        }

        public int iIdSituacion
        {
            get { return _IdSituacion; }
            set { _IdSituacion = value; }
        }

        public int iIdServicioOficinaLineaNegocio
        {
            get { return _IdServicioOficinaLineaNegocio; }
            set { _IdServicioOficinaLineaNegocio = value; }
        }
        public int iIdOrdSer
        {
            get { return _IdOrdSer; }
            set { _IdOrdSer = value; }
        }

        public int iIdDocOri
        {
            get { return _IdDocOri; }
            set { _IdDocOri = value; }
        }


        public int iIdOrdSerDet
        {
            get { return _IdOrdSerDet; }
            set { _IdOrdSerDet = value; }
        }

        public int iIdMoneda
        {
            get { return _iIdMoneda; }
            set { _iIdMoneda = value; }
        }

        public int iMontoDinero
        {
            get { return _iMontoDinero; }
            set { _iMontoDinero = value; }
        }

        public string sAutomatico
        {
            get { return _OrdSerDet_Chr_Automatico; }
            set { _OrdSerDet_Chr_Automatico = value; }
        }

        public string sFechaAtencion
        {
            get { return _OrdSerDet_Dt_FechaAtencion; }
            set { _OrdSerDet_Dt_FechaAtencion = value; }
        }

        public string sHoraInicio
        {
            get { return _HoraInicio; }
            set { _HoraInicio = value; }
        }

        public string sHoraFinal
        {
            get { return _HoraFinal; }
            set { _HoraFinal = value; }
        }

        public decimal dHoraAtendido
        {
            get { return _OrdSerDet_Int_HoraAtendido; }
            set { _OrdSerDet_Int_HoraAtendido = value; }
        }

        public decimal dVolumenAtentido
        {
            get { return _OrdSerDet_Num_VolumenAtentido; }
            set { _OrdSerDet_Num_VolumenAtentido = value; }
        }

        public decimal dPesoAtendido
        {
            get { return _OrdSerDet_Num_PesoAtendido; }
            set { _OrdSerDet_Num_PesoAtendido = value; }
        }
        public decimal dCantidadAtendido
        {
            get { return _OrdSerDet_Num_CantidadAtendido; }
            set { _OrdSerDet_Num_CantidadAtendido = value; }
        }

        public decimal dHoraSolicitado
        {
            get { return _OrdSerDet_Int_HoraSolicitado; }
            set { _OrdSerDet_Int_HoraSolicitado = value; }
        }
        public decimal dVolumenSolicitado
        {
            get { return _OrdSerDet_Num_VolumenSolicitado; }
            set { _OrdSerDet_Num_VolumenSolicitado = value; }
        }

        public decimal dPesoSolicitado
        {
            get { return _OrdSerDet_Num_PesoSolicitado; }
            set { _OrdSerDet_Num_PesoSolicitado = value; }
        }


        public decimal dCantidadSolicitado
        {
            get { return _OrdSerDet_Num_CantidadSolicitado; }
            set { _OrdSerDet_Num_CantidadSolicitado = value; }
        }


        public string sContenedor
        {
            get { return _OrdSerDet_vch_Contenedor; }
            set { _OrdSerDet_vch_Contenedor = value; }
        }

        public int iIdTipoOperacion
        {
            get { return _iIdTipoOperacion; }
            set { _iIdTipoOperacion = value; }
        }

        public string sNumeroDO
        {
            get { return _OrdSerDet_vch_NumeroDO; }
            set { _OrdSerDet_vch_NumeroDO = value; }
        }

        public string sDescripcionCondicionFacturar
        {
            get { return _CondicionFacturar; }
            set { _CondicionFacturar = value; }
        }

        public string sDescripcionTamanoCnt
        {
            get { return _TamanoCnt; }
            set { _TamanoCnt = value; }
        }

        public string sOrigen
        {
            get { return _Origen; }
            set { _Origen = value; }
        }

        public string sDescripcionSituacion
        {
            get { return _Situacion; }
            set { _Situacion = value; }
        }

        public int iIdServicio
        {
            get { return _IdServicio; }
            set { _IdServicio = value; }
        }

        public int iIdLiquidacion
        {
            get { return _IdLiquidacion; }
            set { _IdLiquidacion = value; }
        }

        public string sDescripcionTipoCnt
        {
            get { return _DescripcionTipoCnt; }
            set { _DescripcionTipoCnt = value; }
        }


        public int iIdCondicionCargaFacturar
        {
            get { return _IdCondicionCargaFacturar; }
            set { _IdCondicionCargaFacturar = value; }
        }

        public int iIdDocOriDet
        {
            get { return _IdDocOriDet; }
            set { _IdDocOriDet = value; }
        }

        public string sMoneda
        {
            get { return _sMoneda; }
            set { _sMoneda = value; }
        }
    }
    [Serializable]
    public class BE_OrdenServicioDetalleLis : List<BE_OrdenServicioDetalle>
    {
        public void Ordenar(string propertyName, direccionOrden Direction)
        {
            BE_OrdenServicioDetalleComparer dc = new BE_OrdenServicioDetalleComparer(propertyName, Direction);
            this.Sort(dc);
        }
    }

    class BE_OrdenServicioDetalleComparer : IComparer<BE_OrdenServicioDetalle>
    {
        string _prop = "";
        direccionOrden _dir;

        public BE_OrdenServicioDetalleComparer(string propertyName, direccionOrden Direction)
        {
            _prop = propertyName;
            _dir = Direction;
        }

        public int Compare(BE_OrdenServicioDetalle x, BE_OrdenServicioDetalle y)
        {

            PropertyInfo propertyX = x.GetType().GetProperty(_prop);
            PropertyInfo propertyY = y.GetType().GetProperty(_prop);

            object px = propertyX.GetValue(x, null);
            object py = propertyY.GetValue(y, null);

            if (px == null && py == null)
            {
                return 0;
            }
            else if (px != null && py == null)
            {
                if (_dir == direccionOrden.Ascending)
                {
                    return 1;
                }
                else
                {
                    return -1;
                }
            }
            else if (px == null && py != null)
            {
                if (_dir == direccionOrden.Ascending)
                {
                    return -1;
                }
                else
                {
                    return 1;
                }
            }
            else if (px.GetType().GetInterface("IComparable") != null)
            {
                if (_dir == direccionOrden.Ascending)
                {
                    return ((IComparable)px).CompareTo(py);
                }
                else
                {
                    return ((IComparable)py).CompareTo(px);
                }
            }
            else
            {
                return 0;
            }
        }





    }

}
