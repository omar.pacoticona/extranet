﻿using System;
using System.Collections.Generic;
using System.Text;
using FENIX.Common;

namespace FENIX.BusinessEntity
{
    public class BE_NaveViaje : Paginador
    {
        #region "Campos"

        int _IdNaveViaje;
        int? _IdVia;
        int _IdNave;
        int? _IdRumbo;
        int _IdAgenciaMaritima;
        int _IdSituacion;
        int _int_ContManif;
        int _int_BultosManif;

        String _Vch_NumeroViaje;
        String _Vch_AnoManifiesto;
        String _Vch_NumManifiesto;
        String _Vch_FechaEstimada;
        String _Vch_FechaArribo;
        String _Vch_FechaZarpe;
        String _Vch_FechaTerminoDescarga;
        String _Vch_FechaInicioEmbarque;
        String _Vch_Operacion;

        String _Vch_DesVia;
        String _Vch_DesNave;
        String _Vch_DesRumbo;
        String _Vch_DesAgenciaMaritima;

        DateTime? _FechaDesde;
        DateTime? _FechaHasta;
        String _Vch_DesSituacion;

        #endregion


        #region "Propiedades"

        public int iIdNaveViaje
        {
            get { return _IdNaveViaje; }
            set { _IdNaveViaje = value; }
        }

        public int? iIdVia
        {
            get { return _IdVia; }
            set { _IdVia = value; }
        }

        public int iIdNave
        {
            get { return _IdNave; }
            set { _IdNave = value; }
        }

        public int? iIdRumbo
        {
            get { return _IdRumbo; }
            set { _IdRumbo = value; }
        }

        public int iIdAgenciaMaritima
        {
            get { return _IdAgenciaMaritima; }
            set { _IdAgenciaMaritima = value; }
        }

        public int iIdSituacion
        {
            get { return _IdSituacion; }
            set { _IdSituacion = value; }
        }

        public String sNumeroViaje
        {
            get { return _Vch_NumeroViaje; }
            set { _Vch_NumeroViaje = value; }
        }

        public String sAnoManifiesto
        {
            get { return _Vch_AnoManifiesto; }
            set { _Vch_AnoManifiesto = value; }
        }

        public String sNumManifiesto
        {
            get { return _Vch_NumManifiesto; }
            set { _Vch_NumManifiesto = value; }
        }

        public String sFechaEstimada
        {
            get { return _Vch_FechaEstimada; }
            set { _Vch_FechaEstimada = value; }
        }

        public String sFechaArribo
        {
            get { return _Vch_FechaArribo; }
            set { _Vch_FechaArribo = value; }
        }

        public String sFechaZarpe
        {
            get { return _Vch_FechaZarpe; }
            set { _Vch_FechaZarpe = value; }
        }

        public String sFechaTerminoDescarga
        {
            get { return _Vch_FechaTerminoDescarga; }
            set { _Vch_FechaTerminoDescarga = value; }
        }

        public String sFechaInicioEmbarque
        {
            get { return _Vch_FechaInicioEmbarque; }
            set { _Vch_FechaInicioEmbarque = value; }
        }

        public String sOperacion
        {
            get { return _Vch_Operacion; }
            set { _Vch_Operacion = value; }
        }

        public String sDesVia
        {
            get { return _Vch_DesVia; }
            set { _Vch_DesVia = value; }
        }

        public String sDesNave
        {
            get { return _Vch_DesNave; }
            set { _Vch_DesNave = value; }
        }

        public String sDesRumbo
        {
            get { return _Vch_DesRumbo; }
            set { _Vch_DesRumbo = value; }
        }

        public String sDesAgenciaMaritima
        {
            get { return _Vch_DesAgenciaMaritima; }
            set { _Vch_DesAgenciaMaritima = value; }
        }

        public DateTime? dtFechaDesde
        {
            get { return _FechaDesde; }
            set { _FechaDesde = value; }
        }

        public DateTime? dtFechaHasta
        {
            get { return _FechaHasta; }
            set { _FechaHasta = value; }
        }

        public String sDesSituacion
        {
            get { return _Vch_DesSituacion; }
            set { _Vch_DesSituacion = value; }
        }

        public int iContManif
        {
            get { return _int_ContManif; }
            set { _int_ContManif = value; }
        }

        public int iBultosManif
        {
            get { return _int_BultosManif; }
            set { _int_BultosManif = value; }
        }
        #endregion
    }
}
