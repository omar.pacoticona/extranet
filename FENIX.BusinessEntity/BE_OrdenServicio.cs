﻿using System;
using System.Collections.Generic;
using System.Text;
using FENIX.Common;
using System.Data.SqlTypes;
using System.Reflection;

namespace FENIX.BusinessEntity
{
    [Serializable]
    public class BE_OrdenServicio : Paginador
    {
        int? _IdOrdSer;
        int? _IdOrdSerDet;

        int? _IdRolCliente;
        int? _IdCliente;
        int? _IdRolSocio;
        int? _IdSocio;
        int? _IdTipoSolicitud;
        int? _IdLineaNegocio;
        int? _IdSituacion;
        int? _IdDocOri;
        int? _IdDocOriDet;
        int? _IdServicio;
        int? _IdMovbal;
        int? _IdOrigen;
        int? _IdTipoOperacion;

        String _sIdOrdSer = null;
        string _OrdSer_chr_TipoRegistro = null;
        string _OrdSer_dt_Fecha = null;
        string _OrdSer_dt_FechaProg = null;
        string _OrdSer_vch_Solicitante = null;
        string _OrdSer_vch_Observacion = null;
        string _OrdSer_chr_Estado;

        string _DescripcionRolCliente = null;
        string _DescripcionCliente = null;
        string _DescripcionSocio = null;
        string _DescripcionRolSocio = null;
        string _DescripcionTipoCreacion = null;
        string _DescripcionSituacion = null;
        string _OrdSerDet_vch_NumeroDO = null;
        string _OrdSerDet_vch_Contenedor = null;
        string _sFechaInicio = null;
        string _sFechaFinal = null;
        SqlDateTime? _dtFechaInicio = null;
        SqlDateTime? _dtFechaFinal = null;

        String _TipoServicio;
        String _CndCarga;
        Int32? _Tamano;
        String _Tipo;
        String _ChasisBulto;
        String _AgenciaAduana;
        String _Consignatario;
        String _Manifiesto;
        Int32? _NroVolante;
        String _sNroVolante;
        String _Nave;
        String _Mercaderia;
        String _StrIdCndCarga;
        String _StrIdTipoServicio;
        DateTime? _FechaProgramacion;
        String _sCliDni;
        String _sSocDni;
        String _DocumentoBuscar;

        String _Automatico;
        String _Volante;
        String _sPlaca;
        String _sReferencia;
        String _sTicket;
        String _sFechaServicio;
        String _sFechaBalanza;
        String _sServicio;
        String _sChofer;
        String _sTransportista;
        String _sIdOrdSerDet;

        decimal _dImporte = 0;



        public string sSocDni
        {
            get { return _sSocDni; }
            set { _sSocDni = value; }
        }

        public string sCliDni
        {
            get { return _sCliDni; }
            set { _sCliDni = value; }
        }
        public string sIdOrdSer
        {
            get { return _sIdOrdSer; }
            set { _sIdOrdSer = value; }
        }

        public string sPlaca
        {
            get { return _sPlaca; }
            set { _sPlaca = value; }
        }

        public string sReferencia
        {
            get { return _sReferencia; }
            set { _sReferencia = value; }
        }

        public string sIdOrdSerDet
        {
            get { return _sIdOrdSerDet; }
            set { _sIdOrdSerDet = value; }
        }

        public string sTicket
        {
            get { return _sTicket; }
            set { _sTicket = value; }
        }

        public string sFechaServicio
        {
            get { return _sFechaServicio; }
            set { _sFechaServicio = value; }
        }

        public string sFechaBalanza
        {
            get { return _sFechaBalanza; }
            set { _sFechaBalanza = value; }
        }

        public string sServicio
        {
            get { return _sServicio; }
            set { _sServicio = value; }
        }

        public string sChofer
        {
            get { return _sChofer; }
            set { _sChofer = value; }
        }

        public string sTransportista
        {
            get { return _sTransportista; }
            set { _sTransportista = value; }
        }

        public SqlDateTime? dtFechaFinal
        {
            get { return _dtFechaFinal; }
            set { _dtFechaFinal = value; }
        }

        public SqlDateTime? dtFechaInicio
        {
            get { return _dtFechaInicio; }
            set { _dtFechaInicio = value; }
        }

        public int? _iIdServicio
        {
            get { return _IdServicio; }
            set { _IdServicio = value; }
        }

        public int? iIdDocOriDet
        {
            get { return _IdDocOriDet; }
            set { _IdDocOriDet = value; }
        }

        public decimal dImporte
        {
            get { return _dImporte; }
            set { _dImporte = value; }
        }

        public string sDescripcionRolSocio
        {
            get { return _DescripcionRolSocio; }
            set { _DescripcionRolSocio = value; }
        }

        public string sFechaFinal
        {
            get { return _sFechaFinal; }
            set { _sFechaFinal = value; }
        }

        public string sFechaInicio
        {
            get { return _sFechaInicio; }
            set { _sFechaInicio = value; }
        }

        public string sContenedor
        {
            get { return _OrdSerDet_vch_Contenedor; }
            set { _OrdSerDet_vch_Contenedor = value; }
        }

        public string sNumeroDO
        {
            get { return _OrdSerDet_vch_NumeroDO; }
            set { _OrdSerDet_vch_NumeroDO = value; }
        }

        public string sDocumentoBuscar
        {
            get { return _DocumentoBuscar; }
            set { _DocumentoBuscar = value; }
        }

        public string sVolante
        {
            get { return _Volante; }
            set { _Volante = value; }
        }

        public string sDescripcionSituacion
        {
            get { return _DescripcionSituacion; }
            set { _DescripcionSituacion = value; }
        }

        public string sDescripcionTipoCreacion
        {
            get { return _DescripcionTipoCreacion; }
            set { _DescripcionTipoCreacion = value; }
        }

        public string sDescripcionSocio
        {
            get { return _DescripcionSocio; }
            set { _DescripcionSocio = value; }
        }


        public string sDescripcionCliente
        {
            get { return _DescripcionCliente; }
            set { _DescripcionCliente = value; }
        }

        public string sDescripcionRolCliente
        {
            get { return _DescripcionRolCliente; }
            set { _DescripcionRolCliente = value; }
        }

        public string sEstado
        {
            get { return _OrdSer_chr_Estado; }
            set { _OrdSer_chr_Estado = value; }
        }

        public string sObservacion
        {
            get { return _OrdSer_vch_Observacion; }
            set { _OrdSer_vch_Observacion = value; }
        }

        public string sSolicitante
        {
            get { return _OrdSer_vch_Solicitante; }
            set { _OrdSer_vch_Solicitante = value; }
        }

        public string sFechaProg
        {
            get { return _OrdSer_dt_FechaProg; }
            set { _OrdSer_dt_FechaProg = value; }
        }

        public string sFecha
        {
            get { return _OrdSer_dt_Fecha; }
            set { _OrdSer_dt_Fecha = value; }
        }

        public string sTipoRegistro
        {
            get { return _OrdSer_chr_TipoRegistro; }
            set { _OrdSer_chr_TipoRegistro = value; }
        }

        public int? iIdDocOri
        {
            get { return _IdDocOri; }
            set { _IdDocOri = value; }
        }
        public int? iIdSituacion
        {
            get { return _IdSituacion; }
            set { _IdSituacion = value; }
        }

        public int? iIdLineaNegocio
        {
            get { return _IdLineaNegocio; }
            set { _IdLineaNegocio = value; }
        }

        public int? iIdTipoSolicitud
        {
            get { return _IdTipoSolicitud; }
            set { _IdTipoSolicitud = value; }
        }

        public int? iIdSocio
        {
            get { return _IdSocio; }
            set { _IdSocio = value; }
        }

        public int? iIdRolSocio
        {
            get { return _IdRolSocio; }
            set { _IdRolSocio = value; }
        }

        public int? iIdCliente
        {
            get { return _IdCliente; }
            set { _IdCliente = value; }
        }
        public int? iIdRolCliente
        {
            get { return _IdRolCliente; }
            set { _IdRolCliente = value; }
        }
        public int? iIdOrdSer
        {
            get { return _IdOrdSer; }
            set { _IdOrdSer = value; }
        }

        public int? iIdOrdSerDet
        {
            get { return _IdOrdSerDet; }
            set { _IdOrdSerDet = value; }
        }

        public int? iIdMovbal
        {
            get { return _IdMovbal; }
            set { _IdMovbal = value; }
        }

        public String sTipoServicio
        {
            get { return _TipoServicio; }
            set { _TipoServicio = value; }
        }

        public String sCndCarga
        {
            get { return _CndCarga; }
            set { _CndCarga = value; }
        }

        public Int32? iTamano
        {
            get { return _Tamano; }
            set { _Tamano = value; }
        }

        public Int32? iIdTipoOperacion
        {
            get { return _IdTipoOperacion; }
            set { _IdTipoOperacion = value; }
        }

        public Int32? iIdOrigen
        {
            get { return _IdOrigen; }
            set { _IdOrigen = value; }
        }

        public String iTipo
        {
            get { return _Tipo; }
            set { _Tipo = value; }
        }

        public String sChasisBulto
        {
            get { return _ChasisBulto; }
            set { _ChasisBulto = value; }
        }

        public String sAgenciaAduana
        {
            get { return _AgenciaAduana; }
            set { _AgenciaAduana = value; }
        }

        public String sConsignatario
        {
            get { return _Consignatario; }
            set { _Consignatario = value; }
        }

        public String sManifiesto
        {
            get { return _Manifiesto; }
            set { _Manifiesto = value; }
        }

        public Int32? iNroVolante
        {
            get { return _NroVolante; }
            set { _NroVolante = value; }
        }

        public String sNroVolante
        {
            get { return _sNroVolante; }
            set { _sNroVolante = value; }
        }


        public String sNave
        {
            get { return _Nave; }
            set { _Nave = value; }
        }

        public String sMercaderia
        {
            get { return _Mercaderia; }
            set { _Mercaderia = value; }
        }

        public String sStrIdCndCarga
        {
            get { return _StrIdCndCarga; }
            set { _StrIdCndCarga = value; }
        }

        public String sStrIdTipoServicio
        {
            get { return _StrIdTipoServicio; }
            set { _StrIdTipoServicio = value; }
        }

        public DateTime? dtFechaProgramacion
        {
            get { return _FechaProgramacion; }
            set { _FechaProgramacion = value; }
        }

        public String sAutomatico
        {
            get { return _Automatico; }
            set { _Automatico = value; }
        }
    }

    [Serializable]
    public class BE_OrdenServicioLis : List<BE_OrdenServicio>
    {
        public void Ordenar(string propertyName, direccionOrden Direction)
        {
            BE_OrdenServicioComparer dc = new BE_OrdenServicioComparer(propertyName, Direction);
            this.Sort(dc);
        }
    }

    class BE_OrdenServicioComparer : IComparer<BE_OrdenServicio>
    {
        string _prop = "";
        direccionOrden _dir;

        public BE_OrdenServicioComparer(string propertyName, direccionOrden Direction)
        {
            _prop = propertyName;
            _dir = Direction;
        }

        public int Compare(BE_OrdenServicio x, BE_OrdenServicio y)
        {

            PropertyInfo propertyX = x.GetType().GetProperty(_prop);
            PropertyInfo propertyY = y.GetType().GetProperty(_prop);

            object px = propertyX.GetValue(x, null);
            object py = propertyY.GetValue(y, null);

            if (px == null && py == null)
            {
                return 0;
            }
            else if (px != null && py == null)
            {
                if (_dir == direccionOrden.Ascending)
                {
                    return 1;
                }
                else
                {
                    return -1;
                }
            }
            else if (px == null && py != null)
            {
                if (_dir == direccionOrden.Ascending)
                {
                    return -1;
                }
                else
                {
                    return 1;
                }
            }
            else if (px.GetType().GetInterface("IComparable") != null)
            {
                if (_dir == direccionOrden.Ascending)
                {
                    return ((IComparable)px).CompareTo(py);
                }
                else
                {
                    return ((IComparable)py).CompareTo(px);
                }
            }
            else
            {
                return 0;
            }
        }

    }

}
