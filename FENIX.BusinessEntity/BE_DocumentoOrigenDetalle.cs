﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Reflection;
using FENIX.Common;

namespace FENIX.BusinessEntity
{
    [Serializable]
    public class BE_DocumentoOrigenDetalle : Paginador
    {

        #region "Campos"
        int? _IdDocOriDet;
        int? _IdTipoCarga;
        int? _IdDocOri;
        int? _IdCondicionCargaDoc;
        int? _IdCondicionCargaFacturar;
        int? _IdCargaManifestada;
        int? _IdEmbalajeManifestado;
        int? _IdCargaRecibida;
        int? _IdEmbalajeRecibido;
        int? _IdStoCon;
        int? _IdDuaActual;
        int? _DocDet_Int_Item;
        int? _IdRolAgenciaAduana;
        int? _IdAgenciaAduana;
        DateTime? _DocDet_Dt_FechaIngreso = null;
        DateTime? _DocDet_Dt_FechaRetiro = null;
        DateTime? _DocDet_Dt_FechaApertura = null;
        String _DocDet_NumeroOrden = null;
        String _DocDet_Chr_CargaPeligrosa = null;
        String _DocDet_Vhr_CodigoImo = null;
        String _DocDet_Vhr_CodigoCm = null;
        decimal? _DocDet_dec_DiceContener = null;
        int? _DocDet_Int_BultosManifestados = null;
        decimal? _DocDet_dec_PesoManifestado = null;
        decimal? _DocDet_dec_VolumenManifestado = null;
        String _DocDet_Vhr_ChasisManifestado = null;
        decimal? _DocDet_dec_PesoRecibido = null;

        String _DocDet_Vch_ChasisRecibido = null;

        decimal? _DocDet_dec_PesoInmovilizado = null;
        decimal? _DocDet_dec_VolumenInmovilizado = null;

        String _TipoContenedorDes = null;
        String _ContenedorDes = null;
        String _TipoContenedorManifestadoDes = null;
        String _TipoDimencionManifestadoDes = null;
        String _CondicionCargaManifestadoDes = null;

        String _CargaManifestadaDes = null;
        String _EmbCargaManifestadoDes = null;
        String _Dua_vch_Anio;
        String _Dua_vch_Numero;
        DateTime? _Dua_dt_Fecha;
        String _AgenciaAduanaDes;
        String _DuaCliAgenciaAduDes;
        String _CndCargaFacturarDes;
        decimal? _DocDet_dec_VolumenRecibido = null;
        decimal? _DocDet_dec_BultosRecibido = null;
        decimal? _DocDet_dec_BultosInmovilizado = null;
        decimal? _DocDet_dec_BultosDespacho = null;
        decimal? _DocDet_dec_PesoDespacho = null;
        decimal? _DocDet_dec_BultosSaldo = null;
        decimal? _DocDet_dec_VolumenSaldo = null;

        decimal? _DocDet_dec_BultosFacturado = null;
        decimal? _DocDet_dec_VolumenFacturado = null;
        decimal? _DocDet_dec_PesoFacturado = null;

        decimal? _DocDet_dec_VolDespacho = null;

        decimal? _DocDet_dec_PesoSaldo = null;
        String _TmnoConManifestadoDes = null;

        String _sUnidadMedidaDes = null;
        Int32? _iIdUnidadMedida = null;

        Int32? _IdTamanoCntManifestado_Cnt = null;
        Int32? _IdTipoCntManifestado_Cnt = null;
        Int32? _IdCondicion_Cnt = null;
        Int32? _IdLineaMaritima_Cnt = null;
        Int32? _IdUnidadMedida_Cnt = null;


        Int32? _Tara_Cnt = null;
        Decimal? _PesoNetoTotal_Cnt = null;
        Decimal? _PesoBrutoTotal_Cnt = null;



        Int32? _BultosTotal_Cnt = null;
        String _Conexion_Cnt = null;
        String _LinMar_Descripcion_Cnt = null;
        Decimal? _Temperatura_Cnt = null;
        int? _iIdCarga = null;

        String _Precin_vch_Linea_Cnt = null;
        String _Precin_vch_Aduana_Cnt = null;
        String _Precin_vch_Cliente_Cnt = null;
        String _Precin_vch_Otros_Cnt = null;

        String _CargaRecibidoDescrp = null;
        String _EmbaRecibidoDescrip = null;
        String _CargaFacturadoDescrp = null;
        String _EmbaFacturadoDescrip = null;
        String _CargaDespachadoDescrp = null;
        String _EmbaDespachadoDescrip = null;
        String _CargaInmovilizadoDescrp = null;
        String _EmbaInmovilizadoDescrip = null;

        String _sCodEmbalajeManifestado = null;
        String _sCodCargaManifestado = null;

        #endregion


        public String sTicketIngreso { get; set; }
        public String sTicketSalida { get; set; }
        public String sDocumento { get; set; }
        public String sCliente { get; set; }



        #region "Propiedades"
        public String sCodCargaManifestado
        {
            get { return _sCodCargaManifestado; }
            set { _sCodCargaManifestado = value; }
        }
        public String sEmbaInmovilizadoDescrip
        {
            get { return _EmbaInmovilizadoDescrip; }
            set { _EmbaInmovilizadoDescrip = value; }
        }

        public String sCargaInmovilizadoDescrp
        {
            get { return _CargaInmovilizadoDescrp; }
            set { _CargaInmovilizadoDescrp = value; }
        }
        public String sEmbaDespachadoDescrip
        {
            get { return _EmbaDespachadoDescrip; }
            set { _EmbaDespachadoDescrip = value; }
        }

        public String sCargaDespachadoDescrp
        {
            get { return _CargaDespachadoDescrp; }
            set { _CargaDespachadoDescrp = value; }
        }

        public String sEmbaFacturadoDescrip
        {
            get { return _EmbaFacturadoDescrip; }
            set { _EmbaFacturadoDescrip = value; }
        }

        public String sCargaFacturadoDescrp
        {
            get { return _CargaFacturadoDescrp; }
            set { _CargaFacturadoDescrp = value; }
        }


        public String sEmbaRecibidoDescrip
        {
            get { return _EmbaRecibidoDescrip; }
            set { _EmbaRecibidoDescrip = value; }
        }


        public String sCargaRecibidoDescrp
        {
            get { return _CargaRecibidoDescrp; }
            set { _CargaRecibidoDescrp = value; }
        }




        public String sLinMar_Descripcion_Cnt
        {
            get { return _LinMar_Descripcion_Cnt; }
            set { _LinMar_Descripcion_Cnt = value; }
        }

        public String sPrecinOtros_Cnt
        {
            get { return _Precin_vch_Otros_Cnt; }
            set { _Precin_vch_Otros_Cnt = value; }
        }


        public String sPrecinCliente_Cnt
        {
            get { return _Precin_vch_Cliente_Cnt; }
            set { _Precin_vch_Cliente_Cnt = value; }
        }
        public String sPrecinAduana_Cnt
        {
            get { return _Precin_vch_Aduana_Cnt; }
            set { _Precin_vch_Aduana_Cnt = value; }
        }

        public String sPrecinLinea_Cnt
        {
            get { return _Precin_vch_Linea_Cnt; }
            set { _Precin_vch_Linea_Cnt = value; }
        }

        public Decimal? dTemperatura_Cnt
        {
            get { return _Temperatura_Cnt; }
            set { _Temperatura_Cnt = value; }
        }
        public String sConexion_Cnt
        {
            get { return _Conexion_Cnt; }
            set { _Conexion_Cnt = value; }
        }
        public Int32? dBultosTotal_Cnt
        {
            get { return _BultosTotal_Cnt; }
            set { _BultosTotal_Cnt = value; }
        }
        public Decimal? dPesoBrutoTotal_Cnt
        {
            get { return _PesoBrutoTotal_Cnt; }
            set { _PesoBrutoTotal_Cnt = value; }
        }
        public Decimal? dPesoNetoTotal_Cnt
        {
            get { return _PesoNetoTotal_Cnt; }
            set { _PesoNetoTotal_Cnt = value; }
        }

        public Int32? dTara_Cnt
        {
            get { return _Tara_Cnt; }
            set { _Tara_Cnt = value; }
        }


        public Int32? iIdUnidadMedida_Cnt
        {
            get { return _IdUnidadMedida_Cnt; }
            set { _IdUnidadMedida_Cnt = value; }
        }
        public Int32? iIdTipoCarga
        {
            get { return _IdTipoCarga; }
            set { _IdTipoCarga = value; }
        }



        public Int32? iIdLineaMaritima_Cnt
        {
            get { return _IdLineaMaritima_Cnt; }
            set { _IdLineaMaritima_Cnt = value; }
        }
        public Int32? iIdCondicion_Cnt
        {
            get { return _IdCondicion_Cnt; }
            set { _IdCondicion_Cnt = value; }
        }

        public Int32? iIdTipoCntManifestado_Cnt
        {
            get { return _IdTipoCntManifestado_Cnt; }
            set { _IdTipoCntManifestado_Cnt = value; }
        }


        public Int32? iIdTamanoCntManifestado_Cnt
        {
            get { return _IdTamanoCntManifestado_Cnt; }
            set { _IdTamanoCntManifestado_Cnt = value; }
        }


        public int? iIdCarga
        {
            get { return _iIdCarga; }
            set { _iIdCarga = value; }
        }

        public Int32? iIdUnidadMedida
        {
            get { return _iIdUnidadMedida; }
            set { _iIdUnidadMedida = value; }
        }

        public string sUnidadMedidaDes
        {
            get { return _sUnidadMedidaDes; }
            set { _sUnidadMedidaDes = value; }
        }

        public decimal? dPesoFacturado
        {
            get { return _DocDet_dec_PesoFacturado; }
            set { _DocDet_dec_PesoFacturado = value; }
        }
        public decimal? dVolumenFacturado
        {
            get { return _DocDet_dec_VolumenFacturado; }
            set { _DocDet_dec_VolumenFacturado = value; }
        }

        public decimal? dBultosFacturado
        {
            get { return _DocDet_dec_BultosFacturado; }
            set { _DocDet_dec_BultosFacturado = value; }
        }


        public int? iIdAgenciaAduana
        {
            get { return _IdAgenciaAduana; }
            set { _IdAgenciaAduana = value; }
        }
        public int? iIdRolAgenciaAduana
        {
            get { return _IdRolAgenciaAduana; }
            set { _IdRolAgenciaAduana = value; }
        }

        public Decimal? dVolumenDespacho
        {
            get { return _DocDet_dec_VolDespacho; }
            set { _DocDet_dec_VolDespacho = value; }
        }

        public Decimal? dPesoDespacho
        {
            get { return _DocDet_dec_PesoDespacho; }
            set { _DocDet_dec_PesoDespacho = value; }
        }

        public String sDuaCliAgenciaAduDes
        {
            get { return _DuaCliAgenciaAduDes; }
            set { _DuaCliAgenciaAduDes = value; }
        }

        public String sCargaPeligrosa
        {
            get { return _DocDet_Chr_CargaPeligrosa; }
            set { _DocDet_Chr_CargaPeligrosa = value; }
        }
        public String sTmnoConManifestadoDes
        {
            get { return _TmnoConManifestadoDes; }
            set { _TmnoConManifestadoDes = value; }
        }
        public Decimal? dPesoSaldo
        {
            get { return _DocDet_dec_PesoSaldo; }
            set { _DocDet_dec_PesoSaldo = value; }
        }
        public Decimal? dVolumenSaldo
        {
            get { return _DocDet_dec_VolumenSaldo; }
            set { _DocDet_dec_VolumenSaldo = value; }
        }
        public Decimal? dBultosSaldo
        {
            get { return _DocDet_dec_BultosSaldo; }
            set { _DocDet_dec_BultosSaldo = value; }
        }
        public Decimal? dBultosDespacho
        {
            get { return _DocDet_dec_BultosDespacho; }
            set { _DocDet_dec_BultosDespacho = value; }
        }
        public Decimal? dBultosInmovilizado
        {
            get { return _DocDet_dec_BultosInmovilizado; }
            set { _DocDet_dec_BultosInmovilizado = value; }
        }
        public Decimal? dBultosRecibido
        {
            get { return _DocDet_dec_BultosRecibido; }
            set { _DocDet_dec_BultosRecibido = value; }
        }

        public String sCndCargaFacturarDes
        {
            get { return _CndCargaFacturarDes; }
            set { _CndCargaFacturarDes = value; }
        }
        public String sAgenciaAduanaDes
        {
            get { return _AgenciaAduanaDes; }
            set { _AgenciaAduanaDes = value; }
        }

        public DateTime? dtDuaFecha
        {
            get { return _Dua_dt_Fecha; }
            set { _Dua_dt_Fecha = value; }
        }

        public String sDuaNumero
        {
            get { return _Dua_vch_Numero; }
            set { _Dua_vch_Numero = value; }
        }

        public String sDuaAnio
        {
            get { return _Dua_vch_Anio; }
            set { _Dua_vch_Anio = value; }
        }



        public String sContenedorDes
        {
            get { return _ContenedorDes; }
            set { _ContenedorDes = value; }
        }

        public String sEmbCargaManifestadoDes
        {
            get { return _EmbCargaManifestadoDes; }
            set { _EmbCargaManifestadoDes = value; }
        }

        public String sCargaManifestadaDes
        {
            get { return _CargaManifestadaDes; }
            set { _CargaManifestadaDes = value; }
        }



        public String sCondicionCargaManifestadoDes
        {
            get { return _CondicionCargaManifestadoDes; }
            set { _CondicionCargaManifestadoDes = value; }
        }
        public String sTipoDimencionManifestadoDes
        {
            get { return _TipoDimencionManifestadoDes; }
            set { _TipoDimencionManifestadoDes = value; }
        }

        public String sTipoContenedorManifestadoDes
        {
            get { return _TipoContenedorManifestadoDes; }
            set { _TipoContenedorManifestadoDes = value; }
        }

        public String sTipoContenedorDes
        {
            get { return _TipoContenedorDes; }
            set { _TipoContenedorDes = value; }
        }

        public int? iItem
        {
            get { return _DocDet_Int_Item; }
            set { _DocDet_Int_Item = value; }
        }
        public int? iIdDuaActual
        {
            get { return _IdDuaActual; }
            set { _IdDuaActual = value; }
        }

        public int? iIdStoCon
        {
            get { return _IdStoCon; }
            set { _IdStoCon = value; }
        }

        public int? iIdEmbalajeRecibido
        {
            get { return _IdEmbalajeRecibido; }
            set { _IdEmbalajeRecibido = value; }
        }

        public int? iIdCargaRecibida
        {
            get { return _IdCargaRecibida; }
            set { _IdCargaRecibida = value; }
        }


        public int? iIdEmbalajeManifestado
        {
            get { return _IdEmbalajeManifestado; }
            set { _IdEmbalajeManifestado = value; }
        }

        public int? iIdCargaManifestada
        {
            get { return _IdCargaManifestada; }
            set { _IdCargaManifestada = value; }
        }

        public int? iIdCondicionCargaFacturar
        {
            get { return _IdCondicionCargaFacturar; }
            set { _IdCondicionCargaFacturar = value; }
        }

        public int? iIdCondicionCargaDoc
        {
            get { return _IdCondicionCargaDoc; }
            set { _IdCondicionCargaDoc = value; }
        }

        public int? iIdDocOri
        {
            get { return _IdDocOri; }
            set { _IdDocOri = value; }
        }
        public int? iIdDocOriDet
        {
            get { return _IdDocOriDet; }
            set { _IdDocOriDet = value; }
        }

        public DateTime? dtFechaIngreso
        {
            get { return _DocDet_Dt_FechaIngreso; }
            set { _DocDet_Dt_FechaIngreso = value; }
        }
        public DateTime? dtFechaRetiro
        {
            get { return _DocDet_Dt_FechaRetiro; }
            set { _DocDet_Dt_FechaRetiro = value; }
        }

        public DateTime? dtFechaApertura
        {
            get { return _DocDet_Dt_FechaApertura; }
            set { _DocDet_Dt_FechaApertura = value; }
        }

        public String sNumeroOrden
        {
            get { return _DocDet_NumeroOrden; }
            set { _DocDet_NumeroOrden = value; }
        }

        public String sCodigoImo
        {
            get { return _DocDet_Vhr_CodigoImo; }
            set { _DocDet_Vhr_CodigoImo = value; }
        }

        public String sCodigoCm
        {
            get { return _DocDet_Vhr_CodigoCm; }
            set { _DocDet_Vhr_CodigoCm = value; }
        }
        public decimal? dDiceContener
        {
            get { return _DocDet_dec_DiceContener; }
            set { _DocDet_dec_DiceContener = value; }
        }

        public int? iBultosManifestados
        {
            get { return _DocDet_Int_BultosManifestados; }
            set { _DocDet_Int_BultosManifestados = value; }
        }

        public decimal? dPesoManifestado
        {
            get { return _DocDet_dec_PesoManifestado; }
            set { _DocDet_dec_PesoManifestado = value; }
        }

        public decimal? dVolumenManifestado
        {
            get { return _DocDet_dec_VolumenManifestado; }
            set { _DocDet_dec_VolumenManifestado = value; }
        }
        public String sChasisManifestado
        {
            get { return _DocDet_Vhr_ChasisManifestado; }
            set { _DocDet_Vhr_ChasisManifestado = value; }
        }


        public decimal? dPesoRecibido
        {
            get { return _DocDet_dec_PesoRecibido; }
            set { _DocDet_dec_PesoRecibido = value; }
        }


        public decimal? dVolumenRecibido
        {
            get { return _DocDet_dec_VolumenRecibido; }
            set { _DocDet_dec_VolumenRecibido = value; }
        }

        public String sChasisRecibido
        {
            get { return _DocDet_Vch_ChasisRecibido; }
            set { _DocDet_Vch_ChasisRecibido = value; }
        }


        public decimal? dPesoInmovilizado
        {
            get { return _DocDet_dec_PesoInmovilizado; }
            set { _DocDet_dec_PesoInmovilizado = value; }
        }

        public decimal? dVolumenInmovilizado
        {
            get { return _DocDet_dec_VolumenInmovilizado; }
            set { _DocDet_dec_VolumenInmovilizado = value; }
        }

        public String sCodEmbalajeManifestado
        {
            get { return _sCodEmbalajeManifestado; }
            set { _sCodEmbalajeManifestado = value; }
        }
        #endregion
    }
    #region Metodos
    [Serializable]
    public class BE_DocumentoOrigenDetalleList : List<BE_DocumentoOrigenDetalle>
    {
        public void Ordenar(string propertyName, direccionOrden Direction)
        {
            BE_DocumentoOrigenDetalleComparer dc = new BE_DocumentoOrigenDetalleComparer(propertyName, Direction);
            this.Sort(dc);
        }
    }

    class BE_DocumentoOrigenDetalleComparer : IComparer<BE_DocumentoOrigenDetalle>
    {
        string _prop = "";
        direccionOrden _dir;

        public BE_DocumentoOrigenDetalleComparer(string propertyName, direccionOrden Direction)
        {
            _prop = propertyName;
            _dir = Direction;
        }

        public int Compare(BE_DocumentoOrigenDetalle x, BE_DocumentoOrigenDetalle y)
        {

            PropertyInfo propertyX = x.GetType().GetProperty(_prop);
            PropertyInfo propertyY = y.GetType().GetProperty(_prop);

            object px = propertyX.GetValue(x, null);
            object py = propertyY.GetValue(y, null);

            if (px == null && py == null)
            {
                return 0;
            }
            else if (px != null && py == null)
            {
                if (_dir == direccionOrden.Ascending)
                {
                    return 1;
                }
                else
                {
                    return -1;
                }
            }
            else if (px == null && py != null)
            {
                if (_dir == direccionOrden.Ascending)
                {
                    return -1;
                }
                else
                {
                    return 1;
                }
            }
            else if (px.GetType().GetInterface("IComparable") != null)
            {
                if (_dir == direccionOrden.Ascending)
                {
                    return ((IComparable)px).CompareTo(py);
                }
                else
                {
                    return ((IComparable)py).CompareTo(px);
                }
            }
            else
            {
                return 0;
            }
        }



    }

    #endregion
}
