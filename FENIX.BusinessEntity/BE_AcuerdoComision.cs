﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;

namespace FENIX.BusinessEntity
{
    [Serializable]
    public class BE_AcuerdoComision
    {
        int _iLiquidacion;
        String  _sLiquidacion;
        int _idDocOriDet;
        Int32 _iComisionista;
        String _sEjecutivo;
        String _sComisionista;
        int _iMoneda;
        String _sdocMaster;
        String _sdocHijo;
        String _sVolante;
        String _sconsignatario;
        String _scontenedor;
        String _sCond;
        String _sMoned;
        Decimal? _dimporte;
        String _sNroFactura;
        String _sContrato;
        String _sApto;
        String _sServicio;
        Int32 __idDocpagoDet;
        Int32 __idDetpaqSLI;
        String _sRetirado;
        String _sFacturado;
        String _sSituacion;
        Int32 _idAcuerdoComision;
        Int32 _idTarifa;
        String _sPqtSLI;
        String _sUsuario;
        Int32 _iTipoTran;
        Int32 _nPagina;
        Int32 _nRegistros;
        Int32 _totalRegistros;
        Int32 _iTipoDoc;
        String _sNomPc;
        String _sTipoDoc;

        String _sDocNroComisionista;
        String _sDocNroSerie;
        Int32 _idDocComisionista;
        String _sTiempoSituacion;
        Int32 _idSituacion;
        Int32 _idValor;
        String _sValor;
        Int32 _iPeriodo;
        String _sMesIni;
        String _sMesFin;
        Int32 _iEstado;
        bool _sAceptaCondicion;
        String _Estado;
        String _sFechaIngreso;
        decimal? _dImporteVisual;


        public bool sAceptaCondicion
        {
            get
            {
                return _sAceptaCondicion;
            }
            set
            {
                _sAceptaCondicion = value;
            }
        }
        public decimal? dImporteVisual
        {
            get
            {
                return _dImporteVisual;
            }
            set
            {
                _dImporteVisual = value;
            }
        }
        public String sFechaIngreso
        {
            get
            {
                return _sFechaIngreso;
            }
            set
            {
                _sFechaIngreso = value;
            }
        }
        public String sEstado
        {
            get
            {
                return _Estado;
            }
            set
            {
                _Estado = value;
            }
        }


        public int iPeriodo
        {
            get
            {
                return _iPeriodo;
            }
            set
            {
                _iPeriodo = value;
            }
        }
        public String sMesIni
        {
            get
            {
                return _sMesIni;
            }
            set
            {
                _sMesIni = value;
            }
        }

        public String sMesFin
        {
            get
            {
                return _sMesFin;
            }
            set
            {
                _sMesFin = value;
            }
        }

        public int iEstado
        {
            get
            {
                return _iEstado;
            }
            set
            {
                _iEstado = value;
            }
        }

        public int iLiquidacion
        {
            get
            {
                return _iLiquidacion;
            }
            set
            {
                _iLiquidacion = value;
            }
        }

        public String sLiquidacion
        {
            get
            {
                return _sLiquidacion;
            }
            set
            {
                _sLiquidacion = value;
            }
        }

        public String sValor
        {
            get
            {
                return _sValor;
            }
            set
            {
                _sValor = value;
            }
        }

        public Int32 iIdValor
        {
            get
            {
                return _idValor;
            }
            set
            {
                _idValor = value;
            }
        }

        public int iIdSituacion
        {
            get
            {
                return _idSituacion;
            }
            set
            {
                _idSituacion = value;
            }
        }

        public int iIdDocOriDet
        {
            get
            {
                return _idDocOriDet;
            }
            set
            {
                _idDocOriDet = value;
            }
        }

        public String sDocMaster
        {
            get
            {
                return _sdocMaster;
            }
            set
            {
                _sdocMaster = value;
            }
        }


        public String sTiempoSituacion
        {
            get
            {
                return _sTiempoSituacion;
            }
            set
            {
                _sTiempoSituacion = value;
            }
        }

        public String sTipoDoc
        {
            get
            {
                return _sTipoDoc;
            }
            set
            {
                _sTipoDoc = value;
            }
        }
        public String sNomPc
        {
            get
            {
                return _sNomPc;
            }
            set
            {
                _sNomPc = value;
            }
        }


        public Int32 iIdDocComisionista
        {
            get
            {
                return _idDocComisionista;
            }
            set
            {
                _idDocComisionista = value;
            }
        }
        public String sDocHijo
        {
            get
            {
                return _sdocHijo;
            }
            set
            {
                _sdocHijo = value;
            }
        }
        public String sVolante
        {
            get
            {
                return _sVolante;
            }
            set
            {
                _sVolante = value;
            }
        }
        public String sConsignatario
        {
            get
            {
                return _sconsignatario;
            }
            set
            {
                _sconsignatario = value;
            }
        }
        public String sContenedor
        {
            get
            {
                return _scontenedor;
            }
            set
            {
                _scontenedor = value;
            }
        }
        public String sCondicion
        {
            get
            {
                return _sCond;
            }
            set
            {
                _sCond = value;
            }
        }
        public String sMoneda
        {
            get
            {
                return _sMoned;
            }
            set
            {
                _sMoned = value;
            }
        }

        public Decimal? dImporte
        {
            get
            {
                return _dimporte;
            }
            set
            {
                _dimporte = value;
            }
        }

        public Int32 iTipoDoc
        {
            get
            {
                return _iTipoDoc;
            }
            set
            {
                _iTipoDoc = value;
            }
        }

        public String sNroFactura
        {
            get
            {
                return _sNroFactura;
            }
            set
            {
                _sNroFactura = value;
            }
        }
        public String sNroContrato
        {
            get
            {
                return _sContrato;
            }
            set
            {
                _sContrato = value;
            }
        }
        public Int32 iTipoTran
        {
            get
            {
                return _iTipoTran;
            }
            set
            {
                _iTipoTran = value;
            }
        }

        public String sEjecutivo
        {
            get
            {
                return _sEjecutivo;
            }
            set
            {
                _sEjecutivo = value;
            }
        }


        public String sApto
        {
            get
            {
                return _sApto;
            }
            set
            {
                _sApto = value;
            }
        }

        public String sServicio
        {
            get
            {
                return _sServicio;
            }
            set
            {
                _sServicio = value;
            }
        }

        public int iIdMoneda
        {
            get
            {
                return _iMoneda;
            }
            set
            {
                _iMoneda = value;
            }
        }

        public Int32 iIdComisionista
        {
            get
            {
                return _iComisionista;
            }
            set
            {
                _iComisionista = value;
            }
        }
        public Int32 iIdDocPagoDet
        {
            get
            {
                return __idDocpagoDet;
            }
            set
            {
                __idDocpagoDet = value;
            }
        }
        public Int32 iIdDetpaqSLI
        {
            get
            {
                return __idDetpaqSLI;
            }
            set
            {
                __idDetpaqSLI = value;
            }
        }

        public String sRetirado
        {
            get
            {
                return _sRetirado;
            }
            set
            {
                _sRetirado = value;
            }
        }
        public String sFacturado
        {
            get
            {
                return _sFacturado;
            }
            set
            {
                _sFacturado = value;
            }
        }
        public String sSituacion
        {
            get
            {
                return _sSituacion;
            }
            set
            {
                _sSituacion = value;
            }
        }
        public Int32 IidAcuerdoComision
        {
            get
            {
                return _idAcuerdoComision;
            }
            set
            {
                _idAcuerdoComision = value;
            }
        }
        public Int32 IidTarifa
        {
            get
            {
                return _idTarifa;
            }
            set
            {
                _idTarifa = value;
            }
        }

        public String sPqtSLI
        {
            get
            {
                return _sPqtSLI;
            }
            set
            {
                _sPqtSLI = value;
            }
        }

        public String sUsuario
        {
            get
            {
                return _sUsuario;
            }
            set
            {
                _sUsuario = value;
            }
        }
        public String sComisionista
        {
            get
            {
                return _sComisionista;
            }
            set
            {
                _sComisionista = value;
            }
        }

        public Int32 NPagina
        {
            get
            {
                return _nPagina;
            }
            set
            {
                _nPagina = value;
            }
        }

        public Int32 NRegistros
        {
            get
            {
                return _nRegistros;
            }
            set
            {
                _nRegistros = value;
            }
        }
        public Int32 NtotalRegistros
        {
            get
            {
                return _totalRegistros;
            }
            set
            {
                _totalRegistros = value;
            }
        }

        public String sDocNroComisionista
        {
            get
            {
                return _sDocNroComisionista;
            }
            set
            {
                _sDocNroComisionista = value;
            }
        }

        public String sDocNroSerie
        {
            get
            {
                return _sDocNroSerie;
            }
            set
            {
                _sDocNroSerie = value;
            }
        }



        [Serializable]
        public class BE_AcuerdoComisionList : List<BE_AcuerdoComision>
        {
            public void Ordenar(string propertyName, direccionOrden Direction)
            {
                BE_AcuerdoComisionComparer dc = new BE_AcuerdoComisionComparer(propertyName, Direction);
                this.Sort(dc);
            }
        }

        class BE_AcuerdoComisionComparer : IComparer<BE_AcuerdoComision>
        {
            string _prop = "";
            direccionOrden _dir;

            public BE_AcuerdoComisionComparer(string propertyName, direccionOrden Direction)
            {
                _prop = propertyName;
                _dir = Direction;
            }

            public int Compare(BE_AcuerdoComision x, BE_AcuerdoComision y)
            {

                PropertyInfo propertyX = x.GetType().GetProperty(_prop);
                PropertyInfo propertyY = y.GetType().GetProperty(_prop);

                object px = propertyX.GetValue(x, null);
                object py = propertyY.GetValue(y, null);

                if (px == null && py == null)
                {
                    return 0;
                }
                else if (px != null && py == null)
                {
                    if (_dir == direccionOrden.Ascending)
                    {
                        return 1;
                    }
                    else
                    {
                        return -1;
                    }
                }
                else if (px == null && py != null)
                {
                    if (_dir == direccionOrden.Ascending)
                    {
                        return -1;
                    }
                    else
                    {
                        return 1;
                    }
                }
                else if (px.GetType().GetInterface("IComparable") != null)
                {
                    if (_dir == direccionOrden.Ascending)
                    {
                        return ((IComparable)px).CompareTo(py);
                    }
                    else
                    {
                        return ((IComparable)py).CompareTo(px);
                    }
                }
                else
                {
                    return 0;
                }
            }

        }
        
    }
}
