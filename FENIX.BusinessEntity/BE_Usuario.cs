﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Reflection;

/*
 * @001 ECH 22-05-2013:  Se agrego las entidades para el listado de menu por usuario
 */
namespace FENIX.BusinessEntity
{
    [Serializable]
    public class BE_Usuario
    {
        String _Usuario;
        String _Clave;
        String _NuevaClave;
        String _NuevaClaveConf;
        String _Sistema;
        String _SolicitarCambioClave;
        String _SolicitarNewClave;
        String _Correo;
        //Int32 _IdUsuario;
        String _DNI;
        string _sesionIni;

        Int32 _idUsuario;
        Int32 _iDiasCambioClave;
        Int32 _iNumClavesVerificar;
        Int32 _iMmBloqueoVecesFallido;
        Int32 _iMmDesBloqueoUsuario;
        Int32 _iMnCierreSesion;
        Int32 _iMaximoSolicitudVolantes;
        String _esCliente;
        Int32 _idcliente;
        String _sCliente;
        String _sRucCliente;
        String _sNomUsuario;
        String _sFecha;
        String _sHabilitado;
        String _sUsuarioActualiza;
        String _sNomPc;

        /* @001 - I*/

        Int32 _PadreMenuId;
        String _DescripcionPadre;
        Int32 _Posicion;
        Int32 _MenuId;
        String _Descripcion;
        String _Url;

        public Boolean isHabilitado { get; set; }
        public int iTerCondiciones { get; set; }
        /* @001 - F */

        public Int32 iIdUsuario
        {
            get { return _idUsuario; }
            set { _idUsuario = value;}
        }

        public String sUsuario
        {
            get { return _Usuario; }
            set { _Usuario = value; }
        }
        
        public String sClave
        {
            get { return _Clave; }
            set { _Clave = value; }
        }

        public String sNuevaClave
        {
            get { return _NuevaClave; }
            set { _NuevaClave = value; }
        }

        public String sNuevaClaveConf
        {
            get { return _NuevaClaveConf; }
            set { _NuevaClaveConf = value; }
        }

        public String sSistema
        {
            get { return _Sistema; }
            set { _Sistema = value; }
        }

        public String sSolicitarCambioClave
        {
            get { return _SolicitarCambioClave; }
            set { _SolicitarCambioClave = value; }
        }

        public String sSolicitarNewClaveOlvidada
        {
            get { return _SolicitarNewClave; }
            set { _SolicitarNewClave = value; }
        }

        public String sCorreo
        {
            get { return _Correo; }
            set { _Correo =value;}
        }

        public String sSesionIniciada
        {
            get { return _sesionIni; }
            set { _sesionIni = value; }
        }

        public String sDNI
        {
            get { return _DNI; }
            set {_DNI = value; }
        }

        public String sEsCliente
        {
            get { return _esCliente; }
            set { _esCliente = value; }
        }

        public Int32 iIdCliente
        {
            get { return _idcliente; }
            set { _idcliente = value; }
        }

        public String sCliente
        {
            get { return _sCliente; }
            set { _sCliente = value; }
        }

        public String sRucCliente
        {
            get { return _sRucCliente; }
            set { _sRucCliente = value; }
        }

        public String sNomUsuario
        {
            get { return _sNomUsuario; }
            set { _sNomUsuario = value; }
        }

        public String sFecha
        {
            get { return _sFecha; }
            set { _sFecha = value; }
        }

        public String sHabilitado
        {
            get { return _sHabilitado; }
            set { _sHabilitado = value; }
        }

        public String sUsuarioActualiza
        {
            get { return _sUsuarioActualiza; }
            set { _sUsuarioActualiza = value; }
        }

        public String sNombrePc
        {
            get { return _sNomPc; }
            set { _sNomPc = value; }
        }




        public String GetMD5(String input)
        {
            String retorno = String.Empty;

            MD5HashAlgorithm oCrip = new MD5HashAlgorithm();
            retorno = oCrip.ComputeHash(input);
            oCrip = null;

            return retorno;
        }


        /* @001 - I*/
        public Int32 iPadreMenuId
        {
            get { return _PadreMenuId; }
            set { _PadreMenuId = value; }
        }
        public String sDescripcionPadre
        {
            get { return _DescripcionPadre; }
            set { _DescripcionPadre = value; }
        }
        public Int32 iPosicion
        {
            get { return _Posicion; }
            set { _Posicion = value; }
        }
        public Int32 iMenuId
        {
            get { return _MenuId; }
            set { _MenuId = value; }
        }
        public String sDescripcion
        {
            get { return _Descripcion; }
            set { _Descripcion = value; }
        }
        public String sUrl
        {
            get { return _Url; }
            set { _Url = value; }
        }   

        /* @001 - F */

        public Int32 iDiasCambioClave
        {
            get { return _iDiasCambioClave; }
            set { _iDiasCambioClave = value; }
        }


        public Int32 iNumClavesVerificar
        {
            get { return _iNumClavesVerificar; }
            set { _iNumClavesVerificar = value; }
        }

        public Int32 iMmBloqueoVecesFallido
        {
            get { return _iMmBloqueoVecesFallido; }
            set { _iMmBloqueoVecesFallido = value; }
        }

        public Int32 iMmDesBloqueoUsuario
        {
            get { return _iMmDesBloqueoUsuario; }
            set { _iMmDesBloqueoUsuario = value; }
        }

        public Int32 iMnCierreSesion
        {
            get { return _iMnCierreSesion; }
            set { _iMnCierreSesion = value; }
        }

        public Int32 iMaximoSolicitudVolantes
        {
            get { return _iMaximoSolicitudVolantes; }
            set { _iMaximoSolicitudVolantes = value; }
        }        

    }
}
