﻿using System;
using System.Collections.Generic;
using System.Text;
using FENIX.BusinessEntity;
using System.Reflection;

namespace FENIX.BusinessEntity
{
    [Serializable]
    public class BE_Despachador
    {
        String _Codigo = String.Empty;
        String _Nombres = String.Empty;
        String _Apellidos = String.Empty;
        String _DNI = String.Empty;
        String _Situacion = String.Empty;
        String _Carnet = String.Empty;
        String _FechaVenc = String.Empty;
        String _FechaVencIni = String.Empty;

        public string Codigo
        {
            get
            {
                return _Codigo;
            }

            set
            {
                _Codigo = value;
            }
        }

        public string Nombres
        {
            get
            {
                return _Nombres;
            }

            set
            {
                _Nombres = value;
            }
        }

        public string Apellidos
        {
            get
            {
                return _Apellidos;
            }

            set
            {
                _Apellidos = value;
            }
        }

        public string DNI
        {
            get
            {
                return _DNI;
            }

            set
            {
                _DNI = value;
            }
        }

        public string Situacion
        {
            get
            {
                return _Situacion;
            }

            set
            {
                _Situacion = value;
            }
        }

        public string Carnet
        {
            get
            {
                return _Carnet;
            }

            set
            {
                _Carnet = value;
            }
        }

        public string FechaVenc
        {
            get
            {
                return _FechaVenc;
            }

            set
            {
                _FechaVenc = value;
            }
        }

        public string FechaVencIni
        {
            get
            {
                return _FechaVencIni;
            }

            set
            {
                _FechaVencIni = value;
            }
        }
    }

    [Serializable]
    public class BE_DespachadorList : List<BE_Despachador>
    {
        public void Ordenar(string propertyName, direccionOrden Direction)
        {
            BE_DespachadorComparer dc = new BE_DespachadorComparer(propertyName, Direction);
            this.Sort(dc);
        }
    }

    class BE_DespachadorComparer : IComparer<BE_Despachador>
    {
        string _prop = "";
        direccionOrden _dir;

        public BE_DespachadorComparer(string propertyName, direccionOrden Direction)
        {
            _prop = propertyName;
            _dir = Direction;
        }

        public int Compare(BE_Despachador x, BE_Despachador y)
        {

            PropertyInfo propertyX = x.GetType().GetProperty(_prop);
            PropertyInfo propertyY = y.GetType().GetProperty(_prop);

            object px = propertyX.GetValue(x, null);
            object py = propertyY.GetValue(y, null);

            if (px == null && py == null)
            {
                return 0;
            }
            else if (px != null && py == null)
            {
                if (_dir == direccionOrden.Ascending)
                {
                    return 1;
                }
                else
                {
                    return -1;
                }
            }
            else if (px == null && py != null)
            {
                if (_dir == direccionOrden.Ascending)
                {
                    return -1;
                }
                else
                {
                    return 1;
                }
            }
            else if (px.GetType().GetInterface("IComparable") != null)
            {
                if (_dir == direccionOrden.Ascending)
                {
                    return ((IComparable)px).CompareTo(py);
                }
                else
                {
                    return ((IComparable)py).CompareTo(px);
                }
            }
            else
            {
                return 0;
            }
        }

    }

}
