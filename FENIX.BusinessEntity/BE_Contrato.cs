﻿using System;
using System.Collections.Generic;
using System.Text;
using FENIX.Common;
using System.Reflection;

namespace FENIX.BusinessEntity
{
    [Serializable]
    public class BE_Contrato : Paginador
    {
        #region "Campos"

        int _IdContrato = 0;
        int _IdVendedor = 0;
        String _sIdVendedor = String.Empty;
        String _sIdTipoContrato = String.Empty;
        int _IdRolCliente = 0;
        int? _IdSocio;
        int? _IdRolSocio;
        int _IdTipoOperacion = 0;
        int _iDiasLibres = 0;
        decimal _dMonto = 0;
        int _IdCliente = 0;
        String _sIdLineaNegocio = String.Empty;
        int _IdTarifa = 0;
        int _IdConDet = 0;
        int? _idLineaMartima;
        String _Con_Dt_FechaInicio;
        String _Con_Dt_FechaFin;
        String _Con_Chr_Situacion = String.Empty;
        String _Con_Chr_Estado = String.Empty;
        String _Con_Vch_Observacion = String.Empty;
        String _Con_Vch_TarifaSli = String.Empty;
        String _sIdContrato = String.Empty;
        String _Cliente_Razon_Social = String.Empty;
        String _Cliente_Rol = String.Empty;
        String _Socio_Razon_Social = String.Empty;
        String _Socio_Rol = String.Empty;
        String _DescripcionTipoContrato = String.Empty;

        String _DescripcionVendedor = String.Empty;
        String _sLineaMaritima;
        String _DescripcionComisionista;
        Int32? _IdComisionista;
        String _NroContrato;

        int? _iIdTarifaOrigen;

        #endregion

        #region "Propiedades"

        public int? iIdTarifaOrigen
        {
            get { return _iIdTarifaOrigen; }
            set { _iIdTarifaOrigen = value; }
        }

        public decimal dMonto
        {
            get { return _dMonto; }
            set { _dMonto = value; }
        }
        public int? iIdLineaMartima
        {
            get { return _idLineaMartima; }
            set { _idLineaMartima = value; }
        }

        public int iDiasLibres
        {
            get { return _iDiasLibres; }
            set { _iDiasLibres = value; }
        }



        public String sLineaMaritima
        {
            get { return _sLineaMaritima; }
            set { _sLineaMaritima = value; }
        }

        public String sIdVendedor
        {
            get { return _sIdVendedor; }
            set { _sIdVendedor = value; }
        }



        public String sDescripcionVendedor
        {
            get { return _DescripcionVendedor; }
            set { _DescripcionVendedor = value; }
        }

        public String sDescripcionTipoContrato
        {
            get { return _DescripcionTipoContrato; }
            set { _DescripcionTipoContrato = value; }
        }
        public String sDescripcionSocioRol
        {
            get { return _Socio_Rol; }
            set { _Socio_Rol = value; }
        }

        public String sDescripcionSocio
        {
            get { return _Socio_Razon_Social; }
            set { _Socio_Razon_Social = value; }
        }

        public String sDescripcionClienteRol
        {
            get { return _Cliente_Rol; }
            set { _Cliente_Rol = value; }
        }

        public String sDescripcionCliente
        {
            get { return _Cliente_Razon_Social; }
            set { _Cliente_Razon_Social = value; }
        }

        public String sNroContrato
        {
            get { return _NroContrato; }
            set { _NroContrato = value; }
        }

        public int iIdConDet
        {
            get { return _IdConDet; }
            set { _IdConDet = value; }
        }
        public int iIdTarifa
        {
            get { return _IdTarifa; }
            set { _IdTarifa = value; }
        }
        public String sTarifaSli
        {
            get { return _Con_Vch_TarifaSli; }
            set { _Con_Vch_TarifaSli = value; }
        }

        public String sObservacion
        {
            get { return _Con_Vch_Observacion; }
            set { _Con_Vch_Observacion = value; }
        }

        public String sEstado
        {
            get { return _Con_Chr_Estado; }
            set { _Con_Chr_Estado = value; }
        }


        public String sSituacion
        {
            get { return _Con_Chr_Situacion; }
            set { _Con_Chr_Situacion = value; }
        }

        public String sFechaFin
        {
            get { return _Con_Dt_FechaFin; }
            set { _Con_Dt_FechaFin = value; }
        }
        public String sFechaInicio
        {
            get { return _Con_Dt_FechaInicio; }
            set { _Con_Dt_FechaInicio = value; }
        }

        public String sIdLineaNegocio
        {
            get { return _sIdLineaNegocio; }
            set { _sIdLineaNegocio = value; }
        }

        public int iIdCliente
        {
            get { return _IdCliente; }
            set { _IdCliente = value; }
        }

        public int iIdTipoOperacion
        {
            get { return _IdTipoOperacion; }
            set { _IdTipoOperacion = value; }
        }
        public int? iIdRolSocio
        {
            get { return _IdRolSocio; }
            set { _IdRolSocio = value; }
        }

        public int? iIdSocio
        {
            get { return _IdSocio; }
            set { _IdSocio = value; }
        }
        public int iIdRolCliente
        {
            get { return _IdRolCliente; }
            set { _IdRolCliente = value; }
        }
        public String sIdTipoContrato
        {
            get { return _sIdTipoContrato; }
            set { _sIdTipoContrato = value; }
        }
        public int iIdVendedor
        {
            get { return _IdVendedor; }
            set { _IdVendedor = value; }
        }
        public int iIdContrato
        {
            get { return _IdContrato; }
            set { _IdContrato = value; }
        }

        public String sIdContrato
        {
            get { return _sIdContrato; }
            set { _sIdContrato = value; }
        }

        public String sDescripcionComisionista
        {
            get { return _DescripcionComisionista; }
            set { _DescripcionComisionista = value; }
        }

        public Int32? iIdComisionista
        {
            get { return _IdComisionista; }
            set { _IdComisionista = value; }
        }


        #endregion
    }
    #region Metodos
    [Serializable]
    public class BE_ContratoList : List<BE_Contrato>
    {
        public void Ordenar(string propertyName, direccionOrden Direction)
        {
            BE_ContratoComparer dc = new BE_ContratoComparer(propertyName, Direction);
            this.Sort(dc);
        }
    }

    class BE_ContratoComparer : IComparer<BE_Contrato>
    {
        string _prop = "";
        direccionOrden _dir;

        public BE_ContratoComparer(string propertyName, direccionOrden Direction)
        {
            _prop = propertyName;
            _dir = Direction;
        }

        public int Compare(BE_Contrato x, BE_Contrato y)
        {

            PropertyInfo propertyX = x.GetType().GetProperty(_prop);
            PropertyInfo propertyY = y.GetType().GetProperty(_prop);

            object px = propertyX.GetValue(x, null);
            object py = propertyY.GetValue(y, null);

            if (px == null && py == null)
            {
                return 0;
            }
            else if (px != null && py == null)
            {
                if (_dir == direccionOrden.Ascending)
                {
                    return 1;
                }
                else
                {
                    return -1;
                }
            }
            else if (px == null && py != null)
            {
                if (_dir == direccionOrden.Ascending)
                {
                    return -1;
                }
                else
                {
                    return 1;
                }
            }
            else if (px.GetType().GetInterface("IComparable") != null)
            {
                if (_dir == direccionOrden.Ascending)
                {
                    return ((IComparable)px).CompareTo(py);
                }
                else
                {
                    return ((IComparable)py).CompareTo(px);
                }
            }
            else
            {
                return 0;
            }
        }



    }

    #endregion
}
