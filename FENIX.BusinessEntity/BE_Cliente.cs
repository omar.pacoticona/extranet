﻿using System;
using System.Collections.Generic;
using System.Text;
using FENIX.Common;
using System.Reflection;

namespace FENIX.BusinessEntity
{
    [Serializable]
    public class BE_Cliente : Paginador
    {
        #region "Campos"
        int _iIdRow = 0;
        int _iIdRol = 0;
        int _IdTipoDocumento = 0;
        String _IdCliente = String.Empty;
        String _Client_vch_NumDocumento = String.Empty;
        String _Client_vch_Razon_Social = String.Empty;
        String _Client_vch_Razon_Comercial = String.Empty;
        String _Client_vch_DireccionFiscal = String.Empty;
        DateTime? _Client_dt_InicioActividades;
        String _Client_vch_EstadoSunat = String.Empty;
        String _Client_vch_CondicionSunat = String.Empty;
        String _Client_Vch_PersonaContacto = String.Empty;
        String _Client_vch_Telefono = String.Empty;
        String _Client_vch_Fax = String.Empty;
        String _Client_vch_Correo = String.Empty;
        String _Client_vch_CodigoSpring = String.Empty;
        String _Client_chr_Estado = String.Empty;
        String _Client_Chr_Estrategico = String.Empty;
        String _TipoDocumentoDescrip = String.Empty;
        String _CliRol_vch_CodigoAduana = String.Empty;
        String _CliRol_vch_CasillaTD = string.Empty;
        String _CliRol_vch_CodigoTD = String.Empty;
        String _CliRol_vch_ClaveTD = String.Empty;

        Int32? _IdBloqueoCliente;
        DateTime? _FechaBloqueo;
        String _ObservacionBloqueo;
        String _Bloqueado;
        DateTime? _FechaDesbloqueo;
        String _ObservacionDesbloqueo;
        String _BloqueadoDes;


        #endregion

        #region "Propiedades"
        public String sCodigoAduana
        {
            get { return _CliRol_vch_CodigoAduana; }
            set { _TipoDocumentoDescrip = value; }
        }
        public String sCasillaTD
        {
            get { return _CliRol_vch_CasillaTD; }
            set { _CliRol_vch_CasillaTD = value; }
        }
        public String sCodigoTD
        {
            get { return _CliRol_vch_CodigoTD; }
            set { _CliRol_vch_CodigoTD = value; }
        }

        public String sClaveTD
        {
            get { return _CliRol_vch_CodigoTD; }
            set { _CliRol_vch_CodigoTD = value; }
        }


        public int iIdRol
        {
            get { return _iIdRol; }
            set { _iIdRol = value; }
        }

        public String sTipoDocumentoDescrip
        {
            get { return _TipoDocumentoDescrip; }
            set { _TipoDocumentoDescrip = value; }
        }
        public String sEstrategico
        {
            get { return _Client_Chr_Estrategico; }
            set { _Client_Chr_Estrategico = value; }
        }
        public String sEstado
        {
            get { return _Client_chr_Estado; }
            set { _Client_chr_Estado = value; }
        }
        public String sCodigoSpring
        {
            get { return _Client_vch_CodigoSpring; }
            set { _Client_vch_CodigoSpring = value; }
        }
        public String sCorreo
        {
            get { return _Client_vch_Correo; }
            set { _Client_vch_Correo = value; }
        }
        public String sFax
        {
            get { return _Client_vch_Fax; }
            set { _Client_vch_Fax = value; }
        }
        public String sTelefono
        {
            get { return _Client_vch_Telefono; }
            set { _Client_vch_Telefono = value; }
        }
        public String sPersonaContacto
        {
            get { return _Client_Vch_PersonaContacto; }
            set { _Client_Vch_PersonaContacto = value; }
        }
        public String sCondicionSunat
        {
            get { return _Client_vch_CondicionSunat; }
            set { _Client_vch_CondicionSunat = value; }
        }
        public String sEstadoSunat
        {
            get { return _Client_vch_EstadoSunat; }
            set { _Client_vch_EstadoSunat = value; }
        }

        public DateTime? sInicioActividades
        {
            get { return _Client_dt_InicioActividades; }
            set { _Client_dt_InicioActividades = value; }
        }
        public String sDireccionFiscal
        {
            get { return _Client_vch_DireccionFiscal; }
            set { _Client_vch_DireccionFiscal = value; }
        }
        public String sRazon_Comercial
        {
            get { return _Client_vch_Razon_Comercial; }
            set { _Client_vch_Razon_Comercial = value; }
        }
        public String sRazon_Social
        {
            get { return _Client_vch_Razon_Social; }
            set { _Client_vch_Razon_Social = value; }
        }
        public String sNumDocumento
        {
            get { return _Client_vch_NumDocumento; }
            set { _Client_vch_NumDocumento = value; }
        }
        public String sIdCliente
        {
            get { return _IdCliente; }
            set { _IdCliente = value; }
        }

        public int iIdRow
        {
            get { return _iIdRow; }
            set { _iIdRow = value; }
        }
        public int iIdTipoDocumento
        {
            get { return _IdTipoDocumento; }
            set { _IdTipoDocumento = value; }
        }

        public Int32? iIdBloqueoCliente
        {
            get { return _IdBloqueoCliente; }
            set { _IdBloqueoCliente = value; }
        }

        public DateTime? dtFechaBloqueo
        {
            get { return _FechaBloqueo; }
            set { _FechaBloqueo = value; }
        }

        public String sObservacionBloqueo
        {
            get { return _ObservacionBloqueo; }
            set { _ObservacionBloqueo = value; }
        }

        public String sBloqueado
        {
            get { return _Bloqueado; }
            set { _Bloqueado = value; }
        }

        public DateTime? dtFechaDesbloqueo
        {
            get { return _FechaDesbloqueo; }
            set { _FechaDesbloqueo = value; }
        }

        public String sObservacionDesbloqueo
        {
            get { return _ObservacionDesbloqueo; }
            set { _ObservacionDesbloqueo = value; }
        }

        public String sBloqueadoDes
        {
            get { return _BloqueadoDes; }
            set { _BloqueadoDes = value; }
        }

        #endregion

    }

    [Serializable]
    public class BE_ClienteList : List<BE_Cliente>
    {
        public void Ordenar(string propertyName, direccionOrden Direction)
        {
            BE_ClienteComparer dc = new BE_ClienteComparer(propertyName, Direction);
            this.Sort(dc);
        }
    }

    class BE_ClienteComparer : IComparer<BE_Cliente>
    {
        string _prop = "";
        direccionOrden _dir;

        public BE_ClienteComparer(string propertyName, direccionOrden Direction)
        {
            _prop = propertyName;
            _dir = Direction;
        }

        public int Compare(BE_Cliente x, BE_Cliente y)
        {

            PropertyInfo propertyX = x.GetType().GetProperty(_prop);
            PropertyInfo propertyY = y.GetType().GetProperty(_prop);

            object px = propertyX.GetValue(x, null);
            object py = propertyY.GetValue(y, null);

            if (px == null && py == null)
            {
                return 0;
            }
            else if (px != null && py == null)
            {
                if (_dir == direccionOrden.Ascending)
                {
                    return 1;
                }
                else
                {
                    return -1;
                }
            }
            else if (px == null && py != null)
            {
                if (_dir == direccionOrden.Ascending)
                {
                    return -1;
                }
                else
                {
                    return 1;
                }
            }
            else if (px.GetType().GetInterface("IComparable") != null)
            {
                if (_dir == direccionOrden.Ascending)
                {
                    return ((IComparable)px).CompareTo(py);
                }
                else
                {
                    return ((IComparable)py).CompareTo(px);
                }
            }
            else
            {
                return 0;
            }
        }
    }

}

