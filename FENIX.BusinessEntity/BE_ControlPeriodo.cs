﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FENIX.Common;

namespace FENIX.BusinessEntity
{
    public class BE_ControlPeriodo: Paginador
    {
        #region "Campos"

        Int32? _iIdControl;
        Int32 _iIdTipoCierre;
        Int32 _iIdTipoProceso;
        Int32 _iAnioPeriodo;
        Int32 _iMesPeriodo;
        Int32? _iCantidadDias;
        DateTime? _dtFecha;
        String _sEstado;

        String _sTipoProceso;

        #endregion

        #region "Propiedades"

        public Int32? iiIdControl
        {
            get { return _iIdControl; }
            set { _iIdControl = value; }
        }

        public Int32 iIdTipoCierre
        {
            get { return _iIdTipoCierre; }
            set { _iIdTipoCierre = value; }
        }

        public Int32 iIdTipoProceso
        {
            get { return _iIdTipoProceso; }
            set { _iIdTipoProceso = value; }
        }

        public Int32 iAnioPeriodo
        {
            get { return _iAnioPeriodo; }
            set { _iAnioPeriodo = value; }
        }

        public Int32 iMesPeriodo
        {
            get { return _iMesPeriodo; }
            set { _iMesPeriodo = value; }
        }

        public Int32? iCantidadDias
        {
            get { return _iCantidadDias; }
            set { _iCantidadDias = value; }
        }

        public DateTime? dtFecha
        {
            get { return _dtFecha; }
            set { _dtFecha = value; }
        }

        public String sEstado
        {
            get { return _sEstado; }
            set { _sEstado = value; }
        }

        public String sTipoProceso
        {
            get { return _sTipoProceso; }
            set { _sTipoProceso = value; }
        }

        #endregion
    }
}
