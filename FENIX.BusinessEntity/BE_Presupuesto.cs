﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FENIX.BusinessEntity
{
    public class BE_Presupuesto
    {
        private Int32 _iIdPresupuesto;
        private String _sPeriodo;
        private Int32 _iIdGrupoNegocio;
        private Int32 _iIdVendedor;
        private Int32 _iIdCliente;
        private Int32 _iIdServicio;
        private Int32 _iIdGrupo;
        private Int32 _iMes1;
        private Int32 _iMes2;
        private Int32 _iMes3;
        private Int32 _iMes4;
        private Int32 _iMes5;
        private Int32 _iMes6;
        private Int32 _iMes7;
        private Int32 _iMes8;
        private Int32 _iMes9;
        private Int32 _iMes10;
        private Int32 _iMes11;
        private Int32 _iMes12;
        private String _sNegocio;
        private String _sVendedor;
        private String _sCliente;
        private String _sServicio;
        private String _sUsuario;
        private String _sNombrePC;
        private Decimal _dVentas;
        private Decimal _dVentasAnt;
        private Decimal _dPorc;
        private Decimal _dPorcAnt;
        private String _sMes;
        private Int32 _iMes;
        private Int32 _iMesIni;
        private Int32 _iMesFin;
        private Int32 _iPresupuesto;
        private Int32 _iItem;
        private Int32 _iOperacion;
        private Int32 _iLimite;

        public String sPeriodo
        {
            get { return _sPeriodo; }
            set { _sPeriodo = value; }
        }

        public Int32 iIdPresupuesto
        {
            get { return _iIdPresupuesto; }
            set { _iIdPresupuesto = value; }
        }

        public Int32 iIdLineaNegocio
        {
            get { return _iIdGrupoNegocio; }
            set { _iIdGrupoNegocio = value; }
        }

        public Int32 iIdVendedor
        {
            get { return _iIdVendedor; }
            set { _iIdVendedor = value; }
        }

        public Int32 iIdCliente
        {
            get { return _iIdCliente; }
            set { _iIdCliente = value; }
        }

        public String sNegocio
        {
            get { return _sNegocio; }
            set { _sNegocio = value; }
        }

        public String sVendedor
        {
            get { return _sVendedor; }
            set { _sVendedor = value; }
        }

        public String sCliente
        {
            get { return _sCliente; }
            set { _sCliente = value; }
        }

        public String sServicio
        {
            get { return _sServicio; }
            set { _sServicio = value; }
        }

        public Int32 iMes1
        {
            get { return _iMes1; }
            set { _iMes1 = value; }
        }

        public Int32 iMes2
        {
            get { return _iMes2; }
            set { _iMes2 = value; }
        }

        public Int32 iMes3
        {
            get { return _iMes3; }
            set { _iMes3 = value; }
        }

        public Int32 iMes4
        {
            get { return _iMes4; }
            set { _iMes4 = value; }
        }

        public Int32 iMes5
        {
            get { return _iMes5; }
            set { _iMes5 = value; }
        }

        public Int32 iMes6
        {
            get { return _iMes6; }
            set { _iMes6 = value; }
        }

        public Int32 iMes7
        {
            get { return _iMes7; }
            set { _iMes7 = value; }
        }

        public Int32 iMes8
        {
            get { return _iMes8; }
            set { _iMes8 = value; }
        }

        public Int32 iMes9
        {
            get { return _iMes9; }
            set { _iMes9 = value; }
        }

        public Int32 iMes10
        {
            get { return _iMes10; }
            set { _iMes10 = value; }
        }

        public Int32 iMes11
        {
            get { return _iMes11; }
            set { _iMes11 = value; }
        }

        public Int32 iMes12
        {
            get { return _iMes12; }
            set { _iMes12 = value; }
        }

        public String sUsuario
        {
            get { return _sUsuario; }
            set { _sUsuario = value; }
        }

        public String sNombrePC
        {
            get { return _sNombrePC; }
            set { _sNombrePC = value; }
        }

        public String sMes
        {
            get { return _sMes; }
            set { _sMes = value; }
        }

        public Decimal dVentas
        {
            get { return _dVentas; }
            set { _dVentas = value; }
        }

        public Decimal dVentasAnt
        {
            get { return _dVentasAnt; }
            set { _dVentasAnt = value; }
        }

        public Int32 iPresupuesto
        {
            get { return _iPresupuesto; }
            set { _iPresupuesto = value; }
        }

        public Decimal dPorc
        {
            get { return _dPorc; }
            set { _dPorc = value; }
        }

        public Decimal dPorcAnt
        {
            get { return _dPorcAnt; }
            set { _dPorcAnt = value; }
        }

        public Int32 iMes
        {
            get { return _iMes; }
            set { _iMes = value; }
        }

        public Int32 iMesIni
        {
            get { return _iMesIni; }
            set { _iMesIni = value; }
        }

        public Int32 iMesFin
        {
            get { return _iMesFin; }
            set { _iMesFin = value; }
        }

        public Int32 iItem
        {
            get { return _iItem; }
            set { _iItem = value; }
        }

        public Int32 iOperacion
        {
            get { return _iOperacion; }
            set { _iOperacion = value; }
        }
        public Int32 iLimite
        {
            get { return _iLimite; }
            set { _iLimite = value; }
        }
        public Int32 iIdServicio
        {
            get { return _iIdServicio; }
            set { _iIdServicio = value; }
        }
        public Int32 iIdGrupo
        {
            get { return _iIdGrupo; }
            set { _iIdGrupo = value; }
        }
    }
}
