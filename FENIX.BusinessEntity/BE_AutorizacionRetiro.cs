﻿using System;
using System.Collections.Generic;
using System.Text;
using FENIX.BusinessEntity;
using System.Reflection;

namespace FENIX.BusinessEntity
{
    [Serializable]
    public class BE_AutorizacionRetiro
    {
        int _iIdAutRet;
        String _sNumAutRet;
        DateTime _sFecha;
        String _sCliente;
        String _sDespachador;
        String _sDocumentoO;
        String _sCondicion;
        String _sSituacion;
        String _sEstado;
        String _sAgenciaAdu;
        String _Usuario;
        String _sContenedor;
        String _sVolante;
        DateTime _dtFechaDesde;
        DateTime _dtFechaHasta;
        int _iIdCliente;
        int _iIdAgencia;
        int _idAutRetDet;
        String _sChasis;
        decimal _sCantAte;
        decimal _sPesoAte;
        decimal _sVolAte;
        int _iIdDespachador;
        String _sDniDespachador;
        String _sFechaVigencia;
        String _sUsuario;
        String _sPC;
        int _iIdRegistro;
        int _iEstado;

        public int? IdSolicitudAre { get; set; }
        public int idDocOri { get; set; }
        public string sDuaAnio { get; set; }
        public int IdRegimen { get; set; }
        public string sDuaNumero { get; set; }
        public string sBanco { get; set; }
        public int IdMoneda { get; set; }
        public string sObservacion { get; set; }
        public DateTime? dtFechaLevante { get; set; }
        public int idLiquidacion { get; set; }
        public int idAduana { get; set; }

        public int idSituacion { get; set; }
        public int iTipoDocAdjuntado { get; set; }
        public string sRuta { get; set; }

        public int idSolicitudAreDet { get; set; }
        public DateTime? dtFechaRegistro { get;set;}
        public string sVolanteNumero { get; set; }
        public string sNumeroDO { get; set; }
        //public string sCliente { get; set; }
        public string sAutRetNumero { get; set; }
        public DateTime? dtAutRetFecha { get; set; }
        public DateTime? dtAutRetFechaVigencia { get; set; }
        public string sEstadoTramite { get; set; }
        public int idAutRet { get; set; }
        public int IdSolicitudAreDet { get; set; }
        public DateTime? dtFechaInicio { get; set; }
        public DateTime? dtFechaFin { get; set; }
        public int? idTipoCobro { get; set; }
        public int? idFormaPago { get; set; }
        //public int? idBanco { get; set; }
        public int? idMonedaPago { get; set; }
        public string sNumeroOperacion { get; set; }
        public string sRuc { get; set; }
        public string RepUserRegi { get; set; }

        public int RepIdAutRet { get; set; }

        public int RepIdAutRetDet { get; set; }
        public string RepNumeroDO { get; set; }
        public string RepNumeroMadre { get; set; }
        public string RepNumeroManifiesto { get; set; }
        public string RepNaveViaje { get; set; }
        public string RepNroAutRet { get; set; }
        public string RepOperacion { get; set; }
        public DateTime RepFechaFinDescarga { get; set; }
        public int RepIdConsignatario { get; set; }
        public string RepConsignatario { get; set; }
        public int RepIdAgenciaAduana { get; set; }
        public string RepAgenciaAduana { get; set; }
        public int RepIdDespachador { get; set; }
        public string RepDespachador { get; set; }
        public string RepVolante { get; set; }

        public DateTime RepFecha { get; set; }
        public DateTime RepFechaVigencia { get; set; }
        public int RepItem { get; set; }
        public string RepCondicion { get; set; }
        public string RepChasisContenedor { get; set; }
        public string RepTipoContenedor { get; set; }

        public int RepTamano { get; set; }
        public decimal RepCantidad { get; set; }
        public int RepTara { get; set; }
        public int RepPesoNeto { get; set; }
        public string RepCarga { get; set; }
        public string RepNumeroDUA { get; set; }
        public string RepTrasladoInterno { get; set; }
        public string RepObservacion { get; set; }
        public int RepBultos { get; set; }
        public string RepReferencia { get; set; }
        public DateTime RepFMaxVigencia { get; set; }
        public string sObservacionCliente { get; set; }
        public string sObservacionResp { get; set; }
        public string sDniDespachador { get; set; }
        public string sNombreDespachador { get; set; }
        public string sEstadoVigencia { get; set; }
        //public string sSituacion { get; set; }
        public int iIdDespachador { get; set; }
        //public string sDespachador { get; set; }
        //public int iIdAutRet { get; set; }
        public DateTime dFechaCreacion { get; set; }
        public DateTime dFechaHabilitado { get; set; }
        public DateTime dFechaMaxVigencia { get; set; }
        public int iTipoSolicitud { get; set; }
        public string sTipoSolicitud { get; set; }
        public decimal dMontoPago { get; set; }
        public int iNumeroOrden { get; set; }
        public string sFormaPago { get; set; }
        public string sTipoPago { get; set; }
        public string sRucCliente { get; set; }
        public decimal dTotalLiquidacion { get; set; }
        public DateTime dtFechaLiquidacion { get; set; }
        public string sNumeroDua { get; set; }
        public string sNumeroDOMaster { get; set; }
        //public string sCliente { get; set; }
        public int iIdLiquidacion { get; set; }

        public int iEstado
        {
            get
            {
                return _iEstado;
            }
            set
            {
                _iEstado = value;
            }
        }
        public int iIdCliente
        {
            get
            {
                return _iIdCliente;
            }

            set
            {
                _iIdCliente = value;
            }
        }

        public int iIdAgencia
        {
            get
            {
                return _iIdAgencia;
            }

            set
            {
                _iIdAgencia = value;
            }
        }
        public DateTime dtFechaDesde
        {
            get
            {
                return _dtFechaDesde;
            }

            set
            {
                _dtFechaDesde = value;
            }
        }
        public DateTime dtFechaHasta
        {
            get
            {
                return _dtFechaHasta;
            }

            set
            {
                _dtFechaHasta = value;
            }
        }
        public int IIdAutRet
        {
            get
            {
                return _iIdAutRet;
            }

            set
            {
                _iIdAutRet = value;
            }
        }

        public string SNumAutRet
        {
            get
            {
                return _sNumAutRet;
            }

            set
            {
                _sNumAutRet = value;
            }
        }

        public DateTime SFecha
        {
            get
            {
                return _sFecha;
            }

            set
            {
                _sFecha = value;
            }
        }

        public string SCliente
        {
            get
            {
                return _sCliente;
            }

            set
            {
                _sCliente = value;
            }
        }


        public String sUsuario
        {
            get { return _Usuario; }
            set { _Usuario = value; }
        }

        public string SDespachador
        {
            get
            {
                return _sDespachador;
            }

            set
            {
                _sDespachador = value;
            }
        }

        public string SDocumentoO
        {
            get
            {
                return _sDocumentoO;
            }

            set
            {
                _sDocumentoO = value;
            }
        }

        public string SCondicion
        {
            get
            {
                return _sCondicion;
            }

            set
            {
                _sCondicion = value;
            }
        }

        public string SSituacion
        {
            get
            {
                return _sSituacion;
            }

            set
            {
                _sSituacion = value;
            }
        }

        public string SEstado
        {
            get
            {
                return _sEstado;
            }

            set
            {
                _sEstado = value;
            }
        }

        public string SAgenciaAdu
        {
            get
            {
                return _sAgenciaAdu;
            }

            set
            {
                _sAgenciaAdu = value;
            }
        }

        public string SContenedor
        {
            get
            {
                return _sContenedor;
            }

            set
            {
                _sContenedor = value;
            }
        }

        public string SVolante
        {
            get
            {
                return _sVolante;
            }

            set
            {
                _sVolante = value;
            }
        }

        public int IdAutRetDet
        {
            get
            {
                return _idAutRetDet;
            }

            set
            {
                _idAutRetDet = value;
            }
        }

        public string SChasis
        {
            get
            {
                return _sChasis;
            }

            set
            {
                _sChasis = value;
            }
        }

        public decimal SCantAte
        {
            get
            {
                return _sCantAte;
            }

            set
            {
                _sCantAte = value;
            }
        }

        public decimal SPesoAte
        {
            get
            {
                return _sPesoAte;
            }

            set
            {
                _sPesoAte = value;
            }
        }

        public decimal SVolAte
        {
            get
            {
                return _sVolAte;
            }

            set
            {
                _sVolAte = value;
            }
        }

        public int IIdDespachador
        {
            get
            {
                return _iIdDespachador;
            }

            set
            {
                _iIdDespachador = value;
            }
        }

        public string SDniDespachador
        {
            get
            {
                return _sDniDespachador;
            }

            set
            {
                _sDniDespachador = value;
            }
        }

        public string SFechaVigencia
        {
            get
            {
                return _sFechaVigencia;
            }

            set
            {
                _sFechaVigencia = value;
            }
        }

        public string SUsuario { get => _sUsuario; set => _sUsuario = value; }
        public string SPC { get => _sPC; set => _sPC = value; }
        public int IIdRegistro { get => _iIdRegistro; set => _iIdRegistro = value; }
    }

    [Serializable]
    public class BE_AutorizacionRetiroList : List<BE_AutorizacionRetiro>
    {
        public void Ordenar(string propertyName, direccionOrden Direction)
        {
            BE_AutorizacionRetiroComparer dc = new BE_AutorizacionRetiroComparer(propertyName, Direction);
            this.Sort(dc);
        }
    }

    class BE_AutorizacionRetiroComparer : IComparer<BE_AutorizacionRetiro>
    {
        string _prop = "";
        direccionOrden _dir;

        public BE_AutorizacionRetiroComparer(string propertyName, direccionOrden Direction)
        {
            _prop = propertyName;
            _dir = Direction;
        }

        public int Compare(BE_AutorizacionRetiro x, BE_AutorizacionRetiro y)
        {

            PropertyInfo propertyX = x.GetType().GetProperty(_prop);
            PropertyInfo propertyY = y.GetType().GetProperty(_prop);

            object px = propertyX.GetValue(x, null);
            object py = propertyY.GetValue(y, null);

            if (px == null && py == null)
            {
                return 0;
            }
            else if (px != null && py == null)
            {
                if (_dir == direccionOrden.Ascending)
                {
                    return 1;
                }
                else
                {
                    return -1;
                }
            }
            else if (px == null && py != null)
            {
                if (_dir == direccionOrden.Ascending)
                {
                    return -1;
                }
                else
                {
                    return 1;
                }
            }
            else if (px.GetType().GetInterface("IComparable") != null)
            {
                if (_dir == direccionOrden.Ascending)
                {
                    return ((IComparable)px).CompareTo(py);
                }
                else
                {
                    return ((IComparable)py).CompareTo(px);
                }
            }
            else
            {
                return 0;
            }
        }

    }
}
