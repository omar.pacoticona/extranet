﻿using System;
using System.Collections.Generic;
using System.Text;
using FENIX.Common;
using System.Reflection;

namespace FENIX.BusinessEntity
{

    [Serializable]
    public class BE_Liquidacion : Paginador
    {
        #region "Campos"


        int _IdDocOriDet;
        Int32? _IdDocOri;
        int _IdOrdSer;
        int _IdOrdSerDet;
        int _iIdRolCliente;
        int? _iIdNavVia;
        string _Descripcion;
        string _Contenedor;
        string _TipoCarga;
        string _TipoContenedor;
        string _sOperacion;
        string _sClienteFacturar;
        DateTime? _dtFechaIni;
        DateTime? _dtFechaFin;
        string _sNumManifiesto;
        string _sAnnoMfto;
        string _sAgAduana;

        int _TamanoContenedor;
        int? _IdClienteFacturar;
        string _Embalaje;
        int _IdTarifa;
        int? _dIdLiq;
        decimal _MontoUnitario;
        decimal _Cantidad;
        decimal _Afecto;
        decimal _Inafecto;
        decimal _Descuento;
        decimal _dMontoNeto;
        decimal _dMontoBruto;
        decimal _Detraccion;
        decimal _Igv;
        decimal _Total;

        decimal _dPeso;
        decimal _dVolumen;

        string _Modalidad;
        string _sIdDua;
        string _Origen;
        int _Id_Registro;

        string _ClienteContrato;
        string _Cliente;
        string _NumeroDo;
        string _Nave;
        string _viaje;
        string _Rumbo;
        string _sSituacion;
        string _sObservacion;

        string _sRolCliente;
        string _sDireccion;
        string _sDireccionFac;
        int? _IdCliente;
        int? _IdAgAduana;
        decimal _dMontoTarifa;
        int? _IdContrato;

        String _NroVolante;
        DateTime? _dtFecha_Liquidacion;
        int _iIdMoneda;
        int _iIdOperacion;
        int _iIdTamanoContenedor;

        Int32? _IdVolante;
        DateTime? _dtFecha_Ingreso;
        String _sMensaje;
        String _sManifiesto;
        Int32 _iItem;
        String _sTarifa;

        String _sDniClienteFac;
        String _sDniCliente;
        String _sIdDocOriDet;
        String _sFechaAlmaFin;
        String _sFechaAlmaIni;
        String _sNroContrato;
        String _sCondCarga;
        decimal _dBultosRecibidos;
        String _sCodigoCm;
        String _sDocumentoClie;
        String _sDocumentoClieFac;
        String _sDomentoAgeAdu;
        DateTime? _dtFechaEta;
        Int32 _iIdCondCarga;
        Int32? _iIdRegistro;
        Int32? _iIdServicio;
        String _sIdDocOri;
        Int32 _Item_Servicio;
        decimal? _dTotSubTotal;
        decimal? _dTotDescuento;
        decimal? _dTotIgv;
        decimal? _dTotTotal;
        String _sTipoMoneda;
        Int32 _iDiasLibres;
        String _sRetroactivo;
        DateTime? _dtFechaRetiro;
        String _sCuenta;
        String _sPorcentDetraccion;
        Decimal _dMontoMinimoDetraccion;
        Decimal _dFactorDetraccion;
        Decimal _dTotDetraccion;
        Decimal _dMontoTipoCambio;
        String _sDocOrigen;
        String _sFechaIngreso;
        String _sEstado;
        Int32 _iIdPago;
        String _sIdSuperNegocio_2;
        Int32 _iIdSuperNegocio_2;
        Int32 _iIdAcuerdo;
        String _sMasterLCL;
        Int32 _iIdTipoServicio_2;
        Int32 _iIdAsumeEnergia_2;
        String _sOrigenPqteSLI;
        String _sMarcaSLI;
        String _sNroAcuerdo;
        Int32 _iIdTipoLiquidacion_2;
        String _sFechaCalculo;
        String _sAlmacenaje;
        String _sPeriodo;
        String _sTipoFactDeposito;
        Int32? _iVez;
        Int32? _iOficina;
        String _sMesIni;
        String _sMesFin;
        String _sLiquidacion;
        Int32 _NRegistro;
        Int32 _NPaginas;
        Int32 _NtotalRegistros;
        Int32 _iIdAgCarga;
        String _sTipoMensaje;
        String _sOrigenSolicitud;
        Int32? _iIdSolitudPreLiq;
        Int32? _iIdValidacion;
        Int32 _iTipoProceso;
        String _sValidacionContrato;
        String _sValiacionServicio;
        String _sValidacionTarifa;
        String _sValidacionAceptaUsuario;
        //decimal _dTotSubTotalDolares;
        //decimal _dTotDescuentoDolares;
        //decimal _dTotIgvDolares;
        //decimal _dTotTotalDolares;
        //decimal _dDetraccionDolares;
        //decimal _dInafectoDolares;
        //decimal _dTotSubTotalSoles;
        //decimal _dTotDescuentoSoles;
        //decimal _dTotIgvSoles;
        //decimal _dTotTotalSoles;
        //decimal _dDetraccionSoles;
        //decimal _dInafectoSoles;
        //decimal _dUnitarioDolares;
        //decimal _dUnitarioSoles;
        decimal _dAfectoGrabar;
        decimal _dInafectoGrabar;
        decimal _ddetraccionGrabar;
        decimal _dDescuentoGrabar;
        decimal _dIgvGrabar;
        decimal _dMontoTotalGrabar;
        decimal _dUnitarioGrabar;
        Int32 _idMonedaGrabar;
        Int32 _idMonedaVisual;
        decimal _dMontoTotalVisual;
        decimal _dUnitarioVisual;
        Int32 _idMonedaGrabar2;

        decimal _dAfectoGrabar2;
        decimal _dInafectoGrabar2;
        decimal _ddetraccionGrabar2;
        decimal _dDescuentoGrabar2;
        decimal _dIgvGrabar2;
        decimal _dMontoTotalGrabar2;
        decimal _dUnitarioGrabar2;
        decimal  _dMontoNetoGrabar2;
        decimal _dMontoBrutoGrabar2;
       


        #endregion


        #region "Propiedades"

        public Int32 idMonedaGrabar
        {
            get { return _idMonedaGrabar; }
            set { _idMonedaGrabar = value; }
        }
        public decimal dMontoBrutoGrabar2
        {
            get { return _dMontoBrutoGrabar2; }
            set { _dMontoBrutoGrabar2 = value; }
        }

        public decimal dMontoNetoGrabar2
        {
            get { return _dMontoNetoGrabar2; }
            set { _dMontoNetoGrabar2 = value; }
        }

        public decimal dUnitarioGrabar2
        {
            get { return _dUnitarioGrabar2; }
            set { _dUnitarioGrabar2 = value; }
        }

        public decimal dMontoTotalGrabar2
        {
            get { return _dMontoTotalGrabar2; }
            set { _dMontoTotalGrabar2 = value; }
        }

        public decimal dIgvGrabar2
        {
            get { return _dIgvGrabar2; }
            set { _dIgvGrabar2 = value; }
        }

        public decimal dDescuentoGrabar2
        {
            get { return _dDescuentoGrabar2; }
            set { _dDescuentoGrabar2 = value; }
        }

        public decimal ddetraccionGrabar2
        {
            get { return _ddetraccionGrabar2; }
            set { _ddetraccionGrabar2 = value; }
        }

        public decimal dInafectoGrabar2
        {
            get { return _dInafectoGrabar2; }
            set { _dInafectoGrabar2 = value; }
        }

        public decimal dAfectoGrabar2
        {
            get { return _dAfectoGrabar2; }
            set { _dAfectoGrabar2 = value; }
        }

        public Int32 idMonedaGrabar2
        {
            get { return _idMonedaGrabar2; }
            set { _idMonedaGrabar2 = value; }
        }

        public decimal dMontoTotalVisual
        {
            get { return _dMontoTotalVisual; }
            set { _dMontoTotalVisual = value; }
        }

        public decimal dUnitarioVisual
        {
            get { return _dUnitarioVisual; }
            set { _dUnitarioVisual = value; }
        }

        public Int32 idMonedaVisual
        {
            get { return _idMonedaVisual; }
            set { _idMonedaVisual = value; }
        }


        public decimal dAfectoGrabar
        {
            get { return _dAfectoGrabar; }
            set { _dAfectoGrabar = value; }
        }

        public decimal dUnitarioGrabar
        {
            get { return _dUnitarioGrabar; }
            set { _dUnitarioGrabar = value; }
        }


        public decimal ddetraccionGrabar
        {
            get { return _ddetraccionGrabar; }
            set { _ddetraccionGrabar = value; }
        }

        public decimal dInafectoGrabar
        {
            get { return _dInafectoGrabar; }
            set { _dInafectoGrabar = value; }
        }


        public decimal dDescuentoGrabar
        {
            get { return _dDescuentoGrabar; }
            set { _dDescuentoGrabar = value; }
        }

        public decimal dIgvGrabar
        {
            get { return _dIgvGrabar; }
            set { _dIgvGrabar = value; }
        }

        public decimal dMontoTotalGrabar
        {
            get { return _dMontoTotalGrabar; }
            set { _dMontoTotalGrabar = value; }
        }

        //public decimal dUnitarioSoles
        //{
        //    get { return _dUnitarioSoles; }
        //    set { _dUnitarioSoles = value; }
        //}


        //public decimal dUnitarioDolares
        //{
        //    get { return _dUnitarioDolares; }
        //    set { _dUnitarioDolares = value; }
        //}
        //public decimal dDetraccionSoles
        //{
        //    get { return _dDetraccionSoles; }
        //    set { _dDetraccionSoles = value; }
        //}

        //public decimal dTotTotalSoles
        //{
        //    get { return _dTotTotalSoles; }
        //    set { _dTotTotalSoles = value; }
        //}
        //public decimal dTotIgvSoles
        //{
        //    get { return _dTotIgvSoles; }
        //    set { _dTotIgvSoles = value; }
        //}

        //public decimal dTotDescuentoSoles
        //{
        //    get { return _dTotDescuentoSoles; }
        //    set { _dTotDescuentoSoles = value; }
        //}

        //public decimal dTotSubTotalSoles
        //{
        //    get { return _dTotSubTotalSoles; }
        //    set { _dTotSubTotalSoles = value; }
        //}

        public String sValidacionAceptaUsuario
        {
            get { return _sValidacionAceptaUsuario; }
            set { _sValidacionAceptaUsuario = value; }
        }

        //public decimal dInafectoDolares
        //{
        //    get { return _dInafectoDolares; }
        //    set { _dInafectoDolares = value; }
        //}

        //public decimal dTotSubTotalDolares
        //{
        //    get { return _dTotSubTotalDolares; }
        //    set { _dTotSubTotalDolares = value; }
        //}
        //public decimal dDetraccionDolares
        //{
        //    get { return _dDetraccionDolares; }
        //    set { _dDetraccionDolares = value; }
        //}
        //public decimal dTotTotalDolares
        //{
        //    get { return _dTotTotalDolares; }
        //    set { _dTotTotalDolares = value; }
        //}
        //public decimal dTotIgvDolares
        //{
        //    get { return _dTotIgvDolares; }
        //    set { _dTotIgvDolares = value; }
        //}
        //public decimal dTotDescuentoDolares
        //{
        //    get { return _dTotDescuentoDolares; }
        //    set { _dTotDescuentoDolares = value; }
        //}


        public String sValidacionTarifa
        {
            get { return _sValidacionTarifa; }
            set { _sValidacionTarifa = value; }
        }

        public String   sValidacionContrato
        {
            get { return _sValidacionContrato; }
            set { _sValidacionContrato = value; }
        }

        public String sValiacionServicio
        {
            get { return _sValiacionServicio; }
            set { _sValiacionServicio = value; }
        }

        public Int32? iIdServicio
        {
            get { return _iIdServicio; }
            set { _iIdServicio = value; }
        }

        public Int32 iTipoProceso
        {
            get { return _iTipoProceso; }
            set { _iTipoProceso = value; }
        }

        public Int32? iIdValidacion
        {
            get { return _iIdValidacion; }
            set { _iIdValidacion = value; }
        }

        public Int32? iIdSolitudPreLiq
        {
            get { return _iIdSolitudPreLiq; }
            set { _iIdSolitudPreLiq = value; }
        }

        public String sOrigenSolicitud
        {
            get { return _sOrigenSolicitud; }
            set { _sOrigenSolicitud = value; }
        }
        public String sTipoMensaje
        {
            get { return _sTipoMensaje; }
            set { _sTipoMensaje = value; }
        }
        public Int32 iIdAgCarga
        {
            get { return _iIdAgCarga; }
            set { _iIdAgCarga = value; }
        }
        public Int32 NtotalRegistros
        {
            get { return _NtotalRegistros; }
            set { _NtotalRegistros = value; }
        }
        public Int32 NRegistro
        {
            get { return _NRegistro; }
            set { _NRegistro = value; }
        }
        public Int32 NPaginastro
        {
            get { return _NPaginas; }
            set { _NPaginas = value; }
        }
        public String sLiquidacion
        {
            get { return _sLiquidacion; }
            set { _sLiquidacion = value; }
        }

        public String sMesIni
        {
            get { return _sMesIni; }
            set { _sMesIni = value; }
        }
        public String sMesFin
        {
            get { return _sMesFin; }
            set { _sMesFin = value; }
        }

        public Int32? iOficina
        {
            get { return _iOficina; }
            set { _iOficina = value; }
        }

        public String sPeriodo
        {
            get { return _sPeriodo; }
            set { _sPeriodo = value; }
        }
        public Int32? iVez
        {
            get { return _iVez; }
            set { _iVez = value; }
        }
        public String sTipoFactDeposito
        {
            get { return _sTipoFactDeposito; }
            set { _sTipoFactDeposito = value; }
        }

        public String sAlmacenaje
        {
            get { return _sAlmacenaje; }
            set { _sAlmacenaje = value; }
        }

        public String sFechaCalculo
        {
            get { return _sFechaCalculo; }
            set { _sFechaCalculo = value; }
        }

        public Int32 iIdTipoLiquidacion_2
        {
            get { return _iIdTipoLiquidacion_2; }
            set { _iIdTipoLiquidacion_2 = value; }
        }


        public String sNroAcuerdo
        {
            get { return _sNroAcuerdo; }
            set { _sNroAcuerdo = value; }
        }

        public String sMarcaSLI
        {
            get { return _sMarcaSLI; }
            set { _sMarcaSLI = value; }
        }
        public String sOrigenPqteSLI
        {
            get { return _sOrigenPqteSLI; }
            set { _sOrigenPqteSLI = value; }
        }
        public Int32 iIdAsumeEnergia_2
        {
            get { return _iIdAsumeEnergia_2; }
            set { _iIdAsumeEnergia_2 = value; }
        }
        public Int32 iIdTipoServicio_2
        {
            get { return _iIdTipoServicio_2; }
            set { _iIdTipoServicio_2 = value; }
        }
        public String sMasterLCL
        {
            get { return _sMasterLCL; }
            set { _sMasterLCL = value; }
        }
        public String sEstado
        {
            get { return _sEstado; }
            set { _sEstado = value; }
        }
        public Int32 iIdPago
        {
            get { return _iIdPago; }
            set { _iIdPago = value; }
        }
        public String sIdSuperNegocio_2
        {
            get { return _sIdSuperNegocio_2; }
        set { _sIdSuperNegocio_2 = value; }
        }

        public Int32 iIdAcuerdo
        {
            get { return _iIdAcuerdo; }
            set { _iIdAcuerdo = value; }
        }

        public Int32 iIdSuperNegocio_2
        {
            get { return _iIdSuperNegocio_2; }
            set { _iIdSuperNegocio_2 = value; }
        }

        public String sFechaIngreso
        {
            get { return _sFechaIngreso; }
            set { _sFechaIngreso = value; }
        }
        public String sDocOrigen
        {
            get { return _sDocOrigen; }
            set { _sDocOrigen = value; }
        }
        
        public Decimal dMontoTipoCambio
        {
            get { return _dMontoTipoCambio; }
            set { _dMontoTipoCambio = value; }
        }
        public Decimal dTotDetraccion
        {
            get { return _dTotDetraccion; }
            set { _dTotDetraccion = value; }
        }
        public Decimal dFactorDetraccion
        {
            get { return _dFactorDetraccion; }
            set { _dFactorDetraccion = value; }
        }
        public Decimal dMontoMinimoDetraccion
        {
            get { return _dMontoMinimoDetraccion; }
            set { _dMontoMinimoDetraccion = value; }
        }
        public String sPorcentDetraccion
        {
            get { return _sPorcentDetraccion; }
            set { _sPorcentDetraccion = value; }
        }
        public String sCuenta
        {
            get { return _sCuenta; }
            set { _sCuenta = value; }
        }
        public DateTime? dtFechaRetiro
        {
            get { return _dtFechaRetiro; }
            set { _dtFechaRetiro = value; }
        }
        public String sRetroactivo
        {
            get { return _sRetroactivo; }
            set { _sRetroactivo = value; }
        }

        public Int32 iDiasLibres
        {
            get { return _iDiasLibres; }
            set { _iDiasLibres = value; }
        }

        public String sTipoMoneda
        {
            get { return _sTipoMoneda; }
            set { _sTipoMoneda = value; }
        }

        public Int32 iItem_Servicio
        {
            get { return _Item_Servicio; }
            set { _Item_Servicio = value; }
        }

        public decimal? dTotTotal
        {
            get { return _dTotTotal; }
            set { _dTotTotal = value; }
        }


        public decimal? dTotIgv
        {
            get { return _dTotIgv; }
            set { _dTotIgv = value; }
        }


        public decimal? dTotDescuento
        {
            get { return _dTotDescuento; }
            set { _dTotDescuento = value; }
        }


        public decimal? dTotSubTotal
        {
            get { return _dTotSubTotal; }
            set { _dTotSubTotal = value; }
        }



        public Int32? iIdRegistro
        {
            get { return _iIdRegistro; }
            set { _iIdRegistro = value; }
        }

        public String sIdDocOri
        {
            get { return _sIdDocOri; }
            set { _sIdDocOri = value; }
        }

        public Int32 iIdCondCarga
        {
            get { return _iIdCondCarga; }
            set { _iIdCondCarga = value; }
        }

        public String sDomentoAgeAdu
        {
            get { return _sDomentoAgeAdu; }
            set { _sDomentoAgeAdu = value; }
        }
        public String sDocumentoClieFac
        {
            get { return _sDocumentoClieFac; }
            set { _sDocumentoClieFac = value; }
        }
        public String sDocumentoClie
        {
            get { return _sDocumentoClie; }
            set { _sDocumentoClie = value; }
        }

        public DateTime? dtFechaEta
        {
            get { return _dtFechaEta; }
            set { _dtFechaEta = value; }
        }


        public String sCodigoCm
        {
            get { return _sCodigoCm; }
            set { _sCodigoCm = value; }
        }

        public Decimal dBultosRecibidos
        {
            get { return _dBultosRecibidos; }
            set { _dBultosRecibidos = value; }
        }

        public String sCondCarga
        {
            get { return _sCondCarga; }
            set { _sCondCarga = value; }
        }

        public String sNroContrato
        {
            get { return _sNroContrato; }
            set { _sNroContrato = value; }
        }

        public String sFechaAlmaFin
        {
            get { return _sFechaAlmaFin; }
            set { _sFechaAlmaFin = value; }
        }

        public String sFechaAlmaIni
        {
            get { return _sFechaAlmaIni; }
            set { _sFechaAlmaIni = value; }
        }
        public String sIdDocOriDet
        {
            get { return _sIdDocOriDet; }
            set { _sIdDocOriDet = value; }
        }

        public String sDniCliente
        {
            get { return _sDniCliente; }
            set { _sDniCliente = value; }
        }
        public String sDniClienteFac
        {
            get { return _sDniClienteFac; }
            set { _sDniClienteFac = value; }
        }
        public String sTarifa
        {
            get { return _sTarifa; }
            set { _sTarifa = value; }
        }
        public Int32 iItem
        {
            get { return _iItem; }
            set { _iItem = value; }
        }


        public DateTime? dtFecha_Ingreso
        {
            get { return _dtFecha_Ingreso; }
            set { _dtFecha_Ingreso = value; }
        }

        public String sMensaje
        {
            get { return _sMensaje; }
            set { _sMensaje = value; }
        }
        public String sManifiesto
        {
            get { return _sManifiesto; }
            set { _sManifiesto = value; }
        }

        public Int32? iIdNavVia
        {
            get { return _iIdNavVia; }
            set { _iIdNavVia = value; }
        }


        public Int32 iIdRolCliente
        {
            get { return _iIdRolCliente; }
            set { _iIdRolCliente = value; }
        }
        public Int32? iIdAgAduana
        {
            get { return _IdAgAduana; }
            set { _IdAgAduana = value; }
        }
        public Int32? iIdCliente
        {
            get { return _IdCliente; }
            set { _IdCliente = value; }
        }

        public String sOperacion
        {
            get { return _sOperacion; }
            set { _sOperacion = value; }
        }
        public String sDireccion
        {
            get { return _sDireccion; }
            set { _sDireccion = value; }
        }
        public String sDireccionFac
        {
            get { return _sDireccionFac; }
            set { _sDireccionFac = value; }
        }
        public String sRolCliente
        {
            get { return _sRolCliente; }
            set { _sRolCliente = value; }
        }

        public String sIdDua
        {
            get { return _sIdDua; }
            set { _sIdDua = value; }
        }

        public String sAgAduana
        {
            get { return _sAgAduana; }
            set { _sAgAduana = value; }
        }


        public String sAnnoMfto
        {
            get { return _sAnnoMfto; }
            set { _sAnnoMfto = value; }
        }


        public String sNumManifiesto
        {
            get { return _sNumManifiesto; }
            set { _sNumManifiesto = value; }
        }


        public DateTime? dtFechaFin
        {
            get { return _dtFechaFin; }
            set { _dtFechaFin = value; }
        }

        public DateTime? dtFechaIni
        {
            get { return _dtFechaIni; }
            set { _dtFechaIni = value; }
        }

        public String sClienteFacturar
        {
            get { return _sClienteFacturar; }
            set { _sClienteFacturar = value; }
        }

        public decimal dPeso
        {
            get { return _dPeso; }
            set { _dPeso = value; }
        }
        public int? iIdLiq
        {
            get { return _dIdLiq; }
            set { _dIdLiq = value; }
        }

        public decimal dVolumen
        {
            get { return _dVolumen; }
            set { _dVolumen = value; }
        }


        public decimal dMontoBruto
        {
            get { return _dMontoBruto; }
            set { _dMontoBruto = value; }
        }

        public decimal dMontoNeto
        {
            get { return _dMontoNeto; }
            set { _dMontoNeto = value; }
        }

        public int? iIdClienteFacturar
        {
            get { return _IdClienteFacturar; }
            set { _IdClienteFacturar = value; }
        }

        public String sObservacion
        {
            get { return _sObservacion; }
            set { _sObservacion = value; }
        }
        public String sSituacion
        {
            get { return _sSituacion; }
            set { _sSituacion = value; }
        }

        public int iIdTamanoContenedor
        {
            get { return _iIdTamanoContenedor; }
            set { _iIdTamanoContenedor = value; }
        }
        public int iIdOperacion
        {
            get { return _iIdOperacion; }
            set { _iIdOperacion = value; }
        }
        public int iIdMoneda
        {
            get { return _iIdMoneda; }
            set { _iIdMoneda = value; }
        }

        public DateTime? dtFecha_Liquidacion
        {
            get { return _dtFecha_Liquidacion; }
            set { _dtFecha_Liquidacion = value; }
        }
        public String sNroVolante
        {
            get { return _NroVolante; }
            set { _NroVolante = value; }
        }
        public int? iIdContrato
        {
            get { return _IdContrato; }
            set { _IdContrato = value; }
        }
        public String sRumbo
        {
            get { return _Rumbo; }
            set { _Rumbo = value; }
        }
        public String sviaje
        {
            get { return _viaje; }
            set { _viaje = value; }
        }
        public String sNave
        {
            get { return _Nave; }
            set { _Nave = value; }
        }
        public String sNumeroDo
        {
            get { return _NumeroDo; }
            set { _NumeroDo = value; }
        }
        public String sCliente
        {
            get { return _Cliente; }
            set { _Cliente = value; }
        }
        public String sClienteContrato
        {
            get { return _ClienteContrato; }
            set { _ClienteContrato = value; }
        }
        public int iIdDocOriDet
        {
            get { return _IdDocOriDet; }
            set { _IdDocOriDet = value; }
        }
        public Int32? iIdDocOri
        {
            get { return _IdDocOri; }
            set { _IdDocOri = value; }
        }
        public int iIdOrdSer
        {
            get { return _IdOrdSer; }
            set { _IdOrdSer = value; }
        }
        public int iIdOrdSerDet
        {
            get { return _IdOrdSerDet; }
            set { _IdOrdSerDet = value; }
        }
        public string sDescripcionServicio
        {
            get { return _Descripcion; }
            set { _Descripcion = value; }
        }
        public string sContenedor
        {
            get { return _Contenedor; }
            set { _Contenedor = value; }
        }
        public string sTipoCarga
        {
            get { return _TipoCarga; }
            set { _TipoCarga = value; }
        }
        public string sTipoContenedor
        {
            get { return _TipoContenedor; }
            set { _TipoContenedor = value; }
        }
        public int iTamanoContenedor
        {
            get { return _TamanoContenedor; }
            set { _TamanoContenedor = value; }
        }
        public string sEmbalaje
        {
            get { return _Embalaje; }
            set { _Embalaje = value; }
        }
        public int iIdTarifa
        {
            get { return _IdTarifa; }
            set { _IdTarifa = value; }
        }
        public decimal dMontoUnitario
        {
            get { return _MontoUnitario; }
            set { _MontoUnitario = value; }
        }
        public decimal dCantidad
        {
            get { return _Cantidad; }
            set { _Cantidad = value; }
        }
        public decimal dAfecto
        {
            get { return _Afecto; }
            set { _Afecto = value; }
        }
        public decimal dInafecto
        {
            get { return _Inafecto; }
            set { _Inafecto = value; }
        }
        public decimal dDescuento
        {
            get { return _Descuento; }
            set { _Descuento = value; }
        }
        public decimal dDetraccion
        {
            get { return _Detraccion; }
            set { _Detraccion = value; }
        }
        public decimal dMontoTarifa
        {
            get { return _dMontoTarifa; }
            set { _dMontoTarifa = value; }
        }


        public decimal dIgv
        {
            get { return _Igv; }
            set { _Igv = value; }
        }

        public decimal dTotal
        {
            get { return _Total; }
            set { _Total = value; }
        }
        public string sModalidad
        {
            get { return _Modalidad; }
            set { _Modalidad = value; }
        }
        public string sOrigen
        {
            get { return _Origen; }
            set { _Origen = value; }
        }

        public Int32? iIdVolante
        {
            get { return _IdVolante; }
            set { _IdVolante = value; }
        }


        #endregion
    }
    #region Metodos
    [Serializable]
    public class BE_LiquidacionList : List<BE_Liquidacion>
    {
        public void Ordenar(string propertyName, direccionOrden Direction)
        {
            BE_LiquidacionConparer dc = new BE_LiquidacionConparer(propertyName, Direction);
            this.Sort(dc);
        }
    }

    class BE_LiquidacionConparer : IComparer<BE_Liquidacion>
    {
        string _prop = "";
        direccionOrden _dir;

        public BE_LiquidacionConparer(string propertyName, direccionOrden Direction)
        {
            _prop = propertyName;
            _dir = Direction;
        }

        public int Compare(BE_Liquidacion x, BE_Liquidacion y)
        {

            PropertyInfo propertyX = x.GetType().GetProperty(_prop);
            PropertyInfo propertyY = y.GetType().GetProperty(_prop);

            object px = propertyX.GetValue(x, null);
            object py = propertyY.GetValue(y, null);

            if (px == null && py == null)
            {
                return 0;
            }
            else if (px != null && py == null)
            {
                if (_dir == direccionOrden.Ascending)
                {
                    return 1;
                }
                else
                {
                    return -1;
                }
            }
            else if (px == null && py != null)
            {
                if (_dir == direccionOrden.Ascending)
                {
                    return -1;
                }
                else
                {
                    return 1;
                }
            }
            else if (px.GetType().GetInterface("IComparable") != null)
            {
                if (_dir == direccionOrden.Ascending)
                {
                    return ((IComparable)px).CompareTo(py);
                }
                else
                {
                    return ((IComparable)py).CompareTo(px);
                }
            }
            else
            {
                return 0;
            }
        }



    }

    #endregion
}
