﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FENIX.BusinessEntity;
using System.Reflection;

namespace FENIX.BusinessEntity
{
    [Serializable]
    public class BE_DocumentoPago
    {
        Int32 _iIdDocPago;
        String _sSerie;
        String _sNumero;
        String _sMoneda;
        Decimal _dSubtotal;
        Decimal _dIGV;
        Decimal _dTotal;

        Int32 _iIdVolante;
        String _sNroVolante;
        String _sNroDocumentoOrigen;
        String _sNroContenedor;
        String _sNegocio;
        String _sFechaIni;
        String _sFechaFin;
        Int32 _mesFactura;
        Int32 _anioFactura;

        DateTime _sFechaEmision;
        String _sCliente;
        Int32 _idAgenciaAduana;
        String _sAgenciaAduana;
        String _sUsuario;
        String _sPC;
        Int32 _iEstado;


        public String sTipoOperacion{ get; set; }
        public Int32 iIdTipoDocumentoPago { get; set; }
        public String sTipoDocumentoPago { get; set; }
        Int32 _nPagina;
        Int32 _nRegistros;
        Int32 _totalRegistros;

        public Int32 NPagina
        {
            get
            {
                return _nPagina;
            }
            set
            {
                _nPagina = value;
            }
        }

        public Int32 NRegistros
        {
            get
            {
                return _nRegistros;
            }
            set
            {
                _nRegistros = value;
            }
        }
        public Int32 NtotalRegistros
        {
            get
            {
                return _totalRegistros;
            }
            set
            {
                _totalRegistros = value;
            }
        }

        public Int32 iIdDocPago
        {
            get { return _iIdDocPago; }
            set { _iIdDocPago = value; }
        }

        public Int32 mesFactura
        {
            get { return _mesFactura; }
            set { _mesFactura = value; }
        }

        public Int32 anioFactura
        {
            get { return _anioFactura; }
            set { _anioFactura = value; }
        }
        public String sSerie
        {
            get { return _sSerie; }
            set { _sSerie = value; }
        }
        public String sNumero
        {
            get { return _sNumero; }
            set { _sNumero = value; }
        }

        public String sNegocio
        {
            get { return _sNegocio; }
            set { _sNegocio = value; }
        }

        public String sMoneda
        {
            get { return _sMoneda; }
            set { _sMoneda = value; }
        } 

        public Decimal dSubtotal
        {
            get { return _dSubtotal; }
            set { _dSubtotal = value; }
        }
        public Decimal dIGV
        {
            get { return _dIGV; }
            set { _dIGV = value; }
        }
        public Decimal dTotal
        {
            get { return _dTotal; }
            set { _dTotal = value; }
        }
        public Int32 iIdVolante
        {
            get { return _iIdVolante; }
            set { _iIdVolante = value; }
        }

        public String sNroVolante
        {
            get { return _sNroVolante; }
            set { _sNroVolante = value; }
        }
        public String sNroDocumentoOrigen
        {
            get { return _sNroDocumentoOrigen; }
            set { _sNroDocumentoOrigen = value; }
        }

        public String sNroContenedor
        {
            get { return _sNroContenedor; }
            set { _sNroContenedor = value; }
        }

        public String sFechaIni
        {
            get { return _sFechaIni; }
            set { _sFechaIni = value; }
        }
        public String sFechaFin
        {
            get { return _sFechaFin; }
            set { _sFechaFin = value; }
        }
        public DateTime sFechaEmision
        {
            get { return _sFechaEmision; }
            set { _sFechaEmision = value; }
        }

        public String sCliente
        {
            get { return _sCliente; }
            set { _sCliente = value; }
        }

        public Int32 idAgenciaAduana
        {
            get { return _idAgenciaAduana; }
            set { _idAgenciaAduana = value; }
        }
        public String sAgenciaAduana
        {
            get { return _sAgenciaAduana; }
            set { _sAgenciaAduana = value; }
        }

        public String sUsuario
        {
            get { return _sUsuario; }
            set { _sUsuario = value; }
        }

        public String sPC
        {
            get { return _sPC; }
            set { _sPC = value; }
        }

        public Int32 iEstado
        {
            get { return _iEstado; }
            set { _iEstado = value; }
        }
      
    }
    
    [Serializable]
    public class BE_DocumentoPagoList : List<BE_DocumentoPago>
    {
        public void Ordenar(string propertyName, direccionOrden Direction)
        {
            BE_DocumentoPagoComparer dc = new BE_DocumentoPagoComparer(propertyName, Direction);
            this.Sort(dc);
        }
    }
    class BE_DocumentoPagoComparer : IComparer<BE_DocumentoPago>
    {
        string _prop = "";
        direccionOrden _dir;

        public BE_DocumentoPagoComparer(string propertyName, direccionOrden Direction)
        {
            _prop = propertyName;
            _dir = Direction;
        }

        public int Compare(BE_DocumentoPago x, BE_DocumentoPago y)
        {

            PropertyInfo propertyX = x.GetType().GetProperty(_prop);
            PropertyInfo propertyY = y.GetType().GetProperty(_prop);

            object px = propertyX.GetValue(x, null);
            object py = propertyY.GetValue(y, null);

            if (px == null && py == null)
            {
                return 0;
            }
            else if (px != null && py == null)
            {
                if (_dir == direccionOrden.Ascending)
                {
                    return 1;
                }
                else
                {
                    return -1;
                }
            }
            else if (px == null && py != null)
            {
                if (_dir == direccionOrden.Ascending)
                {
                    return -1;
                }
                else
                {
                    return 1;
                }
            }
            else if (px.GetType().GetInterface("IComparable") != null)
            {
                if (_dir == direccionOrden.Ascending)
                {
                    return ((IComparable)px).CompareTo(py);
                }
                else
                {
                    return ((IComparable)py).CompareTo(px);
                }
            }
            else
            {
                return 0;
            }
        }

    }



    [Serializable]
    public class BE_LineaNegocio
    {
        public Int32 idLinea { get; set; }
        public String LineaNegocio { get; set; }
    }

    public class BE_TipoDocumentoPago
    {
        public Int32 iIdTipoDocumentoPago { get; set; }
        public String sTipoDocumentoPago { get; set; }
    }
}
