﻿using System;
using System.Collections.Generic;
using System.Text;
using FENIX.Common;
using System.Reflection;

namespace FENIX.BusinessEntity
{
    [Serializable]
    public class BE_Tracking : Paginador
    {
        #region "Campos"
            int? _IdRol;
            int? _IdCliente;
            int? _IdDocOriDet;
            int? _IdDocOri;
            int? _TicketDescarga = null;
            int? _PesoDescarga = null;
            int? _Volante = null;
            int? _IdVolante = null;
            int? _AutRetiro = null;
            int? _TicketDespacho = null;
            int? _PesoDespacho = null;
            int? _Codigo = null;
            int? _BultoManif = null;
            int? _PesoManif = null;
            int? _BultoRecib = null;
            int? _PesoRecib = null;
        
            String _DocumentoOrigen = null;
            String _Contenedor = null;
            String _Manifiesto = null;
            String _TerminoDescarga = null;
            String _Nave = null;
            String _Viaje = null;
            String _IngresoDescarga = null;
            String _SalidaDescarga = null;
            String _FechaVolante = null;
            String _AgenciaAduana = null;
            String _FechaRetiro = null;
            String _Despachador = null;
            String _IngresoDespacho = null;
            String _SalidaDespacho = null;
            String _Descripcion = null;
            String _AnoManifiesto = null;
            String _NumManifiesto = null;
            String _Condicion = null;
            String _TipoCtn = null;
            String _ClienteFinal = null;
            String _Precintos = null;
            String _Situacion = null;
            String _Consignatario = null;
            String _DniDespachador = null;

        public Int32? iIdMovBalIngreso { get; set; }
        public Int32? iIdMovBalSalida { get; set; }
        public Int32? iIdEirIngreso { get; set; }
        public Int32? iIdEirSalida { get; set; }
        #endregion



        #region "Propiedades"
            public int? iIdRol
            {
                get { return _IdRol; }
                set { _IdRol = value; }
            }
            public int? iIdCliente
            {
                get { return _IdCliente; }
                set { _IdCliente = value; }
            }
            public int? iIdDocOriDet
            {
                get { return _IdDocOriDet; }
                set { _IdDocOriDet = value; }
            }

            public int? iIdDocOri
            {
                get { return _IdDocOri; }
                set { _IdDocOri = value; }
            }
            public int? iTicketDescarga
            {
                get { return _TicketDescarga; }
                set { _TicketDescarga = value; }
            }
            public int? iPesoDescarga
            {
                get { return _PesoDescarga; }
                set { _PesoDescarga = value; }
            }
            public int? iVolante
            {
                get { return _Volante; }
                set { _Volante = value; }
            }
            public int? iIdVolante
            {
                get { return _IdVolante; }
                set { _IdVolante = value; }
            }
            public int? iAutRetiro
            {
                get { return _AutRetiro; }
                set { _AutRetiro = value; }
            }
            public int? iTicketDespacho
            {
                get { return _TicketDespacho; }
                set { _TicketDespacho = value; }
            }
            public int? iPesoDespacho
            {
                get { return _PesoDespacho; }
                set { _PesoDespacho = value; }
            }
            public int? iCodigo
            {
                get { return _Codigo; }
                set { _Codigo = value; }
            }
            public int? dBultoManif
            {
                get { return _BultoManif; }
                set { _BultoManif = value; }
            }
            public int? dPesoManif
            {
                get { return _PesoManif; }
                set { _PesoManif = value; }
            }
            public int? dBultoRecib
            {
                get { return _BultoRecib; }
                set { _BultoRecib = value; }
            }
            public int? dPesoRecib
            {
                get { return _PesoRecib; }
                set { _PesoRecib = value; }
            }

            public String sDocumentoOrigen
            {
                get { return _DocumentoOrigen; }
                set { _DocumentoOrigen = value; }
            }
            public String sContenedor 
            {
                get { return _Contenedor; }
                set { _Contenedor = value; }
            }
            public String sManifiesto 
            {
                get { return _Manifiesto; }
                set { _Manifiesto = value; }
            }
            public String sTerminoDescarga 
            {
                get { return _TerminoDescarga; }
                set { _TerminoDescarga = value; }
            }
            public String sNave 
            {
                get { return _Nave; }
                set { _Nave = value; }
            }
            public String sViaje 
            {
                get { return _Viaje; }
                set { _Viaje = value; }
            }
            public String sIngresoDescarga 
            {
                get { return _IngresoDescarga; }
                set { _IngresoDescarga = value; }
            }
            public String sSalidaDescarga 
            {
                get { return _SalidaDescarga; }
                set { _SalidaDescarga = value; }
            }
            public String sFechaVolante 
            {
                get { return _FechaVolante; }
                set { _FechaVolante = value; }
            }
            public String sAgenciaAduana 
            {
                get { return _AgenciaAduana; }
                set { _AgenciaAduana = value; }
            }
            public String sFechaRetiro 
            {
                get { return _FechaRetiro; }
                set { _FechaRetiro = value; }
            }
            public String sDespachador 
            {
                get { return _Despachador; }
                set { _Despachador = value; }
            }
            public String sDNIDespachador
            {
                get { return _DniDespachador; }
                set { _DniDespachador = value; }
            }
            public String sIngresoDespacho 
            {
                get { return _IngresoDespacho; }
                set { _IngresoDespacho = value; }
            }
            public String sSalidaDespacho 
            {
                get { return _SalidaDespacho; }
                set { _SalidaDespacho = value; }
            }
            public String sDescripcion
            {
                get { return _Descripcion; }
                set { _Descripcion = value; }
            }
            public String sAnoManifiesto
            {
                get { return _AnoManifiesto; }
                set { _AnoManifiesto = value; }
            }
            public String sNumManifiesto
            {
                get { return _NumManifiesto; }
                set { _NumManifiesto = value; }
            }
            public String sCondicion
            {
                get { return _Condicion; }
                set { _Condicion = value; }
            }
            public String sTipoCtn
            {
                get { return _TipoCtn; }
                set { _TipoCtn = value; }
            }
            public String sClienteFinal
            {
                get { return _ClienteFinal; }
                set { _ClienteFinal = value; }
            }
            public String sPrecintos
            {
                get { return _Precintos; }
                set { _Precintos = value; }
            }
            public String sSituacion
            {
                get { return _Situacion; }
                set { _Situacion = value; }
            }
            public String sConsignatario
            {
                get { return _Consignatario; }
                set { _Consignatario = value; }
            }
        #endregion
    }
}
