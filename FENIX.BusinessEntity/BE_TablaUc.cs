﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FENIX.BusinessEntity
{
    public class BE_TablaUc
    {
        #region "Campos"
        int _idTabla = 0;
        int _idTablaPadre = 0;
        int _IdValor = 0;
        string _sIdValor = string.Empty;
        string _ValVcr_Descripcion = string.Empty;
        String _ValChr_Estado = string.Empty;
        string _valVchr_Descripcion1 = string.Empty;
        string _valVchr_Descripcion2 = string.Empty;
        string _valVchr_Descripcion3 = string.Empty;
        string _valVchr_Descripcion4 = string.Empty;
        string _valVchr_Descripcion5 = string.Empty;
        #endregion

        #region "Propiedades"
        public String sDescripcion5
        {
            get { return _valVchr_Descripcion5; }
            set { _valVchr_Descripcion5 = value; }
        }
        public String sDescripcion4
        {
            get { return _valVchr_Descripcion4; }
            set { _valVchr_Descripcion4 = value; }
        }
        public String sDescripcion3
        {
            get { return _valVchr_Descripcion3; }
            set { _valVchr_Descripcion3 = value; }
        }

        public String sDescripcion2
        {
            get { return _valVchr_Descripcion2; }
            set { _valVchr_Descripcion2 = value; }
        }

        public String sDescripcion1
        {
            get { return _valVchr_Descripcion1; }
            set { _valVchr_Descripcion1 = value; }
        }

        public String sIdValor
        {
            get { return _sIdValor; }
            set { _sIdValor = value; }
        }
        public int iIdTabla
        {
            get { return _idTabla; }
            set { _idTabla = value; }
        }

        public int iIdTablaPadre
        {
            get { return _idTablaPadre; }
            set { _idTablaPadre = value; }
        }

        public int iIdValor
        {
            get { return _IdValor; }
            set { _IdValor = value; }
        }

        public String sDescripcion
        {
            get { return _ValVcr_Descripcion; }
            set { _ValVcr_Descripcion = value; }
        }

        public String sEstado
        {
            get { return _ValChr_Estado; }
            set { _ValChr_Estado = value; }
        }

        #endregion

    }
}
