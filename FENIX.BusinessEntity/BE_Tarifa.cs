﻿using System;
using System.Collections.Generic;
using System.Text;
using FENIX.Common;
using System.Reflection;

namespace FENIX.BusinessEntity
{
    [Serializable]
    public class BE_Tarifa : Paginador
    {
        #region "Campos"
        int _IdTarifa = 0;
        String _sIdTarifa = string.Empty;
        int _IdContrato = 0;
        int _IdTipoOperacion = 0;
        int _IdTamanoCnt = 0;
        int _IdCondicionCarga = 0;
        int _IdLineaNegocio = 0;
        int _IdCentroCosto = 0;
        int _IdCliente = 0;
        int _IdModalidad = 0;
        int _IdTerminalPorturario = 0;
        int _IdServicio = 0;
        int _IdTipoCnt = 0;
        int _IdCarga = 0;
        int _IdEmbalaje = 0;
        int _IdConDet = 0;
        String _Tar_Vch_Descripcion = String.Empty;
        String _Tar_Chr_FlagAlmacenajeRetroactivo = String.Empty;
        String _Tar_Vhr_DescripcionCosto = String.Empty;
        decimal _Tar_Num_MontoMinimo;
        decimal _Tar_Num_MontoMaximo;
        decimal _Tar_Num_MontoMinimoNeg;
        decimal _Tar_Num_MontoMaximoNeg;
        String _Tar_Chr_TipoTarifa = String.Empty;
        int _Tar_Num_DiasLibresAlmacenaje = 0;
        string _Tar_Chr_FlagTarifaSLI;
        string _Tar_Chr_FlagMandatorio;
        string _Tar_Chr_FlagTarifaPeligrosa;
        String _Tar_Chr_Estado = String.Empty;
        String _Tar_Chr_FlagLimite = String.Empty;
        String _Tar_Chr_FlagLimiteNego = String.Empty;
        String _Tar_Chr_FlagRangoDias = String.Empty;
        String _Ser_Vch_DescripcionServicio = String.Empty;
        String _sDescripcionTipoTarifa = String.Empty;
        int _Tar_Int_RangoDias1 = 0;
        int _Tar_Int_RangoDias2 = 0;
        int _Tar_Int_RangoDias3 = 0;
        int _IdMoneda;
        decimal _Tar_Num_RangoMonto1;
        decimal _Tar_Num_RangoMonto2;
        decimal _Tar_Num_RangoMonto3 = 0;
        decimal _Tar_Num_Monto = 0;
        decimal _Tar_Num_Costo = 0;
        String _DescripcionToperacion = String.Empty;
        String _DescripcionTcontenedor = String.Empty;
        String _DescripcionTcarga = String.Empty;
        String _DescripcionTembalaje = String.Empty;
        String _DescripcionTTamanoCnt = String.Empty;
        String _DescripcionTCondicionCarga = String.Empty;
        String _DescripcionTmodalidad = String.Empty;
        String _DescripcionTPorturario = String.Empty;
        String _sMoneda = String.Empty;
        String _sEmbalaje = String.Empty;
        String _sModalidad = String.Empty;
        Int32? _IdLinea;
        String _Linea;
        String _sCodTipoCont;
        String _sCodEmbalaje;
        String _sCodTipoCarga;
        String _sNumContrato;
        String _sDiasLibPeligrosa;
        int? _iIdTarifaOrigen;
        int? _iIdAsume;
        #endregion

        #region "Propiedades"
        public int? iIdAsume
        {
            get { return _iIdAsume; }
            set { _iIdAsume = value; }
        }
        public string sDiasLibPeligrosa
        {
            get { return _sDiasLibPeligrosa; }
            set { _sDiasLibPeligrosa = value; }
        }

        public string sNumContrato
        {
            get { return _sNumContrato; }
            set { _sNumContrato = value; }
        }

        public int? iIdTarifaOrigen
        {
            get { return _iIdTarifaOrigen; }
            set { _iIdTarifaOrigen = value; }
        }


        public string sEmbalaje
        {
            get { return _sEmbalaje; }
            set { _sEmbalaje = value; }
        }
        public string sMoneda
        {
            get { return _sMoneda; }
            set { _sMoneda = value; }
        }
        public string sModalidad
        {
            get { return _sModalidad; }
            set { _sModalidad = value; }
        }

        public string sDescripcionTipoTarifa
        {
            get { return _sDescripcionTipoTarifa; }
            set { _sDescripcionTipoTarifa = value; }
        }


        public int iIdConDet
        {
            get { return _IdConDet; }
            set { _IdConDet = value; }
        }


        public int iIdMoneda
        {
            get { return _IdMoneda; }
            set { _IdMoneda = value; }
        }

        public String sDescripcionCosto
        {
            get { return _Tar_Vhr_DescripcionCosto; }
            set { _Tar_Vhr_DescripcionCosto = value; }
        }

        public String sFlagRangoDias
        {
            get { return _Tar_Chr_FlagRangoDias; }
            set { _Tar_Chr_FlagRangoDias = value; }
        }

        public String sFlagLimiteNego
        {
            get { return _Tar_Chr_FlagLimiteNego; }
            set { _Tar_Chr_FlagLimiteNego = value; }
        }

        public String sFlagLimite
        {
            get { return _Tar_Chr_FlagLimite; }
            set { _Tar_Chr_FlagLimite = value; }
        }


        public String sIdTarifa
        {
            get { return _sIdTarifa; }
            set { _sIdTarifa = value; }
        }

        public String sDescripcionTPorturario
        {
            get { return _DescripcionTPorturario; }
            set { _DescripcionTPorturario = value; }
        }


        public String sDescripcionTmodalidad
        {
            get { return _DescripcionTmodalidad; }
            set { _DescripcionTmodalidad = value; }
        }


        public String sDescripcionTCondicionCarga
        {
            get { return _DescripcionTCondicionCarga; }
            set { _DescripcionTCondicionCarga = value; }
        }



        public String sDescripcionTTamanoCnt
        {
            get { return _DescripcionTTamanoCnt; }
            set { _DescripcionTTamanoCnt = value; }
        }


        public String sDescripcionTembalaje
        {
            get { return _DescripcionTembalaje; }
            set { _DescripcionTembalaje = value; }
        }

        public String sDescripcionTcarga
        {
            get { return _DescripcionTcarga; }
            set { _DescripcionTcarga = value; }
        }

        public String sDescripcionTcontenedor
        {
            get { return _DescripcionTcontenedor; }
            set { _DescripcionTcontenedor = value; }
        }

        public String sDescripcionToperacion
        {
            get { return _DescripcionToperacion; }
            set { _DescripcionToperacion = value; }
        }

        public String sDescripcionServicio
        {
            get { return _Ser_Vch_DescripcionServicio; }
            set { _Ser_Vch_DescripcionServicio = value; }
        }
        public int iIdTarifa
        {
            get { return _IdTarifa; }
            set { _IdTarifa = value; }
        }

        public int iIdContrato
        {
            get { return _IdContrato; }
            set { _IdContrato = value; }
        }

        public int iIdTipoOperacion
        {
            get { return _IdTipoOperacion; }
            set { _IdTipoOperacion = value; }
        }

        public int iIdTamanoCnt
        {
            get { return _IdTamanoCnt; }
            set { _IdTamanoCnt = value; }
        }


        public int iIdCondicionCarga
        {
            get { return _IdCondicionCarga; }
            set { _IdCondicionCarga = value; }
        }

        public int iIdLineaNegocio
        {
            get { return _IdLineaNegocio; }
            set { _IdLineaNegocio = value; }
        }

        public int iIdCentroCosto
        {
            get { return _IdCentroCosto; }
            set { _IdCentroCosto = value; }
        }

        public int iIdCliente
        {
            get { return _IdCliente; }
            set { _IdCliente = value; }
        }


        public int iIdModalidad
        {
            get { return _IdModalidad; }
            set { _IdModalidad = value; }
        }

        public int iIdTerminalPorturario
        {
            get { return _IdTerminalPorturario; }
            set { _IdTerminalPorturario = value; }
        }

        public int iIdServicio
        {
            get { return _IdServicio; }
            set { _IdServicio = value; }
        }

        public int iIdTipoCnt
        {
            get { return _IdTipoCnt; }
            set { _IdTipoCnt = value; }
        }

        public int iIdCarga
        {
            get { return _IdCarga; }
            set { _IdCarga = value; }
        }
        public int iIdEmbalaje
        {
            get { return _IdEmbalaje; }
            set { _IdEmbalaje = value; }
        }

        public string sDescripcion
        {
            get { return _Tar_Vch_Descripcion; }
            set { _Tar_Vch_Descripcion = value; }
        }
        public string sFlagAlmacenajeRetroactivo
        {
            get { return _Tar_Chr_FlagAlmacenajeRetroactivo; }
            set { _Tar_Chr_FlagAlmacenajeRetroactivo = value; }
        }
        public decimal dMontoMinimo
        {
            get { return _Tar_Num_MontoMinimo; }
            set { _Tar_Num_MontoMinimo = value; }
        }
        public decimal dMontoMaximo
        {
            get { return _Tar_Num_MontoMaximo; }
            set { _Tar_Num_MontoMaximo = value; }
        }
        public string sTipoTarifa
        {
            get { return _Tar_Chr_TipoTarifa; }
            set { _Tar_Chr_TipoTarifa = value; }
        }
        public int iDiasLibresAlmacenaje
        {
            get { return _Tar_Num_DiasLibresAlmacenaje; }
            set { _Tar_Num_DiasLibresAlmacenaje = value; }
        }
        public string sFlagTarifaSLI
        {
            get { return _Tar_Chr_FlagTarifaSLI; }
            set { _Tar_Chr_FlagTarifaSLI = value; }
        }
        public string sFlagMandatorio
        {
            get { return _Tar_Chr_FlagMandatorio; }
            set { _Tar_Chr_FlagMandatorio = value; }
        }
        public string sFlagTarifaPeligrosa
        {
            get { return _Tar_Chr_FlagTarifaPeligrosa; }
            set { _Tar_Chr_FlagTarifaPeligrosa = value; }
        }
        public string sEstado
        {
            get { return _Tar_Chr_Estado; }
            set { _Tar_Chr_Estado = value; }
        }
        public int iRangoDias1
        {
            get { return _Tar_Int_RangoDias1; }
            set { _Tar_Int_RangoDias1 = value; }
        }
        public int iRangoDias2
        {
            get { return _Tar_Int_RangoDias2; }
            set { _Tar_Int_RangoDias2 = value; }
        }
        public int iRangoDias3
        {
            get { return _Tar_Int_RangoDias3; }
            set { _Tar_Int_RangoDias3 = value; }
        }

        public decimal dRangoMonto1
        {
            get { return _Tar_Num_RangoMonto1; }
            set { _Tar_Num_RangoMonto1 = value; }
        }
        public decimal dRangoMonto2
        {
            get { return _Tar_Num_RangoMonto2; }
            set { _Tar_Num_RangoMonto2 = value; }
        }
        public decimal dRangoMonto3
        {
            get { return _Tar_Num_RangoMonto3; }
            set { _Tar_Num_RangoMonto3 = value; }
        }
        public decimal dMontoMinimoNeg
        {
            get { return _Tar_Num_MontoMinimoNeg; }
            set { _Tar_Num_MontoMinimoNeg = value; }
        }

        public decimal dMontoMaximoNeg
        {
            get { return _Tar_Num_MontoMaximoNeg; }
            set { _Tar_Num_MontoMaximoNeg = value; }
        }
        public decimal dMonto
        {
            get { return _Tar_Num_Monto; }
            set { _Tar_Num_Monto = value; }
        }
        public decimal dCosto
        {
            get { return _Tar_Num_Costo; }
            set { _Tar_Num_Costo = value; }
        }

        public Int32? iIdLinea
        {
            get { return _IdLinea; }
            set { _IdLinea = value; }
        }
        public String sLinea
        {
            get { return _Linea; }
            set { _Linea = value; }
        }

        public String sCodTipoCont
        {
            get { return _sCodTipoCont; }
            set { _sCodTipoCont = value; }
        }

        public String sCodEmbalaje
        {
            get { return _sCodEmbalaje; }
            set { _sCodEmbalaje = value; }
        }

        public String sCodTipoCarga
        {
            get { return _sCodTipoCarga; }
            set { _sCodTipoCarga = value; }
        }

        #endregion
    }

    #region Metodos
    [Serializable]
    public class BE_TarifaList : List<BE_Tarifa>
    {
        public void Ordenar(string propertyName, direccionOrden Direction)
        {
            BE_TarifaComparer dc = new BE_TarifaComparer(propertyName, Direction);
            this.Sort(dc);
        }
    }

    class BE_TarifaComparer : IComparer<BE_Tarifa>
    {
        string _prop = "";
        direccionOrden _dir;

        public BE_TarifaComparer(string propertyName, direccionOrden Direction)
        {
            _prop = propertyName;
            _dir = Direction;
        }

        public int Compare(BE_Tarifa x, BE_Tarifa y)
        {

            PropertyInfo propertyX = x.GetType().GetProperty(_prop);
            PropertyInfo propertyY = y.GetType().GetProperty(_prop);

            object px = propertyX.GetValue(x, null);
            object py = propertyY.GetValue(y, null);

            if (px == null && py == null)
            {
                return 0;
            }
            else if (px != null && py == null)
            {
                if (_dir == direccionOrden.Ascending)
                {
                    return 1;
                }
                else
                {
                    return -1;
                }
            }
            else if (px == null && py != null)
            {
                if (_dir == direccionOrden.Ascending)
                {
                    return -1;
                }
                else
                {
                    return 1;
                }
            }
            else if (px.GetType().GetInterface("IComparable") != null)
            {
                if (_dir == direccionOrden.Ascending)
                {
                    return ((IComparable)px).CompareTo(py);
                }
                else
                {
                    return ((IComparable)py).CompareTo(px);
                }
            }
            else
            {
                return 0;
            }
        }



    }

    #endregion
}



