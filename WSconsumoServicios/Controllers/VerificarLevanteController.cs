﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Cryptography.X509Certificates;
using System.ServiceModel;
using System.Web.Http;
using WSconsumoServicios.Entity;
using WSFargoLine.ServiceReconocimientoFisico;

namespace WSconsumoServicios.Controllers
{
    public class VerificarLevanteController : ApiController
    {

        public class levanteImput
        {
            public string anio { get; set; }
            public string nroDua { get; set; }
        }


        [HttpPost]
        public VerificarLevanteDua resLevanteDua([FromBody]  levanteImput value)
        {
            VerificarLevanteDua BE_VerificarLevanteDua = new VerificarLevanteDua();

            BE_VerificarLevanteDua = consultSuntLevante(value.nroDua,value.anio);

            return BE_VerificarLevanteDua;
        }

 

        VerificarLevanteDua consultSuntLevante(string vi_nroDua, string vi_anio)
        {
            VerificarLevanteDua oBE_VerificarLevanteDua = new VerificarLevanteDua();

            String SunatEndpoint = ConfigurationManager.AppSettings["SunatEndpoint"];
            String SunatThumbPrint = ConfigurationManager.AppSettings["SunatThumbPrint"];

            try
            {


                ReconocimientoFisicoPortTypeClient clientResponse = new ReconocimientoFisicoPortTypeClient("ReconocimientoFisicoPort");
                //New
                clientResponse.Endpoint.Address = new EndpointAddress(new Uri(SunatEndpoint),
                     clientResponse.Endpoint.Address.Identity, clientResponse.Endpoint.Address.Headers);
                clientResponse.ClientCredentials.ClientCertificate.SetCertificate(
                    StoreLocation.LocalMachine,
                    StoreName.My,
                    X509FindType.FindByIssuerName,
                    SunatThumbPrint
                    );
                /*RoleCode*/
                SubmitterRoleCodeType roleCodeType = new SubmitterRoleCodeType
                {
                    Value = "31"
                };
                /*with list object*/
                List<LevanteRequestSubmitter> clienteSubmitters = new List<LevanteRequestSubmitter>();
                clienteSubmitters.Add(new LevanteRequestSubmitter { RoleCode = roleCodeType });
                /*OfficeId*/
                DeclarationDeclarationOfficeIDType clienteOfficeIDType = new DeclarationDeclarationOfficeIDType();
                /*Anio*/
                DeclarationIssueDateTimeTypeDateTimeString anioType = new DeclarationIssueDateTimeTypeDateTimeString();
                DeclarationIssueDateTimeType clienteAnio = new DeclarationIssueDateTimeType();
                /*CurrentCode*/
                GovernmentProcedureCurrentCodeType currentCodeType = new GovernmentProcedureCurrentCodeType
                {
                    Value = "10"
                };
                /*with list object*/
                List<LevanteRequestGovernmentProcedure> requestGovernmentProcedures = new List<LevanteRequestGovernmentProcedure>
            {
                new LevanteRequestGovernmentProcedure { CurrentCode = currentCodeType }
            };
                /*ID DUA*/
                DeclarationIdentificationIDType clienteID = new DeclarationIdentificationIDType();
                String DeclarationOfficeID = "118";
                String IssueDateTime = vi_anio;
                String ID = vi_nroDua;
                String Regimen = "10";
                clienteOfficeIDType.Value = DeclarationOfficeID;
                anioType.Value = IssueDateTime;
                anioType.formatCode = "602";
                clienteAnio.Item = anioType;
                clienteID.Value = ID;


                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;


                verificarLevanteRequest requestList = new verificarLevanteRequest(clienteSubmitters.ToArray(), clienteOfficeIDType, clienteAnio, requestGovernmentProcedures.ToArray(), clienteID);

                /*   Get Response    */
                /*      REQUESTS        */
                DeclarationIssueDateTimeType issueDateTimeType = new DeclarationIssueDateTimeType
                {
                    Item = anioType
                };
                /*      RESPONSES       */
                DeclarationStatusCodeType declarationStatusCode = new DeclarationStatusCodeType();
                DeclarationAcceptanceDateTimeType acceptanceDateTimeOut = new DeclarationAcceptanceDateTimeType();
                LevanteResponseGoodsShipment[] levanteResponseGoodsShipment;

                DeclarationTotalGrossMassMeasureType totalGrossMassMeasureType = new DeclarationTotalGrossMassMeasureType();
                DeclarationTotalPackageQuantityType totalPackageQuantityType = new DeclarationTotalPackageQuantityType();
                LevanteResponseAdditionalInformation[] levanteResponseAdditionalInformation;
                LevanteResponseGoodsShipmentCustomsValuation[] levanteResponseCustomsValuation;


                //ExpectedArrivalDateTime[] 
                String statusCode = "";
                string BlCode = "";

                // verificarLevanteResponse levanteResponse;  
                verificarLevanteResponse retVal = ((ReconocimientoFisicoPortType)(clientResponse)).verificarLevante(requestList);


                oBE_VerificarLevanteDua.StatusCode = retVal.StatusCode.Value.ToString();

                String fechaLevante = "";

                if (retVal.StatusCode == null)
                {
                    oBE_VerificarLevanteDua.StatusCode = "-1";

                }
                else
                {
                    levanteResponseGoodsShipment = retVal.GoodsShipment;
                    DateTime fechaLevanteDate;
                    fechaLevante = (levanteResponseGoodsShipment[0].Status[0].ReleaseDateTime.Item.Value != null) ? levanteResponseGoodsShipment[0].Status[0].ReleaseDateTime.Item.Value : "";

                    fechaLevante = Convert.ToString(String.Format("{0:dd/MM/yyyy HH:mm}", fechaLevante));

                    if (DateTime.TryParse(fechaLevante, out fechaLevanteDate))
                    {
                        //fechaLevante = String.Format(fechaLevante, "dd/MM/yyyy HH:mm");
                        fechaLevante = fechaLevante.Substring(0, fechaLevante.Length - 3);
                    }
                    else
                    {                  //DIA                            //MES                            //AÑO
                        fechaLevante = fechaLevante.Substring(6, 2) + "/" + fechaLevante.Substring(4, 2) + "/" + fechaLevante.Substring(0, 4) +
                            " " + fechaLevante.Substring(8, 2) + ":" + fechaLevante.Substring(10, 2);
                        //HORA                          //MIN                                
                    }


                    BlCode = (levanteResponseGoodsShipment[0].Consignment[0].TransportContractDocument[0].ID.Value != null) ? levanteResponseGoodsShipment[0].Consignment[0].TransportContractDocument[0].ID.Value : "";

                    //fechaLevante = fechaLevante.ToString().Trim();

                    oBE_VerificarLevanteDua.BlCode = BlCode;
                    oBE_VerificarLevanteDua.fechaLevante = fechaLevante;


                }

                return oBE_VerificarLevanteDua;

            }
            catch (Exception ex)
            {
                return oBE_VerificarLevanteDua;

            }

        }
    }
}
