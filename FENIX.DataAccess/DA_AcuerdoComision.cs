﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using FENIX.BusinessEntity;
using FENIX.Common;

namespace FENIX.DataAccess
{
    public class DA_AcuerdoComision
    {


        public Int32 GrabarLiquidacion(BE_AcuerdoComision ent)
        {
            try
            {
                using (Database dbTerminal = new Database())
                {
                    dbTerminal.ProcedureName = "Comercial_T_Ins_Liquidacion";
                    dbTerminal.AddParameter("@IdComisionista", DbType.Int32, ParameterDirection.Input, ent.iIdComisionista);
                    dbTerminal.AddParameter("@UsuarioReg", DbType.String, ParameterDirection.Input, ent.sUsuario);
                    dbTerminal.AddParameter("@Tipo", DbType.Int32, ParameterDirection.Input, ent.iTipoTran);
                    dbTerminal.AddParameter("@vo_Retorno", DbType.Int32, ParameterDirection.Output, 0, 30);
                    //REUTILIZANDO CAMPOS IDTIPODDOC QUE ES EL TIPO SI ES 1 = INSERT SI ES = UPDATE.

                    dbTerminal.Execute();

                    return Convert.ToInt32(dbTerminal.GetParameter("@vo_Retorno"));
                }
            }
            catch (Exception e)
            {
                return 0;
            }
        }

        public Int32 GrabarLiquidacionDetalle(BE_AcuerdoComision ent)
        {
           // Int32 rpta = 0;
            Int32 rpta = 0;
            SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["FFAPRD_EXTRANET"].ConnectionString);
            SqlCommand cmd = new SqlCommand("Comercial_T_Ins_LiquidacionDetalle", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@IdLiquidacion", ent.iLiquidacion);
            cmd.Parameters.AddWithValue("@IdDocPagDet", ent.iIdDocPagoDet);
            cmd.Parameters.AddWithValue("@IdDocOriDet", ent.iIdDocOriDet);
            cmd.Parameters.AddWithValue("@IdAcuerdoComision", ent.IidAcuerdoComision);
            cmd.Parameters.AddWithValue("@FechaRetiro", Convert.ToDateTime(ent.sRetirado));
            cmd.Parameters.AddWithValue("@FechaEmision", Convert.ToDateTime(ent.sFacturado));
            cmd.Parameters.AddWithValue("@IdTarifa", ent.IidTarifa);
            cmd.Parameters.AddWithValue("@MonPago", ent.iIdMoneda);
            cmd.Parameters.AddWithValue("@ImportePago", ent.dImporte);
            cmd.Parameters.AddWithValue("@Situacion", ent.sSituacion);
            cmd.Parameters.AddWithValue("@PqteSLI", ent.sPqtSLI);
            cmd.Parameters.AddWithValue("@IdDetPqteSLI", ent.iIdDetpaqSLI);
            cmd.Parameters.AddWithValue("@UsuarioReg", ent.sUsuario);

            try
            {
                conn.Open();
                if (cmd.ExecuteNonQuery() > 0)
                    rpta = 1;
            }
            catch (Exception)
            {
                rpta = 0;
                throw;
            }
            finally
            {
                conn.Close();
                conn.Dispose();
            }
            return rpta;



            //using (Database dbTerminal = new Database())
            //{
            //    dbTerminal.ProcedureName = "Comercial_T_Ins_LiquidacionDetalle";
            //    dbTerminal.AddParameter("@IdLiquidacion", DbType.Int32, ParameterDirection.Input, ent.iLiquidacion);
            //    dbTerminal.AddParameter("@IdDocPagDet", DbType.Int32, ParameterDirection.Input, ent.iIdDocPagoDet);
            //    dbTerminal.AddParameter("@IdDocOriDet", DbType.Int32, ParameterDirection.Input, ent.iIdDocOriDet);
            //    dbTerminal.AddParameter("@IdAcuerdoComision", DbType.Int32, ParameterDirection.Input, ent.IidAcuerdoComision);             
            //    dbTerminal.AddParameter("@FechaRetiro", DbType.String, ParameterDirection.Input, ent.sRetirado);
            //    dbTerminal.AddParameter("@FechaEmision", DbType.String, ParameterDirection.Input, ent.sFacturado);
            //    dbTerminal.AddParameter("@IdTarifa", DbType.Int32, ParameterDirection.Input, ent.IidTarifa);
            //    dbTerminal.AddParameter("@MonPago", DbType.Int32, ParameterDirection.Input, ent.iIdMoneda);
            //    dbTerminal.AddParameter("@ImportePago", DbType.Decimal, ParameterDirection.Input, ent.dImporte);
            //    dbTerminal.AddParameter("@Situacion", DbType.String, ParameterDirection.Input, ent.sSituacion);
            //    dbTerminal.AddParameter("@PqteSLI", DbType.String, ParameterDirection.Input, ent.sPqtSLI);
            //    dbTerminal.AddParameter("@IdDetPqteSLI", DbType.Int32, ParameterDirection.Input, ent.iIdDetpaqSLI);               
            //    dbTerminal.AddParameter("@UsuarioReg", DbType.String, ParameterDirection.Input, ent.sUsuario);
            //    //REUTILIZANDO CAMPOS IDTIPODDOC QUE ES EL TIPO SI ES 1 = INSERT SI ES = UPDATE.

            //    dbTerminal.Execute();

            //    try
            //    {
            //        if (dbTerminal.Execute() > 0)
            //            rpta = 1;
            //    }
            //    catch (Exception)
            //    {
            //        rpta = 0;
            //        throw;
            //    }            
                
            //}

         
           // return rpta;

        }

        public String GrabaDocsComisionista(BE_AcuerdoComision ent)
        {
            

            try
            {
                using (Database dbTerminal = new Database())
                {
                    dbTerminal.ProcedureName = "Comercial_Ins_DocsComisionista";
                    dbTerminal.AddParameter("@vi_IdLiquidacion", DbType.Int32, ParameterDirection.Input, ent.iLiquidacion);
                    dbTerminal.AddParameter("@vi_IdDoc", DbType.String, ParameterDirection.Input, ent.iIdDocComisionista);
                    dbTerminal.AddParameter("@vi_IdComisionista", DbType.String, ParameterDirection.Input, ent.iIdComisionista);
                    dbTerminal.AddParameter("@vi_TipoDoc", DbType.Int32, ParameterDirection.Input, ent.iTipoDoc);
                    dbTerminal.AddParameter("@vi_Serie", DbType.String, ParameterDirection.Input, ent.sDocNroSerie);
                    dbTerminal.AddParameter("@vi_NumeroDoc", DbType.String, ParameterDirection.Input, ent.sDocNroComisionista);
                    dbTerminal.AddParameter("@vi_Usuario", DbType.String, ParameterDirection.Input, ent.sUsuario);
                    dbTerminal.AddParameter("@vi_NombrePc", DbType.String, ParameterDirection.Input, ent.sNomPc);
                    dbTerminal.AddParameter("@Retorno", DbType.String, ParameterDirection.Output, 0, 500);


                    dbTerminal.Execute();

                    return dbTerminal.GetParameter("@Retorno").ToString();
                }
            }
            catch (Exception e)
            {
                return "Error Catch Behind";
            }


        }


        public Int32 Ins_AceptaCondiciones(BE_AcuerdoComision ent)
        {
            try
            {
                using (Database dbTerminal = new Database())
                {
                    dbTerminal.ProcedureName = "Extranet_AceptaCondicionesUso";
                    dbTerminal.AddParameter("@IdComisionista", DbType.Int32, ParameterDirection.Input, ent.iIdComisionista);
                    dbTerminal.AddParameter("@vch_UsuarioExtranet", DbType.String, ParameterDirection.Input, ent.sUsuario);
                    dbTerminal.AddParameter("@Acepta_Condicion", DbType.Int32, ParameterDirection.Input, ent.sAceptaCondicion);
                    dbTerminal.AddParameter("@Retorno", DbType.String, ParameterDirection.Output, 0, 30);
                    //REUTILIZANDO CAMPOS IDTIPODDOC QUE ES EL TIPO SI ES 1 = INSERT SI ES = UPDATE.

                    dbTerminal.Execute();

                    return Convert.ToInt32(dbTerminal.GetParameter("@Retorno"));
                }
            }
            catch (Exception e)
            {
                return 0;
            }
        }

        public String AutorizarLiquidacion(BE_AcuerdoComision ent)
        {
            try
            {
                using (Database dbTerminal = new Database())
                {
                    dbTerminal.ProcedureName = "[Comercial_T_Autorizar_Liquidacion]";
                    dbTerminal.AddParameter("@IdLiquidacion", DbType.Int32, ParameterDirection.Input, ent.iLiquidacion);
                    dbTerminal.AddParameter("@UsuarioReg", DbType.String, ParameterDirection.Input, ent.sUsuario);
                    dbTerminal.AddParameter("@vo_Retorno", DbType.String, ParameterDirection.Output, String.Empty, 400);

                    dbTerminal.Execute();

                    String Retorno = dbTerminal.GetParameter("@vo_Retorno").ToString();
                    return Retorno;
                }
            }
            catch (Exception e)
            {
                return "-1|Error";
            }

        }


        public List<BE_AcuerdoComision> ListarDocumentoParaLiquidarComision(BE_AcuerdoComision oBE_Acuerdo)
         {
            IDataReader reader = null;
            try
            {
                using (Database dbTerminal = new Database())
                {

                    dbTerminal.ProcedureName = "[Comercial_Lis_DocumentosLiquidarXComisionistaV6]";
                    dbTerminal.AddParameter("@vi_IdComisionista", DbType.Int32, ParameterDirection.Input, oBE_Acuerdo.iIdComisionista);
                    dbTerminal.AddParameter("@vi_IdMoneda", DbType.Int32, ParameterDirection.Input, oBE_Acuerdo.iIdMoneda);


                    reader = dbTerminal.GetDataReader();


                    List<BE_AcuerdoComision> Lst_BE_Comision = new List<BE_AcuerdoComision>();
                    while (reader.Read())
                    {
                        Lst_BE_Comision.Add(Pu_AcuerdoComision.ListarDocumentoParaLiquidarComision(reader));
                    }
                    reader.Close();
                    //oBE_Acuerdo.NTotalRegistros = Convert.ToInt32(dbTerminal.GetParameter("@vo_totalregistros"));
                    return Lst_BE_Comision;
                }

            }
            catch (Exception e)
            {
                ResultadoTransaccionTx oResultadoTransaccionTx = null;
                oResultadoTransaccionTx.RegistrarError(e);
                return null;
            }
        }

        public List<BE_AcuerdoComision> ListarDatosLiquidacion(Int32 iIdLiquidacion)
        {
            IDataReader reader = null;
            try
            {
                using (Database dbTerminal = new Database())
                {

                    dbTerminal.ProcedureName = "EXTRANET_COM_ListaComisiones_Aprobar";
                    dbTerminal.AddParameter("@idLiquidacion", DbType.Int32, ParameterDirection.Input, iIdLiquidacion);
                 


                    reader = dbTerminal.GetDataReader();


                    List<BE_AcuerdoComision> Lst_BE_Comision = new List<BE_AcuerdoComision>();
                    while (reader.Read())
                    {
                        Lst_BE_Comision.Add(Pu_AcuerdoComision.ListarDatosLiquidacion(reader));
                    }
                    reader.Close();
                    //oBE_Acuerdo.NTotalRegistros = Convert.ToInt32(dbTerminal.GetParameter("@vo_totalregistros"));
                    return Lst_BE_Comision;
                }

            }
            catch (Exception e)
            {
                ResultadoTransaccionTx oResultadoTransaccionTx = null;
                oResultadoTransaccionTx.RegistrarError(e);
                return null;
            }
        }

        public List<BE_AcuerdoComision> Listar_LiquidacionXComisionista(BE_AcuerdoComision oBE_Acuerdo)
        {
            IDataReader reader = null;
            try
            {
                using (Database dbTerminal = new Database())
                {

                    dbTerminal.ProcedureName = "Extranet_COM_Lista_Liquidaciones_ComisionistaV2";
                    dbTerminal.AddParameter("@vi_IdComisionista", DbType.Int32, ParameterDirection.Input, oBE_Acuerdo.iIdComisionista);
                   // dbTerminal.AddParameter("@Periodo", DbType.Int32, ParameterDirection.Input, oBE_Acuerdo.iPeriodo);
                    dbTerminal.AddParameter("@MesIni", DbType.String, ParameterDirection.Input, oBE_Acuerdo.sMesIni);
                    dbTerminal.AddParameter("@MesFin", DbType.String, ParameterDirection.Input, oBE_Acuerdo.sMesFin);
                    dbTerminal.AddParameter("@Estado", DbType.Int32, ParameterDirection.Input, oBE_Acuerdo.iEstado);
                    dbTerminal.AddParameter("@IdLiquidacion", DbType.String, ParameterDirection.Input, oBE_Acuerdo.sLiquidacion);
                    dbTerminal.AddParameter("@vi_numeropagina", DbType.Int32, ParameterDirection.Input, oBE_Acuerdo.NPagina);
                    dbTerminal.AddParameter("@vi_numeroregistros", DbType.Int32, ParameterDirection.Input, oBE_Acuerdo.NRegistros);
                    dbTerminal.AddParameter("@vo_totalregistros", DbType.Int32, ParameterDirection.Output, oBE_Acuerdo.NtotalRegistros);

                    reader = dbTerminal.GetDataReader();
                    
                    List<BE_AcuerdoComision> Lst_BE_Comision = new List<BE_AcuerdoComision>();
                    while (reader.Read())
                    {
                        Lst_BE_Comision.Add(Pu_AcuerdoComision.Listar_LiquidacionXComisionista(reader));
                    }
                    reader.Close();
                    oBE_Acuerdo.NtotalRegistros = Convert.ToInt32(dbTerminal.GetParameter("@vo_totalregistros"));
                    return Lst_BE_Comision;
                }

            }
            catch (Exception e)
            {
                ResultadoTransaccionTx oResultadoTransaccionTx = null;
                oResultadoTransaccionTx.RegistrarError(e);
                return null;
            }
        }

        public List<BE_AcuerdoComision> ListarDocsComisionista(Int32 iIdLiquidacion)
        {
            IDataReader reader = null;
            try
            {
                using (Database dbTerminal = new Database())
                {
                    dbTerminal.ProcedureName = "[Comercial_LisDocsComisionista]";
                    dbTerminal.AddParameter("@vi_IdLiquidacion", DbType.Int32, ParameterDirection.Input, iIdLiquidacion);

                    reader = dbTerminal.GetDataReader();

                    List<BE_AcuerdoComision> Lis_BE_AcuerdoComision = new List<BE_AcuerdoComision>();
                    while (reader.Read())
                    {
                        Lis_BE_AcuerdoComision.Add(Pu_AcuerdoComision.Listar_DocsComisionista(reader));
                    }

                    return Lis_BE_AcuerdoComision;
                }
            }
            catch (Exception e)
            {
                ResultadoTransaccionTx oResultadoTransaccionTx = new ResultadoTransaccionTx(ResultadoTransaccionTx.TipoResultado.Error, e);
                oResultadoTransaccionTx.RegistrarError(e);
                return null;

            }
        }

        public List<BE_AcuerdoComision> Listar_EstadosXLiquidacion()
        {
            IDataReader reader = null;
            try
            {
                using (Database dbTerminal = new Database())
                {
                    dbTerminal.ProcedureName = "[Extranet_ListaEstadosLiquidaciones]";
                   // dbTerminal.AddParameter("@vi_IdLiquidacion", DbType.Int32, ParameterDirection.Input, iIdLiquidacion);

                    reader = dbTerminal.GetDataReader();

                    List<BE_AcuerdoComision> Lis_BE_AcuerdoComision = new List<BE_AcuerdoComision>();
                    while (reader.Read())
                    {
                        Lis_BE_AcuerdoComision.Add(Pu_AcuerdoComision.Listar_EstadosXLiquidacion(reader));
                    }

                    return Lis_BE_AcuerdoComision;
                }
            }
            catch (Exception e)
            {
                ResultadoTransaccionTx oResultadoTransaccionTx = new ResultadoTransaccionTx(ResultadoTransaccionTx.TipoResultado.Error, e);
                oResultadoTransaccionTx.RegistrarError(e);
                return null;

            }
        }
    }
}
