﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FENIX.BusinessEntity;
using System.Data;
using FENIX.Common;
using System.Data.Common;
using System.Data.SqlClient;
using FENIX.DataAccess;

namespace FENIX.DataAccess
{
    public class DA_Servicio
    {
        public BE_DetalleLis ListarDocumentoDetalle(BE_eOrdenServicio oBE_eOrdenServicio)
        {
            IDataReader reader = null;
            try
            {
                using (Database dbTerminal = new Database())
                {
                    dbTerminal.ProcedureName = "[Extranet_ListarDetalleServicio]";
                    dbTerminal.AddParameter("@vi_TipoOperacion", DbType.Int32, ParameterDirection.Input, oBE_eOrdenServicio.iIdTipoOperacion);
                    dbTerminal.AddParameter("@vi_IdClienteAduana", DbType.Int32, ParameterDirection.Input, oBE_eOrdenServicio.iIdClienteAduana);
                    dbTerminal.AddParameter("@vi_Documento", DbType.String, ParameterDirection.Input, oBE_eOrdenServicio.sNumeroDO);
                    dbTerminal.AddParameter("@vi_Volante", DbType.String, ParameterDirection.Input, oBE_eOrdenServicio.sNumeroVO);

                    reader = dbTerminal.GetDataReader();

                    BE_DetalleLis lis_BE_Detalle = new BE_DetalleLis();

                    while (reader.Read())
                    {
                        lis_BE_Detalle.Add(Pu_Servicio.listarDetalle(reader));
                    }
                    reader.Close();
                    return lis_BE_Detalle;
                }
            }
            catch(Exception e) {
                ResultadoTransaccionTx oResultadoTransaccionTx = new ResultadoTransaccionTx(ResultadoTransaccionTx.TipoResultado.Error, e);
                oResultadoTransaccionTx.RegistrarError(e);
                return null;
            }
        }
        public List<BE_Operacion> ListarItems()
        {
            IDataReader reader = null;
            try
            {
                using (Database dbTerminal = new Database())
                {
                    dbTerminal.ProcedureName = "[EXTRANET_ListarTipoOperacion]";
                    reader = dbTerminal.GetDataReader();

                    List<BE_Operacion> lis_BE_Operacion = new List<BE_Operacion>();
                    while (reader.Read())
                    {
                        lis_BE_Operacion.Add(Pu_Servicio.ListarTipoOperacion(reader));
                    }
                    reader.Close();
                    return lis_BE_Operacion;
                }
            }
            catch (Exception e)
            {
                ResultadoTransaccionTx oResultadoTransaccionTx = new ResultadoTransaccionTx(ResultadoTransaccionTx.TipoResultado.Error, e);
                oResultadoTransaccionTx.RegistrarError(e);
                return null;
            }
        }

        public List<BE_Servicio> ListarServicios()
        {
            IDataReader reader = null;
            try
            {
                using (Database dbTerminal = new Database())
                {
                    dbTerminal.ProcedureName = "[Extranet_ListarServicios]";
                    reader = dbTerminal.GetDataReader();

                    List<BE_Servicio> lis_BE_Servicio = new List<BE_Servicio>();
                    while (reader.Read())
                    {
                        lis_BE_Servicio.Add(Pu_Servicio.ListarServicio(reader));
                    }
                    reader.Close();
                    return lis_BE_Servicio;
                }
            }
            catch (Exception e)
            {
                ResultadoTransaccionTx oResultadoTransaccionTx = new ResultadoTransaccionTx(ResultadoTransaccionTx.TipoResultado.Error, e);
                oResultadoTransaccionTx.RegistrarError(e);
                return null;
            }
        }

        public List<BE_ServicioAdicional> ListarServiciosAdicionales(Int32 iIdTipoOperacion, Int32 iIdOficina, Int32 iIdSuperNegocio, Int32 iIdLineaNegocio)
        {
            IDataReader reader = null;
            try
            {
                using (Database dbTerminal = new Database())
                {
                    dbTerminal.ProcedureName = "[Extranet_ListarServiciosAdicionales]";
                    dbTerminal.AddParameter("@IdTipoOperacion", DbType.Int32, ParameterDirection.Input, iIdTipoOperacion);
                    dbTerminal.AddParameter("@IdOficina", DbType.Int32, ParameterDirection.Input, iIdOficina);
                    dbTerminal.AddParameter("@IdSuperNegocio", DbType.Int32, ParameterDirection.Input, iIdSuperNegocio);
                    dbTerminal.AddParameter("@IdLineaNegocio", DbType.Int32, ParameterDirection.Input, iIdLineaNegocio);

                    reader = dbTerminal.GetDataReader();

                    BE_ServicioAdicionalLis lis_BE_ServicioAdicional = new BE_ServicioAdicionalLis();

                    while (reader.Read())
                    {
                        lis_BE_ServicioAdicional.Add(Pu_Servicio.listarServicioAdicional(reader));
                    }
                    reader.Close();
                    return lis_BE_ServicioAdicional;
                }
            }
            catch (Exception e)
            {
                ResultadoTransaccionTx oResultadoTransaccionTx = new ResultadoTransaccionTx(ResultadoTransaccionTx.TipoResultado.Error, e);
                oResultadoTransaccionTx.RegistrarError(e);
                return null;
            }
        }
    }
}
