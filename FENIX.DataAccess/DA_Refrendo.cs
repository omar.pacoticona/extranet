﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FENIX.BusinessEntity;
using FENIX.Common;
using FENIX.DataAccess.Publicacion;

namespace FENIX.DataAccess
{
    public class DA_Refrendo
    {

        public List<BE_Refrendo> ListarDatosXBooking_Refrendo(BE_Refrendo oBE_Refrendo)
        {
            IDataReader reader = null;
            try
            {
                using (Database dbTerminal = new Database())
                {

                    dbTerminal.ProcedureName = "EXTRANET_ListDatosBooking_Refrendo";
                    dbTerminal.AddParameter("@V_vch_Booking", DbType.String, ParameterDirection.Input, oBE_Refrendo.sBooking);
                    dbTerminal.AddParameter("@v_iIdClienteExtranet", DbType.Int32, ParameterDirection.Input, oBE_Refrendo.IdCliente);



                    reader = dbTerminal.GetDataReader();

                    List<BE_Refrendo> Lst_BE_Refrendo = new List<BE_Refrendo>();
                    while (reader.Read())
                    {
                        Lst_BE_Refrendo.Add(Pu_Refrendo.ListarDatosXBooking_Refrendo(reader));
                    }
                    reader.Close();

                    return Lst_BE_Refrendo;
                }

            }
            catch (Exception e)
            {
                ResultadoTransaccionTx oResultadoTransaccionTx = null;
                oResultadoTransaccionTx.RegistrarError(e);
                return null;
            }
        }

        public List<BE_Refrendo> ListarDetalleDocumento(BE_Refrendo oBE_Refrendo)
        {
            IDataReader reader = null;
            try
            {
                using (Database dbTerminal = new Database())
                {

                    dbTerminal.ProcedureName = "[EXTRANET_ListarDetallesDocumento]";
                    dbTerminal.AddParameter("@V_vch_Numero", DbType.String, ParameterDirection.Input, oBE_Refrendo.sBooking);
                    
                    reader = dbTerminal.GetDataReader();

                    List<BE_Refrendo> Lst_BE_Refrendo = new List<BE_Refrendo>();
                    while (reader.Read())
                    {
                        Lst_BE_Refrendo.Add(Pu_Refrendo.ListarDetalleDocumento(reader));
                    }
                    reader.Close();

                    return Lst_BE_Refrendo;
                }

            }
            catch (Exception e)
            {
                ResultadoTransaccionTx oResultadoTransaccionTx = null;
                oResultadoTransaccionTx.RegistrarError(e);
                return null;
            }
        }

        public List<BE_Refrendo> ListarSolicitudRefrendo(BE_Refrendo oBE_Refrendo)
        {
            IDataReader reader = null;
            try
            {
                using (Database dbTerminal = new Database())
                {

                    dbTerminal.ProcedureName = "EXTRANET_List_RefrendoElectronico";
                    dbTerminal.AddParameter("@V_vch_Booking", DbType.String, ParameterDirection.Input, oBE_Refrendo.sBooking);
                    dbTerminal.AddParameter("@V_vch_Situacion", DbType.String, ParameterDirection.Input, oBE_Refrendo.sSituacion);
                    dbTerminal.AddParameter("@V_vch_IdSolicitudRefrendo", DbType.String, ParameterDirection.Input, oBE_Refrendo.sSolicitudRefr);
                    dbTerminal.AddParameter("@v_IdClientExtranet", DbType.Int32, ParameterDirection.Input, oBE_Refrendo.IdCliente);                    
                    dbTerminal.AddParameter("@V_vch_FechaInicio", DbType.String, ParameterDirection.Input, oBE_Refrendo.sMesIni);
                    dbTerminal.AddParameter("@V_vch_FechaFin", DbType.String, ParameterDirection.Input, oBE_Refrendo.sMesFin);
                    dbTerminal.AddParameter("@vi_numeropagina", DbType.Int32, ParameterDirection.Input, oBE_Refrendo.NPagina);
                    dbTerminal.AddParameter("@vi_numeroregistros", DbType.Int32, ParameterDirection.Input, oBE_Refrendo.NRegistros);
                    dbTerminal.AddParameter("@vo_totalregistros", DbType.Int32, ParameterDirection.Output, oBE_Refrendo.NtotalRegistros);


                    reader = dbTerminal.GetDataReader();

                    List<BE_Refrendo> Lst_BE_Refrendo = new List<BE_Refrendo>();
                    while (reader.Read())
                    {
                        Lst_BE_Refrendo.Add(Pu_Refrendo.ListarSolicitudRefrendo(reader));
                    }
                    reader.Close();
                    oBE_Refrendo.NtotalRegistros = Convert.ToInt32(dbTerminal.GetParameter("@vo_totalregistros"));

                    return Lst_BE_Refrendo;
                }

            }
            catch (Exception e)
            {
                ResultadoTransaccionTx oResultadoTransaccionTx = null;
                oResultadoTransaccionTx.RegistrarError(e);
                return null;
            }
        }

        public List<BE_Refrendo> Listar_Despachadores(String _codigoRUC)
        {
            IDataReader reader = null;
            try
            {
                using (Database dbTerminal = new Database())
                {

                    dbTerminal.ProcedureName = "EXTRANET_REFRENDO_LISTARDESPACHADORES";
                    dbTerminal.AddParameter("@v_Codigo_RUC", DbType.String, ParameterDirection.Input, _codigoRUC);
                    


                    reader = dbTerminal.GetDataReader();

                    List<BE_Refrendo> Lst_BE_Refrendo = new List<BE_Refrendo>();
                    while (reader.Read())
                    {
                        Lst_BE_Refrendo.Add(Pu_Refrendo.ListarDespachadores(reader));
                    }
                    reader.Close();
                    

                    return Lst_BE_Refrendo;
                }

            }
            catch (Exception e)
            {
                ResultadoTransaccionTx oResultadoTransaccionTx = null;
                oResultadoTransaccionTx.RegistrarError(e);
                return null;
            }
        }


        public List<BE_Refrendo> ListarDetalleBultosPrecintosIngresadosCliente(BE_Refrendo oBE_Refrendo)
        {
            IDataReader reader = null;
            try
            {
                using (Database dbTerminal = new Database())
                {

                    dbTerminal.ProcedureName = "[Listar_BultosPrecintosIngresadosExtranet]";
                    dbTerminal.AddParameter("@v_IdSolicitudREL", DbType.Int32, ParameterDirection.Input, oBE_Refrendo.iIdSolicitudRefr);
                    reader = dbTerminal.GetDataReader();

                    List<BE_Refrendo> Lst_BE_Refrendo = new List<BE_Refrendo>();
                    while (reader.Read())
                    {
                        Lst_BE_Refrendo.Add(Pu_Refrendo.ListarDetalleBultosPrecintosIngresadosCliente(reader));
                    }
                    reader.Close();


                    return Lst_BE_Refrendo;
                }

            }
            catch (Exception e)
            {
                ResultadoTransaccionTx oResultadoTransaccionTx = null;
                oResultadoTransaccionTx.RegistrarError(e);
                return null;
            }
        }

        public List<BE_Refrendo> ListarREL_Ruc(BE_Refrendo oBE_Refrendo)
        {
            IDataReader reader = null;
            try
            {
                using (Database dbTerminal = new Database())
                {

                    dbTerminal.ProcedureName = "[Extranet.ListarRucCliente]";
                    dbTerminal.AddParameter("@v_vch_Ruc", DbType.String, ParameterDirection.Input, oBE_Refrendo.sRuc);
                    reader = dbTerminal.GetDataReader();

                    List<BE_Refrendo> Lst_BE_Refrendo = new List<BE_Refrendo>();
                    while (reader.Read())
                    {
                        Lst_BE_Refrendo.Add(Pu_Refrendo.ListarREL_Ruc(reader));
                    }
                    reader.Close();


                    return Lst_BE_Refrendo;
                }

            }
            catch (Exception e)
            {
                ResultadoTransaccionTx oResultadoTransaccionTx = null;
                oResultadoTransaccionTx.RegistrarError(e);
                return null;
            }
        }


        public List<BE_Refrendo> ListarDetalleSolicitudRefrendo(BE_Refrendo oBE_Refrendo)
        {
            IDataReader reader = null;
            try
            {
                using (Database dbTerminal = new Database())
                {

                    dbTerminal.ProcedureName = "Extranet_ListarInfoXSolicitudRefrendo";
                    dbTerminal.AddParameter("@V_int_IdSolicitudRefrendo", DbType.Int32, ParameterDirection.Input, oBE_Refrendo.iIdSolicitudRefr);
                    

                    reader = dbTerminal.GetDataReader();

                    List<BE_Refrendo> Lst_BE_Refrendo = new List<BE_Refrendo>();
                    while (reader.Read())
                    {
                        Lst_BE_Refrendo.Add(Pu_Refrendo.ListarDetalleSolicitudRefrendo(reader));
                    }
                    reader.Close();
                   

                    return Lst_BE_Refrendo;
                }

            }
            catch (Exception e)
            {
                ResultadoTransaccionTx oResultadoTransaccionTx = null;
                oResultadoTransaccionTx.RegistrarError(e);
                return null;
            }
        }
        
         public List<BE_Refrendo> ListarEstadosRefrendo(BE_Refrendo oBE_Refrendo)
        {
            IDataReader reader = null;
            try
            {
                using (Database dbTerminal = new Database())
                {

                    dbTerminal.ProcedureName = "Listar_EstadosRefrendo";

                    reader = dbTerminal.GetDataReader();

                    List<BE_Refrendo> Lst_BE_Refrendo = new List<BE_Refrendo>();
                    while (reader.Read())
                    {
                        Lst_BE_Refrendo.Add(Pu_Refrendo.ListarEstadosRefrendo(reader));
                    }
                    reader.Close();

                    return Lst_BE_Refrendo;
                }

            }
            catch (Exception e)
            {
                ResultadoTransaccionTx oResultadoTransaccionTx = null;
                oResultadoTransaccionTx.RegistrarError(e);
                return null;
            }
        }



        public List<BE_Refrendo> ListarArchivosXClienteUsuario(BE_Refrendo oBE_Refrendo)
        {
            IDataReader reader = null;
            try
            {
                using (Database dbTerminal = new Database())
                {

                    dbTerminal.ProcedureName = "Extranet_ArchivosXClienteUsuario";
                    dbTerminal.AddParameter("@Vi_int_IdClienteExtranet", DbType.Int32, ParameterDirection.Input, oBE_Refrendo.IdCliente);
                    dbTerminal.AddParameter("@Vi_vch_Usuario", DbType.String, ParameterDirection.Input, oBE_Refrendo.sUsuario);

                    reader = dbTerminal.GetDataReader();

                    List<BE_Refrendo> Lst_BE_Refrendo = new List<BE_Refrendo>();
                    while (reader.Read())
                    {
                        Lst_BE_Refrendo.Add(Pu_Refrendo.ListarArchivosXClienteUsuario(reader));
                    }
                    reader.Close();

                    return Lst_BE_Refrendo;
                }

            }
            catch (Exception e)
            {
                ResultadoTransaccionTx oResultadoTransaccionTx = null;
                oResultadoTransaccionTx.RegistrarError(e);
                return null;
            }
        }


        public Tuple<List<BE_Refrendo>, string> Extranet_Ins_ArchivosRefrendo(BE_Refrendo oBE_Refrendo)
        {
            string returnMsj = "";
            IDataReader reader = null;
            try
            {

                using (Database dbTerminal = new Database())
                {

                    dbTerminal.ProcedureName = "[Ext_Ins_ArchivosRefrendo]";

                    dbTerminal.AddParameter("@V_int_IdClienteExtranet", DbType.Int32, ParameterDirection.Input, oBE_Refrendo.IdCliente);
                    dbTerminal.AddParameter("@Archivo_vch_Documento", DbType.String, ParameterDirection.Input, oBE_Refrendo.sNombreArchivo);
                    dbTerminal.AddParameter("@Archivo_vch_Ruta", DbType.String, ParameterDirection.Input, oBE_Refrendo.sRutaArchivo);
                    dbTerminal.AddParameter("@v_int_IdUsuarioExtranet", DbType.Int32, ParameterDirection.Input, oBE_Refrendo.iIdusuario);
                    dbTerminal.AddParameter("@v_int_IdTipoDocumento", DbType.Int32, ParameterDirection.Input, oBE_Refrendo.iIdTipoDocumento);
                    dbTerminal.AddParameter("@UsuarioRegistro", DbType.String, ParameterDirection.Input, oBE_Refrendo.sUsuario);
                    dbTerminal.AddParameter("@Retorno", DbType.String, ParameterDirection.Output, String.Empty, 1000);
                    reader = dbTerminal.GetDataReader();

                    List<BE_Refrendo> oBE_List = new List<BE_Refrendo>();


                    while (reader.Read())
                    {
                        oBE_List.Add((Pu_Refrendo.ListarDatosXBooking_Refrendo(reader)));
                    }
                    reader.Close();
                    //oBE_List = null;


                    returnMsj = dbTerminal.GetParameter("@Retorno").ToString();

                    return Tuple.Create(oBE_List, returnMsj);

                }

            }
            catch (Exception e)
            {
                return null;

            }
        }

        public String Insertar_RefrendoElectronico(BE_Refrendo oBE_Refrendo, List<BE_Refrendo> oBE_RefrendoList)
        {
            String Retorno = string.Empty;
            DbTransaction oTx = null;
            int vl_iCodigo = 0;
            int vl_iCodigodOrdSerDet = 0;
            int vl_IdOrdSerDet = 0;
            String vl_sMessage = string.Empty;
            String vl_sResultado = string.Empty;

            using (Database dbTerminal = new Database())
            {
                try
                {
                    dbTerminal.ProcedureName = "[EXTRANET_Ins_SolicitudRefrendo]";
                    oTx = dbTerminal.Transaction();
                    dbTerminal.AddTransaction(oTx);
                    dbTerminal.AddParameter("@V_Int_Docori", DbType.Int32, ParameterDirection.Input, oBE_Refrendo.IidDocori);
                    dbTerminal.AddParameter("@V_Int_Booking", DbType.Int32, ParameterDirection.Input, oBE_Refrendo.IdBooking);
                    dbTerminal.AddParameter("@V_int_IdExportador", DbType.Int32, ParameterDirection.Input, oBE_Refrendo.IdExportador);
                    dbTerminal.AddParameter("@V_int_IdAgenciaAduanas", DbType.Int32, ParameterDirection.Input, oBE_Refrendo.IdAgenciaAguanas);
                    dbTerminal.AddParameter("@V_int_IdClienteExtranet", DbType.Int32, ParameterDirection.Input, oBE_Refrendo.IdCliente);
                    dbTerminal.AddParameter("@V_vch_RucFacturar", DbType.String, ParameterDirection.Input, oBE_Refrendo.sRuc);
                    dbTerminal.AddParameter("@V_int_IdTipoPago", DbType.Int32, ParameterDirection.Input, oBE_Refrendo.iIdTipoPago);
                    dbTerminal.AddParameter("@V_int_IdFormaPago", DbType.Int32, ParameterDirection.Input, oBE_Refrendo.iIdFormaPago);
                    dbTerminal.AddParameter("@V_vch_IdBancoOrigen", DbType.String, ParameterDirection.Input, oBE_Refrendo.sIdBancoOrigen);
                    dbTerminal.AddParameter("@V_vch_IdBancoDestino", DbType.String, ParameterDirection.Input, oBE_Refrendo.sIdBancoDestino);
                    dbTerminal.AddParameter("@V_dec_Monto", DbType.Decimal, ParameterDirection.Input, oBE_Refrendo.dMonto);
                    dbTerminal.AddParameter("@V_vch_NroOperacion", DbType.String, ParameterDirection.Input, oBE_Refrendo.sNroOperacion);
                    dbTerminal.AddParameter("@v_IdLiquidacion", DbType.Int32, ParameterDirection.Input, oBE_Refrendo.iIdLiquidacion);
                    dbTerminal.AddParameter("@V_int_Moneda", DbType.Int32, ParameterDirection.Input, oBE_Refrendo.iIdMoneda);
                    dbTerminal.AddParameter("@Refrendo_vch_ComentariosCliente", DbType.String, ParameterDirection.Input, oBE_Refrendo.sComentarios);                    
                    dbTerminal.AddParameter("@V_vch_UsuarioRegistra", DbType.String, ParameterDirection.Input, oBE_Refrendo.sUsuario);
                    dbTerminal.AddParameter("@v_IdDespachador", DbType.Int32, ParameterDirection.Input, oBE_Refrendo.iIdDespachador);
                    dbTerminal.AddParameter("@V_vch_Retorno", DbType.String, ParameterDirection.Output, string.Empty, 200);

                    dbTerminal.ExecuteTransaction();

                    String[] xx = dbTerminal.GetParameter("@V_vch_Retorno").ToString().Split('|'); ;

                    vl_sResultado = xx[0] + "| " + xx[1];
                    oBE_Refrendo.iIdSolicitudRefr = Convert.ToInt32(xx[0]);

                    if (oBE_Refrendo.iIdSolicitudRefr > 0)
                    {
                        oTx.Commit();
                    }
                    else
                    {
                        oTx.Rollback();
                        return vl_sResultado;
                    }

                    dbTerminal.ParameterClear();
                  // oTx = dbTerminal.Transaction();
                    for (int i = 0; i < oBE_RefrendoList.Count; i++)
                    {
                        dbTerminal.ProcedureName = null;
                        dbTerminal.ProcedureName = "EXTRANET_Ins_SolicitudRefrendoDetalle";                       
                       // dbTerminal.AddTransaction(oTx);
                        dbTerminal.AddParameter("@Vi_int_IdSolicRefre", DbType.Int32, ParameterDirection.Input, oBE_Refrendo.iIdSolicitudRefr);
                        dbTerminal.AddParameter("@Vi_int_IdArchRefrendo", DbType.Int32, ParameterDirection.Input, oBE_RefrendoList[i].iIdArchivoRefrendo);
                        dbTerminal.AddParameter("@Vi_int_IdTipoDoc", DbType.Int32, ParameterDirection.Input, oBE_RefrendoList[i].iIdTipoDocumento);
                        dbTerminal.AddParameter("@Vi_vch_NombreDoc", DbType.String, ParameterDirection.Input, oBE_RefrendoList[i].sNombreArchivo);
                        dbTerminal.AddParameter("@Vi_vch_Ruta", DbType.String, ParameterDirection.Input, oBE_RefrendoList[i].sRutaArchivo);
                        dbTerminal.AddParameter("@Vi_vch_UsuarioRegistra", DbType.String, ParameterDirection.Input, oBE_Refrendo.sUsuario);
                        dbTerminal.AddParameter("@Vi_int_IdClienteExtranet", DbType.Int32, ParameterDirection.Input, oBE_Refrendo.IdCliente);
                        dbTerminal.AddParameter("@Vo_Retorno", DbType.String, ParameterDirection.Output, string.Empty, 200);

                        dbTerminal.ExecuteTransaction();
                       
                        int vl_iCodRes = 0;
                        String[] vl_Res = dbTerminal.GetParameter("@Vo_Retorno").ToString().Split('|');
                        //for (int j = 0; j < vl_Res.Length; j++)
                        //{
                        //    if (j == 0)
                        //    {
                        //        vl_iCodRes = Convert.ToInt32(vl_Res[j]);
                        //    }
                        //    else
                        //    {
                        //        vl_sMessage = vl_Res[j];
                        //    }
                        //}

                        //if (vl_iCodRes == -1)
                        //{
                        //    oTx.Rollback();
                        //    return vl_sMessage;
                        //}
                        //else
                            dbTerminal.ParameterClear();

                    }
                  // oTx.Commit();
                    return vl_sResultado;

                }
                catch (Exception e)
                {
                    oTx.Rollback();
                    return "-1|Ocurrio un error en la capa de Datos";
                }
            }
        }

        public String Actualizar_BultosDetalleRefrendo(BE_Refrendo oBE_Refrendo)
        {
          
            String vl_sMessage = string.Empty;
            String vl_sResultado = string.Empty;

            using (Database dbTerminal = new Database())
            {
                try
                {
                    dbTerminal.ProcedureName = "[Extranet_Upd_BultosDetalleRefrendo]";                    
                    dbTerminal.AddParameter("@v_IdDocOriDet", DbType.Int32, ParameterDirection.Input, oBE_Refrendo.iIdDocOridet);
                    dbTerminal.AddParameter("@V_IdDocOri", DbType.Int32, ParameterDirection.Input, oBE_Refrendo.IidDocori);
                    dbTerminal.AddParameter("@v_UsuarioModifica", DbType.String, ParameterDirection.Input, oBE_Refrendo.sUsuario);
                    dbTerminal.AddParameter("@v_Bultos", DbType.String, ParameterDirection.Input, oBE_Refrendo.sBultos);
                    dbTerminal.AddParameter("@v_Retorno", DbType.String, ParameterDirection.Output, string.Empty, 200);

                    dbTerminal.ExecuteTransaction();

                    String[] xx = dbTerminal.GetParameter("@v_Retorno").ToString().Split('|'); ;

                    vl_sResultado = xx[0] + "| " + xx[1];
                    return vl_sResultado;

                }
                catch (Exception e)
                {                   
                    return "-1|Ocurrio un error en la capa de Datos";
                }
            }
        }

        public String ValidarCarga(BE_Refrendo oBE_Refrendo)
        {

            String vl_sMessage = string.Empty;
            String vl_sResultado = string.Empty;

            using (Database dbTerminal = new Database())
            {
                try
                {
                    dbTerminal.ProcedureName = "[Extranet_REL_ValidarCargaEnFargo]";
                    dbTerminal.AddParameter("@v_int_idDocOri", DbType.Int32, ParameterDirection.Input, oBE_Refrendo.iIdDocori);                   
                    dbTerminal.AddParameter("@v_Retorno", DbType.String, ParameterDirection.Output, string.Empty, 400);

                    dbTerminal.ExecuteTransaction();

                    String[] xx = dbTerminal.GetParameter("@v_Retorno").ToString().Split('|'); ;

                    // vl_sResultado = xx[0] + "| " + xx[1];
                    //vl_sResultado = xx.ToString();
                    // return vl_sResultado;
                    return dbTerminal.GetParameter("@v_Retorno").ToString();

                }
                catch (Exception e)
                {
                    return "-1|Ocurrio un error en la capa de Datos";
                }
            }
        }

        public String ValidarRUCCliente_ParaDespachador(BE_Refrendo oBE_Refrendo)
        {


            using (Database dbTerminal = new Database())
            {
                try
                {
                    dbTerminal.ProcedureName = "[EXTRANET_VALIDARUC_LISTARDESPACHOR]";
                    dbTerminal.AddParameter("@V_IdCliente", DbType.Int32, ParameterDirection.Input, oBE_Refrendo.IdCliente);
                    dbTerminal.AddParameter("@v_Retorno", DbType.String, ParameterDirection.Output, string.Empty, 400);

                    dbTerminal.ExecuteTransaction();

                    String[] xx = dbTerminal.GetParameter("@v_Retorno").ToString().Split('|');

                    // vl_sResultado = xx[0] + "| " + xx[1];
                    //vl_sResultado = xx.ToString();
                    // return vl_sResultado;
                    return dbTerminal.GetParameter("@v_Retorno").ToString();

                }
                catch (Exception e)
                {
                    return "-1|Ocurrio un error en la capa de Datos";
                }
            }
        }

        public String Insertar_BultosPrecintoDetalleRefrendo(BE_Refrendo oBE_Refrendo)
        {

            String vl_sMessage = string.Empty;
            String vl_sResultado = string.Empty;

            using (Database dbTerminal = new Database())
            {
                try
                {
                    dbTerminal.ProcedureName = "[Extranet_Ins_BultosPrecintosDetalleRefrendo]";
                    dbTerminal.AddParameter("@v_IdSolicitudRefr", DbType.Int32, ParameterDirection.Input, oBE_Refrendo.iIdSolicitudRefr);
                    dbTerminal.AddParameter("@v_IdDocOriDet", DbType.Int32, ParameterDirection.Input, oBE_Refrendo.iIdDocOridet);
                    dbTerminal.AddParameter("@V_IdDocOri", DbType.Int32, ParameterDirection.Input, oBE_Refrendo.IidDocori);
                    dbTerminal.AddParameter("@v_PrecintoAduanas", DbType.String, ParameterDirection.Input, oBE_Refrendo.sPrecintoAduanas);
                    dbTerminal.AddParameter("@v_PrecintoLinea", DbType.String, ParameterDirection.Input, oBE_Refrendo.sPrecintoLinea);
                    dbTerminal.AddParameter("@v_UsuarioModifica", DbType.String, ParameterDirection.Input, oBE_Refrendo.sUsuario);
                    dbTerminal.AddParameter("@v_Bultos", DbType.String, ParameterDirection.Input, oBE_Refrendo.sBultos);
                    dbTerminal.AddParameter("@v_Retorno", DbType.String, ParameterDirection.Output, string.Empty, 200);

                    dbTerminal.ExecuteTransaction();

                    String[] xx = dbTerminal.GetParameter("@v_Retorno").ToString().Split('|'); ;

                    vl_sResultado = xx[0] + "| " + xx[1];
                    return vl_sResultado;

                }
                catch (Exception e)
                {
                    return "-1|Ocurrio un error en la capa de Datos";
                }
            }
        }



    }
}


