﻿using System;
using System.Collections.Generic;
using System.Text;
using FENIX.BusinessEntity;
using FENIX.Common;
using System.Data.Common;
using System.Data;

namespace FENIX.DataAccess
{
    public class DA_Liquidacion
    {
        #region "Transaccion"
        public int RegistrarServicios_Mandatorios(int? idContrato, int? iIdVolante, int? iIdDocOri, int IdOperacion)
        {
            using (Database dbTerminal = new Database())
            {

                try
                {
                    dbTerminal.ProcedureName = "Comercial_Ins_Registra_OrdenServicio_Automatico";
                    dbTerminal.AddParameter("@vi_IdDocOri", DbType.Int32, ParameterDirection.Input, iIdDocOri);
                    dbTerminal.AddParameter("@vi_IdOperacion", DbType.Int32, ParameterDirection.Input, IdOperacion);
                    dbTerminal.AddParameter("@vi_IDcontrato", DbType.Int32, ParameterDirection.Input, idContrato);
                    dbTerminal.AddParameter("@vi_IdVolante", DbType.Int32, ParameterDirection.Input, iIdVolante);

                    dbTerminal.AddParameter("@vo_IdOrdSer", DbType.Int32, ParameterDirection.Output, 0);

                    dbTerminal.ExecuteTransaction();

                    int Retorno = Convert.ToInt32(dbTerminal.GetParameter("@vo_IdOrdSer").ToString());

                    return Retorno;
                }
                catch (Exception e)
                {
                    ResultadoTransaccionTx oResultadoTransaccionTx = new ResultadoTransaccionTx(ResultadoTransaccionTx.TipoResultado.Error, e);
                    oResultadoTransaccionTx.RegistrarError(e);
                    return 0;
                }
            }
        }
        public void RegistrarDescuento(int iTipo, decimal iValor, int IdOrdSer, int IdOrdSerDet, decimal dMontoAfecto)
        {
            using (Database dbTerminal = new Database())
            {
                try
                {
                    dbTerminal.ProcedureName = "Facturacion_Upd_Descuento";

                    dbTerminal.AddParameter("@vi_Tipo", DbType.Int32, ParameterDirection.Input, iTipo);
                    dbTerminal.AddParameter("@vi_Valor", DbType.Decimal, ParameterDirection.Input, iValor);

                    dbTerminal.AddParameter("@vi_IdOrdSer", DbType.Int32, ParameterDirection.Input, IdOrdSer);
                    dbTerminal.AddParameter("@vi_IdOrdSerDet", DbType.Int32, ParameterDirection.Input, IdOrdSerDet);
                    dbTerminal.AddParameter("@vi_MontoAfecto", DbType.Decimal, ParameterDirection.Input, dMontoAfecto);


                    dbTerminal.ExecuteTransaction();

                }
                catch (Exception e)
                {
                    ResultadoTransaccionTx oResultadoTransaccionTx = new ResultadoTransaccionTx(ResultadoTransaccionTx.TipoResultado.Error, e);
                    oResultadoTransaccionTx.RegistrarError(e);

                }
            }
        }
        public String RegistrarLiquidacion(BE_Liquidacion oBE_Liquidacion, BE_LiquidacionList oBE_LiquidacionLis)
        {
            String Retorno = string.Empty;
            DbTransaction oTx = null;
            int vl_iCodigo = 0;
            String vl_sMessage = string.Empty;
            String vl_sResultado = string.Empty;

            using (Database dbTerminal = new Database())
            {
                try
                {

                    dbTerminal.ProcedureName = "Facturacion_Ins_Liquidacion";
                    oTx = dbTerminal.Transaction();
                    dbTerminal.AddTransaction(oTx);
                    dbTerminal.AddParameter("@vi_idClienteFacturar", DbType.Int32, ParameterDirection.Input, oBE_Liquidacion.iIdClienteFacturar);
                    dbTerminal.AddParameter("@vi_Observacion", DbType.String, ParameterDirection.Input, oBE_Liquidacion.sObservacion);
                    dbTerminal.AddParameter("@vi_IdDocOri", DbType.Int32, ParameterDirection.Input, oBE_Liquidacion.iIdDocOri);
                    dbTerminal.AddParameter("@vi_IdContrato", DbType.Int32, ParameterDirection.Input, oBE_Liquidacion.iIdContrato);
                    dbTerminal.AddParameter("@vi_IdMoneda", DbType.Int32, ParameterDirection.Input, oBE_Liquidacion.iIdMoneda);
                    dbTerminal.AddParameter("@vi_IdOperacion", DbType.Int32, ParameterDirection.Input, oBE_Liquidacion.iIdOperacion);

                    dbTerminal.AddParameter("@vi_TotDescuento", DbType.Decimal, ParameterDirection.Input, oBE_Liquidacion.dTotDescuento);
                    dbTerminal.AddParameter("@vi_TotIgv", DbType.Decimal, ParameterDirection.Input, oBE_Liquidacion.dTotIgv);
                    dbTerminal.AddParameter("@vi_TotSubTotal", DbType.Decimal, ParameterDirection.Input, oBE_Liquidacion.dTotSubTotal);
                    dbTerminal.AddParameter("@vi_TotTotal", DbType.Decimal, ParameterDirection.Input, oBE_Liquidacion.dTotTotal);
                    dbTerminal.AddParameter("@vi_IdAgencia", DbType.Int32, ParameterDirection.Input, oBE_Liquidacion.iIdAgAduana);
                    dbTerminal.AddParameter("@vi_TotDetraccion", DbType.Decimal, ParameterDirection.Input, oBE_Liquidacion.dTotDetraccion);

                    dbTerminal.AddParameter("@Usuario", DbType.String, ParameterDirection.Input, oBE_Liquidacion.sUsuario);
                    dbTerminal.AddParameter("@NombrePc", DbType.String, ParameterDirection.Input, oBE_Liquidacion.sNombrePc);
                    dbTerminal.AddParameter("@vo_resultado", DbType.String, ParameterDirection.Output, string.Empty, 300);
                    dbTerminal.ExecuteTransaction();

                    String[] xx = dbTerminal.GetParameter("@vo_resultado").ToString().Split('|'); ;

                    for (int i = 0; i < xx.Length; i++)
                    {
                        if (i == 0)
                        {
                            vl_iCodigo = Convert.ToInt32(xx[i]);
                        }
                        else
                        {
                            vl_sMessage += xx[i];
                        }
                    }

                    vl_sResultado = xx[0] + "| " + xx[1];
                    oBE_Liquidacion.iIdLiq = vl_iCodigo;


                    dbTerminal.ParameterClear();

                    for (int i = 0; i < oBE_LiquidacionLis.Count; i++)
                    {

                        dbTerminal.ProcedureName = null;
                        dbTerminal.ProcedureName = "[Facturacion_Ins_liquidacion_Detalle]";
                        dbTerminal.AddTransaction(oTx);
                        dbTerminal.AddParameter("@vi_CantidadLiquidada", DbType.Decimal, ParameterDirection.Input, oBE_LiquidacionLis[i].dCantidad);
                        dbTerminal.AddParameter("@vi_PesoLiquidado", DbType.Decimal, ParameterDirection.Input, oBE_LiquidacionLis[i].dPeso);
                        dbTerminal.AddParameter("@vi_VolumenLiquidado", DbType.Decimal, ParameterDirection.Input, oBE_LiquidacionLis[i].dVolumen);
                        dbTerminal.AddParameter("@vi_Impuesto", DbType.Decimal, ParameterDirection.Input, oBE_LiquidacionLis[i].dIgv);
                        dbTerminal.AddParameter("@vi_MontoAfecto", DbType.Decimal, ParameterDirection.Input, oBE_LiquidacionLis[i].dAfecto);
                        dbTerminal.AddParameter("@vi_MontoInafecto", DbType.Decimal, ParameterDirection.Input, oBE_LiquidacionLis[i].dInafecto);
                        dbTerminal.AddParameter("@vi_Detraccion", DbType.Decimal, ParameterDirection.Input, oBE_LiquidacionLis[i].dDetraccion);
                        dbTerminal.AddParameter("@vi_MontoBruto", DbType.Decimal, ParameterDirection.Input, oBE_LiquidacionLis[i].dMontoBruto);
                        dbTerminal.AddParameter("@vi_MontoNeto", DbType.Decimal, ParameterDirection.Input, oBE_LiquidacionLis[i].dMontoNeto);
                        dbTerminal.AddParameter("@vi_Descuento", DbType.Decimal, ParameterDirection.Input, oBE_LiquidacionLis[i].dDescuento);
                        dbTerminal.AddParameter("@vi_IdLiq", DbType.Int32, ParameterDirection.Input, oBE_Liquidacion.iIdLiq);
                        dbTerminal.AddParameter("@vi_IdOrdSer", DbType.Int32, ParameterDirection.Input, oBE_LiquidacionLis[i].iIdOrdSer);
                        dbTerminal.AddParameter("@vi_IdOrdSerDet", DbType.Int32, ParameterDirection.Input, oBE_LiquidacionLis[i].iIdOrdSerDet);
                        dbTerminal.AddParameter("@vi_IdDua", DbType.String, ParameterDirection.Input, oBE_LiquidacionLis[i].sIdDua);
                        dbTerminal.AddParameter("@vi_IdDocOri", DbType.Int32, ParameterDirection.Input, oBE_LiquidacionLis[i].iIdDocOri);
                        dbTerminal.AddParameter("@vi_IdDocOriDet", DbType.Int32, ParameterDirection.Input, oBE_LiquidacionLis[i].iIdDocOriDet);
                        dbTerminal.AddParameter("@vi_IdTarifa", DbType.Int32, ParameterDirection.Input, oBE_LiquidacionLis[i].iIdTarifa);
                        dbTerminal.AddParameter("@vi_Origen", DbType.String, ParameterDirection.Input, oBE_LiquidacionLis[i].sOrigen);
                        if (oBE_LiquidacionLis[i].sFechaAlmaIni != null)
                            dbTerminal.AddParameter("@vi_Liq_dt_FechaAlmaIni", DbType.DateTime, ParameterDirection.Input, Convert.ToDateTime(oBE_LiquidacionLis[i].sFechaAlmaIni));
                        else
                            dbTerminal.AddParameter("@vi_Liq_dt_FechaAlmaIni", DbType.DateTime, ParameterDirection.Input, null);

                        if (oBE_LiquidacionLis[i].sFechaAlmaFin != null)
                            dbTerminal.AddParameter("@vi_Liq_dt_FechaAlmaFin", DbType.DateTime, ParameterDirection.Input, Convert.ToDateTime(oBE_LiquidacionLis[i].sFechaAlmaFin));
                        else
                            dbTerminal.AddParameter("@vi_Liq_dt_FechaAlmaFin", DbType.DateTime, ParameterDirection.Input, null);

                        dbTerminal.AddParameter("@Usuario", DbType.String, ParameterDirection.Input, oBE_LiquidacionLis[i].sUsuario);
                        dbTerminal.AddParameter("@NombrePc", DbType.String, ParameterDirection.Input, oBE_LiquidacionLis[i].sNombrePc);
                        dbTerminal.AddParameter("@vo_resultado", DbType.String, ParameterDirection.Output, string.Empty, 100);
                        dbTerminal.AddParameter("@vi_LiqDet_Num_MontoTarifa", DbType.Decimal, ParameterDirection.Input, oBE_LiquidacionLis[i].dMontoTarifa);


                        dbTerminal.ExecuteTransaction();

                        vl_sResultado = dbTerminal.GetParameter("@vo_Resultado").ToString();

                        dbTerminal.ParameterClear();
                    }
                    oTx.Commit();
                    return vl_sResultado;

                }
                catch (Exception e)
                {
                    ResultadoTransaccionTx oResultadoTransaccionTx = new ResultadoTransaccionTx(ResultadoTransaccionTx.TipoResultado.Error, e);
                    oResultadoTransaccionTx.RegistrarError(e);
                    oTx.Rollback();
                    return "-1|Ocurrio un error en la capa de Datos";
                }
            }
        }
        public String ActualizarLiquidacion(Int32 iIdLiq, Int32 iIdClienteFacturar)
        {
            using (Database dbTerminal = new Database())
            {
                try
                {
                    dbTerminal.ProcedureName = "Facturacion_Upd_Liquidacion";
                    dbTerminal.AddParameter("@vi_IdLiq", DbType.Int32, ParameterDirection.Input, iIdLiq);
                    dbTerminal.AddParameter("@vi_IdClienteFacturar", DbType.Int32, ParameterDirection.Input, iIdClienteFacturar);
                    dbTerminal.AddParameter("@vo_Retorno", DbType.String, ParameterDirection.Output, string.Empty, 300);
                    dbTerminal.Execute();
                    return dbTerminal.GetParameter("@vo_Retorno").ToString();
                }
                catch (Exception e)
                {
                    ResultadoTransaccionTx oResultadoTransaccionTx = new ResultadoTransaccionTx(ResultadoTransaccionTx.TipoResultado.Error, e);
                    oResultadoTransaccionTx.RegistrarError(e);
                    return "-1|Ocurrio un error en la capa de Datos";
                }
            }
        }
        public void ActualizarNroCuenta(Int32 IdDocOri, String sNroCuenta)
        {
            using (Database dbTerminal = new Database())
            {
                try
                {
                    dbTerminal.ProcedureName = "Facturacion_Upd_Liquidacion_NroCuenta";
                    dbTerminal.AddParameter("@vi_IdDocOri", DbType.Int32, ParameterDirection.Input, IdDocOri);
                    dbTerminal.AddParameter("@vi_NroCuenta", DbType.String, ParameterDirection.Input, sNroCuenta);
                    dbTerminal.Execute();

                }
                catch (Exception e)
                {
                    ResultadoTransaccionTx oResultadoTransaccionTx = new ResultadoTransaccionTx(ResultadoTransaccionTx.TipoResultado.Error, e);
                    oResultadoTransaccionTx.RegistrarError(e);
                }
            }
        }
        public String EliminarLiquidacion(int iIdLiquidacion, string sUsuario, string sNombrePc)
        {
            String vl_sResultado = string.Empty;

            using (Database dbTerminal = new Database())
            {

                try
                {
                    dbTerminal.ProcedureName = "Facturacion_Del_Liquidacion";

                    dbTerminal.AddParameter("@vi_IdLiquidacion", DbType.Int32, ParameterDirection.Input, iIdLiquidacion);
                    dbTerminal.AddParameter("@Usuario", DbType.String, ParameterDirection.Input, sUsuario);
                    dbTerminal.AddParameter("@NombrePc", DbType.String, ParameterDirection.Input, sNombrePc);

                    dbTerminal.AddParameter("@vo_Retorno", DbType.String, ParameterDirection.Output, string.Empty, 1000);

                    dbTerminal.ExecuteTransaction();

                    vl_sResultado = dbTerminal.GetParameter("@vo_Retorno").ToString();

                    return vl_sResultado;
                }
                catch (Exception e)
                {
                    ResultadoTransaccionTx oResultadoTransaccionTx = new ResultadoTransaccionTx(ResultadoTransaccionTx.TipoResultado.Error, e);
                    oResultadoTransaccionTx.RegistrarError(e);
                    return vl_sResultado;
                }
            }

        }

        public String Insertar_PreLiquidacion(BE_Liquidacion oBE_Liquidacion, List<BE_Liquidacion> lst_BE_Liquidacion)
        {
            String Retorno = string.Empty;
            DbTransaction oTx = null;
            using (Database dbTerminal = new Database())
            {
                try
                {

                    dbTerminal.ProcedureName = "facturacion_ins_preLiquidacion";
                    oTx = dbTerminal.Transaction();
                    dbTerminal.AddTransaction(oTx);
                    dbTerminal.AddParameter("@vi_IdMoneda", DbType.Int32, ParameterDirection.Input, oBE_Liquidacion.iIdMoneda);                    
                    dbTerminal.AddParameter("@vi_IdOperacion", DbType.Int32, ParameterDirection.Input, oBE_Liquidacion.iIdOperacion);
                    dbTerminal.AddParameter("@vi_IdContrato", DbType.Int32, ParameterDirection.Input, oBE_Liquidacion.iIdContrato);
                    dbTerminal.AddParameter("@vi_PreLiq_vch_NumeroVolante", DbType.String, ParameterDirection.Input, oBE_Liquidacion.sNroVolante);
                    dbTerminal.AddParameter("@vi_PreLiq_dt_FechaAlmacenaje", DbType.DateTime, ParameterDirection.Input, oBE_Liquidacion.dtFechaRetiro);
                    dbTerminal.AddParameter("@vi_PreLiq_vch_NumeroContrato", DbType.String, ParameterDirection.Input, oBE_Liquidacion.sNroContrato);
                    dbTerminal.AddParameter("@vi_PreLiq_vch_Usuario", DbType.String, ParameterDirection.Input, oBE_Liquidacion.sUsuario);
                    dbTerminal.AddParameter("@vi_PreLiq_vch_NombrePc", DbType.String, ParameterDirection.Input, oBE_Liquidacion.sNombrePc);
                    dbTerminal.AddParameter("@vo_Retorno", DbType.String, ParameterDirection.Output, String.Empty, 100);

                    dbTerminal.ExecuteTransaction();

                    String[] xx = dbTerminal.GetParameter("@vo_Retorno").ToString().Split('|'); ;
                    int vl_iCodigo = 0;
                    String vl_sMessage = string.Empty;
                    String vl_sResultado = string.Empty;


                    for (int i = 0; i < xx.Length; i++)
                    {
                        if (i == 0)
                        {
                            vl_iCodigo = Convert.ToInt32(xx[i]);
                        }
                        else
                        {
                            vl_sMessage += xx[i];
                        }
                    }

                    if (vl_iCodigo< 1)
                    {
                        oTx.Rollback();
                        return "-1|Ocurrio un error en la capa de Datos";
                    }

                    vl_sResultado = xx[0] + "| " + xx[1];
                   

                    dbTerminal.ParameterClear();

                    for (int i = 0; i < lst_BE_Liquidacion.Count; i++)
                    {

                        dbTerminal.ProcedureName = null;
                        dbTerminal.ProcedureName = "[Facturacion_Ins_PreLiquidacionDetalle]";                        
                        dbTerminal.AddParameter("@vi_IdPreLiq", DbType.Int32, ParameterDirection.Input, vl_iCodigo);
                        dbTerminal.AddParameter("@vi_IdOrdSer", DbType.Int32, ParameterDirection.Input, lst_BE_Liquidacion[i].iIdOrdSer);
                        dbTerminal.AddParameter("@vi_IdOrdSerDet", DbType.Int32, ParameterDirection.Input, lst_BE_Liquidacion[i].iIdOrdSerDet);                                                
                        dbTerminal.AddParameter("@vi_IdTarifa", DbType.Int32, ParameterDirection.Input, lst_BE_Liquidacion[i].iIdTarifa);
                        dbTerminal.AddParameter("@vi_IdModalidad", DbType.Int32, ParameterDirection.Input, Convert.ToInt32(lst_BE_Liquidacion[i].sModalidad));
                        dbTerminal.AddParameter("@vi_PreLiqDet_vch_Origen", DbType.String, ParameterDirection.Input, lst_BE_Liquidacion[i].sOrigen);
                        dbTerminal.AddParameter("@vi_PreLiqDet_dec_Monto", DbType.Decimal, ParameterDirection.Input, lst_BE_Liquidacion[i].dMontoBruto);
                        dbTerminal.AddParameter("@vi_PreLiqDet_dec_Cantidad", DbType.Decimal, ParameterDirection.Input, lst_BE_Liquidacion[i].dCantidad);
                        dbTerminal.AddParameter("@vi_PreLiqDet_dec_Afecto", DbType.Decimal, ParameterDirection.Input, lst_BE_Liquidacion[i].dAfecto);
                        dbTerminal.AddParameter("@vi_PreLiqDet_dec_Detraccion", DbType.Decimal, ParameterDirection.Input, lst_BE_Liquidacion[i].dDetraccion);
                        dbTerminal.AddParameter("@vi_PreLiqDet_dec_Impuesto", DbType.Decimal, ParameterDirection.Input, lst_BE_Liquidacion[i].dIgv);
                        dbTerminal.AddParameter("@vi_PreLiqDet_dec_Total", DbType.Decimal, ParameterDirection.Input, lst_BE_Liquidacion[i].dTotal);
                        dbTerminal.AddParameter("@vi_PreLiqDet_vch_Usuario", DbType.String, ParameterDirection.Input, lst_BE_Liquidacion[i].sUsuario);
                        dbTerminal.AddParameter("@vi_PreLiqDet_vch_NombrePc", DbType.String, ParameterDirection.Input, lst_BE_Liquidacion[i].sNombrePc);
                        dbTerminal.AddParameter("@vo_Retorno", DbType.String, ParameterDirection.Output, string.Empty, 200);

                        dbTerminal.ExecuteTransaction();

                        Retorno = dbTerminal.GetParameter("@vo_Retorno").ToString();

                        for (int j = 0; j < xx.Length; j++)
                        {
                            if (i == 0)
                            {
                                vl_iCodigo = Convert.ToInt32(xx[i]);
                                if (vl_iCodigo < 1)
                                {
                                    oTx.Rollback();
                                    return "-1|Ocurrio un error en la capa de Datos";
                                }
                            }                           
                        }

                        dbTerminal.ParameterClear();
                    }
                    oTx.Commit();
                    return vl_sResultado;
                }
                catch (Exception e)
                {
                    ResultadoTransaccionTx oResultadoTransaccionTx = new ResultadoTransaccionTx(ResultadoTransaccionTx.TipoResultado.Error, e);
                    oResultadoTransaccionTx.RegistrarError(e);
                    oTx.Rollback();
                    return "-1|Ocurrio un error en la capa de Datos";
                }
            }
        }


        #endregion

        #region "Listado"
        public BE_Liquidacion Listar_PreLiquidacion_Info_Cabecera(String sNumeroVolante, Int32 iIdOperacion, Int32? iIdDocOri, Int32? iIdNavVia)
        {
            IDataReader reader = null;

            try
            {

                using (Database dbTerminal = new Database())
                {

                    dbTerminal.ProcedureName = "[Facturacion_Lis_InfoPreLiquidacion]";
                    dbTerminal.AddParameter("@vi__vch_NumeroVolante", DbType.String, ParameterDirection.Input, sNumeroVolante);
                    dbTerminal.AddParameter("@vi_IdOperacion", DbType.Int32, ParameterDirection.Input, iIdOperacion);
                    dbTerminal.AddParameter("@vi_IdDocOri", DbType.Int32, ParameterDirection.Input, iIdDocOri);
                    dbTerminal.AddParameter("@vi_IdNavVia", DbType.Int32, ParameterDirection.Input, iIdNavVia);


                    reader = dbTerminal.GetDataReader();

                    BE_LiquidacionList oBE_Preliquidacion = new BE_LiquidacionList();
                    while (reader.Read())
                    {
                        oBE_Preliquidacion.Add(Listar_Info_PorID(reader));
                    }
                    reader.Close();

                    return oBE_Preliquidacion[0];
                }

            }
            catch (Exception e)
            {
                ResultadoTransaccionTx oResultadoTransaccionTx = new ResultadoTransaccionTx(ResultadoTransaccionTx.TipoResultado.Error, e);
                oResultadoTransaccionTx.RegistrarError(e);
                return null;

            }
        }
        public BE_LiquidacionList Listar_PreLinquidacion(BE_Liquidacion oBE_Liquidacion)
        {
            IDataReader reader = null;

            try
            {

                using (Database dbTerminal = new Database())
                {

                    dbTerminal.ProcedureName = "[Facturacion_Lis_PreLinquidacion]";
                    dbTerminal.AddParameter("@vin_IdLineaNegocio", DbType.Int32, ParameterDirection.Input, oBE_Liquidacion.iIdLineaNegocio);
                    dbTerminal.AddParameter("@Vin_IdVolante", DbType.String, ParameterDirection.Input, oBE_Liquidacion.sNroVolante);
                    dbTerminal.AddParameter("@Vin_IdDocOri", DbType.Int32, ParameterDirection.Input, oBE_Liquidacion.iIdDocOri);
                    dbTerminal.AddParameter("@vin_Fecha_Liquidacion", DbType.DateTime, ParameterDirection.Input, oBE_Liquidacion.dtFecha_Liquidacion);
                    dbTerminal.AddParameter("@vin_IdTipoMoneda", DbType.Int32, ParameterDirection.Input, oBE_Liquidacion.iIdMoneda);
                    dbTerminal.AddParameter("@vin_IdOperacion", DbType.Int32, ParameterDirection.Input, oBE_Liquidacion.iIdOperacion);
                    dbTerminal.AddParameter("@vin_IdContrato", DbType.Int32, ParameterDirection.Input, oBE_Liquidacion.iIdContrato);

                    reader = dbTerminal.GetDataReader();

                    BE_LiquidacionList oBE_Preliquidacion = new BE_LiquidacionList();
                    while (reader.Read())
                    {
                        oBE_Preliquidacion.Add(Listar_PreLiquidacion(reader));
                    }
                    reader.Close();

                    return oBE_Preliquidacion;
                }

            }
            catch (Exception e)
            {
                ResultadoTransaccionTx oResultadoTransaccionTx = new ResultadoTransaccionTx(ResultadoTransaccionTx.TipoResultado.Error, e);
                oResultadoTransaccionTx.RegistrarError(e);
                return null;

            }
        }

        public BE_LiquidacionList Listar_Linquidacion_Servicios(BE_Liquidacion oBE_Liquidacion)
        {
            IDataReader reader = null;

            try
            {

                using (Database dbTerminal = new Database())
                {

                    dbTerminal.ProcedureName = "[Facturacion_Lis_Linquidacion]";
                    dbTerminal.AddParameter("@vin_IdLineaNegocio", DbType.Int32, ParameterDirection.Input, oBE_Liquidacion.iIdLineaNegocio);
                    dbTerminal.AddParameter("@Vin_IdVolante", DbType.Int32, ParameterDirection.Input, oBE_Liquidacion.iIdVolante);
                    dbTerminal.AddParameter("@Vin_IdDocOri", DbType.Int32, ParameterDirection.Input, oBE_Liquidacion.iIdDocOri);
                    dbTerminal.AddParameter("@vin_Fecha_Liquidacion", DbType.DateTime, ParameterDirection.Input, oBE_Liquidacion.dtFecha_Liquidacion);
                    dbTerminal.AddParameter("@vin_IdTipoMoneda", DbType.Int32, ParameterDirection.Input, oBE_Liquidacion.iIdMoneda);
                    dbTerminal.AddParameter("@vin_IdOperacion", DbType.Int32, ParameterDirection.Input, oBE_Liquidacion.iIdOperacion);
                    dbTerminal.AddParameter("@vin_IdContrato", DbType.Int32, ParameterDirection.Input, oBE_Liquidacion.iIdContrato);
                    dbTerminal.AddParameter("@vin_idDocOriDet", DbType.String, ParameterDirection.Input, oBE_Liquidacion.sIdDocOriDet);

                    reader = dbTerminal.GetDataReader();

                    BE_LiquidacionList oBE_Preliquidacion = new BE_LiquidacionList();
                    while (reader.Read())
                    {
                        oBE_Preliquidacion.Add(Listar_Liquidacion_Servicios_Pop(reader));
                    }
                    reader.Close();

                    return oBE_Preliquidacion;
                }

            }
            catch (Exception e)
            {
                ResultadoTransaccionTx oResultadoTransaccionTx = new ResultadoTransaccionTx(ResultadoTransaccionTx.TipoResultado.Error, e);
                oResultadoTransaccionTx.RegistrarError(e);
                return null;

            }
        }

        public BE_LiquidacionList Listar_Linquidacion_Servicios_Registrado(int idLiquidacion)
        {
            IDataReader reader = null;

            try
            {

                using (Database dbTerminal = new Database())
                {

                    dbTerminal.ProcedureName = "[Facturacion_Lis_Linquidacion_Registrado]";

                    dbTerminal.AddParameter("@vi_idLiq", DbType.Int32, ParameterDirection.Input, idLiquidacion);

                    reader = dbTerminal.GetDataReader();

                    BE_LiquidacionList oBE_Preliquidacion = new BE_LiquidacionList();
                    while (reader.Read())
                    {
                        oBE_Preliquidacion.Add(Listar_Liquidacion_Servicios_Pop(reader));
                    }
                    reader.Close();

                    return oBE_Preliquidacion;
                }

            }
            catch (Exception e)
            {
                ResultadoTransaccionTx oResultadoTransaccionTx = new ResultadoTransaccionTx(ResultadoTransaccionTx.TipoResultado.Error, e);
                oResultadoTransaccionTx.RegistrarError(e);
                return null;

            }
        }


        public BE_LiquidacionList Listar_Linquidacion(BE_Liquidacion oBE_Liquidacion)
        {
            IDataReader reader = null;

            try
            {

                using (Database dbTerminal = new Database())
                {

                    dbTerminal.ProcedureName = "[Facturacion_Lis_Liquidacion]";
                    dbTerminal.AddParameter("@vi_LineaNegocio", DbType.Int32, ParameterDirection.Input, oBE_Liquidacion.iIdLineaNegocio);
                    dbTerminal.AddParameter("@vi_idLiq", DbType.String, ParameterDirection.Input, oBE_Liquidacion.iIdLiq);
                    dbTerminal.AddParameter("@vi_Cliente", DbType.String, ParameterDirection.Input, oBE_Liquidacion.sCliente);
                    dbTerminal.AddParameter("@vi_ClienteFacturar", DbType.String, ParameterDirection.Input, oBE_Liquidacion.sClienteFacturar);
                    dbTerminal.AddParameter("@vi_FechaIni", DbType.DateTime, ParameterDirection.Input, oBE_Liquidacion.dtFechaIni);
                    dbTerminal.AddParameter("@vi_FechaFin", DbType.DateTime, ParameterDirection.Input, oBE_Liquidacion.dtFechaFin);
                    dbTerminal.AddParameter("@vi_NumManifiesto", DbType.String, ParameterDirection.Input, oBE_Liquidacion.sNumManifiesto);
                    dbTerminal.AddParameter("@vi_AnoManifiesto", DbType.String, ParameterDirection.Input, oBE_Liquidacion.sAnnoMfto);
                    dbTerminal.AddParameter("@vi_AgenciaAduana", DbType.String, ParameterDirection.Input, oBE_Liquidacion.sAgAduana);
                    dbTerminal.AddParameter("@vi_NumeroDocumento", DbType.String, ParameterDirection.Input, oBE_Liquidacion.sNumeroDo);
                    dbTerminal.AddParameter("@vi_numeropagina", DbType.Int32, ParameterDirection.Input, oBE_Liquidacion.NPagina);
                    dbTerminal.AddParameter("@vi_numeroregistros", DbType.Int32, ParameterDirection.Input, oBE_Liquidacion.NRegistros);
                    dbTerminal.AddParameter("@vo_totalregistros", DbType.Int32, ParameterDirection.Output, oBE_Liquidacion.NTotalRegistros);


                    reader = dbTerminal.GetDataReader();

                    BE_LiquidacionList oBE_Preliquidacion = new BE_LiquidacionList();
                    while (reader.Read())
                    {
                        oBE_Preliquidacion.Add(Listar_Liquidacion(reader));

                    }
                    reader.Close();
                    oBE_Liquidacion.NTotalRegistros = Convert.ToInt32(dbTerminal.GetParameter("@vo_totalregistros").ToString());
                    return oBE_Preliquidacion;
                }

            }
            catch (Exception e)
            {
                ResultadoTransaccionTx oResultadoTransaccionTx = new ResultadoTransaccionTx(ResultadoTransaccionTx.TipoResultado.Error, e);
                oResultadoTransaccionTx.RegistrarError(e);
                return null;

            }
        }

        public BE_LiquidacionList Listar_Info_DocumentoOrigen(BE_Liquidacion oBE_Liquidacion)
        {
            IDataReader reader = null;

            try
            {

                using (Database dbTerminal = new Database())
                {

                    dbTerminal.ProcedureName = "[EXTRANET_ADM_Lis_InfoPreLiquidacion_2]";
                    dbTerminal.AddParameter("@vi_IdLineaNegocio", DbType.Int32, ParameterDirection.Input, oBE_Liquidacion.iIdLineaNegocio);
                    dbTerminal.AddParameter("@vi_IdOperacion", DbType.Int32, ParameterDirection.Input, oBE_Liquidacion.iIdOperacion);
                    dbTerminal.AddParameter("@vi_NumeroDocumento", DbType.String, ParameterDirection.Input, oBE_Liquidacion.sDocOrigen);
                    dbTerminal.AddParameter("@vi_MasterLCL", DbType.String, ParameterDirection.Input, 0);
                    dbTerminal.AddParameter("@vi_Oficina", DbType.Int32, ParameterDirection.Input, 1);
             

                    reader = dbTerminal.GetDataReader();

                    BE_LiquidacionList oBE_Preliquidacion = new BE_LiquidacionList();
                    while (reader.Read())
                    {
                        oBE_Preliquidacion.Add(Listar_Info_DocumentoOrigen(reader));

                    }
                    reader.Close();
                    //oBE_Liquidacion.NTotalRegistros = Convert.ToInt32(dbTerminal.GetParameter("@vo_totalregistros").ToString());
                    return oBE_Preliquidacion;
                }

            }
            catch (Exception e)
            {
                ResultadoTransaccionTx oResultadoTransaccionTx = new ResultadoTransaccionTx(ResultadoTransaccionTx.TipoResultado.Error, e);
                oResultadoTransaccionTx.RegistrarError(e);
                return null;

            }
        }

        public BE_LiquidacionList Listar_Contenedores_Liquidar(BE_Liquidacion oBE_Liquidacion)
        {
            IDataReader reader = null;

            try
            {

                using (Database dbTerminal = new Database())
                {

                    dbTerminal.ProcedureName = "[EXTRANET_Lis_Liquidar_ContendoresXVolante]";                    
                    dbTerminal.AddParameter("@vi_NumeroDocumento", DbType.String, ParameterDirection.Input, oBE_Liquidacion.sNroVolante);
                    
                    reader = dbTerminal.GetDataReader();

                    BE_LiquidacionList oBE_Preliquidacion = new BE_LiquidacionList();
                    while (reader.Read())
                    {
                        oBE_Preliquidacion.Add(Listar_Contenedores_Liquidar(reader));

                    }
                    reader.Close();
                    //oBE_Liquidacion.NTotalRegistros = Convert.ToInt32(dbTerminal.GetParameter("@vo_totalregistros").ToString());
                    return oBE_Preliquidacion;
                }

            }
            catch (Exception e)
            {
                ResultadoTransaccionTx oResultadoTransaccionTx = new ResultadoTransaccionTx(ResultadoTransaccionTx.TipoResultado.Error, e);
                oResultadoTransaccionTx.RegistrarError(e);
                return null;

            }
        }

        public String Listar_Recuperar_Contrato_2(Int32? iIdNegocio, Int32? iIdVolante, int? iIdDocOri, int iIdOperacion, String sMasterLCL)
        {
            IDataReader reader = null;
            String sRetorno = string.Empty;
            try
            {
                using (Database dbTerminal = new Database())
                {
                    dbTerminal.ProcedureName = "[EXTRANET_COM_Lis_RecuperarContrato_2]";
                    dbTerminal.AddParameter("@vi_Negocio", DbType.Int32, ParameterDirection.Input, iIdNegocio);
                    dbTerminal.AddParameter("@vi_IdOperacion", DbType.Int32, ParameterDirection.Input, iIdOperacion);
                    dbTerminal.AddParameter("@vi_IdDocOri", DbType.Int32, ParameterDirection.Input, iIdDocOri);
                    dbTerminal.AddParameter("@vi_IdVolante", DbType.Int32, ParameterDirection.Input, iIdVolante);
                    dbTerminal.AddParameter("@vi_sMasterLCL", DbType.String, ParameterDirection.Input, sMasterLCL);
                    dbTerminal.AddParameter("@vo_NroAcuerdo", DbType.String, ParameterDirection.Output, string.Empty, 100);
                    dbTerminal.AddParameter("@vo_sCliente", DbType.String, ParameterDirection.Output, string.Empty, 300);
                    dbTerminal.AddParameter("@vo_sObservacion", DbType.String, ParameterDirection.Output, string.Empty, 3000);
                    dbTerminal.AddParameter("@vo_IdAcuerdo", DbType.Int32, ParameterDirection.Output, 0);

                    reader = dbTerminal.GetDataReader();

                    sRetorno = dbTerminal.GetParameter("@vo_NroAcuerdo").ToString() + "|" +
                               dbTerminal.GetParameter("@vo_sCliente").ToString() + "|" +
                               dbTerminal.GetParameter("@vo_sObservacion").ToString() + "|" +
                               dbTerminal.GetParameter("@vo_IdAcuerdo").ToString();

                    return sRetorno;
                }
            }
            catch (Exception e)
            {
                return "-99|Ocurrio un error en la capa de datos : Listar_Recuperar_Contrato_2()";
            }
        }


        public Int32 RegistrarServicios_Mandatorios_2(Int32? iIdLineaNegocio, Int32? iIdOperacion, Int32? iIdDocOri, Int32? iIdAcuerdo, Int32? iIdVolante, String sMasterLCl)
        {
            using (Database dbTerminal = new Database())
            {
                try
                {
                    dbTerminal.ProcedureName = "[EXTRANET_COM_Ins_Registra_OrdenServicio_Automatico_2]";
                    dbTerminal.AddParameter("@vi_IdSuperNegocio", DbType.Int32, ParameterDirection.Input, iIdLineaNegocio);
                    dbTerminal.AddParameter("@vi_IdOperacion", DbType.Int32, ParameterDirection.Input, iIdOperacion);
                    dbTerminal.AddParameter("@vi_IdDocOri", DbType.Int32, ParameterDirection.Input, iIdDocOri);
                    dbTerminal.AddParameter("@vi_IdAcuerdo", DbType.Int32, ParameterDirection.Input, iIdAcuerdo);
                    dbTerminal.AddParameter("@vi_IdVolante", DbType.Int32, ParameterDirection.Input, iIdVolante);
                    dbTerminal.AddParameter("@vi_MasterLCL", DbType.String, ParameterDirection.Input, sMasterLCl);
                    dbTerminal.AddParameter("@vo_IdOrdSer", DbType.Int32, ParameterDirection.Output, 0);

                    dbTerminal.ExecuteTransaction();

                    int Retorno = Convert.ToInt32(dbTerminal.GetParameter("@vo_IdOrdSer").ToString());

                    return Retorno;
                }
                catch (Exception e)
                {
                    return -99;
                }
            }
        }

        public String Actualizar_FechaRetiroXContenedor(Int32? IdDocOriDet, DateTime? FechaRetiro)
        {
            using (Database dbTerminal = new Database())
            {
                try
                {
                    dbTerminal.ProcedureName = "[EXTRANET_Upd_OrdSer_PreLiquidar]";
                    dbTerminal.AddParameter("@IdDocoriDet", DbType.Int32, ParameterDirection.Input, IdDocOriDet);
                    dbTerminal.AddParameter("@d_FechaRetiro", DbType.DateTime, ParameterDirection.Input, FechaRetiro);
            
                    dbTerminal.AddParameter("@vo_Retorno", DbType.String, ParameterDirection.Output, 30);

                    dbTerminal.ExecuteTransaction();

                    String Retorno =dbTerminal.GetParameter("@vo_Retorno").ToString();

                    return Retorno;
                }
                catch (Exception e)
                {
                    return "-99";
                }
            }
        }

        public BE_LiquidacionList Listar_PreLiquidacion_2(BE_Liquidacion oBE_Liquidacion)
        {
            IDataReader reader = null;

            try
            {
                using (Database dbTerminal = new Database())
                {
                    dbTerminal.ProcedureName = "[EXTRANET_ADM_Lis_PreLiquidacion_4]";
                    dbTerminal.AddParameter("@vi_IdPreLiquidacion", DbType.Int32, ParameterDirection.Input, oBE_Liquidacion.iIdLiq);
                    dbTerminal.AddParameter("@vi_IdSuperNegocio", DbType.Int32, ParameterDirection.Input, oBE_Liquidacion.iIdSuperNegocio_2);
                    dbTerminal.AddParameter("@Vi_IdVolante", DbType.String, ParameterDirection.Input, oBE_Liquidacion.sNroVolante);
                    dbTerminal.AddParameter("@Vi_IdDocOri", DbType.Int32, ParameterDirection.Input, oBE_Liquidacion.iIdDocOri);
                    dbTerminal.AddParameter("@vi_Fecha_Liquidacion", DbType.DateTime, ParameterDirection.Input, oBE_Liquidacion.dtFecha_Liquidacion);
                    dbTerminal.AddParameter("@vi_IdTipoMoneda", DbType.Int32, ParameterDirection.Input, oBE_Liquidacion.iIdMoneda);
                    dbTerminal.AddParameter("@vi_IdOperacion", DbType.Int32, ParameterDirection.Input, oBE_Liquidacion.iIdOperacion);
                    dbTerminal.AddParameter("@vi_IdAcuerdo", DbType.Int32, ParameterDirection.Input, oBE_Liquidacion.iIdAcuerdo);
                    dbTerminal.AddParameter("@vi_FlagMasterLCL", DbType.String, ParameterDirection.Input, oBE_Liquidacion.sMasterLCL);
                    dbTerminal.AddParameter("@vi_TipoMonedaGrabar", DbType.Int32, ParameterDirection.Input, oBE_Liquidacion.idMonedaGrabar);
                    dbTerminal.AddParameter("@vi_TipoMonedaGrabar2", DbType.Int32, ParameterDirection.Input, oBE_Liquidacion.idMonedaGrabar2);

                    reader = dbTerminal.GetDataReader();

                    BE_LiquidacionList oBE_Preliquidacion = new BE_LiquidacionList();
                    while (reader.Read())
                    {
                        oBE_Preliquidacion.Add(Listar_PreLiquidacion_2(reader));
                    }
                    reader.Close();

                    return oBE_Preliquidacion;
                }
            }
            catch (Exception e)
            {
                return null;
            }
        }







        public BE_LiquidacionList Listar_Linquidacion_Pendiente(BE_Liquidacion oBE_Liquidacion)
        {
            IDataReader reader = null;

            try
            {

                using (Database dbTerminal = new Database())
                {

                    dbTerminal.ProcedureName = "[Facturacion_Lis_Liquidacion_Pendiente_Facturar]";

                    dbTerminal.AddParameter("@vi_FechaIni", DbType.DateTime, ParameterDirection.Input, oBE_Liquidacion.dtFechaIni);
                    dbTerminal.AddParameter("@vi_FechaFin", DbType.DateTime, ParameterDirection.Input, oBE_Liquidacion.dtFechaFin);



                    reader = dbTerminal.GetDataReader();

                    BE_LiquidacionList oBE_Preliquidacion = new BE_LiquidacionList();
                    while (reader.Read())
                    {
                        oBE_Preliquidacion.Add(Listar_Liquidacion(reader));

                    }
                    reader.Close();

                    return oBE_Preliquidacion;
                }

            }
            catch (Exception e)
            {
                ResultadoTransaccionTx oResultadoTransaccionTx = new ResultadoTransaccionTx(ResultadoTransaccionTx.TipoResultado.Error, e);
                oResultadoTransaccionTx.RegistrarError(e);
                return null;

            }
        }

        //public BE_DuaList Listar_Dua_Detalle(String sIdCodigo, Int32 iIdOperacion)
        //{
        //    IDataReader reader = null;

        //    try
        //    {

        //        using (Database dbTerminal = new Database())
        //        {

        //            dbTerminal.ProcedureName = "[Facturacion_Lis_DuaDetalle]";
        //            dbTerminal.AddParameter("@vi_IdOperacion", DbType.String, ParameterDirection.Input, iIdOperacion);
        //            dbTerminal.AddParameter("@vi_IdCodigo", DbType.String, ParameterDirection.Input, sIdCodigo);


        //            reader = dbTerminal.GetDataReader();

        //            BE_DuaList oBE_DuaList = new BE_DuaList();
        //            while (reader.Read())
        //            {
        //                oBE_DuaList.Add(Listar_Dua_Detalle(reader));
        //            }
        //            reader.Close();

        //            return oBE_DuaList;
        //        }

        //    }
        //    catch (Exception e)
        //    {
        //        ResultadoTransaccionTx oResultadoTransaccionTx = new ResultadoTransaccionTx(ResultadoTransaccionTx.TipoResultado.Error, e);
        //        oResultadoTransaccionTx.RegistrarError(e);
        //        return null;

        //    }
        //}

        //public BE_DuaList Listar_Dua_DocOrigen(Int32 iIdDocOri)
        //{
        //    IDataReader reader = null;

        //    try
        //    {

        //        using (Database dbTerminal = new Database())
        //        {

        //            dbTerminal.ProcedureName = "[Facturacion_lis_Dua_Por_DocumentoOrigen]";
        //            dbTerminal.AddParameter("@vi_IdDocOri", DbType.Int32, ParameterDirection.Input, iIdDocOri);


        //            reader = dbTerminal.GetDataReader();

        //            BE_DuaList oBE_DuaList = new BE_DuaList();
        //            while (reader.Read())
        //            {
        //                oBE_DuaList.Add(Listar_Dua_DocOri(reader));
        //            }
        //            reader.Close();

        //            return oBE_DuaList;
        //        }

        //    }
        //    catch (Exception e)
        //    {
        //        ResultadoTransaccionTx oResultadoTransaccionTx = new ResultadoTransaccionTx(ResultadoTransaccionTx.TipoResultado.Error, e);
        //        oResultadoTransaccionTx.RegistrarError(e);
        //        return null;

        //    }
        //}

        //public BE_DuaList Listar_Dua_DocOrigen_Registrado(Int32 iIdDocOri)
        //{
        //    IDataReader reader = null;

        //    try
        //    {

        //        using (Database dbTerminal = new Database())
        //        {

        //            dbTerminal.ProcedureName = "[Facturacion_lis_Dua_Por_DocumentoOrigen_Registrado]";
        //            dbTerminal.AddParameter("@vi_IdDocOri", DbType.Int32, ParameterDirection.Input, iIdDocOri);


        //            reader = dbTerminal.GetDataReader();

        //            BE_DuaList oBE_DuaList = new BE_DuaList();
        //            while (reader.Read())
        //            {
        //                oBE_DuaList.Add(Listar_Dua_DocOri(reader));
        //            }
        //            reader.Close();

        //            return oBE_DuaList;
        //        }

        //    }
        //    catch (Exception e)
        //    {
        //        ResultadoTransaccionTx oResultadoTransaccionTx = new ResultadoTransaccionTx(ResultadoTransaccionTx.TipoResultado.Error, e);
        //        oResultadoTransaccionTx.RegistrarError(e);
        //        return null;

        //    }
        //}


        public BE_LiquidacionList Listar_Buscar_Info_Liquidacion(Int32 iIdCodigo, Int32 iIdOperacion)
        {
            IDataReader reader = null;

            try
            {

                using (Database dbTerminal = new Database())
                {

                    dbTerminal.ProcedureName = "[Facturacion_lis_Buscar_Por_Volante]";
                    dbTerminal.AddParameter("@vi_IdCodigo", DbType.Int32, ParameterDirection.Input, iIdCodigo);
                    dbTerminal.AddParameter("@vi_IdOperacion", DbType.Int32, ParameterDirection.Input, iIdOperacion);


                    reader = dbTerminal.GetDataReader();

                    BE_LiquidacionList oBE_LiquidacionList = new BE_LiquidacionList();
                    while (reader.Read())
                    {
                        oBE_LiquidacionList.Add(Listar_Por_Volante(reader));
                    }
                    reader.Close();

                    return oBE_LiquidacionList;
                }

            }
            catch (Exception e)
            {
                ResultadoTransaccionTx oResultadoTransaccionTx = new ResultadoTransaccionTx(ResultadoTransaccionTx.TipoResultado.Error, e);
                oResultadoTransaccionTx.RegistrarError(e);
                return null;

            }
        }

        public BE_LiquidacionList Listar_Liquidacion_PorId(Int32 idLiq)
        {
            IDataReader reader = null;

            try
            {

                using (Database dbTerminal = new Database())
                {

                    dbTerminal.ProcedureName = "[Facturacion_Lis_liquidacion_PorId]";
                    dbTerminal.AddParameter("@vi_IdLiq", DbType.Int32, ParameterDirection.Input, idLiq);


                    reader = dbTerminal.GetDataReader();

                    BE_LiquidacionList oBE_LiquidacionList = new BE_LiquidacionList();
                    while (reader.Read())
                    {
                        oBE_LiquidacionList.Add(Listar_Por_PorId(reader));
                    }
                    reader.Close();

                    return oBE_LiquidacionList;
                }

            }
            catch (Exception e)
            {
                ResultadoTransaccionTx oResultadoTransaccionTx = new ResultadoTransaccionTx(ResultadoTransaccionTx.TipoResultado.Error, e);
                oResultadoTransaccionTx.RegistrarError(e);
                return null;

            }
        }

        public String Validar_TipoCambio()
        {
            IDataReader reader = null;
            try
            {
                using (Database dbTerminal = new Database())
                {
                    dbTerminal.ProcedureName = "[Facturacion_Valida_TipoCambio]";
                    dbTerminal.AddParameter("@vo_resultado", DbType.String, ParameterDirection.Output, String.Empty, 100);
                    reader = dbTerminal.GetDataReader();
                    return dbTerminal.GetParameter("@vo_resultado").ToString();
                }
            }
            catch (Exception e)
            {
                ResultadoTransaccionTx oResultadoTransaccionTx = new ResultadoTransaccionTx(ResultadoTransaccionTx.TipoResultado.Error, e);
                oResultadoTransaccionTx.RegistrarError(e);
                return "-3|Ocurrio un error en la capa de datos : Validar_TipoCambio()";
            }
        }

        public string Listar_Contrato(Int32? iIdVolante, int? iIdDocOri, int iIdOperacion)
        {
            IDataReader reader = null;
            string sRetorno = string.Empty;
            try
            {


                using (Database dbTerminal = new Database())
                {

                    dbTerminal.ProcedureName = "[Comercial_Lis_RecuperarContrato]";
                    dbTerminal.AddParameter("@Vin_IdVolante", DbType.Int32, ParameterDirection.Input, iIdVolante);
                    dbTerminal.AddParameter("@vin_IdDocOri", DbType.Int32, ParameterDirection.Input, iIdDocOri);
                    dbTerminal.AddParameter("@vin_IdOperacion", DbType.Int32, ParameterDirection.Input, iIdOperacion);

                    dbTerminal.AddParameter("@vo_NroContrato", DbType.String, ParameterDirection.Output, string.Empty, 100);
                    dbTerminal.AddParameter("@vo_sCliente", DbType.String, ParameterDirection.Output, string.Empty, 100);
                    dbTerminal.AddParameter("@vo_sObservacion", DbType.String, ParameterDirection.Output, string.Empty, 1000);
                    dbTerminal.AddParameter("@vo_IdContrato", DbType.Int32, ParameterDirection.Output, 0);


                    reader = dbTerminal.GetDataReader();

                    sRetorno = dbTerminal.GetParameter("@vo_NroContrato").ToString() + "|" +
                               dbTerminal.GetParameter("@vo_sCliente").ToString() + "|" +
                               dbTerminal.GetParameter("@vo_sObservacion").ToString() + "|" +
                               dbTerminal.GetParameter("@vo_IdContrato").ToString();

                    return sRetorno;
                }

            }
            catch (Exception e)
            {
                ResultadoTransaccionTx oResultadoTransaccionTx = new ResultadoTransaccionTx(ResultadoTransaccionTx.TipoResultado.Error, e);
                oResultadoTransaccionTx.RegistrarError(e);
                return string.Empty;

            }
        }

        public string Validar_NumeroDo(String sNumeroDo)
        {
            IDataReader reader = null;
            try
            {
                using (Database dbTerminal = new Database())
                {
                    dbTerminal.ProcedureName = "[Facturacion_Lis_Valida_Booking_Por_Nave]";
                    dbTerminal.AddParameter("@vi_NumeroDo", DbType.String, ParameterDirection.Input, sNumeroDo);
                    dbTerminal.AddParameter("@vo_Resultado", DbType.String, ParameterDirection.Output, String.Empty, 100);
                    reader = dbTerminal.GetDataReader();
                    return dbTerminal.GetParameter("@vo_Resultado").ToString();
                }

            }
            catch (Exception e)
            {
                ResultadoTransaccionTx oResultadoTransaccionTx = new ResultadoTransaccionTx(ResultadoTransaccionTx.TipoResultado.Error, e);
                oResultadoTransaccionTx.RegistrarError(e);
                return string.Empty;

            }
        }



        #endregion

        #region "publicacion"

        //public static BE_Dua Listar_Dua_DocOri(IDataReader DReader)
        //{
        //    ResultadoTransaccionTx oResultadoTransaccionTx = null;
        //    try
        //    {

        //        BE_Dua oBE_Dua = new BE_Dua();
        //        int indice = 0;

        //        indice = DReader.GetOrdinal("IdDua");
        //        if (!DReader.IsDBNull(indice)) oBE_Dua.iIdDua = DReader.GetInt32(indice);
        //        indice = DReader.GetOrdinal("IdAduana");
        //        if (!DReader.IsDBNull(indice)) oBE_Dua.iIdAduana = DReader.GetString(indice);
        //        indice = DReader.GetOrdinal("Dua_vch_Anio");
        //        if (!DReader.IsDBNull(indice)) oBE_Dua.sAnio = DReader.GetString(indice);
        //        indice = DReader.GetOrdinal("IdRegimen");
        //        if (!DReader.IsDBNull(indice)) oBE_Dua.iIdRegimen = DReader.GetString(indice);
        //        indice = DReader.GetOrdinal("Dua_vch_Numero");
        //        if (!DReader.IsDBNull(indice)) oBE_Dua.sDuaNumero = DReader.GetString(indice);

        //        return oBE_Dua;
        //    }
        //    catch (Exception e)
        //    {
        //        oResultadoTransaccionTx.RegistrarError(e);
        //        return null;
        //    }
        //}

        //public static BE_Dua Listar_Dua_Detalle(IDataReader DReader)
        //{
        //    ResultadoTransaccionTx oResultadoTransaccionTx = null;
        //    try
        //    {

        //        BE_Dua oBE_Dua = new BE_Dua();
        //        int indice = 0;

        //        indice = DReader.GetOrdinal("IdDua");
        //        if (!DReader.IsDBNull(indice)) oBE_Dua.iIdDua = DReader.GetInt32(indice);
        //        indice = DReader.GetOrdinal("IdDocOri");
        //        if (!DReader.IsDBNull(indice)) oBE_Dua.iIdDocOri = DReader.GetInt32(indice);
        //        indice = DReader.GetOrdinal("IdDocOriDet");
        //        if (!DReader.IsDBNull(indice)) oBE_Dua.iIdDocOriDet = DReader.GetInt32(indice);
        //        indice = DReader.GetOrdinal("Contenedor");
        //        if (!DReader.IsDBNull(indice)) oBE_Dua.sContenedor = DReader.GetString(indice);
        //        indice = DReader.GetOrdinal("CondicionDoc");
        //        if (!DReader.IsDBNull(indice)) oBE_Dua.sCondicionCarga = DReader.GetString(indice);
        //        indice = DReader.GetOrdinal("Item");
        //        if (!DReader.IsDBNull(indice)) oBE_Dua.iItem = DReader.GetInt32(indice);
        //        indice = DReader.GetOrdinal("CantidadAutorizado");
        //        if (!DReader.IsDBNull(indice)) oBE_Dua.dCantidadAutorizado = DReader.GetDecimal(indice);
        //        indice = DReader.GetOrdinal("PesoAutorizado");
        //        if (!DReader.IsDBNull(indice)) oBE_Dua.dPesoAutorizado = DReader.GetDecimal(indice);
        //        indice = DReader.GetOrdinal("VolumenAutorizado");
        //        if (!DReader.IsDBNull(indice)) oBE_Dua.dVolumenAutorizado = DReader.GetDecimal(indice);
        //        indice = DReader.GetOrdinal("CantidadDespachado");
        //        if (!DReader.IsDBNull(indice)) oBE_Dua.dCantidadDespachado = DReader.GetDecimal(indice);
        //        indice = DReader.GetOrdinal("PesoDespachado");
        //        if (!DReader.IsDBNull(indice)) oBE_Dua.dPesoDespachado = DReader.GetDecimal(indice);
        //        indice = DReader.GetOrdinal("VolumenDespachado");
        //        if (!DReader.IsDBNull(indice)) oBE_Dua.dVolumenDespachado = DReader.GetDecimal(indice);
        //        indice = DReader.GetOrdinal("CantidadLiquidado");
        //        if (!DReader.IsDBNull(indice)) oBE_Dua.dCantidadLiquidado = DReader.GetDecimal(indice);
        //        indice = DReader.GetOrdinal("Liquidado");
        //        if (!DReader.IsDBNull(indice)) oBE_Dua.sSituacion = DReader.GetString(indice);
        //        indice = DReader.GetOrdinal("ChasisRecibido");
        //        if (!DReader.IsDBNull(indice)) oBE_Dua.sChasis = DReader.GetString(indice);
        //        indice = DReader.GetOrdinal("Embalaje");
        //        if (!DReader.IsDBNull(indice)) oBE_Dua.sEmbalaje = DReader.GetString(indice);
        //        indice = DReader.GetOrdinal("TipoCarga");
        //        if (!DReader.IsDBNull(indice)) oBE_Dua.sCarga = DReader.GetString(indice);



        //        return oBE_Dua;
        //    }
        //    catch (Exception e)
        //    {
        //        oResultadoTransaccionTx.RegistrarError(e);
        //        return null;
        //    }
        //}

        public static BE_Liquidacion Listar_Liquidacion(IDataReader DReader)
        {
            ResultadoTransaccionTx oResultadoTransaccionTx = null;
            try
            {

                BE_Liquidacion oBE_Liquidacion = new BE_Liquidacion();
                int indice = 0;

                indice = DReader.GetOrdinal("idLiq");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.iIdLiq = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("Fecha");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.dtFecha_Liquidacion = DReader.GetDateTime(indice);
                indice = DReader.GetOrdinal("Cliente");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.sCliente = DReader.GetString(indice);
                indice = DReader.GetOrdinal("ClienteFacturado");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.sClienteFacturar = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Nave");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.sNave = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Viaje");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.sviaje = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Rumbo");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.sRumbo = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Operacion");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.sOperacion = DReader.GetString(indice);
                indice = DReader.GetOrdinal("NumeroDO");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.sNumeroDo = DReader.GetString(indice);
                indice = DReader.GetOrdinal("MontoAfecto");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.dAfecto = DReader.GetDecimal(indice);
                indice = DReader.GetOrdinal("Inafecto");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.dInafecto = DReader.GetDecimal(indice);
                indice = DReader.GetOrdinal("ImpuestoVenta");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.dIgv = DReader.GetDecimal(indice);
                indice = DReader.GetOrdinal("MontoBruto");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.dMontoBruto = DReader.GetDecimal(indice);
                indice = DReader.GetOrdinal("MontoNeto");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.dMontoNeto = DReader.GetDecimal(indice);
                indice = DReader.GetOrdinal("Descuento");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.dDescuento = DReader.GetDecimal(indice);


                return oBE_Liquidacion;
            }
            catch (Exception e)
            {
                oResultadoTransaccionTx.RegistrarError(e);
                return null;
            }
        }



        public static BE_Liquidacion Listar_Liquidacion_2(IDataReader DReader)
        {
            try
            {
                BE_Liquidacion oBE_Liquidacion = new BE_Liquidacion();
                int indice = 0;

                indice = DReader.GetOrdinal("IdLiquidacion");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.iIdLiq = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("Fecha");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.sFechaCalculo = DReader.GetString(indice);
                indice = DReader.GetOrdinal("ClienteFacturara");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.sClienteFacturar = DReader.GetString(indice);
                //indice = DReader.GetOrdinal("ClienteFacturado");
                //if (!DReader.IsDBNull(indice)) oBE_Liquidacion.sClienteFacturar = DReader.GetString(indice);
                //indice = DReader.GetOrdinal("Nave");
                //if (!DReader.IsDBNull(indice)) oBE_Liquidacion.sNave = DReader.GetString(indice);
                //indice = DReader.GetOrdinal("Viaje");
                //if (!DReader.IsDBNull(indice)) oBE_Liquidacion.sviaje = DReader.GetString(indice);
                //indice = DReader.GetOrdinal("Rumbo");
                //if (!DReader.IsDBNull(indice)) oBE_Liquidacion.sRumbo = DReader.GetString(indice);
                indice = DReader.GetOrdinal("BL");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.sNumeroDo = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Operacion");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.sOperacion = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Volante");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.sNroVolante = DReader.GetString(indice);
                indice = DReader.GetOrdinal("TipoMoneda");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.sTipoMoneda = DReader.GetString(indice);
                indice = DReader.GetOrdinal("MontoAfecto");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.dAfecto = DReader.GetDecimal(indice);              
                indice = DReader.GetOrdinal("Igv");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.dIgv = DReader.GetDecimal(indice);
                indice = DReader.GetOrdinal("MontoTotal");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.dMontoBruto = DReader.GetDecimal(indice);
                
               
                return oBE_Liquidacion;
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public static BE_Liquidacion Listar_PreLiquidacion_2(IDataReader DReader)
        {
            try
            {
                BE_Liquidacion oBE_Liquidacion = new BE_Liquidacion();
                int indice = 0;

                indice = DReader.GetOrdinal("IdOrdSer");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.iIdOrdSer = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("IdOrdSerDet");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.iIdOrdSerDet = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("Descripcion");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.sDescripcionServicio = DReader.GetString(indice);
                indice = DReader.GetOrdinal("IdDocOriDet");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.iIdDocOriDet = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("Contenedor");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.sContenedor = DReader.GetString(indice);
                indice = DReader.GetOrdinal("TCarga");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.sTipoCarga = DReader.GetString(indice);
                indice = DReader.GetOrdinal("TipoContenedor");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.sTipoContenedor = DReader.GetString(indice);
                indice = DReader.GetOrdinal("IdTamanoCntManifestado");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.iIdTamanoContenedor = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("TipEmb_vch_Descripcion");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.sEmbalaje = DReader.GetString(indice);
                indice = DReader.GetOrdinal("IdTarifa");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.iIdTarifa = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("MontoUnitario");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.dMontoUnitario = DReader.GetDecimal(indice);
                indice = DReader.GetOrdinal("Cantidad");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.dCantidad = DReader.GetDecimal(indice);
                indice = DReader.GetOrdinal("Afecto");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.dAfecto = DReader.GetDecimal(indice);
                indice = DReader.GetOrdinal("Inafecto");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.dInafecto = DReader.GetDecimal(indice);
                indice = DReader.GetOrdinal("Descuento");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.dDescuento = DReader.GetDecimal(indice);
                indice = DReader.GetOrdinal("Detraccion");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.dDetraccion = DReader.GetDecimal(indice);
                indice = DReader.GetOrdinal("Igv");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.dIgv = DReader.GetDecimal(indice);
                indice = DReader.GetOrdinal("Total");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.dTotal = DReader.GetDecimal(indice);
                indice = DReader.GetOrdinal("Modalidad");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.sModalidad = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Situacion");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.sSituacion = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Origen");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.sOrigen = DReader.GetString(indice);
                indice = DReader.GetOrdinal("FechaIngreso");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.sFechaIngreso = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Tarifa");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.sTarifa = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Item");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.iItem = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("DiasLibres");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.iDiasLibres = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("EsRetroactivo");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.sRetroactivo = DReader.GetString(indice);
                indice = DReader.GetOrdinal("FechaRetiro");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.dtFechaRetiro = DReader.GetDateTime(indice);
                indice = DReader.GetOrdinal("PesoDespachado");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.dPeso = DReader.GetDecimal(indice);
                indice = DReader.GetOrdinal("TipoMoneda");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.sTipoMoneda = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Porcent_Detraccion");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.sPorcentDetraccion = DReader.GetString(indice);
                indice = DReader.GetOrdinal("IdTipoServicio");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.iIdTipoServicio_2 = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("IdAsumeEnergia");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.iIdAsumeEnergia_2 = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("PqteSLI");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.sOrigenPqteSLI = DReader.GetString(indice);
                indice = DReader.GetOrdinal("MarcaSLI");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.sMarcaSLI = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Carga");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.sCondCarga = DReader.GetString(indice);

                //indice = DReader.GetOrdinal("AfectoDolares");
                //if (!DReader.IsDBNull(indice)) oBE_Liquidacion.dTotSubTotalDolares = DReader.GetDecimal(indice);
                indice = DReader.GetOrdinal("AfectoNoVisible");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.dAfectoGrabar = DReader.GetDecimal(indice);
                indice = DReader.GetOrdinal("InafectoNoVisible");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.dInafectoGrabar = DReader.GetDecimal(indice);
                indice = DReader.GetOrdinal("DescuentoNoVisible");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.dDescuentoGrabar = DReader.GetDecimal(indice);
                indice = DReader.GetOrdinal("DetraccionNoVisible");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.ddetraccionGrabar = DReader.GetDecimal(indice);
                indice = DReader.GetOrdinal("IgvNoVisible");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.dIgvGrabar = DReader.GetDecimal(indice);
                indice = DReader.GetOrdinal("TotalNoVisible");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.dMontoTotalGrabar = DReader.GetDecimal(indice);
                indice = DReader.GetOrdinal("MontoUnitarioNoVisible");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.dUnitarioGrabar = DReader.GetDecimal(indice);
                indice = DReader.GetOrdinal("MonedaGrabar");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.idMonedaGrabar = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("MontoUnitarioNoVisible2");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.dUnitarioGrabar2 = DReader.GetDecimal(indice);
                indice = DReader.GetOrdinal("AfectoNoVisible2");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.dAfectoGrabar2 = DReader.GetDecimal(indice);
                indice = DReader.GetOrdinal("InafectoNoVisible2");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.dInafectoGrabar2 = DReader.GetDecimal(indice);
                indice = DReader.GetOrdinal("DescuentoNoVisible2");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.dDescuentoGrabar2 = DReader.GetDecimal(indice);
                indice = DReader.GetOrdinal("DetraccionNoVisible2");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.ddetraccionGrabar2 = DReader.GetDecimal(indice);
                indice = DReader.GetOrdinal("IgvNoVisible2");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.dIgvGrabar2 = DReader.GetDecimal(indice);
                indice = DReader.GetOrdinal("TotalNoVisible2");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.dMontoTotalGrabar2 = DReader.GetDecimal(indice);
                indice = DReader.GetOrdinal("MonedaGrabar2");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.idMonedaGrabar2 = DReader.GetInt32(indice);
                return oBE_Liquidacion;
            }
            catch (Exception e)
            {
                return null;
            }
        }


        public static BE_Liquidacion Listar_Info_DocumentoOrigen(IDataReader DReader)
        {
            ResultadoTransaccionTx oResultadoTransaccionTx = null;
            try
            {

                BE_Liquidacion oBE_Liquidacion = new BE_Liquidacion();
                int indice = 0;

                indice = DReader.GetOrdinal("IdDocOri");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.iIdDocOri = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("IdCliente");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.iIdCliente = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("Cliente");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.sCliente = DReader.GetString(indice);
                indice = DReader.GetOrdinal("IdContrato");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.iIdContrato = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("DocOri_vch_NumeroDO");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.sDocOrigen = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Moneda");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.sTipoMoneda = DReader.GetString(indice);
                indice = DReader.GetOrdinal("AgenteAduana");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.sDomentoAgeAdu = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Volante");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.sNroVolante = DReader.GetString(indice);
                indice = DReader.GetOrdinal("IdVolante");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.iIdVolante = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("Operacion");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.iIdOperacion = DReader.GetInt32(indice);


                return oBE_Liquidacion;
            }
            catch (Exception e)
            {
                oResultadoTransaccionTx.RegistrarError(e);
                return null;
            }
        }

        public static BE_Liquidacion Listar_Contenedores_Liquidar(IDataReader DReader)
        {
            ResultadoTransaccionTx oResultadoTransaccionTx = null;
            try
            {

                BE_Liquidacion oBE_Liquidacion = new BE_Liquidacion();
                int indice = 0;

                indice = DReader.GetOrdinal("BL");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.sNumeroDo = DReader.GetString(indice);
                indice = DReader.GetOrdinal("IdDocOriDet");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.iIdDocOriDet = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("Item");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.iItem = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("Contenedor");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.sContenedor = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Tamano");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.iIdTamanoContenedor = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("TipoContenedor");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.sTipoContenedor = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Condicion");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.sCondCarga = DReader.GetString(indice);                
                indice = DReader.GetOrdinal("FechaIngreso");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.sFechaIngreso = DReader.GetString(indice);
                indice = DReader.GetOrdinal("FechaRetiro");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.dtFechaRetiro = DReader.GetDateTime(indice);
                            


                return oBE_Liquidacion;
            }
            catch (Exception e)
            {
                oResultadoTransaccionTx.RegistrarError(e);
                return null;
            }
        }

        public static BE_Liquidacion Listar_Info_PorID(IDataReader DReader)
        {
            ResultadoTransaccionTx oResultadoTransaccionTx = null;
            try
            {
                BE_Liquidacion oBE_Liquidacion = new BE_Liquidacion();
                int indice = 0;

                indice = DReader.GetOrdinal("IdContrato");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.iIdContrato = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("IdDocOri");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.iIdDocOri = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("IdDocOriDet");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.iIdDocOriDet = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("NumeroDo");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.sNumeroDo = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Nave");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.sNave = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Viaje");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.sviaje = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Rumbo");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.sRumbo = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Cliente");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.sCliente = DReader.GetString(indice);
                indice = DReader.GetOrdinal("ClienteContrato");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.sClienteContrato = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Manifiesto");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.sManifiesto = DReader.GetString(indice);
                indice = DReader.GetOrdinal("AgAduana");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.sAgAduana = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Mensaje");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.sMensaje = DReader.GetString(indice);

                return oBE_Liquidacion;
            }
            catch (Exception e)
            {
                oResultadoTransaccionTx.RegistrarError(e);
                return null;
            }
        }
        public static BE_Liquidacion Listar_PreLiquidacion(IDataReader DReader)
        {
            ResultadoTransaccionTx oResultadoTransaccionTx = null;
            try
            {


                BE_Liquidacion oBE_Liquidacion = new BE_Liquidacion();
                int indice = 0;

                indice = DReader.GetOrdinal("IdOrdSer");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.iIdOrdSer = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("IdOrdSerDet");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.iIdOrdSerDet = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("Descripcion");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.sDescripcionServicio = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Contenedor");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.sContenedor = DReader.GetString(indice);
                indice = DReader.GetOrdinal("TCarga");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.sTipoCarga = DReader.GetString(indice);
                indice = DReader.GetOrdinal("TipoContenedor");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.sTipoContenedor = DReader.GetString(indice);
                indice = DReader.GetOrdinal("IdTamanoCntManifestado");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.iIdTamanoContenedor = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("TipEmb_vch_Descripcion");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.sEmbalaje = DReader.GetString(indice);
                indice = DReader.GetOrdinal("IdTarifa");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.iIdTarifa = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("MontoUnitario");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.dMontoUnitario = DReader.GetDecimal(indice);
                indice = DReader.GetOrdinal("Cantidad");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.dCantidad = DReader.GetDecimal(indice);
                indice = DReader.GetOrdinal("Afecto");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.dAfecto = DReader.GetDecimal(indice);
                indice = DReader.GetOrdinal("Inafecto");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.dInafecto = DReader.GetDecimal(indice);
                indice = DReader.GetOrdinal("Descuento");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.dDescuento = DReader.GetDecimal(indice);
                indice = DReader.GetOrdinal("Detraccion");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.dDetraccion = DReader.GetDecimal(indice);
                indice = DReader.GetOrdinal("Igv");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.dIgv = DReader.GetDecimal(indice);
                indice = DReader.GetOrdinal("Total");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.dTotal = DReader.GetDecimal(indice);
                indice = DReader.GetOrdinal("Modalidad");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.sModalidad = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Situacion");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.sSituacion = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Origen");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.sOrigen = DReader.GetString(indice);
                indice = DReader.GetOrdinal("FechaIngreso");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.dtFecha_Ingreso = DReader.GetDateTime(indice);
                indice = DReader.GetOrdinal("Tarifa");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.sTarifa = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Item");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.iItem = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("DiasLibres");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.iDiasLibres = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("EsRetroactivo");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.sRetroactivo = DReader.GetString(indice);
                indice = DReader.GetOrdinal("FechaRetiro");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.dtFechaRetiro = DReader.GetDateTime(indice);
                indice = DReader.GetOrdinal("TipoMoneda");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.sTipoMoneda = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Porcent_Detraccion");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.sPorcentDetraccion = DReader.GetString(indice);


                return oBE_Liquidacion;
            }
            catch (Exception e)
            {
                oResultadoTransaccionTx.RegistrarError(e);
                return null;
            }
        }
        public static BE_Liquidacion Listar_Liquidacion_Servicios_Pop(IDataReader DReader)
        {
            ResultadoTransaccionTx oResultadoTransaccionTx = null;
            try
            {


                BE_Liquidacion oBE_Liquidacion = new BE_Liquidacion();
                int indice = 0;

                indice = DReader.GetOrdinal("IdDocOri");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.iIdDocOri = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("IdDocOriDet");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.iIdDocOriDet = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("IdOrdSer");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.iIdOrdSer = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("IdOrdSerDet");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.iIdOrdSerDet = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("Descripcion");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.sDescripcionServicio = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Contenedor");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.sContenedor = DReader.GetString(indice);
                indice = DReader.GetOrdinal("TCarga");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.sTipoCarga = DReader.GetString(indice);
                indice = DReader.GetOrdinal("TipoContenedor");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.sTipoContenedor = DReader.GetString(indice);
                indice = DReader.GetOrdinal("IdTamanoCntManifestado");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.iIdTamanoContenedor = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("TipEmb_vch_Descripcion");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.sEmbalaje = DReader.GetString(indice);
                indice = DReader.GetOrdinal("IdTarifa");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.iIdTarifa = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("MontoUnitario");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.dMontoUnitario = DReader.GetDecimal(indice);
                indice = DReader.GetOrdinal("Cantidad");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.dCantidad = DReader.GetDecimal(indice);
                indice = DReader.GetOrdinal("Afecto");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.dAfecto = DReader.GetDecimal(indice);
                indice = DReader.GetOrdinal("Inafecto");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.dInafecto = DReader.GetDecimal(indice);
                indice = DReader.GetOrdinal("Descuento");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.dDescuento = DReader.GetDecimal(indice);
                indice = DReader.GetOrdinal("Detraccion");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.dDetraccion = DReader.GetDecimal(indice);
                indice = DReader.GetOrdinal("Igv");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.dIgv = DReader.GetDecimal(indice);
                indice = DReader.GetOrdinal("Total");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.dTotal = DReader.GetDecimal(indice);
                indice = DReader.GetOrdinal("Modalidad");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.sModalidad = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Situacion");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.sSituacion = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Origen");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.sOrigen = DReader.GetString(indice);
                indice = DReader.GetOrdinal("FechaAlmaIni");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.sFechaAlmaIni = DReader.GetString(indice);
                indice = DReader.GetOrdinal("FechaAlmaFin");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.sFechaAlmaFin = DReader.GetString(indice);
                indice = DReader.GetOrdinal("PesoDespachado");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.dPeso = DReader.GetDecimal(indice);
                indice = DReader.GetOrdinal("TipoMoneda");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.sTipoMoneda = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Porcent_Detraccion");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.sPorcentDetraccion = DReader.GetString(indice);
                indice = DReader.GetOrdinal("MontoMinDetraccion");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.dMontoMinimoDetraccion = DReader.GetDecimal(indice);
                indice = DReader.GetOrdinal("Factor_Detraccion");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.dFactorDetraccion = DReader.GetDecimal(indice);
                indice = DReader.GetOrdinal("IdServicio");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.iIdServicio = DReader.GetInt32(indice);

                return oBE_Liquidacion;
            }
            catch (Exception e)
            {
                oResultadoTransaccionTx.RegistrarError(e);
                return null;
            }
        }
        public static BE_Liquidacion Listar_Por_Volante(IDataReader DReader)
        {
            ResultadoTransaccionTx oResultadoTransaccionTx = null;
            try
            {


                BE_Liquidacion oBE_Liquidacion = new BE_Liquidacion();
                int indice = 0;

                indice = DReader.GetOrdinal("RolCliente");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.iIdRolCliente = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("IdCliente");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.iIdCliente = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("Cliente");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.sCliente = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Direccion");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.sDireccion = DReader.GetString(indice);
                indice = DReader.GetOrdinal("IdAgenciaAduana");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.iIdAgAduana = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("AgAduana");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.sAgAduana = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Nave");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.sNave = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Viaje");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.sviaje = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Rumbo");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.sRumbo = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Operacion");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.iIdOperacion = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("NumeroDO");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.sNumeroDo = DReader.GetString(indice);
                indice = DReader.GetOrdinal("IdDocOri");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.iIdDocOri = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("IdNavVia");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.iIdNavVia = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("IdVolante");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.iIdVolante = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("Cli_NumDocumento");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.sDniCliente = DReader.GetString(indice);
                indice = DReader.GetOrdinal("DniAgenciaAduana");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.sDomentoAgeAdu = DReader.GetString(indice);
                indice = DReader.GetOrdinal("MontoTipoCambio");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.dMontoTipoCambio = DReader.GetDecimal(indice);

                indice = DReader.GetOrdinal("IdClienteFac");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.iIdClienteFacturar = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("ClienteFac");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.sClienteFacturar = DReader.GetString(indice);
                indice = DReader.GetOrdinal("NumDocumentoFac");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.sDniClienteFac = DReader.GetString(indice);
                indice = DReader.GetOrdinal("DireccionFac");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.sDireccionFac = DReader.GetString(indice);

                return oBE_Liquidacion;
            }
            catch (Exception e)
            {
                oResultadoTransaccionTx.RegistrarError(e);
                return null;
            }
        }
        public static BE_Liquidacion Listar_Por_PorId(IDataReader DReader)
        {
            ResultadoTransaccionTx oResultadoTransaccionTx = null;
            try
            {


                BE_Liquidacion oBE_Liquidacion = new BE_Liquidacion();
                int indice = 0;

                indice = DReader.GetOrdinal("idLiq");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.iIdLiq = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("IdRolClienteConsignatario");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.iIdRolCliente = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("IdTipoOperacion");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.iIdOperacion = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("IdCliente");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.iIdCliente = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("Cliente");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.sCliente = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Direccion");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.sDireccion = DReader.GetString(indice);
                indice = DReader.GetOrdinal("IdClienteFac");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.iIdClienteFacturar = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("ClienteFact");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.sClienteFacturar = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Moneda");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.iIdMoneda = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("Volante");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.sNroVolante = DReader.GetString(indice);
                indice = DReader.GetOrdinal("NumeroDO");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.sNumeroDo = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Fecha");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.dtFecha_Liquidacion = DReader.GetDateTime(indice);
                indice = DReader.GetOrdinal("Nave");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.sNave = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Viaje");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.sviaje = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Rumbo");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.sRumbo = DReader.GetString(indice);
                indice = DReader.GetOrdinal("IdDocOri");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.iIdDocOri = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("IdContrato");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.iIdContrato = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("ClienteDes");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.sClienteContrato = DReader.GetString(indice);

                indice = DReader.GetOrdinal("MontoBruto");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.dMontoBruto = DReader.GetDecimal(indice);
                indice = DReader.GetOrdinal("Descuento");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.dDescuento = DReader.GetDecimal(indice);
                indice = DReader.GetOrdinal("Igv");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.dIgv = DReader.GetDecimal(indice);
                indice = DReader.GetOrdinal("Neto");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.dTotal = DReader.GetDecimal(indice);
                indice = DReader.GetOrdinal("IdVolante");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.iIdVolante = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("Cli_NumDocumento");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.sDniCliente = DReader.GetString(indice);
                indice = DReader.GetOrdinal("CliFac_NumDocumento");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.sDniClienteFac = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Afecto");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.dAfecto = DReader.GetDecimal(indice);
                indice = DReader.GetOrdinal("Observacion");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.sObservacion = DReader.GetString(indice);
                indice = DReader.GetOrdinal("NroContrato");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.sNroContrato = DReader.GetString(indice);
                indice = DReader.GetOrdinal("IdAgenteAduana");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.iIdAgAduana = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("Dni_AgenteAduana");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.sDomentoAgeAdu = DReader.GetString(indice);
                indice = DReader.GetOrdinal("AgenteAduana");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.sAgAduana = DReader.GetString(indice);

                return oBE_Liquidacion;
            }
            catch (Exception e)
            {
                oResultadoTransaccionTx.RegistrarError(e);
                return null;
            }
        }


        /////////////////// LIQUIDACION - FRANK
        ///


        public BE_Liquidacion Listar_PreLiquidacion_Info_Cabecera_2(Int32 iIdLineaNegocio, Int32 iIdOperacion, String sNumeroDocumento, String sMasterLCL, Int32 iOficina, Int32 IdCliente)
        {
            IDataReader reader = null;

            try
            {
                using (Database dbTerminal = new Database())
                {
                    dbTerminal.ProcedureName = "[EXTRANET_ADM_Lis_InfoPreLiquidacion_2_V2]";
                    dbTerminal.AddParameter("@vi_IdLineaNegocio", DbType.Int32, ParameterDirection.Input, iIdLineaNegocio);
                    dbTerminal.AddParameter("@vi_IdOperacion", DbType.Int32, ParameterDirection.Input, iIdOperacion);
                    dbTerminal.AddParameter("@vi_NumeroDocumento", DbType.String, ParameterDirection.Input, sNumeroDocumento);
                    dbTerminal.AddParameter("@vi_MasterLCL", DbType.String, ParameterDirection.Input, sMasterLCL);
                    dbTerminal.AddParameter("@vi_Oficina", DbType.Int32, ParameterDirection.Input, iOficina);
                    dbTerminal.AddParameter("@vi_IdClienteAduana", DbType.Int32, ParameterDirection.Input, IdCliente);

                    reader = dbTerminal.GetDataReader();

                    BE_Liquidacion oBE_Preliquidacion = new BE_Liquidacion();
                    while (reader.Read())
                    {
                        oBE_Preliquidacion = Listar_PreLiquidacion_Info_Cabecera_2(reader);
                    }
                    reader.Close();

                    return oBE_Preliquidacion;
                }
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public static BE_Liquidacion Listar_PreLiquidacion_Info_Cabecera_2(IDataReader DReader)
        {
            try
            {
                BE_Liquidacion oBE_Liquidacion = new BE_Liquidacion();
                int indice = 0;

                indice = DReader.GetOrdinal("IdDocOri");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.iIdDocOri = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("IdCliente");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.iIdCliente = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("IdAgenteAduanas");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.iIdAgAduana = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("AgCarga");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.iIdAgCarga = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("IdRol");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.iIdRolCliente = DReader.GetInt32(indice);               
                indice = DReader.GetOrdinal("Cliente");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.sCliente = DReader.GetString(indice);
                indice = DReader.GetOrdinal("IdContrato");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.iIdContrato = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("AcuCom_Int_NroAcuerdo");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.sNroAcuerdo = DReader.GetString(indice);
                indice = DReader.GetOrdinal("DocOri_vch_NumeroDO");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.sNumeroDo = DReader.GetString(indice);
                indice = DReader.GetOrdinal("ClienteContrato");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.sClienteContrato = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Situacion");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.sSituacion = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Moneda");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.sTipoMoneda = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Observacion");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.sObservacion = DReader.GetString(indice);

                return oBE_Liquidacion;
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public int ConsultarEstadoAcuerdo_2(Int32? iIdAcuerdo)
        {
            using (Database dbTerminal = new Database())
            {
                try
                {
                    dbTerminal.ProcedureName = "COM_Ins_ConsultaEstadoAcuerdo_2";

                    dbTerminal.AddParameter("@vi_IdAcuerdo", DbType.Int32, ParameterDirection.Input, iIdAcuerdo);
                    dbTerminal.AddParameter("@vo_SituacionAcuerdo", DbType.Int32, ParameterDirection.Output, 0);

                    dbTerminal.ExecuteTransaction();

                    int Retorno = Convert.ToInt32(dbTerminal.GetParameter("@vo_SituacionAcuerdo").ToString());

                    return Retorno;
                }
                catch (Exception e)
                {
                    return 0;
                }
            }
        }

        public Decimal IGV()
        {
            //IDataReader reader = null;
            try
            {
                using (Database dbTerminal = new Database())
                {
                    dbTerminal.ProcedureName = "[Facturacion_Lis_DocumentoPagoDetalleIGV]";
                    dbTerminal.AddParameter("@vo_IGV", DbType.String, ParameterDirection.Output, String.Empty, 10);

                    dbTerminal.Execute();

                    String numero;
                    numero = dbTerminal.GetParameter("@vo_IGV").ToString();
                    return Convert.ToDecimal(numero);
                }
            }
            catch (Exception e)
            {
                ResultadoTransaccionTx oResultadoTransaccionTx = new ResultadoTransaccionTx(ResultadoTransaccionTx.TipoResultado.Error, e);
                oResultadoTransaccionTx.RegistrarError(e);
                return 0;
            }
        }

        public String Operacion(String Documento)
        {
            //IDataReader reader = null;
            try
            {
                using (Database dbTerminal = new Database())
                {
                    dbTerminal.ProcedureName = "[EXTRANET_ValidarVolanteBooking]";
                    dbTerminal.AddParameter("@Documento", DbType.String, ParameterDirection.Input, Documento);
                    dbTerminal.AddParameter("@IdOperacion", DbType.String, ParameterDirection.Output, String.Empty, 10);

                    dbTerminal.Execute();

                    String Operacion;
                    Operacion = dbTerminal.GetParameter("@IdOperacion").ToString();
                    return Operacion;
                }
            }
            catch (Exception e)
            {
                ResultadoTransaccionTx oResultadoTransaccionTx = new ResultadoTransaccionTx(ResultadoTransaccionTx.TipoResultado.Error, e);
                oResultadoTransaccionTx.RegistrarError(e);
                return "";
            }
        }

        public Decimal TipoCambio()
        {
            //IDataReader reader = null;
            try
            {
                using (Database dbTerminal = new Database())
                {
                    dbTerminal.ProcedureName = "[Facturacion_Lis_DocumentoPagoDetalleTcambio]";
                    dbTerminal.AddParameter("@vo_TipoCambio", DbType.String, ParameterDirection.Output, String.Empty, 10);

                    dbTerminal.Execute();

                    String numero;
                    numero = dbTerminal.GetParameter("@vo_TipoCambio").ToString();
                    return Convert.ToDecimal(numero);
                }
            }
            catch (Exception e)
            {
                ResultadoTransaccionTx oResultadoTransaccionTx = new ResultadoTransaccionTx(ResultadoTransaccionTx.TipoResultado.Error, e);
                oResultadoTransaccionTx.RegistrarError(e);
                return 0;
            }
        }


        public String Insertar_PreLiquidacion_2(BE_Liquidacion oBE_Liquidacion, BE_LiquidacionList oBE_LiquidacionLis)
        {
            String Retorno = string.Empty;
            DbTransaction oTx = null;
            int vl_iCodigo = 0;
            int vl_iCodigodOrdSerDet = 0;
            int vl_IdOrdSerDet = 0;
            String vl_sMessage = string.Empty;
            String vl_sResultado = string.Empty;

            using (Database dbTerminal = new Database())
            {
                try
                {
                    dbTerminal.ProcedureName = "[EXTRANET_ADM_Ins_PreLiquidacion_2]";
                    oTx = dbTerminal.Transaction();
                    dbTerminal.AddTransaction(oTx);
                    dbTerminal.AddParameter("@vi_IdSuperNegocio", DbType.Int32, ParameterDirection.Input, oBE_Liquidacion.iIdSuperNegocio_2);
                    dbTerminal.AddParameter("@vi_IdTipoOperacion", DbType.Int32, ParameterDirection.Input, oBE_Liquidacion.iIdOperacion);
                    dbTerminal.AddParameter("@vi_IdCliente", DbType.Int32, ParameterDirection.Input, oBE_Liquidacion.iIdClienteFacturar);
                    dbTerminal.AddParameter("@vi_IdAgenteAduana", DbType.Int32, ParameterDirection.Input, oBE_Liquidacion.iIdAgAduana);
                    dbTerminal.AddParameter("@vi_IdAcuerdo", DbType.Int32, ParameterDirection.Input, oBE_Liquidacion.iIdAcuerdo);
                    dbTerminal.AddParameter("@vi_IdMoneda", DbType.Int32, ParameterDirection.Input, oBE_Liquidacion.iIdMoneda);
                    dbTerminal.AddParameter("@vi_Num_TotalMontoAfecto", DbType.Decimal, ParameterDirection.Input, oBE_Liquidacion.dAfecto);
                    dbTerminal.AddParameter("@vi_Num_TotalMontoInafecto", DbType.Decimal, ParameterDirection.Input, oBE_Liquidacion.dInafecto);
                    dbTerminal.AddParameter("@vi_Num_TotalMontoImpuestoVenta", DbType.Decimal, ParameterDirection.Input, oBE_Liquidacion.dIgv);
                    dbTerminal.AddParameter("@vi_Num_TotalMontoBruto", DbType.Decimal, ParameterDirection.Input, oBE_Liquidacion.dMontoBruto);
                    dbTerminal.AddParameter("@vi_Num_TotalMontoNeto", DbType.Decimal, ParameterDirection.Input, oBE_Liquidacion.dMontoNeto);
                    dbTerminal.AddParameter("@vi_Num_TotalMontoDescuento", DbType.Decimal, ParameterDirection.Input, oBE_Liquidacion.dDescuento);
                    dbTerminal.AddParameter("@vi_Num_TotalMontoDetraccion", DbType.Decimal, ParameterDirection.Input, oBE_Liquidacion.dDetraccion);
                    dbTerminal.AddParameter("@vi_Observacion", DbType.String, ParameterDirection.Input, oBE_Liquidacion.sObservacion);
                    dbTerminal.AddParameter("@vi_IdTipoLiquidacion", DbType.Int32, ParameterDirection.Input, oBE_Liquidacion.iIdTipoLiquidacion_2);
                    dbTerminal.AddParameter("@Usuario", DbType.String, ParameterDirection.Input, oBE_Liquidacion.sUsuario);
                    dbTerminal.AddParameter("@NombrePc", DbType.String, ParameterDirection.Input, oBE_Liquidacion.sNombrePc);
                    dbTerminal.AddParameter("@vo_resultado", DbType.String, ParameterDirection.Output, String.Empty, 200);
                    dbTerminal.AddParameter("@vi_Liq_Vch_FechaCalculo", DbType.String, ParameterDirection.Input, oBE_Liquidacion.sFechaCalculo);
                    dbTerminal.AddParameter("@vi_NroBl", DbType.String, ParameterDirection.Input, oBE_Liquidacion.sNumeroDo);
                    dbTerminal.AddParameter("@vi_Almacenaje", DbType.String, ParameterDirection.Input, oBE_Liquidacion.sAlmacenaje);
                    dbTerminal.AddParameter("@vi_IdPeriodo", DbType.String, ParameterDirection.Input, oBE_Liquidacion.sPeriodo);
                    dbTerminal.AddParameter("@vi_IdDocOri", DbType.String, ParameterDirection.Input, oBE_Liquidacion.sIdDocOri);
                    dbTerminal.AddParameter("@vi_TipoFactura", DbType.String, ParameterDirection.Input, oBE_Liquidacion.sTipoFactDeposito);
                    dbTerminal.AddParameter("@vi_Vez", DbType.Int32, ParameterDirection.Input, oBE_Liquidacion.iVez);
                    dbTerminal.AddParameter("@vi_Oficina", DbType.Int32, ParameterDirection.Input, oBE_Liquidacion.iOficina);
                    dbTerminal.AddParameter("@v_IdSolicitudPreLiq", DbType.Int32, ParameterDirection.Input, oBE_Liquidacion.iIdSolitudPreLiq);
                  
                    dbTerminal.ExecuteTransaction();

                    String[] xx = dbTerminal.GetParameter("@vo_resultado").ToString().Split('|'); ;
                    if (Convert.ToInt32(xx[0]) > 0)
                    {
                        for (int i = 0; i < xx.Length; i++)
                        {
                            if (i == 0)
                                vl_iCodigo = Convert.ToInt32(xx[i]);

                            else if (i == 1)
                                vl_sMessage += xx[i];

                            else
                                vl_iCodigodOrdSerDet = Convert.ToInt32(xx[i]);
                        }

                        vl_sResultado = xx[0] + "| " + xx[1];
                        oBE_Liquidacion.iIdLiq = vl_iCodigo;

                        dbTerminal.ParameterClear();

                        for (int i = 0; i < oBE_LiquidacionLis.Count; i++)
                        {
                            if (vl_iCodigodOrdSerDet > 0)
                                vl_IdOrdSerDet = vl_iCodigodOrdSerDet;
                            else
                                vl_IdOrdSerDet = oBE_LiquidacionLis[i].iIdOrdSerDet;

                            dbTerminal.ProcedureName = null;
                            dbTerminal.ProcedureName = "[EXTRANET_ADM_Ins_PreLiquidacion_Detalle_2]";
                            dbTerminal.AddTransaction(oTx);
                            dbTerminal.AddParameter("@vi_IdLiquidacion", DbType.Int32, ParameterDirection.Input, vl_iCodigo);
                            dbTerminal.AddParameter("@vi_IdOrdSerDet", DbType.Int32, ParameterDirection.Input, vl_IdOrdSerDet);
                            dbTerminal.AddParameter("@vi_IdTarifa", DbType.Int32, ParameterDirection.Input, oBE_LiquidacionLis[i].iIdTarifa);
                            dbTerminal.AddParameter("@vi_DescAdicionalServicio", DbType.String, ParameterDirection.Input, oBE_LiquidacionLis[i].sDescripcionServicio);
                            dbTerminal.AddParameter("@vi_Int_CantidadLiquidada", DbType.Decimal, ParameterDirection.Input, Convert.ToDecimal(oBE_LiquidacionLis[i].dCantidad));
                            dbTerminal.AddParameter("@vi_Num_PesoLiquidado", DbType.Decimal, ParameterDirection.Input, oBE_LiquidacionLis[i].dPeso);
                            dbTerminal.AddParameter("@vi_Num_VolumenLiquidado", DbType.Decimal, ParameterDirection.Input, oBE_LiquidacionLis[i].dVolumen);
                            dbTerminal.AddParameter("@vi_Num_Impuesto", DbType.Decimal, ParameterDirection.Input, oBE_LiquidacionLis[i].dIgv);
                            dbTerminal.AddParameter("@vi_Num_MontoAfecto", DbType.Decimal, ParameterDirection.Input, oBE_LiquidacionLis[i].dAfecto);
                            dbTerminal.AddParameter("@vi_Num_MontoInafecto", DbType.Decimal, ParameterDirection.Input, oBE_LiquidacionLis[i].dInafecto);
                            dbTerminal.AddParameter("@vi_Num_MontoBruto", DbType.Decimal, ParameterDirection.Input, oBE_LiquidacionLis[i].dMontoBruto);
                            dbTerminal.AddParameter("@vi_Num_MontoNeto", DbType.Decimal, ParameterDirection.Input, oBE_LiquidacionLis[i].dMontoNeto);
                            dbTerminal.AddParameter("@vi_Num_Descuento", DbType.Decimal, ParameterDirection.Input, oBE_LiquidacionLis[i].dDescuento);
                            dbTerminal.AddParameter("@vi_Num_Detraccion", DbType.Decimal, ParameterDirection.Input, oBE_LiquidacionLis[i].dDetraccion);
                            dbTerminal.AddParameter("@vi_IdTipoLiquidacion", DbType.Int32, ParameterDirection.Input, oBE_Liquidacion.iIdTipoLiquidacion_2);
                            dbTerminal.AddParameter("@Usuario", DbType.String, ParameterDirection.Input, oBE_LiquidacionLis[i].sUsuario);
                            dbTerminal.AddParameter("@NombrePc", DbType.String, ParameterDirection.Input, oBE_LiquidacionLis[i].sNombrePc);
                            dbTerminal.AddParameter("@vi_Num_MontoUnitario", DbType.Decimal, ParameterDirection.Input, oBE_LiquidacionLis[i].dMontoUnitario);
                            dbTerminal.AddParameter("@vi_PqteSLI", DbType.String, ParameterDirection.Input, oBE_LiquidacionLis[i].sOrigenPqteSLI);
                            dbTerminal.AddParameter("@vi_MarcaSLI", DbType.String, ParameterDirection.Input, oBE_LiquidacionLis[i].sMarcaSLI);
                            dbTerminal.AddParameter("@LiqDet_int_IdMoneda", DbType.Int32, ParameterDirection.Input, oBE_LiquidacionLis[i].idMonedaVisual);
                            dbTerminal.AddParameter("@LiqDet_dec_MontoUnitarioVisual", DbType.Decimal, ParameterDirection.Input, oBE_LiquidacionLis[i].dUnitarioVisual);
                            dbTerminal.AddParameter("@LiqDet_dec_MontototalVisual", DbType.Decimal, ParameterDirection.Input, oBE_LiquidacionLis[i].dMontoTotalVisual);
                            dbTerminal.AddParameter("@vi_Num_MontoUnitario2", DbType.Decimal, ParameterDirection.Input, oBE_LiquidacionLis[i].dUnitarioGrabar2);
                            dbTerminal.AddParameter("@vi_Num_MontoAfecto2", DbType.Decimal, ParameterDirection.Input, oBE_LiquidacionLis[i].dAfectoGrabar2);
                            dbTerminal.AddParameter("@vi_Num_Impuesto2", DbType.Decimal, ParameterDirection.Input, oBE_LiquidacionLis[i].dIgvGrabar2);
                            dbTerminal.AddParameter("@vi_Num_MontoInafecto2", DbType.Decimal, ParameterDirection.Input, oBE_LiquidacionLis[i].dInafectoGrabar2);
                            dbTerminal.AddParameter("@vi_Num_Detraccion2", DbType.Decimal, ParameterDirection.Input, oBE_LiquidacionLis[i].ddetraccionGrabar2);
                            dbTerminal.AddParameter("@vi_Num_Descuento2", DbType.Decimal, ParameterDirection.Input, oBE_LiquidacionLis[i].dDescuentoGrabar2);
                            dbTerminal.AddParameter("@vi_Num_MontoNeto2", DbType.Decimal, ParameterDirection.Input, oBE_LiquidacionLis[i].dMontoNetoGrabar2);
                            dbTerminal.AddParameter("@vi_Num_MontoBruto2", DbType.Decimal, ParameterDirection.Input, oBE_LiquidacionLis[i].dMontoBrutoGrabar2);
                            dbTerminal.AddParameter("@LiqDet_int_IdMoneda2", DbType.Int32, ParameterDirection.Input, oBE_LiquidacionLis[i].idMonedaGrabar2);
                            dbTerminal.AddParameter("@vo_resultado", DbType.String, ParameterDirection.Output, string.Empty, 200);

                            dbTerminal.ExecuteTransaction();

                            //vl_sResultado = dbTerminal.GetParameter("@vo_Resultado").ToString();

                            int vl_iCodRes = 0;
                            String[] vl_Res = dbTerminal.GetParameter("@vo_resultado").ToString().Split('|'); ;

                            for (int j = 0; j < vl_Res.Length; j++)
                            {
                                if (j == 0)
                                {
                                    vl_iCodRes = Convert.ToInt32(vl_Res[j]);
                                }
                                else
                                {
                                    vl_sMessage = vl_Res[j];
                                }
                            }

                            if (vl_iCodRes == -1)
                            {
                                oTx.Rollback();
                                return vl_sMessage;
                            }
                            else
                                dbTerminal.ParameterClear();
                        }
                        oTx.Commit();
                        return vl_sResultado;

                    }
                    else
                    {
                        oTx.Commit();
                    }
                    return vl_sResultado = xx[0] + "|" + xx[1];
                }
                catch (Exception e)
                {
                    oTx.Rollback();
                    return "-1|Ocurrio un error en la capa de Datos";
                }
            }
        }

        public BE_LiquidacionList Listar_Liquidacion_2(BE_Liquidacion oBE_Liquidacion)
        {
            IDataReader reader = null;

            try
            {
                using (Database dbTerminal = new Database())
                {
                    dbTerminal.ProcedureName = "[EXTRANET_ADM_Lis_Liquidacion]";
                    dbTerminal.AddParameter("@vi_TipoLiquidacion", DbType.Int32, ParameterDirection.Input, oBE_Liquidacion.iIdTipoLiquidacion_2);
                    dbTerminal.AddParameter("@vi_IdSuperNegocio", DbType.Int32, ParameterDirection.Input, oBE_Liquidacion.iIdLineaNegocio);
                    dbTerminal.AddParameter("@vi_IdLiquidacion", DbType.String, ParameterDirection.Input, oBE_Liquidacion.sLiquidacion);                    
                    dbTerminal.AddParameter("@vi_FechaIni", DbType.String, ParameterDirection.Input, oBE_Liquidacion.sFechaAlmaIni);
                    dbTerminal.AddParameter("@vi_FechaFin", DbType.String, ParameterDirection.Input, oBE_Liquidacion.sFechaAlmaFin);                 
                    dbTerminal.AddParameter("@vl_Documento", DbType.String, ParameterDirection.Input, oBE_Liquidacion.sNumeroDo);
                    dbTerminal.AddParameter("@vi_Estado", DbType.String, ParameterDirection.Input, oBE_Liquidacion.sEstado);                    
                    dbTerminal.AddParameter("@vi_Oficina", DbType.Int32, ParameterDirection.Input, oBE_Liquidacion.iOficina);
                    dbTerminal.AddParameter("@vi_IdClienteAduana", DbType.Int32, ParameterDirection.Input, oBE_Liquidacion.iIdCliente);
                    dbTerminal.AddParameter("@vl_Volante", DbType.String, ParameterDirection.Input, oBE_Liquidacion.sNroVolante);
                    //dbTerminal.AddParameter("@vi_numeropagina", DbType.Int32, ParameterDirection.Input, oBE_Liquidacion.NPagina);
                    //dbTerminal.AddParameter("@vi_numeroregistros", DbType.Int32, ParameterDirection.Input, oBE_Liquidacion.NRegistros);
                    //dbTerminal.AddParameter("@vo_totalregistros", DbType.Int32, ParameterDirection.Output, oBE_Liquidacion.NtotalRegistros);

                    reader = dbTerminal.GetDataReader();

                    BE_LiquidacionList oBE_Preliquidacion = new BE_LiquidacionList();
                    while (reader.Read())
                    {
                        oBE_Preliquidacion.Add(Listar_Liquidacion_2(reader));
                    }
                    reader.Close();
                    oBE_Liquidacion.NtotalRegistros = Convert.ToInt32(dbTerminal.GetParameter("@vo_totalregistros"));
                    return oBE_Preliquidacion;

                }
            }
            catch (Exception e)
            {
                return null;
            }
        }

        //public static BE_Liquidacion Listar_Liquidacion_2(IDataReader DReader)
        //{
        //    try
        //    {
        //        BE_Liquidacion oBE_Liquidacion = new BE_Liquidacion();
        //        int indice = 0;

        //        indice = DReader.GetOrdinal("IdLiquidacion");
        //        if (!DReader.IsDBNull(indice)) oBE_Liquidacion.iIdLiq = DReader.GetInt32(indice);
        //        indice = DReader.GetOrdinal("Fecha");
        //        if (!DReader.IsDBNull(indice)) oBE_Liquidacion.dtFecha_Liquidacion = DReader.GetDateTime(indice);
        //        indice = DReader.GetOrdinal("Cliente");
        //        if (!DReader.IsDBNull(indice)) oBE_Liquidacion.sCliente = DReader.GetString(indice);
        //        indice = DReader.GetOrdinal("ClienteFacturado");
        //        if (!DReader.IsDBNull(indice)) oBE_Liquidacion.sClienteFacturar = DReader.GetString(indice);
        //        indice = DReader.GetOrdinal("Nave");
        //        if (!DReader.IsDBNull(indice)) oBE_Liquidacion.sNave = DReader.GetString(indice);
        //        indice = DReader.GetOrdinal("Viaje");
        //        if (!DReader.IsDBNull(indice)) oBE_Liquidacion.sviaje = DReader.GetString(indice);
        //        indice = DReader.GetOrdinal("Rumbo");
        //        if (!DReader.IsDBNull(indice)) oBE_Liquidacion.sRumbo = DReader.GetString(indice);
        //        indice = DReader.GetOrdinal("Operacion");
        //        if (!DReader.IsDBNull(indice)) oBE_Liquidacion.sOperacion = DReader.GetString(indice);
        //        indice = DReader.GetOrdinal("NumeroDO");
        //        if (!DReader.IsDBNull(indice)) oBE_Liquidacion.sNumeroDo = DReader.GetString(indice);
        //        indice = DReader.GetOrdinal("MontoAfecto");
        //        if (!DReader.IsDBNull(indice)) oBE_Liquidacion.dAfecto = DReader.GetDecimal(indice);
        //        indice = DReader.GetOrdinal("Inafecto");
        //        if (!DReader.IsDBNull(indice)) oBE_Liquidacion.dInafecto = DReader.GetDecimal(indice);
        //        indice = DReader.GetOrdinal("ImpuestoVenta");
        //        if (!DReader.IsDBNull(indice)) oBE_Liquidacion.dIgv = DReader.GetDecimal(indice);
        //        indice = DReader.GetOrdinal("MontoBruto");
        //        if (!DReader.IsDBNull(indice)) oBE_Liquidacion.dMontoBruto = DReader.GetDecimal(indice);
        //        indice = DReader.GetOrdinal("MontoNeto");
        //        if (!DReader.IsDBNull(indice)) oBE_Liquidacion.dMontoNeto = DReader.GetDecimal(indice);
        //        indice = DReader.GetOrdinal("Descuento");
        //        if (!DReader.IsDBNull(indice)) oBE_Liquidacion.dDescuento = DReader.GetDecimal(indice);
        //        indice = DReader.GetOrdinal("TipoMoneda");
        //        if (!DReader.IsDBNull(indice)) oBE_Liquidacion.sTipoMoneda = DReader.GetString(indice);
        //        indice = DReader.GetOrdinal("Estado");
        //        if (!DReader.IsDBNull(indice)) oBE_Liquidacion.sEstado = DReader.GetString(indice);
        //        indice = DReader.GetOrdinal("FlagFacturado");
        //        if (!DReader.IsDBNull(indice)) oBE_Liquidacion.sModalidad = DReader.GetString(indice);
        //        indice = DReader.GetOrdinal("idDocPago");
        //        if (!DReader.IsDBNull(indice)) oBE_Liquidacion.iIdPago = DReader.GetInt32(indice);
        //        indice = DReader.GetOrdinal("Negocio");
        //        if (!DReader.IsDBNull(indice)) oBE_Liquidacion.sIdSuperNegocio_2 = DReader.GetString(indice);
        //        indice = DReader.GetOrdinal("EstadoSunat");
        //        if (!DReader.IsDBNull(indice)) oBE_Liquidacion.sSituacion = DReader.GetString(indice);


        //        return oBE_Liquidacion;
        //    }
        //    catch (Exception e)
        //    {
        //        return null;
        //    }
        //}

        public String PreLiquidacion_EnviarAlertas( BE_Liquidacion oBE_Alertas)
        {
            //IDataReader reader = null;
            try
            {
                using (Database dbTerminal = new Database())
                {
                    dbTerminal.ProcedureName = "[EXTRANET_EnviaAlertasPreLiquidacion]";
                    dbTerminal.AddParameter("@vl_NroVolante", DbType.String,ParameterDirection.Input, oBE_Alertas.sNroVolante);
                    dbTerminal.AddParameter("@vi_Operacion", DbType.String, ParameterDirection.Input, oBE_Alertas.sOperacion);
                    dbTerminal.AddParameter("@vi_TipoMensaje", DbType.String, ParameterDirection.Input,oBE_Alertas.sTipoMensaje);
                    dbTerminal.AddParameter("@vl_Retorno", DbType.String, ParameterDirection.Output, String.Empty, 700);

                    dbTerminal.Execute();

                    String Retorno;
                    Retorno = dbTerminal.GetParameter("@vl_Retorno").ToString();
                    return Retorno;
                        //Convert.ToDecimal(numero);
                }
            }
            catch (Exception e)
            {
                ResultadoTransaccionTx oResultadoTransaccionTx = new ResultadoTransaccionTx(ResultadoTransaccionTx.TipoResultado.Error, e);
                oResultadoTransaccionTx.RegistrarError(e);
                return "0";
            }
        }

        public String PreLiquidacion_Ins_SolicitudPreLiquidacion(BE_Liquidacion oBE_Liquidacion)
        {
            //IDataReader reader = null;
            try
            {
                using (Database dbTerminal = new Database())
                {
                    dbTerminal.ProcedureName = "[EXTRANET_Ins_SolicitudPreLiquidacion]";
                    dbTerminal.AddParameter("@IdDocOri", DbType.Int32, ParameterDirection.Input, oBE_Liquidacion.iIdDocOri);
                    dbTerminal.AddParameter("@PreLiqOnL_vch_Origen", DbType.String, ParameterDirection.Input, oBE_Liquidacion.sOrigenSolicitud);
                    dbTerminal.AddParameter("@IdSuperNegocio", DbType.Int32, ParameterDirection.Input, oBE_Liquidacion.iIdSuperNegocio_2);
                    dbTerminal.AddParameter("@UsuarioGenerar", DbType.String, ParameterDirection.Input, oBE_Liquidacion.sUsuario);
                    dbTerminal.AddParameter("@IdPreLiquidacion", DbType.Int32, ParameterDirection.Input, oBE_Liquidacion.iIdLiq);
                    dbTerminal.AddParameter("@IdClienteExtranet", DbType.Int32, ParameterDirection.Input, oBE_Liquidacion.iIdCliente);
                    dbTerminal.AddParameter("@IdContrato", DbType.Int32, ParameterDirection.Input, oBE_Liquidacion.iIdAcuerdo);
                    dbTerminal.AddParameter("@FechaRetiro", DbType.String, ParameterDirection.Input, oBE_Liquidacion.sFechaCalculo);
                    dbTerminal.AddParameter("@PreLiqOnL_ch_Situacion", DbType.String, ParameterDirection.Input, oBE_Liquidacion.sSituacion);
                    dbTerminal.AddParameter("@v_Retorno", DbType.String, ParameterDirection.Output, String.Empty, 700);
                    

                    dbTerminal.Execute();

                    String Retorno;
                    Retorno = dbTerminal.GetParameter("@v_Retorno").ToString();
                    return Retorno;
                    //Convert.ToDecimal(numero);
                }
            }
            catch (Exception e)
            {
                ResultadoTransaccionTx oResultadoTransaccionTx = new ResultadoTransaccionTx(ResultadoTransaccionTx.TipoResultado.Error, e);
                oResultadoTransaccionTx.RegistrarError(e);
                return "0";
            }
        }

        public String PreLiquidacion_Ins_Validaciones(BE_Liquidacion oBE_Liquidacion)
        {
            //IDataReader reader = null;
            try
            {
                using (Database dbTerminal = new Database())
                {
                    dbTerminal.ProcedureName = "[EXTRANET_Ins_ValidacionesPreLiquidacion]";
                    dbTerminal.AddParameter("@IdSolicitudPreLiqOnline", DbType.Int32, ParameterDirection.Input, oBE_Liquidacion.iIdSolitudPreLiq);
                    dbTerminal.AddParameter("@Validacion", DbType.String, ParameterDirection.Input, oBE_Liquidacion.iIdValidacion);
                    dbTerminal.AddParameter("@Tipo", DbType.Int32, ParameterDirection.Input, oBE_Liquidacion.iTipoProceso);                    
                    dbTerminal.AddParameter("@Retorno", DbType.String, ParameterDirection.Output, String.Empty, 700);

                    dbTerminal.Execute();

                    String Retorno;
                    Retorno = dbTerminal.GetParameter("@Retorno").ToString();
                    return Retorno;
                    //Convert.ToDecimal(numero);
                }
            }
            catch (Exception e)
            {
                ResultadoTransaccionTx oResultadoTransaccionTx = new ResultadoTransaccionTx(ResultadoTransaccionTx.TipoResultado.Error, e);
                oResultadoTransaccionTx.RegistrarError(e);
                return "0";
            }
        }

        public String PreLiquidacion_Lis_ValidacionesServicioPendiente(BE_Liquidacion oBE_Liquidacion)
        {
            //IDataReader reader = null;
            try
            {
                using (Database dbTerminal = new Database())
                {
                    dbTerminal.ProcedureName = "[EXTRANET_Lis_ValidacionesPreLiquidacionServicioPendiente]";
                    dbTerminal.AddParameter("@IdOrdSerDet", DbType.Int32, ParameterDirection.Input, oBE_Liquidacion.iIdOrdSerDet);                   
                    dbTerminal.AddParameter("@Retorno", DbType.String, ParameterDirection.Output, String.Empty, 700);

                    dbTerminal.Execute();

                    String Retorno;
                    Retorno = dbTerminal.GetParameter("@Retorno").ToString();
                    return Retorno;
                    //Convert.ToDecimal(numero);
                }
            }
            catch (Exception e)
            {
                ResultadoTransaccionTx oResultadoTransaccionTx = new ResultadoTransaccionTx(ResultadoTransaccionTx.TipoResultado.Error, e);
                oResultadoTransaccionTx.RegistrarError(e);
                return "0";
            }
        }

        public String PreLiquidacion_Ins_ServiciosPreValidacion(BE_Liquidacion oBE_Liquidacion)
        {
            //IDataReader reader = null;
            try
            {
                using (Database dbTerminal = new Database())
                {
                    dbTerminal.ProcedureName = "[EXTRANET_Ins_ServiciosPreLiquidacion]";
                    dbTerminal.AddParameter("@IdSolicitudPreLiqOnline", DbType.Int32, ParameterDirection.Input, oBE_Liquidacion.iIdSolitudPreLiq);
                    dbTerminal.AddParameter("@IdDocOriDet", DbType.Int32, ParameterDirection.Input, oBE_Liquidacion.iIdDocOriDet);
                    dbTerminal.AddParameter("@IdOrdSer", DbType.Int32, ParameterDirection.Input, oBE_Liquidacion.iIdOrdSer);
                    dbTerminal.AddParameter("@IdOrdSerDet", DbType.Int32, ParameterDirection.Input, oBE_Liquidacion.iIdOrdSerDet);
                    dbTerminal.AddParameter("@PreLiqOnlSer_dc_Monto", DbType.Decimal, ParameterDirection.Input, oBE_Liquidacion.dAfecto);
                    dbTerminal.AddParameter("@IdMoneda", DbType.Int32, ParameterDirection.Input, oBE_Liquidacion.iIdMoneda);
                    dbTerminal.AddParameter("@PreLiqOnlSer_vch_Servicio", DbType.String, ParameterDirection.Input, oBE_Liquidacion.sDescripcionServicio);
                    dbTerminal.AddParameter("@Retorno", DbType.String, ParameterDirection.Output, String.Empty, 700);

                    dbTerminal.Execute();

                    String Retorno;
                    Retorno = dbTerminal.GetParameter("@Retorno").ToString();
                    return Retorno;
                    //Convert.ToDecimal(numero);
                }
            }
            catch (Exception e)
            {
                ResultadoTransaccionTx oResultadoTransaccionTx = new ResultadoTransaccionTx(ResultadoTransaccionTx.TipoResultado.Error, e);
                oResultadoTransaccionTx.RegistrarError(e);
                return "0";
            }
        }

        public BE_LiquidacionList Listar_Estados_ParaAlertas(BE_Liquidacion oBE_Liquidacion)
        {
            IDataReader reader = null;

            try
            {
                using (Database dbTerminal = new Database())
                {
                    dbTerminal.ProcedureName = "[EXTRANET.Listar_ValidacionesParaCorreo]";
                    dbTerminal.AddParameter("@v_IdSolicitudPreLiq", DbType.Int32, ParameterDirection.Input, oBE_Liquidacion.iIdSolitudPreLiq);                  

                    reader = dbTerminal.GetDataReader();

                    BE_LiquidacionList oBE_Preliquidacion = new BE_LiquidacionList();
                    while (reader.Read())
                    {
                        oBE_Preliquidacion.Add(Listar_Estados_ParaAlertas(reader));
                    }
                    reader.Close();

                    return oBE_Preliquidacion;
                }
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public static BE_Liquidacion Listar_Estados_ParaAlertas(IDataReader DReader)
        {
            try
            {
                BE_Liquidacion oBE_Liquidacion = new BE_Liquidacion();
                int indice = 0;

                indice = DReader.GetOrdinal("ValidacionServicio");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.sValiacionServicio = DReader.GetString(indice);
                indice = DReader.GetOrdinal("ValidacionContrato");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.sValidacionContrato = DReader.GetString(indice);
                indice = DReader.GetOrdinal("ValidacionTarifa");                
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.sValidacionTarifa = DReader.GetString(indice);
                indice = DReader.GetOrdinal("ValidacionAceptaUsuario");
                if (!DReader.IsDBNull(indice)) oBE_Liquidacion.sValidacionAceptaUsuario = DReader.GetString(indice);

                return oBE_Liquidacion;
            }
            catch (Exception e)
            {
                return null;
            }
        }




        #endregion

    }
}
