﻿using FENIX.BusinessEntity;
using FENIX.Common;
using FENIX.DataAccess.Publicacion;
using System;
using System.Collections.Generic;
using System.Data;

namespace FENIX.DataAccess
{
 public   class DA_AutorizacionRetiro
    {
        public Tuple<List<BE_AutorizacionRetiro>, string> Ext_SO_AutorizacionRetiro(BE_AutorizacionRetiro oBE_AutRetiro, int accion)
        {
            string returnMsj = "";
            IDataReader reader = null;
            try
            {

                using (Database dbTerminal = new Database())
                {

                    dbTerminal.ProcedureName = "[Ext_SO_SolicitudAutorizacionRetiro]";
                    dbTerminal.AddParameter("@vi_IdSolicitudAre", DbType.Int32, ParameterDirection.Input, oBE_AutRetiro.IdSolicitudAre);
                    dbTerminal.AddParameter("@vi_idDocOri", DbType.Int32, ParameterDirection.Input, oBE_AutRetiro.idDocOri);
                    dbTerminal.AddParameter("@vi_IdAduana", DbType.Int32, ParameterDirection.Input, oBE_AutRetiro.iIdAgencia);
                    dbTerminal.AddParameter("@vi_nvDuaAnio", DbType.String, ParameterDirection.Input, oBE_AutRetiro.sDuaAnio);
                    dbTerminal.AddParameter("@vi_IdRegimen", DbType.Int32, ParameterDirection.Input, oBE_AutRetiro.IdRegimen);
                    dbTerminal.AddParameter("@vi_nvDuaNumero", DbType.String, ParameterDirection.Input, oBE_AutRetiro.sDuaNumero);
                    dbTerminal.AddParameter("@vi_IdCliente", DbType.Int32, ParameterDirection.Input, oBE_AutRetiro.iIdCliente);
                    dbTerminal.AddParameter("@vi_IdAgenciaAduana", DbType.Int32, ParameterDirection.Input, oBE_AutRetiro.iIdAgencia);
                    dbTerminal.AddParameter("@vi_IdDespachador", DbType.String, ParameterDirection.Input, oBE_AutRetiro.IIdDespachador);
                    dbTerminal.AddParameter("@vi_IdLiquidacion", DbType.Int32, ParameterDirection.Input, oBE_AutRetiro.idLiquidacion);                    
                    dbTerminal.AddParameter("@vi_dtFechaLevante", DbType.DateTime, ParameterDirection.Input, oBE_AutRetiro.dtFechaLevante);
                    dbTerminal.AddParameter("@vi_nvUsuario", DbType.String, ParameterDirection.Input, oBE_AutRetiro.SUsuario);
                    /*Accion 01*/
                    dbTerminal.AddParameter("@vi_nvNumeroDO", DbType.String, ParameterDirection.Input, oBE_AutRetiro.sNumeroDO);
                    dbTerminal.AddParameter("@vi_nvCliente", DbType.String, ParameterDirection.Input, oBE_AutRetiro.SCliente);
                    dbTerminal.AddParameter("@vi_dtFechaDesde", DbType.DateTime, ParameterDirection.Input, oBE_AutRetiro.dtFechaInicio);
                    dbTerminal.AddParameter("@vi_dtFechaHasta", DbType.DateTime, ParameterDirection.Input, oBE_AutRetiro.dtFechaFin);
                    /**/

                    dbTerminal.AddParameter("@vi_nvBanco", DbType.String, ParameterDirection.Input, oBE_AutRetiro.sBanco);
                    dbTerminal.AddParameter("@vi_IdMoneda", DbType.Int32, ParameterDirection.Input, oBE_AutRetiro.IdMoneda);
                    dbTerminal.AddParameter("@vi_idTipoCobro", DbType.Int32, ParameterDirection.Input, oBE_AutRetiro.idTipoCobro);
                    dbTerminal.AddParameter("@vi_idFormaPago", DbType.Int32, ParameterDirection.Input, oBE_AutRetiro.idFormaPago);
                    dbTerminal.AddParameter("@vi_nvNumeroOperacion", DbType.String, ParameterDirection.Input, oBE_AutRetiro.sNumeroOperacion);
                    dbTerminal.AddParameter("@vi_nvObservacion", DbType.String, ParameterDirection.Input, oBE_AutRetiro.sObservacion);
                    dbTerminal.AddParameter("@vi_nvRuc", DbType.String, ParameterDirection.Input, oBE_AutRetiro.sRuc);
                    dbTerminal.AddParameter("@vi_dMontoPago", DbType.Decimal, ParameterDirection.Input, oBE_AutRetiro.dMontoPago);

                    dbTerminal.AddParameter("@vi_iSituacion", DbType.Int32, ParameterDirection.Input, oBE_AutRetiro.idSituacion);
                    dbTerminal.AddParameter("@vi_nvAutRetNumero", DbType.String, ParameterDirection.Input, oBE_AutRetiro.sAutRetNumero);
                    dbTerminal.AddParameter("@vi_sRuta", DbType.String, ParameterDirection.Input, oBE_AutRetiro.sRuta);

                    dbTerminal.AddParameter("@vi_accion", DbType.Int32, ParameterDirection.Input, accion);
                    dbTerminal.AddParameter("@vo_Resultado", DbType.String, ParameterDirection.Output, String.Empty, 50);


                    reader = dbTerminal.GetDataReader();

                    List<BE_AutorizacionRetiro> oBE_List = new List<BE_AutorizacionRetiro>();

                    if (accion == 12)
                    {
                        while (reader.Read())
                        {
                            oBE_List.Add(Pu_AutorizacionRetiro.Ext_SO_AutorizacionRetiroCola(reader));
                        }
                        reader.Close();
                    }
                    else if (accion == 11)
                    {
                        while (reader.Read())
                        {
                            oBE_List.Add(Pu_AutorizacionRetiro.Ext_SO_AutorizacionRetiroPago(reader));
                        }
                        reader.Close();
                    }
                    else if (accion == 14)
                    {
                        while (reader.Read())
                        {
                            oBE_List.Add(Pu_AutorizacionRetiro.Ext_SO_AutorizacionRetiroLigado(reader));
                        }
                        reader.Close();
                    }else
                    {
                        while (reader.Read())
                        {
                            oBE_List.Add(Pu_AutorizacionRetiro.Ext_SO_AutorizacionRetiro(reader));
                        }
                        reader.Close();
                    }


                    returnMsj = dbTerminal.GetParameter("@vo_Resultado").ToString();

                    return Tuple.Create(oBE_List, returnMsj);

                }

            }
            catch (Exception e)
            {
                return null;

            }
        }

        public Tuple<List<BE_AutorizacionRetiro>, string> Ext_SO_AutorizacionRetiroDet(BE_AutorizacionRetiro oBE_AutRetiro, int accion)
        {
            string returnMsj = "";
            IDataReader reader = null;
            try
            {

                using (Database dbTerminal = new Database())
                {

                    dbTerminal.ProcedureName = "[Ext_SO_SolicitudAutorizacionRetiroDet]";

                    dbTerminal.AddParameter("@vi_idSolicitudAreDet", DbType.Int32, ParameterDirection.Input, oBE_AutRetiro.idSolicitudAreDet);
                    dbTerminal.AddParameter("@vi_IdSolicitudAre", DbType.Int32, ParameterDirection.Input, oBE_AutRetiro.IdSolicitudAre);
                    dbTerminal.AddParameter("@vi_iSituacion", DbType.Int32, ParameterDirection.Input, oBE_AutRetiro.idSituacion);
                    dbTerminal.AddParameter("@vi_iTipoDocAdjuntado", DbType.Int32, ParameterDirection.Input, oBE_AutRetiro.iTipoDocAdjuntado);
                    dbTerminal.AddParameter("@vi_sRuta", DbType.String, ParameterDirection.Input, oBE_AutRetiro.sRuta);
                    dbTerminal.AddParameter("@vi_nvUsuario", DbType.String, ParameterDirection.Input, oBE_AutRetiro.SUsuario);
                    dbTerminal.AddParameter("@vi_accion", DbType.Int32, ParameterDirection.Input, accion);
                    dbTerminal.AddParameter("@vi_nvAutRetNumero", DbType.String, ParameterDirection.Input, oBE_AutRetiro.sAutRetNumero);

                    dbTerminal.AddParameter("@vo_Resultado", DbType.String, ParameterDirection.Output, String.Empty, 1000);


                    reader = dbTerminal.GetDataReader();

                    List<BE_AutorizacionRetiro> oBE_List = new List<BE_AutorizacionRetiro>();


                    while (reader.Read())
                    {
                        oBE_List.Add(Pu_AutorizacionRetiro.Ext_SO_AutorizacionRetiroDet(reader));
                    }
                    reader.Close();



                    returnMsj = dbTerminal.GetParameter("@vo_Resultado").ToString();

                    return Tuple.Create(oBE_List, returnMsj);

                }

            }
            catch (Exception e)
            {
                return null;

            }
        }


        public BE_AutorizacionRetiro ListarProcesoPorID(String NroAutRet,int IdAgente)
        {
            IDataReader reader = null;
            try
            {
                using (Database dbTerminal = new Database())
                {
                    dbTerminal.ProcedureName = "[Documentacion_Lis_ProcesoEsp_AutRetiro]";
                    dbTerminal.AddParameter("@vi_NroAutRet", DbType.String, ParameterDirection.Input, NroAutRet);
                    dbTerminal.AddParameter("@vi_IdAgente", DbType.String, ParameterDirection.Input, IdAgente);
                    reader = dbTerminal.GetDataReader();

                    BE_AutorizacionRetiro Lis_AutorizacionRetiro = new BE_AutorizacionRetiro();
                    while (reader.Read())
                    {
                        Lis_AutorizacionRetiro = Pu_AutorizacionRetiro.ListarProcesoPorId(reader);
                    }
                    reader.Close();

                    return Lis_AutorizacionRetiro;
                }
            }
            catch (Exception e)
            {              
                return null;

            }
        }

        public BE_AutorizacionRetiro ListarAutorizacionRetiroPorID_Rep(Int32 IdAutRet)
        {
            IDataReader reader = null;
            try
            {
                using (Database dbTerminal = new Database())
                {
                    dbTerminal.ProcedureName = "[Documentacion_Rep_AutorizacionRetiro_Cab]";
                    dbTerminal.AddParameter("@vi_IdAutRet", DbType.Int32, ParameterDirection.Input, IdAutRet);
                    reader = dbTerminal.GetDataReader();

                    BE_AutorizacionRetiro Lis_AutorizacionRetiro = new BE_AutorizacionRetiro();
                    while (reader.Read())
                    {
                        Lis_AutorizacionRetiro = Pu_AutorizacionRetiro.ListarAutorizacionRetiroPorId_Rep(reader);
                    }
                    reader.Close();

                    return Lis_AutorizacionRetiro;
                }
            }
            catch (Exception e)
            {
                ResultadoTransaccionTx oResultadoTransaccionTx = new ResultadoTransaccionTx(ResultadoTransaccionTx.TipoResultado.Error, e);
                oResultadoTransaccionTx.RegistrarError(e);
                return null;

            }
        }

        public List<BE_AutorizacionRetiro> ListarAutorizacionRetiroPorID_Det_Rep(Int32 IdAutRet)
        {
            IDataReader reader = null;
            try
            {
                using (Database dbTerminal = new Database())
                {
                    dbTerminal.ProcedureName = "[Documentacion_Rep_AutorizacionRetiro_Det]";
                    dbTerminal.AddParameter("@vi_IdAutRet", DbType.Int32, ParameterDirection.Input, IdAutRet);
                    reader = dbTerminal.GetDataReader();

                    List<BE_AutorizacionRetiro> Lis_AutorizacionRetiro = new List<BE_AutorizacionRetiro>();
                    while (reader.Read())
                    {
                        Lis_AutorizacionRetiro.Add(Pu_AutorizacionRetiro.ListarAutorizacionRetiroPorId_Rep(reader));
                    }
                    reader.Close();

                    return Lis_AutorizacionRetiro;
                }
            }
            catch (Exception e)
            {
                ResultadoTransaccionTx oResultadoTransaccionTx = new ResultadoTransaccionTx(ResultadoTransaccionTx.TipoResultado.Error, e);
                oResultadoTransaccionTx.RegistrarError(e);
                return null;

            }
        }

    }
}
