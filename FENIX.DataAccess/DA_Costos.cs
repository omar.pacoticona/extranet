﻿using System;
using System.Collections.Generic;
using System.Text;
using FENIX.BusinessEntity;
using FENIX.Common;
using System.Data.Common;
using System.Data;

namespace FENIX.DataAccess
{
    public class DA_Costos
    {
        


       

        public List<BE_Costos> ListarCliente()
        {
            IDataReader reader = null;

            try
            {
                using (Database dbTerminal = new Database())
                {
                    dbTerminal.ProcedureName = "[COM_Lis_Clientes]";
                    dbTerminal.AddParameter("@vi_IdRol", DbType.Int32, ParameterDirection.Input, 100);
                    //dbTerminal.AddParameter("@Client_vch_NumDocumento", DbType.String, ParameterDirection.Input, "");
                    //dbTerminal.AddParameter("@Client_vch_Razon_Social", DbType.String, ParameterDirection.Input, "");
                    //dbTerminal.AddParameter("@Cliente_chr_Situacion", DbType.String, ParameterDirection.Input, "");

                    reader = dbTerminal.GetDataReader();

                    List<BE_Costos> Lst_Tabla = new List<BE_Costos>();
                    while (reader.Read())
                    {
                        Lst_Tabla.Add(Pu_Costos.ListarCliente(reader));

                    }
                    reader.Close();

                    return Lst_Tabla;
                }

            }
            catch (Exception e)
            {
                return null;
            }
        }

        //public List<BE_Presupuesto> ListarVtasXTipoCli(BE_Presupuesto oBE_Presupuesto)
        //{
        //    IDataReader reader = null;

        //    try
        //    {
        //        using (Database dbTerminal = new Database())
        //        {
        //            dbTerminal.ProcedureName = "[Extranet_LisVtasXTipoCliente]";
        //            dbTerminal.AddParameter("@vi_Periodo", DbType.Int32, ParameterDirection.Input, Convert.ToInt32(oBE_Presupuesto.sPeriodo));
        //            dbTerminal.AddParameter("@vi_MesIni", DbType.Int32, ParameterDirection.Input, oBE_Presupuesto.iMesIni);
        //            dbTerminal.AddParameter("@vi_MesFin", DbType.Int32, ParameterDirection.Input, oBE_Presupuesto.iMesFin);

        //            reader = dbTerminal.GetDataReader();

        //            List<BE_Presupuesto> Lst_Ventas = new List<BE_Presupuesto>();
        //            while (reader.Read())
        //            {
        //                Lst_Ventas.Add(Pu_Presupuesto.ListarVtasXTipoCli(reader));

        //            }
        //            reader.Close();

        //            return Lst_Ventas;
        //        }

        //    }
        //    catch (Exception e)
        //    {
        //        return null;
        //    }
        //}


        //public List<BE_Presupuesto> ListarVtasXTipoCarga(BE_Presupuesto oBE_Presupuesto)
        //{
        //    IDataReader reader = null;

        //    try
        //    {
        //        using (Database dbTerminal = new Database())
        //        {
        //            dbTerminal.ProcedureName = "[Extranet_LisCNT_TNXConsign]";
        //            dbTerminal.AddParameter("@vi_Periodo", DbType.Int32, ParameterDirection.Input, Convert.ToInt32(oBE_Presupuesto.sPeriodo));
        //            dbTerminal.AddParameter("@vi_MesIni", DbType.Int32, ParameterDirection.Input, oBE_Presupuesto.iMesIni);
        //            dbTerminal.AddParameter("@vi_MesFin", DbType.Int32, ParameterDirection.Input, oBE_Presupuesto.iMesFin);
        //            dbTerminal.AddParameter("@vi_TipoOpe", DbType.Int32, ParameterDirection.Input, oBE_Presupuesto.iOperacion);
        //            dbTerminal.AddParameter("@vi_LinNegocio", DbType.Int32, ParameterDirection.Input, oBE_Presupuesto.iIdLineaNegocio);
        //            dbTerminal.AddParameter("@vi_NroLimite", DbType.Int32, ParameterDirection.Input, oBE_Presupuesto.iLimite);

        //            reader = dbTerminal.GetDataReader();

        //            List<BE_Presupuesto> Lst_Ventas = new List<BE_Presupuesto>();
        //            while (reader.Read())
        //            {
        //                Lst_Ventas.Add(Pu_Presupuesto.ListarVtasXTipoCarga(reader));

        //            }
        //            reader.Close();

        //            return Lst_Ventas;
        //        }

        //    }
        //    catch (Exception e)
        //    {
        //        return null;
        //    }
        //}


        public List<BE_Costos> ListarVtasXPeriodo(BE_Costos oBE_Costos)
        {
            IDataReader reader = null;

            try
            {
                using (Database dbTerminal = new Database())
                {
                    dbTerminal.ProcedureName = "[Extranet_LisVtasCostoXPeriodo]";
                    dbTerminal.AddParameter("@vi_Periodo", DbType.Int32, ParameterDirection.Input, Convert.ToInt32(oBE_Costos.sPeriodo));
                    dbTerminal.AddParameter("@vi_MesIni", DbType.Int32, ParameterDirection.Input, oBE_Costos.iMesIni);
                    dbTerminal.AddParameter("@vi_MesFin", DbType.Int32, ParameterDirection.Input, oBE_Costos.iMesFin);

                    reader = dbTerminal.GetDataReader();

                    List<BE_Costos> Lst_Costos = new List<BE_Costos>();
                    while (reader.Read())
                    {
                        Lst_Costos.Add(Pu_Costos.ListarVtasXPeriodo(reader));

                    }
                    reader.Close();

                    return Lst_Costos;
                }

            }
            catch (Exception e)
            {
                return null;
            }
        }

        public List<BE_Costos> ListarVtasXLinea(BE_Costos oBE_Costos)
        {
            IDataReader reader = null;

            try
            {
                using (Database dbTerminal = new Database())
                {
                    dbTerminal.ProcedureName = "[Extranet_LisVtasCostoXLineaNeg]";
                    dbTerminal.AddParameter("@vi_Periodo", DbType.Int32, ParameterDirection.Input, Convert.ToInt32(oBE_Costos.sPeriodo));
                    dbTerminal.AddParameter("@vi_MesIni", DbType.Int32, ParameterDirection.Input, oBE_Costos.iMesIni);
                    dbTerminal.AddParameter("@vi_MesFin", DbType.Int32, ParameterDirection.Input, oBE_Costos.iMesFin);
                    dbTerminal.AddParameter("@vi_Cliente", DbType.Int32, ParameterDirection.Input, oBE_Costos.iIdCliente);                    

                    reader = dbTerminal.GetDataReader();

                    List<BE_Costos> Lst_Ventas = new List<BE_Costos>();
                    while (reader.Read())
                    {
                        Lst_Ventas.Add(Pu_Costos.ListarVtasXLinea(reader));
                    }
                    reader.Close();

                    return Lst_Ventas;
                }

            }
            catch (Exception e)
            {
                return null;
            }
        }


        public List<BE_Costos> ListarVtasXCliente(BE_Costos oBE_Costos)
        {
            IDataReader reader = null;

            try
            {
                using (Database dbTerminal = new Database())
                {
                    dbTerminal.ProcedureName = "[Extranet_LisVtasCostoXCliente]";
                    dbTerminal.AddParameter("@vi_Periodo", DbType.Int32, ParameterDirection.Input, Convert.ToInt32(oBE_Costos.sPeriodo));
                    dbTerminal.AddParameter("@vi_MesIni", DbType.Int32, ParameterDirection.Input, oBE_Costos.iMesIni);
                    dbTerminal.AddParameter("@vi_MesFin", DbType.Int32, ParameterDirection.Input, oBE_Costos.iMesFin);
                    dbTerminal.AddParameter("@vi_TipoOpe", DbType.Int32, ParameterDirection.Input, oBE_Costos.iOperacion);
                    dbTerminal.AddParameter("@vi_LinNegocio", DbType.Int32, ParameterDirection.Input, oBE_Costos.iIdLineaNegocio);
                    dbTerminal.AddParameter("@vi_NroLimite", DbType.Int32, ParameterDirection.Input, oBE_Costos.iLimite);

                    reader = dbTerminal.GetDataReader();

                    List<BE_Costos> Lst_Costos = new List<BE_Costos>();
                    while (reader.Read())
                    {
                        Lst_Costos.Add(Pu_Costos.ListarVtasXCliente(reader));
                    }
                    reader.Close();

                    return Lst_Costos;
                }

            }
            catch (Exception e)
            {
                return null;
            }
        }

        //public List<BE_Presupuesto> ListarVtasXVendedor(BE_Presupuesto oBE_Presupuesto)
        //{
        //    IDataReader reader = null;

        //    try
        //    {
        //        using (Database dbTerminal = new Database())
        //        {
        //            dbTerminal.ProcedureName = "[Extranet_LisVtasXVendedor]";
        //            dbTerminal.AddParameter("@vi_Periodo", DbType.Int32, ParameterDirection.Input, Convert.ToInt32(oBE_Presupuesto.sPeriodo));
        //            dbTerminal.AddParameter("@vi_MesIni", DbType.Int32, ParameterDirection.Input, oBE_Presupuesto.iMesIni);
        //            dbTerminal.AddParameter("@vi_MesFin", DbType.Int32, ParameterDirection.Input, oBE_Presupuesto.iMesFin);
        //           // dbTerminal.AddParameter("@vi_LinNegocio", DbType.Int32, ParameterDirection.Input, oBE_Presupuesto.iIdLineaNegocio);
        //           // dbTerminal.AddParameter("@vi_Vendedor", DbType.Int32, ParameterDirection.Input, oBE_Presupuesto.iIdVendedor);

        //            reader = dbTerminal.GetDataReader();

        //            List<BE_Presupuesto> Lst_Ventas = new List<BE_Presupuesto>();
        //            while (reader.Read())
        //            {
        //                Lst_Ventas.Add(Pu_Presupuesto.ListarVtasXVendedor(reader));

        //            }
        //            reader.Close();

        //            return Lst_Ventas;
        //        }

        //    }
        //    catch (Exception e)
        //    {
        //        return null;
        //    }
        //}


        public List<BE_Costos> ListarVtasXServicio(BE_Costos oBE_Costos)
        {
            IDataReader reader = null;

            try
            {
                using (Database dbTerminal = new Database())
                {
                    dbTerminal.ProcedureName = "[Extranet_LisVtasCostoXServicio]";
                    dbTerminal.AddParameter("@vi_Periodo", DbType.Int32, ParameterDirection.Input, Convert.ToInt32(oBE_Costos.sPeriodo));
                    dbTerminal.AddParameter("@vi_MesIni", DbType.Int32, ParameterDirection.Input, oBE_Costos.iMesIni);
                    dbTerminal.AddParameter("@vi_MesFin", DbType.Int32, ParameterDirection.Input, oBE_Costos.iMesFin);
                    dbTerminal.AddParameter("@vi_LinNegocio", DbType.Int32, ParameterDirection.Input, oBE_Costos.iIdLineaNegocio);                    
                    dbTerminal.AddParameter("@vi_Cliente", DbType.Int32, ParameterDirection.Input, oBE_Costos.iIdCliente);
                    dbTerminal.AddParameter("@vi_NroLimite", DbType.Int32, ParameterDirection.Input, oBE_Costos.iLimite);

                    reader = dbTerminal.GetDataReader();

                    List<BE_Costos> Lst_Ventas = new List<BE_Costos>();
                    while (reader.Read())
                    {
                        Lst_Ventas.Add(Pu_Costos.ListarVtasXServicio(reader));

                    }
                    reader.Close();

                    return Lst_Ventas;
                }

            }
            catch (Exception e)
            {
                return null;
            }
        }


       
    }

}
