﻿using System;
using System.Collections.Generic;
using System.Text;
using FENIX.BusinessEntity;
using System.Data;
using FENIX.Common;
using System.Data.SqlClient;

namespace FENIX.DataAccess
{
    public class DA_DocumentoOrigen
    {

        #region "Transaccion"

        public String GrabarAutorizacDscto(BE_DocumentoOrigen oBE_DocumentoOrigen)
        {
            try
            {
                using (WebDatabase dbTerminal = new WebDatabase())
                {
                    dbTerminal.ProcedureName = "[Extranet_Ins_RsptaAutorizDscto]";
                    dbTerminal.AddParameter("@vi_IdDscto", DbType.Int32, ParameterDirection.Input, oBE_DocumentoOrigen.iIdDscto);
                    dbTerminal.AddParameter("@vi_Usuario", DbType.String, ParameterDirection.Input, oBE_DocumentoOrigen.sUsuario);
                    dbTerminal.AddParameter("@vi_Acepta", DbType.String, ParameterDirection.Input, oBE_DocumentoOrigen.sAcepta);
                    dbTerminal.AddParameter("@vo_Resultado", DbType.String, ParameterDirection.Output, String.Empty, 50);

                    dbTerminal.Execute();
                    return dbTerminal.GetParameter("@vo_Resultado").ToString();
                }
            }
            catch (Exception e)
            {
                return null;
            }
        }

        #endregion

        #region "Listado"


        public BE_DocumentoOrigenListaList ListarDO(BE_DocumentoOrigen oBE_DocumentoOrigen)
        {
            IDataReader reader = null;

            try
            {
                using (Database dbTerminal = new Database())
                {
                    dbTerminal.ProcedureName = "[EXTRANET_Lis_DocumentoOrigen]";
                    dbTerminal.AddParameter("@vi_IdAgenciaAduana", DbType.Int32, ParameterDirection.Input, oBE_DocumentoOrigen.iIdCliente);
                    dbTerminal.AddParameter("@vi_IdNaveViaje", DbType.Int32, ParameterDirection.Input, oBE_DocumentoOrigen.iIdNaveViaje);
                    dbTerminal.AddParameter("@vi_AnoManifiesto", DbType.String, ParameterDirection.Input, oBE_DocumentoOrigen.sAnoManifiesto);
                    dbTerminal.AddParameter("@vi_NumManifiesto", DbType.String, ParameterDirection.Input, oBE_DocumentoOrigen.sNumManifiesto);
                    dbTerminal.AddParameter("@vi_NumeroDO", DbType.String, ParameterDirection.Input, oBE_DocumentoOrigen.sNumeroDO);
                    dbTerminal.AddParameter("@vi_NumeroDOMadre", DbType.String, ParameterDirection.Input, oBE_DocumentoOrigen.sNumeroDOMadre);
                    dbTerminal.AddParameter("@vi_FechaEstimadaIni", DbType.DateTime, ParameterDirection.Input, oBE_DocumentoOrigen.dtFechaEtaInicio);
                    dbTerminal.AddParameter("@vi_FechaEstimadaFin", DbType.DateTime, ParameterDirection.Input, oBE_DocumentoOrigen.dtFechaEtaFinal);
                    dbTerminal.AddParameter("@vi_Contenedor", DbType.String, ParameterDirection.Input, oBE_DocumentoOrigen.sContenedor);
                    dbTerminal.AddParameter("@vi_EstadoTransmitido", DbType.String, ParameterDirection.Input, oBE_DocumentoOrigen.sEstadoTransmitido);
                    dbTerminal.AddParameter("@Vi_Razon_Social", DbType.String, ParameterDirection.Input, oBE_DocumentoOrigen.sRazonSocial);
                    dbTerminal.AddParameter("@vi_IdLineaNegocio", DbType.String, ParameterDirection.Input, oBE_DocumentoOrigen.iIdLineaNegocio);
                    dbTerminal.AddParameter("@vi_TipoOperacion", DbType.String, ParameterDirection.Input, oBE_DocumentoOrigen.iIdTipoOperacion);
                    dbTerminal.AddParameter("@vi_numeropagina", DbType.Int32, ParameterDirection.Input, oBE_DocumentoOrigen.NPagina);
                    dbTerminal.AddParameter("@vi_numeroregistros", DbType.Int32, ParameterDirection.Input, oBE_DocumentoOrigen.NRegistros);
                    dbTerminal.AddParameter("@vo_totalregistros", DbType.Int32, ParameterDirection.Output, oBE_DocumentoOrigen.NTotalRegistros);
                    dbTerminal.AddParameter("@vi_NroOrden", DbType.String, ParameterDirection.Input, oBE_DocumentoOrigen.sNroOrden);

                    reader = dbTerminal.GetDataReader();

                    BE_DocumentoOrigenListaList Lst_DocumentoOrigen = new BE_DocumentoOrigenListaList();
                    while (reader.Read())
                    {
                        Lst_DocumentoOrigen.Add(Pu_DocumentoOrigen.ListarDO(reader));
                    }
                    reader.Close();
                    oBE_DocumentoOrigen.NTotalRegistros = Convert.ToInt32(dbTerminal.GetParameter("@vo_totalregistros"));
                    return Lst_DocumentoOrigen;
                }
            }
            catch (Exception e)
            {
                ResultadoTransaccionTx oResultadoTransaccionTx = new ResultadoTransaccionTx(ResultadoTransaccionTx.TipoResultado.Error, e);
                oResultadoTransaccionTx.RegistrarError(e);
                return null;
            }
        }

        public BE_DocumentoOrigenDetalleListaList ListarDODetalle(BE_DocumentoOrigen oBE_DocumentoOrigen)
        {
            IDataReader reader = null;

            try
            {
                using (Database dbTerminal = new Database())
                {
                    dbTerminal.ProcedureName = "[EXTRANET_Lis_DocumentoOrigenDetalle]";
                    dbTerminal.AddParameter("@vi_NumeroDO", DbType.String, ParameterDirection.Input, oBE_DocumentoOrigen.sNumeroDO);
                    dbTerminal.AddParameter("@vi_IdCliente", DbType.Int32, ParameterDirection.Input, oBE_DocumentoOrigen.iIdCliente);
                    dbTerminal.AddParameter("@vi_numeropagina", DbType.Int32, ParameterDirection.Input, oBE_DocumentoOrigen.NPagina);
                    dbTerminal.AddParameter("@vi_numeroregistros", DbType.Int32, ParameterDirection.Input, oBE_DocumentoOrigen.NRegistros);
                    dbTerminal.AddParameter("@vo_totalregistros", DbType.Int32, ParameterDirection.Output, oBE_DocumentoOrigen.NtotalRegistros);


                    reader = dbTerminal.GetDataReader();

                    BE_DocumentoOrigenDetalleListaList Lst_DocumentoOrigenDetalle = new BE_DocumentoOrigenDetalleListaList();
                    while (reader.Read())
                    {
                        Lst_DocumentoOrigenDetalle.Add(Pu_DocumentoOrigen.ListarDODetalle(reader));
                    }
                    reader.Close();
                    oBE_DocumentoOrigen.NtotalRegistros = Convert.ToInt32(dbTerminal.GetParameter("@vo_totalregistros"));
                    return Lst_DocumentoOrigenDetalle;
                }
            }
            catch (Exception e)
            {
                ResultadoTransaccionTx oResultadoTransaccionTx = new ResultadoTransaccionTx(ResultadoTransaccionTx.TipoResultado.Error, e);
                oResultadoTransaccionTx.RegistrarError(e);
                return null;
            }
        }

        public List<BE_DocumentoOrigen> ListarVolantesWeb(BE_DocumentoOrigen oBE_DocumentoOrigen)
        {
            IDataReader reader = null;
            try
            {
                using (Database dbTerminal = new Database())
                {
                    dbTerminal.ProcedureName = "[Extranet_Operacion_Lis_Volantes]";
                    dbTerminal.AddParameter("@vi_IdCliente", DbType.Int32, ParameterDirection.Input, oBE_DocumentoOrigen.iIdCliente);
                    dbTerminal.AddParameter("@vi_AnoManifiesto", DbType.String, ParameterDirection.Input, oBE_DocumentoOrigen.sAnoManifiesto);
                    dbTerminal.AddParameter("@vi_NumManifiesto", DbType.String, ParameterDirection.Input, oBE_DocumentoOrigen.sNumManifiesto);
                    dbTerminal.AddParameter("@vi_DocumentoMaster", DbType.String, ParameterDirection.Input, oBE_DocumentoOrigen.sDocumentoMaster);
                    dbTerminal.AddParameter("@vi_DocumentoHijo", DbType.String, ParameterDirection.Input, oBE_DocumentoOrigen.sDocumentoHijo);
                    dbTerminal.AddParameter("@vi_Contenedor", DbType.String, ParameterDirection.Input, oBE_DocumentoOrigen.sContenedor);
                    dbTerminal.AddParameter("@vi_Volante", DbType.String, ParameterDirection.Input, oBE_DocumentoOrigen.sVolante);
                    dbTerminal.AddParameter("@vo_totalregistros", DbType.Int32, ParameterDirection.Output, oBE_DocumentoOrigen.NTotalRegistros);

                    reader = dbTerminal.GetDataReader();

                    List<BE_DocumentoOrigen> Lis_Volante = new List<BE_DocumentoOrigen>();
                    while (reader.Read())
                    {
                        Lis_Volante.Add(Pu_DocumentoOrigen.ListarVolantesWeb(reader));
                    }
                    reader.Close();
                    oBE_DocumentoOrigen.NTotalRegistros = Convert.ToInt32(dbTerminal.GetParameter("@vo_totalregistros").ToString());
                    return Lis_Volante;
                }
            }
            catch (Exception e)
            {
                ResultadoTransaccionTx oResultadoTransaccionTx = new ResultadoTransaccionTx(ResultadoTransaccionTx.TipoResultado.Error, e);
                oResultadoTransaccionTx.RegistrarError(e);
                return null;
            }
        }

        public List<BE_DocumentoOrigen> ListarVolantesParaServicio(BE_DocumentoOrigen oBE_DocumentoOrigen)
        {
            IDataReader reader = null;
            try
            {
                using (Database dbTerminal = new Database())
                {
                    dbTerminal.ProcedureName = "[OPE_Lis_Orden_Servicio_Web_2]";
                    dbTerminal.AddParameter("@vi_IdCliente", DbType.Int32, ParameterDirection.Input, oBE_DocumentoOrigen.iIdCliente);
                    dbTerminal.AddParameter("@vi_AnoManifiesto", DbType.String, ParameterDirection.Input, oBE_DocumentoOrigen.sAnoManifiesto);
                    dbTerminal.AddParameter("@vi_NumManifiesto", DbType.String, ParameterDirection.Input, oBE_DocumentoOrigen.sNumManifiesto);
                    dbTerminal.AddParameter("@vi_Documento", DbType.String, ParameterDirection.Input, oBE_DocumentoOrigen.sDocumentoMaster);
                    dbTerminal.AddParameter("@vi_Volante", DbType.String, ParameterDirection.Input, oBE_DocumentoOrigen.sVolante);
                    dbTerminal.AddParameter("@vi_Orden", DbType.String, ParameterDirection.Input, oBE_DocumentoOrigen.sOrden);
                    dbTerminal.AddParameter("@vo_totalregistros", DbType.Int32, ParameterDirection.Output, oBE_DocumentoOrigen.NTotalRegistros);

                    reader = dbTerminal.GetDataReader();

                    List<BE_DocumentoOrigen> Lis_OrdenServicio = new List<BE_DocumentoOrigen>();
                    while (reader.Read())
                    {
                        Lis_OrdenServicio.Add(Pu_DocumentoOrigen.ListarVolantesParaServicio(reader));
                    }
                    reader.Close();
                    oBE_DocumentoOrigen.NTotalRegistros = Convert.ToInt32(dbTerminal.GetParameter("@vo_totalregistros").ToString());
                    return Lis_OrdenServicio;
                }
            }
            catch (Exception e)
            {
                ResultadoTransaccionTx oResultadoTransaccionTx = new ResultadoTransaccionTx(ResultadoTransaccionTx.TipoResultado.Error, e);
                oResultadoTransaccionTx.RegistrarError(e);
                return null;
            }
        }

        public List<BE_DocumentoOrigen> ListarBLsparaVolante(BE_DocumentoOrigen oBE_DocumentoOrigen)
        {
            IDataReader reader = null;
            try
            {
                using (Database dbTerminal = new Database())
                {
                    dbTerminal.ProcedureName = "[Documentacion_Lis_SituacionParaVolante]";
                    dbTerminal.AddParameter("@vi_Documentos", DbType.String, ParameterDirection.Input, oBE_DocumentoOrigen.sDocumentoHijo);
                    dbTerminal.AddParameter("@vi_IdAgenciaAduana", DbType.Int32, ParameterDirection.Input, oBE_DocumentoOrigen.iIdCliente);
                    dbTerminal.AddParameter("@vi_Usuario", DbType.String, ParameterDirection.Input, oBE_DocumentoOrigen.sUsuario);
                    dbTerminal.AddParameter("@vi_NombrePc", DbType.String, ParameterDirection.Input, oBE_DocumentoOrigen.sNombrePc);
                    dbTerminal.AddParameter("@vo_totalregistros", DbType.Int32, ParameterDirection.Output, oBE_DocumentoOrigen.NTotalRegistros);

                    reader = dbTerminal.GetDataReader();

                    List<BE_DocumentoOrigen> Lis_Volante = new List<BE_DocumentoOrigen>();
                    while (reader.Read())
                    {
                        Lis_Volante.Add(Pu_DocumentoOrigen.ListarBLsparaVolante(reader));
                    }
                    reader.Close();
                    oBE_DocumentoOrigen.NTotalRegistros = Convert.ToInt32(dbTerminal.GetParameter("@vo_totalregistros").ToString());
                    return Lis_Volante;
                }
            }
            catch (Exception e)
            {
                ResultadoTransaccionTx oResultadoTransaccionTx = new ResultadoTransaccionTx(ResultadoTransaccionTx.TipoResultado.Error, e);
                oResultadoTransaccionTx.RegistrarError(e);
                return null;
            }
        }

        public List<BE_DocumentoOrigen> ListarDatosVolante(BE_DocumentoOrigen oBE_DocumentoOrigen)
        {
            IDataReader reader = null;
            try
            {
                using (Database dbTerminal = new Database())
                {
                    dbTerminal.ProcedureName = "[Documentacion_Rep_Volante]";
                    dbTerminal.AddParameter("@vi_IdVolante", DbType.Int32, ParameterDirection.Input, oBE_DocumentoOrigen.iIdVolante);
                    dbTerminal.AddParameter("@vi_Usuario", DbType.String, ParameterDirection.Input, oBE_DocumentoOrigen.sUsuario);
                    dbTerminal.AddParameter("@vi_IdDocOri", DbType.Int32, ParameterDirection.Input, oBE_DocumentoOrigen.iIdDocOri);

                    reader = dbTerminal.GetDataReader();

                    List<BE_DocumentoOrigen> Lis_DocumentoOrigen = new List<BE_DocumentoOrigen>();
                    while (reader.Read())
                    {
                        Lis_DocumentoOrigen.Add(Pu_DocumentoOrigen.ListarDatosVolante(reader));
                    }
                    reader.Close();

                    return Lis_DocumentoOrigen;
                }
            }
            catch (Exception e)
            {
                ResultadoTransaccionTx oResultadoTransaccionTx = new ResultadoTransaccionTx(ResultadoTransaccionTx.TipoResultado.Error, e);
                oResultadoTransaccionTx.RegistrarError(e);
                return null;
            }
        }

        public List<BE_DocumentoOrigen> BuscaDatosDsctoAlmac(Int32 iIdDscto)
        {
            IDataReader reader = null;
            try
            {
                using (Database dbTerminal = new Database())
                {
                    dbTerminal.ProcedureName = "[Extranet_LisDocDscto]";
                    dbTerminal.AddParameter("@vi_IdDscto", DbType.Int32, ParameterDirection.Input, iIdDscto);

                    reader = dbTerminal.GetDataReader();

                    List<BE_DocumentoOrigen> Lis_DocumentoOrigen = new List<BE_DocumentoOrigen>();
                    while (reader.Read())
                    {
                        Lis_DocumentoOrigen.Add(Pu_DocumentoOrigen.ListarDatosDscto(reader));
                    }
                    reader.Close();

                    return Lis_DocumentoOrigen;
                }
            }
            catch (Exception e)
            {
                ResultadoTransaccionTx oResultadoTransaccionTx = new ResultadoTransaccionTx(ResultadoTransaccionTx.TipoResultado.Error, e);
                oResultadoTransaccionTx.RegistrarError(e);
                return null;
            }
        }

        public BE_AutorizacionRetiroList ListarAutRetiro(BE_AutorizacionRetiro oBE_AutRet)
        {
            IDataReader reader = null;
            try
            {
                using (Database dbTerminal = new Database())
                {
                    dbTerminal.ProcedureName = "[Documentacion_Lis_AutorizacionRetiroExtranet]";
                    dbTerminal.AddParameter("@vi_NroAutRet", DbType.String, ParameterDirection.Input, oBE_AutRet.SNumAutRet);
                    dbTerminal.AddParameter("@vi_Cliente", DbType.String, ParameterDirection.Input, oBE_AutRet.SCliente);
                    dbTerminal.AddParameter("@vi_Agencia", DbType.Int32, ParameterDirection.Input, oBE_AutRet.iIdAgencia);
                    dbTerminal.AddParameter("@vi_NumeroDO", DbType.String, ParameterDirection.Input, oBE_AutRet.SDocumentoO);
                    dbTerminal.AddParameter("@vi_Fecha_Desde", DbType.DateTime, ParameterDirection.Input, oBE_AutRet.dtFechaDesde);
                    dbTerminal.AddParameter("@vi_Fecha_Hasta", DbType.DateTime, ParameterDirection.Input, oBE_AutRet.dtFechaHasta);
                    dbTerminal.AddParameter("@vi_Volante", DbType.String, ParameterDirection.Input, oBE_AutRet.SVolante);
                    dbTerminal.AddParameter("@vi_Contenedor", DbType.String, ParameterDirection.Input, oBE_AutRet.SContenedor);

                    reader = dbTerminal.GetDataReader();

                    BE_AutorizacionRetiroList ListAutRet = new BE_AutorizacionRetiroList();
                    while (reader.Read())
                    {
                        ListAutRet.Add(Pu_DocumentoOrigen.ListarAutRetiro(reader));
                    }
                    reader.Close();
                    return ListAutRet;
                }
            }
            catch (Exception e)
            {
                ResultadoTransaccionTx oResultadoTransaccionTx = new ResultadoTransaccionTx(ResultadoTransaccionTx.TipoResultado.Error, e);
                oResultadoTransaccionTx.RegistrarError(e);
                return null;
            }
        }

        public BE_AutorizacionRetiro ConsultarAutRetId(Int32 iIdAutRet)
        {
            IDataReader reader = null;
            try
            {
                using (Database dbTerminal = new Database())
                {
                    dbTerminal.ProcedureName = "[Documentacion_Lis_AutorizacionRetiroPorIDExtranet]";
                    dbTerminal.AddParameter("@vi_IdAutRet", DbType.String, ParameterDirection.Input, iIdAutRet);
                    reader = dbTerminal.GetDataReader();

                    BE_AutorizacionRetiro ListAutRet = new BE_AutorizacionRetiro();
                    while (reader.Read())
                    {
                        ListAutRet = Pu_DocumentoOrigen.ConsultarAutRetId(reader);
                    }
                    reader.Close();
                    return ListAutRet;
                }
            }
            catch (Exception e)
            {
                ResultadoTransaccionTx oResultadoTransaccionTx = new ResultadoTransaccionTx(ResultadoTransaccionTx.TipoResultado.Error, e);
                oResultadoTransaccionTx.RegistrarError(e);
                return null;
            }
        }

        public String InsertarPlacaChofer(BE_PlacaChofer objEntidad, int tipo, int accion)
        {

            try
            {
                using (Database dbTerminal = new Database())
                {
                    dbTerminal.ProcedureName = "[DOC_Ins_AutRetiroPlacaChoferExtranet]";
                    dbTerminal.AddParameter("@vi_Placa", DbType.String, ParameterDirection.Input, objEntidad.SPlaca);
                    dbTerminal.AddParameter("@vi_Chofer", DbType.String, ParameterDirection.Input, objEntidad.SChofer.Trim());
                    dbTerminal.AddParameter("@vi_Doc", DbType.String, ParameterDirection.Input, objEntidad.SDoc);
                    dbTerminal.AddParameter("@vi_Brevete", DbType.String, ParameterDirection.Input, objEntidad.sBrevete);
                    dbTerminal.AddParameter("@Usuario", DbType.String, ParameterDirection.Input, objEntidad.SUsuario);
                    dbTerminal.AddParameter("@NombrePc", DbType.String, ParameterDirection.Input, objEntidad.SPC);
                    dbTerminal.AddParameter("@vi_IdAutRet", DbType.String, ParameterDirection.Input, objEntidad.IIdAutRet);
                    dbTerminal.AddParameter("@vi_tipo", DbType.Int32, ParameterDirection.Input, tipo);
                    dbTerminal.AddParameter("@vi_accion", DbType.Int32, ParameterDirection.Input, accion);
                    dbTerminal.AddParameter("@vo_Resultado", DbType.String, ParameterDirection.Output, String.Empty, 300);

                    dbTerminal.Execute();
                    String Retorno = dbTerminal.GetParameter("@vo_Resultado").ToString();
                    return Retorno;

                }
            }
            catch (Exception e)
            {
                return "-999|Ocurrio un error en la capa de datos";
            }

        }

        public BE_PlacaChoferList ListarPlacaChofer(int IdAutRet)
        {
            IDataReader reader = null;

            try
            {
                using (Database dbTerminal = new Database())
                {
                    dbTerminal.ProcedureName = "[DOC_Lis_AutRetiroPlacaChoferExtranet]";
                    dbTerminal.AddParameter("@vi_IdAutRet", DbType.String, ParameterDirection.Input, IdAutRet);
                    reader = dbTerminal.GetDataReader();

                    BE_PlacaChoferList Lst_Dua = new BE_PlacaChoferList();
                    while (reader.Read())
                    {
                        Lst_Dua.Add(Pu_DocumentoOrigen.ListarPlacaChofer(reader));

                    }
                    reader.Close();

                    return Lst_Dua;
                }

            }
            catch (Exception e)
            {
                ResultadoTransaccionTx oResultadoTransaccionTx = new ResultadoTransaccionTx(ResultadoTransaccionTx.TipoResultado.Error, e);
                oResultadoTransaccionTx.RegistrarError(e);
                return null;
            }
        }

        public ResultadoTransaccionTx EliminarPlacaChofer(Int32 IdRegistro, String Usuario, String NombrePc)
        {

            try
            {
                using (Database dbTerminal = new Database())
                {
                    dbTerminal.ProcedureName = "[DOC_Del_PlacaChofer]";
                    dbTerminal.AddParameter("@IdRegistro", DbType.Int32, ParameterDirection.Input, IdRegistro);
                    dbTerminal.AddParameter("@Usuario", DbType.String, ParameterDirection.Input, Usuario);
                    dbTerminal.AddParameter("@NombrePc", DbType.String, ParameterDirection.Input, NombrePc);
                    dbTerminal.Execute();
                    return new ResultadoTransaccionTx(ResultadoTransaccionTx.TipoResultado.Exito, null);
                }
            }
            catch (Exception e)
            {
                return new ResultadoTransaccionTx(ResultadoTransaccionTx.TipoResultado.Error, e);
            }
        }

        public BE_AutorizacionRetiroList ListarAutorizacionRetiroDetPorID(BE_AutorizacionRetiro objEntidad)
        {
            IDataReader reader = null;
            try
            {
                using (Database dbTerminal = new Database())
                {
                    dbTerminal.ProcedureName = "[Documentacion_Lis_AutorizacionRetiroDetalleExtranet]";
                    dbTerminal.AddParameter("@vi_IdAutRet", DbType.Int32, ParameterDirection.Input, objEntidad.IIdAutRet);
                    reader = dbTerminal.GetDataReader();

                    BE_AutorizacionRetiroList Lis_BE_AutorizacionRetiroDetPorID = new BE_AutorizacionRetiroList();
                    while (reader.Read())
                    {
                        Lis_BE_AutorizacionRetiroDetPorID.Add(Pu_DocumentoOrigen.ListarAutorizacionRetiroDetPorId(reader));
                    }
                    reader.Close();
                    return Lis_BE_AutorizacionRetiroDetPorID;
                }
            }
            catch (Exception e)
            {
                ResultadoTransaccionTx oResultadoTransaccionTx = new ResultadoTransaccionTx(ResultadoTransaccionTx.TipoResultado.Error, e);
                oResultadoTransaccionTx.RegistrarError(e);
                return null;
            }
        }
        public BE_AutorizacionRetiroList ListarAutorizacionRetiroDetStatusPorID(BE_AutorizacionRetiro objEntidad)
        {
            IDataReader reader = null;
            try
            {
                using (Database dbTerminal = new Database())
                {
                    dbTerminal.ProcedureName = "[Documentacion_Lis_AutorizacionRetiroDetalleStatusExtranet]";
                    dbTerminal.AddParameter("@vi_IdAutRet", DbType.Int32, ParameterDirection.Input, objEntidad.IIdAutRet);
                    reader = dbTerminal.GetDataReader();

                    BE_AutorizacionRetiroList Lis_BE_AutorizacionRetiroDetStatusPorID = new BE_AutorizacionRetiroList();
                    while (reader.Read())
                    {
                        Lis_BE_AutorizacionRetiroDetStatusPorID.Add(Pu_DocumentoOrigen.ListarAutorizacionRetiroDetStatusPorId(reader));
                    }
                    reader.Close();
                    return Lis_BE_AutorizacionRetiroDetStatusPorID;
                }
            }
            catch (Exception e)
            {
                ResultadoTransaccionTx oResultadoTransaccionTx = new ResultadoTransaccionTx(ResultadoTransaccionTx.TipoResultado.Error, e);
                oResultadoTransaccionTx.RegistrarError(e);
                return null;
            }
        }

        public BE_AutorizacionRetiroList ListarDespachador(String Codigo, String Despachador, int IdCliente)
        {
            IDataReader reader = null;
            try
            {
                using (Database dbTerminal = new Database())
                {
                    dbTerminal.ProcedureName = "[Operacion_Lis_DespachadorExtranet]";
                    dbTerminal.AddParameter("@vi_Descripcion", DbType.String, ParameterDirection.Input, Despachador);
                    dbTerminal.AddParameter("@vi_CodigoAduana", DbType.String, ParameterDirection.Input, Codigo);
                    dbTerminal.AddParameter("@vi_IdCliente", DbType.Int32, ParameterDirection.Input, IdCliente);

                    reader = dbTerminal.GetDataReader();

                    BE_AutorizacionRetiroList Lis_BE_AutorizacionRetiroDetPorID = new BE_AutorizacionRetiroList();
                    while (reader.Read())
                    {
                        Lis_BE_AutorizacionRetiroDetPorID.Add(Pu_DocumentoOrigen.ListarDespachador(reader));
                    }
                    reader.Close();
                    return Lis_BE_AutorizacionRetiroDetPorID;
                }
            }
            catch (Exception e)
            {
                ResultadoTransaccionTx oResultadoTransaccionTx = new ResultadoTransaccionTx(ResultadoTransaccionTx.TipoResultado.Error, e);
                oResultadoTransaccionTx.RegistrarError(e);
                return null;
            }
        }

        public String HabilitarAutRet(int IdAutRet, DateTime Vigencia, int dia, String Usuario, String PC)
        {

            try
            {
                using (Database dbTerminal = new Database())
                {
                    dbTerminal.ProcedureName = "[Documentacion_Upd_ProcesoEsp_AutRetiro_Extranet]";
                    dbTerminal.AddParameter("@vi_IdAutRet", DbType.String, ParameterDirection.Input, IdAutRet);
                    dbTerminal.AddParameter("@vi_FechaVigencia", DbType.DateTime, ParameterDirection.Input, Vigencia);
                    dbTerminal.AddParameter("@vi_dias", DbType.Int32, ParameterDirection.Input, dia);
                    //dbTerminal.AddParameter("@vi_presencia", DbType.String, ParameterDirection.Input, Presencia);
                    //dbTerminal.AddParameter("@vi_IdDespachador", DbType.Int32, ParameterDirection.Input, IdDespachador);
                    //dbTerminal.AddParameter("@vi_cont", DbType.String, ParameterDirection.Input, SelCont);
                    //dbTerminal.AddParameter("@vi_IdAutRetDet", DbType.Int32, ParameterDirection.Input, IdAutRetDet);
                    dbTerminal.AddParameter("@Usuario", DbType.String, ParameterDirection.Input, Usuario);
                    dbTerminal.AddParameter("@NombrePc", DbType.String, ParameterDirection.Input, PC);
                    dbTerminal.AddParameter("@vo_Resultado", DbType.String, ParameterDirection.Output, String.Empty, 300);

                    dbTerminal.Execute();
                    String Retorno = dbTerminal.GetParameter("@vo_Resultado").ToString();
                    return Retorno;

                }
            }
            catch (Exception e)
            {
                return "-999|Ocurrio un error en la capa de datos";
            }

        }

        public String ValidarPresencia(int IdCliente, int IdAutRet)
        {
            try
            {
                using (Database dbTerminal = new Database())
                {
                    dbTerminal.ProcedureName = "[Extranet_ValidarPresencia]";
                    dbTerminal.AddParameter("@IdCliente", DbType.String, ParameterDirection.Input, IdCliente);
                    dbTerminal.AddParameter("@IdAutRet", DbType.String, ParameterDirection.Input, IdAutRet);
                    dbTerminal.AddParameter("@retorno", DbType.String, ParameterDirection.Output, String.Empty, 300);

                    dbTerminal.Execute();
                    String Retorno = dbTerminal.GetParameter("@retorno").ToString();
                    return Retorno;

                }
            }
            catch (Exception e)
            {
                return "-999|Ocurrio un error en la capa de datos";
            }
        }

        public BE_PlacaChoferList ListarPlacaChofer2(String AutRet, String Placa, String tipoDoc, String Doc, String Cliente)
        {
            IDataReader reader = null;

            try
            {
                using (Database dbTerminal = new Database())
                {
                    dbTerminal.ProcedureName = "[DOC_Lis_AutRetiroPlacaChoferExtranet2]";
                    dbTerminal.AddParameter("@vi_AutRet", DbType.String, ParameterDirection.Input, AutRet);
                    dbTerminal.AddParameter("@vi_placa", DbType.String, ParameterDirection.Input, Placa);
                    dbTerminal.AddParameter("@vi_tipo", DbType.String, ParameterDirection.Input, tipoDoc);
                    dbTerminal.AddParameter("@vi_doc", DbType.String, ParameterDirection.Input, Doc);
                    dbTerminal.AddParameter("@vi_Cliente", DbType.String, ParameterDirection.Input, Cliente);
                    reader = dbTerminal.GetDataReader();

                    BE_PlacaChoferList Lst_Dua = new BE_PlacaChoferList();
                    while (reader.Read())
                    {
                        Lst_Dua.Add(Pu_DocumentoOrigen.ListarPlacaChofer2(reader));

                    }
                    reader.Close();

                    return Lst_Dua;
                }

            }
            catch (Exception e)
            {
                ResultadoTransaccionTx oResultadoTransaccionTx = new ResultadoTransaccionTx(ResultadoTransaccionTx.TipoResultado.Error, e);
                oResultadoTransaccionTx.RegistrarError(e);
                return null;
            }
        }

        public String ModificarPlacaChofer(int IdRegistro, String AutRet, String Placa, String Tipo, String Doc, String Usuario, int Cliente)
        {
            try
            {
                using (Database dbTerminal = new Database())
                {
                    dbTerminal.ProcedureName = "[AUD_Ins_AutRetiroPlacaChofer]";
                    dbTerminal.AddParameter("@vi_IdRegistro", DbType.String, ParameterDirection.Input, IdRegistro);
                    dbTerminal.AddParameter("@vi_AutRet", DbType.String, ParameterDirection.Input, AutRet);
                    dbTerminal.AddParameter("@vi_Placa", DbType.String, ParameterDirection.Input, Placa);
                    dbTerminal.AddParameter("@vi_TipoDoc", DbType.String, ParameterDirection.Input, Tipo);
                    dbTerminal.AddParameter("@vi_Chofer", DbType.String, ParameterDirection.Input, Doc);
                    dbTerminal.AddParameter("@Usuario", DbType.String, ParameterDirection.Input, Usuario);
                    dbTerminal.AddParameter("@IdCliente", DbType.String, ParameterDirection.Input, Cliente);
                    dbTerminal.AddParameter("@vo_Resultado", DbType.String, ParameterDirection.Output, String.Empty, 300);

                    dbTerminal.Execute();
                    String Retorno = dbTerminal.GetParameter("@vo_Resultado").ToString();
                    return Retorno;

                }
            }
            catch (Exception e)
            {
                return "-999|Ocurrio un error en la capa de datos";
            }
        }

        public String AlertaCodigo(String NumAut, String Placa, String Tipo, String Doc, String Usuario, String CodigoCliente, int IdCliente, int IdRegistro)
        {
            try
            {
                using (Database dbTerminal = new Database())
                {
                    //Falta store procedure
                    dbTerminal.ProcedureName = "[SP_AlertaEnvioCodigo_Extranet]";
                    dbTerminal.AddParameter("@vi_AutRet", DbType.String, ParameterDirection.Input, NumAut);
                    dbTerminal.AddParameter("@vi_Placa", DbType.String, ParameterDirection.Input, Placa);
                    dbTerminal.AddParameter("@vi_TipoDoc", DbType.String, ParameterDirection.Input, Tipo);
                    dbTerminal.AddParameter("@vi_Chofer", DbType.String, ParameterDirection.Input, Doc);
                    dbTerminal.AddParameter("@Usuario", DbType.String, ParameterDirection.Input, Usuario);
                    dbTerminal.AddParameter("@CodigoCliente", DbType.String, ParameterDirection.Input, CodigoCliente);
                    dbTerminal.AddParameter("@IdCliente", DbType.String, ParameterDirection.Input, IdCliente);
                    dbTerminal.AddParameter("@IdRegistro", DbType.String, ParameterDirection.Input, IdRegistro);
                    dbTerminal.AddParameter("@vo_Resultado", DbType.String, ParameterDirection.Output, String.Empty, 300);

                    dbTerminal.Execute();
                    String Retorno = dbTerminal.GetParameter("@vo_Resultado").ToString();
                    return Retorno;

                }
            }
            catch (Exception e)
            {
                return "-999|Ocurrio un error en la capa de datos";
            }
        }

        public BE_DespachadorList ListarDespachador2(String Cliente, String Codigo, String Nombre, String Apellido, String DNI)
        {
            IDataReader reader = null;

            try
            {
                using (Database dbTerminal = new Database())
                {
                    dbTerminal.ProcedureName = "[DOC_Lis_DespachadorExtranet]";
                    dbTerminal.AddParameter("@vi_cliente", DbType.String, ParameterDirection.Input, Cliente);
                    dbTerminal.AddParameter("@vi_codigo", DbType.String, ParameterDirection.Input, Codigo);
                    dbTerminal.AddParameter("@vi_nombre", DbType.String, ParameterDirection.Input, Nombre);
                    dbTerminal.AddParameter("@vi_apellido", DbType.String, ParameterDirection.Input, Apellido);
                    dbTerminal.AddParameter("@vi_dni", DbType.String, ParameterDirection.Input, DNI);
                    reader = dbTerminal.GetDataReader();

                    BE_DespachadorList Lst_Dua = new BE_DespachadorList();
                    while (reader.Read())
                    {
                        Lst_Dua.Add(Pu_DocumentoOrigen.ListarDespachador2(reader));

                    }
                    reader.Close();

                    return Lst_Dua;
                }

            }
            catch (Exception e)
            {
                ResultadoTransaccionTx oResultadoTransaccionTx = new ResultadoTransaccionTx(ResultadoTransaccionTx.TipoResultado.Error, e);
                oResultadoTransaccionTx.RegistrarError(e);
                return null;
            }
        }

        public String GrabarDespachador(String Nombre, String Apellido, String DNI, Int32 IdCliente, String Usuario)
        {
            try
            {
                using (Database dbTerminal = new Database())
                {
                    dbTerminal.ProcedureName = "[DOC_Ins_DespachadorExtranet]";
                    dbTerminal.AddParameter("@vi_nombre", DbType.String, ParameterDirection.Input, Nombre);
                    dbTerminal.AddParameter("@vi_apellido", DbType.String, ParameterDirection.Input, Apellido);
                    dbTerminal.AddParameter("@vi_dni", DbType.String, ParameterDirection.Input, DNI);
                    dbTerminal.AddParameter("@CodigoCliente", DbType.Int32, ParameterDirection.Input, IdCliente);
                    dbTerminal.AddParameter("@Usuario", DbType.String, ParameterDirection.Input, Usuario);
                    dbTerminal.AddParameter("@vo_Resultado", DbType.String, ParameterDirection.Output, String.Empty, 300);

                    dbTerminal.Execute();
                    String Retorno = dbTerminal.GetParameter("@vo_Resultado").ToString();
                    return Retorno;

                }
            }
            catch (Exception e)
            {
                return "-999|Ocurrio un error en la capa de datos";
            }
        }

        public String ACtualizarDespachador(String Codigo, String Nombre, String Apellido, String DNI, String IdCliente, String Usuario)
        {
            try
            {
                using (Database dbTerminal = new Database())
                {
                    dbTerminal.ProcedureName = "[DOC_Upd_DespachadorExtranet]";
                    dbTerminal.AddParameter("@vi_nombre", DbType.String, ParameterDirection.Input, Nombre);
                    dbTerminal.AddParameter("@vi_apellido", DbType.String, ParameterDirection.Input, Apellido);
                    dbTerminal.AddParameter("@vi_dni", DbType.String, ParameterDirection.Input, DNI);
                    dbTerminal.AddParameter("@CodigoCliente", DbType.String, ParameterDirection.Input, IdCliente);
                    dbTerminal.AddParameter("@Usuario", DbType.String, ParameterDirection.Input, Usuario);
                    dbTerminal.AddParameter("@Codigo", DbType.String, ParameterDirection.Input, Codigo);
                    dbTerminal.AddParameter("@vo_Resultado", DbType.String, ParameterDirection.Output, String.Empty, 300);

                    dbTerminal.Execute();
                    String Retorno = dbTerminal.GetParameter("@vo_Resultado").ToString();
                    return Retorno;

                }
            }
            catch (Exception e)
            {
                return "-999|Ocurrio un error en la capa de datos";
            }
        }

        public String EliminarDespachador(String Codigo)
        {
            try
            {
                using (Database dbTerminal = new Database())
                {
                    dbTerminal.ProcedureName = "[DOC_Del_DespachadorExtranet]";
                    dbTerminal.AddParameter("@Codigo", DbType.String, ParameterDirection.Input, Codigo);
                    dbTerminal.AddParameter("@vo_Resultado", DbType.String, ParameterDirection.Output, String.Empty, 300);

                    dbTerminal.Execute();
                    String Retorno = dbTerminal.GetParameter("@vo_Resultado").ToString();
                    return Retorno;

                }
            }
            catch (Exception e)
            {
                return "-999|Ocurrio un error en la capa de datos";
            }
        }

        public String ValidarCodigo(String Codigo)
        {
            try
            {
                using (Database dbTerminal = new Database())
                {
                    dbTerminal.ProcedureName = "[SP_ValidarCodigoCliente_Extranet]";
                    dbTerminal.AddParameter("@Codigo", DbType.String, ParameterDirection.Input, Codigo);
                    dbTerminal.AddParameter("@vo_Resultado", DbType.String, ParameterDirection.Output, String.Empty, 300);

                    dbTerminal.Execute();
                    String Retorno = dbTerminal.GetParameter("@vo_Resultado").ToString();
                    return Retorno;

                }
            }
            catch (Exception e)
            {
                return "-999|Ocurrio un error en la capa de datos";
            }
        }

        public String ValidarDNI(String DNI)
        {
            try
            {
                using (Database dbTerminal = new Database())
                {
                    dbTerminal.ProcedureName = "[SP_ValidarDNIHuella_Extranet]";
                    dbTerminal.AddParameter("@DNI", DbType.String, ParameterDirection.Input, DNI);
                    dbTerminal.AddParameter("@vo_Resultado", DbType.String, ParameterDirection.Output, String.Empty, 300);

                    dbTerminal.Execute();
                    String Retorno = dbTerminal.GetParameter("@vo_Resultado").ToString();
                    return Retorno;

                }
            }
            catch (Exception e)
            {
                return "-999|Ocurrio un error en la capa de datos";
            }
        }
        public String ValidarPlaca(String Placa)
        {
            try
            {
                using (Database dbTerminal = new Database())
                {
                    dbTerminal.ProcedureName = "[SP_ValidarPlaca_Extranet]";
                    dbTerminal.AddParameter("@Placa", DbType.String, ParameterDirection.Input, Placa);
                    dbTerminal.AddParameter("@vo_Resultado", DbType.String, ParameterDirection.Output, String.Empty, 300);

                    dbTerminal.Execute();
                    String Retorno = dbTerminal.GetParameter("@vo_Resultado").ToString();
                    return Retorno;

                }
            }
            catch (Exception e)
            {
                return "-999|Ocurrio un error en la capa de datos";
            }
        }

        public String ValidarPermisos(String Codigo)
        {
            try
            {
                using (Database dbTerminal = new Database())
                {
                    dbTerminal.ProcedureName = "[SP_ValidarPermisosAgencia]";
                    dbTerminal.AddParameter("@Codigo", DbType.String, ParameterDirection.Input, Codigo);
                    dbTerminal.AddParameter("@vo_Resultado", DbType.String, ParameterDirection.Output, String.Empty, 300);

                    dbTerminal.Execute();
                    String Retorno = dbTerminal.GetParameter("@vo_Resultado").ToString();
                    return Retorno;

                }
            }
            catch (Exception e)
            {
                return "-999|Ocurrio un error en la capa de datos";
            }
        }

        public BE_AutorizacionRetiro ListarAutRetAgre(String AutRet)
        {
            IDataReader reader = null;
            try
            {
                using (Database dbTerminal = new Database())
                {
                    dbTerminal.ProcedureName = "[Operacion_Lis_AutRetAgregar]";
                    dbTerminal.AddParameter("@vi_AutRet", DbType.String, ParameterDirection.Input, AutRet);

                    reader = dbTerminal.GetDataReader();

                    BE_AutorizacionRetiro Lis_BE_AutorizacionRetiroDetPorID = new BE_AutorizacionRetiro();
                    while (reader.Read())
                    {
                        Lis_BE_AutorizacionRetiroDetPorID = Pu_DocumentoOrigen.ListarAutRetAgre(reader);
                    }
                    reader.Close();
                    return Lis_BE_AutorizacionRetiroDetPorID;
                }
            }
            catch (Exception e)
            {
                ResultadoTransaccionTx oResultadoTransaccionTx = new ResultadoTransaccionTx(ResultadoTransaccionTx.TipoResultado.Error, e);
                oResultadoTransaccionTx.RegistrarError(e);
                return null;
            }
        }

        public BE_AutorizacionRetiroList ListarAutRetiro2(String NumAut, String Volante, String BL, String Cliente, String Contenedor, String Agencia)
        {
            IDataReader reader = null;
            try
            {
                using (Database dbTerminal = new Database())
                {
                    dbTerminal.ProcedureName = "[Documentacion_Lis_AutorizacionRetiroExtranet2]";
                    dbTerminal.AddParameter("@vi_NroAutRet", DbType.String, ParameterDirection.Input, NumAut);
                    dbTerminal.AddParameter("@vi_Volante", DbType.String, ParameterDirection.Input, Volante);
                    dbTerminal.AddParameter("@vi_BL", DbType.String, ParameterDirection.Input, BL);
                    dbTerminal.AddParameter("@vi_Cliente", DbType.String, ParameterDirection.Input, Cliente);
                    dbTerminal.AddParameter("@vi_Contenedor", DbType.String, ParameterDirection.Input, Contenedor);
                    dbTerminal.AddParameter("@vi_Agencia", DbType.String, ParameterDirection.Input, Agencia);

                    reader = dbTerminal.GetDataReader();

                    BE_AutorizacionRetiroList ListAutRet = new BE_AutorizacionRetiroList();
                    while (reader.Read())
                    {
                        ListAutRet.Add(Pu_DocumentoOrigen.ListarAutRetiro2(reader));
                    }
                    reader.Close();
                    return ListAutRet;
                }
            }
            catch (Exception e)
            {
                ResultadoTransaccionTx oResultadoTransaccionTx = new ResultadoTransaccionTx(ResultadoTransaccionTx.TipoResultado.Error, e);
                oResultadoTransaccionTx.RegistrarError(e);
                return null;
            }
        }

        public BE_AutorizacionRetiroList ListarAutDespachadores(int IdAutRet)
        {
            IDataReader reader = null;

            try
            {
                using (Database dbTerminal = new Database())
                {
                    dbTerminal.ProcedureName = "[DOC_Lis_AutRetiroDespachadorExtranet]";
                    dbTerminal.AddParameter("@vi_IdAutRet", DbType.String, ParameterDirection.Input, IdAutRet);
                    reader = dbTerminal.GetDataReader();

                    BE_AutorizacionRetiroList Lst_Dua = new BE_AutorizacionRetiroList();
                    while (reader.Read())
                    {
                        Lst_Dua.Add(Pu_DocumentoOrigen.ListarAutDespachadores(reader));

                    }
                    reader.Close();

                    return Lst_Dua;
                }

            }
            catch (Exception e)
            {
                ResultadoTransaccionTx oResultadoTransaccionTx = new ResultadoTransaccionTx(ResultadoTransaccionTx.TipoResultado.Error, e);
                oResultadoTransaccionTx.RegistrarError(e);
                return null;
            }
        }

        public String InsertarDespachadorAut(BE_AutorizacionRetiro objEntidad)
        {

            try
            {
                using (Database dbTerminal = new Database())
                {
                    dbTerminal.ProcedureName = "[DOC_Ins_AutRetiroDespachadorExtranet]";
                    dbTerminal.AddParameter("@IIdDespachador", DbType.Int32, ParameterDirection.Input, objEntidad.IIdDespachador);
                    dbTerminal.AddParameter("@IIdAutRet", DbType.Int32, ParameterDirection.Input, objEntidad.IIdAutRet);
                    dbTerminal.AddParameter("@SDespachador", DbType.String, ParameterDirection.Input, objEntidad.SDespachador);
                    dbTerminal.AddParameter("@SDniDespachador", DbType.String, ParameterDirection.Input, objEntidad.SDniDespachador);
                    dbTerminal.AddParameter("@Usuario", DbType.String, ParameterDirection.Input, objEntidad.SUsuario);
                    dbTerminal.AddParameter("@NombrePc", DbType.String, ParameterDirection.Input, objEntidad.SPC);
                    dbTerminal.AddParameter("@vo_Resultado", DbType.String, ParameterDirection.Output, String.Empty, 300);

                    dbTerminal.Execute();
                    String Retorno = dbTerminal.GetParameter("@vo_Resultado").ToString();
                    return Retorno;

                }
            }
            catch (Exception e)
            {
                return "-999|Ocurrio un error en la capa de datos";
            }

        }

        public ResultadoTransaccionTx EliminarDespachador2(Int32 IdRegistro, String Usuario, String NombrePc)
        {

            try
            {
                using (Database dbTerminal = new Database())
                {
                    dbTerminal.ProcedureName = "[DOC_Del_DespachadorAutExtranet]";
                    dbTerminal.AddParameter("@IdRegistro", DbType.Int32, ParameterDirection.Input, IdRegistro);
                    dbTerminal.AddParameter("@Usuario", DbType.String, ParameterDirection.Input, Usuario);
                    dbTerminal.AddParameter("@NombrePc", DbType.String, ParameterDirection.Input, NombrePc);
                    dbTerminal.Execute();
                    return new ResultadoTransaccionTx(ResultadoTransaccionTx.TipoResultado.Exito, null);
                }
            }
            catch (Exception e)
            {
                return new ResultadoTransaccionTx(ResultadoTransaccionTx.TipoResultado.Error, e);
            }
        }

        public String InsContSelAutRet(int IdAutRet, DateTime Vigencia, int dia, String Presencia, String SelCont, int IdAutRetDet,
            String Usuario, String PC)
        {

            try
            {
                using (Database dbTerminal = new Database())
                {
                    dbTerminal.ProcedureName = "[DOC_Ins_ContSel_AutRetiro_Extranet]";
                    dbTerminal.AddParameter("@vi_IdAutRet", DbType.String, ParameterDirection.Input, IdAutRet);
                    dbTerminal.AddParameter("@vi_FechaVigencia", DbType.DateTime, ParameterDirection.Input, Vigencia);
                    dbTerminal.AddParameter("@vi_dias", DbType.Int32, ParameterDirection.Input, dia);
                    dbTerminal.AddParameter("@vi_presencia", DbType.String, ParameterDirection.Input, Presencia);
                    //dbTerminal.AddParameter("@vi_IdDespachador", DbType.Int32, ParameterDirection.Input, IdDespachador);
                    dbTerminal.AddParameter("@vi_cont", DbType.String, ParameterDirection.Input, SelCont);
                    dbTerminal.AddParameter("@vi_IdAutRetDet", DbType.Int32, ParameterDirection.Input, IdAutRetDet);
                    dbTerminal.AddParameter("@Usuario", DbType.String, ParameterDirection.Input, Usuario);
                    dbTerminal.AddParameter("@NombrePc", DbType.String, ParameterDirection.Input, PC);
                    dbTerminal.AddParameter("@vo_Resultado", DbType.String, ParameterDirection.Output, String.Empty, 300);

                    dbTerminal.Execute();
                    String Retorno = dbTerminal.GetParameter("@vo_Resultado").ToString();
                    return Retorno;

                }
            }
            catch (Exception e)
            {
                return "-999|Ocurrio un error en la capa de datos";
            }
        }

        public BE_PlacaChoferList ListarPlacaChoferApp(int IdAutRet,int accion)
        {
            IDataReader reader = null;

            try
            {
                using (Database dbTerminal = new Database())
                {
                    dbTerminal.ProcedureName = "[DOC_Lis_AutRetiroPlacaChoferExtranet]";
                    dbTerminal.AddParameter("@vi_IdAutRet", DbType.String, ParameterDirection.Input, IdAutRet);
                    dbTerminal.AddParameter("@vi_accion", DbType.Int32, ParameterDirection.Input, accion);
                    reader = dbTerminal.GetDataReader();

                    BE_PlacaChoferList Lst_Dua = new BE_PlacaChoferList();
                    while (reader.Read())
                    {
                        Lst_Dua.Add(Pu_DocumentoOrigen.ListarPlacaChoferApp(reader));

                    }
                    reader.Close();

                    return Lst_Dua;
                }

            }
            catch (Exception e)
            {
                ResultadoTransaccionTx oResultadoTransaccionTx = new ResultadoTransaccionTx(ResultadoTransaccionTx.TipoResultado.Error, e);
                oResultadoTransaccionTx.RegistrarError(e);
                return null;
            }
        }

        #endregion

        #region MsgContainers


        public void SendContainers(List<String> lstContainers, Int32 AutRetiro)
        {
            try
            {
                String connstring = System.Configuration.ConfigurationManager.ConnectionStrings["FFAPRD_EXTRANET"].ConnectionString;

                using (var con = new SqlConnection(connstring))
                {
                    con.Open();
                    using (SqlCommand cmd = new SqlCommand("exec OPE_HabilitarAutContainersExtranet @list,@AutRetiro", con))
                    {

                        using (var table = new DataTable())
                        {
                            table.Columns.Add("Container", typeof(string));

                            foreach (var container in lstContainers)
                            {
                                table.Rows.Add(container);
                            }
                            var pList = new SqlParameter("@list", SqlDbType.Structured);
                            pList.TypeName = "dbo.ContainerList";
                            pList.Value = table;
                            cmd.Parameters.Add(pList);
                            cmd.Parameters.Add("@AutRetiro", SqlDbType.Int).Value = AutRetiro;
                            cmd.ExecuteNonQuery();
                        }
                    }
                }
            }
            catch (Exception e)
            {
                ResultadoTransaccionTx oResultadoTransaccionTx = new ResultadoTransaccionTx(ResultadoTransaccionTx.TipoResultado.Error, e);
                oResultadoTransaccionTx.RegistrarError(e);
            }

        }
        public void UpdateSentContainers(List<String> lstContainers, Int32 AutRetiro)
        {
            try
            {
                String connstring = System.Configuration.ConfigurationManager.ConnectionStrings["FFAPRD_EXTRANET"].ConnectionString;

                using (var con = new SqlConnection(connstring))
                {
                    con.Open();
                    using (SqlCommand cmd = new SqlCommand("exec OPE_ActualizarAutContainersExtranet @list,@AutRetiro", con))
                    {

                        using (var table = new DataTable())
                        {
                            table.Columns.Add("Container", typeof(string));

                            foreach (var container in lstContainers)
                            {
                                table.Rows.Add(container);
                            }
                            var pList = new SqlParameter("@list", SqlDbType.Structured);
                            pList.TypeName = "dbo.ContainerList";
                            pList.Value = table;
                            cmd.Parameters.Add(pList);
                            cmd.Parameters.Add("@AutRetiro", SqlDbType.Int).Value = AutRetiro;
                            cmd.ExecuteNonQuery();
                        }
                    }
                }
            }
            catch (Exception e)
            {
                ResultadoTransaccionTx oResultadoTransaccionTx = new ResultadoTransaccionTx(ResultadoTransaccionTx.TipoResultado.Error, e);
                oResultadoTransaccionTx.RegistrarError(e);
            }

        }

        public void InsLstContSelAutRet(List<BE_Contenedor> lstContainers, Int32 AutRetiro)
        {
            try
            {
                String connstring = System.Configuration.ConfigurationManager.ConnectionStrings["FFAPRD_EXTRANET"].ConnectionString;

                using (var con = new SqlConnection(connstring))
                {
                    con.Open();
                    using (SqlCommand cmd = new SqlCommand("exec DOC_InsList_ContSel_AutRetiro_Extranet @ROContainerList, @AutRetiroId", con))
                    {

                        using (var table = new DataTable())
                        {
                            table.Columns.Add("AutRetId", typeof(Int32));
                            table.Columns.Add("FechaVigencia", typeof(DateTime));
                            table.Columns.Add("dias", typeof(Int32));
                            table.Columns.Add("presencia", typeof(String));
                            table.Columns.Add("Container", typeof(Int32));
                            table.Columns.Add("AutRetDetId", typeof(Int32));
                            table.Columns.Add("userName", typeof(String));
                            table.Columns.Add("nombrePC", typeof(String));

                            foreach (var container in lstContainers)
                            {
                                table.Rows.Add(container.IdAutRet, container.Vigencia, container.Dia, container.sPresencia, container.selCont,
                                    container.IdAutRetDet, container.sUsuario, container.sNombrePC);
                            }
                            var pList = new SqlParameter("@ROContainerList", SqlDbType.Structured);
                            pList.TypeName = "dbo.ContainerAutExtranet";
                            pList.Value = table;
                            cmd.Parameters.Add(pList);
                            cmd.Parameters.Add("@AutRetiroId", SqlDbType.Int).Value = AutRetiro;
                            cmd.ExecuteNonQuery();
                        }
                    }
                }
            }
            catch (Exception e)
            {
                ResultadoTransaccionTx oResultadoTransaccionTx = new ResultadoTransaccionTx(ResultadoTransaccionTx.TipoResultado.Error, e);
                oResultadoTransaccionTx.RegistrarError(e);
            }

        }


        #endregion


        #region AR
        public List<BE_DocumentoOrigen> ListarDO_AR(BE_DocumentoOrigen oBE_DocOrigen, int accion)
        {
            IDataReader reader = null;
        
            try
            {

                using (Database dbTerminal = new Database())
                {

                    dbTerminal.ProcedureName = "[Ext_SS_SolicitudAutorizacion]";
                    dbTerminal.AddParameter("@vi_NumeroDo", DbType.String, ParameterDirection.Input, oBE_DocOrigen.sNumeroDO);
                    dbTerminal.AddParameter("@vi_IdDocOri", DbType.Int32, ParameterDirection.Input, oBE_DocOrigen.iIdDocOri);
                    dbTerminal.AddParameter("@vi_Dni", DbType.String, ParameterDirection.Input, oBE_DocOrigen.sDni);
                    dbTerminal.AddParameter("@vi_AgenciaAduana", DbType.String, ParameterDirection.Input, oBE_DocOrigen.sRucAgencia);
                    dbTerminal.AddParameter("@vi_RucFacturar", DbType.String, ParameterDirection.Input, oBE_DocOrigen.sRuc);
                    dbTerminal.AddParameter("@vi_idAgenciaAduana", DbType.Int32, ParameterDirection.Input, oBE_DocOrigen.iIdAgenciaAduana);
                    dbTerminal.AddParameter("@vi_idSolicitudAre", DbType.Int32, ParameterDirection.Input, oBE_DocOrigen.IdSolicitudAre);
                    dbTerminal.AddParameter("@vi_accion", DbType.Int32, ParameterDirection.Input, accion);
                    dbTerminal.AddParameter("@vo_Resultado", DbType.String, ParameterDirection.Output, String.Empty, 300);

                    reader = dbTerminal.GetDataReader();
                    

                    List<BE_DocumentoOrigen> oBE_List = new List<BE_DocumentoOrigen>();
                    String Retorno = String.Empty;
                    if (accion == 1)
                    {
                        try { Retorno = dbTerminal.GetParameter("@vo_Resultado").ToString(); } catch (Exception) { }


                        while (reader.Read())
                        {
                            oBE_List.Add(Pu_DocumentoOrigen.ListarDO_AR(reader));
                        }
                        reader.Close();

                        oBE_DocOrigen.sRetorno = Retorno;
                    }
                    else if (accion == 2)
                    {
                        while (reader.Read())
                        {
                            oBE_List.Add(Pu_DocumentoOrigen.ListarARE_DocOri_Detalle(reader));
                        }
                        reader.Close();
                    }
                    else if (accion == 3)
                    {
                        while (reader.Read())
                        {
                            oBE_List.Add(Pu_DocumentoOrigen.ListarARE_Despachador(reader));
                        }
                        reader.Close();
                    }
                    else if (accion == 4)
                    {
                        while (reader.Read())
                        {
                            oBE_List.Add(Pu_DocumentoOrigen.ListarARE_Ruc(reader));
                        }
                        reader.Close();
                    }
                    else if (accion == 5)
                    {
                        while (reader.Read())
                        {
                            oBE_List.Add(Pu_DocumentoOrigen.ListarARE_Liquidacion(reader));
                        }
                        reader.Close();
                    }
                    return oBE_List;
                }

            }
            catch (Exception e)
            {
                return null;
            }
        }


        #endregion
    }
}
