﻿using System;
using System.Collections.Generic;
using System.Text;
using FENIX.BusinessEntity;
using FENIX.Common;
using System.Data.Common;
using System.Data;

namespace FENIX.DataAccess
{
    public class DA_Presupuesto
    {
        public List<BE_Presupuesto> ListarPresupuesto(string strPeriodo, string strTipo)
        {
            IDataReader reader = null;

            try
            {
                using (Database dbTerminal = new Database())
                {
                    dbTerminal.ProcedureName = "[Extranet_Lis_Presupuesto]";
                    dbTerminal.AddParameter("@vi_Periodo", DbType.String, ParameterDirection.Input, strPeriodo );
                    dbTerminal.AddParameter("@vi_Tipo", DbType.String, ParameterDirection.Input, strTipo);


                    reader = dbTerminal.GetDataReader();

                    List<BE_Presupuesto> Lst_Presupuesto = new List<BE_Presupuesto>();
                    while (reader.Read())
                    {
                        Lst_Presupuesto.Add(Pu_Presupuesto.ListarPresupuesto(reader));

                    }
                    reader.Close();

                    return Lst_Presupuesto;
                }

            }
            catch (Exception e)
            {
                return null;
            }
        }


        public List<BE_Presupuesto> ListarVendedor()
        {
            IDataReader reader = null;

            try
            {
                using (Database dbTerminal = new Database())
                {
                    dbTerminal.ProcedureName = "[Comercial_T_Lis_Vendedor]";
                    reader = dbTerminal.GetDataReader();

                    List<BE_Presupuesto> Lst_Tabla = new List<BE_Presupuesto>();
                    while (reader.Read())
                    {
                        Lst_Tabla.Add(Pu_Presupuesto.ListarVendedor(reader));

                    }
                    reader.Close();

                    return Lst_Tabla;
                }
            }
            catch (Exception e)
            {
                ResultadoTransaccionTx oResultadoTransaccionTx = null;
                oResultadoTransaccionTx.RegistrarError(e);
                return null;
            }
        }


        public List<BE_Presupuesto> ListarCliente()
        {
            IDataReader reader = null;

            try
            {
                using (Database dbTerminal = new Database())
                {
                    dbTerminal.ProcedureName = "[COM_Lis_Clientes]";
                    dbTerminal.AddParameter("@vi_IdRol", DbType.Int32, ParameterDirection.Input, 100);
                    //dbTerminal.AddParameter("@Client_vch_NumDocumento", DbType.String, ParameterDirection.Input, "");
                    //dbTerminal.AddParameter("@Client_vch_Razon_Social", DbType.String, ParameterDirection.Input, "");
                    //dbTerminal.AddParameter("@Cliente_chr_Situacion", DbType.String, ParameterDirection.Input, "");

                    reader = dbTerminal.GetDataReader();

                    List<BE_Presupuesto> Lst_Tabla = new List<BE_Presupuesto>();
                    while (reader.Read())
                    {
                        Lst_Tabla.Add(Pu_Presupuesto.ListarCliente(reader));

                    }
                    reader.Close();

                    return Lst_Tabla;
                }

            }
            catch (Exception e)
            {
                return null;
            }
        }

        public List<BE_Presupuesto> ListarVtasXTipoCli(BE_Presupuesto oBE_Presupuesto)
        {
            IDataReader reader = null;

            try
            {
                using (Database dbTerminal = new Database())
                {
                    dbTerminal.ProcedureName = "[Extranet_LisVtasXTipoCliente]";
                    dbTerminal.AddParameter("@vi_Periodo", DbType.Int32, ParameterDirection.Input, Convert.ToInt32(oBE_Presupuesto.sPeriodo));
                    dbTerminal.AddParameter("@vi_MesIni", DbType.Int32, ParameterDirection.Input, oBE_Presupuesto.iMesIni);
                    dbTerminal.AddParameter("@vi_MesFin", DbType.Int32, ParameterDirection.Input, oBE_Presupuesto.iMesFin);

                    reader = dbTerminal.GetDataReader();

                    List<BE_Presupuesto> Lst_Ventas = new List<BE_Presupuesto>();
                    while (reader.Read())
                    {
                        Lst_Ventas.Add(Pu_Presupuesto.ListarVtasXTipoCli(reader));

                    }
                    reader.Close();

                    return Lst_Ventas;
                }

            }
            catch (Exception e)
            {
                return null;
            }
        }


        public List<BE_Presupuesto> ListarVtasXTipoCarga(BE_Presupuesto oBE_Presupuesto)
        {
            IDataReader reader = null;

            try
            {
                using (Database dbTerminal = new Database())
                {
                    dbTerminal.ProcedureName = "[Extranet_LisCNT_TNXConsign]";
                    dbTerminal.AddParameter("@vi_Periodo", DbType.Int32, ParameterDirection.Input, Convert.ToInt32(oBE_Presupuesto.sPeriodo));
                    dbTerminal.AddParameter("@vi_MesIni", DbType.Int32, ParameterDirection.Input, oBE_Presupuesto.iMesIni);
                    dbTerminal.AddParameter("@vi_MesFin", DbType.Int32, ParameterDirection.Input, oBE_Presupuesto.iMesFin);
                    dbTerminal.AddParameter("@vi_TipoOpe", DbType.Int32, ParameterDirection.Input, oBE_Presupuesto.iOperacion);
                    dbTerminal.AddParameter("@vi_LinNegocio", DbType.Int32, ParameterDirection.Input, oBE_Presupuesto.iIdLineaNegocio);
                    dbTerminal.AddParameter("@vi_NroLimite", DbType.Int32, ParameterDirection.Input, oBE_Presupuesto.iLimite);

                    reader = dbTerminal.GetDataReader();

                    List<BE_Presupuesto> Lst_Ventas = new List<BE_Presupuesto>();
                    while (reader.Read())
                    {
                        Lst_Ventas.Add(Pu_Presupuesto.ListarVtasXTipoCarga(reader));

                    }
                    reader.Close();

                    return Lst_Ventas;
                }

            }
            catch (Exception e)
            {
                return null;
            }
        }


        public List<BE_Presupuesto> ListarVtasXPeriodo(BE_Presupuesto oBE_Presupuesto)
        {
            IDataReader reader = null;

            try
            {
                using (Database dbTerminal = new Database())
                {
                    dbTerminal.ProcedureName = "[Extranet_LisVtasXPeriodo]";
                    dbTerminal.AddParameter("@vi_Periodo", DbType.Int32, ParameterDirection.Input, Convert.ToInt32(oBE_Presupuesto.sPeriodo));
                    dbTerminal.AddParameter("@vi_MesIni", DbType.Int32, ParameterDirection.Input, oBE_Presupuesto.iMesIni);
                    dbTerminal.AddParameter("@vi_MesFin", DbType.Int32, ParameterDirection.Input, oBE_Presupuesto.iMesFin);

                    reader = dbTerminal.GetDataReader();

                    List<BE_Presupuesto> Lst_Ventas = new List<BE_Presupuesto>();
                    while (reader.Read())
                    {
                        Lst_Ventas.Add(Pu_Presupuesto.ListarVtasXPeriodo(reader));

                    }
                    reader.Close();

                    return Lst_Ventas;
                }

            }
            catch (Exception e)
            {
                return null;
            }
        }

        public List<BE_Presupuesto> ListarVtasXLinea(BE_Presupuesto oBE_Presupuesto)
        {
            IDataReader reader = null;

            try
            {
                using (Database dbTerminal = new Database())
                {
                    dbTerminal.ProcedureName = "[Extranet_LisVtasXLineaNeg]";
                    dbTerminal.AddParameter("@vi_Periodo", DbType.Int32, ParameterDirection.Input, Convert.ToInt32(oBE_Presupuesto.sPeriodo));
                    dbTerminal.AddParameter("@vi_MesIni", DbType.Int32, ParameterDirection.Input, oBE_Presupuesto.iMesIni);
                    dbTerminal.AddParameter("@vi_MesFin", DbType.Int32, ParameterDirection.Input, oBE_Presupuesto.iMesFin);
                    dbTerminal.AddParameter("@vi_Cliente", DbType.Int32, ParameterDirection.Input, oBE_Presupuesto.iIdCliente);
                    dbTerminal.AddParameter("@vi_Vendedor", DbType.Int32, ParameterDirection.Input, oBE_Presupuesto.iIdVendedor);

                    reader = dbTerminal.GetDataReader();

                    List<BE_Presupuesto> Lst_Ventas = new List<BE_Presupuesto>();
                    while (reader.Read())
                    {
                        Lst_Ventas.Add(Pu_Presupuesto.ListarVtasXLinea(reader));

                    }
                    reader.Close();

                    return Lst_Ventas;
                }

            }
            catch (Exception e)
            {
                return null;
            }
        }


        public List<BE_Presupuesto> ListarVtasXCliente(BE_Presupuesto oBE_Presupuesto)
        {
            IDataReader reader = null;

            try
            {
                using (Database dbTerminal = new Database())
                {
                    dbTerminal.ProcedureName = "[Extranet_LisVtasRankin]";
                    dbTerminal.AddParameter("@vi_Periodo", DbType.Int32, ParameterDirection.Input, Convert.ToInt32(oBE_Presupuesto.sPeriodo));
                    dbTerminal.AddParameter("@vi_MesIni", DbType.Int32, ParameterDirection.Input, oBE_Presupuesto.iMesIni);
                    dbTerminal.AddParameter("@vi_MesFin", DbType.Int32, ParameterDirection.Input, oBE_Presupuesto.iMesFin);
                    dbTerminal.AddParameter("@vi_TipoOpe", DbType.Int32, ParameterDirection.Input, oBE_Presupuesto.iOperacion);
                    dbTerminal.AddParameter("@vi_LinNegocio", DbType.Int32, ParameterDirection.Input, oBE_Presupuesto.iIdLineaNegocio);
                    dbTerminal.AddParameter("@vi_NroLimite", DbType.Int32, ParameterDirection.Input, oBE_Presupuesto.iLimite);

                    reader = dbTerminal.GetDataReader();

                    List<BE_Presupuesto> Lst_Ventas = new List<BE_Presupuesto>();
                    while (reader.Read())
                    {
                        Lst_Ventas.Add(Pu_Presupuesto.ListarVtasXCliente(reader));

                    }
                    reader.Close();

                    return Lst_Ventas;
                }

            }
            catch (Exception e)
            {
                return null;
            }
        }

        public List<BE_Presupuesto> ListarVtasXVendedor(BE_Presupuesto oBE_Presupuesto)
        {
            IDataReader reader = null;

            try
            {
                using (Database dbTerminal = new Database())
                {
                    dbTerminal.ProcedureName = "[Extranet_LisVtasXVendedor]";
                    dbTerminal.AddParameter("@vi_Periodo", DbType.Int32, ParameterDirection.Input, Convert.ToInt32(oBE_Presupuesto.sPeriodo));
                    dbTerminal.AddParameter("@vi_MesIni", DbType.Int32, ParameterDirection.Input, oBE_Presupuesto.iMesIni);
                    dbTerminal.AddParameter("@vi_MesFin", DbType.Int32, ParameterDirection.Input, oBE_Presupuesto.iMesFin);
                   // dbTerminal.AddParameter("@vi_LinNegocio", DbType.Int32, ParameterDirection.Input, oBE_Presupuesto.iIdLineaNegocio);
                   // dbTerminal.AddParameter("@vi_Vendedor", DbType.Int32, ParameterDirection.Input, oBE_Presupuesto.iIdVendedor);

                    reader = dbTerminal.GetDataReader();

                    List<BE_Presupuesto> Lst_Ventas = new List<BE_Presupuesto>();
                    while (reader.Read())
                    {
                        Lst_Ventas.Add(Pu_Presupuesto.ListarVtasXVendedor(reader));

                    }
                    reader.Close();

                    return Lst_Ventas;
                }

            }
            catch (Exception e)
            {
                return null;
            }
        }


        public List<BE_Presupuesto> ListarVtasXServicio(BE_Presupuesto oBE_Presupuesto)
        {
            IDataReader reader = null;

            try
            {
                using (Database dbTerminal = new Database())
                {
                    dbTerminal.ProcedureName = "[Extranet_LisVtasXServicio]";
                    dbTerminal.AddParameter("@vi_Periodo", DbType.Int32, ParameterDirection.Input, Convert.ToInt32(oBE_Presupuesto.sPeriodo));
                    dbTerminal.AddParameter("@vi_MesIni", DbType.Int32, ParameterDirection.Input, oBE_Presupuesto.iMesIni);
                    dbTerminal.AddParameter("@vi_MesFin", DbType.Int32, ParameterDirection.Input, oBE_Presupuesto.iMesFin);                    
                    dbTerminal.AddParameter("@vi_LinNegocio", DbType.Int32, ParameterDirection.Input, oBE_Presupuesto.iIdLineaNegocio);
                    dbTerminal.AddParameter("@vi_Vendedor", DbType.Int32, ParameterDirection.Input, oBE_Presupuesto.iIdVendedor);
                    dbTerminal.AddParameter("@vi_Cliente", DbType.Int32, ParameterDirection.Input, oBE_Presupuesto.iIdCliente);
                    dbTerminal.AddParameter("@vi_NroLimite", DbType.Int32, ParameterDirection.Input, oBE_Presupuesto.iLimite);

                    reader = dbTerminal.GetDataReader();

                    List<BE_Presupuesto> Lst_Ventas = new List<BE_Presupuesto>();
                    while (reader.Read())
                    {
                        Lst_Ventas.Add(Pu_Presupuesto.ListarVtasXServicio(reader));

                    }
                    reader.Close();

                    return Lst_Ventas;
                }

            }
            catch (Exception e)
            {
                return null;
            }
        }


        public String InsertarPresupuesto (BE_Presupuesto oBE_Presupuesto)
        {
            using (Database dbTerminal = new Database())
            {

                try
                {
                    dbTerminal.ProcedureName = "Extranet_Ins_Presupuesto";
                    dbTerminal.AddParameter("@vi_IdPresupuesto", DbType.Int32, ParameterDirection.Input, oBE_Presupuesto.iIdPresupuesto);
                    dbTerminal.AddParameter("@vi_sPeriodo", DbType.String, ParameterDirection.Input, oBE_Presupuesto.sPeriodo);
                    dbTerminal.AddParameter("@vi_IdGrupoNegocio", DbType.Int32, ParameterDirection.Input, oBE_Presupuesto.iIdLineaNegocio);
                    dbTerminal.AddParameter("@vi_IdVendedor", DbType.Int32, ParameterDirection.Input, oBE_Presupuesto.iIdVendedor);
                    dbTerminal.AddParameter("@vi_decMes01", DbType.Decimal, ParameterDirection.Input, oBE_Presupuesto.iMes1);
                    dbTerminal.AddParameter("@vi_decMes02", DbType.Decimal, ParameterDirection.Input, oBE_Presupuesto.iMes2);
                    dbTerminal.AddParameter("@vi_decMes03", DbType.Decimal, ParameterDirection.Input, oBE_Presupuesto.iMes3);
                    dbTerminal.AddParameter("@vi_decMes04", DbType.Decimal, ParameterDirection.Input, oBE_Presupuesto.iMes4);
                    dbTerminal.AddParameter("@vi_decMes05", DbType.Decimal, ParameterDirection.Input, oBE_Presupuesto.iMes5);
                    dbTerminal.AddParameter("@vi_decMes06", DbType.Decimal, ParameterDirection.Input, oBE_Presupuesto.iMes6);
                    dbTerminal.AddParameter("@vi_decMes07", DbType.Decimal, ParameterDirection.Input, oBE_Presupuesto.iMes7);
                    dbTerminal.AddParameter("@vi_decMes08", DbType.Decimal, ParameterDirection.Input, oBE_Presupuesto.iMes8);
                    dbTerminal.AddParameter("@vi_decMes09", DbType.Decimal, ParameterDirection.Input, oBE_Presupuesto.iMes9);
                    dbTerminal.AddParameter("@vi_decMes10", DbType.Decimal, ParameterDirection.Input, oBE_Presupuesto.iMes10);
                    dbTerminal.AddParameter("@vi_decMes11", DbType.Decimal, ParameterDirection.Input, oBE_Presupuesto.iMes11);
                    dbTerminal.AddParameter("@vi_decMes12", DbType.Decimal, ParameterDirection.Input, oBE_Presupuesto.iMes12);
                    dbTerminal.AddParameter("@Usuario", DbType.String, ParameterDirection.Input, oBE_Presupuesto.sUsuario);
                    dbTerminal.AddParameter("@NombrePc", DbType.String, ParameterDirection.Input, oBE_Presupuesto.sNombrePC);
                    dbTerminal.AddParameter("@vo_Resultado", DbType.String, ParameterDirection.Output, String.Empty, 100);

                    dbTerminal.ExecuteTransaction();

                    return dbTerminal.GetParameter("@vo_Resultado").ToString();
                   
                }
                catch (Exception e)
                {
                    ResultadoTransaccionTx oResultadoTransaccionTx = new ResultadoTransaccionTx(ResultadoTransaccionTx.TipoResultado.Error, e);
                    oResultadoTransaccionTx.RegistrarError(e);
                    return null;
                }
            }
        }


        public String EliminarPresupuesto(BE_Presupuesto oBE_Presupuesto)
        {
            using (Database dbTerminal = new Database())
            {
                try
                {
                    dbTerminal.ProcedureName = "Extranet_Del_Presupuesto";

                    dbTerminal.AddParameter("@vi_IdPresupuesto", DbType.Int32, ParameterDirection.Input, oBE_Presupuesto.iIdPresupuesto);
                    dbTerminal.AddParameter("@Usuario", DbType.String, ParameterDirection.Input, oBE_Presupuesto.sUsuario);
                    dbTerminal.AddParameter("@NombrePc", DbType.String, ParameterDirection.Input, oBE_Presupuesto.sNombrePC);
                    dbTerminal.AddParameter("@vo_Resultado", DbType.String, ParameterDirection.Output, String.Empty, 100);
                    dbTerminal.Execute();

                    return dbTerminal.GetParameter("@vo_Resultado").ToString();
                }
                catch (Exception e)
                {
                    return "-99|ocurrio un error al anular el Presupuesto";
                }
            }
        }
    }

}
