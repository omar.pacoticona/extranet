﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using FENIX.BusinessEntity;


namespace FENIX.DataAccess
{
    public class Pu_Presupuesto
    {
        public static BE_Presupuesto ListarPresupuesto(IDataReader DReader )
        {
            try
            {
                BE_Presupuesto oBE_Presupuesto = new BE_Presupuesto();
                int indice = 0;
                indice = DReader.GetOrdinal("IdPresupuesto");
                if (!DReader.IsDBNull(indice)) oBE_Presupuesto.iIdPresupuesto = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("IdGrupoNegocio");
                if (!DReader.IsDBNull(indice)) oBE_Presupuesto.iIdLineaNegocio = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("Negocio");
                if (!DReader.IsDBNull(indice)) oBE_Presupuesto.sNegocio = DReader.GetString(indice);
                indice = DReader.GetOrdinal("IdVendedor");
                if (!DReader.IsDBNull(indice)) oBE_Presupuesto.iIdVendedor = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("Vendedor");
                if (!DReader.IsDBNull(indice)) oBE_Presupuesto.sVendedor = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Mes01");
                if (!DReader.IsDBNull(indice)) oBE_Presupuesto.iMes1 = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("Mes02");
                if (!DReader.IsDBNull(indice)) oBE_Presupuesto.iMes2 = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("Mes03");
                if (!DReader.IsDBNull(indice)) oBE_Presupuesto.iMes3 = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("Mes04");
                if (!DReader.IsDBNull(indice)) oBE_Presupuesto.iMes4 = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("Mes05");
                if (!DReader.IsDBNull(indice)) oBE_Presupuesto.iMes5 = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("Mes06");
                if (!DReader.IsDBNull(indice)) oBE_Presupuesto.iMes6 = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("Mes07");
                if (!DReader.IsDBNull(indice)) oBE_Presupuesto.iMes7 = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("Mes08");
                if (!DReader.IsDBNull(indice)) oBE_Presupuesto.iMes8 = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("Mes09");
                if (!DReader.IsDBNull(indice)) oBE_Presupuesto.iMes9 = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("Mes10");
                if (!DReader.IsDBNull(indice)) oBE_Presupuesto.iMes10 = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("Mes11");
                if (!DReader.IsDBNull(indice)) oBE_Presupuesto.iMes11 = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("Mes12");
                if (!DReader.IsDBNull(indice)) oBE_Presupuesto.iMes12 = DReader.GetInt32(indice);

                return oBE_Presupuesto;
            }
            catch (Exception)
            {
                return null;
            }
        }
    

        public static BE_Presupuesto ListarVendedor(IDataReader DReader)
        {
            try
            {
                BE_Presupuesto oBE_Presupuesto = new BE_Presupuesto();
                int indice = 0;
                indice = DReader.GetOrdinal("Codigo");
                if (!DReader.IsDBNull(indice)) oBE_Presupuesto.iIdVendedor = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("Descripcion");
                if (!DReader.IsDBNull(indice)) oBE_Presupuesto.sVendedor = DReader.GetString(indice);

                return oBE_Presupuesto;
            }
            catch (Exception)
            {
                return null;
            }
        }


        public static BE_Presupuesto ListarCliente(IDataReader DReader)
        {
            try
            {
                BE_Presupuesto oBE_Presupuesto = new BE_Presupuesto();
                int indice = 0;
                indice = DReader.GetOrdinal("IdCliente");
                if (!DReader.IsDBNull(indice)) oBE_Presupuesto.iIdCliente = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("Cliente");
                if (!DReader.IsDBNull(indice)) oBE_Presupuesto.sCliente = DReader.GetString(indice);

                return oBE_Presupuesto;
            }
            catch (Exception)
            {
                return null;
            }
        }


        public static BE_Presupuesto ListarVtasXTipoCli(IDataReader DReader)
        {
            try
            {
                BE_Presupuesto oBE_Presupuesto = new BE_Presupuesto();
                int indice = 0;
                indice = DReader.GetOrdinal("Mes");
                if (!DReader.IsDBNull(indice)) oBE_Presupuesto.iMes = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("DescMes");
                if (!DReader.IsDBNull(indice)) oBE_Presupuesto.sMes = DReader.GetString(indice);
                indice = DReader.GetOrdinal("VtaGrupo");
                if (!DReader.IsDBNull(indice)) oBE_Presupuesto.dVentas = DReader.GetDecimal(indice);
                indice = DReader.GetOrdinal("PorcGrupo");
                if (!DReader.IsDBNull(indice)) oBE_Presupuesto.iMes1 = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("VtaTerceros");
                if (!DReader.IsDBNull(indice)) oBE_Presupuesto.dVentasAnt = DReader.GetDecimal(indice);
                indice = DReader.GetOrdinal("PorcTerceros");
                if (!DReader.IsDBNull(indice)) oBE_Presupuesto.iMes2 = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("PorcTotal");
                if (!DReader.IsDBNull(indice)) oBE_Presupuesto.iMes3 = DReader.GetInt32(indice);


                return oBE_Presupuesto;
            }
            catch (Exception)
            {
                return null;
            }
        }


        public static BE_Presupuesto ListarVtasXTipoCarga(IDataReader DReader)
        {
            try
            {
                BE_Presupuesto oBE_Presupuesto = new BE_Presupuesto();
                int indice = 0;
                indice = DReader.GetOrdinal("NroItem");
                if (!DReader.IsDBNull(indice)) oBE_Presupuesto.iItem = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("IdCliente");
                if (!DReader.IsDBNull(indice)) oBE_Presupuesto.iIdCliente = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("cliente");
                if (!DReader.IsDBNull(indice)) oBE_Presupuesto.sCliente = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Cantidad");
                if (!DReader.IsDBNull(indice)) oBE_Presupuesto.iMes1 = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("CantidadAnt");
                if (!DReader.IsDBNull(indice)) oBE_Presupuesto.iMes2 = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("Porc");
                if (!DReader.IsDBNull(indice)) oBE_Presupuesto.dPorc = DReader.GetDecimal(indice);


                return oBE_Presupuesto;
            }
            catch (Exception)
            {
                return null;
            }
        }


        public static BE_Presupuesto ListarVtasXPeriodo(IDataReader DReader)
        {
            try
            {
                BE_Presupuesto oBE_Presupuesto = new BE_Presupuesto();
                int indice = 0;
                indice = DReader.GetOrdinal("Mes");
                if (!DReader.IsDBNull(indice)) oBE_Presupuesto.iMes = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("DescMes");
                if (!DReader.IsDBNull(indice)) oBE_Presupuesto.sMes = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Ventas");
                if (!DReader.IsDBNull(indice)) oBE_Presupuesto.dVentas = DReader.GetDecimal(indice);
                indice = DReader.GetOrdinal("Presupuesto");
                if (!DReader.IsDBNull(indice)) oBE_Presupuesto.iPresupuesto = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("Porc");
                if (!DReader.IsDBNull(indice)) oBE_Presupuesto.dPorc = DReader.GetDecimal(indice);
                indice = DReader.GetOrdinal("VentasAnt");
                if (!DReader.IsDBNull(indice)) oBE_Presupuesto.dVentasAnt = DReader.GetDecimal(indice);
                indice = DReader.GetOrdinal("PorcAnt");
                if (!DReader.IsDBNull(indice)) oBE_Presupuesto.dPorcAnt = DReader.GetDecimal(indice);

                return oBE_Presupuesto;
            }
            catch (Exception)
            {
                return null;
            }
        }


        public static BE_Presupuesto ListarVtasXLinea(IDataReader DReader)
        {
            try
            {
                BE_Presupuesto oBE_Presupuesto = new BE_Presupuesto();
                int indice = 0;
                indice = DReader.GetOrdinal("IdLineaNegocio");
                if (!DReader.IsDBNull(indice)) oBE_Presupuesto.iIdLineaNegocio = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("Descripcion");
                if (!DReader.IsDBNull(indice)) oBE_Presupuesto.sNegocio = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Ventas");
                if (!DReader.IsDBNull(indice)) oBE_Presupuesto.dVentas = DReader.GetDecimal(indice);
                indice = DReader.GetOrdinal("Presupuesto");
                if (!DReader.IsDBNull(indice)) oBE_Presupuesto.iPresupuesto = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("Porc");
                if (!DReader.IsDBNull(indice)) oBE_Presupuesto.dPorc = DReader.GetDecimal(indice);

                return oBE_Presupuesto;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public static BE_Presupuesto ListarVtasXCliente(IDataReader DReader)
        {
            try
            {
                BE_Presupuesto oBE_Presupuesto = new BE_Presupuesto();
                int indice = 0;
                indice = DReader.GetOrdinal("NroItem");
                if (!DReader.IsDBNull(indice)) oBE_Presupuesto.iItem = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("IdCliente");
                if (!DReader.IsDBNull(indice)) oBE_Presupuesto.iIdCliente = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("cliente");
                if (!DReader.IsDBNull(indice)) oBE_Presupuesto.sCliente = DReader.GetString(indice);
                indice = DReader.GetOrdinal("IdVendedor");
                if (!DReader.IsDBNull(indice)) oBE_Presupuesto.iIdVendedor = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("Vendedor");
                if (!DReader.IsDBNull(indice)) oBE_Presupuesto.sVendedor = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Ventas");
                if (!DReader.IsDBNull(indice)) oBE_Presupuesto.dVentas = DReader.GetDecimal(indice);
                indice = DReader.GetOrdinal("VentasAnt");
                if (!DReader.IsDBNull(indice)) oBE_Presupuesto.dVentasAnt = DReader.GetDecimal(indice);
                indice = DReader.GetOrdinal("Porc");
                if (!DReader.IsDBNull(indice)) oBE_Presupuesto.dPorc = DReader.GetDecimal(indice);

                return oBE_Presupuesto;
            }
            catch (Exception)
            {
                return null;
            }
        }



        public static BE_Presupuesto ListarVtasXVendedor(IDataReader DReader)
        {
            try
            {
                BE_Presupuesto oBE_Presupuesto = new BE_Presupuesto();
                int indice = 0;
                indice = DReader.GetOrdinal("IdVendedor");
                if (!DReader.IsDBNull(indice)) oBE_Presupuesto.iIdVendedor = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("Vendedor");
                if (!DReader.IsDBNull(indice)) oBE_Presupuesto.sVendedor = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Ventas");
                if (!DReader.IsDBNull(indice)) oBE_Presupuesto.dVentas = DReader.GetDecimal(indice);
                indice = DReader.GetOrdinal("Presupuesto");
                if (!DReader.IsDBNull(indice)) oBE_Presupuesto.iPresupuesto = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("Porc");
                if (!DReader.IsDBNull(indice)) oBE_Presupuesto.dPorc = DReader.GetDecimal(indice);

                return oBE_Presupuesto;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public static BE_Presupuesto ListarVtasXServicio(IDataReader DReader)
        {
            try
            {
                BE_Presupuesto oBE_Presupuesto = new BE_Presupuesto();
                int indice = 0;
                indice = DReader.GetOrdinal("NroItem");
                if (!DReader.IsDBNull(indice)) oBE_Presupuesto.iItem = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("IdGrupo");
                if (!DReader.IsDBNull(indice)) oBE_Presupuesto.iIdGrupo = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("IdServicio");
                if (!DReader.IsDBNull(indice)) oBE_Presupuesto.iIdServicio = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("Servicio");
                if (!DReader.IsDBNull(indice)) oBE_Presupuesto.sServicio = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Ventas");
                if (!DReader.IsDBNull(indice)) oBE_Presupuesto.dVentas = DReader.GetDecimal(indice);
                indice = DReader.GetOrdinal("Porc");
                if (!DReader.IsDBNull(indice)) oBE_Presupuesto.dPorc = DReader.GetDecimal(indice);

                return oBE_Presupuesto;
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}
