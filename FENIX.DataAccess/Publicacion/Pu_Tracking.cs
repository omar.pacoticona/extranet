﻿using System;
using System.Collections.Generic;
using System.Text;
using FENIX.BusinessEntity;
using System.Data;
using FENIX.Common;


namespace FENIX.DataAccess
{
    public class Pu_Tracking
    {

        public static BE_Tracking Listar(IDataReader DReader)
        {
            try
            {
                BE_Tracking oBE_Tracking = new BE_Tracking();

                int indice = 0;

                //DIRECCIONAMIENTO
                indice = DReader.GetOrdinal("Manifiesto");
                if (!DReader.IsDBNull(indice)) oBE_Tracking.sManifiesto = DReader.GetString(indice);
                indice = DReader.GetOrdinal("TerminoDescarga");
                if (!DReader.IsDBNull(indice)) oBE_Tracking.sTerminoDescarga = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Nave");
                if (!DReader.IsDBNull(indice)) oBE_Tracking.sNave = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Viaje");
                if (!DReader.IsDBNull(indice)) oBE_Tracking.sViaje = DReader.GetString(indice);

                //INGRESO AL ALMACEN
                indice = DReader.GetOrdinal("TicketDescarga");
                if (!DReader.IsDBNull(indice)) oBE_Tracking.iTicketDescarga = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("IngresoDescarga");
                if (!DReader.IsDBNull(indice)) oBE_Tracking.sIngresoDescarga = DReader.GetString(indice);
                indice = DReader.GetOrdinal("SalidaDescarga");
                if (!DReader.IsDBNull(indice)) oBE_Tracking.sSalidaDescarga = DReader.GetString(indice);
                indice = DReader.GetOrdinal("PesoDescarga");
                if (!DReader.IsDBNull(indice)) oBE_Tracking.iPesoDescarga = DReader.GetInt32(indice);

                //EMISION DE VOLANTE
                indice = DReader.GetOrdinal("Volante");
                if (!DReader.IsDBNull(indice)) oBE_Tracking.iVolante = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("FechaVolante");
                if (!DReader.IsDBNull(indice)) oBE_Tracking.sFechaVolante = DReader.GetString(indice);
                indice = DReader.GetOrdinal("AgenciaAduana");
                if (!DReader.IsDBNull(indice)) oBE_Tracking.sAgenciaAduana = DReader.GetString(indice);
                indice = DReader.GetOrdinal("IdVolante");
                if (!DReader.IsDBNull(indice)) oBE_Tracking.iIdVolante = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("UsuarioGenera");
                if (!DReader.IsDBNull(indice)) oBE_Tracking.sUsuario = DReader.GetString(indice);

                //AUTORIZACION DE RETIRO
                indice = DReader.GetOrdinal("AutRetiro");
                if (!DReader.IsDBNull(indice)) oBE_Tracking.iAutRetiro = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("FecRetiro");
                if (!DReader.IsDBNull(indice)) oBE_Tracking.sFechaRetiro = DReader.GetString(indice);
                indice = DReader.GetOrdinal("DniDespachador");
                if (!DReader.IsDBNull(indice)) oBE_Tracking.sDNIDespachador = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Despachador");
                if (!DReader.IsDBNull(indice)) oBE_Tracking.sDespachador = DReader.GetString(indice);

                //SALIDA DEL ALMACEN
                indice = DReader.GetOrdinal("TicketDespacho");
                if (!DReader.IsDBNull(indice)) oBE_Tracking.iTicketDespacho = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("IngresoDespacho");
                if (!DReader.IsDBNull(indice)) oBE_Tracking.sIngresoDespacho = DReader.GetString(indice);
                indice = DReader.GetOrdinal("SalidaDespacho");
                if (!DReader.IsDBNull(indice)) oBE_Tracking.sSalidaDespacho = DReader.GetString(indice);
                indice = DReader.GetOrdinal("PesoDespacho");
                if (!DReader.IsDBNull(indice)) oBE_Tracking.iPesoDespacho = DReader.GetInt32(indice);

                indice = DReader.GetOrdinal("Consignatario");
                if (!DReader.IsDBNull(indice)) oBE_Tracking.sConsignatario = DReader.GetString(indice);
                indice = DReader.GetOrdinal("ContenedorBuscar");
                if (!DReader.IsDBNull(indice)) oBE_Tracking.sContenedor = DReader.GetString(indice);
                indice = DReader.GetOrdinal("IdMovBalIngreso");
                if (!DReader.IsDBNull(indice)) oBE_Tracking.iIdMovBalIngreso = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("IdMovBalSalida");
                if (!DReader.IsDBNull(indice)) oBE_Tracking.iIdMovBalSalida = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("IdEirIngreso");
                if (!DReader.IsDBNull(indice)) oBE_Tracking.iIdEirIngreso = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("IdEirSalida");
                if (!DReader.IsDBNull(indice)) oBE_Tracking.iIdEirSalida = DReader.GetInt32(indice);

                return oBE_Tracking;
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public static BE_Tracking ListarItems(IDataReader DReader)
        {
            try
            {
                BE_Tracking oBE_Tracking = new BE_Tracking();

                int indice = 0;

                indice = DReader.GetOrdinal("Codigo");
                if (!DReader.IsDBNull(indice)) oBE_Tracking.iCodigo = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("Descripcion");
                if (!DReader.IsDBNull(indice)) oBE_Tracking.sDescripcion = DReader.GetString(indice);

                return oBE_Tracking;
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public static BE_Tracking ListarSeguimiento(IDataReader DReader)
        {
            try
            {
                BE_Tracking oBE_Tracking = new BE_Tracking();

                int indice = 0;

                indice = DReader.GetOrdinal("IdDocOriDet");
                if (!DReader.IsDBNull(indice)) oBE_Tracking.iIdDocOriDet = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("NumeroDO");
                if (!DReader.IsDBNull(indice)) oBE_Tracking.sDocumentoOrigen = DReader.GetString(indice);
                indice = DReader.GetOrdinal("ContenedorChasis");
                if (!DReader.IsDBNull(indice)) oBE_Tracking.sContenedor = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Condicion");
                if (!DReader.IsDBNull(indice)) oBE_Tracking.sCondicion = DReader.GetString(indice);
                indice = DReader.GetOrdinal("TipoCtn");
                if (!DReader.IsDBNull(indice)) oBE_Tracking.sTipoCtn = DReader.GetString(indice);
                indice = DReader.GetOrdinal("ClienteFinal");
                if (!DReader.IsDBNull(indice)) oBE_Tracking.sClienteFinal = DReader.GetString(indice);
                indice = DReader.GetOrdinal("BultoManif");
                if (!DReader.IsDBNull(indice)) oBE_Tracking.dBultoManif = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("PesoManif");
                if (!DReader.IsDBNull(indice)) oBE_Tracking.dPesoManif = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("BultosRecib");
                if (!DReader.IsDBNull(indice)) oBE_Tracking.dBultoRecib = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("PesoRecib");
                if (!DReader.IsDBNull(indice)) oBE_Tracking.dPesoRecib = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("Precintos");
                if (!DReader.IsDBNull(indice)) oBE_Tracking.sPrecintos = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Situacion");
                if (!DReader.IsDBNull(indice)) oBE_Tracking.sSituacion = DReader.GetString(indice);
                indice = DReader.GetOrdinal("IdDocOri");
                if (!DReader.IsDBNull(indice)) oBE_Tracking.iIdDocOri = DReader.GetInt32(indice);

                return oBE_Tracking;
            }
            catch (Exception e)
            {
                return null;
            }
        }
    }
}