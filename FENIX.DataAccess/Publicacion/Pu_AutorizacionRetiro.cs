﻿using FENIX.BusinessEntity;
using FENIX.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FENIX.DataAccess.Publicacion
{
    class Pu_AutorizacionRetiro
    {
        public static BE_AutorizacionRetiro Ext_SO_AutorizacionRetiro(IDataReader DReader)
        {
            ResultadoTransaccionTx oResultadoTransaccionTx = null;
            try
            {
                BE_AutorizacionRetiro oBE_Doc = new BE_AutorizacionRetiro();
                int indice = 0;
                
                indice = DReader.GetOrdinal("idSolicitudAre");
                if (!DReader.IsDBNull(indice)) oBE_Doc.IdSolicitudAre = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("dtFechaRegistro");
                if (!DReader.IsDBNull(indice)) oBE_Doc.dtFechaRegistro = DReader.GetDateTime(indice);
                indice = DReader.GetOrdinal("Volant_vch_Numero");
                if (!DReader.IsDBNull(indice)) oBE_Doc.sVolanteNumero = DReader.GetString(indice);
                indice = DReader.GetOrdinal("DocOri_vch_NumeroDO");
                if (!DReader.IsDBNull(indice)) oBE_Doc.sNumeroDO = DReader.GetString(indice);
                indice = DReader.GetOrdinal("cliente");
                if (!DReader.IsDBNull(indice)) oBE_Doc.SCliente = DReader.GetString(indice);
                indice = DReader.GetOrdinal("AutRet_vch_Numero");
                if (!DReader.IsDBNull(indice)) oBE_Doc.sAutRetNumero = DReader.GetString(indice);
                indice = DReader.GetOrdinal("AutRet_dt_Fecha");
                if (!DReader.IsDBNull(indice)) oBE_Doc.dtAutRetFecha = DReader.GetDateTime(indice);
                indice = DReader.GetOrdinal("AutRet_dt_FechaVigencia");
                if (!DReader.IsDBNull(indice)) oBE_Doc.dtAutRetFechaVigencia = DReader.GetDateTime(indice);
                indice = DReader.GetOrdinal("EstadoTramite");
                if (!DReader.IsDBNull(indice)) oBE_Doc.sEstadoTramite = DReader.GetString(indice);
                indice = DReader.GetOrdinal("IdAutRet");
                if (!DReader.IsDBNull(indice)) oBE_Doc.idAutRet = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("iSituacion");
                if (!DReader.IsDBNull(indice)) oBE_Doc.idSituacion = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("nvTipoSolicitud");
                if (!DReader.IsDBNull(indice)) oBE_Doc.sTipoSolicitud = DReader.GetString(indice);
                indice = DReader.GetOrdinal("iTipoSolicitud");
                if (!DReader.IsDBNull(indice)) oBE_Doc.iTipoSolicitud = DReader.GetInt32(indice);


                return oBE_Doc;
            }
            catch (Exception e)
            {
                oResultadoTransaccionTx.RegistrarError(e);
                return null;
            }

        }


        public static BE_AutorizacionRetiro Ext_SO_AutorizacionRetiroDet(IDataReader DReader)
        {
            ResultadoTransaccionTx oResultadoTransaccionTx = null;
            try
            {
                BE_AutorizacionRetiro oBE_Doc = new BE_AutorizacionRetiro();
                int indice = 0;

                indice = DReader.GetOrdinal("idSolicitudAreDet");
                if (!DReader.IsDBNull(indice)) oBE_Doc.IdSolicitudAreDet = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("iTipoDocAdjuntado");
                if (!DReader.IsDBNull(indice)) oBE_Doc.iTipoDocAdjuntado =DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("iSituacion");
                if (!DReader.IsDBNull(indice)) oBE_Doc.idSituacion = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("sRuta");
                if (!DReader.IsDBNull(indice)) oBE_Doc.sRuta = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Situacion");
                if (!DReader.IsDBNull(indice)) oBE_Doc.SSituacion = DReader.GetString(indice);
                indice = DReader.GetOrdinal("ObservacionCLiente");
                if (!DReader.IsDBNull(indice)) oBE_Doc.sObservacionCliente = DReader.GetString(indice);
                indice = DReader.GetOrdinal("ObservacionFargo");
                if (!DReader.IsDBNull(indice)) oBE_Doc.sObservacionResp = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Despac_vch_DNI");
                if (!DReader.IsDBNull(indice)) oBE_Doc.sDniDespachador = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Despac_vch_Nombre");
                if (!DReader.IsDBNull(indice)) oBE_Doc.sNombreDespachador = DReader.GetString(indice);
                indice = DReader.GetOrdinal("iTipoSolicitud");
                if (!DReader.IsDBNull(indice)) oBE_Doc.iTipoSolicitud = DReader.GetInt32(indice);
                return oBE_Doc;
            }
            catch (Exception e)
            {
                oResultadoTransaccionTx.RegistrarError(e);
                return null;
            }

        }

        public static BE_AutorizacionRetiro ListarProcesoPorId(IDataReader DReader)
        {
            try
            {
                BE_AutorizacionRetiro oBE_AutorizacionRetiro = new BE_AutorizacionRetiro();
                int indice = 0;

                indice = DReader.GetOrdinal("EstadoVigencia");
                if (!DReader.IsDBNull(indice)) oBE_AutorizacionRetiro.sEstadoVigencia = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Situacion");
                if (!DReader.IsDBNull(indice)) oBE_AutorizacionRetiro.SSituacion = DReader.GetString(indice);
                indice = DReader.GetOrdinal("IdDespachador");
                if (!DReader.IsDBNull(indice)) oBE_AutorizacionRetiro.iIdDespachador = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("Despachador");
                if (!DReader.IsDBNull(indice)) oBE_AutorizacionRetiro.SDespachador = DReader.GetString(indice);
                indice = DReader.GetOrdinal("DniDespachador");
                if (!DReader.IsDBNull(indice)) oBE_AutorizacionRetiro.sDniDespachador = DReader.GetString(indice);
                indice = DReader.GetOrdinal("IdAutRet");
                if (!DReader.IsDBNull(indice)) oBE_AutorizacionRetiro.IIdAutRet = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("FechaCreacion");
                if (!DReader.IsDBNull(indice)) oBE_AutorizacionRetiro.dFechaCreacion = DReader.GetDateTime(indice);
                indice = DReader.GetOrdinal("FechaHabilitado");
                if (!DReader.IsDBNull(indice)) oBE_AutorizacionRetiro.dFechaHabilitado = DReader.GetDateTime(indice);
                indice = DReader.GetOrdinal("FechaMaxVigencia");
                if (!DReader.IsDBNull(indice)) oBE_AutorizacionRetiro.dFechaMaxVigencia = DReader.GetDateTime(indice);

                return oBE_AutorizacionRetiro;
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public static BE_AutorizacionRetiro ListarAutorizacionRetiroPorId_Rep(IDataReader DReader)
        {
            try
            {
                BE_AutorizacionRetiro oBE_AutorizacionRetiro = new BE_AutorizacionRetiro();
                int indice = 0;
                indice = DReader.GetOrdinal("IdAutRet");
                if (!DReader.IsDBNull(indice)) oBE_AutorizacionRetiro.RepIdAutRet = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("NroAutRet");
                if (!DReader.IsDBNull(indice)) oBE_AutorizacionRetiro.RepNroAutRet = DReader.GetString(indice);
                indice = DReader.GetOrdinal("IdAutRetDet");
                if (!DReader.IsDBNull(indice)) oBE_AutorizacionRetiro.RepIdAutRetDet = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("NumeroManifiesto");
                if (!DReader.IsDBNull(indice)) oBE_AutorizacionRetiro.RepNumeroManifiesto = DReader.GetString(indice);
                indice = DReader.GetOrdinal("NaveViaje");
                if (!DReader.IsDBNull(indice)) oBE_AutorizacionRetiro.RepNaveViaje = DReader.GetString(indice);
                indice = DReader.GetOrdinal("NumeroDO");
                if (!DReader.IsDBNull(indice)) oBE_AutorizacionRetiro.RepNumeroDO = DReader.GetString(indice);
                indice = DReader.GetOrdinal("NumeroMadre");
                if (!DReader.IsDBNull(indice)) oBE_AutorizacionRetiro.RepNumeroMadre = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Operacion"); ;
                if (!DReader.IsDBNull(indice)) oBE_AutorizacionRetiro.RepOperacion = DReader.GetString(indice);
                indice = DReader.GetOrdinal("FechaFinDescarga");
                if (!DReader.IsDBNull(indice)) oBE_AutorizacionRetiro.RepFechaFinDescarga = DReader.GetDateTime(indice);
                indice = DReader.GetOrdinal("Consignatario");
                if (!DReader.IsDBNull(indice)) oBE_AutorizacionRetiro.RepConsignatario = DReader.GetString(indice);
                indice = DReader.GetOrdinal("AgenciaAduana");
                if (!DReader.IsDBNull(indice)) oBE_AutorizacionRetiro.RepAgenciaAduana = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Despachador");
                if (!DReader.IsDBNull(indice)) oBE_AutorizacionRetiro.RepDespachador = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Volante");
                if (!DReader.IsDBNull(indice)) oBE_AutorizacionRetiro.RepVolante = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Fecha");
                if (!DReader.IsDBNull(indice)) oBE_AutorizacionRetiro.RepFecha = DReader.GetDateTime(indice);
                indice = DReader.GetOrdinal("FechaVigencia");
                if (!DReader.IsDBNull(indice)) oBE_AutorizacionRetiro.RepFechaVigencia = DReader.GetDateTime(indice);
                indice = DReader.GetOrdinal("Item");
                if (!DReader.IsDBNull(indice)) oBE_AutorizacionRetiro.RepItem = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("Condicion");
                if (!DReader.IsDBNull(indice)) oBE_AutorizacionRetiro.RepCondicion = DReader.GetString(indice);
                indice = DReader.GetOrdinal("UsuarioRegistro");
                if (!DReader.IsDBNull(indice)) oBE_AutorizacionRetiro.RepUserRegi = DReader.GetString(indice);
                indice = DReader.GetOrdinal("chasis");
                if (!DReader.IsDBNull(indice)) oBE_AutorizacionRetiro.RepChasisContenedor = DReader.GetString(indice);
                indice = DReader.GetOrdinal("GitsCNT");
                if (!DReader.IsDBNull(indice)) oBE_AutorizacionRetiro.RepTipoContenedor = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Tamano");
                if (!DReader.IsDBNull(indice)) oBE_AutorizacionRetiro.RepTamano = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("Cantidad");
                if (!DReader.IsDBNull(indice)) oBE_AutorizacionRetiro.RepCantidad = DReader.GetDecimal(indice);
                indice = DReader.GetOrdinal("Tara");
                if (!DReader.IsDBNull(indice)) oBE_AutorizacionRetiro.RepTara = Convert.ToInt32(DReader.GetDecimal(indice));
                indice = DReader.GetOrdinal("PesoNeto");
                if (!DReader.IsDBNull(indice)) oBE_AutorizacionRetiro.RepPesoNeto = Convert.ToInt32(DReader.GetDecimal(indice));
                indice = DReader.GetOrdinal("Carga");
                if (!DReader.IsDBNull(indice)) oBE_AutorizacionRetiro.RepCarga = DReader.GetString(indice);
                indice = DReader.GetOrdinal("NumeroDUA");
                if (!DReader.IsDBNull(indice)) oBE_AutorizacionRetiro.RepNumeroDUA = DReader.GetString(indice);
                indice = DReader.GetOrdinal("TrasladoInterno");
                if (!DReader.IsDBNull(indice)) oBE_AutorizacionRetiro.RepTrasladoInterno = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Observacion");
                if (!DReader.IsDBNull(indice)) oBE_AutorizacionRetiro.RepObservacion = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Bultos");
                if (!DReader.IsDBNull(indice)) oBE_AutorizacionRetiro.RepBultos = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("Referencia");
                if (!DReader.IsDBNull(indice)) oBE_AutorizacionRetiro.RepReferencia = DReader.GetString(indice);
                indice = DReader.GetOrdinal("FMaxVigencia");
                if (!DReader.IsDBNull(indice)) oBE_AutorizacionRetiro.RepFMaxVigencia = DReader.GetDateTime(indice);

                return oBE_AutorizacionRetiro;
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public static BE_AutorizacionRetiro Ext_SO_AutorizacionRetiroCola(IDataReader DReader)
        {
            try
            {
                BE_AutorizacionRetiro oBE_AutorizacionRetiro = new BE_AutorizacionRetiro();
                int indice = 0;
                indice = DReader.GetOrdinal("numeroOrden");
                if (!DReader.IsDBNull(indice)) oBE_AutorizacionRetiro.iNumeroOrden = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("idSolicitudAre");
                if (!DReader.IsDBNull(indice)) oBE_AutorizacionRetiro.IdSolicitudAre = DReader.GetInt32(indice);
                return oBE_AutorizacionRetiro;
            }
            catch (Exception e)
            {
                return null;
            }
        }
        public static BE_AutorizacionRetiro Ext_SO_AutorizacionRetiroPago(IDataReader DReader)
        {
            ResultadoTransaccionTx oResultadoTransaccionTx = null;
            try
            {
                BE_AutorizacionRetiro oBE_Doc = new BE_AutorizacionRetiro();
                int indice = 0;

                indice = DReader.GetOrdinal("nvFormaPago");
                if (!DReader.IsDBNull(indice)) oBE_Doc.sFormaPago = DReader.GetString(indice);
                indice = DReader.GetOrdinal("nvTipoPago");
                if (!DReader.IsDBNull(indice)) oBE_Doc.sTipoPago = DReader.GetString(indice);
                indice = DReader.GetOrdinal("nvBanco");
                if (!DReader.IsDBNull(indice)) oBE_Doc.sBanco = DReader.GetString(indice);
                indice = DReader.GetOrdinal("dMontoPago");
                if (!DReader.IsDBNull(indice)) oBE_Doc.dMontoPago = DReader.GetDecimal(indice);
                indice = DReader.GetOrdinal("nvRuc");
                if (!DReader.IsDBNull(indice)) oBE_Doc.sRucCliente = DReader.GetString(indice);
                indice = DReader.GetOrdinal("nvNumeroOperacion");
                if (!DReader.IsDBNull(indice)) oBE_Doc.sNumeroOperacion = DReader.GetString(indice);
                indice = DReader.GetOrdinal("idMoneda");
                if (!DReader.IsDBNull(indice)) oBE_Doc.IdMoneda = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("IdLiquidacion");
                if (!DReader.IsDBNull(indice)) oBE_Doc.idLiquidacion = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("Liq_Num_TotalMontoBruto");
                if (!DReader.IsDBNull(indice)) oBE_Doc.dTotalLiquidacion = DReader.GetDecimal(indice);
                indice = DReader.GetOrdinal("Liq_Dt_Fecha");
                if (!DReader.IsDBNull(indice)) oBE_Doc.dtFechaLiquidacion = DReader.GetDateTime(indice);
                indice = DReader.GetOrdinal("dua");
                if (!DReader.IsDBNull(indice)) oBE_Doc.sNumeroDua = DReader.GetString(indice);
                
                return oBE_Doc;
            }
            catch (Exception e)
            {
                oResultadoTransaccionTx.RegistrarError(e);
                return null;
            }

        }
        public static BE_AutorizacionRetiro Ext_SO_AutorizacionRetiroLigado(IDataReader DReader)
        {
            ResultadoTransaccionTx oResultadoTransaccionTx = null;
            try
            {
                BE_AutorizacionRetiro oBE_Doc = new BE_AutorizacionRetiro();
                int indice = 0;

                indice = DReader.GetOrdinal("idSolicitudAre");
                if (!DReader.IsDBNull(indice)) oBE_Doc.IdSolicitudAre = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("Volant_vch_Numero");
                if (!DReader.IsDBNull(indice)) oBE_Doc.sVolanteNumero = DReader.GetString(indice);
                indice = DReader.GetOrdinal("dua");
                if (!DReader.IsDBNull(indice)) oBE_Doc.sNumeroDua = DReader.GetString(indice);
                indice = DReader.GetOrdinal("DocOri_vch_NumeroDO");
                if (!DReader.IsDBNull(indice)) oBE_Doc.sNumeroDO = DReader.GetString(indice);
                indice = DReader.GetOrdinal("DocOri_vch_NumeroDOMadre");
                if (!DReader.IsDBNull(indice)) oBE_Doc.sNumeroDOMaster = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Client_vch_Razon_Social");
                if (!DReader.IsDBNull(indice)) oBE_Doc.SCliente = DReader.GetString(indice);
                indice = DReader.GetOrdinal("IdLiquidacion");
                if (!DReader.IsDBNull(indice)) oBE_Doc.iIdLiquidacion = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("Liq_Num_TotalMontoBruto");
                if (!DReader.IsDBNull(indice)) oBE_Doc.dTotalLiquidacion = DReader.GetDecimal(indice);


                return oBE_Doc;
            }
            catch (Exception e)
            {
                oResultadoTransaccionTx.RegistrarError(e);
                return null;
            }

        }

    }
}
