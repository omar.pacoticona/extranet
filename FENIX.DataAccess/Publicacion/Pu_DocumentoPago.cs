﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FENIX.BusinessEntity;
using System.Data;
using FENIX.Common;

namespace FENIX.DataAccess.Publicacion
{
    public class Pu_DocumentoPago
    {
        public static BE_DocumentoPago ListarDocumentosPago(IDataReader DReader)
        {
            try
            {
                BE_DocumentoPago oBE_DocumentoPago = new BE_DocumentoPago();
                int indice = 0 ;

                indice = DReader.GetOrdinal("IdDocPago");
                if (!DReader.IsDBNull(indice)) oBE_DocumentoPago.iIdDocPago = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("DocPag_Serie");
                if (!DReader.IsDBNull(indice)) oBE_DocumentoPago.sSerie = DReader.GetString(indice);
                indice = DReader.GetOrdinal("DocPag_Numero");
                if (!DReader.IsDBNull(indice)) oBE_DocumentoPago.sNumero = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Negocio");
                if (!DReader.IsDBNull(indice)) oBE_DocumentoPago.sNegocio = DReader.GetString(indice);
                indice = DReader.GetOrdinal("DocPag_dt_FechaEmision");
                if (!DReader.IsDBNull(indice)) oBE_DocumentoPago.sFechaEmision = DReader.GetDateTime(indice);
                indice = DReader.GetOrdinal("Client_vch_Razon_Social");
                if (!DReader.IsDBNull(indice)) oBE_DocumentoPago.sCliente = DReader.GetString(indice);
                indice = DReader.GetOrdinal("NumeroDO");
                if (!DReader.IsDBNull(indice)) oBE_DocumentoPago.sNroDocumentoOrigen = DReader.GetString(indice);
                indice = DReader.GetOrdinal("volante");
                if (!DReader.IsDBNull(indice)) oBE_DocumentoPago.sNroVolante = DReader.GetString(indice);
                indice = DReader.GetOrdinal("TipoMoneda");
                if (!DReader.IsDBNull(indice)) oBE_DocumentoPago.sMoneda = DReader.GetString(indice);
                indice = DReader.GetOrdinal("DocPag_dec_MontoNeto");
                if (!DReader.IsDBNull(indice)) oBE_DocumentoPago.dSubtotal = DReader.GetDecimal(indice);
                indice = DReader.GetOrdinal("DocPag_dec_Impuesto");
                if (!DReader.IsDBNull(indice)) oBE_DocumentoPago.dIGV = DReader.GetDecimal(indice);
                indice = DReader.GetOrdinal("DocPag_dec_MontoBruto");
                if (!DReader.IsDBNull(indice)) oBE_DocumentoPago.dTotal = DReader.GetDecimal(indice);
                indice = DReader.GetOrdinal("TipoOperacion");
                if (!DReader.IsDBNull(indice)) oBE_DocumentoPago.sTipoOperacion = DReader.GetString(indice);
                //indice = DReader.GetOrdinal("TipoDocumentoPago");
                //if (!DReader.IsDBNull(indice)) oBE_DocumentoPago.sTipoDocumentoPago = DReader.GetString(indice);

                return oBE_DocumentoPago;             

            }
            catch (Exception e)
            {
                return null;
            }
        }

        public static BE_LineaNegocio ListarLineaNegocio(IDataReader DReader)
        {
            try
            {
                BE_LineaNegocio oBE_LineaNegocio = new BE_LineaNegocio();
                int indice = 0;

                indice = DReader.GetOrdinal("IdLinea");
                if (!DReader.IsDBNull(indice)) oBE_LineaNegocio.idLinea = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("Linea");
                if (!DReader.IsDBNull(indice)) oBE_LineaNegocio.LineaNegocio = DReader.GetString(indice);
               

                return oBE_LineaNegocio;

            }
            catch (Exception e)
            {
                return null;
            }
        }

        public static BE_TipoDocumentoPago ListarTipoDocumentoPago(IDataReader DReader)
        {
            try
            {
                BE_TipoDocumentoPago oBE_TipoDocumentoPago = new BE_TipoDocumentoPago();
                int indice = 0;
                indice = DReader.GetOrdinal("IdTipoDocumentoPago");
                if (!DReader.IsDBNull(indice)) oBE_TipoDocumentoPago.iIdTipoDocumentoPago = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("TipoDocumentoPago");
                if (!DReader.IsDBNull(indice)) oBE_TipoDocumentoPago.sTipoDocumentoPago  = DReader.GetString(indice);


                return oBE_TipoDocumentoPago;

            }
            catch(Exception e)
            {
                return null;
            }

        }
    }
}
