﻿using System;
using System.Collections.Generic;
using System.Text;
using FENIX.Common;
using FENIX.BusinessEntity;
using System.Data;

namespace FENIX.DataAccess
{
    public class Pu_OrdenServicio
    {
        public static BE_OrdenServicio Listar(IDataReader DReader)
        {
            try
            {
                BE_OrdenServicio oBE_OrdenServicio = new BE_OrdenServicio();
                int indice = 0;
                indice = DReader.GetOrdinal("IdOrdSer");
                if (!DReader.IsDBNull(indice)) oBE_OrdenServicio.iIdOrdSer = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("RolCliente");
                if (!DReader.IsDBNull(indice)) oBE_OrdenServicio.sDescripcionRolCliente = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Cliente_Razon_Social");
                if (!DReader.IsDBNull(indice)) oBE_OrdenServicio.sDescripcionCliente = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Socio_Razon_Social");
                if (!DReader.IsDBNull(indice)) oBE_OrdenServicio.sDescripcionSocio = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Socio_Rol");
                if (!DReader.IsDBNull(indice)) oBE_OrdenServicio.sDescripcionRolSocio = DReader.GetString(indice);
                indice = DReader.GetOrdinal("TipoCreacion");
                if (!DReader.IsDBNull(indice)) oBE_OrdenServicio.sDescripcionTipoCreacion = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Situacion");
                if (!DReader.IsDBNull(indice)) oBE_OrdenServicio.sDescripcionSituacion = DReader.GetString(indice);
                indice = DReader.GetOrdinal("OrdSer_dt_Fecha");
                if (!DReader.IsDBNull(indice)) oBE_OrdenServicio.sFecha = DReader.GetString(indice);
                indice = DReader.GetOrdinal("OrdSer_vch_Observacion");
                if (!DReader.IsDBNull(indice)) oBE_OrdenServicio.sObservacion = DReader.GetString(indice);

                indice = DReader.GetOrdinal("Automatico");
                if (!DReader.IsDBNull(indice)) oBE_OrdenServicio.sAutomatico = DReader.GetString(indice);

                return oBE_OrdenServicio;
            }
            catch (Exception e)
            {

                return null;
            }
        }

        public static BE_OrdenServicio ListarPorId(IDataReader DReader)
        {
            try
            {
                BE_OrdenServicio oBE_OrdenServicio = new BE_OrdenServicio();
                int indice = 0;
                indice = DReader.GetOrdinal("IdOrdSer");
                if (!DReader.IsDBNull(indice)) oBE_OrdenServicio.iIdOrdSer = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("IdRolCliente");
                if (!DReader.IsDBNull(indice)) oBE_OrdenServicio.iIdRolCliente = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("RolCliente");
                if (!DReader.IsDBNull(indice)) oBE_OrdenServicio.sDescripcionRolCliente = DReader.GetString(indice);
                indice = DReader.GetOrdinal("IdCliente");
                if (!DReader.IsDBNull(indice)) oBE_OrdenServicio.iIdCliente = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("Cliente_Razon_Social");
                if (!DReader.IsDBNull(indice)) oBE_OrdenServicio.sDescripcionCliente = DReader.GetString(indice);
                indice = DReader.GetOrdinal("IdSocio");
                if (!DReader.IsDBNull(indice)) oBE_OrdenServicio.iIdSocio = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("Socio_Razon_Social");
                if (!DReader.IsDBNull(indice)) oBE_OrdenServicio.sDescripcionSocio = DReader.GetString(indice);
                indice = DReader.GetOrdinal("IdRolSocio");
                if (!DReader.IsDBNull(indice)) oBE_OrdenServicio.iIdRolSocio = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("Socio_Rol");
                if (!DReader.IsDBNull(indice)) oBE_OrdenServicio.sDescripcionRolSocio = DReader.GetString(indice);
                indice = DReader.GetOrdinal("OrdSer_vch_Solicitante");
                if (!DReader.IsDBNull(indice)) oBE_OrdenServicio.sSolicitante = DReader.GetString(indice);
                indice = DReader.GetOrdinal("OrdSer_vch_Observacion");
                if (!DReader.IsDBNull(indice)) oBE_OrdenServicio.sObservacion = DReader.GetString(indice);
                indice = DReader.GetOrdinal("OrdSer_dt_Fecha");
                if (!DReader.IsDBNull(indice)) oBE_OrdenServicio.sFecha = DReader.GetString(indice);
                indice = DReader.GetOrdinal("OrdSer_dt_FechaProg");
                if (!DReader.IsDBNull(indice)) oBE_OrdenServicio.sFechaProg = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Automatico");
                if (!DReader.IsDBNull(indice)) oBE_OrdenServicio.sAutomatico = DReader.GetString(indice);
                indice = DReader.GetOrdinal("cli_NumDocumento");
                if (!DReader.IsDBNull(indice)) oBE_OrdenServicio.sCliDni = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Soc_NumDocumento");
                if (!DReader.IsDBNull(indice)) oBE_OrdenServicio.sSocDni = DReader.GetString(indice);


                return oBE_OrdenServicio;
            }
            catch (Exception e)
            {

                return null;
            }
        }

        public static BE_OrdenServicio ListarPorVolante(IDataReader DReader)
        {
            try
            {
                BE_OrdenServicio oBE_OrdenServicio = new BE_OrdenServicio();
                int indice = 0;

                indice = DReader.GetOrdinal("IdCliente");
                if (!DReader.IsDBNull(indice)) oBE_OrdenServicio.iIdCliente = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("Cliente_Razon_Social");
                if (!DReader.IsDBNull(indice)) oBE_OrdenServicio.sDescripcionCliente = DReader.GetString(indice);
                indice = DReader.GetOrdinal("IdSocio");
                if (!DReader.IsDBNull(indice)) oBE_OrdenServicio.iIdSocio = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("Socio_Razon_Social");
                if (!DReader.IsDBNull(indice)) oBE_OrdenServicio.sDescripcionSocio = DReader.GetString(indice);
                indice = DReader.GetOrdinal("IdDocOri");
                if (!DReader.IsDBNull(indice)) oBE_OrdenServicio.iIdDocOri = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("NumeroDO");
                if (!DReader.IsDBNull(indice)) oBE_OrdenServicio.sNumeroDO = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Cli_NumDocumento");
                if (!DReader.IsDBNull(indice)) oBE_OrdenServicio.sCliDni = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Soc_NumDocumento");
                if (!DReader.IsDBNull(indice)) oBE_OrdenServicio.sSocDni = DReader.GetString(indice);


                return oBE_OrdenServicio;
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public static BE_OrdenServicioDetalle ListarOrdenServicioDetalle(IDataReader DReader)
        {
            try
            {
                BE_OrdenServicioDetalle oBE_OrdenServicioDetalle = new BE_OrdenServicioDetalle();
                int indice = 0;
                indice = DReader.GetOrdinal("IdDocOriDet");
                if (!DReader.IsDBNull(indice)) oBE_OrdenServicioDetalle.iIdDocOriDet = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("Situacion");
                if (!DReader.IsDBNull(indice)) oBE_OrdenServicioDetalle.sDescripcionSituacion = DReader.GetString(indice);
                indice = DReader.GetOrdinal("TamanoCnt");
                if (!DReader.IsDBNull(indice)) oBE_OrdenServicioDetalle.sDescripcionTamanoCnt = DReader.GetString(indice);
                indice = DReader.GetOrdinal("TipoCnt");
                if (!DReader.IsDBNull(indice)) oBE_OrdenServicioDetalle.sDescripcionTipoCnt = DReader.GetString(indice);
                indice = DReader.GetOrdinal("CondicionFacturar");
                if (!DReader.IsDBNull(indice)) oBE_OrdenServicioDetalle.sDescripcionCondicionFacturar = DReader.GetString(indice);
                indice = DReader.GetOrdinal("IdServicio");
                if (!DReader.IsDBNull(indice)) oBE_OrdenServicioDetalle.iIdServicio = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("Ser_Vch_Descripcion");
                if (!DReader.IsDBNull(indice)) oBE_OrdenServicioDetalle.sDescripcionServicio = DReader.GetString(indice);
                indice = DReader.GetOrdinal("IdLiquidacion");
                if (!DReader.IsDBNull(indice)) oBE_OrdenServicioDetalle.iIdLiquidacion = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("OrdSerDet_vch_NumeroDO");
                if (!DReader.IsDBNull(indice)) oBE_OrdenServicioDetalle.sNumeroDO = DReader.GetString(indice);
                indice = DReader.GetOrdinal("OrdSerDet_vch_Contenedor");
                if (!DReader.IsDBNull(indice)) oBE_OrdenServicioDetalle.sContenedor = DReader.GetString(indice);
                indice = DReader.GetOrdinal("OrdSerDet_dec_CantidadSolicitado");
                if (!DReader.IsDBNull(indice)) oBE_OrdenServicioDetalle.dCantidadSolicitado = DReader.GetDecimal(indice);
                indice = DReader.GetOrdinal("OrdSerDet_dec_PesoSolicitado");
                if (!DReader.IsDBNull(indice)) oBE_OrdenServicioDetalle.dPesoSolicitado = DReader.GetDecimal(indice);
                indice = DReader.GetOrdinal("OrdSerDet_dec_VolumenSolicitado");
                if (!DReader.IsDBNull(indice)) oBE_OrdenServicioDetalle.dVolumenSolicitado = DReader.GetDecimal(indice);
                indice = DReader.GetOrdinal("OrdSerDet_dec_HoraSolicitado");
                if (!DReader.IsDBNull(indice)) oBE_OrdenServicioDetalle.dHoraSolicitado = DReader.GetDecimal(indice);
                indice = DReader.GetOrdinal("OrdSerDet_dec_CantidadAtendido");
                if (!DReader.IsDBNull(indice)) oBE_OrdenServicioDetalle.dCantidadAtendido = DReader.GetDecimal(indice);
                indice = DReader.GetOrdinal("OrdSerDet_dec_PesoAtendido");
                if (!DReader.IsDBNull(indice)) oBE_OrdenServicioDetalle.dPesoAtendido = DReader.GetDecimal(indice);
                indice = DReader.GetOrdinal("OrdSerDet_dec_HoraAtendido");
                if (!DReader.IsDBNull(indice)) oBE_OrdenServicioDetalle.dHoraAtendido = DReader.GetDecimal(indice);
                indice = DReader.GetOrdinal("OrdSerDet_Dt_FechaAtencion");
                if (!DReader.IsDBNull(indice)) oBE_OrdenServicioDetalle.sFechaAtencion = DReader.GetString(indice);
                indice = DReader.GetOrdinal("OrdSerDet_Chr_Automatico");
                if (!DReader.IsDBNull(indice)) oBE_OrdenServicioDetalle.sAutomatico = DReader.GetString(indice);
                indice = DReader.GetOrdinal("IdCondicionCargaFacturar");
                if (!DReader.IsDBNull(indice)) oBE_OrdenServicioDetalle.iIdCondicionCargaFacturar = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("IdDocOri");
                if (!DReader.IsDBNull(indice)) oBE_OrdenServicioDetalle.iIdDocOri = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("IdOrdSerDet");
                if (!DReader.IsDBNull(indice)) oBE_OrdenServicioDetalle.iIdOrdSerDet = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("OrdSerDet_dec_Monto");
                if (!DReader.IsDBNull(indice)) oBE_OrdenServicioDetalle.dMonto = DReader.GetDecimal(indice);
                indice = DReader.GetOrdinal("TipoOrdSerDet");
                if (!DReader.IsDBNull(indice)) oBE_OrdenServicioDetalle.sDescripcionTipoOrd = DReader.GetString(indice);
                indice = DReader.GetOrdinal("DocDet_Int_Item");
                if (!DReader.IsDBNull(indice)) oBE_OrdenServicioDetalle.iItem = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("ObservacionD");
                if (!DReader.IsDBNull(indice)) oBE_OrdenServicioDetalle.sObservacion = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Moneda");
                if (!DReader.IsDBNull(indice)) oBE_OrdenServicioDetalle.sMoneda = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Modalidad");
                if (!DReader.IsDBNull(indice)) oBE_OrdenServicioDetalle.iIdModalidad = DReader.GetInt32(indice);



                return oBE_OrdenServicioDetalle;
            }
            catch (Exception e)
            {

                return null;
            }
        }

        public static BE_OrdenServicioDetalle ListarAtencion(IDataReader DReader)
        {
            try
            {
                BE_OrdenServicioDetalle oBE_OrdenServicioDetalle = new BE_OrdenServicioDetalle();
                int indice = 0;
                indice = DReader.GetOrdinal("IdDocOriDet");
                if (!DReader.IsDBNull(indice)) oBE_OrdenServicioDetalle.iIdDocOriDet = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("Situacion");
                if (!DReader.IsDBNull(indice)) oBE_OrdenServicioDetalle.sDescripcionSituacion = DReader.GetString(indice);
                indice = DReader.GetOrdinal("TamanoCnt");
                if (!DReader.IsDBNull(indice)) oBE_OrdenServicioDetalle.sDescripcionTamanoCnt = DReader.GetString(indice);
                indice = DReader.GetOrdinal("TipoCnt");
                if (!DReader.IsDBNull(indice)) oBE_OrdenServicioDetalle.sDescripcionTipoCnt = DReader.GetString(indice);
                indice = DReader.GetOrdinal("CondicionFacturar");
                if (!DReader.IsDBNull(indice)) oBE_OrdenServicioDetalle.sDescripcionCondicionFacturar = DReader.GetString(indice);
                indice = DReader.GetOrdinal("IdServicio");
                if (!DReader.IsDBNull(indice)) oBE_OrdenServicioDetalle.iIdServicio = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("Ser_Vch_Descripcion");
                if (!DReader.IsDBNull(indice)) oBE_OrdenServicioDetalle.sDescripcionServicio = DReader.GetString(indice);
                indice = DReader.GetOrdinal("IdLiquidacion");
                if (!DReader.IsDBNull(indice)) oBE_OrdenServicioDetalle.iIdLiquidacion = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("OrdSerDet_vch_NumeroDO");
                if (!DReader.IsDBNull(indice)) oBE_OrdenServicioDetalle.sNumeroDO = DReader.GetString(indice);
                indice = DReader.GetOrdinal("OrdSerDet_vch_Contenedor");
                if (!DReader.IsDBNull(indice)) oBE_OrdenServicioDetalle.sContenedor = DReader.GetString(indice);
                indice = DReader.GetOrdinal("OrdSerDet_dec_CantidadSolicitado");
                if (!DReader.IsDBNull(indice)) oBE_OrdenServicioDetalle.dCantidadSolicitado = DReader.GetDecimal(indice);
                indice = DReader.GetOrdinal("OrdSerDet_dec_PesoSolicitado");
                if (!DReader.IsDBNull(indice)) oBE_OrdenServicioDetalle.dPesoSolicitado = DReader.GetDecimal(indice);
                indice = DReader.GetOrdinal("OrdSerDet_dec_HoraSolicitado");
                if (!DReader.IsDBNull(indice)) oBE_OrdenServicioDetalle.dHoraSolicitado = DReader.GetDecimal(indice);
                indice = DReader.GetOrdinal("OrdSerDet_dec_CantidadAtendido");
                if (!DReader.IsDBNull(indice)) oBE_OrdenServicioDetalle.dCantidadAtendido = DReader.GetDecimal(indice);
                indice = DReader.GetOrdinal("OrdSerDet_dec_PesoAtendido");
                if (!DReader.IsDBNull(indice)) oBE_OrdenServicioDetalle.dPesoAtendido = DReader.GetDecimal(indice);
                indice = DReader.GetOrdinal("OrdSerDet_dec_HoraAtendido");
                if (!DReader.IsDBNull(indice)) oBE_OrdenServicioDetalle.dHoraAtendido = DReader.GetDecimal(indice);
                indice = DReader.GetOrdinal("OrdSerDet_Dt_FechaAtencion");
                if (!DReader.IsDBNull(indice)) oBE_OrdenServicioDetalle.sFechaAtencion = DReader.GetString(indice);
                indice = DReader.GetOrdinal("OrdSerDet_Chr_Automatico");
                if (!DReader.IsDBNull(indice)) oBE_OrdenServicioDetalle.sAutomatico = DReader.GetString(indice);
                indice = DReader.GetOrdinal("IdCondicionCargaFacturar");
                if (!DReader.IsDBNull(indice)) oBE_OrdenServicioDetalle.iIdCondicionCargaFacturar = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("IdDocOri");
                if (!DReader.IsDBNull(indice)) oBE_OrdenServicioDetalle.iIdDocOri = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("IdOrdSerDet");
                if (!DReader.IsDBNull(indice)) oBE_OrdenServicioDetalle.iIdOrdSerDet = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("OrdSerDet_dec_Monto");
                if (!DReader.IsDBNull(indice)) oBE_OrdenServicioDetalle.dMonto = DReader.GetDecimal(indice);
                indice = DReader.GetOrdinal("Moneda");
                if (!DReader.IsDBNull(indice)) oBE_OrdenServicioDetalle.sMoneda = DReader.GetString(indice);
                indice = DReader.GetOrdinal("TipoOrdSerDet");
                if (!DReader.IsDBNull(indice)) oBE_OrdenServicioDetalle.sDescripcionTipoOrd = DReader.GetString(indice);
                indice = DReader.GetOrdinal("FechaRegistro");
                if (!DReader.IsDBNull(indice)) oBE_OrdenServicioDetalle.sFechaRegistro = DReader.GetString(indice);
                indice = DReader.GetOrdinal("IdOrdSer");
                if (!DReader.IsDBNull(indice)) oBE_OrdenServicioDetalle.iIdOrdSer = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("IdSituacion");
                if (!DReader.IsDBNull(indice)) oBE_OrdenServicioDetalle.iIdSituacion = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("IdTipoServicio");
                if (!DReader.IsDBNull(indice)) oBE_OrdenServicioDetalle.iIdTipoServicio = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("Cliente");
                if (!DReader.IsDBNull(indice)) oBE_OrdenServicioDetalle.sCliente = DReader.GetString(indice);
                indice = DReader.GetOrdinal("UsuarioRegistro");
                if (!DReader.IsDBNull(indice)) oBE_OrdenServicioDetalle.sUsuarioRegistro = DReader.GetString(indice);
                indice = DReader.GetOrdinal("FechaCreacion");
                if (!DReader.IsDBNull(indice)) oBE_OrdenServicioDetalle.sFechaCrea = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Item");
                if (!DReader.IsDBNull(indice)) oBE_OrdenServicioDetalle.iItem = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("Automatico");
                if (!DReader.IsDBNull(indice)) oBE_OrdenServicioDetalle.sAutomatico = DReader.GetString(indice);



                return oBE_OrdenServicioDetalle;
            }
            catch (Exception e)
            {

                return null;
            }
        }

        public static BE_OrdenServicioDetalle ListarODocumentoOrigenDetalle(IDataReader DReader)
        {
            try
            {
                BE_OrdenServicioDetalle oBE_OrdenServicioDetalle = new BE_OrdenServicioDetalle();
                int indice = 0;
                indice = DReader.GetOrdinal("IdDocOri");
                if (!DReader.IsDBNull(indice)) oBE_OrdenServicioDetalle.iIdDocOri = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("IdDocOriDet");
                if (!DReader.IsDBNull(indice)) oBE_OrdenServicioDetalle.iIdDocOriDet = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("DocOri_vch_NumeroDO");
                if (!DReader.IsDBNull(indice)) oBE_OrdenServicioDetalle.sNumeroDO = DReader.GetString(indice);
                indice = DReader.GetOrdinal("IdStoCon");
                if (!DReader.IsDBNull(indice)) oBE_OrdenServicioDetalle.iIdStoCon = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("IdTipoCnt");
                if (!DReader.IsDBNull(indice)) oBE_OrdenServicioDetalle.iIdTipoCnt = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("TipoCnt");
                if (!DReader.IsDBNull(indice)) oBE_OrdenServicioDetalle.sDescripcionTipoCnt = DReader.GetString(indice);
                indice = DReader.GetOrdinal("IdTamanoCnt");
                if (!DReader.IsDBNull(indice)) oBE_OrdenServicioDetalle.iIdTamanoCnt = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("TamanoCnt");
                if (!DReader.IsDBNull(indice)) oBE_OrdenServicioDetalle.sDescripcionTamanoCnt = DReader.GetString(indice);
                indice = DReader.GetOrdinal("IdCondicionCargaFacturar");
                if (!DReader.IsDBNull(indice)) oBE_OrdenServicioDetalle.iIdCondicionCargaFacturar = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("CondicionFacturar");
                if (!DReader.IsDBNull(indice)) oBE_OrdenServicioDetalle.sDescripcionCondicionFacturar = DReader.GetString(indice);
                indice = DReader.GetOrdinal("IdEmbalajeRecibido");
                if (!DReader.IsDBNull(indice)) oBE_OrdenServicioDetalle.iIdEmbalajeRecibido = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("Embalaje");
                if (!DReader.IsDBNull(indice)) oBE_OrdenServicioDetalle.sDescripcionEmbalaje = DReader.GetString(indice);
                indice = DReader.GetOrdinal("IdCargaRecibida");
                if (!DReader.IsDBNull(indice)) oBE_OrdenServicioDetalle.iIdCargaRecibida = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("Carga");
                if (!DReader.IsDBNull(indice)) oBE_OrdenServicioDetalle.sDescripcionCarga = DReader.GetString(indice);
                indice = DReader.GetOrdinal("DocDet_dec_PesoRecibido");
                if (!DReader.IsDBNull(indice)) oBE_OrdenServicioDetalle.dPesoRecibido = DReader.GetDecimal(indice);
                indice = DReader.GetOrdinal("DocDet_dec_BultosRecibido");
                if (!DReader.IsDBNull(indice)) oBE_OrdenServicioDetalle.dBultosRecibidos = DReader.GetDecimal(indice);
                indice = DReader.GetOrdinal("StoCon_vch_Contenedor");
                if (!DReader.IsDBNull(indice)) oBE_OrdenServicioDetalle.sContenedor = DReader.GetString(indice);
                indice = DReader.GetOrdinal("EstaDespachado");
                if (!DReader.IsDBNull(indice)) oBE_OrdenServicioDetalle.sEstaDespachado = DReader.GetString(indice);
                indice = DReader.GetOrdinal("DocDet_Int_Item");
                if (!DReader.IsDBNull(indice)) oBE_OrdenServicioDetalle.iItem = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("TipoOperacion");
                if (!DReader.IsDBNull(indice)) oBE_OrdenServicioDetalle.iTipoOperacion = DReader.GetInt32(indice);


                return oBE_OrdenServicioDetalle;
            }
            catch (Exception e)
            {

                return null;
            }
        }


        public static BE_OrdenServicio ListarReporteOrdenServicio(IDataReader DReader)
        {
            try
            {
                BE_OrdenServicio oBE_OrdenServicio = new BE_OrdenServicio();
                int indice = 0;
                indice = DReader.GetOrdinal("OrdenServicio");
                if (!DReader.IsDBNull(indice)) oBE_OrdenServicio.iIdOrdSer = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("Manifiesto");
                if (!DReader.IsDBNull(indice)) oBE_OrdenServicio.sManifiesto = DReader.GetString(indice);
                indice = DReader.GetOrdinal("NroVolante");
                if (!DReader.IsDBNull(indice)) oBE_OrdenServicio.sNroVolante = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Nave");
                if (!DReader.IsDBNull(indice)) oBE_OrdenServicio.sNave = DReader.GetString(indice);
                indice = DReader.GetOrdinal("NumeroDO");
                if (!DReader.IsDBNull(indice)) oBE_OrdenServicio.sNumeroDO = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Contenedor");
                if (!DReader.IsDBNull(indice)) oBE_OrdenServicio.sContenedor = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Tamano");
                if (!DReader.IsDBNull(indice)) oBE_OrdenServicio.iTamano = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("Tipo");
                if (!DReader.IsDBNull(indice)) oBE_OrdenServicio.iTipo = DReader.GetString(indice);
                indice = DReader.GetOrdinal("ChasisBultos");
                if (!DReader.IsDBNull(indice)) oBE_OrdenServicio.sChasisBulto = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Descripcion");
                if (!DReader.IsDBNull(indice)) oBE_OrdenServicio.sMercaderia = DReader.GetString(indice);
                indice = DReader.GetOrdinal("AgenciaAduana");
                if (!DReader.IsDBNull(indice)) oBE_OrdenServicio.sAgenciaAduana = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Consignatario");
                if (!DReader.IsDBNull(indice)) oBE_OrdenServicio.sConsignatario = DReader.GetString(indice);




                return oBE_OrdenServicio;
            }
            catch (Exception e)
            {

                return null;
            }
        }

        public static BE_OrdenServicioDetalle ListarODocumentoOrigenDetalle_Traslado(IDataReader DReader)
        {
            try
            {
                BE_OrdenServicioDetalle oBE_OrdenServicioDetalle = new BE_OrdenServicioDetalle();
                int indice = 0;
                indice = DReader.GetOrdinal("IdDocOri");
                if (!DReader.IsDBNull(indice)) oBE_OrdenServicioDetalle.iIdDocOri = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("IdDocOriDet");
                if (!DReader.IsDBNull(indice)) oBE_OrdenServicioDetalle.iIdDocOriDet = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("CndCarga");
                if (!DReader.IsDBNull(indice)) oBE_OrdenServicioDetalle.sCondCarga = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Chasis");
                if (!DReader.IsDBNull(indice)) oBE_OrdenServicioDetalle.sChasis = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Contenedor");
                if (!DReader.IsDBNull(indice)) oBE_OrdenServicioDetalle.sContenedor = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Consignatario");
                if (!DReader.IsDBNull(indice)) oBE_OrdenServicioDetalle.sConsignatario = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Carga");
                if (!DReader.IsDBNull(indice)) oBE_OrdenServicioDetalle.sDescripcionCarga = DReader.GetString(indice);
                indice = DReader.GetOrdinal("NumeroOrden");
                if (!DReader.IsDBNull(indice)) oBE_OrdenServicioDetalle.sNroOrden = DReader.GetString(indice);
                indice = DReader.GetOrdinal("BultosRecibido");
                if (!DReader.IsDBNull(indice)) oBE_OrdenServicioDetalle.dBultosRecibidos = DReader.GetDecimal(indice);
                indice = DReader.GetOrdinal("PesoRecibido");
                if (!DReader.IsDBNull(indice)) oBE_OrdenServicioDetalle.dPesoRecibido = DReader.GetDecimal(indice);
                indice = DReader.GetOrdinal("FechaIngreso");
                if (!DReader.IsDBNull(indice)) oBE_OrdenServicioDetalle.dtFechaIngreso = DReader.GetDateTime(indice);
                indice = DReader.GetOrdinal("FechaRetiro");
                if (!DReader.IsDBNull(indice)) oBE_OrdenServicioDetalle.dtFechaRetiro = DReader.GetDateTime(indice);
                indice = DReader.GetOrdinal("DiasAlmacenaje");
                if (!DReader.IsDBNull(indice)) oBE_OrdenServicioDetalle.iDiasAlmacenaje = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("FechaVencimiento");
                if (!DReader.IsDBNull(indice)) oBE_OrdenServicioDetalle.dtFechaVencimiento = DReader.GetDateTime(indice);
                indice = DReader.GetOrdinal("NumeroDO");
                if (!DReader.IsDBNull(indice)) oBE_OrdenServicioDetalle.sNumeroDO = DReader.GetString(indice);



                return oBE_OrdenServicioDetalle;
            }
            catch (Exception e)
            {

                return null;
            }
        }


        public static BE_OrdenServicio ListarServiciosTransporte(IDataReader DReader)
        {
            BE_OrdenServicio Entidad = new BE_OrdenServicio();

            int indice;
            indice = DReader.GetOrdinal("IdOrdSer");
            Entidad.iIdOrdSer = (DReader.IsDBNull(indice) ? 0 : DReader.GetInt32(indice));
            indice = DReader.GetOrdinal("IdOrdSerDet");
            Entidad.iIdOrdSerDet = (DReader.IsDBNull(indice) ? 0 : DReader.GetInt32(indice));
            indice = DReader.GetOrdinal("FechaServicio");
            Entidad.sFechaServicio = (DReader.IsDBNull(indice) ? "" : DReader.GetString(indice));
            indice = DReader.GetOrdinal("IdServicio");
            Entidad._iIdServicio = (DReader.IsDBNull(indice) ? 0 : DReader.GetInt32(indice));
            indice = DReader.GetOrdinal("Servicio");
            Entidad.sServicio = (DReader.IsDBNull(indice) ? "" : DReader.GetString(indice));
            indice = DReader.GetOrdinal("IdMovbal");
            Entidad.iIdMovbal = (DReader.IsDBNull(indice) ? 0 : DReader.GetInt32(indice));
            indice = DReader.GetOrdinal("Ticket");
            Entidad.sTicket = (DReader.IsDBNull(indice) ? "" : DReader.GetString(indice));
            indice = DReader.GetOrdinal("FechaBalanza");
            Entidad.sFechaBalanza = (DReader.IsDBNull(indice) ? "" : DReader.GetString(indice));
            indice = DReader.GetOrdinal("Placa");
            Entidad.sPlaca = (DReader.IsDBNull(indice) ? "" : DReader.GetString(indice));
            indice = DReader.GetOrdinal("Chofer");
            Entidad.sChofer = (DReader.IsDBNull(indice) ? "" : DReader.GetString(indice));
            indice = DReader.GetOrdinal("Transportista");
            Entidad.sTransportista = (DReader.IsDBNull(indice) ? "" : DReader.GetString(indice));
            indice = DReader.GetOrdinal("Situacion");
            Entidad.sDescripcionSituacion = (DReader.IsDBNull(indice) ? "" : DReader.GetString(indice));
            indice = DReader.GetOrdinal("IdSituacion");
            Entidad.iIdSituacion = (DReader.IsDBNull(indice) ? 0 : DReader.GetInt32(indice));
            indice = DReader.GetOrdinal("Importe");
            Entidad.dImporte = (DReader.IsDBNull(indice) ? 0 : DReader.GetDecimal(indice));

            return Entidad;
        }



        #region "FIFA"
        public static BE_OrdenServicio Entidad_ListarOrdenesServicio(IDataRecord DReader)
        {
            try
            {
                BE_OrdenServicio Entidad = new BE_OrdenServicio();

                int indice;
                indice = DReader.GetOrdinal("IdOrdSer");
                Entidad.iIdOrdSer = (DReader.IsDBNull(indice) ? 0 : DReader.GetInt32(indice));
                indice = DReader.GetOrdinal("Fecha");
                Entidad.sFecha = (DReader.IsDBNull(indice) ? "" : DReader.GetString(indice));
                indice = DReader.GetOrdinal("Cliente");
                Entidad.sDescripcionCliente = (DReader.IsDBNull(indice) ? "" : DReader.GetString(indice));
                indice = DReader.GetOrdinal("Socio");
                Entidad.sDescripcionSocio = (DReader.IsDBNull(indice) ? "" : DReader.GetString(indice));
                indice = DReader.GetOrdinal("Origen");
                Entidad.sDescripcionTipoCreacion = (DReader.IsDBNull(indice) ? "" : DReader.GetString(indice));
                indice = DReader.GetOrdinal("Documento");
                Entidad.sNumeroDO = (DReader.IsDBNull(indice) ? "" : DReader.GetString(indice));
                indice = DReader.GetOrdinal("Observacion");
                Entidad.sObservacion = (DReader.IsDBNull(indice) ? "" : DReader.GetString(indice));
                indice = DReader.GetOrdinal("Situacion");
                Entidad.sDescripcionSituacion = (DReader.IsDBNull(indice) ? "" : DReader.GetString(indice));
                indice = DReader.GetOrdinal("DocumentoBuscar");
                Entidad.sDocumentoBuscar = (DReader.IsDBNull(indice) ? "" : DReader.GetString(indice));

                return Entidad;
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public static BE_OrdenServicioDetalle Entidad_ListarDocumentoOrigenDetalleNuevaOrden(IDataRecord DReader)
        {
            BE_OrdenServicioDetalle Entidad = new BE_OrdenServicioDetalle();

            int indice;
            indice = DReader.GetOrdinal("IdDocOri");
            Entidad.iIdDocOri = (DReader.IsDBNull(indice) ? 0 : DReader.GetInt32(indice));
            indice = DReader.GetOrdinal("IdDocOriDet");
            Entidad.iIdDocOriDet = (DReader.IsDBNull(indice) ? 0 : DReader.GetInt32(indice));
            indice = DReader.GetOrdinal("Documento");
            Entidad.sNumeroDO = (DReader.IsDBNull(indice) ? "" : DReader.GetString(indice));
            indice = DReader.GetOrdinal("IdStoCon");
            Entidad.iIdStoCon = (DReader.IsDBNull(indice) ? 0 : DReader.GetInt32(indice));
            indice = DReader.GetOrdinal("TipoCnt");
            Entidad.sDescripcionTipoCnt = (DReader.IsDBNull(indice) ? "" : DReader.GetString(indice));
            indice = DReader.GetOrdinal("TamanoCnt");
            Entidad.sDescripcionTamanoCnt = (DReader.IsDBNull(indice) ? "" : DReader.GetString(indice));
            indice = DReader.GetOrdinal("CondicionFacturar");
            Entidad.sDescripcionCondicionFacturar = (DReader.IsDBNull(indice) ? "" : DReader.GetString(indice));
            indice = DReader.GetOrdinal("Bultos");
            Entidad.dBultosRecibidos = (DReader.IsDBNull(indice) ? 0 : DReader.GetDecimal(indice));
            indice = DReader.GetOrdinal("Peso");
            Entidad.dPesoRecibido = (DReader.IsDBNull(indice) ? 0 : DReader.GetDecimal(indice));
            indice = DReader.GetOrdinal("Contenedor");
            Entidad.sContenedor = (DReader.IsDBNull(indice) ? "" : DReader.GetString(indice));
            indice = DReader.GetOrdinal("EstaDespachado");
            Entidad.sEstaDespachado = (DReader.IsDBNull(indice) ? "" : DReader.GetString(indice));
            indice = DReader.GetOrdinal("Item");
            Entidad.iItem = (DReader.IsDBNull(indice) ? 0 : DReader.GetInt32(indice));
            indice = DReader.GetOrdinal("IdCliente");
            Entidad.iIdCliente = (DReader.IsDBNull(indice) ? 0 : DReader.GetInt32(indice));
            indice = DReader.GetOrdinal("Cliente");
            Entidad.sCliente = (DReader.IsDBNull(indice) ? "" : DReader.GetString(indice));
            indice = DReader.GetOrdinal("IdSocio");
            Entidad.iIdSocio = (DReader.IsDBNull(indice) ? 0 : DReader.GetInt32(indice));
            indice = DReader.GetOrdinal("Socio");
            Entidad.sSocio = (DReader.IsDBNull(indice) ? "" : DReader.GetString(indice));

            return Entidad;
        }


        public static BE_OrdenServicio ListarOrdenServicioPorId_2(IDataReader DReader)
        {
            try
            {
                BE_OrdenServicio Entidad = new BE_OrdenServicio();

                int indice;
                indice = DReader.GetOrdinal("TipoOperacion");
                Entidad.iIdTipoOperacion = (DReader.IsDBNull(indice) ? 0 : DReader.GetInt32(indice));
                indice = DReader.GetOrdinal("IdOrigen");
                Entidad.iIdOrigen = (DReader.IsDBNull(indice) ? 0 : DReader.GetInt32(indice));
                indice = DReader.GetOrdinal("IdSituacion");
                Entidad.iIdSituacion = (DReader.IsDBNull(indice) ? 0 : DReader.GetInt32(indice));
                indice = DReader.GetOrdinal("FechaSolicitud");
                Entidad.sFechaServicio = (DReader.IsDBNull(indice) ? "" : DReader.GetString(indice));
                indice = DReader.GetOrdinal("FechaProgramacion");
                Entidad.sFechaProg = (DReader.IsDBNull(indice) ? "" : DReader.GetString(indice));
                indice = DReader.GetOrdinal("Observacion");
                Entidad.sObservacion = (DReader.IsDBNull(indice) ? "" : DReader.GetString(indice));
                indice = DReader.GetOrdinal("IdCliente");
                Entidad.iIdCliente = (DReader.IsDBNull(indice) ? 0 : DReader.GetInt32(indice));
                indice = DReader.GetOrdinal("Cliente");
                Entidad.sConsignatario = (DReader.IsDBNull(indice) ? "" : DReader.GetString(indice));

                return Entidad;
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public static BE_OrdenServicioDetalle Entidad_ListarOrdenesServicioAtencion(IDataRecord DReader)
        {
            try
            {
                BE_OrdenServicioDetalle Entidad = new BE_OrdenServicioDetalle();

                int indice;
                indice = DReader.GetOrdinal("IdOrdSer");
                Entidad.iIdOrdSer = (DReader.IsDBNull(indice) ? 0 : DReader.GetInt32(indice));
                indice = DReader.GetOrdinal("IdOrdSerDet");
                Entidad.iIdOrdSerDet = (DReader.IsDBNull(indice) ? 0 : DReader.GetInt32(indice));

                indice = DReader.GetOrdinal("IdServicio");
                Entidad.iIdServicio = (DReader.IsDBNull(indice) ? 0 : DReader.GetInt32(indice));
                indice = DReader.GetOrdinal("IdServicioOficinaLineaNegocio");
                Entidad.iIdServicioOficinaLineaNegocio = (DReader.IsDBNull(indice) ? 0 : DReader.GetInt32(indice));
                indice = DReader.GetOrdinal("Servicio");
                Entidad.sDescripcionServicio = (DReader.IsDBNull(indice) ? "" : DReader.GetString(indice));

                indice = DReader.GetOrdinal("IdCliente");
                Entidad.iIdCliente = (DReader.IsDBNull(indice) ? 0 : DReader.GetInt32(indice));
                indice = DReader.GetOrdinal("Cliente");
                Entidad.sCliente = (DReader.IsDBNull(indice) ? "" : DReader.GetString(indice));

                indice = DReader.GetOrdinal("IdDocOriDet");
                Entidad.iIdDocOriDet = (DReader.IsDBNull(indice) ? 0 : DReader.GetInt32(indice));
                indice = DReader.GetOrdinal("Documento");
                Entidad.sNumeroDO = (DReader.IsDBNull(indice) ? "" : DReader.GetString(indice));
                indice = DReader.GetOrdinal("Contenedor");
                Entidad.sContenedor = (DReader.IsDBNull(indice) ? "" : DReader.GetString(indice));
                indice = DReader.GetOrdinal("TamanoCnt");
                Entidad.sDescripcionTamanoCnt = (DReader.IsDBNull(indice) ? "" : DReader.GetString(indice));
                indice = DReader.GetOrdinal("TipoCnt");
                Entidad.sDescripcionTipoCnt = (DReader.IsDBNull(indice) ? "" : DReader.GetString(indice));
                indice = DReader.GetOrdinal("CondicionFacturar");
                Entidad.sCondCarga = (DReader.IsDBNull(indice) ? "" : DReader.GetString(indice));

                indice = DReader.GetOrdinal("IdSituacion");
                Entidad.iIdSituacion = (DReader.IsDBNull(indice) ? 0 : DReader.GetInt32(indice));
                indice = DReader.GetOrdinal("Situacion");
                Entidad.sDescripcionSituacion = (DReader.IsDBNull(indice) ? "" : DReader.GetString(indice));
                indice = DReader.GetOrdinal("OrigenServicio");
                Entidad.sOrigen = (DReader.IsDBNull(indice) ? "" : DReader.GetString(indice));

                indice = DReader.GetOrdinal("IdMoneda");
                Entidad.iIdMoneda = (DReader.IsDBNull(indice) ? 0 : DReader.GetInt32(indice));
                indice = DReader.GetOrdinal("Moneda");
                Entidad.sMoneda = (DReader.IsDBNull(indice) ? "" : DReader.GetString(indice));
                indice = DReader.GetOrdinal("Importe");
                Entidad.dMonto = (DReader.IsDBNull(indice) ? 0 : DReader.GetDecimal(indice));

                indice = DReader.GetOrdinal("UsuarioRegistro");
                Entidad.sUsuarioRegistro = (DReader.IsDBNull(indice) ? "" : DReader.GetString(indice));
                indice = DReader.GetOrdinal("FechaCreacion");
                Entidad.sFechaRegistro = (DReader.IsDBNull(indice) ? "" : DReader.GetString(indice));

                return Entidad;
            }
            catch (Exception e)
            {
                return null;
            }
        }
        #endregion


        public static BE_OrdenServicioDetalle ListarServicioAduanero(IDataReader DReader)
        {
            try
            {
                BE_OrdenServicioDetalle oBE_OrdenServicioDetalle = new BE_OrdenServicioDetalle();

                int indice = 0;

                indice = DReader.GetOrdinal("Codigo");
                if (!DReader.IsDBNull(indice)) oBE_OrdenServicioDetalle.iCodigo = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("Descripcion");
                if (!DReader.IsDBNull(indice)) oBE_OrdenServicioDetalle.sDescripcion = DReader.GetString(indice);

                return oBE_OrdenServicioDetalle;
            }
            catch (Exception e)
            {
                return null;
            }
        }
    }
}
