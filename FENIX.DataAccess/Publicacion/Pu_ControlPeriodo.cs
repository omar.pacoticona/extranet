﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FENIX.BusinessEntity;
using FENIX.Common;
using System.Data;

namespace FENIX.DataAccess
{
    public class Pu_ControlPeriodo
    {
        public static BE_ControlPeriodo Listar(IDataReader DReader)
        {
            try
            {
                BE_ControlPeriodo oBE_ControlPeriodo = new BE_ControlPeriodo();
                int indice = 0;

                indice = DReader.GetOrdinal("IdTipoProceso");
                if (!DReader.IsDBNull(indice)) oBE_ControlPeriodo.iIdTipoProceso = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("ConPer_int_CantidadDias");
                if (!DReader.IsDBNull(indice)) oBE_ControlPeriodo.iCantidadDias = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("ConPer_chr_Estado");
                if (!DReader.IsDBNull(indice)) oBE_ControlPeriodo.sEstado = DReader.GetString(indice);
                indice = DReader.GetOrdinal("ConPer_dt_Fecha");
                if (!DReader.IsDBNull(indice)) oBE_ControlPeriodo.dtFecha = DReader.GetDateTime(indice);

                return oBE_ControlPeriodo;
            }
            catch (Exception e)
            {
                ResultadoTransaccionTx oResultadoTransaccionTx = null;
                oResultadoTransaccionTx.RegistrarError(e);
                return null;
            }
        }

        public static BE_ControlPeriodo ListarLog(IDataReader DReader)
        {
            try
            {
                BE_ControlPeriodo oBE_ControlPeriodo = new BE_ControlPeriodo();
                int indice = 0;

                indice = DReader.GetOrdinal("FechaRegistro");
                if (!DReader.IsDBNull(indice)) oBE_ControlPeriodo.dtFecha = DReader.GetDateTime(indice);
                indice = DReader.GetOrdinal("Usuario");
                if (!DReader.IsDBNull(indice)) oBE_ControlPeriodo.sUsuario = DReader.GetString(indice);
                indice = DReader.GetOrdinal("TipoProceso");
                if (!DReader.IsDBNull(indice)) oBE_ControlPeriodo.sTipoProceso = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Dias");
                if (!DReader.IsDBNull(indice)) oBE_ControlPeriodo.iCantidadDias = DReader.GetInt32(indice);

                return oBE_ControlPeriodo;
            }
            catch (Exception e)
            {
                ResultadoTransaccionTx oResultadoTransaccionTx = null;
                oResultadoTransaccionTx.RegistrarError(e);
                return null;
            }
        }
    }
}
