﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FENIX.BusinessEntity;

namespace FENIX.DataAccess.Publicacion
{
    public class Pu_Refrendo
    {

        public static BE_Refrendo ListarDatosXBooking_Refrendo(IDataRecord DReader)
        {
            BE_Refrendo Entidad = new BE_Refrendo();

            int indice;
            indice = DReader.GetOrdinal("iddocori");
            Entidad.IidDocori = (DReader.IsDBNull(indice) ? 0 : DReader.GetInt32(indice));
            indice = DReader.GetOrdinal("IdBooking");
            Entidad.IdBooking = (DReader.IsDBNull(indice) ? 0 : DReader.GetInt32(indice));
            indice = DReader.GetOrdinal("IdExportador");
            Entidad.IdExportador = (DReader.IsDBNull(indice) ? 0 : DReader.GetInt32(indice));
            indice = DReader.GetOrdinal("Exportador");
            Entidad.sExportador = (DReader.IsDBNull(indice) ? "" : DReader.GetString(indice));
            indice = DReader.GetOrdinal("IdAgenteAduana");
            Entidad.IdAgenciaAguanas = (DReader.IsDBNull(indice) ? 0 : DReader.GetInt32(indice));
            indice = DReader.GetOrdinal("AgenteAduanas");
            Entidad.sAgenciaAduanas = (DReader.IsDBNull(indice) ? "" : DReader.GetString(indice));
            indice = DReader.GetOrdinal("IdNave");
            Entidad.IdNave = (DReader.IsDBNull(indice) ? 0 : DReader.GetInt32(indice));
            indice = DReader.GetOrdinal("Nave");
            Entidad.Nave = (DReader.IsDBNull(indice) ? "" : DReader.GetString(indice));
            indice = DReader.GetOrdinal("Viaje");
            Entidad.sViaje = (DReader.IsDBNull(indice) ? "" : DReader.GetString(indice));
            indice = DReader.GetOrdinal("Rumbo");
            Entidad.sRumbo = (DReader.IsDBNull(indice) ? "" : DReader.GetString(indice));
            indice = DReader.GetOrdinal("CondicionTransmitir");
            Entidad.iIdCondicionTransmitir = (DReader.IsDBNull(indice) ? 0 : DReader.GetInt32(indice));
            indice = DReader.GetOrdinal("NavVia_dt_CargaSeca");
            Entidad.dtCargaSeca = (DReader.IsDBNull(indice) ? "" : DReader.GetString(indice));
            indice = DReader.GetOrdinal("NavVia_dt_CargaRefrigerada");
            Entidad.dtCargaRefrigerada = (DReader.IsDBNull(indice) ? "" : DReader.GetString(indice)); //iIdCondicionCarga
            indice = DReader.GetOrdinal("IdCondicionTransmitir");
            Entidad.iIdCondicionCarga = (DReader.IsDBNull(indice) ? 0 : DReader.GetInt32(indice)); //


            return Entidad;
        }



        
        public static BE_Refrendo ListarArchivosXClienteUsuario(IDataRecord DReader)
        {
            BE_Refrendo Entidad = new BE_Refrendo();

            int indice;
            indice = DReader.GetOrdinal("IdArchivoRefrendo");
            Entidad.iIdArchivoRefrendo = (DReader.IsDBNull(indice) ? 0 : DReader.GetInt32(indice));
            indice = DReader.GetOrdinal("IdTipoDocumento");
            Entidad.iIdTipoDocumento = (DReader.IsDBNull(indice) ? 0 : DReader.GetInt32(indice));
            indice = DReader.GetOrdinal("Documento");
            Entidad.sNombreArchivo = (DReader.IsDBNull(indice) ? "" : DReader.GetString(indice));
            indice = DReader.GetOrdinal("Ruta");
            Entidad.sRutaArchivo = (DReader.IsDBNull(indice) ? "" : DReader.GetString(indice));           

            return Entidad;
        }

        public static BE_Refrendo ListarEstadosRefrendo(IDataRecord DReader)
        {
            BE_Refrendo Entidad = new BE_Refrendo();

            int indice;
            indice = DReader.GetOrdinal("IdValor");
            Entidad.iIdValor = (DReader.IsDBNull(indice) ? 0 : DReader.GetInt32(indice));
            indice = DReader.GetOrdinal("Descripcion");
            Entidad.sValor = (DReader.IsDBNull(indice) ? "" : DReader.GetString(indice));
            

            return Entidad;
        }

        public static BE_Refrendo ListarSolicitudRefrendo(IDataRecord DReader)
        {
            BE_Refrendo Entidad = new BE_Refrendo();

            int indice;
            indice = DReader.GetOrdinal("IdSolicitudRefrendo");
            Entidad.sSolicitudRefr = (DReader.IsDBNull(indice) ? "" : DReader.GetString(indice));
            indice = DReader.GetOrdinal("FechaRegistro");
            Entidad.sMesIni = (DReader.IsDBNull(indice) ? "" : DReader.GetString(indice));
            indice = DReader.GetOrdinal("NumeroDua");
            Entidad.sNroDua = (DReader.IsDBNull(indice) ? "" : DReader.GetString(indice));
            indice = DReader.GetOrdinal("Exportador");
            Entidad.sExportador = (DReader.IsDBNull(indice) ? "" : DReader.GetString(indice));
            indice = DReader.GetOrdinal("IdDocOri");
            Entidad.iIdDocori = (DReader.IsDBNull(indice) ? 0 : DReader.GetInt32(indice));
            indice = DReader.GetOrdinal("Booking");
            Entidad.sBooking = (DReader.IsDBNull(indice) ? "" : DReader.GetString(indice));
            indice = DReader.GetOrdinal("IdSituacion");
            Entidad.sSituacion = (DReader.IsDBNull(indice) ? "" : DReader.GetString(indice));
            indice = DReader.GetOrdinal("Estado");
            Entidad.sNombreSituacion = (DReader.IsDBNull(indice) ? "" : DReader.GetString(indice));
            indice = DReader.GetOrdinal("Canal");
            Entidad.sCanal = (DReader.IsDBNull(indice) ? "" : DReader.GetString(indice));
            indice = DReader.GetOrdinal("Nave");
            Entidad.sNave = (DReader.IsDBNull(indice) ? "" : DReader.GetString(indice));

            return Entidad;
        }

        public static BE_Refrendo ListarDetalleSolicitudRefrendo(IDataRecord DReader)
        {
            BE_Refrendo Entidad = new BE_Refrendo();

            int indice;
            indice = DReader.GetOrdinal("IdSolicitudRefrendoDetalle");
            Entidad.iIdSolicitudRefrDet = (DReader.IsDBNull(indice) ? 0 : DReader.GetInt32(indice));
            indice = DReader.GetOrdinal("NumeroDua");
            Entidad.sNroDua = (DReader.IsDBNull(indice) ? "" : DReader.GetString(indice));
            indice = DReader.GetOrdinal("Booking");
            Entidad.sBooking = (DReader.IsDBNull(indice) ? "" : DReader.GetString(indice));
            indice = DReader.GetOrdinal("NombreArchivo");
            Entidad.sNombreArchivo = (DReader.IsDBNull(indice) ? "" : DReader.GetString(indice));
            indice = DReader.GetOrdinal("Ruta");
            Entidad.sRutaArchivo = (DReader.IsDBNull(indice) ? "" : DReader.GetString(indice));
            indice = DReader.GetOrdinal("IdTipoDocumento");
            Entidad.iIdTipoDocumento = (DReader.IsDBNull(indice) ? 0 : DReader.GetInt32(indice));
            indice = DReader.GetOrdinal("IdSituacion");
            Entidad.sSituacion = (DReader.IsDBNull(indice) ? "" : DReader.GetString(indice));
            indice = DReader.GetOrdinal("Situacion");
            Entidad.sNombreSituacion = (DReader.IsDBNull(indice) ? "" : DReader.GetString(indice));
            indice = DReader.GetOrdinal("NavVia_dt_CargaSeca");
            Entidad.dtCargaSeca = (DReader.IsDBNull(indice) ? "" : DReader.GetString(indice));
            indice = DReader.GetOrdinal("NavVia_dt_CargaRefrigerada");
            Entidad.dtCargaRefrigerada = (DReader.IsDBNull(indice) ? "" : DReader.GetString(indice));
            indice = DReader.GetOrdinal("ComentariosCliente");
            Entidad.sComentarios = (DReader.IsDBNull(indice) ? "" : DReader.GetString(indice));
            indice = DReader.GetOrdinal("ComentariosRevision");
            Entidad.sComentariosUsuario = (DReader.IsDBNull(indice) ? "" : DReader.GetString(indice));
            indice = DReader.GetOrdinal("RucFacturar");
            Entidad.sRuc = (DReader.IsDBNull(indice) ? "" : DReader.GetString(indice));
            indice = DReader.GetOrdinal("ClienteFacturar");
            Entidad.sClienteFacturar = (DReader.IsDBNull(indice) ? "" : DReader.GetString(indice));
            indice = DReader.GetOrdinal("DniDespachador");
            Entidad.sDniDesp = (DReader.IsDBNull(indice) ? "" : DReader.GetString(indice));
            indice = DReader.GetOrdinal("NombreDespachador");
            Entidad.sNombreDesp = (DReader.IsDBNull(indice) ? "" : DReader.GetString(indice));

            return Entidad;
        }

        public static BE_Refrendo ListarDetalleBultosPrecintosIngresadosCliente(IDataRecord DReader)
        {
            BE_Refrendo Entidad = new BE_Refrendo();

            int indice;
            indice = DReader.GetOrdinal("IdDocoriDet");
            Entidad.iIdDocOridet = (DReader.IsDBNull(indice) ? 0 : DReader.GetInt32(indice));
            indice = DReader.GetOrdinal("Contenedor");
            Entidad.sContenedor = (DReader.IsDBNull(indice) ? "" : DReader.GetString(indice));
            indice = DReader.GetOrdinal("Tipo");
            Entidad.sTipoContenedor = (DReader.IsDBNull(indice) ? "" : DReader.GetString(indice));
            indice = DReader.GetOrdinal("Tamano");
            Entidad.sDimension = (DReader.IsDBNull(indice) ? "" : DReader.GetString(indice));
            indice = DReader.GetOrdinal("Bultos");
            Entidad.sBultos = (DReader.IsDBNull(indice) ? "" : DReader.GetString(indice));
            indice = DReader.GetOrdinal("Det_vch_PrecintoAduanas");
            Entidad.sPrecintoAduanas = (DReader.IsDBNull(indice) ? "" : DReader.GetString(indice));
            indice = DReader.GetOrdinal("PrecintoLinea");
            Entidad.sPrecintoLinea = (DReader.IsDBNull(indice) ? "" : DReader.GetString(indice));
          
            return Entidad;
        }

        public static BE_Refrendo ListarDetalleDocumento(IDataRecord DReader)
        {
            BE_Refrendo Entidad = new BE_Refrendo();

            int indice;
            indice = DReader.GetOrdinal("IdDocoriDet");
            Entidad.iIdDocOridet = (DReader.IsDBNull(indice) ? 0 : DReader.GetInt32(indice));
            indice = DReader.GetOrdinal("Contenedor");
            Entidad.sContenedor = (DReader.IsDBNull(indice) ? "" : DReader.GetString(indice));
            indice = DReader.GetOrdinal("Tipo");
            Entidad.sTipoContenedor = (DReader.IsDBNull(indice) ? "" : DReader.GetString(indice));
            indice = DReader.GetOrdinal("Tamano");
            Entidad.sDimension = (DReader.IsDBNull(indice) ? "" : DReader.GetString(indice));
            indice = DReader.GetOrdinal("Tara");
            Entidad.dTara = (DReader.IsDBNull(indice) ? 0 : DReader.GetDecimal(indice));
            indice = DReader.GetOrdinal("PesoNeto");
            Entidad.dPesoNeto = (DReader.IsDBNull(indice) ? 0 : DReader.GetDecimal(indice));
            indice = DReader.GetOrdinal("Bultos");
            Entidad.sBultos = (DReader.IsDBNull(indice) ? "" : DReader.GetString(indice));
            indice = DReader.GetOrdinal("PrecintoAduana");
            Entidad.sPrecintoAduanas = (DReader.IsDBNull(indice) ? "" : DReader.GetString(indice));
            indice = DReader.GetOrdinal("Situacion");
            Entidad.sSituacion = (DReader.IsDBNull(indice) ? "" : DReader.GetString(indice));
            indice = DReader.GetOrdinal("Conexion");
            Entidad.iIConexionCarga = (DReader.IsDBNull(indice) ? 0 : DReader.GetInt32(indice));

            return Entidad;
        }

        public static BE_Refrendo ListarDespachadores(IDataRecord DReader)
        {
            BE_Refrendo Entidad = new BE_Refrendo();

            int indice;
            indice = DReader.GetOrdinal("IdDespachador");
            Entidad.iIdValor = (DReader.IsDBNull(indice) ? 0 : DReader.GetInt32(indice));
            indice = DReader.GetOrdinal("DescripcionDespachador");
            Entidad.sValor = (DReader.IsDBNull(indice) ? "" : DReader.GetString(indice));
            

            return Entidad;
        }

        public static BE_Refrendo ListarREL_Ruc(IDataReader DReader)
        {
            //oResultadoTransaccionTx oResultadoTransaccionTx = null;
            try
            {
                BE_Refrendo oBE_Refrendo = new BE_Refrendo();
                int indice = 0;

                indice = DReader.GetOrdinal("IdCliente");
                if (!DReader.IsDBNull(indice)) oBE_Refrendo.IdCliente = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("Client_vch_NumDocumento");
                if (!DReader.IsDBNull(indice)) oBE_Refrendo.sRuc = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Client_vch_Razon_Social");
                if (!DReader.IsDBNull(indice)) oBE_Refrendo.sRazSocial = DReader.GetString(indice);
                return oBE_Refrendo;
            }
            catch (Exception e)
            {
               // oResultadoTransaccionTx.RegistrarError(e);
                return null;
            }

        }




    }
}
