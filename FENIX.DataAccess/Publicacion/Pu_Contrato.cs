﻿using System;
using System.Collections.Generic;
using System.Text;
using FENIX.BusinessEntity;
using FENIX.DataAccess;
using System.Data;
using FENIX.Common;

namespace FENIX.DataAccess
{
    public class Pu_Contrato
    {
        public static BE_Contrato Listar(IDataReader DReader)
        {
            try
            {
                BE_Contrato oBE_Contrato = new BE_Contrato();
                int indice = 0;

                indice = DReader.GetOrdinal("IdContrato");
                if (!DReader.IsDBNull(indice)) oBE_Contrato.sIdContrato = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Cliente_Razon_Social");
                if (!DReader.IsDBNull(indice)) oBE_Contrato.sDescripcionCliente = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Cliente_Rol");
                if (!DReader.IsDBNull(indice)) oBE_Contrato.sDescripcionClienteRol = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Socio_Razon_Social");
                if (!DReader.IsDBNull(indice)) oBE_Contrato.sDescripcionSocio = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Socio_Rol");
                if (!DReader.IsDBNull(indice)) oBE_Contrato.sDescripcionSocioRol = DReader.GetString(indice);
                indice = DReader.GetOrdinal("TipoContrato");
                if (!DReader.IsDBNull(indice)) oBE_Contrato.sDescripcionTipoContrato = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Vendedor");
                if (!DReader.IsDBNull(indice)) oBE_Contrato.sDescripcionVendedor = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Con_Dt_FechaInicio");
                if (!DReader.IsDBNull(indice)) oBE_Contrato.sFechaInicio = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Con_Dt_FechaFin");
                if (!DReader.IsDBNull(indice)) oBE_Contrato.sFechaFin = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Situacion");
                if (!DReader.IsDBNull(indice)) oBE_Contrato.sSituacion = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Con_Vch_Observacion");
                if (!DReader.IsDBNull(indice)) oBE_Contrato.sObservacion = DReader.GetString(indice);
                indice = DReader.GetOrdinal("NroContrato");
                if (!DReader.IsDBNull(indice)) oBE_Contrato.sNroContrato = DReader.GetString(indice);
                indice = DReader.GetOrdinal("LineaMaritima");
                if (!DReader.IsDBNull(indice)) oBE_Contrato.sLineaMaritima = DReader.GetString(indice);


                return oBE_Contrato;
            }
            catch (Exception e)
            {
                ResultadoTransaccionTx oResultadoTransaccionTx = null;
                oResultadoTransaccionTx.RegistrarError(e);
                return null;
            }
        }

        public static BE_Tarifa ListarDetalle(IDataReader DReader)
        {
            try
            {
                BE_Tarifa oBE_Tarifa = new BE_Tarifa();
                int indice = 0;

                indice = DReader.GetOrdinal("IdConDet");
                if (!DReader.IsDBNull(indice)) oBE_Tarifa.iIdConDet = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("IdTarifa");
                if (!DReader.IsDBNull(indice)) oBE_Tarifa.iIdTarifa = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("Tar_Vch_Descripcion");
                if (!DReader.IsDBNull(indice)) oBE_Tarifa.sDescripcion = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Ser_Vch_Descripcion");
                if (!DReader.IsDBNull(indice)) oBE_Tarifa.sDescripcionServicio = DReader.GetString(indice);
                indice = DReader.GetOrdinal("TCondicionCarga");
                if (!DReader.IsDBNull(indice)) oBE_Tarifa.sDescripcionTCondicionCarga = DReader.GetString(indice);
                indice = DReader.GetOrdinal("TTamanoCnt");
                if (!DReader.IsDBNull(indice)) oBE_Tarifa.sDescripcionTTamanoCnt = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Tar_Chr_FlagMandatorio");
                if (!DReader.IsDBNull(indice)) oBE_Tarifa.sFlagMandatorio = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Tar_Num_Monto");
                if (!DReader.IsDBNull(indice)) oBE_Tarifa.dMonto = DReader.GetDecimal(indice);

                indice = DReader.GetOrdinal("DiasLibres");
                if (!DReader.IsDBNull(indice)) oBE_Tarifa.iDiasLibresAlmacenaje = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("Moneda");
                if (!DReader.IsDBNull(indice)) oBE_Tarifa.sMoneda = DReader.GetString(indice);
                indice = DReader.GetOrdinal("TEmbalaje");
                if (!DReader.IsDBNull(indice)) oBE_Tarifa.sEmbalaje = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Costo");
                if (!DReader.IsDBNull(indice)) oBE_Tarifa.dCosto = DReader.GetDecimal(indice);
                indice = DReader.GetOrdinal("Modalidad");
                if (!DReader.IsDBNull(indice)) oBE_Tarifa.sModalidad = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Tar_Chr_FlagLimite");
                if (!DReader.IsDBNull(indice)) oBE_Tarifa.sFlagLimite = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Tar_Num_MontoMaximo");
                if (!DReader.IsDBNull(indice)) oBE_Tarifa.dMontoMaximo = DReader.GetDecimal(indice);
                indice = DReader.GetOrdinal("Tar_Num_MontoMinimo");
                if (!DReader.IsDBNull(indice)) oBE_Tarifa.dMontoMinimo = DReader.GetDecimal(indice);
                indice = DReader.GetOrdinal("IdMoneda");
                if (!DReader.IsDBNull(indice)) oBE_Tarifa.iIdMoneda = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("IdModalidad");
                if (!DReader.IsDBNull(indice)) oBE_Tarifa.iIdModalidad = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("IdTarifaOrigen");
                if (!DReader.IsDBNull(indice)) oBE_Tarifa.iIdTarifaOrigen = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("Retroactivo");
                if (!DReader.IsDBNull(indice)) oBE_Tarifa.sFlagAlmacenajeRetroactivo = DReader.GetString(indice);
                indice = DReader.GetOrdinal("DiasLibAlmaPeli");
                if (!DReader.IsDBNull(indice)) oBE_Tarifa.sDiasLibPeligrosa = DReader.GetString(indice);
                indice = DReader.GetOrdinal("idAsume");
                if (!DReader.IsDBNull(indice)) oBE_Tarifa.iIdAsume = DReader.GetInt32(indice);


                return oBE_Tarifa;
            }
            catch (Exception e)
            {
                ResultadoTransaccionTx oResultadoTransaccionTx = null;
                oResultadoTransaccionTx.RegistrarError(e);
                return null;
            }
        }

        public static BE_Contrato ListarPorId(IDataReader DReader)
        {
            try
            {
                BE_Contrato oBE_Contrato = new BE_Contrato();
                int indice = 0;

                indice = DReader.GetOrdinal("IdContrato");
                if (!DReader.IsDBNull(indice)) oBE_Contrato.sIdContrato = DReader.GetString(indice);
                indice = DReader.GetOrdinal("IdVendedor");
                if (!DReader.IsDBNull(indice)) oBE_Contrato.iIdVendedor = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("IdTipoContrato");
                if (!DReader.IsDBNull(indice)) oBE_Contrato.sIdTipoContrato = DReader.GetString(indice);
                indice = DReader.GetOrdinal("IdRolCliente");
                if (!DReader.IsDBNull(indice)) oBE_Contrato.iIdRolCliente = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("IdSocio");
                if (!DReader.IsDBNull(indice)) oBE_Contrato.iIdSocio = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("IdRolSocio");
                if (!DReader.IsDBNull(indice)) oBE_Contrato.iIdRolSocio = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("IdTipoOperacion");
                if (!DReader.IsDBNull(indice)) oBE_Contrato.iIdTipoOperacion = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("IdCliente");
                if (!DReader.IsDBNull(indice)) oBE_Contrato.iIdCliente = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("IdLineaNegocio");
                if (!DReader.IsDBNull(indice)) oBE_Contrato.sIdLineaNegocio = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Cliente_Razon_Social");
                if (!DReader.IsDBNull(indice)) oBE_Contrato.sDescripcionCliente = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Cliente_Rol");
                if (!DReader.IsDBNull(indice)) oBE_Contrato.sDescripcionClienteRol = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Socio_Razon_Social");
                if (!DReader.IsDBNull(indice)) oBE_Contrato.sDescripcionSocio = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Socio_Rol");
                if (!DReader.IsDBNull(indice)) oBE_Contrato.sDescripcionSocioRol = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Vendedor");
                if (!DReader.IsDBNull(indice)) oBE_Contrato.sDescripcionVendedor = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Con_Dt_FechaInicio");
                if (!DReader.IsDBNull(indice)) oBE_Contrato.sFechaInicio = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Con_Dt_FechaFin");
                if (!DReader.IsDBNull(indice)) oBE_Contrato.sFechaFin = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Situacion");
                if (!DReader.IsDBNull(indice)) oBE_Contrato.sSituacion = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Con_Vch_Observacion");
                if (!DReader.IsDBNull(indice)) oBE_Contrato.sObservacion = DReader.GetString(indice);
                indice = DReader.GetOrdinal("IdLineaMaritima");
                if (!DReader.IsDBNull(indice)) oBE_Contrato.iIdLineaMartima = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("LinMar_vch_Descripcion");
                if (!DReader.IsDBNull(indice)) oBE_Contrato.sLineaMaritima = DReader.GetString(indice);
                indice = DReader.GetOrdinal("NroContrato");
                if (!DReader.IsDBNull(indice)) oBE_Contrato.sNroContrato = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Comisionista");
                if (!DReader.IsDBNull(indice)) oBE_Contrato.sDescripcionComisionista = DReader.GetString(indice);
                return oBE_Contrato;
            }
            catch (Exception e)
            {
                ResultadoTransaccionTx oResultadoTransaccionTx = null;
                oResultadoTransaccionTx.RegistrarError(e);
                return null;
            }
        }

        //public static BE_DireccionamientoContrato ListarDireccionamientoCnt(IDataReader DReader)
        //{
        //    try
        //    {
        //        BE_DireccionamientoContrato oBE_DireccionamientoContrato = new BE_DireccionamientoContrato();
        //        int indice = 0;

        //        indice = DReader.GetOrdinal("IdDocOri");
        //        if (!DReader.IsDBNull(indice)) oBE_DireccionamientoContrato.iIdDocOri = DReader.GetInt32(indice);
        //        indice = DReader.GetOrdinal("idContrato");
        //        if (!DReader.IsDBNull(indice)) oBE_DireccionamientoContrato.idContrato = DReader.GetInt32(indice);
        //        indice = DReader.GetOrdinal("Contrato");
        //        if (!DReader.IsDBNull(indice)) oBE_DireccionamientoContrato.sContrato = DReader.GetString(indice);
        //        indice = DReader.GetOrdinal("Cliente_Consignatario");
        //        if (!DReader.IsDBNull(indice)) oBE_DireccionamientoContrato.sRazon_Social = DReader.GetString(indice);
        //        indice = DReader.GetOrdinal("Cliente_Direccionante");
        //        if (!DReader.IsDBNull(indice)) oBE_DireccionamientoContrato.sDireccionante = DReader.GetString(indice);
        //        indice = DReader.GetOrdinal("Vendedor");
        //        if (!DReader.IsDBNull(indice)) oBE_DireccionamientoContrato.sVendedor = DReader.GetString(indice);
        //        indice = DReader.GetOrdinal("vol_numero");
        //        if (!DReader.IsDBNull(indice)) oBE_DireccionamientoContrato.sVolante = DReader.GetString(indice);
        //        indice = DReader.GetOrdinal("Cliente_Consolidador");
        //        if (!DReader.IsDBNull(indice)) oBE_DireccionamientoContrato.sConsolidador = DReader.GetString(indice);
        //        indice = DReader.GetOrdinal("TipoDireccionamiento");
        //        if (!DReader.IsDBNull(indice)) oBE_DireccionamientoContrato.sDireccionamiento = DReader.GetString(indice);
        //        indice = DReader.GetOrdinal("IdVolante");
        //        if (!DReader.IsDBNull(indice)) oBE_DireccionamientoContrato.iIdVolante = DReader.GetInt32(indice);
        //        indice = DReader.GetOrdinal("NumeroDO");
        //        if (!DReader.IsDBNull(indice)) oBE_DireccionamientoContrato.sNumeroDO = DReader.GetString(indice);
        //        indice = DReader.GetOrdinal("NumeroDOMaster");
        //        if (!DReader.IsDBNull(indice)) oBE_DireccionamientoContrato.sNumeroDOMadre = DReader.GetString(indice);



        //        return oBE_DireccionamientoContrato;
        //    }
        //    catch (Exception e)
        //    {
        //        ResultadoTransaccionTx oResultadoTransaccionTx = null;
        //        oResultadoTransaccionTx.RegistrarError(e);
        //        return null;
        //    }
        //}

        public static BE_Contrato ListarContratoReasignacion(IDataReader DReader)
        {
            try
            {
                BE_Contrato oBE_Contrato = new BE_Contrato();
                int indice = 0;

                indice = DReader.GetOrdinal("IdContrato");
                if (!DReader.IsDBNull(indice)) oBE_Contrato.sIdContrato = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Cliente_Razon_Social");
                if (!DReader.IsDBNull(indice)) oBE_Contrato.sDescripcionCliente = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Socio_Razon_Social");
                if (!DReader.IsDBNull(indice)) oBE_Contrato.sDescripcionSocio = DReader.GetString(indice);
                indice = DReader.GetOrdinal("TipoContrato");
                if (!DReader.IsDBNull(indice)) oBE_Contrato.sIdTipoContrato = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Vendedor");
                if (!DReader.IsDBNull(indice)) oBE_Contrato.sDescripcionVendedor = DReader.GetString(indice);
                indice = DReader.GetOrdinal("TLineaNegocio");
                if (!DReader.IsDBNull(indice)) oBE_Contrato.sIdLineaNegocio = DReader.GetString(indice);

                return oBE_Contrato;

            }
            catch (Exception e)
            {
                ResultadoTransaccionTx oResultadoTransaccionTx = null;
                oResultadoTransaccionTx.RegistrarError(e);
                return null;
            }
        }

        //public static BE_DireccionamientoContrato ListarHistoricoReasignacion(IDataReader DReader)
        //{
        //    try
        //    {
        //        BE_DireccionamientoContrato oBE_DireccionamientoContrato = new BE_DireccionamientoContrato();
        //        int indice = 0;
        //        indice = DReader.GetOrdinal("IdRegistro");
        //        if (!DReader.IsDBNull(indice)) oBE_DireccionamientoContrato.IdRegistro = DReader.GetInt32(indice);
        //        indice = DReader.GetOrdinal("IdContrato");
        //        if (!DReader.IsDBNull(indice)) oBE_DireccionamientoContrato.idContrato = DReader.GetInt32(indice);
        //        indice = DReader.GetOrdinal("IdUsuarioRegistro");
        //        if (!DReader.IsDBNull(indice)) oBE_DireccionamientoContrato.UsuarioRegistro = DReader.GetString(indice);
        //        indice = DReader.GetOrdinal("His_FechaRegistro");
        //        if (!DReader.IsDBNull(indice)) oBE_DireccionamientoContrato.dtFechaHistorico = DReader.GetDateTime(indice);
        //        indice = DReader.GetOrdinal("VendedorAnterior");
        //        if (!DReader.IsDBNull(indice)) oBE_DireccionamientoContrato.VendedorAnterior = DReader.GetString(indice);
        //        indice = DReader.GetOrdinal("VendedorActual");
        //        if (!DReader.IsDBNull(indice)) oBE_DireccionamientoContrato.VendedorActual = DReader.GetString(indice);

        //        return oBE_DireccionamientoContrato;

        //    }
        //    catch (Exception e)
        //    {
        //        ResultadoTransaccionTx oResultadoTransaccionTx = null;
        //        oResultadoTransaccionTx.RegistrarError(e);
        //        return null;
        //    }
        //}

    }

}
