﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using FENIX.BusinessEntity;


namespace FENIX.DataAccess
{
    public class Pu_Costos
    {
        
        
        public static BE_Costos ListarCliente(IDataReader DReader)
        {
            try
            {
                BE_Costos oBE_Costos = new BE_Costos();
                int indice = 0;
                indice = DReader.GetOrdinal("IdCliente");
                if (!DReader.IsDBNull(indice)) oBE_Costos.iIdCliente = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("Cliente");
                if (!DReader.IsDBNull(indice)) oBE_Costos.sCliente = DReader.GetString(indice);

                return oBE_Costos;
            }
            catch (Exception)
            {
                return null;
            }
        }


        //public static BE_Presupuesto ListarVtasXTipoCli(IDataReader DReader)
        //{
        //    try
        //    {
        //        BE_Presupuesto oBE_Presupuesto = new BE_Presupuesto();
        //        int indice = 0;
        //        indice = DReader.GetOrdinal("Mes");
        //        if (!DReader.IsDBNull(indice)) oBE_Presupuesto.iMes = DReader.GetInt32(indice);
        //        indice = DReader.GetOrdinal("DescMes");
        //        if (!DReader.IsDBNull(indice)) oBE_Presupuesto.sMes = DReader.GetString(indice);
        //        indice = DReader.GetOrdinal("VtaGrupo");
        //        if (!DReader.IsDBNull(indice)) oBE_Presupuesto.dVentas = DReader.GetDecimal(indice);
        //        indice = DReader.GetOrdinal("PorcGrupo");
        //        if (!DReader.IsDBNull(indice)) oBE_Presupuesto.iMes1 = DReader.GetInt32(indice);
        //        indice = DReader.GetOrdinal("VtaTerceros");
        //        if (!DReader.IsDBNull(indice)) oBE_Presupuesto.dVentasAnt = DReader.GetDecimal(indice);
        //        indice = DReader.GetOrdinal("PorcTerceros");
        //        if (!DReader.IsDBNull(indice)) oBE_Presupuesto.iMes2 = DReader.GetInt32(indice);
        //        indice = DReader.GetOrdinal("PorcTotal");
        //        if (!DReader.IsDBNull(indice)) oBE_Presupuesto.iMes3 = DReader.GetInt32(indice);


        //        return oBE_Presupuesto;
        //    }
        //    catch (Exception)
        //    {
        //        return null;
        //    }
        //}


        //public static BE_Presupuesto ListarVtasXTipoCarga(IDataReader DReader)
        //{
        //    try
        //    {
        //        BE_Presupuesto oBE_Presupuesto = new BE_Presupuesto();
        //        int indice = 0;
        //        indice = DReader.GetOrdinal("NroItem");
        //        if (!DReader.IsDBNull(indice)) oBE_Presupuesto.iItem = DReader.GetInt32(indice);
        //        indice = DReader.GetOrdinal("IdCliente");
        //        if (!DReader.IsDBNull(indice)) oBE_Presupuesto.iIdCliente = DReader.GetInt32(indice);
        //        indice = DReader.GetOrdinal("cliente");
        //        if (!DReader.IsDBNull(indice)) oBE_Presupuesto.sCliente = DReader.GetString(indice);
        //        indice = DReader.GetOrdinal("Cantidad");
        //        if (!DReader.IsDBNull(indice)) oBE_Presupuesto.iMes1 = DReader.GetInt32(indice);
        //        indice = DReader.GetOrdinal("CantidadAnt");
        //        if (!DReader.IsDBNull(indice)) oBE_Presupuesto.iMes2 = DReader.GetInt32(indice);
        //        indice = DReader.GetOrdinal("Porc");
        //        if (!DReader.IsDBNull(indice)) oBE_Presupuesto.dPorc = DReader.GetDecimal(indice);


        //        return oBE_Presupuesto;
        //    }
        //    catch (Exception)
        //    {
        //        return null;
        //    }
        //}


        public static BE_Costos ListarVtasXPeriodo(IDataReader DReader)
        {
            try
            {
                BE_Costos oBE_Costos = new BE_Costos();
                int indice = 0;
                indice = DReader.GetOrdinal("Mes");
                if (!DReader.IsDBNull(indice)) oBE_Costos.iMes = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("DescMes");
                if (!DReader.IsDBNull(indice)) oBE_Costos.sMes = DReader.GetString(indice);
                indice = DReader.GetOrdinal("CNT");
                if (!DReader.IsDBNull(indice)) oBE_Costos.iTotalCnt = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("Peso");
                if (!DReader.IsDBNull(indice)) oBE_Costos.iTotalTn = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("CostoOpe");
                if (!DReader.IsDBNull(indice)) oBE_Costos.dCostoOpe = DReader.GetDecimal(indice);
                indice = DReader.GetOrdinal("CostoAdm");
                if (!DReader.IsDBNull(indice)) oBE_Costos.dCostoAdm = DReader.GetDecimal(indice);
                indice = DReader.GetOrdinal("Ventas");
                if (!DReader.IsDBNull(indice)) oBE_Costos.dVentas = DReader.GetDecimal(indice);
                indice = DReader.GetOrdinal("Utilidad");
                if (!DReader.IsDBNull(indice)) oBE_Costos.dUtilidad = DReader.GetDecimal(indice);
                indice = DReader.GetOrdinal("VentaEsperada");
                if (!DReader.IsDBNull(indice)) oBE_Costos.dVtaEsperada = DReader.GetDecimal(indice);
                
                
                return oBE_Costos;
            }
            catch (Exception)
            {
                return null;
            }
        }


        public static BE_Costos ListarVtasXLinea(IDataReader DReader)
        {
            try
            {
                BE_Costos oBE_Costos = new BE_Costos();
                int indice = 0;
                indice = DReader.GetOrdinal("IdLinea");
                if (!DReader.IsDBNull(indice)) oBE_Costos.iIdLineaNegocio = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("LineaNegocio");
                if (!DReader.IsDBNull(indice)) oBE_Costos.sNegocio = DReader.GetString(indice);
                indice = DReader.GetOrdinal("CostoOpe");
                if (!DReader.IsDBNull(indice)) oBE_Costos.dCostoOpe = DReader.GetDecimal(indice);
                indice = DReader.GetOrdinal("CostoAdm");
                if (!DReader.IsDBNull(indice)) oBE_Costos.dCostoAdm = DReader.GetDecimal(indice);
                indice = DReader.GetOrdinal("Ventas");
                if (!DReader.IsDBNull(indice)) oBE_Costos.dVentas = DReader.GetDecimal(indice);
                indice = DReader.GetOrdinal("Utilidad");
                if (!DReader.IsDBNull(indice)) oBE_Costos.dUtilidad = DReader.GetDecimal(indice);
                indice = DReader.GetOrdinal("VentaEsperada");
                if (!DReader.IsDBNull(indice)) oBE_Costos.dVtaEsperada = DReader.GetDecimal(indice);

                return oBE_Costos;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public static BE_Costos ListarVtasXCliente(IDataReader DReader)
        {
            try
            {
                BE_Costos oBE_Costos = new BE_Costos();
                int indice = 0;
                indice = DReader.GetOrdinal("NroItem");
                if (!DReader.IsDBNull(indice)) oBE_Costos.iItem = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("IdCliente");
                if (!DReader.IsDBNull(indice)) oBE_Costos.iIdCliente = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("Cliente");
                if (!DReader.IsDBNull(indice)) oBE_Costos.sCliente = DReader.GetString(indice);
                indice = DReader.GetOrdinal("CostoOpe");
                if (!DReader.IsDBNull(indice)) oBE_Costos.dCostoOpe = DReader.GetDecimal(indice);
                indice = DReader.GetOrdinal("CostoAdm");
                if (!DReader.IsDBNull(indice)) oBE_Costos.dCostoAdm = DReader.GetDecimal(indice);
                indice = DReader.GetOrdinal("Ventas");
                if (!DReader.IsDBNull(indice)) oBE_Costos.dVentas = DReader.GetDecimal(indice);
                indice = DReader.GetOrdinal("Utilidad");
                if (!DReader.IsDBNull(indice)) oBE_Costos.dUtilidad = DReader.GetDecimal(indice);
                indice = DReader.GetOrdinal("VentaEsperada");
                if (!DReader.IsDBNull(indice)) oBE_Costos.dVtaEsperada = DReader.GetDecimal(indice);
                indice = DReader.GetOrdinal("CNT");
                if (!DReader.IsDBNull(indice)) oBE_Costos.iTotalCnt = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("Peso");
                if (!DReader.IsDBNull(indice)) oBE_Costos.iTotalTn = DReader.GetInt32(indice);
                

                return oBE_Costos;
            }
            catch (Exception)
            {
                return null;
            }
        }




        public static BE_Costos ListarVtasXServicio(IDataReader DReader)
        {
            try
            {
                BE_Costos oBE_Costos = new BE_Costos();
                int indice = 0;
                indice = DReader.GetOrdinal("NroItem");
                if (!DReader.IsDBNull(indice)) oBE_Costos.iItem = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("IdGrupo");
                if (!DReader.IsDBNull(indice)) oBE_Costos.iIdGrupo = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("IdServicio");
                if (!DReader.IsDBNull(indice)) oBE_Costos.iIdServicio = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("Servicio");
                if (!DReader.IsDBNull(indice)) oBE_Costos.sServicio = DReader.GetString(indice);
                indice = DReader.GetOrdinal("CostoOpe");
                if (!DReader.IsDBNull(indice)) oBE_Costos.dCostoOpe = DReader.GetDecimal(indice);
                indice = DReader.GetOrdinal("CostoAdm");
                if (!DReader.IsDBNull(indice)) oBE_Costos.dCostoAdm = DReader.GetDecimal(indice);
                indice = DReader.GetOrdinal("Ventas");
                if (!DReader.IsDBNull(indice)) oBE_Costos.dVentas = DReader.GetDecimal(indice);
                indice = DReader.GetOrdinal("Utilidad");
                if (!DReader.IsDBNull(indice)) oBE_Costos.dUtilidad = DReader.GetDecimal(indice);
                indice = DReader.GetOrdinal("VentaEsperada");
                if (!DReader.IsDBNull(indice)) oBE_Costos.dVtaEsperada = DReader.GetDecimal(indice);

                return oBE_Costos;
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}
