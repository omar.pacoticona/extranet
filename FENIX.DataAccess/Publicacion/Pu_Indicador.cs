﻿using System;
using System.Collections.Generic;
using System.Text;
using FENIX.BusinessEntity;
using System.Data;
using FENIX.Common;


namespace FENIX.DataAccess
{
    public class Pu_Indicador
    {

        public static BE_Indicador Listar(IDataReader DReader)
        {
            try
            {
                BE_Indicador oBE_Indicador = new BE_Indicador();

                int indice = 0;

                indice = DReader.GetOrdinal("IdIndicador");
                if (!DReader.IsDBNull(indice)) oBE_Indicador.iIdIndicador = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("NombreIndicador");
                if (!DReader.IsDBNull(indice)) oBE_Indicador.sNombreIndicador = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Valor");
                if (!DReader.IsDBNull(indice)) oBE_Indicador.sValor = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Objetivo");
                if (!DReader.IsDBNull(indice)) oBE_Indicador.sObjetivo = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Cumplimiento");
                if (!DReader.IsDBNull(indice)) oBE_Indicador.sCumplimiento = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Estado");
                if (!DReader.IsDBNull(indice)) oBE_Indicador.sEstado = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Propietario");
                if (!DReader.IsDBNull(indice)) oBE_Indicador.sPropietario = DReader.GetString(indice);
                indice = DReader.GetOrdinal("VerDetalle");
                if (!DReader.IsDBNull(indice)) oBE_Indicador.sVerDetalle = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Formula");
                if (!DReader.IsDBNull(indice)) oBE_Indicador.sFormula = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Color");
                if (!DReader.IsDBNull(indice)) oBE_Indicador.sColor = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Tooltip");
                if (!DReader.IsDBNull(indice)) oBE_Indicador.sTooltip = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Complemento");
                if (!DReader.IsDBNull(indice)) oBE_Indicador.sComplemento = DReader.GetString(indice);

                return oBE_Indicador;
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public static BE_Indicador ListarAnual(IDataReader DReader)
        {
            try
            {
                BE_Indicador oBE_Indicador = new BE_Indicador();

                int indice = 0;

                indice = DReader.GetOrdinal("IdIndicador");
                if (!DReader.IsDBNull(indice)) oBE_Indicador.iIdIndicador = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("NombreIndicador");
                if (!DReader.IsDBNull(indice)) oBE_Indicador.sNombreIndicador = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Propietario");
                if (!DReader.IsDBNull(indice)) oBE_Indicador.sPropietario = DReader.GetString(indice);

                indice = DReader.GetOrdinal("Objetivo");
                if (!DReader.IsDBNull(indice)) oBE_Indicador.sObjetivo = DReader.GetString(indice);

                indice = DReader.GetOrdinal("Mes1");
                if (!DReader.IsDBNull(indice)) oBE_Indicador.sMes1 = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Mes2");
                if (!DReader.IsDBNull(indice)) oBE_Indicador.sMes2 = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Mes3");
                if (!DReader.IsDBNull(indice)) oBE_Indicador.sMes3 = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Mes4");
                if (!DReader.IsDBNull(indice)) oBE_Indicador.sMes4 = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Mes5");
                if (!DReader.IsDBNull(indice)) oBE_Indicador.sMes5 = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Mes6");
                if (!DReader.IsDBNull(indice)) oBE_Indicador.sMes6 = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Mes7"); 
                if (!DReader.IsDBNull(indice)) oBE_Indicador.sMes7 = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Mes8");
                if (!DReader.IsDBNull(indice)) oBE_Indicador.sMes8 = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Mes9");
                if (!DReader.IsDBNull(indice)) oBE_Indicador.sMes9 = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Mes10");
                if (!DReader.IsDBNull(indice)) oBE_Indicador.sMes10 = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Mes11");
                if (!DReader.IsDBNull(indice)) oBE_Indicador.sMes11 = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Mes12");
                if (!DReader.IsDBNull(indice)) oBE_Indicador.sMes12 = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Mes13");
                if (!DReader.IsDBNull(indice)) oBE_Indicador.sMes13 = DReader.GetString(indice);

                indice = DReader.GetOrdinal("ObjetivoMes1");
                if (!DReader.IsDBNull(indice)) oBE_Indicador.sObjetivoMes1 = DReader.GetString(indice);
                indice = DReader.GetOrdinal("ObjetivoMes2");
                if (!DReader.IsDBNull(indice)) oBE_Indicador.sObjetivoMes2 = DReader.GetString(indice);
                indice = DReader.GetOrdinal("ObjetivoMes3");
                if (!DReader.IsDBNull(indice)) oBE_Indicador.sObjetivoMes3 = DReader.GetString(indice);
                indice = DReader.GetOrdinal("ObjetivoMes4");
                if (!DReader.IsDBNull(indice)) oBE_Indicador.sObjetivoMes4 = DReader.GetString(indice);
                indice = DReader.GetOrdinal("ObjetivoMes5");
                if (!DReader.IsDBNull(indice)) oBE_Indicador.sObjetivoMes5 = DReader.GetString(indice);
                indice = DReader.GetOrdinal("ObjetivoMes6");
                if (!DReader.IsDBNull(indice)) oBE_Indicador.sObjetivoMes6 = DReader.GetString(indice);
                indice = DReader.GetOrdinal("ObjetivoMes7");
                if (!DReader.IsDBNull(indice)) oBE_Indicador.sObjetivoMes7 = DReader.GetString(indice);
                indice = DReader.GetOrdinal("ObjetivoMes8");
                if (!DReader.IsDBNull(indice)) oBE_Indicador.sObjetivoMes8 = DReader.GetString(indice);
                indice = DReader.GetOrdinal("ObjetivoMes9");
                if (!DReader.IsDBNull(indice)) oBE_Indicador.sObjetivoMes9 = DReader.GetString(indice);
                indice = DReader.GetOrdinal("ObjetivoMes10");
                if (!DReader.IsDBNull(indice)) oBE_Indicador.sObjetivoMes10 = DReader.GetString(indice);
                indice = DReader.GetOrdinal("ObjetivoMes11");
                if (!DReader.IsDBNull(indice)) oBE_Indicador.sObjetivoMes11 = DReader.GetString(indice);
                indice = DReader.GetOrdinal("ObjetivoMes12");
                if (!DReader.IsDBNull(indice)) oBE_Indicador.sObjetivoMes12 = DReader.GetString(indice);
                indice = DReader.GetOrdinal("ObjetivoMes13");
                if (!DReader.IsDBNull(indice)) oBE_Indicador.sObjetivoMes13 = DReader.GetString(indice);


                indice = DReader.GetOrdinal("Color");
                if (!DReader.IsDBNull(indice)) oBE_Indicador.sColor = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Color2");
                if (!DReader.IsDBNull(indice)) oBE_Indicador.sColor2 = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Color3");
                if (!DReader.IsDBNull(indice)) oBE_Indicador.sColor3 = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Color4");
                if (!DReader.IsDBNull(indice)) oBE_Indicador.sColor4 = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Color5");
                if (!DReader.IsDBNull(indice)) oBE_Indicador.sColor5 = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Color6");
                if (!DReader.IsDBNull(indice)) oBE_Indicador.sColor6 = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Color7");
                if (!DReader.IsDBNull(indice)) oBE_Indicador.sColor7 = DReader.GetString(indice);

                indice = DReader.GetOrdinal("Tooltip");
                if (!DReader.IsDBNull(indice)) oBE_Indicador.sTooltip = DReader.GetString(indice);

                indice = DReader.GetOrdinal("Objetivo1");
                if (!DReader.IsDBNull(indice)) oBE_Indicador.sObjetivo1 = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Objetivo2");
                if (!DReader.IsDBNull(indice)) oBE_Indicador.sObjetivo2 = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Objetivo3");
                if (!DReader.IsDBNull(indice)) oBE_Indicador.sObjetivo3 = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Objetivo4");
                if (!DReader.IsDBNull(indice)) oBE_Indicador.sObjetivo4 = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Objetivo5");
                if (!DReader.IsDBNull(indice)) oBE_Indicador.sObjetivo5 = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Objetivo6");
                if (!DReader.IsDBNull(indice)) oBE_Indicador.sObjetivo6 = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Objetivo7");
                if (!DReader.IsDBNull(indice)) oBE_Indicador.sObjetivo7 = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Objetivo8");
                if (!DReader.IsDBNull(indice)) oBE_Indicador.sObjetivo8 = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Objetivo9");
                if (!DReader.IsDBNull(indice)) oBE_Indicador.sObjetivo9 = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Objetivo10");
                if (!DReader.IsDBNull(indice)) oBE_Indicador.sObjetivo10 = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Objetivo11");
                if (!DReader.IsDBNull(indice)) oBE_Indicador.sObjetivo11 = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Objetivo12");
                if (!DReader.IsDBNull(indice)) oBE_Indicador.sObjetivo12 = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Objetivo13");
                if (!DReader.IsDBNull(indice)) oBE_Indicador.sObjetivo13 = DReader.GetString(indice);

              

                return oBE_Indicador;
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public static BE_Indicador ListarDetalle(IDataReader DReader)
        {
            try
            {
                BE_Indicador oBE_Indicador = new BE_Indicador();

                int indice = 0;

                indice = DReader.GetOrdinal("NombreIndicador");
                if (!DReader.IsDBNull(indice)) oBE_Indicador.sNombreIndicador = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Semana1");
                if (!DReader.IsDBNull(indice)) oBE_Indicador.sSemana1 = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Semana2");
                if (!DReader.IsDBNull(indice)) oBE_Indicador.sSemana2 = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Semana3");
                if (!DReader.IsDBNull(indice)) oBE_Indicador.sSemana3 = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Semana4");
                if (!DReader.IsDBNull(indice)) oBE_Indicador.sSemana4 = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Semana5");
                if (!DReader.IsDBNull(indice)) oBE_Indicador.sSemana5 = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Semana6");
                if (!DReader.IsDBNull(indice)) oBE_Indicador.sSemana6 = DReader.GetString(indice);
                indice = DReader.GetOrdinal("TotalxItem");
                if (!DReader.IsDBNull(indice)) oBE_Indicador.sTotalItem = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Porcentaje");
                if (!DReader.IsDBNull(indice)) oBE_Indicador.sPorcentaje = DReader.GetString(indice);

                return oBE_Indicador;
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public static BE_Indicador ListarGrafico(IDataReader DReader)
        {
            try
            {
                BE_Indicador oBE_Indicador = new BE_Indicador();

                int indice = 0;

                indice = DReader.GetOrdinal("Item");
                if (!DReader.IsDBNull(indice)) oBE_Indicador.iItem = DReader.GetInt32(indice);

                indice = DReader.GetOrdinal("Mes1");
                if (!DReader.IsDBNull(indice)) oBE_Indicador.sMes1 = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Mes2");
                if (!DReader.IsDBNull(indice)) oBE_Indicador.sMes2 = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Mes3");
                if (!DReader.IsDBNull(indice)) oBE_Indicador.sMes3 = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Mes4");
                if (!DReader.IsDBNull(indice)) oBE_Indicador.sMes4 = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Mes5");
                if (!DReader.IsDBNull(indice)) oBE_Indicador.sMes5 = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Mes6");
                if (!DReader.IsDBNull(indice)) oBE_Indicador.sMes6 = DReader.GetString(indice);
                //indice = DReader.GetOrdinal("Mes7");
                //if (!DReader.IsDBNull(indice)) oBE_Indicador.sMes7 = DReader.GetString(indice);
                //indice = DReader.GetOrdinal("Mes8");
                //if (!DReader.IsDBNull(indice)) oBE_Indicador.sMes8 = DReader.GetString(indice);
                //indice = DReader.GetOrdinal("Mes9");
                //if (!DReader.IsDBNull(indice)) oBE_Indicador.sMes9 = DReader.GetString(indice);
                //indice = DReader.GetOrdinal("Mes10");
                //if (!DReader.IsDBNull(indice)) oBE_Indicador.sMes10 = DReader.GetString(indice);
                //indice = DReader.GetOrdinal("Mes11");
                //if (!DReader.IsDBNull(indice)) oBE_Indicador.sMes11 = DReader.GetString(indice);
                //indice = DReader.GetOrdinal("Mes12");
                //if (!DReader.IsDBNull(indice)) oBE_Indicador.sMes12 = DReader.GetString(indice);
                //indice = DReader.GetOrdinal("Mes13");
                //if (!DReader.IsDBNull(indice)) oBE_Indicador.sMes13 = DReader.GetString(indice);

                return oBE_Indicador;
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public static BE_Indicador ListarGraficoTotal(IDataReader DReader)
        {
            try
            {
                BE_Indicador oBE_Indicador = new BE_Indicador();

                int indice = 0;

                indice = DReader.GetOrdinal("IdIndicador");
                if (!DReader.IsDBNull(indice)) oBE_Indicador.iIdIndicador = DReader.GetInt32(indice);

                indice = DReader.GetOrdinal("NombreIndicador");
                if (!DReader.IsDBNull(indice)) oBE_Indicador.sNombreIndicador = DReader.GetString(indice);

                indice = DReader.GetOrdinal("Propietario");
                if (!DReader.IsDBNull(indice)) oBE_Indicador.sPropietario = DReader.GetString(indice);

                indice = DReader.GetOrdinal("Objetivo");
                if (!DReader.IsDBNull(indice)) oBE_Indicador.dObjetivo = DReader.GetDecimal(indice);

                indice = DReader.GetOrdinal("Mes1");
                if (!DReader.IsDBNull(indice)) oBE_Indicador.dMes1 = DReader.GetDecimal(indice);

                indice = DReader.GetOrdinal("Mes2");
                if (!DReader.IsDBNull(indice)) oBE_Indicador.dMes2 = DReader.GetDecimal(indice);

                indice = DReader.GetOrdinal("Mes3");
                if (!DReader.IsDBNull(indice)) oBE_Indicador.dMes3 = DReader.GetDecimal(indice);

                indice = DReader.GetOrdinal("Mes4");
                if (!DReader.IsDBNull(indice)) oBE_Indicador.dMes4 = DReader.GetDecimal(indice);

                indice = DReader.GetOrdinal("Mes5");
                if (!DReader.IsDBNull(indice)) oBE_Indicador.dMes5 = DReader.GetDecimal(indice);

                indice = DReader.GetOrdinal("Mes6");
                if (!DReader.IsDBNull(indice)) oBE_Indicador.dMes6 = DReader.GetDecimal(indice);

                indice = DReader.GetOrdinal("Mes7");
                if (!DReader.IsDBNull(indice)) oBE_Indicador.dMes7 = DReader.GetDecimal(indice);

                indice = DReader.GetOrdinal("Mes8");
                if (!DReader.IsDBNull(indice)) oBE_Indicador.dMes8 = DReader.GetDecimal(indice);

                indice = DReader.GetOrdinal("Mes9");
                if (!DReader.IsDBNull(indice)) oBE_Indicador.dMes9 = DReader.GetDecimal(indice);

                indice = DReader.GetOrdinal("Mes10");
                if (!DReader.IsDBNull(indice)) oBE_Indicador.dMes10 = DReader.GetDecimal(indice);

                indice = DReader.GetOrdinal("Mes11");
                if (!DReader.IsDBNull(indice)) oBE_Indicador.dMes11 = DReader.GetDecimal(indice);

                indice = DReader.GetOrdinal("Mes12");
                if (!DReader.IsDBNull(indice)) oBE_Indicador.dMes12 = DReader.GetDecimal(indice);

                indice = DReader.GetOrdinal("Mes13");
                if (!DReader.IsDBNull(indice)) oBE_Indicador.dMes13 = DReader.GetDecimal(indice);

                indice = DReader.GetOrdinal("Mes");
                if (!DReader.IsDBNull(indice)) oBE_Indicador.dMes = DReader.GetDecimal(indice);

                indice = DReader.GetOrdinal("MesPorc");
                if (!DReader.IsDBNull(indice)) oBE_Indicador.dMesPorc = DReader.GetDecimal(indice);

                indice = DReader.GetOrdinal("NombreSerie");
                if (!DReader.IsDBNull(indice)) oBE_Indicador.sNombreSerie = DReader.GetString(indice); 

                return oBE_Indicador;
            }
            catch (Exception e)
            {
                return null;
            }
        }
    }
}