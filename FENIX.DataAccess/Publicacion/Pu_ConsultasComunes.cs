﻿using System;
using System.Collections.Generic;
using System.Text;
using FENIX.BusinessEntity;
using System.Data;

namespace FENIX.DataAccess
{
    public class Pu_ListarTabla
    {
        public static BE_Tabla  Listar(IDataReader DReader)
        {
            try
            {
                BE_Tabla oBE_Tabla = new BE_Tabla();
                int indice = 0;

                indice = DReader.GetOrdinal("idTabla");
                if (!DReader.IsDBNull(indice)) oBE_Tabla.iIdTabla  = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("idTablaPadre");
                if (!DReader.IsDBNull(indice)) oBE_Tabla.iIdTablaPadre = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("IdValor");
                if (!DReader.IsDBNull(indice)) oBE_Tabla.iIdValor = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("ValVcr_Descripcion");
                if (!DReader.IsDBNull(indice)) oBE_Tabla.sDescripcion = DReader.GetString(indice);
                indice = DReader.GetOrdinal("ValChr_Estado");
                if (!DReader.IsDBNull(indice)) oBE_Tabla.sEstado = DReader.GetString(indice);
                
                return oBE_Tabla;
            }
            catch (Exception e)
            {

                return null;
            }
        }

        public static BE_TablaUc ListarUc(IDataReader DReader)
        {
            try
            {
                BE_TablaUc oBE_Tabla = new BE_TablaUc();
                int indice = 0;

                indice = DReader.GetOrdinal("idTabla");
                if (!DReader.IsDBNull(indice)) oBE_Tabla.iIdTabla = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("idTablaPadre");
                if (!DReader.IsDBNull(indice)) oBE_Tabla.iIdTablaPadre = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("IdValor");
                if (!DReader.IsDBNull(indice)) oBE_Tabla.iIdValor = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("ValVcr_Descripcion");
                if (!DReader.IsDBNull(indice)) oBE_Tabla.sDescripcion = DReader.GetString(indice);
                indice = DReader.GetOrdinal("valVchr_Descripcion1");
                if (!DReader.IsDBNull(indice)) oBE_Tabla.sDescripcion1 = DReader.GetString(indice);
                indice = DReader.GetOrdinal("valVchr_Descripcion2");
                if (!DReader.IsDBNull(indice)) oBE_Tabla.sDescripcion2 = DReader.GetString(indice);
                indice = DReader.GetOrdinal("valVchr_Descripcion3");
                if (!DReader.IsDBNull(indice)) oBE_Tabla.sDescripcion3 = DReader.GetString(indice);
                indice = DReader.GetOrdinal("valVchr_Descripcion4");
                if (!DReader.IsDBNull(indice)) oBE_Tabla.sDescripcion4 = DReader.GetString(indice);
                indice = DReader.GetOrdinal("valVchr_Descripcion5");
                if (!DReader.IsDBNull(indice)) oBE_Tabla.sDescripcion5 = DReader.GetString(indice);

                return oBE_Tabla;
            }
            catch (Exception e)
            {

                return null;
            }
        }

        public static BE_Menu ListarMenu(IDataReader DReader)
        {
            try
            {
                BE_Menu oBE_Menu = new BE_Menu();
                int indice = 0;

                indice = DReader.GetOrdinal("MenuId");
                if (!DReader.IsDBNull(indice)) oBE_Menu.iMenuId = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("Descripcion");
                if (!DReader.IsDBNull(indice)) oBE_Menu.sDescripcion = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Posicion");
                if (!DReader.IsDBNull(indice)) oBE_Menu.iPosicion = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("PadreId");
                if (!DReader.IsDBNull(indice)) oBE_Menu.iPadreId = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("Icono");
                if (!DReader.IsDBNull(indice)) oBE_Menu.sIcono = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Habilitado");
                if (!DReader.IsDBNull(indice)) oBE_Menu.iHabilitado = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("Url");
                if (!DReader.IsDBNull(indice)) oBE_Menu.sUrl = DReader.GetString(indice);               
              
                return oBE_Menu;
            }
            catch (Exception e)
            {

                return null;
            }
        }

        public static BE_Menu ListarBoton(IDataReader DReader)
        {
            try
            {
                BE_Menu oBE_Menu = new BE_Menu();
                int indice = 0;

                indice = DReader.GetOrdinal("BotonId");
                if (!DReader.IsDBNull(indice)) oBE_Menu.iBotonId = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("MenuId");
                if (!DReader.IsDBNull(indice)) oBE_Menu.iMenuId = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("GrupoId");
                if (!DReader.IsDBNull(indice)) oBE_Menu.iGrupoId = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("NombreObjeto");
                if (!DReader.IsDBNull(indice)) oBE_Menu.sBotonNombreObjeto = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Visible");
                if (!DReader.IsDBNull(indice)) oBE_Menu.iVisible = DReader.GetInt32(indice);
                           
              
                return oBE_Menu;
            }
            catch (Exception e)
            {

                return null;
            }
        }

        public static BE_Tabla ListarServicios(IDataReader DReader)
        {
            try
            {
                BE_Tabla oBE_Tabla = new BE_Tabla();
                int indice = 0;

                indice = DReader.GetOrdinal("IdServicio");
                if (!DReader.IsDBNull(indice)) oBE_Tabla.iIdValor = DReader.GetInt32(indice);      
                indice = DReader.GetOrdinal("Ser_Vch_Descripcion");
                if (!DReader.IsDBNull(indice)) oBE_Tabla.sDescripcion = DReader.GetString(indice);
               

                return oBE_Tabla;
            }
            catch (Exception e)
            {

                return null;
            }
        }

        public static BE_Tabla ListarRolCliente(IDataReader DReader)
        {
            try
            {
                BE_Tabla oBE_Tabla = new BE_Tabla();
                int indice = 0;

                indice = DReader.GetOrdinal("idTabla");
                if (!DReader.IsDBNull(indice)) oBE_Tabla.iIdTabla = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("idTablaPadre");
                if (!DReader.IsDBNull(indice)) oBE_Tabla.iIdTablaPadre = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("IdValor");
                if (!DReader.IsDBNull(indice)) oBE_Tabla.iIdValor = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("ValVcr_Descripcion");
                if (!DReader.IsDBNull(indice)) oBE_Tabla.sDescripcion = DReader.GetString(indice);

                indice = DReader.GetOrdinal("CliRol_vch_CodigoAduana");
                if (!DReader.IsDBNull(indice)) oBE_Tabla.sDescripcion1 = DReader.GetString(indice);

                indice = DReader.GetOrdinal("CliRol_vch_CasillaTD");
                if (!DReader.IsDBNull(indice)) oBE_Tabla.sDescripcion2 = DReader.GetString(indice);

                indice = DReader.GetOrdinal("CliRol_vch_CodigoTD");
                if (!DReader.IsDBNull(indice)) oBE_Tabla.sDescripcion3 = DReader.GetString(indice);

                indice = DReader.GetOrdinal("CliRol_vch_ClaveTD");
                if (!DReader.IsDBNull(indice)) oBE_Tabla.sDescripcion4 = DReader.GetString(indice);

              
                return oBE_Tabla;
            }
            catch (Exception e)
            {

                return null;
            }
        }

        public static BE_Tabla ListarBanco(IDataReader DReader)
        {
            try
            {
                BE_Tabla oBE_Tabla = new BE_Tabla();
                int indice = 0;

                indice = DReader.GetOrdinal("IdBanco");
                if (!DReader.IsDBNull(indice)) oBE_Tabla.sIdBanco = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Ban_vch_Descripcion");
                if (!DReader.IsDBNull(indice)) oBE_Tabla.sBanco_Descripcion = DReader.GetString(indice);

                return oBE_Tabla;
            }
            catch (Exception e)
            {

                return null;
            }
        }

        public static BE_Tabla ListarRegimen(IDataReader DReader)
        {
            try
            {
                BE_Tabla oBE_Tabla = new BE_Tabla();
                int indice = 0;

                indice = DReader.GetOrdinal("IdRegimen");
                if (!DReader.IsDBNull(indice)) oBE_Tabla.sIdRegimen = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Reg_vch_Descripcion");
                if (!DReader.IsDBNull(indice)) oBE_Tabla.sRegimen_Descripcion = DReader.GetString(indice);


                return oBE_Tabla;
            }
            catch (Exception e)
            {

                return null;
            }
        }

    }

}
