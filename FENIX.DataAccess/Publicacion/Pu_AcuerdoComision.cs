﻿using FENIX.BusinessEntity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace FENIX.DataAccess
{
    public class Pu_AcuerdoComision
    {

        public static BE_AcuerdoComision ListarDocumentoParaLiquidarComision(IDataRecord DReader)
        {
            BE_AcuerdoComision Entidad = new BE_AcuerdoComision();
           
            int indice;
            indice = DReader.GetOrdinal("IdDocPagoDet");
            Entidad.iIdDocPagoDet = (DReader.IsDBNull(indice) ? 0 : DReader.GetInt32(indice));
            indice = DReader.GetOrdinal("IdDetPqteSLI");
            Entidad.iIdDetpaqSLI = (DReader.IsDBNull(indice) ? 0 : DReader.GetInt32(indice));
            indice = DReader.GetOrdinal("IdDocOriDet");
            Entidad.iIdDocOriDet = (DReader.IsDBNull(indice) ? 0 : DReader.GetInt32(indice));
            indice = DReader.GetOrdinal("DocMaster");
            Entidad.sDocMaster = (DReader.IsDBNull(indice) ? "" : DReader.GetString(indice));
            indice = DReader.GetOrdinal("DocHijo");
            Entidad.sDocHijo = (DReader.IsDBNull(indice) ? "" : DReader.GetString(indice));
            indice = DReader.GetOrdinal("Volante");
            Entidad.sVolante = (DReader.IsDBNull(indice) ? "" : DReader.GetString(indice));
            indice = DReader.GetOrdinal("Consignatario");
            Entidad.sConsignatario = (DReader.IsDBNull(indice) ? "" : DReader.GetString(indice));
            indice = DReader.GetOrdinal("Cond");
            Entidad.sCondicion = (DReader.IsDBNull(indice) ? "" : DReader.GetString(indice));
            indice = DReader.GetOrdinal("Contenedor");
            Entidad.sContenedor = (DReader.IsDBNull(indice) ? "" : DReader.GetString(indice));
            indice = DReader.GetOrdinal("FechaIngreso");
            Entidad.sFechaIngreso = (DReader.IsDBNull(indice) ? "" : DReader.GetString(indice));
            indice = DReader.GetOrdinal("Retirado");
            Entidad.sRetirado = (DReader.IsDBNull(indice) ? "" : DReader.GetString(indice));
            indice = DReader.GetOrdinal("Facturado");
            Entidad.sFacturado = (DReader.IsDBNull(indice) ? "" : DReader.GetString(indice));
            //indice = DReader.GetOrdinal("Cobrado");
            //Entidad.sSituacion = (DReader.IsDBNull(indice) ? "" : DReader.GetString(indice));
            indice = DReader.GetOrdinal("Moneda");
            Entidad.sMoneda = (DReader.IsDBNull(indice) ? "" : DReader.GetString(indice));
            indice = DReader.GetOrdinal("ImporteVisual");
            Entidad.dImporteVisual = (DReader.IsDBNull(indice) ? 0 : DReader.GetDecimal(indice));
            indice = DReader.GetOrdinal("Importe");
            Entidad.dImporte = (DReader.IsDBNull(indice) ? 0 : DReader.GetDecimal(indice));
            
            indice = DReader.GetOrdinal("IdAcuerdoComision");
            Entidad.IidAcuerdoComision = (DReader.IsDBNull(indice) ? 0 : DReader.GetInt32(indice));            
            indice = DReader.GetOrdinal("Factura");
            Entidad.sNroFactura = (DReader.IsDBNull(indice) ? "" : DReader.GetString(indice));
            indice = DReader.GetOrdinal("Contrato");
            Entidad.sNroContrato = (DReader.IsDBNull(indice) ? "" : DReader.GetString(indice));

            indice = DReader.GetOrdinal("IdTarifa");
            Entidad.IidTarifa = (DReader.IsDBNull(indice) ? 0 : DReader.GetInt32(indice));
            
            indice = DReader.GetOrdinal("Tar_Vch_Descripcion");
            Entidad.sServicio = (DReader.IsDBNull(indice) ? "" : DReader.GetString(indice));
            indice = DReader.GetOrdinal("PqteSLI");
            Entidad.sPqtSLI = (DReader.IsDBNull(indice) ? "" : DReader.GetString(indice));
            indice = DReader.GetOrdinal("Situacion");
            Entidad.sSituacion = (DReader.IsDBNull(indice) ? "" : DReader.GetString(indice));
            indice = DReader.GetOrdinal("Tiempo");
            Entidad.sTiempoSituacion = (DReader.IsDBNull(indice) ? "" : DReader.GetString(indice));
            indice = DReader.GetOrdinal("Liquidacion");
            Entidad.sEstado = (DReader.IsDBNull(indice) ? "" : DReader.GetString(indice));



            return Entidad;
        }



        public static BE_AcuerdoComision Listar_LiquidacionXComisionista(IDataRecord DReader)
        {
            BE_AcuerdoComision Entidad = new BE_AcuerdoComision();

            int indice;
            indice = DReader.GetOrdinal("IdLiquidacion");
            Entidad.iLiquidacion = (DReader.IsDBNull(indice) ? 0 : DReader.GetInt32(indice));
            indice = DReader.GetOrdinal("Fecha");
            Entidad.sApto = (DReader.IsDBNull(indice) ? "" : DReader.GetString(indice));           
            indice = DReader.GetOrdinal("importe");
            Entidad.dImporte = (DReader.IsDBNull(indice) ? 0 : DReader.GetDecimal(indice));            
            indice = DReader.GetOrdinal("Items");
            Entidad.iTipoTran = (DReader.IsDBNull(indice) ? 0 : DReader.GetInt32(indice));
            indice = DReader.GetOrdinal("Situacion");
            Entidad.sSituacion = (DReader.IsDBNull(indice) ? "" : DReader.GetString(indice));
            indice = DReader.GetOrdinal("IdSituacion");
            Entidad.iIdSituacion = (DReader.IsDBNull(indice) ? 0 : DReader.GetInt32(indice));
            indice = DReader.GetOrdinal("Serie");
            Entidad.sDocNroSerie = (DReader.IsDBNull(indice) ? "" : DReader.GetString(indice));
            indice = DReader.GetOrdinal("NumeroDocumento");
            Entidad.sDocNroComisionista = (DReader.IsDBNull(indice) ? "" : DReader.GetString(indice));

            return Entidad;
        }

        public static BE_AcuerdoComision Listar_EstadosXLiquidacion(IDataRecord DReader)
        {
            BE_AcuerdoComision Entidad = new BE_AcuerdoComision();

            int indice;
            indice = DReader.GetOrdinal("Codigo");
            Entidad.iIdValor = (DReader.IsDBNull(indice) ? 0 : DReader.GetInt32(indice));
            indice = DReader.GetOrdinal("Descripcion");
            Entidad.sValor = (DReader.IsDBNull(indice) ? "" : DReader.GetString(indice));
            

            return Entidad;
        }



        public static BE_AcuerdoComision Listar_DocsComisionista(IDataRecord DReader)
        {
            BE_AcuerdoComision Entidad = new BE_AcuerdoComision();

            int indice;
            indice = DReader.GetOrdinal("IdDocsComisionista");
            Entidad.iIdDocComisionista = (DReader.IsDBNull(indice) ? 0 : DReader.GetInt32(indice));
            indice = DReader.GetOrdinal("IdComisionistaCobra");
            Entidad.iIdComisionista = (DReader.IsDBNull(indice) ? 0 : DReader.GetInt32(indice));            
            indice = DReader.GetOrdinal("Comisionista");
            Entidad.sComisionista = (DReader.IsDBNull(indice) ? "" : DReader.GetString(indice));            
            indice = DReader.GetOrdinal("TipoDoc");
            Entidad.iTipoDoc = (DReader.IsDBNull(indice) ? 0 : DReader.GetInt32(indice));            
            indice = DReader.GetOrdinal("DescTipoDoc");
            Entidad.sTipoDoc = (DReader.IsDBNull(indice) ? "" : DReader.GetString(indice));            
            indice = DReader.GetOrdinal("SerieDoc");
            Entidad.sDocNroSerie = (DReader.IsDBNull(indice) ? "" : DReader.GetString(indice));            
            indice = DReader.GetOrdinal("NumeroDoc");
            Entidad.sDocNroComisionista = (DReader.IsDBNull(indice) ? "" : DReader.GetString(indice));            

            return Entidad;
        }


        public static BE_AcuerdoComision ListarDatosLiquidacion(IDataReader DReader)
        {
            try
            {
                BE_AcuerdoComision Entidad = new BE_AcuerdoComision();

                int indice = 0;

                indice = DReader.GetOrdinal("IdLiquidacion");
                Entidad.iLiquidacion = (DReader.IsDBNull(indice) ? 0 : DReader.GetInt32(indice));
                indice = DReader.GetOrdinal("Comisionista");
                Entidad.sComisionista = (DReader.IsDBNull(indice) ? "" : DReader.GetString(indice));
                indice = DReader.GetOrdinal("Importe");
                Entidad.dImporte = (DReader.IsDBNull(indice) ? 0 : DReader.GetDecimal(indice));
                indice = DReader.GetOrdinal("FechaEmision");
                Entidad.sFacturado = (DReader.IsDBNull(indice) ? "" : DReader.GetString(indice));
                indice = DReader.GetOrdinal("Ejecutivo");
                Entidad.sEjecutivo = (DReader.IsDBNull(indice) ? "" : DReader.GetString(indice));
                indice = DReader.GetOrdinal("Estado");
                Entidad.sApto = (DReader.IsDBNull(indice) ? "" : DReader.GetString(indice));

                return Entidad;
            }
            catch (Exception e)
            {
                return null;
            }
        }
    }
}
