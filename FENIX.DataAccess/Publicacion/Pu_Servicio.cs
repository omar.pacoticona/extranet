﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FENIX.Common;
using FENIX.BusinessEntity;
using System.Data;

namespace FENIX.DataAccess
{
    public class Pu_Servicio
    {
        public static BE_Detalle listarDetalle(IDataReader DReader)
        {
            try
            {
                BE_Detalle oBE_Detalle = new BE_Detalle();
                Int32 indice = 0;
                indice = DReader.GetOrdinal("IdDocOri");
                if (!DReader.IsDBNull(indice)) oBE_Detalle.iIdDocOri = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("DocOri_vch_NumeroDO");
                if (!DReader.IsDBNull(indice)) oBE_Detalle.sNumeroDO = DReader.GetString(indice);
                indice = DReader.GetOrdinal("DocDet_int_Item");
                if (!DReader.IsDBNull(indice)) oBE_Detalle.iItem = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("IdDocOriDet");
                if (!DReader.IsDBNull(indice)) oBE_Detalle.iIdDocOriDet = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("Contenedor");
                if (!DReader.IsDBNull(indice)) oBE_Detalle.sCarga = DReader.GetString(indice);
                indice = DReader.GetOrdinal("TamanoCnt");
                if (!DReader.IsDBNull(indice)) oBE_Detalle.sTamano = DReader.GetString(indice);
                indice = DReader.GetOrdinal("TipoCnt");
                if (!DReader.IsDBNull(indice)) oBE_Detalle.sTipo = DReader.GetString(indice);
                indice = DReader.GetOrdinal("CondicionFacturar");
                if (!DReader.IsDBNull(indice)) oBE_Detalle.sCondicFacturar = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Bultos");
                if (!DReader.IsDBNull(indice)) oBE_Detalle.dBultos = DReader.GetDecimal(indice);
                indice = DReader.GetOrdinal("Peso");
                if (!DReader.IsDBNull(indice)) oBE_Detalle.dPeso = DReader.GetDecimal(indice);
                indice = DReader.GetOrdinal("Volante");
                if (!DReader.IsDBNull(indice)) oBE_Detalle.iVolante = DReader.GetInt32(indice);

                return oBE_Detalle;
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public static BE_ServicioAdicional listarServicioAdicional(IDataReader DReader)
        {
            try
            {
                BE_ServicioAdicional oBE_ServicioAdicional = new BE_ServicioAdicional();
                Int32 indice = 0;
                indice = DReader.GetOrdinal("IdSuperNegocio");
                if (!DReader.IsDBNull(indice)) oBE_ServicioAdicional.iIdSuperNegocio = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("IdOficina");
                if (!DReader.IsDBNull(indice)) oBE_ServicioAdicional.iIdOficina = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("IdServicioOficinaLineaNegocio");
                if (!DReader.IsDBNull(indice)) oBE_ServicioAdicional.iIdServicioOficinaLineaNegocio = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("Servicio");
                if (!DReader.IsDBNull(indice)) oBE_ServicioAdicional.sServicio = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Estado");
                if (!DReader.IsDBNull(indice)) oBE_ServicioAdicional.sEstado = DReader.GetString(indice);
                indice = DReader.GetOrdinal("IdTipoServicio");
                if (!DReader.IsDBNull(indice)) oBE_ServicioAdicional.iIdTipoServicio = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("TipoServicio");
                if (!DReader.IsDBNull(indice)) oBE_ServicioAdicional.sTipoServicio = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Mandatorio");
                if (!DReader.IsDBNull(indice)) oBE_ServicioAdicional.sMandatorio = DReader.GetString(indice);
                indice = DReader.GetOrdinal("IdCondicionCarga");
                if (!DReader.IsDBNull(indice)) oBE_ServicioAdicional.iIdCondicionCarga = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("IdTipoOperacion");
                if (!DReader.IsDBNull(indice)) oBE_ServicioAdicional.iIdTipoOperacion = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("IdZonaServicio");
                if (!DReader.IsDBNull(indice)) oBE_ServicioAdicional.iIdZonaServicio = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("CodigoDetraccion");
                if (!DReader.IsDBNull(indice)) oBE_ServicioAdicional.sCodigoDetraccion = DReader.GetString(indice);

                return oBE_ServicioAdicional;
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public static BE_Operacion ListarTipoOperacion(IDataReader DReader)
        {
            try
            {
                BE_Operacion oBE_Operacion = new BE_Operacion();
                Int32 indice = 0;
                indice = DReader.GetOrdinal("IdTipoOperacion");
                if (!DReader.IsDBNull(indice)) oBE_Operacion.iIdTipoOperacion = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("Operacion");
                if (!DReader.IsDBNull(indice)) oBE_Operacion.sTipoOperacion = DReader.GetString(indice);
                return oBE_Operacion;
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public static BE_Servicio ListarServicio(IDataReader DReader)
        {
            try
            {
                BE_Servicio oBE_Servicio = new BE_Servicio();
                Int32 indice = 0;
                indice = DReader.GetOrdinal("IdServicio");
                if (!DReader.IsDBNull(indice)) oBE_Servicio.iIdServicio = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("Servicio");
                if (!DReader.IsDBNull(indice)) oBE_Servicio.sServicio = DReader.GetString(indice);
                return oBE_Servicio;
            }
            catch (Exception e)
            {
                return null;
            }
        }

    }
}
