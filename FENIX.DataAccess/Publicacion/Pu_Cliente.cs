﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using FENIX.BusinessEntity;

namespace FENIX.DataAccess
{
    public class Pu_Cliente
    {
        public static BE_Cliente Listar(IDataReader DReader)
        {
            try
            {
                BE_Cliente oBE_Cliente = new BE_Cliente();
                int indice = 0;

                indice = DReader.GetOrdinal("idrow");
                if (!DReader.IsDBNull(indice)) oBE_Cliente.iIdRow = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("IdCliente");
                if (!DReader.IsDBNull(indice)) oBE_Cliente.sIdCliente = DReader.GetString(indice);
                indice = DReader.GetOrdinal("IdTipoDocumento");
                if (!DReader.IsDBNull(indice)) oBE_Cliente.iIdTipoDocumento = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("Client_vch_NumDocumento");
                if (!DReader.IsDBNull(indice)) oBE_Cliente.sNumDocumento = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Client_vch_Razon_Social");
                if (!DReader.IsDBNull(indice)) oBE_Cliente.sRazon_Social = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Client_vch_Razon_Comercial");
                if (!DReader.IsDBNull(indice)) oBE_Cliente.sRazon_Comercial = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Client_vch_DirecciónFiscal");
                if (!DReader.IsDBNull(indice)) oBE_Cliente.sDireccionFiscal = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Client_dt_InicioActividades");
                if (!DReader.IsDBNull(indice)) oBE_Cliente.sInicioActividades = DReader.GetDateTime(indice);
                indice = DReader.GetOrdinal("Client_vch_EstadoSunat");
                if (!DReader.IsDBNull(indice)) oBE_Cliente.sEstadoSunat = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Client_vch_CondicionSunat");
                if (!DReader.IsDBNull(indice)) oBE_Cliente.sCondicionSunat = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Client_Vch_PersonaContacto");
                if (!DReader.IsDBNull(indice)) oBE_Cliente.sPersonaContacto = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Client_vch_Telefono");
                if (!DReader.IsDBNull(indice)) oBE_Cliente.sTelefono = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Client_vch_Fax");
                if (!DReader.IsDBNull(indice)) oBE_Cliente.sFax = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Client_vch_Correo");
                if (!DReader.IsDBNull(indice)) oBE_Cliente.sCorreo = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Client_vch_CodigoSpring");
                if (!DReader.IsDBNull(indice)) oBE_Cliente.sCodigoSpring = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Client_chr_Estado");
                if (!DReader.IsDBNull(indice)) oBE_Cliente.sEstado = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Client_Chr_Estrategico");
                if (!DReader.IsDBNull(indice)) oBE_Cliente.sEstrategico = DReader.GetString(indice);
                indice = DReader.GetOrdinal("TipoDocumentoDescrip");
                if (!DReader.IsDBNull(indice)) oBE_Cliente.sTipoDocumentoDescrip = DReader.GetString(indice);

                return oBE_Cliente;
            }
            catch (Exception e)
            {

                return null;
            }
        }

        public static BE_Cliente ListarRol(IDataReader DReader)
        {
            try
            {
                BE_Cliente oBE_Cliente = new BE_Cliente();
                int indice = 0;

                indice = DReader.GetOrdinal("IdCliente");
                if (!DReader.IsDBNull(indice)) oBE_Cliente.sIdCliente = Convert.ToString(DReader.GetInt32(indice));
                indice = DReader.GetOrdinal("Client_vch_Razon_Social");
                if (!DReader.IsDBNull(indice)) oBE_Cliente.sRazon_Social =  DReader.GetString(indice); 
                indice = DReader.GetOrdinal("Client_vch_NumDocumento");
                if (!DReader.IsDBNull(indice)) oBE_Cliente.sNumDocumento = DReader.GetString(indice);
               

                return oBE_Cliente;
            }
            catch (Exception e)
            {

                return null;
            }
        }

        public static BE_Cliente ListarBloqueo(IDataReader DReader)
        {
            try
            {
                BE_Cliente oBE_Cliente = new BE_Cliente();
                int indice = 0;

                indice = DReader.GetOrdinal("IdBloqueoCliente");
                if (!DReader.IsDBNull(indice)) oBE_Cliente.iIdBloqueoCliente = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("FechaBloqueo");
                if (!DReader.IsDBNull(indice)) oBE_Cliente.dtFechaBloqueo = DReader.GetDateTime(indice);
                indice = DReader.GetOrdinal("Cliente");
                if (!DReader.IsDBNull(indice)) oBE_Cliente.sRazon_Social = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Bloqueado");
                if (!DReader.IsDBNull(indice)) oBE_Cliente.sBloqueadoDes = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Observacion");
                if (!DReader.IsDBNull(indice)) oBE_Cliente.sObservacionBloqueo = DReader.GetString(indice);

                return oBE_Cliente;
            }
            catch (Exception)
            {

                return null;
            }
        }
        public static BE_Cliente ListarBloqueoPorId(IDataReader DReader)
        {
            try
            {
                BE_Cliente oBE_Cliente = new BE_Cliente();
                int indice = 0;
                indice = DReader.GetOrdinal("IdBloqueoCliente");
                if (!DReader.IsDBNull(indice)) oBE_Cliente.iIdBloqueoCliente = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("FechaBloqueo");
                if (!DReader.IsDBNull(indice)) oBE_Cliente.dtFechaBloqueo = DReader.GetDateTime(indice);
                indice = DReader.GetOrdinal("IdCliente");
                if (!DReader.IsDBNull(indice)) oBE_Cliente.sIdCliente = Convert.ToString(DReader.GetInt32(indice));
                indice = DReader.GetOrdinal("DniCliente");
                if (!DReader.IsDBNull(indice)) oBE_Cliente.sNumDocumento = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Cliente");
                if (!DReader.IsDBNull(indice)) oBE_Cliente.sRazon_Social = DReader.GetString(indice);
                indice = DReader.GetOrdinal("ObservacionBloqueo");
                if (!DReader.IsDBNull(indice)) oBE_Cliente.sObservacionBloqueo = DReader.GetString(indice);
                indice = DReader.GetOrdinal("FechaDesbloqueo");
                if (!DReader.IsDBNull(indice)) oBE_Cliente.dtFechaDesbloqueo = DReader.GetDateTime(indice);
                indice = DReader.GetOrdinal("ObservacionDesbloqueo");
                if (!DReader.IsDBNull(indice)) oBE_Cliente.sObservacionDesbloqueo = DReader.GetString(indice);

                indice = DReader.GetOrdinal("Bloqueado");
                if (!DReader.IsDBNull(indice)) oBE_Cliente.sBloqueado = DReader.GetString(indice);

                return oBE_Cliente;
            }
            catch (Exception e)
            {
                return null;
            }
        }
        public static BE_Cliente ListarDniClienteKeyPressPorId(IDataReader DReader)
        {
            try
            {
                BE_Cliente oBE_Cliente = new BE_Cliente();
                int indice = 0;
                indice = DReader.GetOrdinal("IdCliente");
                if (!DReader.IsDBNull(indice)) oBE_Cliente.sIdCliente = Convert.ToString(DReader.GetInt32(indice));
                indice = DReader.GetOrdinal("Cliente");
                if (!DReader.IsDBNull(indice)) oBE_Cliente.sRazon_Social = DReader.GetString(indice);

                return oBE_Cliente;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public static BE_Cliente ListarRuc(IDataReader DReader)
        {
            try
            {
                BE_Cliente oBE_Cliente = new BE_Cliente();
                int indice = 0;
                indice = DReader.GetOrdinal("cli_Nombre");
                if (!DReader.IsDBNull(indice)) oBE_Cliente.sRazon_Social = DReader.GetString(indice);
                indice = DReader.GetOrdinal("cli_NumRuc");
                if (!DReader.IsDBNull(indice)) oBE_Cliente.sNumDocumento = DReader.GetString(indice);              
                indice = DReader.GetOrdinal("cli_NombreComercial");
                if (!DReader.IsDBNull(indice)) oBE_Cliente.sRazon_Comercial = DReader.GetString(indice);

                indice = DReader.GetOrdinal("cli_FechaInscripcion");
                if (!DReader.IsDBNull(indice)) oBE_Cliente.sInicioActividades = DReader.GetDateTime(indice);
                indice = DReader.GetOrdinal("cli_FechaIncioActividades");
                if (!DReader.IsDBNull(indice)) oBE_Cliente.sInicioActividades = DReader.GetDateTime(indice);
                indice = DReader.GetOrdinal("cli_EstadoContribuyente");
                if (!DReader.IsDBNull(indice)) oBE_Cliente.sEstadoSunat = DReader.GetString(indice);
                indice = DReader.GetOrdinal("cli_Direccion");
                if (!DReader.IsDBNull(indice)) oBE_Cliente.sDireccionFiscal = DReader.GetString(indice);
                indice = DReader.GetOrdinal("cli_Telefono");
                if (!DReader.IsDBNull(indice)) oBE_Cliente.sTelefono = DReader.GetString(indice);
                indice = DReader.GetOrdinal("cli_CondicionContribuyente");
                if (!DReader.IsDBNull(indice)) oBE_Cliente.sCondicionSunat = DReader.GetString(indice);
                indice = DReader.GetOrdinal("cli_fax");
                if (!DReader.IsDBNull(indice)) oBE_Cliente.sFax = DReader.GetString(indice);
               


                return oBE_Cliente;
            }
            catch (Exception)
            {
                return null;
            }
        }       
    }
}
