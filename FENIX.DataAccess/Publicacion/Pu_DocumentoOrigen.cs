﻿using System;
using System.Collections.Generic;
using System.Text;
using FENIX.BusinessEntity;
using System.Data;
using FENIX.Common;


namespace FENIX.DataAccess
{
    public class Pu_DocumentoOrigen
    {
        public static BE_DocumentoOrigen ListarVolantesWeb(IDataReader DReader)
        {
            try
            {
                BE_DocumentoOrigen oBE_DocumentoOrigen = new BE_DocumentoOrigen();

                int indice = 0;

                indice = DReader.GetOrdinal("IdVolante");
                if (!DReader.IsDBNull(indice)) oBE_DocumentoOrigen.iIdVolante = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("Manifiesto");
                if (!DReader.IsDBNull(indice)) oBE_DocumentoOrigen.sManifiesto = DReader.GetString(indice);
                indice = DReader.GetOrdinal("NumeroMaster");
                if (!DReader.IsDBNull(indice)) oBE_DocumentoOrigen.sDocumentoMaster = DReader.GetString(indice);
                indice = DReader.GetOrdinal("NumeroHijo");
                if (!DReader.IsDBNull(indice)) oBE_DocumentoOrigen.sDocumentoHijo = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Volante");
                if (!DReader.IsDBNull(indice)) oBE_DocumentoOrigen.sVolante = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Condicion");
                if (!DReader.IsDBNull(indice)) oBE_DocumentoOrigen.sCondicion = DReader.GetString(indice);
                /*indice = DReader.GetOrdinal("ContenedorChasis");
                if (!DReader.IsDBNull(indice)) oBE_DocumentoOrigen.sContenedor = DReader.GetString(indice);*/
                indice = DReader.GetOrdinal("Nave");
                if (!DReader.IsDBNull(indice)) oBE_DocumentoOrigen.sNave = DReader.GetString(indice);
                indice = DReader.GetOrdinal("TipoCtn");
                if (!DReader.IsDBNull(indice)) oBE_DocumentoOrigen.sTipoCtn = DReader.GetString(indice);
                indice = DReader.GetOrdinal("BultosRecib");
                if (!DReader.IsDBNull(indice)) oBE_DocumentoOrigen.dBultoRecib = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("PesoRecib");
                if (!DReader.IsDBNull(indice)) oBE_DocumentoOrigen.dPesoRecib = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("IdDocOri");
                if (!DReader.IsDBNull(indice)) oBE_DocumentoOrigen.iIdDocOri = DReader.GetInt32(indice);

                return oBE_DocumentoOrigen;
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public static BE_DocumentoOrigen ListarVolantesParaServicio(IDataReader DReader)
        {
            try
            {
                BE_DocumentoOrigen oBE_DocumentoOrigen = new BE_DocumentoOrigen();

                int indice = 0;

                indice = DReader.GetOrdinal("IdOrdSer");
                if (!DReader.IsDBNull(indice)) oBE_DocumentoOrigen.iIdOrdSer = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("Manifiesto");
                if (!DReader.IsDBNull(indice)) oBE_DocumentoOrigen.sManifiesto = DReader.GetString(indice);
                indice = DReader.GetOrdinal("NumeroHijo");
                if (!DReader.IsDBNull(indice)) oBE_DocumentoOrigen.sDocumentoHijo = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Volante");
                if (!DReader.IsDBNull(indice)) oBE_DocumentoOrigen.sVolante = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Cliente");
                if (!DReader.IsDBNull(indice)) oBE_DocumentoOrigen.sCliente = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Servicio");
                if (!DReader.IsDBNull(indice)) oBE_DocumentoOrigen.sServicio = DReader.GetString(indice);
                indice = DReader.GetOrdinal("FecSolicitud");
                if (!DReader.IsDBNull(indice)) oBE_DocumentoOrigen.sFechaSolicitud = DReader.GetString(indice);
                indice = DReader.GetOrdinal("FecProgramacion");
                if (!DReader.IsDBNull(indice)) oBE_DocumentoOrigen.sFechaProgramacion = DReader.GetString(indice);

                return oBE_DocumentoOrigen;
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public static BE_DocumentoOrigen ListarBLsparaVolante(IDataReader DReader)
        {
            try
            {
                BE_DocumentoOrigen oBE_DocumentoOrigen = new BE_DocumentoOrigen();

                int indice = 0;

                indice = DReader.GetOrdinal("IdDocOri");
                if (!DReader.IsDBNull(indice)) oBE_DocumentoOrigen.iIdDocOri = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("IdVolante");
                if (!DReader.IsDBNull(indice)) oBE_DocumentoOrigen.iIdVolante = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("Volante");
                if (!DReader.IsDBNull(indice)) oBE_DocumentoOrigen.sVolante = DReader.GetString(indice);
                indice = DReader.GetOrdinal("DocumentoOrigen");
                if (!DReader.IsDBNull(indice)) oBE_DocumentoOrigen.sDocumentoHijo = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Comentario");
                if (!DReader.IsDBNull(indice)) oBE_DocumentoOrigen.sComentario = DReader.GetString(indice);

                return oBE_DocumentoOrigen;
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public static BE_DocumentoOrigen ListarDatosVolante(IDataReader DReader)
        {
            try
            {
                BE_DocumentoOrigen oBE_DocumentoOrigen = new BE_DocumentoOrigen();

                int indice = 0;

                indice = DReader.GetOrdinal("NumeroVolante");
                if (!DReader.IsDBNull(indice)) oBE_DocumentoOrigen.sVolante = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Manifiesto");
                if (!DReader.IsDBNull(indice)) oBE_DocumentoOrigen.sManifiesto = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Nave");
                if (!DReader.IsDBNull(indice)) oBE_DocumentoOrigen.sNave = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Viaje");
                if (!DReader.IsDBNull(indice)) oBE_DocumentoOrigen.sViaje = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Rumbo");
                if (!DReader.IsDBNull(indice)) oBE_DocumentoOrigen.sRumbo = DReader.GetString(indice);
                indice = DReader.GetOrdinal("FechaEmision");
                if (!DReader.IsDBNull(indice)) oBE_DocumentoOrigen.sFechaEmision = DReader.GetString(indice);
                indice = DReader.GetOrdinal("NumeroDo");
                if (!DReader.IsDBNull(indice)) oBE_DocumentoOrigen.sDocumentoHijo = DReader.GetString(indice);
                indice = DReader.GetOrdinal("NumeroDoMaster");
                if (!DReader.IsDBNull(indice)) oBE_DocumentoOrigen.sDocumentoMaster = DReader.GetString(indice);
                indice = DReader.GetOrdinal("PtoEmbarque");
                if (!DReader.IsDBNull(indice)) oBE_DocumentoOrigen.sPtoEmbarque = DReader.GetString(indice);
                indice = DReader.GetOrdinal("PtoFinal");
                if (!DReader.IsDBNull(indice)) oBE_DocumentoOrigen.sPtoFinal = DReader.GetString(indice);
                indice = DReader.GetOrdinal("FechaLlegada");
                if (!DReader.IsDBNull(indice)) oBE_DocumentoOrigen.sFechaLlegada = DReader.GetString(indice);
                indice = DReader.GetOrdinal("FechaTermino");
                if (!DReader.IsDBNull(indice)) oBE_DocumentoOrigen.sFechaTermino = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Operacion");
                if (!DReader.IsDBNull(indice)) oBE_DocumentoOrigen.sOperacion = DReader.GetString(indice);
                indice = DReader.GetOrdinal("LineaMaritima");
                if (!DReader.IsDBNull(indice)) oBE_DocumentoOrigen.sLineaMaritima = DReader.GetString(indice);
                indice = DReader.GetOrdinal("AgenciaMaritima");
                if (!DReader.IsDBNull(indice)) oBE_DocumentoOrigen.sAgenciaMaritima = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Consignatario");
                if (!DReader.IsDBNull(indice)) oBE_DocumentoOrigen.sConsignatario = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Cliente");
                if (!DReader.IsDBNull(indice)) oBE_DocumentoOrigen.sCliente = DReader.GetString(indice);
                indice = DReader.GetOrdinal("AgenciaAduana");
                if (!DReader.IsDBNull(indice)) oBE_DocumentoOrigen.sAgenciaAduana = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Consolidador");
                if (!DReader.IsDBNull(indice)) oBE_DocumentoOrigen.sConsolidador = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Notificante");
                if (!DReader.IsDBNull(indice)) oBE_DocumentoOrigen.sNotificante = DReader.GetString(indice);

                indice = DReader.GetOrdinal("ObservacionAduana");
                if (!DReader.IsDBNull(indice)) oBE_DocumentoOrigen.sObservacionAduana = DReader.GetString(indice);
                indice = DReader.GetOrdinal("BultoManif");
                if (!DReader.IsDBNull(indice)) oBE_DocumentoOrigen.dBultoManif = DReader.GetDecimal(indice);
                indice = DReader.GetOrdinal("PesoManif");
                if (!DReader.IsDBNull(indice)) oBE_DocumentoOrigen.dPesoManif = DReader.GetDecimal(indice);
                indice = DReader.GetOrdinal("BultoMalo");
                if (!DReader.IsDBNull(indice)) oBE_DocumentoOrigen.dBultoMalo = DReader.GetDecimal(indice);
                indice = DReader.GetOrdinal("PesoMalo");
                if (!DReader.IsDBNull(indice)) oBE_DocumentoOrigen.dPesoMalo = DReader.GetDecimal(indice);
                indice = DReader.GetOrdinal("BultoFaltante");
                if (!DReader.IsDBNull(indice)) oBE_DocumentoOrigen.dBultoFaltante = DReader.GetDecimal(indice);
                indice = DReader.GetOrdinal("PesoFaltante");
                if (!DReader.IsDBNull(indice)) oBE_DocumentoOrigen.dPesoFaltante = DReader.GetDecimal(indice);
                indice = DReader.GetOrdinal("BultoSobrante");
                if (!DReader.IsDBNull(indice)) oBE_DocumentoOrigen.dBultoSobrante = DReader.GetDecimal(indice);
                indice = DReader.GetOrdinal("PesoSobrante");
                if (!DReader.IsDBNull(indice)) oBE_DocumentoOrigen.dPesoSobrante = DReader.GetDecimal(indice);
                indice = DReader.GetOrdinal("DiceContener");
                if (!DReader.IsDBNull(indice)) oBE_DocumentoOrigen.dDiceContener = DReader.GetDecimal(indice);
                indice = DReader.GetOrdinal("EmpaqueRecibido");
                if (!DReader.IsDBNull(indice)) oBE_DocumentoOrigen.dEmpaqueRecibido = DReader.GetDecimal(indice);
                indice = DReader.GetOrdinal("EstadoTransmitido");
                if (!DReader.IsDBNull(indice)) oBE_DocumentoOrigen.sEstadoTransmitido = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Mercaderia");
                if (!DReader.IsDBNull(indice)) oBE_DocumentoOrigen.sMercaderia = DReader.GetString(indice);

                indice = DReader.GetOrdinal("Contenedor");
                if (!DReader.IsDBNull(indice)) oBE_DocumentoOrigen.sContenedor = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Tamano");
                if (!DReader.IsDBNull(indice)) oBE_DocumentoOrigen.sTamano = Convert.ToString(DReader.GetInt32(indice));
                indice = DReader.GetOrdinal("GitsContenedor");
                if (!DReader.IsDBNull(indice)) oBE_DocumentoOrigen.sTipoCtn = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Tara");
                if (!DReader.IsDBNull(indice)) oBE_DocumentoOrigen.dTara = DReader.GetDecimal(indice);
                indice = DReader.GetOrdinal("FechaIngreso");
                if (!DReader.IsDBNull(indice)) oBE_DocumentoOrigen.sFechaIngreso = DReader.GetString(indice);
                indice = DReader.GetOrdinal("BultoRecib");
                if (!DReader.IsDBNull(indice)) oBE_DocumentoOrigen.dBultosRecib = DReader.GetDecimal(indice);
                indice = DReader.GetOrdinal("PesoRecib");
                if (!DReader.IsDBNull(indice)) oBE_DocumentoOrigen.dPesoRecibido = DReader.GetDecimal(indice);
                indice = DReader.GetOrdinal("PrecintoManifestado");
                if (!DReader.IsDBNull(indice)) oBE_DocumentoOrigen.sPrecintoManifestado = DReader.GetString(indice);
                indice = DReader.GetOrdinal("PrecintoRecepcionado");
                if (!DReader.IsDBNull(indice)) oBE_DocumentoOrigen.sPrecintoRecepcionado = DReader.GetString(indice);
                indice = DReader.GetOrdinal("CndCarga");
                if (!DReader.IsDBNull(indice)) oBE_DocumentoOrigen.sCondicion = DReader.GetString(indice);
                indice = DReader.GetOrdinal("TipoCarga");
                if (!DReader.IsDBNull(indice)) oBE_DocumentoOrigen.sTipoCarga = DReader.GetString(indice);

                indice = DReader.GetOrdinal("PesoInvLib");
                if (!DReader.IsDBNull(indice)) oBE_DocumentoOrigen.sPesoInvLib = DReader.GetString(indice);
                //Actualizacion Volante || Erick
                indice = DReader.GetOrdinal("ObservacionEir");
                if (!DReader.IsDBNull(indice)) oBE_DocumentoOrigen.sObservacionEir = DReader.GetString(indice);
                //// Fin
                indice = DReader.GetOrdinal("CliBloqueado");
                if (!DReader.IsDBNull(indice)) oBE_DocumentoOrigen.sCliBloqueado = DReader.GetString(indice);
                indice = DReader.GetOrdinal("AgeBloqueado");
                if (!DReader.IsDBNull(indice)) oBE_DocumentoOrigen.sAgeBloqueado = DReader.GetString(indice);
                indice = DReader.GetOrdinal("ConsBloqueado");
                if (!DReader.IsDBNull(indice)) oBE_DocumentoOrigen.sConsBloqueado = DReader.GetString(indice);

                indice = DReader.GetOrdinal("DetalleAduana");
                if (!DReader.IsDBNull(indice)) oBE_DocumentoOrigen.sDetalleAduana = DReader.GetString(indice);
                indice = DReader.GetOrdinal("FecIngresoBL");
                if (!DReader.IsDBNull(indice)) oBE_DocumentoOrigen.sFecIngresoBL = DReader.GetString(indice);
                indice = DReader.GetOrdinal("ObsTarjaApertura");
                if (!DReader.IsDBNull(indice)) oBE_DocumentoOrigen.sObservacion = DReader.GetString(indice);
                indice = DReader.GetOrdinal("TerminalPortuario");
                if (!DReader.IsDBNull(indice)) oBE_DocumentoOrigen.sTerminalPortuario = DReader.GetString(indice);
                indice = DReader.GetOrdinal("BultosRecCS");
                if (!DReader.IsDBNull(indice)) oBE_DocumentoOrigen.dBultosRecCs = DReader.GetDecimal(indice);
                indice = DReader.GetOrdinal("PesosRecCS");
                if (!DReader.IsDBNull(indice)) oBE_DocumentoOrigen.dPesosRecCs = DReader.GetDecimal(indice);
                indice = DReader.GetOrdinal("UsuarioGenera");
                if (!DReader.IsDBNull(indice)) oBE_DocumentoOrigen.sUsuarioGenera = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Sini");
                if (!DReader.IsDBNull(indice)) oBE_DocumentoOrigen.sSini = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Reestiba");
                if (!DReader.IsDBNull(indice)) oBE_DocumentoOrigen.sReestiba = DReader.GetString(indice);

                indice = DReader.GetOrdinal("DescCndCarga");
                if (!DReader.IsDBNull(indice)) oBE_DocumentoOrigen.sCondicCarga = DReader.GetString(indice);

                indice = DReader.GetOrdinal("BultoRecib2");
                if (!DReader.IsDBNull(indice)) oBE_DocumentoOrigen.dBultoRecib2 = DReader.GetDecimal(indice);

                return oBE_DocumentoOrigen;
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public static BE_DocumentoOrigen ListarDatosDscto(IDataReader DReader)
        {
            try
            {
                BE_DocumentoOrigen oBE_DocumentoOrigen = new BE_DocumentoOrigen();

                int indice = 0;

                indice = DReader.GetOrdinal("Cliente");
                if (!DReader.IsDBNull(indice)) oBE_DocumentoOrigen.sCliente = DReader.GetString(indice);
                indice = DReader.GetOrdinal("AgCarga");
                if (!DReader.IsDBNull(indice)) oBE_DocumentoOrigen.sAgenciaCarga = DReader.GetString(indice);
                indice = DReader.GetOrdinal("AgAduana");
                if (!DReader.IsDBNull(indice)) oBE_DocumentoOrigen.sAgenciaAduana = DReader.GetString(indice);
                indice = DReader.GetOrdinal("NumeroDO");
                if (!DReader.IsDBNull(indice)) oBE_DocumentoOrigen.sDocumentoHijo = DReader.GetString(indice);
                indice = DReader.GetOrdinal("FCalculo");
                if (!DReader.IsDBNull(indice)) oBE_DocumentoOrigen.sFechaSolicitud = DReader.GetString(indice);
                indice = DReader.GetOrdinal("DiasAlmacen");
                if (!DReader.IsDBNull(indice)) oBE_DocumentoOrigen.iDiasAlmacen = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("TasaDscto");
                if (!DReader.IsDBNull(indice)) oBE_DocumentoOrigen.iTasaDscto = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("ImpDscto");
                if (!DReader.IsDBNull(indice)) oBE_DocumentoOrigen.dImpDscto = DReader.GetDecimal(indice);

                return oBE_DocumentoOrigen;
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public static BE_AutorizacionRetiro ListarAutRetiro(IDataReader DReader)
        {
            try
            {
                BE_AutorizacionRetiro objEntidad = new BE_AutorizacionRetiro();

                int indice = 0;

                indice = DReader.GetOrdinal("IdAutRet");
                if (!DReader.IsDBNull(indice)) objEntidad.IIdAutRet = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("NroAutRet");
                if (!DReader.IsDBNull(indice)) objEntidad.SNumAutRet = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Fecha");
                if (!DReader.IsDBNull(indice)) objEntidad.SFechaVigencia = DReader.GetString(indice);
                indice = DReader.GetOrdinal("IdCliente");
                if (!DReader.IsDBNull(indice)) objEntidad.iIdCliente = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("Cliente");
                if (!DReader.IsDBNull(indice)) objEntidad.SCliente = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Despachador");
                if (!DReader.IsDBNull(indice)) objEntidad.SDespachador = DReader.GetString(indice);
                indice = DReader.GetOrdinal("NumeroDO");
                if (!DReader.IsDBNull(indice)) objEntidad.SDocumentoO = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Volante");
                if (!DReader.IsDBNull(indice)) objEntidad.SVolante = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Situacion");
                if (!DReader.IsDBNull(indice)) objEntidad.SSituacion = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Vigencia");
                if (!DReader.IsDBNull(indice)) objEntidad.SEstado = DReader.GetString(indice);
                indice = DReader.GetOrdinal("IdAgencia");
                if (!DReader.IsDBNull(indice)) objEntidad.iIdAgencia = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("AgAduana");
                if (!DReader.IsDBNull(indice)) objEntidad.SAgenciaAdu = DReader.GetString(indice);
                indice = DReader.GetOrdinal("dtFechaCreacion");
                if (!DReader.IsDBNull(indice)) objEntidad.dFechaCreacion = DReader.GetDateTime(indice);
                indice = DReader.GetOrdinal("dtFechaVigencia");
                if (!DReader.IsDBNull(indice)) objEntidad.dFechaHabilitado = DReader.GetDateTime(indice);
                indice = DReader.GetOrdinal("dtFechaMaxVigencia");
                if (!DReader.IsDBNull(indice)) objEntidad.dFechaMaxVigencia = DReader.GetDateTime(indice);
                

                return objEntidad;
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public static BE_AutorizacionRetiro ConsultarAutRetId(IDataReader DReader)
        {
            try
            {
                BE_AutorizacionRetiro objEntidad = new BE_AutorizacionRetiro();

                int indice = 0;

                indice = DReader.GetOrdinal("IdAutRet");
                if (!DReader.IsDBNull(indice)) objEntidad.IIdAutRet = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("NroAutRet");
                if (!DReader.IsDBNull(indice)) objEntidad.SNumAutRet = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Situacion");
                if (!DReader.IsDBNull(indice)) objEntidad.SSituacion = DReader.GetString(indice);
                indice = DReader.GetOrdinal("FechaVigencia");
                if (!DReader.IsDBNull(indice)) objEntidad.SFecha = DReader.GetDateTime(indice);
                indice = DReader.GetOrdinal("Vigencia");
                if (!DReader.IsDBNull(indice)) objEntidad.SEstado = DReader.GetString(indice);

                return objEntidad;
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public static BE_PlacaChofer ListarPlacaChofer(IDataReader DReader)
        {
            ResultadoTransaccionTx oResultadoTransaccionTx = null;
            try
            {
                BE_PlacaChofer oBE_Dua = new BE_PlacaChofer();
                int indice = 0;

                indice = DReader.GetOrdinal("IdRegistro");
                if (!DReader.IsDBNull(indice)) oBE_Dua.IIdRegistro = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("Placa");
                if (!DReader.IsDBNull(indice)) oBE_Dua.SPlaca = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Tipo");
                if (!DReader.IsDBNull(indice)) oBE_Dua.sTipo = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Doc");
                if (!DReader.IsDBNull(indice)) oBE_Dua.SDoc = DReader.GetString(indice);
                indice = DReader.GetOrdinal("NomChofer");
                if (!DReader.IsDBNull(indice)) oBE_Dua.SChofer = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Ticket");
                if (!DReader.IsDBNull(indice)) oBE_Dua.sTicket = DReader.GetString(indice);
                indice = DReader.GetOrdinal("FSalida");
                if (!DReader.IsDBNull(indice)) oBE_Dua.sFSalida = DReader.GetString(indice);


                return oBE_Dua;

            }
            catch (Exception e)
            {
                oResultadoTransaccionTx.RegistrarError(e);
                return null;
            }
        }
        public static BE_PlacaChofer ListarPlacaChoferApp(IDataReader DReader)
        {
            ResultadoTransaccionTx oResultadoTransaccionTx = null;
            try
            {
                BE_PlacaChofer oBE_Dua = new BE_PlacaChofer();
                int indice = 0;

                indice = DReader.GetOrdinal("FechaRegistro");
                if (!DReader.IsDBNull(indice)) oBE_Dua.dtFechaRegistro = DReader.GetDateTime(indice);
                indice = DReader.GetOrdinal("DNI");
                if (!DReader.IsDBNull(indice)) oBE_Dua.SDoc = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Brevete");
                if (!DReader.IsDBNull(indice)) oBE_Dua.sBrevete = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Placa");
                if (!DReader.IsDBNull(indice)) oBE_Dua.SPlaca = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Situacion");
                if (!DReader.IsDBNull(indice)) oBE_Dua.sSituacion = DReader.GetString(indice);

                return oBE_Dua;

            }
            catch (Exception e)
            {
                oResultadoTransaccionTx.RegistrarError(e);
                return null;
            }
        }
        
        public static BE_AutorizacionRetiro ListarAutorizacionRetiroDetPorId(IDataReader DReader)
        {
            try
            {
                BE_AutorizacionRetiro oBE_AutorizacionRetiro = new BE_AutorizacionRetiro();
                int indice = 0;
                indice = DReader.GetOrdinal("IdAutRetDet");
                if (!DReader.IsDBNull(indice)) oBE_AutorizacionRetiro.IdAutRetDet = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("Chasis");
                if (!DReader.IsDBNull(indice)) oBE_AutorizacionRetiro.SChasis = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Contenedor");
                if (!DReader.IsDBNull(indice)) oBE_AutorizacionRetiro.SContenedor = DReader.GetString(indice);
                indice = DReader.GetOrdinal("CantidadDespachado");
                if (!DReader.IsDBNull(indice)) oBE_AutorizacionRetiro.SCantAte = DReader.GetDecimal(indice);
                indice = DReader.GetOrdinal("PesoDespachado");
                if (!DReader.IsDBNull(indice)) oBE_AutorizacionRetiro.SPesoAte = DReader.GetDecimal(indice);
                indice = DReader.GetOrdinal("VolumenDespachado");
                if (!DReader.IsDBNull(indice)) oBE_AutorizacionRetiro.SVolAte = DReader.GetDecimal(indice);
                return oBE_AutorizacionRetiro;

            }
            catch (Exception e)
            {
                return null;
            }
        }

        public static BE_AutorizacionRetiro ListarAutorizacionRetiroDetStatusPorId(IDataReader DReader)
        {
            try
            {
                BE_AutorizacionRetiro oBE_AutorizacionRetiro = new BE_AutorizacionRetiro();
                int indice = 0;
                indice = DReader.GetOrdinal("IdAutRetDet");
                if (!DReader.IsDBNull(indice)) oBE_AutorizacionRetiro.IdAutRetDet = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("Chasis");
                if (!DReader.IsDBNull(indice)) oBE_AutorizacionRetiro.SChasis = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Contenedor");
                if (!DReader.IsDBNull(indice)) oBE_AutorizacionRetiro.SContenedor = DReader.GetString(indice);
                indice = DReader.GetOrdinal("CantidadDespachado");
                if (!DReader.IsDBNull(indice)) oBE_AutorizacionRetiro.SCantAte = DReader.GetDecimal(indice);
                indice = DReader.GetOrdinal("Estado");
                if (!DReader.IsDBNull(indice)) oBE_AutorizacionRetiro.iEstado = DReader.GetInt32(indice);
                return oBE_AutorizacionRetiro;

            }
            catch (Exception e)
            {
                return null;
            }
        }

        public static BE_AutorizacionRetiro ListarDespachador(IDataReader DReader)
        {
            try
            {
                BE_AutorizacionRetiro oBE_AutorizacionRetiro = new BE_AutorizacionRetiro();
                int indice = 0;
                indice = DReader.GetOrdinal("IdDespachador");
                if (!DReader.IsDBNull(indice)) oBE_AutorizacionRetiro.IIdDespachador = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("DNI");
                if (!DReader.IsDBNull(indice)) oBE_AutorizacionRetiro.SDniDespachador = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Nombre");
                if (!DReader.IsDBNull(indice)) oBE_AutorizacionRetiro.SDespachador = DReader.GetString(indice);
                return oBE_AutorizacionRetiro;

            }
            catch (Exception e)
            {
                return null;
            }
        }

        public static BE_PlacaChofer ListarPlacaChofer2(IDataReader DReader)
        {
            ResultadoTransaccionTx oResultadoTransaccionTx = null;
            try
            {
                BE_PlacaChofer oBE_Dua = new BE_PlacaChofer();
                int indice = 0;

                indice = DReader.GetOrdinal("IdRegistro");
                if (!DReader.IsDBNull(indice)) oBE_Dua.IIdRegistro = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("NumAut");
                if (!DReader.IsDBNull(indice)) oBE_Dua.sIdAutRet = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Placa");
                if (!DReader.IsDBNull(indice)) oBE_Dua.SPlaca = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Tipo");
                if (!DReader.IsDBNull(indice)) oBE_Dua.SDoc = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Chofer");
                if (!DReader.IsDBNull(indice)) oBE_Dua.SChofer = DReader.GetString(indice);


                return oBE_Dua;

            }
            catch (Exception e)
            {
                oResultadoTransaccionTx.RegistrarError(e);
                return null;
            }
        }

        public static BE_Despachador ListarDespachador2(IDataReader DReader)
        {
            ResultadoTransaccionTx oResultadoTransaccionTx = null;
            try
            {
                BE_Despachador oBE_Dua = new BE_Despachador();
                int indice = 0;

                indice = DReader.GetOrdinal("Codigo");
                if (!DReader.IsDBNull(indice)) oBE_Dua.Codigo = DReader.GetString(indice);
                indice = DReader.GetOrdinal("DNI");
                if (!DReader.IsDBNull(indice)) oBE_Dua.DNI = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Nombres");
                if (!DReader.IsDBNull(indice)) oBE_Dua.Nombres = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Apellidos");
                if (!DReader.IsDBNull(indice)) oBE_Dua.Apellidos = DReader.GetString(indice);


                return oBE_Dua;

            }
            catch (Exception e)
            {
                oResultadoTransaccionTx.RegistrarError(e);
                return null;
            }
        }

        public static BE_AutorizacionRetiro ListarAutRetAgre(IDataReader DReader)
        {
            try
            {
                BE_AutorizacionRetiro oBE_AutorizacionRetiro = new BE_AutorizacionRetiro();
                int indice = 0;
                indice = DReader.GetOrdinal("FechaVigencia");
                if (!DReader.IsDBNull(indice)) oBE_AutorizacionRetiro.SFechaVigencia = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Situacion");
                if (!DReader.IsDBNull(indice)) oBE_AutorizacionRetiro.SSituacion = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Estado");
                if (!DReader.IsDBNull(indice)) oBE_AutorizacionRetiro.SEstado = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Despachador");
                if (!DReader.IsDBNull(indice)) oBE_AutorizacionRetiro.SDespachador = DReader.GetString(indice);
                return oBE_AutorizacionRetiro;
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public static BE_AutorizacionRetiro ListarAutRetiro2(IDataReader DReader)
        {
            try
            {
                BE_AutorizacionRetiro objEntidad = new BE_AutorizacionRetiro();

                int indice = 0;

                indice = DReader.GetOrdinal("IdAutRet");
                if (!DReader.IsDBNull(indice)) objEntidad.IIdAutRet = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("NroAutRet");
                if (!DReader.IsDBNull(indice)) objEntidad.SNumAutRet = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Fecha");
                if (!DReader.IsDBNull(indice)) objEntidad.SFechaVigencia = DReader.GetString(indice);
                indice = DReader.GetOrdinal("IdCliente");
                if (!DReader.IsDBNull(indice)) objEntidad.iIdCliente = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("Cliente");
                if (!DReader.IsDBNull(indice)) objEntidad.SCliente = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Despachador");
                if (!DReader.IsDBNull(indice)) objEntidad.SDespachador = DReader.GetString(indice);
                indice = DReader.GetOrdinal("NumeroDO");
                if (!DReader.IsDBNull(indice)) objEntidad.SDocumentoO = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Volante");
                if (!DReader.IsDBNull(indice)) objEntidad.SVolante = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Situacion");
                if (!DReader.IsDBNull(indice)) objEntidad.SSituacion = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Vigencia");
                if (!DReader.IsDBNull(indice)) objEntidad.SEstado = DReader.GetString(indice);
                indice = DReader.GetOrdinal("IdAgencia");
                if (!DReader.IsDBNull(indice)) objEntidad.iIdAgencia = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("AgAduana");
                if (!DReader.IsDBNull(indice)) objEntidad.SAgenciaAdu = DReader.GetString(indice);

                return objEntidad;
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public static BE_AutorizacionRetiro ListarAutDespachadores(IDataReader DReader)
        {
            ResultadoTransaccionTx oResultadoTransaccionTx = null;
            try
            {
                BE_AutorizacionRetiro oBE_Dua = new BE_AutorizacionRetiro();
                int indice = 0;

                indice = DReader.GetOrdinal("IdRegistro");
                if (!DReader.IsDBNull(indice)) oBE_Dua.IIdRegistro = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("IdDespachador");
                if (!DReader.IsDBNull(indice)) oBE_Dua.IIdDespachador = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("DNI");
                if (!DReader.IsDBNull(indice)) oBE_Dua.SDniDespachador = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Despachador");
                if (!DReader.IsDBNull(indice)) oBE_Dua.SDespachador = DReader.GetString(indice);


                return oBE_Dua;

            }
            catch (Exception e)
            {
                oResultadoTransaccionTx.RegistrarError(e);
                return null;
            }
        }
        public static BE_DocumentoOrigenLista ListarDO(IDataReader DReader)
        {
            try
            {
                BE_DocumentoOrigenLista oBE_DocumentoOrigen = new BE_DocumentoOrigenLista();
                int indice = 0;


                indice = DReader.GetOrdinal("IdDocOri");
                if (!DReader.IsDBNull(indice)) oBE_DocumentoOrigen.iIdDocOri = DReader.GetInt32(indice);

                indice = DReader.GetOrdinal("PuertoOrigenDes");
                if (!DReader.IsDBNull(indice)) oBE_DocumentoOrigen.sPuertoOrigenDes = DReader.GetString(indice);
                indice = DReader.GetOrdinal("PuertoEmbarqueDes");
                if (!DReader.IsDBNull(indice)) oBE_DocumentoOrigen.sPuertoEmbarqueDes = DReader.GetString(indice);

                indice = DReader.GetOrdinal("DocOri_vch_NumeroDO");
                if (!DReader.IsDBNull(indice)) oBE_DocumentoOrigen.sNumeroDO = DReader.GetString(indice);

                indice = DReader.GetOrdinal("DocOri_vch_NumeroDOMadre");
                if (!DReader.IsDBNull(indice)) oBE_DocumentoOrigen.sNumeroDOMadre = DReader.GetString(indice);

                indice = DReader.GetOrdinal("LineaDes");
                if (!DReader.IsDBNull(indice)) oBE_DocumentoOrigen.sLineaDes = DReader.GetString(indice);

                indice = DReader.GetOrdinal("TipOperacionDes");
                if (!DReader.IsDBNull(indice)) oBE_DocumentoOrigen.sTipOperacionDes = DReader.GetString(indice);

                indice = DReader.GetOrdinal("IdTerminal");
                if (!DReader.IsDBNull(indice)) oBE_DocumentoOrigen.iIdTerminal = DReader.GetInt32(indice);

                indice = DReader.GetOrdinal("DocOri_vch_Embarcador");
                if (!DReader.IsDBNull(indice)) oBE_DocumentoOrigen.sEmbarcador = DReader.GetString(indice);

                indice = DReader.GetOrdinal("DocOri_vch_Consignatario");
                if (!DReader.IsDBNull(indice)) oBE_DocumentoOrigen.sConsignatario = DReader.GetString(indice);

                indice = DReader.GetOrdinal("DocOri_chr_EstadoTransmitido");
                if (!DReader.IsDBNull(indice)) oBE_DocumentoOrigen.sEstadoTransmitido = DReader.GetString(indice);

                indice = DReader.GetOrdinal("DocOri_chr_Sada");
                if (!DReader.IsDBNull(indice)) oBE_DocumentoOrigen.sSada = DReader.GetString(indice);

                indice = DReader.GetOrdinal("NavVia_vch_AnoManifiesto");
                if (!DReader.IsDBNull(indice)) oBE_DocumentoOrigen.sAnoManifiesto = DReader.GetString(indice);

                indice = DReader.GetOrdinal("NavVia_vch_NumManifiesto");
                if (!DReader.IsDBNull(indice)) oBE_DocumentoOrigen.sNumManifiesto = DReader.GetString(indice);

                indice = DReader.GetOrdinal("NavViaRum");
                if (!DReader.IsDBNull(indice)) oBE_DocumentoOrigen.sNaveDes = DReader.GetString(indice);

                indice = DReader.GetOrdinal("Detalle");
                if (!DReader.IsDBNull(indice)) oBE_DocumentoOrigen.sDetalleAduana = DReader.GetString(indice);

                indice = DReader.GetOrdinal("TerminalPortuario");
                if (!DReader.IsDBNull(indice)) oBE_DocumentoOrigen.sTerminalPortuario = DReader.GetString(indice);


                indice = DReader.GetOrdinal("Observacion");
                if (!DReader.IsDBNull(indice)) oBE_DocumentoOrigen.sObservaciones = DReader.GetString(indice);

                indice = DReader.GetOrdinal("CmbCondicion");
                if (!DReader.IsDBNull(indice)) oBE_DocumentoOrigen.sCmbCondicion = DReader.GetString(indice);

                indice = DReader.GetOrdinal("TipoOperacion");
                if (!DReader.IsDBNull(indice)) oBE_DocumentoOrigen.iIdTipoOperacion = DReader.GetInt32(indice);

                indice = DReader.GetOrdinal("IdCliente");
                if (!DReader.IsDBNull(indice)) oBE_DocumentoOrigen.iIdClienteConsignatario = DReader.GetInt32(indice);

                indice = DReader.GetOrdinal("IdSocio");
                if (!DReader.IsDBNull(indice)) oBE_DocumentoOrigen.iIdClienteProcedencia = DReader.GetInt32(indice);

                indice = DReader.GetOrdinal("CodigoTerminal");
                if (!DReader.IsDBNull(indice)) oBE_DocumentoOrigen.CodigoTerminal = DReader.GetString(indice);
                return oBE_DocumentoOrigen;
            }
            catch (Exception e)
            {
                return null;
            }
        }
        public static BE_DocumentoOrigenDetalleLista ListarDODetalle(IDataReader DReader)
        {
            try
            {
                BE_DocumentoOrigenDetalleLista oBE_DocumentoOrigenDetalle = new BE_DocumentoOrigenDetalleLista();
                int indice = 0;


                indice = DReader.GetOrdinal("IdDocOri");
                if (!DReader.IsDBNull(indice)) oBE_DocumentoOrigenDetalle.iIdDocOri = DReader.GetInt32(indice);

                indice = DReader.GetOrdinal("DocOri_vch_NumeroDO");
                if (!DReader.IsDBNull(indice)) oBE_DocumentoOrigenDetalle.sNumeroDO = DReader.GetString(indice);
                indice = DReader.GetOrdinal("IdDocOriDet");
                if (!DReader.IsDBNull(indice)) oBE_DocumentoOrigenDetalle.iIdDocOriDet = DReader.GetInt32(indice);

                indice = DReader.GetOrdinal("ContenedorBuscar");
                if (!DReader.IsDBNull(indice)) oBE_DocumentoOrigenDetalle.sCarga = DReader.GetString(indice);

                indice = DReader.GetOrdinal("Consignatario");
                if (!DReader.IsDBNull(indice)) oBE_DocumentoOrigenDetalle.sCliente = DReader.GetString(indice);

                indice = DReader.GetOrdinal("IdMovBalIngreso");
                if (!DReader.IsDBNull(indice)) oBE_DocumentoOrigenDetalle.iIdMovBalIngreso = DReader.GetInt32(indice);

                indice = DReader.GetOrdinal("TicketIngreso");
                if (!DReader.IsDBNull(indice)) oBE_DocumentoOrigenDetalle.iTicketIngreso = DReader.GetInt32(indice);

                indice = DReader.GetOrdinal("FechaIngreso");
                if (!DReader.IsDBNull(indice)) oBE_DocumentoOrigenDetalle.sFechaIngreso = DReader.GetString(indice);

                indice = DReader.GetOrdinal("IdMovBalSalida");
                if (!DReader.IsDBNull(indice)) oBE_DocumentoOrigenDetalle.iIdMovBalSalida = DReader.GetInt32(indice);

                indice = DReader.GetOrdinal("TicketSalida");
                if (!DReader.IsDBNull(indice)) oBE_DocumentoOrigenDetalle.iTicketSalida = DReader.GetInt32(indice);

                indice = DReader.GetOrdinal("FechaSalida");
                if (!DReader.IsDBNull(indice)) oBE_DocumentoOrigenDetalle.sFechaSalida = DReader.GetString(indice);


                return oBE_DocumentoOrigenDetalle;
            }
            catch (Exception e)
            {
                return null;
            }
        }


        public static BE_DocumentoOrigen ListarDO_AR(IDataReader DReader)
        {
            try
            {
                BE_DocumentoOrigen oBE_Doc = new BE_DocumentoOrigen();
                int indice = 0;

                indice = DReader.GetOrdinal("IdNaveViaje");
                if (!DReader.IsDBNull(indice)) oBE_Doc.iIdNavVia = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("Nave");
                if (!DReader.IsDBNull(indice)) oBE_Doc.sNave = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Viaje");
                if (!DReader.IsDBNull(indice)) oBE_Doc.sViaje = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Rumbo");
                if (!DReader.IsDBNull(indice)) oBE_Doc.sRumbo = DReader.GetString(indice);
                indice = DReader.GetOrdinal("IdOperacion");
                if (!DReader.IsDBNull(indice)) oBE_Doc.iIdOperacion = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("IdRolCliente");
                if (!DReader.IsDBNull(indice)) oBE_Doc.iIdRolCliente = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("IdCliente");
                if (!DReader.IsDBNull(indice)) oBE_Doc.iIdCliente = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("Cliente");
                if (!DReader.IsDBNull(indice)) oBE_Doc.sCliente_Descripcion = DReader.GetString(indice);
                indice = DReader.GetOrdinal("IdAgeAduana");
                if (!DReader.IsDBNull(indice)) oBE_Doc.iIdAgenciaAduana = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("AgeAduana");
                if (!DReader.IsDBNull(indice)) oBE_Doc.sAgenciaAdu_Descripcion = DReader.GetString(indice);
                indice = DReader.GetOrdinal("DniCliente");
                if (!DReader.IsDBNull(indice)) oBE_Doc.sDniCliente = DReader.GetString(indice);
                indice = DReader.GetOrdinal("DniAgeAduana");
                if (!DReader.IsDBNull(indice)) oBE_Doc.sDniAgenciaAduana = DReader.GetString(indice);
                indice = DReader.GetOrdinal("IdDocOri");
                if (!DReader.IsDBNull(indice)) oBE_Doc.iIdDocOri = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("IdDocOriDet");
                if (!DReader.IsDBNull(indice)) oBE_Doc.iIdDocOriDet = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("LineaDe");
                if (!DReader.IsDBNull(indice)) oBE_Doc.sLineaMaritima = DReader.GetString(indice);
                indice = DReader.GetOrdinal("IdLiquidacion");
                if (!DReader.IsDBNull(indice)) oBE_Doc.idliquidacion = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("NavVia_dt_TerminoDescarga");
                if (!DReader.IsDBNull(indice)) oBE_Doc.dtTerminoDescarga = DReader.GetDateTime(indice);
                indice = DReader.GetOrdinal("DocOri_vch_NumeroDO");
                if (!DReader.IsDBNull(indice)) oBE_Doc.sNumeroDO = DReader.GetString(indice);
                indice = DReader.GetOrdinal("dtMontoLiquidacion");
                if (!DReader.IsDBNull(indice)) oBE_Doc.dtMontoLiquidacion = DReader.GetDecimal(indice);


                return oBE_Doc;
            }
            catch (Exception e)
            {

                return null;
            }
        }

        public static BE_DocumentoOrigen ListarARE_DocOri_Detalle(IDataReader DReader)
        {
            ResultadoTransaccionTx oResultadoTransaccionTx = null;
            try
            {
                BE_DocumentoOrigen oBE_Doc = new BE_DocumentoOrigen();
                int indice = 0;

                indice = DReader.GetOrdinal("Contenedor");
                if (!DReader.IsDBNull(indice)) oBE_Doc.sContenedor = DReader.GetString(indice);
                indice = DReader.GetOrdinal("IdDocOriDet");
                if (!DReader.IsDBNull(indice)) oBE_Doc.iIdDocOriDet = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("IdDocOri");
                if (!DReader.IsDBNull(indice)) oBE_Doc.iIdDocOri = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("Tamano");
                if (!DReader.IsDBNull(indice)) oBE_Doc.sTamanoCnt = DReader.GetString(indice);
                indice = DReader.GetOrdinal("TipoContenedor");
                if (!DReader.IsDBNull(indice)) oBE_Doc.sTipoCnt = DReader.GetString(indice);
                indice = DReader.GetOrdinal("ConCarga");
                if (!DReader.IsDBNull(indice)) oBE_Doc.sCondicionCarga = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Carga");
                if (!DReader.IsDBNull(indice)) oBE_Doc.sCarga = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Embalaje");
                if (!DReader.IsDBNull(indice)) oBE_Doc.sEmbalaje = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Cantidad");
                if (!DReader.IsDBNull(indice)) oBE_Doc.dCantidadAutorizado = DReader.GetDecimal(indice);
                indice = DReader.GetOrdinal("Peso");
                if (!DReader.IsDBNull(indice)) oBE_Doc.dPesoAutorizado = DReader.GetDecimal(indice);
                indice = DReader.GetOrdinal("Volumen");
                if (!DReader.IsDBNull(indice)) oBE_Doc.dVolumenAutorizado = DReader.GetDecimal(indice);
                indice = DReader.GetOrdinal("Chasis");
                if (!DReader.IsDBNull(indice)) oBE_Doc.sChasis = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Item");
                if (!DReader.IsDBNull(indice)) oBE_Doc.iItem = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("NumeroDo");
                if (!DReader.IsDBNull(indice)) oBE_Doc.sNumeroDO = DReader.GetString(indice);
                indice = DReader.GetOrdinal("EstaDespachado");
                if (!DReader.IsDBNull(indice)) oBE_Doc.sDespachado = DReader.GetString(indice);
                //indice = DReader.GetOrdinal("ConAtutorizacion");
                //if (!DReader.IsDBNull(indice)) oBE_Doc.sConAutorizacion = DReader.GetString(indice);
                return oBE_Doc;
            }
            catch (Exception e)
            {
                oResultadoTransaccionTx.RegistrarError(e);
                return null;
            }

        }


        public static BE_DocumentoOrigen ListarARE_Despachador(IDataReader DReader)
        {
            ResultadoTransaccionTx oResultadoTransaccionTx = null;
            try
            {
                BE_DocumentoOrigen oBE_Doc = new BE_DocumentoOrigen();
                int indice = 0;

                indice = DReader.GetOrdinal("IdDespachador");
                if (!DReader.IsDBNull(indice)) oBE_Doc.IdDespachador = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("nombreDespachador");
                if (!DReader.IsDBNull(indice)) oBE_Doc.sNombreDespachador = DReader.GetString(indice);

                return oBE_Doc;
            }
            catch (Exception e)
            {
                oResultadoTransaccionTx.RegistrarError(e);
                return null;
            }

        }

        public static BE_DocumentoOrigen ListarARE_Ruc(IDataReader DReader)
        {
            ResultadoTransaccionTx oResultadoTransaccionTx = null;
            try
            {
                BE_DocumentoOrigen oBE_Doc = new BE_DocumentoOrigen();
                int indice = 0;

                indice = DReader.GetOrdinal("IdCliente");
                if (!DReader.IsDBNull(indice)) oBE_Doc.iIdCliente = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("Client_vch_NumDocumento");
                if (!DReader.IsDBNull(indice)) oBE_Doc.sRuc = DReader.GetString(indice);
                indice = DReader.GetOrdinal("Client_vch_Razon_Social");
                if (!DReader.IsDBNull(indice)) oBE_Doc.sRazonSocial = DReader.GetString(indice);
                return oBE_Doc;
            }
            catch (Exception e)
            {
                oResultadoTransaccionTx.RegistrarError(e);
                return null;
            }
        }
        public static BE_DocumentoOrigen ListarARE_Liquidacion(IDataReader DReader)
        {
            ResultadoTransaccionTx oResultadoTransaccionTx = null;
            try
            {
                BE_DocumentoOrigen oBE_Doc = new BE_DocumentoOrigen();
                int indice = 0;

                indice = DReader.GetOrdinal("IdLiquidacion");
                if (!DReader.IsDBNull(indice)) oBE_Doc.idliquidacion = DReader.GetInt32(indice);
                indice = DReader.GetOrdinal("dtMontoLiquidacion");
                if (!DReader.IsDBNull(indice)) oBE_Doc.dtMontoLiquidacion = DReader.GetDecimal(indice);
                indice = DReader.GetOrdinal("dtFechaCalculo");
                if (!DReader.IsDBNull(indice)) oBE_Doc.sFechCalculo = DReader.GetString(indice);
                return oBE_Doc;
            }
            catch (Exception e)
            {
                oResultadoTransaccionTx.RegistrarError(e);
                return null;
            }
        }
        

    }
}