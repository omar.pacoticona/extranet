﻿using System;
using System.Collections.Generic;
using System.Text;
using FENIX.BusinessEntity;
using System.Data;
using FENIX.Common;
using System.Data.Common;
using System.Data.SqlClient;


namespace FENIX.DataAccess
{
    public class DA_OrdenServicio
    {
        #region "Transaccion"

        public String Eliminar(BE_OrdenServicio oBE_OrdenServicio)
        {

            try
            {
                using (Database dbTerminal = new Database())
                {
                    dbTerminal.ProcedureName = "Operacion_del_Orden_Servicio";
                    dbTerminal.AddParameter("@vi_IdOrdSer", DbType.Int32, ParameterDirection.Input, oBE_OrdenServicio.iIdOrdSer);
                    dbTerminal.AddParameter("@Usuario", DbType.String, ParameterDirection.Input, oBE_OrdenServicio.sUsuario);
                    dbTerminal.AddParameter("@NombrePc", DbType.String, ParameterDirection.Input, oBE_OrdenServicio.sNombrePc);
                    dbTerminal.AddParameter("@vo_Resultado", DbType.String, ParameterDirection.Output, String.Empty, 100);
                    dbTerminal.Execute();
                    return dbTerminal.GetParameter("@vo_Resultado").ToString();
                }
            }
            catch (Exception e)
            {
                return "-5|Error en capa de datos " + e.Message.ToString();
            }
        }

        public ResultadoTransaccionTx EliminarDetalleOSPrueba(BE_OrdenServicio oBE_OrdenServicio)
        {
            using (Database dbTerminal = new Database())
            {
                DbTransaction oTx = null;
                try
                {


                    dbTerminal.ProcedureName = "Operacion_del_Orden_Servicio_Detalle";
                    oTx = dbTerminal.Transaction();
                    dbTerminal.AddTransaction(oTx);
                    dbTerminal.AddParameter("@vi_IdOrdSerDet", DbType.Int32, ParameterDirection.Input, oBE_OrdenServicio.iIdOrdSer);
                    dbTerminal.AddParameter("@Usuario", DbType.String, ParameterDirection.Input, oBE_OrdenServicio.sUsuario);
                    dbTerminal.AddParameter("@NombrePc", DbType.String, ParameterDirection.Input, oBE_OrdenServicio.sNombrePc);


                    dbTerminal.Execute();
                    dbTerminal.ProcedureName = null;
                    dbTerminal.ProcedureName = "Operacion_del_Orden_Servicio_Detalle2";
                    dbTerminal.AddTransaction(oTx);
                    dbTerminal.AddParameter("@vi_IdOrdSerDet", DbType.Int32, ParameterDirection.Input, oBE_OrdenServicio.iIdOrdSer);
                    dbTerminal.AddParameter("@Usuario", DbType.String, ParameterDirection.Input, oBE_OrdenServicio.sUsuario);
                    dbTerminal.AddParameter("@NombrePc", DbType.String, ParameterDirection.Input, oBE_OrdenServicio.sNombrePc);

                    dbTerminal.Execute();

                    oTx.Commit();
                    return new ResultadoTransaccionTx(ResultadoTransaccionTx.TipoResultado.Exito, null);

                }

                catch (Exception e)
                {
                    oTx.Rollback();
                    return new ResultadoTransaccionTx(ResultadoTransaccionTx.TipoResultado.Error, e);
                }
            }
        }
        public Int32 EliminarDetalleFisico(int iidOrdSer)
        {
            try
            {
                int vl_Retorno = 0;
                using (Database dbTerminal = new Database())
                {
                    dbTerminal.ProcedureName = "Operacion_Del_Orden_Servicio_fisico";
                    dbTerminal.AddParameter("@vi_IdOrdSer", DbType.Int32, ParameterDirection.Input, iidOrdSer);
                    dbTerminal.AddParameter("@vo_Retur", DbType.Int32, ParameterDirection.Output, vl_Retorno);

                    dbTerminal.Execute();
                    return Convert.ToInt32(dbTerminal.GetParameter("@vo_Retur").ToString());
                }
            }
            catch (Exception e)
            {
                ResultadoTransaccionTx oResultadoTransaccionTx = new ResultadoTransaccionTx(ResultadoTransaccionTx.TipoResultado.Error, e);
                oResultadoTransaccionTx.RegistrarError(e);
                return -1;
            }

        }
        public string ActualizarEstadoDetalle(BE_OrdenServicioDetalle oBE_OrdenServicioDetalle)
        {

            IDataReader reader = null;
            String vl_Retorno = "0";

            try
            {
                using (Database dbTerminal = new Database())
                {
                    int indice = 0;

                    dbTerminal.ProcedureName = "Operacion_Upd_Orden_Servicio_Detalle_Aprobar";
                    dbTerminal.AddParameter("@vi_IdOrdSerDet", DbType.Int32, ParameterDirection.Input, oBE_OrdenServicioDetalle.iIdOrdSerDet);
                    dbTerminal.AddParameter("@vi_IdSituacion", DbType.Int32, ParameterDirection.Input, oBE_OrdenServicioDetalle.iIdSituacion);
                    dbTerminal.AddParameter("@vi_PesoSolicitado", DbType.Decimal, ParameterDirection.Input, oBE_OrdenServicioDetalle.dPesoSolicitado);
                    dbTerminal.AddParameter("@vi_CantidadSolicitado", DbType.Decimal, ParameterDirection.Input, oBE_OrdenServicioDetalle.dCantidadSolicitado);
                    dbTerminal.AddParameter("@vi_HoraSolicitado", DbType.Decimal, ParameterDirection.Input, oBE_OrdenServicioDetalle.dHoraSolicitado);
                    dbTerminal.AddParameter("@vi_CantidadAtendido", DbType.Decimal, ParameterDirection.Input, oBE_OrdenServicioDetalle.dCantidadAtendido);
                    dbTerminal.AddParameter("@vi_PesoAtendido", DbType.Decimal, ParameterDirection.Input, oBE_OrdenServicioDetalle.dPesoAtendido);
                    dbTerminal.AddParameter("@vi_HoraAtendido", DbType.Decimal, ParameterDirection.Input, oBE_OrdenServicioDetalle.dHoraAtendido);
                    dbTerminal.AddParameter("@Usuario", DbType.String, ParameterDirection.Input, oBE_OrdenServicioDetalle.sUsuario);
                    dbTerminal.AddParameter("@NombrePC", DbType.String, ParameterDirection.Input, oBE_OrdenServicioDetalle.sNombrePc);


                    reader = dbTerminal.GetDataReader();


                    while (reader.Read())
                    {
                        indice = reader.GetOrdinal("mensaje");
                        if (!reader.IsDBNull(indice)) vl_Retorno = reader.GetString(indice);
                    }
                    reader.Close();
                    return vl_Retorno;
                }
            }
            catch (Exception e)
            {
                ResultadoTransaccionTx oResultadoTransaccionTx = new ResultadoTransaccionTx(ResultadoTransaccionTx.TipoResultado.Error, e);
                oResultadoTransaccionTx.RegistrarError(e);
                return vl_Retorno;
            }
        }
        public string ActualizarEstadoDetalleVarios(BE_OrdenServicioDetalle oBE_OrdenServicioDetalle)
        {

            IDataReader reader = null;
            String vl_Retorno = "0";

            try
            {
                using (Database dbTerminal = new Database())
                {
                    int indice = 0;

                    dbTerminal.ProcedureName = "Operacion_Upd_Orden_Servicio_Detalle_Aprobar_Varios";
                    dbTerminal.AddParameter("@vi_IdOrdSerDet", DbType.String, ParameterDirection.Input, oBE_OrdenServicioDetalle.sIdDocOriDet);
                    dbTerminal.AddParameter("@vi_IdSituacion", DbType.Int32, ParameterDirection.Input, oBE_OrdenServicioDetalle.iIdSituacion);
                    dbTerminal.AddParameter("@vi_PesoSolicitado", DbType.Decimal, ParameterDirection.Input, oBE_OrdenServicioDetalle.dPesoSolicitado);
                    dbTerminal.AddParameter("@vi_CantidadSolicitado", DbType.Decimal, ParameterDirection.Input, oBE_OrdenServicioDetalle.dCantidadSolicitado);
                    dbTerminal.AddParameter("@vi_HoraSolicitado", DbType.Decimal, ParameterDirection.Input, oBE_OrdenServicioDetalle.dHoraSolicitado);
                    dbTerminal.AddParameter("@vi_CantidadAtendido", DbType.Decimal, ParameterDirection.Input, oBE_OrdenServicioDetalle.dCantidadAtendido);
                    dbTerminal.AddParameter("@vi_PesoAtendido", DbType.Decimal, ParameterDirection.Input, oBE_OrdenServicioDetalle.dPesoAtendido);
                    dbTerminal.AddParameter("@vi_Monto", DbType.Decimal, ParameterDirection.Input, oBE_OrdenServicioDetalle.dMonto);
                    dbTerminal.AddParameter("@vi_HoraAtendido", DbType.Decimal, ParameterDirection.Input, oBE_OrdenServicioDetalle.dHoraAtendido);
                    dbTerminal.AddParameter("@Usuario", DbType.String, ParameterDirection.Input, oBE_OrdenServicioDetalle.sUsuario);
                    dbTerminal.AddParameter("@NombrePC", DbType.String, ParameterDirection.Input, oBE_OrdenServicioDetalle.sNombrePc);


                    reader = dbTerminal.GetDataReader();


                    while (reader.Read())
                    {
                        indice = reader.GetOrdinal("mensaje");
                        if (!reader.IsDBNull(indice)) vl_Retorno = reader.GetString(indice);
                    }
                    reader.Close();
                    return vl_Retorno;
                }
            }
            catch (Exception e)
            {
                ResultadoTransaccionTx oResultadoTransaccionTx = new ResultadoTransaccionTx(ResultadoTransaccionTx.TipoResultado.Error, e);
                oResultadoTransaccionTx.RegistrarError(e);
                return vl_Retorno;
            }
        }
        public String Actualizar_FechaFinalCobroAlmacenaje(Int32 iIdDocOri, Int32 iIdDocOriDet, DateTime dtFechaFinal, String sUsuario, String sNombrePc)
        {
            try
            {
                using (Database dbTerminal = new Database())
                {
                    dbTerminal.ProcedureName = "Operacion_Upd_UpdateFechaAlamacenaje_Traslado";
                    dbTerminal.AddParameter("@vi_IdDocOriDet", DbType.Int32, ParameterDirection.Input, iIdDocOriDet);
                    dbTerminal.AddParameter("@vi_IdDocOri", DbType.Int32, ParameterDirection.Input, iIdDocOri);
                    dbTerminal.AddParameter("@vi_FechaFinAlma", DbType.DateTime, ParameterDirection.Input, dtFechaFinal);
                    dbTerminal.AddParameter("@Usuario", DbType.String, ParameterDirection.Input, sUsuario);
                    dbTerminal.AddParameter("@NombrePC", DbType.String, ParameterDirection.Input, sNombrePc);
                    dbTerminal.AddParameter("@vo_Retorno", DbType.String, ParameterDirection.Output, String.Empty, 100);
                    dbTerminal.Execute();
                    return dbTerminal.GetParameter("@vo_Retorno").ToString();
                }
            }
            catch (Exception e)
            {
                return "0|Ocurrio un error en la capa de datos " + e.Message;

            }
        }

        public String Actualizar_CodigoCm(Int32 iIdDocOri, Int32 iIdDocOriDet, String iCodigoCmNew, String sUsuario, String sNombrePc)
        {
            try
            {
                using (Database dbTerminal = new Database())
                {
                    dbTerminal.ProcedureName = "Operacion_Upd_UpdateCodigoCm";
                    dbTerminal.AddParameter("@vi_IdDocOriDet", DbType.Int32, ParameterDirection.Input, iIdDocOriDet);
                    dbTerminal.AddParameter("@vi_IdDocOri", DbType.Int32, ParameterDirection.Input, iIdDocOri);
                    dbTerminal.AddParameter("@vi_CodigoCmNew", DbType.String, ParameterDirection.Input, iCodigoCmNew);
                    dbTerminal.AddParameter("@Usuario", DbType.String, ParameterDirection.Input, sUsuario);
                    dbTerminal.AddParameter("@NombrePC", DbType.String, ParameterDirection.Input, sNombrePc);
                    dbTerminal.AddParameter("@vo_Retorno", DbType.String, ParameterDirection.Output, String.Empty, 100);
                    dbTerminal.Execute();
                    return dbTerminal.GetParameter("@vo_Retorno").ToString();
                }
            }
            catch (Exception e)
            {
                return "0|Ocurrio un error en la capa de datos " + e.Message;

            }
        }

        public Int32 AtenderOSTransporte(BE_OrdenServicio oBE_OrdenServicio)
        {
            using (Database dbTerminal = new Database())
            {
                try
                {
                    dbTerminal.ProcedureName = "Operacion_Upd_OrdenServicio3Hermanas";
                    dbTerminal.AddParameter("@vi_IdOrdSerDet", DbType.String, ParameterDirection.Input, oBE_OrdenServicio.sIdOrdSerDet);
                    dbTerminal.AddParameter("@vi_Importe", DbType.Decimal, ParameterDirection.Input, oBE_OrdenServicio.dImporte);
                    dbTerminal.AddParameter("@Usuario", DbType.String, ParameterDirection.Input, oBE_OrdenServicio.sUsuario);
                    dbTerminal.AddParameter("@NombrePc", DbType.String, ParameterDirection.Input, oBE_OrdenServicio.sNombrePc);
                    dbTerminal.AddParameter("@vo_Grabo", DbType.Int32, ParameterDirection.Output, 0, 10);

                    dbTerminal.Execute();

                    Int32 Retorno = Convert.ToInt32(dbTerminal.GetParameter("@vo_Grabo").ToString());
                    return Retorno;
                }
                catch (Exception e)
                {
                    return -1;
                }
            }
        }

        public Int32 EnviaraFacturarOSTransporte(BE_OrdenServicio oBE_OrdenServicio)
        {
            using (Database dbTerminal = new Database())
            {
                try
                {
                    dbTerminal.ProcedureName = "Operacion_Ins_Liquidacion_Automatica";
                    dbTerminal.AddParameter("@vi_IdOrdSer", DbType.Int32, ParameterDirection.Input, oBE_OrdenServicio.iIdOrdSer);
                    dbTerminal.AddParameter("@Usuario", DbType.String, ParameterDirection.Input, oBE_OrdenServicio.sUsuario);
                    dbTerminal.AddParameter("@NombrePc", DbType.String, ParameterDirection.Input, oBE_OrdenServicio.sNombrePc);
                    dbTerminal.AddParameter("@vo_IdLiq", DbType.Int32, ParameterDirection.Output, 0, 10);

                    dbTerminal.Execute();

                    Int32 Retorno = Convert.ToInt32(dbTerminal.GetParameter("@vo_IdLiq").ToString());
                    return Retorno;
                }
                catch (Exception e)
                {
                    return -1;
                }
            }
        }

        public Int32 AdicionarOSTransporte(BE_OrdenServicio oBE_OrdenServicio)
        {
            using (Database dbTerminal = new Database())
            {
                try
                {
                    dbTerminal.ProcedureName = "Operacion_Ins_OrdenServicio3Hermanas_Add";
                    dbTerminal.AddParameter("@vi_IdOrdSerDet", DbType.String, ParameterDirection.Input, oBE_OrdenServicio.sIdOrdSerDet);
                    dbTerminal.AddParameter("@vi_Importe", DbType.Decimal, ParameterDirection.Input, oBE_OrdenServicio.dImporte);
                    dbTerminal.AddParameter("@vi_IdServicio", DbType.Int32, ParameterDirection.Input, oBE_OrdenServicio._iIdServicio);
                    dbTerminal.AddParameter("@Usuario", DbType.String, ParameterDirection.Input, oBE_OrdenServicio.sUsuario);
                    dbTerminal.AddParameter("@NombrePc", DbType.String, ParameterDirection.Input, oBE_OrdenServicio.sNombrePc);
                    dbTerminal.AddParameter("@vo_Grabo", DbType.Int32, ParameterDirection.Output, 0, 10);

                    dbTerminal.Execute();

                    Int32 Retorno = Convert.ToInt32(dbTerminal.GetParameter("@vo_Grabo").ToString());
                    return Retorno;
                }
                catch (Exception e)
                {
                    return -1;
                }
            }
        }
        #endregion




        #region "Listado"
        public BE_OrdenServicioDetalleLis ListarOrdenServicioAtencion(BE_OrdenServicio oBE_OrdenServicio)
        {
            IDataReader reader = null;

            try
            {


                if (oBE_OrdenServicio.dtFechaInicio == null)
                    oBE_OrdenServicio.dtFechaInicio = System.Data.SqlTypes.SqlDateTime.MinValue;
                if (oBE_OrdenServicio.dtFechaFinal == null)
                    oBE_OrdenServicio.dtFechaFinal = System.Data.SqlTypes.SqlDateTime.MaxValue;


                using (Database dbTerminal = new Database())
                {

                    dbTerminal.ProcedureName = "[Operacion_Lis_Orden_Servicio_Atencion]";
                    dbTerminal.AddParameter("@vi_IdOrdSer", DbType.String, ParameterDirection.Input, oBE_OrdenServicio.sIdOrdSer);
                    dbTerminal.AddParameter("@vi_NumeroDO", DbType.String, ParameterDirection.Input, oBE_OrdenServicio.sNumeroDO);
                    dbTerminal.AddParameter("@vi_IdSituacion", DbType.String, ParameterDirection.Input, oBE_OrdenServicio.sDescripcionSituacion);
                    dbTerminal.AddParameter("@vi_Contenedor", DbType.String, ParameterDirection.Input, oBE_OrdenServicio.sContenedor);
                    dbTerminal.AddParameter("@vi_fechaInicio", DbType.DateTime, ParameterDirection.Input, oBE_OrdenServicio.dtFechaInicio);
                    dbTerminal.AddParameter("@vi_FechaFinal", DbType.DateTime, ParameterDirection.Input, oBE_OrdenServicio.dtFechaFinal);
                    dbTerminal.AddParameter("@vi_numeropagina", DbType.Int32, ParameterDirection.Input, oBE_OrdenServicio.NPagina);
                    dbTerminal.AddParameter("@vi_numeroregistros", DbType.Int32, ParameterDirection.Input, oBE_OrdenServicio.NRegistros);
                    dbTerminal.AddParameter("@vi_DescripcionServicio", DbType.String, ParameterDirection.Input, oBE_OrdenServicio.sObservacion);


                    dbTerminal.AddParameter("@vo_totalregistros", DbType.Int32, ParameterDirection.Output, oBE_OrdenServicio.NTotalRegistros);

                    reader = dbTerminal.GetDataReader();

                    BE_OrdenServicioDetalleLis Lis_BE_OrdenServicio = new BE_OrdenServicioDetalleLis();

                    while (reader.Read())
                    {
                        Lis_BE_OrdenServicio.Add(Pu_OrdenServicio.ListarAtencion(reader));

                    }
                    reader.Close();
                    oBE_OrdenServicio.NTotalRegistros = Convert.ToInt32(dbTerminal.GetParameter("@vo_totalregistros").ToString());
                    return Lis_BE_OrdenServicio;
                }


            }
            catch (Exception e)
            {
                ResultadoTransaccionTx oResultadoTransaccionTx = new ResultadoTransaccionTx(ResultadoTransaccionTx.TipoResultado.Error, e);
                oResultadoTransaccionTx.RegistrarError(e);
                return null;

            }
        }
        public List<BE_OrdenServicio> Listar(BE_OrdenServicio oBE_OrdenServicio)
        {
            IDataReader reader = null;

            try
            {
                //if ((oBE_OrdenServicio.sFechaInicio.Trim().Length != 0) || (oBE_OrdenServicio.sFechaFinal.Trim().Length != 0))
                //{
                //    DateTime oDateInicial = Convert.ToDateTime(oBE_OrdenServicio.sFechaInicio + " 00:00:00");
                //    DateTime oDateFinal = Convert.ToDateTime(oBE_OrdenServicio.sFechaFinal + " 00:00:00").AddDays(1);
                //    oBE_OrdenServicio.sFechaInicio = oDateInicial.ToString("yyyyMMdd");
                //    oBE_OrdenServicio.sFechaFinal = oDateFinal.ToString("yyyyMMdd");
                //}

                using (Database dbTerminal = new Database())
                {
                    dbTerminal.ProcedureName = "[Operacion_Lis_Orden_Servicio]";
                    dbTerminal.AddParameter("@vi_IdOrdSer", DbType.String, ParameterDirection.Input, oBE_OrdenServicio.sIdOrdSer);
                    dbTerminal.AddParameter("@vi_Client_RazonSocial", DbType.String, ParameterDirection.Input, oBE_OrdenServicio.sDescripcionCliente);
                    dbTerminal.AddParameter("@vi_NumeroDO", DbType.String, ParameterDirection.Input, oBE_OrdenServicio.sNumeroDO);
                    dbTerminal.AddParameter("@vi_Volante", DbType.String, ParameterDirection.Input, oBE_OrdenServicio.sVolante);
                    dbTerminal.AddParameter("@vi_Contenedor", DbType.String, ParameterDirection.Input, oBE_OrdenServicio.sContenedor);
                    dbTerminal.AddParameter("@vi_fechaInicio", DbType.DateTime, ParameterDirection.Input, oBE_OrdenServicio.sFechaInicio);
                    dbTerminal.AddParameter("@vi_FechaFinal", DbType.DateTime, ParameterDirection.Input, oBE_OrdenServicio.sFechaFinal);
                    dbTerminal.AddParameter("@vi_numeropagina", DbType.Int32, ParameterDirection.Input, oBE_OrdenServicio.NPagina);
                    dbTerminal.AddParameter("@vi_numeroregistros", DbType.Int32, ParameterDirection.Input, oBE_OrdenServicio.NRegistros);
                    dbTerminal.AddParameter("@vo_totalregistros", DbType.Int32, ParameterDirection.Output, oBE_OrdenServicio.NTotalRegistros);

                    reader = dbTerminal.GetDataReader();

                    List<BE_OrdenServicio> Lis_BE_OrdenServicio = new List<BE_OrdenServicio>();
                    while (reader.Read())
                    {
                        Lis_BE_OrdenServicio.Add(Pu_OrdenServicio.Listar(reader));

                    }
                    reader.Close();
                    oBE_OrdenServicio.NTotalRegistros = Convert.ToInt32(dbTerminal.GetParameter("@vo_totalregistros").ToString());
                    return Lis_BE_OrdenServicio;
                }
            }
            catch (Exception e)
            {

                ResultadoTransaccionTx oResultadoTransaccionTx = new ResultadoTransaccionTx(ResultadoTransaccionTx.TipoResultado.Error, e);
                oResultadoTransaccionTx.RegistrarError(e);
                return null;
            }
        }

        public List<BE_OrdenServicioDetalle> ListarOrdenServicioDetalle(int iIdOrdSer)
        {
            IDataReader reader = null;

            try
            {


                using (Database dbTerminal = new Database())
                {
                    dbTerminal.ProcedureName = "[Operacion_Lis_Orden_Servicio_Detalle]";
                    dbTerminal.AddParameter("@vi_IdOrdSer", DbType.Int32, ParameterDirection.Input, iIdOrdSer);

                    reader = dbTerminal.GetDataReader();

                    List<BE_OrdenServicioDetalle> Lis_BE_OrdenServicio = new List<BE_OrdenServicioDetalle>();
                    while (reader.Read())
                    {
                        Lis_BE_OrdenServicio.Add(Pu_OrdenServicio.ListarOrdenServicioDetalle(reader));

                    }
                    reader.Close();
                    return Lis_BE_OrdenServicio;
                }


            }
            catch (Exception e)
            {
                ResultadoTransaccionTx oResultadoTransaccionTx = new ResultadoTransaccionTx(ResultadoTransaccionTx.TipoResultado.Error, e);
                oResultadoTransaccionTx.RegistrarError(e);
                return null;

            }
        }
        public List<BE_OrdenServicioDetalle> ListarDocumentoOrigenDetalle(int iIdDocOri)
        {
            IDataReader reader = null;

            try
            {


                using (Database dbTerminal = new Database())
                {
                    dbTerminal.ProcedureName = "[Operacion_Lis_Orden_Servicio_Documento_Origen]";
                    dbTerminal.AddParameter("vi_IdDocOri", DbType.Int32, ParameterDirection.Input, iIdDocOri);

                    reader = dbTerminal.GetDataReader();

                    List<BE_OrdenServicioDetalle> Lis_BE_OrdenServicio = new List<BE_OrdenServicioDetalle>();
                    while (reader.Read())
                    {
                        Lis_BE_OrdenServicio.Add(Pu_OrdenServicio.ListarODocumentoOrigenDetalle(reader));

                    }
                    reader.Close();
                    return Lis_BE_OrdenServicio;
                }


            }
            catch (Exception e)
            {
                ResultadoTransaccionTx oResultadoTransaccionTx = new ResultadoTransaccionTx(ResultadoTransaccionTx.TipoResultado.Error, e);
                oResultadoTransaccionTx.RegistrarError(e);
                return null;

            }
        }

        public BE_OrdenServicio ListarPorId(int IdOrdSer)
        {
            IDataReader reader = null;
            try
            {
                using (Database dbTerminal = new Database())
                {
                    dbTerminal.ProcedureName = "[Operacion_Lis_Orden_Servicio_Por_Id]";

                    dbTerminal.AddParameter("@vi_IdOrdSer", DbType.Int32, ParameterDirection.Input, IdOrdSer);
                    reader = dbTerminal.GetDataReader();


                    BE_OrdenServicio Lis_Orden_Servicio = new BE_OrdenServicio();
                    while (reader.Read())
                    {
                        Lis_Orden_Servicio = Pu_OrdenServicio.ListarPorId(reader);
                    }
                    reader.Close();

                    return Lis_Orden_Servicio;
                }
            }
            catch (Exception e)
            {
                ResultadoTransaccionTx oResultadoTransaccionTx = new ResultadoTransaccionTx(ResultadoTransaccionTx.TipoResultado.Error, e);
                oResultadoTransaccionTx.RegistrarError(e);
                return null;

            }
        }

        public BE_OrdenServicio ListarPorVolante(String NumVolante)
        {
            IDataReader reader = null;
            try
            {
                using (Database dbTerminal = new Database())
                {
                    dbTerminal.ProcedureName = "[Operacion_Lis_Orden_Servicio_Por_Volante]";

                    dbTerminal.AddParameter("@vi_NumVolante", DbType.Int32, ParameterDirection.Input, NumVolante);
                    reader = dbTerminal.GetDataReader();

                    BE_OrdenServicio Lis_Orden_Servicio = new BE_OrdenServicio();
                    while (reader.Read())
                    {
                        Lis_Orden_Servicio = Pu_OrdenServicio.ListarPorVolante(reader);
                    }
                    reader.Close();

                    return Lis_Orden_Servicio;
                }
            }
            catch (Exception e)
            {
                ResultadoTransaccionTx oResultadoTransaccionTx = new ResultadoTransaccionTx(ResultadoTransaccionTx.TipoResultado.Error, e);
                oResultadoTransaccionTx.RegistrarError(e);
                return null;

            }
        }

        public String Orden_Servicio_Valida_SLI(Int32 iIdServicio, Int32 iIdDocOriDet)
        {
            try
            {
                using (Database dbTerminal = new Database())
                {
                    dbTerminal.ProcedureName = "Operacion_Ins_Valida_OS_En_SLI";
                    dbTerminal.AddParameter("@vi_IdServicio", DbType.Int32, ParameterDirection.Input, iIdServicio);
                    dbTerminal.AddParameter("@vi_IdDocOriDet", DbType.String, ParameterDirection.Input, iIdDocOriDet);
                    dbTerminal.AddParameter("@vo_Retorno", DbType.String, ParameterDirection.Output, String.Empty, 100);
                    dbTerminal.Execute();
                    return dbTerminal.GetParameter("@vo_Retorno").ToString();
                }
            }
            catch (Exception e)
            {
                return "-5|Error en capa de datos " + e.Message.ToString();
            }
        }

        public BE_OrdenServicioLis ListarReporteOrdenServicio(BE_OrdenServicio oBE_OrdenServicio)
        {
            IDataReader reader = null;
            try
            {
                using (Database dbTerminal = new Database())
                {
                    dbTerminal.ProcedureName = "[Documentacion_Lis_Rp_OrdenServicio]";
                    dbTerminal.AddParameter("@vi_FechaDesde", DbType.DateTime, ParameterDirection.Input, oBE_OrdenServicio.dtFechaInicio);
                    dbTerminal.AddParameter("@vi_FechaHasta", DbType.DateTime, ParameterDirection.Input, oBE_OrdenServicio.dtFechaFinal);
                    dbTerminal.AddParameter("@vi_Servicio", DbType.String, ParameterDirection.Input, oBE_OrdenServicio.sStrIdTipoServicio);
                    dbTerminal.AddParameter("@vi_CndCarga", DbType.String, ParameterDirection.Input, oBE_OrdenServicio.sStrIdCndCarga);
                    dbTerminal.AddParameter("@vi_numeropagina", DbType.Int32, ParameterDirection.Input, oBE_OrdenServicio.NPagina);
                    dbTerminal.AddParameter("@vi_numeroregistros", DbType.Int32, ParameterDirection.Input, oBE_OrdenServicio.NRegistros);

                    dbTerminal.AddParameter("@vo_totalregistros", DbType.Int32, ParameterDirection.Output, oBE_OrdenServicio.NTotalRegistros);

                    reader = dbTerminal.GetDataReader();

                    BE_OrdenServicioLis Lis_BE_OrdenServicio = new BE_OrdenServicioLis();

                    while (reader.Read())
                    {
                        Lis_BE_OrdenServicio.Add(Pu_OrdenServicio.ListarReporteOrdenServicio(reader));

                    }
                    reader.Close();
                    oBE_OrdenServicio.NTotalRegistros = Convert.ToInt32(dbTerminal.GetParameter("@vo_totalregistros").ToString());
                    return Lis_BE_OrdenServicio;
                }


            }
            catch (Exception e)
            {
                ResultadoTransaccionTx oResultadoTransaccionTx = new ResultadoTransaccionTx(ResultadoTransaccionTx.TipoResultado.Error, e);
                oResultadoTransaccionTx.RegistrarError(e);
                return null;

            }
        }

        public BE_OrdenServicioLis ListarServiciosTransporte(BE_OrdenServicio ent)
        {
            BE_OrdenServicioLis lista = new BE_OrdenServicioLis();
            SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["FFADEV_TERMINAL"].ConnectionString);
            SqlCommand cmd = new SqlCommand("Operacion_Lis_OrdenServicio3Hermanas", conn);
            SqlDataReader reader = null;

            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@IdOrdSer", ent.sIdOrdSer);
            cmd.Parameters.AddWithValue("@Placa", ent.sPlaca);
            cmd.Parameters.AddWithValue("@Referencia", ent.sReferencia);
            cmd.Parameters.AddWithValue("@Ticket", ent.sTicket);
            cmd.Parameters.AddWithValue("@FechaIni", ent.sFechaInicio);
            cmd.Parameters.AddWithValue("@FechaFin", ent.sFechaFinal);
            cmd.Parameters.AddWithValue("@IdSituacion", ent.iIdSituacion);

            try
            {
                conn.Open();
                reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    lista.Add(Pu_OrdenServicio.ListarServiciosTransporte(reader));
                }
                reader.Close();
            }
            catch (Exception)
            {
                if (reader != null && !reader.IsClosed) reader.Close();
                throw;
            }
            finally
            {
                conn.Close();
                conn.Dispose();
            }
            return lista;
        }
        #endregion


        #region "Servicios Ferreyros"
        public List<BE_Tabla> ListarServicios_Traslado()
        {
            IDataReader reader = null;

            try
            {
                using (Database dbTerminal = new Database())
                {
                    dbTerminal.ProcedureName = "[Operacion_Lis_ServicioFerreyros]";
                    reader = dbTerminal.GetDataReader();
                }
                List<BE_Tabla> Lst_Tabla = new List<BE_Tabla>();
                while (reader.Read())
                {
                    Lst_Tabla.Add(Pu_ListarTabla.ListarServicios(reader));

                }
                reader.Close();
                return Lst_Tabla;

            }
            catch (Exception e)
            {
                return null;
            }
        }

        public List<BE_OrdenServicioDetalle> ListarDocumentoOrigenDetalle_Traslado(BE_OrdenServicioDetalle oBE_OrdenServicioDetalle)
        {
            IDataReader reader = null;

            try
            {


                using (Database dbTerminal = new Database())
                {
                    dbTerminal.ProcedureName = "[Operacion_Lis_DocumentoOrigenDetalle_ServicioFerreyros]";
                    dbTerminal.AddParameter("@vi_NumeroOrden", DbType.String, ParameterDirection.Input, oBE_OrdenServicioDetalle.sNroOrden);
                    dbTerminal.AddParameter("@vi_Chasis", DbType.String, ParameterDirection.Input, oBE_OrdenServicioDetalle.sChasis);
                    dbTerminal.AddParameter("@vi_Autorizacion", DbType.String, ParameterDirection.Input, oBE_OrdenServicioDetalle.sNroAutorizacion);
                    dbTerminal.AddParameter("@vi_IdCondCarga", DbType.String, ParameterDirection.Input, oBE_OrdenServicioDetalle.sCondCarga);
                    dbTerminal.AddParameter("@vi_numeropagina", DbType.Int32, ParameterDirection.Input, oBE_OrdenServicioDetalle.NPagina);
                    dbTerminal.AddParameter("@vi_numeroregistros", DbType.Int32, ParameterDirection.Input, oBE_OrdenServicioDetalle.NRegistros);
                    dbTerminal.AddParameter("@vo_totalregistros", DbType.Int32, ParameterDirection.Input, oBE_OrdenServicioDetalle.NTotalRegistros);

                    reader = dbTerminal.GetDataReader();

                    List<BE_OrdenServicioDetalle> Lis_BE_OrdenServicio = new List<BE_OrdenServicioDetalle>();
                    while (reader.Read())
                    {
                        Lis_BE_OrdenServicio.Add(Pu_OrdenServicio.ListarODocumentoOrigenDetalle_Traslado(reader));

                    }
                    reader.Close();
                    return Lis_BE_OrdenServicio;
                }


            }
            catch (Exception e)
            {
                ResultadoTransaccionTx oResultadoTransaccionTx = new ResultadoTransaccionTx(ResultadoTransaccionTx.TipoResultado.Error, e);
                oResultadoTransaccionTx.RegistrarError(e);
                return null;

            }
        }
        #endregion



        #region "FIFA"
        public BE_OrdenServicioLis ListarOrdenesServicio(BE_OrdenServicio ent)
        {
            IDataReader reader = null;

            try
            {
                using (Database dbTerminal = new Database())
                {
                    dbTerminal.ProcedureName = "OPE_Lis_Orden_Servicio_2";
                    dbTerminal.AddParameter("@vi_IdLineaNegocio", DbType.Int32, ParameterDirection.Input, ent.iIdLineaNegocio);
                    dbTerminal.AddParameter("@vi_IdOrdSer", DbType.String, ParameterDirection.Input, ent.sIdOrdSer);
                    dbTerminal.AddParameter("@vi_Documento", DbType.String, ParameterDirection.Input, ent.sNumeroDO);
                    dbTerminal.AddParameter("@vi_Cliente", DbType.String, ParameterDirection.Input, ent.sDescripcionCliente);
                    dbTerminal.AddParameter("@vi_Contenedor", DbType.String, ParameterDirection.Input, ent.sContenedor);
                    dbTerminal.AddParameter("@vi_IdOrigen", DbType.Int32, ParameterDirection.Input, ent.iIdOrigen);
                    dbTerminal.AddParameter("@vi_FechaInicio", DbType.String, ParameterDirection.Input, ent.sFechaInicio);
                    dbTerminal.AddParameter("@vi_FechaFinal", DbType.String, ParameterDirection.Input, ent.sFechaFinal);

                    reader = dbTerminal.GetDataReader();

                    BE_OrdenServicioLis Lis_BE_OrdenServicio = new BE_OrdenServicioLis();
                    while (reader.Read())
                    {
                        Lis_BE_OrdenServicio.Add(Pu_OrdenServicio.Entidad_ListarOrdenesServicio(reader));
                    }
                    reader.Close();

                    return Lis_BE_OrdenServicio;
                }
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public BE_OrdenServicioDetalleLis ListarDocumentoOrigenDetalleNuevaOrden(BE_OrdenServicioDetalle ent)
        {
            IDataReader reader = null;

            try
            {
                using (Database dbTerminal = new Database())
                {
                    dbTerminal.ProcedureName = "OPE_Lis_Orden_Servicio_Documento_Origen_2";
                    dbTerminal.AddParameter("@vi_Negocio", DbType.Int32, ParameterDirection.Input, ent.iIdLineaNegocio);
                    dbTerminal.AddParameter("@vi_TipoOperacion", DbType.Int32, ParameterDirection.Input, ent.iIdTipoOperacion);
                    dbTerminal.AddParameter("@vi_Documento", DbType.String, ParameterDirection.Input, ent.sNumeroDO);

                    reader = dbTerminal.GetDataReader();

                    BE_OrdenServicioDetalleLis Lis_BE_OrdenServicioDetalle = new BE_OrdenServicioDetalleLis();
                    while (reader.Read())
                    {
                        Lis_BE_OrdenServicioDetalle.Add(Pu_OrdenServicio.Entidad_ListarDocumentoOrigenDetalleNuevaOrden(reader));
                    }
                    reader.Close();

                    return Lis_BE_OrdenServicioDetalle;
                }
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public BE_OrdenServicioDetalleLis ListarOrdenServicioDetalle(BE_OrdenServicioDetalle ent)
        {
            BE_OrdenServicioDetalleLis lista = new BE_OrdenServicioDetalleLis();
            SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["FFADEV_TERMINAL"].ConnectionString);
            SqlCommand cmd = new SqlCommand("OPE_Lis_Orden_Servicio_Detalle_2", conn);
            SqlDataReader reader = null;

            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@vi_IdOrdSer", ent.iIdOrdSer);

            try
            {
                conn.Open();
                reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    lista.Add(Entidad_ListarOrdenServicioDetalle(reader));
                }
                reader.Close();
            }
            catch (Exception)
            {
                if (reader != null && !reader.IsClosed) reader.Close();
                throw;
            }
            finally
            {
                conn.Close();
                conn.Dispose();
            }
            return lista;
        }

        private BE_OrdenServicioDetalle Entidad_ListarOrdenServicioDetalle(IDataRecord DReader)
        {
            BE_OrdenServicioDetalle Entidad = new BE_OrdenServicioDetalle();

            int indice;
            indice = DReader.GetOrdinal("Servicio");
            Entidad.sDescripcionServicio = (DReader.IsDBNull(indice) ? "" : DReader.GetString(indice));
            indice = DReader.GetOrdinal("IdDocOri");
            Entidad.iIdDocOri = (DReader.IsDBNull(indice) ? 0 : DReader.GetInt32(indice));
            indice = DReader.GetOrdinal("IdDocOriDet");
            Entidad.iIdDocOriDet = (DReader.IsDBNull(indice) ? 0 : DReader.GetInt32(indice));
            indice = DReader.GetOrdinal("Documento");
            Entidad.sNumeroDO = (DReader.IsDBNull(indice) ? "" : DReader.GetString(indice));
            indice = DReader.GetOrdinal("IdStoCon");
            Entidad.iIdStoCon = (DReader.IsDBNull(indice) ? 0 : DReader.GetInt32(indice));
            indice = DReader.GetOrdinal("TipoCnt");
            Entidad.sDescripcionTipoCnt = (DReader.IsDBNull(indice) ? "" : DReader.GetString(indice));
            indice = DReader.GetOrdinal("TamanoCnt");
            Entidad.sDescripcionTamanoCnt = (DReader.IsDBNull(indice) ? "" : DReader.GetString(indice));
            indice = DReader.GetOrdinal("CondicionFacturar");
            Entidad.sDescripcionCondicionFacturar = (DReader.IsDBNull(indice) ? "" : DReader.GetString(indice));
            indice = DReader.GetOrdinal("Bultos");
            Entidad.dBultosRecibidos = (DReader.IsDBNull(indice) ? 0 : DReader.GetDecimal(indice));
            indice = DReader.GetOrdinal("Peso");
            Entidad.dPesoRecibido = (DReader.IsDBNull(indice) ? 0 : DReader.GetDecimal(indice));
            indice = DReader.GetOrdinal("Contenedor");
            Entidad.sContenedor = (DReader.IsDBNull(indice) ? "" : DReader.GetString(indice));
            indice = DReader.GetOrdinal("Item");
            Entidad.iItem = (DReader.IsDBNull(indice) ? 0 : DReader.GetInt32(indice));
            indice = DReader.GetOrdinal("Situacion");
            Entidad.sDescripcionSituacion = (DReader.IsDBNull(indice) ? "" : DReader.GetString(indice));
            indice = DReader.GetOrdinal("Moneda");
            Entidad.sMoneda = (DReader.IsDBNull(indice) ? "" : DReader.GetString(indice));
            indice = DReader.GetOrdinal("Importe");
            Entidad.dMonto = (DReader.IsDBNull(indice) ? 0 : DReader.GetDecimal(indice));
            indice = DReader.GetOrdinal("IdOrdSerDet");
            Entidad.iIdOrdSerDet = (DReader.IsDBNull(indice) ? 0 : DReader.GetInt32(indice));

            return Entidad;
        }

        public String Insertar_OrdenServicio(BE_OrdenServicio oBE_OrdenServicio, Int32 pIdServicioOficinaLineaNegocio)
        {
            try
            {
                using (Database dbTerminal = new Database())
                {
                    dbTerminal.ProcedureName = "OPE_Ins_Orden_Servicio_2";
                    dbTerminal.AddParameter("@IdSuperNegocio", DbType.Int32, ParameterDirection.Input, oBE_OrdenServicio.iIdLineaNegocio);
                    dbTerminal.AddParameter("@IdDocumento", DbType.Int32, ParameterDirection.Input, oBE_OrdenServicio.iIdDocOri);
                    dbTerminal.AddParameter("@OrdSer_Vch_Documento", DbType.String, ParameterDirection.Input, oBE_OrdenServicio.sNumeroDO);
                    dbTerminal.AddParameter("@IdCliente", DbType.Int32, ParameterDirection.Input, oBE_OrdenServicio.iIdCliente);
                    dbTerminal.AddParameter("@IdSocio", DbType.Int32, ParameterDirection.Input, oBE_OrdenServicio.iIdSocio);
                    dbTerminal.AddParameter("@IdOrigenSolicitud", DbType.Int32, ParameterDirection.Input, oBE_OrdenServicio.iIdOrigen);
                    dbTerminal.AddParameter("@OrdSer_Vch_FechaProgramacion", DbType.String, ParameterDirection.Input, oBE_OrdenServicio.sFechaProg);
                    dbTerminal.AddParameter("@OrdSer_vch_Observacion", DbType.String, ParameterDirection.Input, oBE_OrdenServicio.sObservacion);
                    dbTerminal.AddParameter("@Usuario", DbType.String, ParameterDirection.Input, oBE_OrdenServicio.sUsuario);
                    dbTerminal.AddParameter("@NombrePC", DbType.String, ParameterDirection.Input, oBE_OrdenServicio.sNombrePc);
                    dbTerminal.AddParameter("@vo_Resultado", DbType.String, ParameterDirection.Output, String.Empty, 100);
                    dbTerminal.AddParameter("@vi_IdServicioOficinaLineaNegocio", DbType.String, ParameterDirection.Input, pIdServicioOficinaLineaNegocio);
                    dbTerminal.Execute();

                    String Retorno = dbTerminal.GetParameter("@vo_Resultado").ToString();
                    return Retorno;
                }
            }
            catch (Exception e)
            {
                return "-999|Ocurrio un error en la capa de datos";
            }
        }
        
        public String Actualizar(BE_OrdenServicio oBE_OrdenServicio)
        {
            try
            {
                using (Database dbTerminal = new Database())
                {
                    dbTerminal.ProcedureName = "OPE_Upd_Orden_Servicio_2";
                    dbTerminal.AddParameter("@vi_OrdSer_dt_FechaProg", DbType.String, ParameterDirection.Input, oBE_OrdenServicio.sFechaProg);
                    dbTerminal.AddParameter("@vi_OrdSer_vch_Observacion", DbType.String, ParameterDirection.Input, oBE_OrdenServicio.sObservacion);
                    dbTerminal.AddParameter("@Usuario", DbType.String, ParameterDirection.Input, oBE_OrdenServicio.sUsuario);
                    dbTerminal.AddParameter("@NombrePC", DbType.String, ParameterDirection.Input, oBE_OrdenServicio.sNombrePc);
                    dbTerminal.AddParameter("@vi_IdOrdSer", DbType.Int32, ParameterDirection.Input, oBE_OrdenServicio.iIdOrdSer);
                    dbTerminal.AddParameter("@vo_Resultado", DbType.String, ParameterDirection.Output, String.Empty, 100);
                    dbTerminal.Execute();

                    String Retorno = dbTerminal.GetParameter("@vo_Resultado").ToString();
                    return Retorno;
                }
            }
            catch (Exception e)
            {
                return "-999|Ocurrio un error en la capa de datos";
            }
        }

        public String Insertar_OrdenServicioDetalle(BE_OrdenServicioDetalle oBE_OrdenServicioDetalle)
        {
            try
            {
                using (Database dbTerminal = new Database())
                {
                    dbTerminal.ProcedureName = "OPE_Ins_Orden_Servicio_Detalle_2";

                    dbTerminal.AddParameter("@IdSuperNegocio", DbType.Int32, ParameterDirection.Input, oBE_OrdenServicioDetalle.iIdLineaNegocio);
                    dbTerminal.AddParameter("@vi_IdOrdSer", DbType.Int32, ParameterDirection.Input, oBE_OrdenServicioDetalle.iIdOrdSer);
                    dbTerminal.AddParameter("@vi_IdServicioOficinaLineaNegocio", DbType.Int32, ParameterDirection.Input, oBE_OrdenServicioDetalle.iIdServicioOficinaLineaNegocio);
                    dbTerminal.AddParameter("@vi_IdDocOriDet", DbType.Int32, ParameterDirection.Input, oBE_OrdenServicioDetalle.iIdDocOriDet);
                    dbTerminal.AddParameter("@vi_IdOrigenSolicitud", DbType.Int32, ParameterDirection.Input, oBE_OrdenServicioDetalle.iIdOrigen);
                    dbTerminal.AddParameter("@vi_OrdSerDet_Chr_Valorizado", DbType.String, ParameterDirection.Input, oBE_OrdenServicioDetalle.sValorizado);
                    dbTerminal.AddParameter("@vi_IdMoneda", DbType.Int32, ParameterDirection.Input, oBE_OrdenServicioDetalle.iIdMoneda);
                    dbTerminal.AddParameter("@vi_OrdSerDet_dec_Monto", DbType.Decimal, ParameterDirection.Input, oBE_OrdenServicioDetalle.dMonto);
                    dbTerminal.AddParameter("@vi_OrdSerDet_vch_Observacion", DbType.String, ParameterDirection.Input, oBE_OrdenServicioDetalle.sObservacion);
                    dbTerminal.AddParameter("@Usuario", DbType.String, ParameterDirection.Input, oBE_OrdenServicioDetalle.sUsuario);
                    dbTerminal.AddParameter("@NombrePc", DbType.String, ParameterDirection.Input, oBE_OrdenServicioDetalle.sNombrePc);
                    dbTerminal.AddParameter("@vo_Resultado", DbType.String, ParameterDirection.Output, String.Empty, 100);
                    dbTerminal.Execute();

                    String Retorno = dbTerminal.GetParameter("@vo_Resultado").ToString();
                    return Retorno;
                }
            }
            catch (Exception e)
            {
                return "-999|Ocurrio un error en la capa de datos";
            }
        }

        public String EliminarDetalleOS(BE_OrdenServicio oBE_OrdenServicio)
        {
            try
            {
                using (Database dbTerminal = new Database())
                {
                    dbTerminal.ProcedureName = "OPE_Del_Orden_Servicio_Detalle_2";
                    dbTerminal.AddParameter("@vi_IdOrdSerDet", DbType.Int32, ParameterDirection.Input, oBE_OrdenServicio.iIdOrdSerDet);
                    dbTerminal.AddParameter("@Usuario", DbType.String, ParameterDirection.Input, oBE_OrdenServicio.sUsuario);
                    dbTerminal.AddParameter("@NombrePc", DbType.String, ParameterDirection.Input, oBE_OrdenServicio.sNombrePc);
                    dbTerminal.AddParameter("@vo_Resultado", DbType.String, ParameterDirection.Output, String.Empty, 200);
                    dbTerminal.Execute();

                    String Retorno = dbTerminal.GetParameter("@vo_Resultado").ToString();
                    return Retorno;
                }
            }
            catch (Exception e)
            {
                return "-999|Error en capa de datos";
            }
        }

        public String EliminarCabeceraOS(BE_OrdenServicio oBE_OrdenServicio)
        {
            try
            {
                using (Database dbTerminal = new Database())
                {
                    dbTerminal.ProcedureName = "OPE_Del_Orden_Servicio_Cabecera_2";
                    dbTerminal.AddParameter("@vi_IdOrdSer", DbType.Int32, ParameterDirection.Input, oBE_OrdenServicio.iIdOrdSer);
                    dbTerminal.AddParameter("@Usuario", DbType.String, ParameterDirection.Input, oBE_OrdenServicio.sUsuario);
                    dbTerminal.AddParameter("@NombrePc", DbType.String, ParameterDirection.Input, oBE_OrdenServicio.sNombrePc);
                    dbTerminal.AddParameter("@vo_Resultado", DbType.String, ParameterDirection.Output, String.Empty, 200);
                    dbTerminal.Execute();

                    String Retorno = dbTerminal.GetParameter("@vo_Resultado").ToString();
                    return Retorno;
                }
            }
            catch (Exception e)
            {
                return "-999|Error en capa de datos";
            }
        }

        public BE_OrdenServicio ListarOrdenServicioPorId_2(Int32 IdOrdSer)
        {
            IDataReader reader = null;
            try
            {
                using (Database dbTerminal = new Database())
                {
                    dbTerminal.ProcedureName = "OPE_Lis_Orden_Servicio_PorId_2";
                    dbTerminal.AddParameter("@vi_IdOrdSer", DbType.Int32, ParameterDirection.Input, IdOrdSer);
                    reader = dbTerminal.GetDataReader();

                    BE_OrdenServicio Lis_BE_OrdenServicio = new BE_OrdenServicio();
                    while (reader.Read())
                    {
                        Lis_BE_OrdenServicio = Pu_OrdenServicio.ListarOrdenServicioPorId_2(reader);
                    }
                    reader.Close();

                    return Lis_BE_OrdenServicio;
                }
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public BE_OrdenServicioDetalleLis ListarOrdenesServicioAtencion(BE_OrdenServicio ent)
        {
            IDataReader reader = null;

            try
            {
                using (Database dbTerminal = new Database())
                {
                    dbTerminal.ProcedureName = "OPE_Lis_Orden_Servicio_Atencion_2";
                    dbTerminal.AddParameter("@vi_IdLineaNegocio", DbType.String, ParameterDirection.Input, ent.iIdLineaNegocio);
                    dbTerminal.AddParameter("@vi_IdOrdSer", DbType.String, ParameterDirection.Input, ent.sIdOrdSer);
                    dbTerminal.AddParameter("@vi_Documento", DbType.String, ParameterDirection.Input, ent.sNumeroDO);
                    dbTerminal.AddParameter("@vi_Servicio", DbType.String, ParameterDirection.Input, ent.sServicio);
                    dbTerminal.AddParameter("@vi_Contenedor", DbType.String, ParameterDirection.Input, ent.sContenedor);
                    dbTerminal.AddParameter("@vi_IdSituacion", DbType.String, ParameterDirection.Input, ent.iIdSituacion);
                    dbTerminal.AddParameter("@vi_FechaInicio", DbType.String, ParameterDirection.Input, ent.sFechaInicio);
                    dbTerminal.AddParameter("@vi_FechaFinal", DbType.String, ParameterDirection.Input, ent.sFechaFinal);

                    reader = dbTerminal.GetDataReader();

                    BE_OrdenServicioDetalleLis Lis_BE_OrdenServicioDetalleLis = new BE_OrdenServicioDetalleLis();
                    while (reader.Read())
                    {
                        Lis_BE_OrdenServicioDetalleLis.Add(Pu_OrdenServicio.Entidad_ListarOrdenesServicioAtencion(reader));
                    }
                    reader.Close();

                    return Lis_BE_OrdenServicioDetalleLis;
                }
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public String ActualizarSituacion_2(BE_OrdenServicioDetalle oBE_OrdenServicioDetalle)
        {
            try
            {
                using (Database dbTerminal = new Database())
                {
                    dbTerminal.ProcedureName = "OPE_Upd_Orden_Servicio_Detalle_2";
                    dbTerminal.AddParameter("@vi_IdOrdSerDet", DbType.Int32, ParameterDirection.Input, oBE_OrdenServicioDetalle.iIdOrdSerDet);
                    dbTerminal.AddParameter("@vi_IdSituacion", DbType.Int32, ParameterDirection.Input, oBE_OrdenServicioDetalle.iIdSituacion);
                    dbTerminal.AddParameter("@vi_FechaAtencion", DbType.String, ParameterDirection.Input, oBE_OrdenServicioDetalle.sFechaAtencion);
                    dbTerminal.AddParameter("@vi_HoraInicio", DbType.String, ParameterDirection.Input, oBE_OrdenServicioDetalle.sHoraInicio);
                    dbTerminal.AddParameter("@vi_HoraFinal", DbType.String, ParameterDirection.Input, oBE_OrdenServicioDetalle.sHoraFinal);
                    dbTerminal.AddParameter("@Usuario", DbType.String, ParameterDirection.Input, oBE_OrdenServicioDetalle.sUsuario);
                    dbTerminal.AddParameter("@NombrePC", DbType.String, ParameterDirection.Input, oBE_OrdenServicioDetalle.sNombrePc);
                    dbTerminal.AddParameter("@vo_Resultado", DbType.String, ParameterDirection.Output, String.Empty, 100);
                    dbTerminal.Execute();

                    String Retorno = dbTerminal.GetParameter("@vo_Resultado").ToString();
                    return Retorno;
                }
            }
            catch (Exception e)
            {
                return "-999|Ocurrio un error en la capa de datos";
            }
        }
        #endregion



        public List<BE_OrdenServicioDetalle> ListarServicioAduanero()
        {
            IDataReader reader = null;
            try
            {
                using (Database dbTerminal = new Database())
                {
                    dbTerminal.ProcedureName = "[Extranet_Lis_Servicios_Aduaneros_2]";

                    reader = dbTerminal.GetDataReader();

                    List<BE_OrdenServicioDetalle> Lis_OrdenServicio = new List<BE_OrdenServicioDetalle>();
                    while (reader.Read())
                    {
                        Lis_OrdenServicio.Add(Pu_OrdenServicio.ListarServicioAduanero(reader));
                    }
                    reader.Close();

                    return Lis_OrdenServicio;
                }
            }
            catch (Exception e)
            {
                ResultadoTransaccionTx oResultadoTransaccionTx = new ResultadoTransaccionTx(ResultadoTransaccionTx.TipoResultado.Error, e);
                oResultadoTransaccionTx.RegistrarError(e);
                return null;
            }
        }
    }

}
