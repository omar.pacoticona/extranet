﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;

namespace FENIX.DataAccess
{

    public static class DataBaseHelper
    {
        public static string GetDbProviderWeb()
        {
            return ConfigurationManager.ConnectionStrings["FFAPRD_EXTRANET"].ProviderName;
        }
        public static string GetDbConnectionStringWeb()
        {
            return ConfigurationManager.ConnectionStrings["FFAPRD_EXTRANET"].ConnectionString;
        }

    }

}

