﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using FENIX.BusinessEntity;
using FENIX.Common;

namespace FENIX.DataAccess
{
    public class DA_ConsultasComunes
    {
        public List<BE_Tabla> ListarTabla(int iIdTabla, int iIdtablaPadre)
        {
            IDataReader reader = null;

            try
            {
                using (Database dbTerminal = new Database())
                {
                    dbTerminal.ProcedureName = "[Administracion_Lis_TablaValor]";
                    dbTerminal.AddParameter("@IdTabla", DbType.Int32, ParameterDirection.Input, iIdTabla);
                    dbTerminal.AddParameter("@idTablaPadre", DbType.Int32, ParameterDirection.Input, iIdtablaPadre);                  
                    reader = dbTerminal.GetDataReader();
                }
                List<BE_Tabla> Lst_Tabla = new List<BE_Tabla>();
                while (reader.Read())
                {
                    Lst_Tabla.Add(Pu_ListarTabla.Listar(reader));

                }
                reader.Close();
                return Lst_Tabla;

            }
            catch (Exception e)
            {
                return null;
            }
        }

        public List<BE_Tabla> ListarTablaFormagoPago(int iIdTabla, int iIdtablaPadre, int Tipo)
        {
            IDataReader reader = null;

            try
            {
                using (Database dbTerminal = new Database())
                {
                    dbTerminal.ProcedureName = "[Refrendo_ListarFormaPago]";
                    dbTerminal.AddParameter("@IdTabla", DbType.Int32, ParameterDirection.Input, iIdTabla);
                    dbTerminal.AddParameter("@idTablaPadre", DbType.Int32, ParameterDirection.Input, iIdtablaPadre);
                    dbTerminal.AddParameter("@Tipo", DbType.Int32, ParameterDirection.Input, Tipo);
                    reader = dbTerminal.GetDataReader();
                }
                List<BE_Tabla> Lst_Tabla = new List<BE_Tabla>();
                while (reader.Read())
                {
                    Lst_Tabla.Add(Pu_ListarTabla.Listar(reader));

                }
                reader.Close();
                return Lst_Tabla;

            }
            catch (Exception e)
            {
                return null;
            }
        }

        public List<BE_TablaUc> ListarTablaUsercControl(BE_Tabla oBE_Tabla)
        {
            IDataReader reader = null;

            try
            {
                using (Database dbTerminal = new Database())
                {
                    dbTerminal.ProcedureName = "Administracion_Lis_TablaValorUserControl";
                    dbTerminal.AddParameter("@IdTabla", DbType.Int32, ParameterDirection.Input, oBE_Tabla.iIdTabla);
                    dbTerminal.AddParameter("@idTablaPadre", DbType.Int32, ParameterDirection.Input, oBE_Tabla.iIdTablaPadre);
                    dbTerminal.AddParameter("@IdValor", DbType.String, ParameterDirection.Input, oBE_Tabla.sIdValor);
                    dbTerminal.AddParameter("@ValVcr_Descripcion", DbType.String, ParameterDirection.Input, oBE_Tabla.sDescripcion);
                    dbTerminal.AddParameter("@vi_numeropagina", DbType.Int32, ParameterDirection.Input, oBE_Tabla.NPagina);
                    dbTerminal.AddParameter("@vi_numeroregistros", DbType.Int32, ParameterDirection.Input, oBE_Tabla.NRegistros);
                    dbTerminal.AddParameter("@vo_totalregistros", DbType.Int32, ParameterDirection.Output, oBE_Tabla.NTotalRegistros);
                    reader = dbTerminal.GetDataReader();

                    List<BE_TablaUc> Lst_Tabla = new List<BE_TablaUc>();
                    while (reader.Read())
                    {
                        Lst_Tabla.Add(Pu_ListarTabla.ListarUc(reader));

                    }
                    reader.Close();
                    oBE_Tabla.NTotalRegistros = Convert.ToInt32(dbTerminal.GetParameter("@vo_totalregistros").ToString());
                    return Lst_Tabla;
                }
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public List<BE_TablaUc> ListarTablaUsercControlEmbalaje(BE_Tabla oBE_Tabla)
        {
            IDataReader reader = null;

            try
            {
                using (Database dbTerminal = new Database())
                {
                    dbTerminal.ProcedureName = "Administracion_Lis_TablaValorUserControlEmbalaje";
                    dbTerminal.AddParameter("@IdTabla", DbType.Int32, ParameterDirection.Input, oBE_Tabla.iIdTabla);
                    dbTerminal.AddParameter("@idTablaPadre", DbType.Int32, ParameterDirection.Input, oBE_Tabla.iIdTablaPadre);
                    dbTerminal.AddParameter("@IdValor", DbType.String, ParameterDirection.Input, oBE_Tabla.sIdValor);
                    dbTerminal.AddParameter("@ValVcr_Descripcion", DbType.String, ParameterDirection.Input, oBE_Tabla.sDescripcion);
                    dbTerminal.AddParameter("@vi_numeropagina", DbType.Int32, ParameterDirection.Input, oBE_Tabla.NPagina);
                    dbTerminal.AddParameter("@vi_numeroregistros", DbType.Int32, ParameterDirection.Input, oBE_Tabla.NRegistros);
                    dbTerminal.AddParameter("@vo_totalregistros", DbType.Int32, ParameterDirection.Output, oBE_Tabla.NTotalRegistros);
                    reader = dbTerminal.GetDataReader();

                    List<BE_TablaUc> Lst_Tabla = new List<BE_TablaUc>();
                    while (reader.Read())
                    {
                        Lst_Tabla.Add(Pu_ListarTabla.ListarUc(reader));

                    }
                    reader.Close();
                    oBE_Tabla.NTotalRegistros = Convert.ToInt32(dbTerminal.GetParameter("@vo_totalregistros").ToString());
                    return Lst_Tabla;
                }
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public List<BE_TablaUc> ListarTablaUsercControlCarga(BE_Tabla oBE_Tabla)
        {
            IDataReader reader = null;

            try
            {
                using (Database dbTerminal = new Database())
                {
                    dbTerminal.ProcedureName = "[Administracion_Lis_TablaValorUserControlCarga]";
                    dbTerminal.AddParameter("@IdTabla", DbType.Int32, ParameterDirection.Input, oBE_Tabla.iIdTabla);
                    dbTerminal.AddParameter("@idTablaPadre", DbType.Int32, ParameterDirection.Input, oBE_Tabla.iIdTablaPadre);
                    dbTerminal.AddParameter("@IdValor", DbType.String, ParameterDirection.Input, oBE_Tabla.sIdValor);
                    dbTerminal.AddParameter("@ValVcr_Descripcion", DbType.String, ParameterDirection.Input, oBE_Tabla.sDescripcion);
                    dbTerminal.AddParameter("@vi_numeropagina", DbType.Int32, ParameterDirection.Input, oBE_Tabla.NPagina);
                    dbTerminal.AddParameter("@vi_numeroregistros", DbType.Int32, ParameterDirection.Input, oBE_Tabla.NRegistros);
                    dbTerminal.AddParameter("@vo_totalregistros", DbType.Int32, ParameterDirection.Output, oBE_Tabla.NTotalRegistros);
                    reader = dbTerminal.GetDataReader();

                    List<BE_TablaUc> Lst_Tabla = new List<BE_TablaUc>();
                    while (reader.Read())
                    {
                        Lst_Tabla.Add(Pu_ListarTabla.ListarUc(reader));

                    }
                    reader.Close();
                    oBE_Tabla.NTotalRegistros = Convert.ToInt32(dbTerminal.GetParameter("@vo_totalregistros").ToString());
                    return Lst_Tabla;
                }
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public List<BE_TablaUc> ListarTablaUsercControlTipoCarga(BE_Tabla oBE_Tabla)
        {
            IDataReader reader = null;

            try
            {
                using (Database dbTerminal = new Database())
                {
                    dbTerminal.ProcedureName = "[Administracion_Lis_TablaValorUserControlTipoCarga]";
                    dbTerminal.AddParameter("@IdTabla", DbType.Int32, ParameterDirection.Input, oBE_Tabla.iIdTabla);
                    dbTerminal.AddParameter("@idTablaPadre", DbType.Int32, ParameterDirection.Input, oBE_Tabla.iIdTablaPadre);
                    dbTerminal.AddParameter("@IdValor", DbType.String, ParameterDirection.Input, oBE_Tabla.sIdValor);
                    dbTerminal.AddParameter("@ValVcr_Descripcion", DbType.String, ParameterDirection.Input, oBE_Tabla.sDescripcion);
                    dbTerminal.AddParameter("@vi_numeropagina", DbType.Int32, ParameterDirection.Input, oBE_Tabla.NPagina);
                    dbTerminal.AddParameter("@vi_numeroregistros", DbType.Int32, ParameterDirection.Input, oBE_Tabla.NRegistros);
                    dbTerminal.AddParameter("@vo_totalregistros", DbType.Int32, ParameterDirection.Output, oBE_Tabla.NTotalRegistros);
                    reader = dbTerminal.GetDataReader();

                    List<BE_TablaUc> Lst_Tabla = new List<BE_TablaUc>();
                    while (reader.Read())
                    {
                        Lst_Tabla.Add(Pu_ListarTabla.ListarUc(reader));

                    }
                    reader.Close();
                    return Lst_Tabla;
                }
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public List<BE_TablaUc> ListarTablaUsercControlContenedor(BE_Tabla oBE_Tabla)
        {
            IDataReader reader = null;

            try
            {
                using (Database dbTerminal = new Database())
                {
                    dbTerminal.ProcedureName = "[Comercial_Lis_UserControl_Contenedor]";
                    dbTerminal.AddParameter("@vi_CodigoAduana", DbType.String, ParameterDirection.Input, oBE_Tabla.sIdValor);
                    dbTerminal.AddParameter("@vi_Descripcion", DbType.String, ParameterDirection.Input, oBE_Tabla.sDescripcion);
                    dbTerminal.AddParameter("@vi_numeropagina", DbType.Int32, ParameterDirection.Input, oBE_Tabla.NPagina);
                    dbTerminal.AddParameter("@vi_numeroregistros", DbType.Int32, ParameterDirection.Input, oBE_Tabla.NRegistros);
                    dbTerminal.AddParameter("@vo_totalregistros", DbType.Int32, ParameterDirection.Output, oBE_Tabla.NTotalRegistros);
                    reader = dbTerminal.GetDataReader();

                    List<BE_TablaUc> Lst_Tabla = new List<BE_TablaUc>();
                    while (reader.Read())
                    {
                        Lst_Tabla.Add(Pu_ListarTabla.ListarUc(reader));

                    }
                    reader.Close();
                    oBE_Tabla.NTotalRegistros = Convert.ToInt32(dbTerminal.GetParameter("@vo_totalregistros").ToString());
                    return Lst_Tabla;
                }
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public List<BE_TablaUc> ListarTablaUsercControlInspector(BE_Tabla oBE_Tabla)
        {
            IDataReader reader = null;

            try
            {
                using (Database dbTerminal = new Database())
                {
                    dbTerminal.ProcedureName = "[Comercial_Lis_UserControl_Inspector]";
                    dbTerminal.AddParameter("@vi_Dni", DbType.String, ParameterDirection.Input, oBE_Tabla.sIdValor);
                    dbTerminal.AddParameter("@vi_Descripcion", DbType.String, ParameterDirection.Input, oBE_Tabla.sDescripcion);
                    dbTerminal.AddParameter("@vi_numeropagina", DbType.Int32, ParameterDirection.Input, oBE_Tabla.NPagina);
                    dbTerminal.AddParameter("@vi_numeroregistros", DbType.Int32, ParameterDirection.Input, oBE_Tabla.NRegistros);
                    dbTerminal.AddParameter("@vo_totalregistros", DbType.Int32, ParameterDirection.Output, oBE_Tabla.NTotalRegistros);
                    reader = dbTerminal.GetDataReader();

                    List<BE_TablaUc> Lst_Tabla = new List<BE_TablaUc>();
                    while (reader.Read())
                    {
                        Lst_Tabla.Add(Pu_ListarTabla.ListarUc(reader));

                    }
                    reader.Close();
                    oBE_Tabla.NTotalRegistros = Convert.ToInt32(dbTerminal.GetParameter("@vo_totalregistros").ToString());
                    return Lst_Tabla;
                }
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public List<BE_TablaUc> ListarTablaUsercControlZona(BE_Tabla oBE_Tabla)
        {
            IDataReader reader = null;

            try
            {
                using (Database dbTerminal = new Database())
                {
                    dbTerminal.ProcedureName = "[Comercial_Lis_UserControl_Zona]";
                    dbTerminal.AddParameter("@vi_Codigo", DbType.String, ParameterDirection.Input, oBE_Tabla.sIdValor);
                    dbTerminal.AddParameter("@vi_Descripcion", DbType.String, ParameterDirection.Input, oBE_Tabla.sDescripcion);
                    dbTerminal.AddParameter("@vi_numeropagina", DbType.Int32, ParameterDirection.Input, oBE_Tabla.NPagina);
                    dbTerminal.AddParameter("@vi_numeroregistros", DbType.Int32, ParameterDirection.Input, oBE_Tabla.NRegistros);
                    dbTerminal.AddParameter("@vo_totalregistros", DbType.Int32, ParameterDirection.Output, oBE_Tabla.NTotalRegistros);
                    reader = dbTerminal.GetDataReader();

                    List<BE_TablaUc> Lst_Tabla = new List<BE_TablaUc>();
                    while (reader.Read())
                    {
                        Lst_Tabla.Add(Pu_ListarTabla.ListarUc(reader));

                    }
                    reader.Close();
                    oBE_Tabla.NTotalRegistros = Convert.ToInt32(dbTerminal.GetParameter("@vo_totalregistros").ToString());
                    return Lst_Tabla;
                }
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public List<BE_TablaUc> ListarTablaUsercControlPuerto(BE_Tabla oBE_Tabla)
        {
            IDataReader reader = null;

            try
            {
                using (Database dbTerminal = new Database())
                {
                    dbTerminal.ProcedureName = "[Comercial_Lis_UserControl_Puerto]";
                    dbTerminal.AddParameter("@vi_CodigoAduana", DbType.String, ParameterDirection.Input, oBE_Tabla.sIdValor);
                    dbTerminal.AddParameter("@vi_Descripcion", DbType.String, ParameterDirection.Input, oBE_Tabla.sDescripcion);
                    dbTerminal.AddParameter("@vi_numeropagina", DbType.Int32, ParameterDirection.Input, oBE_Tabla.NPagina);
                    dbTerminal.AddParameter("@vi_numeroregistros", DbType.Int32, ParameterDirection.Input, oBE_Tabla.NRegistros);
                    dbTerminal.AddParameter("@vo_totalregistros", DbType.Int32, ParameterDirection.Output, oBE_Tabla.NTotalRegistros);
                    reader = dbTerminal.GetDataReader();

                    List<BE_TablaUc> Lst_Tabla = new List<BE_TablaUc>();
                    while (reader.Read())
                    {
                        Lst_Tabla.Add(Pu_ListarTabla.ListarUc(reader));

                    }
                    reader.Close();
                    oBE_Tabla.NTotalRegistros = Convert.ToInt32(dbTerminal.GetParameter("@vo_totalregistros").ToString());
                    return Lst_Tabla;
                }
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public List<BE_TablaUc> ListarClienteTodos(BE_Cliente oBE_Cliente)
        {
            IDataReader reader = null;

            try
            {
                using (Database dbTerminal = new Database())
                {
                    dbTerminal.ProcedureName = "[Comercial_Lis_UserControl_Cliente_Todos]";
                    dbTerminal.AddParameter("@NumDocumento", DbType.String, ParameterDirection.Input, oBE_Cliente.sNumDocumento);
                    dbTerminal.AddParameter("@Razon_Social", DbType.String, ParameterDirection.Input, oBE_Cliente.sRazon_Social);
                    dbTerminal.AddParameter("@vi_numeropagina", DbType.Int32, ParameterDirection.Input, oBE_Cliente.NPagina);
                    dbTerminal.AddParameter("@vi_numeroregistros", DbType.Int32, ParameterDirection.Input, oBE_Cliente.NRegistros);
                    dbTerminal.AddParameter("@vo_totalregistros", DbType.Int32, ParameterDirection.Output, oBE_Cliente.NTotalRegistros);

                    reader = dbTerminal.GetDataReader();

                    List<BE_TablaUc> Lst_Tabla = new List<BE_TablaUc>();
                    while (reader.Read())
                    {
                        Lst_Tabla.Add(Pu_ListarTabla.ListarUc(reader));

                    }
                    reader.Close();
                    oBE_Cliente.NTotalRegistros = Convert.ToInt32(dbTerminal.GetParameter("@vo_totalregistros"));

                    return Lst_Tabla;
                }
            }
            catch (Exception e)
            {
                ResultadoTransaccionTx oResultadoTransaccionTx = null;
                oResultadoTransaccionTx.RegistrarError(e);
                return null;
            }
        }

        public List<BE_TablaUc> ListarTablaUsercControlServicio(BE_Tabla oBE_Tabla)
        {
            IDataReader reader = null;

            try
            {
                using (Database dbTerminal = new Database())
                {
                    dbTerminal.ProcedureName = "[Comercial_Lis_UserControlServicio]";
                    dbTerminal.AddParameter("@IdTabla", DbType.Int32, ParameterDirection.Input, oBE_Tabla.iIdTabla);
                    dbTerminal.AddParameter("@idTablaPadre", DbType.Int32, ParameterDirection.Input, oBE_Tabla.iIdTablaPadre);
                    dbTerminal.AddParameter("@IdValor", DbType.String, ParameterDirection.Input, oBE_Tabla.sIdValor);
                    dbTerminal.AddParameter("@ValVcr_Descripcion", DbType.String, ParameterDirection.Input, oBE_Tabla.sDescripcion);
                    dbTerminal.AddParameter("@vi_numeropagina", DbType.Int32, ParameterDirection.Input, oBE_Tabla.NPagina);
                    dbTerminal.AddParameter("@vi_numeroregistros", DbType.Int32, ParameterDirection.Input, oBE_Tabla.NRegistros);
                    dbTerminal.AddParameter("@vo_totalregistros", DbType.Int32, ParameterDirection.Output, oBE_Tabla.NTotalRegistros);
                    reader = dbTerminal.GetDataReader();

                    List<BE_TablaUc> Lst_Tabla = new List<BE_TablaUc>();
                    while (reader.Read())
                    {
                        Lst_Tabla.Add(Pu_ListarTabla.ListarUc(reader));

                    }
                    reader.Close();
                    oBE_Tabla.NTotalRegistros = Convert.ToInt32(dbTerminal.GetParameter("@vo_totalregistros").ToString());
                    return Lst_Tabla;
                }
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public List<BE_TablaUc> ListarTablaUserControlDocumentoOrigen_Desglose(BE_Tabla oBE_Tabla)
        {
            IDataReader reader = null;

            try
            {
                using (Database dbTerminal = new Database())
                {
                    dbTerminal.ProcedureName = "[Comercial_Lis_UserControl_DocumentoOrigen_Desglose]";
                    dbTerminal.AddParameter("@NumDocumento", DbType.String, ParameterDirection.Input, oBE_Tabla.sDescripcion);
                    dbTerminal.AddParameter("@CodigoSecundario", DbType.String, ParameterDirection.Input, oBE_Tabla.iIdTablaPadre);
                    dbTerminal.AddParameter("@vi_IdNaveViaje", DbType.String, ParameterDirection.Input, oBE_Tabla.iIdNavVia);
                    dbTerminal.AddParameter("@vi_IdTipoOperacion", DbType.String, ParameterDirection.Input, oBE_Tabla.iIdOperacion);
                    dbTerminal.AddParameter("@vi_numeropagina", DbType.Int32, ParameterDirection.Input, oBE_Tabla.NPagina);
                    dbTerminal.AddParameter("@vi_numeroregistros", DbType.Int32, ParameterDirection.Input, oBE_Tabla.NRegistros);
                    dbTerminal.AddParameter("@vo_totalregistros", DbType.Int32, ParameterDirection.Output, oBE_Tabla.NTotalRegistros);
                    reader = dbTerminal.GetDataReader();

                    List<BE_TablaUc> Lst_Tabla = new List<BE_TablaUc>();
                    while (reader.Read())
                    {
                        Lst_Tabla.Add(Pu_ListarTabla.ListarUc(reader));
                    }
                    reader.Close();
                    oBE_Tabla.NTotalRegistros = Convert.ToInt32(dbTerminal.GetParameter("@vo_totalregistros").ToString());
                    return Lst_Tabla;
                }
            }
            catch (Exception e)
            {
                return null;
            }
        }
     
        public List<BE_TablaUc> ListarTablaUserControlDocumentoOrigen(BE_Tabla oBE_Tabla)
        {
            IDataReader reader = null;

            try
            {
                using (Database dbTerminal = new Database())
                {
                    dbTerminal.ProcedureName = "[Comercial_Lis_UserControl_DocumentoOrigen]";
                    dbTerminal.AddParameter("@NumDocumento", DbType.String, ParameterDirection.Input, oBE_Tabla.sDescripcion);
                    dbTerminal.AddParameter("@CodigoSecundario", DbType.String, ParameterDirection.Input, oBE_Tabla.iIdTablaPadre);
                    dbTerminal.AddParameter("@vi_numeropagina", DbType.Int32, ParameterDirection.Input, oBE_Tabla.NPagina);
                    dbTerminal.AddParameter("@vi_numeroregistros", DbType.Int32, ParameterDirection.Input, oBE_Tabla.NRegistros);
                    dbTerminal.AddParameter("@vo_totalregistros", DbType.Int32, ParameterDirection.Output, oBE_Tabla.NTotalRegistros);
                    reader = dbTerminal.GetDataReader();

                    List<BE_TablaUc> Lst_Tabla = new List<BE_TablaUc>();
                    while (reader.Read())
                    {
                        Lst_Tabla.Add(Pu_ListarTabla.ListarUc(reader));
                    }
                    reader.Close();
                    oBE_Tabla.NTotalRegistros = Convert.ToInt32(dbTerminal.GetParameter("@vo_totalregistros").ToString());
                    return Lst_Tabla;
                }
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public List<BE_TablaUc> ListarTablaUserControlDocumentoOrigenFinales(BE_Tabla oBE_Tabla)
        {
            IDataReader reader = null;

            try
            {
                using (Database dbTerminal = new Database())
                {
                    dbTerminal.ProcedureName = "[Comercial_Lis_UserControl_DocumentoOrigenFinales]";
                    dbTerminal.AddParameter("@NumDocumento", DbType.String, ParameterDirection.Input, oBE_Tabla.sDescripcion);
                    dbTerminal.AddParameter("@CodigoSecundario", DbType.String, ParameterDirection.Input, oBE_Tabla.iIdTablaPadre);
                    dbTerminal.AddParameter("@vi_numeropagina", DbType.Int32, ParameterDirection.Input, oBE_Tabla.NPagina);
                    dbTerminal.AddParameter("@vi_numeroregistros", DbType.Int32, ParameterDirection.Input, oBE_Tabla.NRegistros);
                    dbTerminal.AddParameter("@vo_totalregistros", DbType.Int32, ParameterDirection.Output, oBE_Tabla.NTotalRegistros);
                    reader = dbTerminal.GetDataReader();

                    List<BE_TablaUc> Lst_Tabla = new List<BE_TablaUc>();
                    while (reader.Read())
                    {
                        Lst_Tabla.Add(Pu_ListarTabla.ListarUc(reader));
                    }
                    reader.Close();
                    oBE_Tabla.NTotalRegistros = Convert.ToInt32(dbTerminal.GetParameter("@vo_totalregistros").ToString());
                    return Lst_Tabla;
                }
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public List<BE_TablaUc> ListarTablaUserControlDocumentoOrigen_InvLib(BE_Tabla oBE_Tabla)
        {
            IDataReader reader = null;

            try
            {
                using (Database dbTerminal = new Database())
                {
                    dbTerminal.ProcedureName = "[Comercial_Lis_UserControl_DocumentoOrigen_InvLib]";
                    dbTerminal.AddParameter("@NumDocumento", DbType.String, ParameterDirection.Input, oBE_Tabla.sDescripcion);
                    dbTerminal.AddParameter("@IdDocOri", DbType.String, ParameterDirection.Input, oBE_Tabla.sIdValor);
                    dbTerminal.AddParameter("@CodigoSecundario", DbType.String, ParameterDirection.Input, oBE_Tabla.iIdTablaPadre);
                    dbTerminal.AddParameter("@CodigoSecundario1", DbType.String, ParameterDirection.Input, oBE_Tabla.iIdTabla);
                    dbTerminal.AddParameter("@vi_numeropagina", DbType.Int32, ParameterDirection.Input, oBE_Tabla.NPagina);
                    dbTerminal.AddParameter("@vi_numeroregistros", DbType.Int32, ParameterDirection.Input, oBE_Tabla.NRegistros);
                    dbTerminal.AddParameter("@vo_totalregistros", DbType.Int32, ParameterDirection.Output, oBE_Tabla.NTotalRegistros);
                    reader = dbTerminal.GetDataReader();

                    List<BE_TablaUc> Lst_Tabla = new List<BE_TablaUc>();
                    while (reader.Read())
                    {
                        Lst_Tabla.Add(Pu_ListarTabla.ListarUc(reader));
                    }
                    reader.Close();
                    oBE_Tabla.NTotalRegistros = Convert.ToInt32(dbTerminal.GetParameter("@vo_totalregistros").ToString());
                    return Lst_Tabla;
                }
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public List<BE_TablaUc> ListarTablaUserControlDocumentoOrigen_Previo(BE_Tabla oBE_Tabla)
        {
            IDataReader reader = null;

            try
            {
                using (Database dbTerminal = new Database())
                {
                    dbTerminal.ProcedureName = "[Comercial_Lis_UserControl_DocumentoOrigen_Previo]";
                    dbTerminal.AddParameter("@NumDocumento", DbType.String, ParameterDirection.Input, oBE_Tabla.sDescripcion);
                    dbTerminal.AddParameter("@IdDocOri", DbType.String, ParameterDirection.Input, oBE_Tabla.sIdValor);
                    dbTerminal.AddParameter("@CodigoSecundario", DbType.String, ParameterDirection.Input, oBE_Tabla.iIdTablaPadre);
                    dbTerminal.AddParameter("@CodigoSecundario1", DbType.String, ParameterDirection.Input, oBE_Tabla.iIdTabla);
                    dbTerminal.AddParameter("@vi_numeropagina", DbType.Int32, ParameterDirection.Input, oBE_Tabla.NPagina);
                    dbTerminal.AddParameter("@vi_numeroregistros", DbType.Int32, ParameterDirection.Input, oBE_Tabla.NRegistros);
                    dbTerminal.AddParameter("@vo_totalregistros", DbType.Int32, ParameterDirection.Output, oBE_Tabla.NTotalRegistros);
                    reader = dbTerminal.GetDataReader();

                    List<BE_TablaUc> Lst_Tabla = new List<BE_TablaUc>();
                    while (reader.Read())
                    {
                        Lst_Tabla.Add(Pu_ListarTabla.ListarUc(reader));
                    }
                    reader.Close();
                    oBE_Tabla.NTotalRegistros = Convert.ToInt32(dbTerminal.GetParameter("@vo_totalregistros").ToString());
                    return Lst_Tabla;
                }
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public List<BE_TablaUc> ListarTablaUserControlDocumentoOrigen_ProcesoEsp(BE_Tabla oBE_Tabla)
        {
            IDataReader reader = null;

            try
            {
                using (Database dbTerminal = new Database())
                {
                    dbTerminal.ProcedureName = "[Documentacion_Lis_UserControl_DocumentoOrigen_ProcesoEsp]";
                    
                    //Antes recibia iIdValor pero debe ser la descripcion del BL
                    dbTerminal.AddParameter("@NumDocumento", DbType.String, ParameterDirection.Input, oBE_Tabla.sDescripcion);
                    
                    dbTerminal.AddParameter("@CodigoSecundario", DbType.String, ParameterDirection.Input, oBE_Tabla.iIdTablaPadre);
                    dbTerminal.AddParameter("@CodigoSecundario1", DbType.String, ParameterDirection.Input, oBE_Tabla.iIdTabla);
                    dbTerminal.AddParameter("@vi_numeropagina", DbType.Int32, ParameterDirection.Input, oBE_Tabla.NPagina);
                    dbTerminal.AddParameter("@vi_numeroregistros", DbType.Int32, ParameterDirection.Input, oBE_Tabla.NRegistros);
                    dbTerminal.AddParameter("@vo_totalregistros", DbType.Int32, ParameterDirection.Output, oBE_Tabla.NTotalRegistros);
                    reader = dbTerminal.GetDataReader();

                    List<BE_TablaUc> Lst_Tabla = new List<BE_TablaUc>();
                    while (reader.Read())
                    {
                        Lst_Tabla.Add(Pu_ListarTabla.ListarUc(reader));
                    }
                    reader.Close();
                    oBE_Tabla.NTotalRegistros = Convert.ToInt32(dbTerminal.GetParameter("@vo_totalregistros").ToString());
                    return Lst_Tabla;
                }
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public List<BE_TablaUc> ListarTablaUserControlDocumentoOrigen_Volante(BE_Tabla oBE_Tabla)
        {
            IDataReader reader = null;

            try
            {
                using (Database dbTerminal = new Database())
                {
                    dbTerminal.ProcedureName = "[Documentacion_Lis_UserControl_DocumentoOrigen_Volante]";
                    dbTerminal.AddParameter("@NumDocumento", DbType.String, ParameterDirection.Input, oBE_Tabla.sDescripcion);
                    dbTerminal.AddParameter("@CodigoSecundario", DbType.String, ParameterDirection.Input, oBE_Tabla.iIdTablaPadre);
                    dbTerminal.AddParameter("@CodigoSecundario1", DbType.String, ParameterDirection.Input, oBE_Tabla.iIdTabla);
                    dbTerminal.AddParameter("@vi_numeropagina", DbType.Int32, ParameterDirection.Input, oBE_Tabla.NPagina);
                    dbTerminal.AddParameter("@vi_numeroregistros", DbType.Int32, ParameterDirection.Input, oBE_Tabla.NRegistros);
                    dbTerminal.AddParameter("@vo_totalregistros", DbType.Int32, ParameterDirection.Output, oBE_Tabla.NTotalRegistros);
                    reader = dbTerminal.GetDataReader();

                    List<BE_TablaUc> Lst_Tabla = new List<BE_TablaUc>();
                    while (reader.Read())
                    {
                        Lst_Tabla.Add(Pu_ListarTabla.ListarUc(reader));
                    }
                    reader.Close();
                    oBE_Tabla.NTotalRegistros = Convert.ToInt32(dbTerminal.GetParameter("@vo_totalregistros").ToString());
                    return Lst_Tabla;
                }
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public List<BE_TablaUc> ListarTablaUserControlDocumentoOrigenDet_Previo(BE_Tabla oBE_Tabla)
        {
            IDataReader reader = null;

            try
            {
                using (Database dbTerminal = new Database())
                {
                    dbTerminal.ProcedureName = "[Operacion_Lis_UserControl_DocumentoOrigenDet_Previo]";
                    dbTerminal.AddParameter("@NumDocumento", DbType.String, ParameterDirection.Input, oBE_Tabla.sIdValor);
                    dbTerminal.AddParameter("@CodigoSecundario", DbType.String, ParameterDirection.Input, oBE_Tabla.iIdTablaPadre);
                    dbTerminal.AddParameter("@vi_numeropagina", DbType.Int32, ParameterDirection.Input, oBE_Tabla.NPagina);
                    dbTerminal.AddParameter("@vi_numeroregistros", DbType.Int32, ParameterDirection.Input, oBE_Tabla.NRegistros);
                    dbTerminal.AddParameter("@vo_totalregistros", DbType.Int32, ParameterDirection.Output, oBE_Tabla.NTotalRegistros);
                    reader = dbTerminal.GetDataReader();

                    List<BE_TablaUc> Lst_Tabla = new List<BE_TablaUc>();
                    while (reader.Read())
                    {
                        Lst_Tabla.Add(Pu_ListarTabla.ListarUc(reader));
                    }
                    reader.Close();
                    oBE_Tabla.NTotalRegistros = Convert.ToInt32(dbTerminal.GetParameter("@vo_totalregistros").ToString());
                    return Lst_Tabla;
                }
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public List<BE_TablaUc> ListarTablaUserControlDocumentoOrigenDet_InvLib(BE_Tabla oBE_Tabla)
        {
            IDataReader reader = null;

            try
            {
                using (Database dbTerminal = new Database())
                {
                    dbTerminal.ProcedureName = "[Documentacion_Lis_UserControl_DocumentoOrigenDet_InvLib]";
                    dbTerminal.AddParameter("@NumDocumento", DbType.String, ParameterDirection.Input, oBE_Tabla.sIdValor);
                    dbTerminal.AddParameter("@CodigoSecundario", DbType.String, ParameterDirection.Input, oBE_Tabla.iIdTablaPadre);
                    dbTerminal.AddParameter("@vi_numeropagina", DbType.Int32, ParameterDirection.Input, oBE_Tabla.NPagina);
                    dbTerminal.AddParameter("@vi_numeroregistros", DbType.Int32, ParameterDirection.Input, oBE_Tabla.NRegistros);
                    dbTerminal.AddParameter("@vo_totalregistros", DbType.Int32, ParameterDirection.Output, oBE_Tabla.NTotalRegistros);
                    reader = dbTerminal.GetDataReader();

                    List<BE_TablaUc> Lst_Tabla = new List<BE_TablaUc>();
                    while (reader.Read())
                    {
                        Lst_Tabla.Add(Pu_ListarTabla.ListarUc(reader));
                    }
                    reader.Close();
                    oBE_Tabla.NTotalRegistros = Convert.ToInt32(dbTerminal.GetParameter("@vo_totalregistros").ToString());
                    return Lst_Tabla;
                }
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public List<BE_TablaUc> ListarTablaUserControlDocumentoOrigenDet_InvLibPrec(BE_Tabla oBE_Tabla)
        {
            IDataReader reader = null;

            try
            {
                using (Database dbTerminal = new Database())
                {
                    dbTerminal.ProcedureName = "[Documentacion_Lis_UserControl_DocumentoOrigenDet_InvLib_Prec]";
                    dbTerminal.AddParameter("@NumDocumento", DbType.String, ParameterDirection.Input, oBE_Tabla.sIdValor);
                    dbTerminal.AddParameter("@CodigoSecundario", DbType.String, ParameterDirection.Input, oBE_Tabla.iIdTablaPadre);
                    dbTerminal.AddParameter("@CodigoSecundario1", DbType.String, ParameterDirection.Input, oBE_Tabla.iIdTabla);
                    dbTerminal.AddParameter("@vi_numeropagina", DbType.Int32, ParameterDirection.Input, oBE_Tabla.NPagina);
                    dbTerminal.AddParameter("@vi_numeroregistros", DbType.Int32, ParameterDirection.Input, oBE_Tabla.NRegistros);
                    dbTerminal.AddParameter("@vo_totalregistros", DbType.Int32, ParameterDirection.Output, oBE_Tabla.NTotalRegistros);
                    reader = dbTerminal.GetDataReader();

                    List<BE_TablaUc> Lst_Tabla = new List<BE_TablaUc>();
                    while (reader.Read())
                    {
                        Lst_Tabla.Add(Pu_ListarTabla.ListarUc(reader));
                    }
                    reader.Close();
                    oBE_Tabla.NTotalRegistros = Convert.ToInt32(dbTerminal.GetParameter("@vo_totalregistros").ToString());
                    return Lst_Tabla;
                }
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public List<BE_TablaUc> ListarTablaUserControlInmovilizacion_Precedente(BE_Tabla oBE_Tabla)
        {
            IDataReader reader = null;

            try
            {
                using (Database dbTerminal = new Database())
                {
                    dbTerminal.ProcedureName = "[Documentacion_Lis_UserControl_InmovilizacionPrecedente]";
                    dbTerminal.AddParameter("@NumDocumento", DbType.String, ParameterDirection.Input, oBE_Tabla.sIdValor);
                    dbTerminal.AddParameter("@CodigoSecundario", DbType.String, ParameterDirection.Input, oBE_Tabla.iIdTablaPadre);
                    dbTerminal.AddParameter("@vi_numeropagina", DbType.Int32, ParameterDirection.Input, oBE_Tabla.NPagina);
                    dbTerminal.AddParameter("@vi_numeroregistros", DbType.Int32, ParameterDirection.Input, oBE_Tabla.NRegistros);
                    dbTerminal.AddParameter("@vo_totalregistros", DbType.Int32, ParameterDirection.Output, oBE_Tabla.NTotalRegistros);
                    reader = dbTerminal.GetDataReader();

                    List<BE_TablaUc> Lst_Tabla = new List<BE_TablaUc>();
                    while (reader.Read())
                    {
                        Lst_Tabla.Add(Pu_ListarTabla.ListarUc(reader));
                    }
                    reader.Close();
                    oBE_Tabla.NTotalRegistros = Convert.ToInt32(dbTerminal.GetParameter("@vo_totalregistros").ToString());
                    return Lst_Tabla;
                }
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public List<BE_TablaUc> ListarTablaUserControlDocumentoOrigenDet_MalEstado(BE_Tabla oBE_Tabla)
        {
            IDataReader reader = null;

            try
            {
                using (Database dbTerminal = new Database())
                {
                    dbTerminal.ProcedureName = "[Operacion_Lis_UserControl_DocumentoOrigenDet_MalEstado]";
                    dbTerminal.AddParameter("@NumDocumento", DbType.String, ParameterDirection.Input, oBE_Tabla.sIdValor);
                    dbTerminal.AddParameter("@CodigoSecundario", DbType.String, ParameterDirection.Input, oBE_Tabla.iIdTablaPadre);
                    dbTerminal.AddParameter("@vi_numeropagina", DbType.Int32, ParameterDirection.Input, oBE_Tabla.NPagina);
                    dbTerminal.AddParameter("@vi_numeroregistros", DbType.Int32, ParameterDirection.Input, oBE_Tabla.NRegistros);
                    dbTerminal.AddParameter("@vo_totalregistros", DbType.Int32, ParameterDirection.Output, oBE_Tabla.NTotalRegistros);
                    reader = dbTerminal.GetDataReader();

                    List<BE_TablaUc> Lst_Tabla = new List<BE_TablaUc>();
                    while (reader.Read())
                    {
                        Lst_Tabla.Add(Pu_ListarTabla.ListarUc(reader));
                    }
                    reader.Close();
                    oBE_Tabla.NTotalRegistros = Convert.ToInt32(dbTerminal.GetParameter("@vo_totalregistros").ToString());
                    return Lst_Tabla;
                }
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public List<BE_TablaUc> ListarTablaUserControlDocumentoOrigenTarjaCS(BE_Tabla oBE_Tabla)
        {
            IDataReader reader = null;

            try
            {
                using (Database dbTerminal = new Database())
                {
                    dbTerminal.ProcedureName = "[Comercial_Lis_UserControl_DocumentoOrigen_tarja_CS]";
                    dbTerminal.AddParameter("@NumDocumento", DbType.String, ParameterDirection.Input, oBE_Tabla.sDescripcion);
                    dbTerminal.AddParameter("@CodigoSecundario", DbType.String, ParameterDirection.Input, oBE_Tabla.iIdTablaPadre);
                    dbTerminal.AddParameter("@vi_idMovimiento", DbType.String, ParameterDirection.Input, oBE_Tabla.IdMovimiento);                    
                    dbTerminal.AddParameter("@vi_numeropagina", DbType.Int32, ParameterDirection.Input, oBE_Tabla.NPagina);
                    dbTerminal.AddParameter("@vi_numeroregistros", DbType.Int32, ParameterDirection.Input, oBE_Tabla.NRegistros);
                    dbTerminal.AddParameter("@vo_totalregistros", DbType.Int32, ParameterDirection.Output, oBE_Tabla.NTotalRegistros);
                    reader = dbTerminal.GetDataReader();

                    List<BE_TablaUc> Lst_Tabla = new List<BE_TablaUc>();
                    while (reader.Read())
                    {
                        Lst_Tabla.Add(Pu_ListarTabla.ListarUc(reader));
                    }
                    reader.Close();
                    oBE_Tabla.NTotalRegistros = Convert.ToInt32(dbTerminal.GetParameter("@vo_totalregistros").ToString());
                    return Lst_Tabla;
                }
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public List<BE_TablaUc> ListarTablaUserControlDocumentoOrigenTarjaVEH(BE_Tabla oBE_Tabla)
        {
            IDataReader reader = null;

            try
            {
                using (Database dbTerminal = new Database())
                {
                    dbTerminal.ProcedureName = "[Comercial_Lis_UserControl_DocumentoOrigen_tarja_VEH]";
                    dbTerminal.AddParameter("@NumDocumento", DbType.String, ParameterDirection.Input, oBE_Tabla.sDescripcion);
                    dbTerminal.AddParameter("@CodigoSecundario", DbType.String, ParameterDirection.Input, oBE_Tabla.iIdTablaPadre);
                    dbTerminal.AddParameter("@vi_IdOperacion", DbType.Int32, ParameterDirection.Input, oBE_Tabla.iIdOperacion);
                    dbTerminal.AddParameter("@vi_numeropagina", DbType.Int32, ParameterDirection.Input, oBE_Tabla.NPagina);
                    dbTerminal.AddParameter("@vi_numeroregistros", DbType.Int32, ParameterDirection.Input, oBE_Tabla.NRegistros);
                    dbTerminal.AddParameter("@vo_totalregistros", DbType.Int32, ParameterDirection.Output, oBE_Tabla.NTotalRegistros);
                    reader = dbTerminal.GetDataReader();

                    List<BE_TablaUc> Lst_Tabla = new List<BE_TablaUc>();
                    while (reader.Read())
                    {
                        Lst_Tabla.Add(Pu_ListarTabla.ListarUc(reader));
                    }
                    reader.Close();
                    oBE_Tabla.NTotalRegistros = Convert.ToInt32(dbTerminal.GetParameter("@vo_totalregistros").ToString());
                    return Lst_Tabla;
                }
            }
            catch (Exception e)
            {
                return null;
            }
        }
        public List<BE_TablaUc> ListarTablaUserControlDocumentoOrigenTarjaVEH_Retiro(BE_Tabla oBE_Tabla)
        {
            IDataReader reader = null;

            try
            {
                using (Database dbTerminal = new Database())
                {
                    dbTerminal.ProcedureName = "[Comercial_Lis_UserControl_DocumentoOrigen_tarja_Retiro_VEH]";
                    dbTerminal.AddParameter("@NumDocumento", DbType.String, ParameterDirection.Input, oBE_Tabla.sIdValor);
                    dbTerminal.AddParameter("@CodigoSecundario", DbType.String, ParameterDirection.Input, oBE_Tabla.iIdTablaPadre);
                    dbTerminal.AddParameter("@vi_IdOperacion", DbType.Int32, ParameterDirection.Input, oBE_Tabla.iIdOperacion);
                    dbTerminal.AddParameter("@vi_numeropagina", DbType.Int32, ParameterDirection.Input, oBE_Tabla.NPagina);
                    dbTerminal.AddParameter("@vi_numeroregistros", DbType.Int32, ParameterDirection.Input, oBE_Tabla.NRegistros);
                    dbTerminal.AddParameter("@vo_totalregistros", DbType.Int32, ParameterDirection.Output, oBE_Tabla.NTotalRegistros);
                    reader = dbTerminal.GetDataReader();

                    List<BE_TablaUc> Lst_Tabla = new List<BE_TablaUc>();
                    while (reader.Read())
                    {
                        Lst_Tabla.Add(Pu_ListarTabla.ListarUc(reader));
                    }
                    reader.Close();
                    oBE_Tabla.NTotalRegistros = Convert.ToInt32(dbTerminal.GetParameter("@vo_totalregistros").ToString());
                    return Lst_Tabla;
                }
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public List<BE_TablaUc> ListarTablaUserControlDireccionante(BE_Tabla oBE_Tabla)
        {
            IDataReader reader = null;

            try
            {
                using (Database dbTerminal = new Database())
                {
                    dbTerminal.ProcedureName = "[Comercial_Lis_Cliente_Direccionar]";
                    dbTerminal.AddParameter("@IdTabla", DbType.Int32, ParameterDirection.Input, oBE_Tabla.iIdTabla);
                    dbTerminal.AddParameter("@idTablaPadre", DbType.Int32, ParameterDirection.Input, oBE_Tabla.iIdTablaPadre);
                    dbTerminal.AddParameter("@IdValor", DbType.String, ParameterDirection.Input, oBE_Tabla.sIdValor);
                    dbTerminal.AddParameter("@ValVcr_Descripcion", DbType.String, ParameterDirection.Input, oBE_Tabla.sDescripcion);
                    dbTerminal.AddParameter("@vi_numeropagina", DbType.Int32, ParameterDirection.Input, oBE_Tabla.NPagina);
                    dbTerminal.AddParameter("@vi_numeroregistros", DbType.Int32, ParameterDirection.Input, oBE_Tabla.NRegistros);
                    dbTerminal.AddParameter("@vo_totalregistros", DbType.Int32, ParameterDirection.Output, oBE_Tabla.NTotalRegistros);
                    reader = dbTerminal.GetDataReader();

                    List<BE_TablaUc> Lst_Tabla = new List<BE_TablaUc>();
                    while (reader.Read())
                    {
                        Lst_Tabla.Add(Pu_ListarTabla.ListarUc(reader));
                    }
                    reader.Close();
                    oBE_Tabla.NTotalRegistros = Convert.ToInt32(dbTerminal.GetParameter("@vo_totalregistros").ToString());
                    return Lst_Tabla;
                }
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public List<BE_TablaUc> ListarTablaUserControlLineaMaritima(BE_Tabla oBE_Tabla)
        {
            IDataReader reader = null;

            try
            {
                using (Database dbTerminal = new Database())
                {
                    dbTerminal.ProcedureName = "[Documentacion_Lis_LineaMaritima_UserControl]";
                    dbTerminal.AddParameter("@IdValor", DbType.String, ParameterDirection.Input, oBE_Tabla.sIdValor);
                    dbTerminal.AddParameter("@ValVcr_Descripcion", DbType.String, ParameterDirection.Input, oBE_Tabla.sDescripcion);
                    dbTerminal.AddParameter("@vi_numeropagina", DbType.Int32, ParameterDirection.Input, oBE_Tabla.NPagina);
                    dbTerminal.AddParameter("@vi_numeroregistros", DbType.Int32, ParameterDirection.Input, oBE_Tabla.NRegistros);
                    dbTerminal.AddParameter("@vo_totalregistros", DbType.Int32, ParameterDirection.Output, oBE_Tabla.NTotalRegistros);
                    reader = dbTerminal.GetDataReader();

                    List<BE_TablaUc> Lst_Tabla = new List<BE_TablaUc>();
                    while (reader.Read())
                    {
                        Lst_Tabla.Add(Pu_ListarTabla.ListarUc(reader));
                    }
                    reader.Close();
                    oBE_Tabla.NTotalRegistros = Convert.ToInt32(dbTerminal.GetParameter("@vo_totalregistros").ToString());
                    return Lst_Tabla;
                }
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public List<BE_TablaUc> Listar_Mobil_Eir_LineaMaritima(BE_Tabla oBE_Tabla)
        {
            IDataReader reader = null;

            try
            {
                using (Database dbTerminal = new Database())
                {
                    dbTerminal.ProcedureName = "[Documentacion_Lis_LineaMaritima_Dpl]";
                  
                    reader = dbTerminal.GetDataReader();

                    List<BE_TablaUc> Lst_Tabla = new List<BE_TablaUc>();
                    while (reader.Read())
                    {
                        Lst_Tabla.Add(Pu_ListarTabla.ListarUc(reader));
                    }
                    reader.Close();                  
                    return Lst_Tabla;
                }
            }
            catch (Exception e)
            {
                return null;
            }
        }


        public List<BE_Cliente> ListarRoles(int iIdRol, String sNumeroDocumento, String sRazonSocial)
        {
            IDataReader reader = null;

            try
            {
                using (Database dbTerminal = new Database())
                {
                    dbTerminal.ProcedureName = "[Operacion_lis_ClienteRol]";
                    dbTerminal.AddParameter("@IdRol", DbType.Int32, ParameterDirection.Input, iIdRol);
                    dbTerminal.AddParameter("@Client_vch_NumDocumento", DbType.String, ParameterDirection.Input, sNumeroDocumento);
                    dbTerminal.AddParameter("@Client_vch_Razon_Social", DbType.String, ParameterDirection.Input, sRazonSocial);

                    reader = dbTerminal.GetDataReader();


                    List<BE_Cliente> Lst_Cliente = new List<BE_Cliente>();
                    while (reader.Read())
                    {
                        Lst_Cliente.Add(Pu_Cliente.ListarRol(reader));

                    }

                    // oBE_Cliente.NTotalRegistros = Convert.ToInt32(dbTerminal.GetParameter("@vo_totalregistros").ToString());

                    reader.Close();
                    return Lst_Cliente;
                }
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public List<BE_TablaUc> ListarTablaUserControlPlacaVehiculo(BE_Tabla oBE_Tabla)
        {
            IDataReader reader = null;

            try
            {
                using (Database dbTerminal = new Database())
                {
                    dbTerminal.ProcedureName = "[Operacion_Lis_UserControl_PlacaVehiculo]";
                    dbTerminal.AddParameter("@vi_Descripcion", DbType.String, ParameterDirection.Input, oBE_Tabla.sDescripcion);
                    dbTerminal.AddParameter("@vi_CodigoAduana", DbType.String, ParameterDirection.Input, oBE_Tabla.sIdValor);
                    dbTerminal.AddParameter("@vi_numeropagina", DbType.Int32, ParameterDirection.Input, oBE_Tabla.NPagina);
                    dbTerminal.AddParameter("@vi_numeroregistros", DbType.Int32, ParameterDirection.Input, oBE_Tabla.NRegistros);
                    dbTerminal.AddParameter("@vo_totalregistros", DbType.Int32, ParameterDirection.Output, oBE_Tabla.NTotalRegistros);
                    reader = dbTerminal.GetDataReader();

                    List<BE_TablaUc> Lst_Tabla = new List<BE_TablaUc>();
                    while (reader.Read())
                    {
                        Lst_Tabla.Add(Pu_ListarTabla.ListarUc(reader));
                    }
                    reader.Close();
                    oBE_Tabla.NTotalRegistros = Convert.ToInt32(dbTerminal.GetParameter("@vo_totalregistros").ToString());
                    return Lst_Tabla;
                }
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public List<BE_TablaUc> ListarTablaUserControlDocumentoOrigen_Dua(BE_Tabla oBE_Tabla)
        {
            IDataReader reader = null;

            try
            {
                using (Database dbTerminal = new Database())
                {
                    dbTerminal.ProcedureName = "[Operacion_Lis_UserControl_DocumentoOrigen_Dua]";

                    dbTerminal.AddParameter("@NumDocumento", DbType.String, ParameterDirection.Input, oBE_Tabla.sDescripcion);
                    dbTerminal.AddParameter("@CodigoSecundario", DbType.String, ParameterDirection.Input, oBE_Tabla.iCodigoSecundario);
                    dbTerminal.AddParameter("@vi_numeropagina", DbType.Int32, ParameterDirection.Input, oBE_Tabla.NPagina);
                    dbTerminal.AddParameter("@vi_numeroregistros", DbType.Int32, ParameterDirection.Input, oBE_Tabla.NRegistros);
                    dbTerminal.AddParameter("@vo_totalregistros", DbType.Int32, ParameterDirection.Output, oBE_Tabla.NTotalRegistros);
                    reader = dbTerminal.GetDataReader();

                    List<BE_TablaUc> Lst_Tabla = new List<BE_TablaUc>();
                    while (reader.Read())
                    {
                        Lst_Tabla.Add(Pu_ListarTabla.ListarUc(reader));
                    }
                    reader.Close();
                    oBE_Tabla.NTotalRegistros = Convert.ToInt32(dbTerminal.GetParameter("@vo_totalregistros").ToString());
                    return Lst_Tabla;
                }
            }
            catch (Exception e)
            {
                return null;
            }
        }
        public List<BE_TablaUc> ListarTablaUserControlDocumentoOrigen_Liquidacion(BE_Tabla oBE_Tabla)
        {
            IDataReader reader = null;

            try
            {
                using (Database dbTerminal = new Database())
                {
                    dbTerminal.ProcedureName = "[Operacion_Lis_UserControl_DocumentoOrigen_Liquidacion]";
                    dbTerminal.AddParameter("@NumDocumento", DbType.String, ParameterDirection.Input, oBE_Tabla.sIdValor);
                    dbTerminal.AddParameter("@CodigoSecundario", DbType.String, ParameterDirection.Input, oBE_Tabla.iCodigoSecundario);
                    dbTerminal.AddParameter("@vi_numeropagina", DbType.Int32, ParameterDirection.Input, oBE_Tabla.NPagina);
                    dbTerminal.AddParameter("@vi_numeroregistros", DbType.Int32, ParameterDirection.Input, oBE_Tabla.NRegistros);
                    dbTerminal.AddParameter("@vo_totalregistros", DbType.Int32, ParameterDirection.Output, oBE_Tabla.NTotalRegistros);
                    reader = dbTerminal.GetDataReader();

                    List<BE_TablaUc> Lst_Tabla = new List<BE_TablaUc>();
                    while (reader.Read())
                    {
                        Lst_Tabla.Add(Pu_ListarTabla.ListarUc(reader));
                    }
                    reader.Close();
                    oBE_Tabla.NTotalRegistros = Convert.ToInt32(dbTerminal.GetParameter("@vo_totalregistros").ToString());
                    return Lst_Tabla;
                }
            }
            catch (Exception e)
            {
                return null;
            }
        }

        

        public List<BE_TablaUc> ListarTablaUserControlDocumentoOrigen_Liquidacion_Fac(BE_Tabla oBE_Tabla)
        {
            IDataReader reader = null;

            try
            {
                using (Database dbTerminal = new Database())
                {
                    dbTerminal.ProcedureName = "[Operacion_Lis_UserControl_DocumentoOrigen_Liquidacion_Fact]";
                    dbTerminal.AddParameter("@NumDocumento", DbType.String, ParameterDirection.Input, oBE_Tabla.sIdValor);
                    dbTerminal.AddParameter("@CodigoSecundario", DbType.String, ParameterDirection.Input, oBE_Tabla.iCodigoSecundario);
                    dbTerminal.AddParameter("@vi_numeropagina", DbType.Int32, ParameterDirection.Input, oBE_Tabla.NPagina);
                    dbTerminal.AddParameter("@vi_numeroregistros", DbType.Int32, ParameterDirection.Input, oBE_Tabla.NRegistros);
                    dbTerminal.AddParameter("@vo_totalregistros", DbType.Int32, ParameterDirection.Output, oBE_Tabla.NTotalRegistros);
                    reader = dbTerminal.GetDataReader();

                    List<BE_TablaUc> Lst_Tabla = new List<BE_TablaUc>();
                    while (reader.Read())
                    {
                        Lst_Tabla.Add(Pu_ListarTabla.ListarUc(reader));
                    }
                    reader.Close();
                    oBE_Tabla.NTotalRegistros = Convert.ToInt32(dbTerminal.GetParameter("@vo_totalregistros").ToString());
                    return Lst_Tabla;
                }
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public List<BE_TablaUc> ListarTablaUserControlDocumentoOrigen_Bookin(BE_Tabla oBE_Tabla)
        {
            IDataReader reader = null;

            try
            {
                using (Database dbTerminal = new Database())
                {
                    dbTerminal.ProcedureName = "[Operacion_Lis_UserControl_DocumentoOrigen_bookin]";
                    dbTerminal.AddParameter("@NumDocumento", DbType.String, ParameterDirection.Input, oBE_Tabla.sIdValor);
                    dbTerminal.AddParameter("@Tipo_bookin", DbType.String, ParameterDirection.Input, oBE_Tabla.sTipoBooking); // para la variable Tipo Booking
                    dbTerminal.AddParameter("@CodigoSecundario", DbType.String, ParameterDirection.Input, oBE_Tabla.iCodigoSecundario);
                    dbTerminal.AddParameter("@vi_numeropagina", DbType.Int32, ParameterDirection.Input, oBE_Tabla.NPagina);
                    dbTerminal.AddParameter("@vi_numeroregistros", DbType.Int32, ParameterDirection.Input, oBE_Tabla.NRegistros);
                    dbTerminal.AddParameter("@vo_totalregistros", DbType.Int32, ParameterDirection.Output, oBE_Tabla.NTotalRegistros);
                    reader = dbTerminal.GetDataReader();

                    List<BE_TablaUc> Lst_Tabla = new List<BE_TablaUc>();
                    while (reader.Read())
                    {
                        Lst_Tabla.Add(Pu_ListarTabla.ListarUc(reader));
                    }
                    reader.Close();
                    oBE_Tabla.NTotalRegistros = Convert.ToInt32(dbTerminal.GetParameter("@vo_totalregistros").ToString());
                    return Lst_Tabla;
                }
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public List<BE_TablaUc> ListarTablaUserControlDocumentoOrigen_Bookin_Roleo(BE_Tabla oBE_Tabla)
        {
            IDataReader reader = null;

            try
            {
                using (Database dbTerminal = new Database())
                {
                    dbTerminal.ProcedureName = "[Operacion_Lis_UserControl_DocumentoOrigen_bookin_Roleo]";
                    dbTerminal.AddParameter("@IdValor", DbType.String, ParameterDirection.Input, oBE_Tabla.sIdValor);
                    dbTerminal.AddParameter("@NumDocumento", DbType.String, ParameterDirection.Input, oBE_Tabla.sDescripcion);
                    dbTerminal.AddParameter("@Tipo_bookin", DbType.String, ParameterDirection.Input, oBE_Tabla.sTipoBooking); // para la variable Tipo Booking                    
                    dbTerminal.AddParameter("@vi_numeropagina", DbType.Int32, ParameterDirection.Input, oBE_Tabla.NPagina);
                    dbTerminal.AddParameter("@vi_numeroregistros", DbType.Int32, ParameterDirection.Input, oBE_Tabla.NRegistros);
                    dbTerminal.AddParameter("@vo_totalregistros", DbType.Int32, ParameterDirection.Output, oBE_Tabla.NTotalRegistros);
                    reader = dbTerminal.GetDataReader();

                    List<BE_TablaUc> Lst_Tabla = new List<BE_TablaUc>();
                    while (reader.Read())
                    {
                        Lst_Tabla.Add(Pu_ListarTabla.ListarUc(reader));
                    }
                    reader.Close();
                    oBE_Tabla.NTotalRegistros = Convert.ToInt32(dbTerminal.GetParameter("@vo_totalregistros").ToString());
                    return Lst_Tabla;
                }
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public List<BE_TablaUc> ListarTablaUserControlDocumentoOrigen_BookingDet_CS(BE_Tabla oBE_Tabla)
        {
            IDataReader reader = null;

            try
            {
                using (Database dbTerminal = new Database())
                {
                    dbTerminal.ProcedureName = "[Operacion_Lis_UserControl_DocumentoOrigen_BookingDet_CS]";
                    dbTerminal.AddParameter("@NumDocumento", DbType.String, ParameterDirection.Input, oBE_Tabla.sIdValor);
                    dbTerminal.AddParameter("@CodigoSecundario", DbType.String, ParameterDirection.Input, oBE_Tabla.iCodigoSecundario);
                    dbTerminal.AddParameter("@vi_numeropagina", DbType.Int32, ParameterDirection.Input, oBE_Tabla.NPagina);
                    dbTerminal.AddParameter("@vi_numeroregistros", DbType.Int32, ParameterDirection.Input, oBE_Tabla.NRegistros);
                    dbTerminal.AddParameter("@vo_totalregistros", DbType.Int32, ParameterDirection.Output, oBE_Tabla.NTotalRegistros);
                    reader = dbTerminal.GetDataReader();

                    List<BE_TablaUc> Lst_Tabla = new List<BE_TablaUc>();
                    while (reader.Read())
                    {
                        Lst_Tabla.Add(Pu_ListarTabla.ListarUc(reader));
                    }
                    reader.Close();
                    oBE_Tabla.NTotalRegistros = Convert.ToInt32(dbTerminal.GetParameter("@vo_totalregistros").ToString());
                    return Lst_Tabla;
                }
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public List<BE_TablaUc> ListarTablaUserControlChofer(BE_Tabla oBE_Tabla)
        {
            IDataReader reader = null;

            try
            {
                using (Database dbTerminal = new Database())
                {
                    dbTerminal.ProcedureName = "[Operacion_Lis_UserControl_Chofer]";
                    dbTerminal.AddParameter("@vi_Descripcion", DbType.String, ParameterDirection.Input, oBE_Tabla.sDescripcion);
                    dbTerminal.AddParameter("@vi_CodigoAduana", DbType.String, ParameterDirection.Input, oBE_Tabla.sIdValor);
                    dbTerminal.AddParameter("@vi_numeropagina", DbType.Int32, ParameterDirection.Input, oBE_Tabla.NPagina);
                    dbTerminal.AddParameter("@vi_numeroregistros", DbType.Int32, ParameterDirection.Input, oBE_Tabla.NRegistros);
                    dbTerminal.AddParameter("@vo_totalregistros", DbType.Int32, ParameterDirection.Output, oBE_Tabla.NTotalRegistros);
                    reader = dbTerminal.GetDataReader();

                    List<BE_TablaUc> Lst_Tabla = new List<BE_TablaUc>();
                    while (reader.Read())
                    {
                        Lst_Tabla.Add(Pu_ListarTabla.ListarUc(reader));
                    }
                    reader.Close();
                    oBE_Tabla.NTotalRegistros = Convert.ToInt32(dbTerminal.GetParameter("@vo_totalregistros").ToString());
                    return Lst_Tabla;
                }
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public List<BE_TablaUc> ListarTablaUserControlDocumentoOrigen_MovBalanza(BE_Tabla oBE_Tabla)
        {
            IDataReader reader = null;

            try
            {
                using (Database dbTerminal = new Database())
                {
                    dbTerminal.ProcedureName = "[Operacion_Lis_UserControl_DocumentoOrigen_MovBalanza]";
                    dbTerminal.AddParameter("@NumDocumento", DbType.String, ParameterDirection.Input, oBE_Tabla.sIdValor);
                    dbTerminal.AddParameter("@Descripcion", DbType.String, ParameterDirection.Input, oBE_Tabla.sDescripcion);
                    dbTerminal.AddParameter("@vi_IdNavViaje", DbType.Int32, ParameterDirection.Input, oBE_Tabla.iIdNavVia);
                    dbTerminal.AddParameter("@vi_numeropagina", DbType.Int32, ParameterDirection.Input, oBE_Tabla.NPagina);
                    dbTerminal.AddParameter("@vi_numeroregistros", DbType.Int32, ParameterDirection.Input, oBE_Tabla.NRegistros);
                    dbTerminal.AddParameter("@vo_totalregistros", DbType.Int32, ParameterDirection.Output, oBE_Tabla.NTotalRegistros);
                    reader = dbTerminal.GetDataReader();

                    List<BE_TablaUc> Lst_Tabla = new List<BE_TablaUc>();
                    while (reader.Read())
                    {
                        Lst_Tabla.Add(Pu_ListarTabla.ListarUc(reader));
                    }
                    reader.Close();
                    oBE_Tabla.NTotalRegistros = Convert.ToInt32(dbTerminal.GetParameter("@vo_totalregistros").ToString());
                    return Lst_Tabla;
                }
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public List<BE_TablaUc> ListarTablaUserControlPlacaTarja(BE_Tabla oBE_Tabla)
        {
            IDataReader reader = null;

            try
            {
                using (Database dbTerminal = new Database())
                {
                    dbTerminal.ProcedureName = "[Documentacion_Lis_Placa_UserControl]";
                    dbTerminal.AddParameter("@IdValor", DbType.String, ParameterDirection.Input, oBE_Tabla.sIdValor);
                    dbTerminal.AddParameter("@idTipoMovimiento", DbType.String, ParameterDirection.Input, oBE_Tabla.iCodigoSecundario);
                    dbTerminal.AddParameter("@ValVcr_Descripcion", DbType.String, ParameterDirection.Input, oBE_Tabla.sDescripcion);
                    dbTerminal.AddParameter("@vi_numeropagina", DbType.Int32, ParameterDirection.Input, oBE_Tabla.NPagina);
                    dbTerminal.AddParameter("@vi_numeroregistros", DbType.Int32, ParameterDirection.Input, oBE_Tabla.NRegistros);
                    dbTerminal.AddParameter("@vo_totalregistros", DbType.Int32, ParameterDirection.Output, oBE_Tabla.NTotalRegistros);
                    reader = dbTerminal.GetDataReader();

                    List<BE_TablaUc> Lst_Tabla = new List<BE_TablaUc>();
                    while (reader.Read())
                    {
                        Lst_Tabla.Add(Pu_ListarTabla.ListarUc(reader));
                    }
                    reader.Close();
                    oBE_Tabla.NTotalRegistros = Convert.ToInt32(dbTerminal.GetParameter("@vo_totalregistros").ToString());
                    return Lst_Tabla;
                }
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public List<BE_TablaUc> ListarTablaUsercControlContenedorNaveViaje(BE_Tabla oBE_Tabla)
        {
            IDataReader reader = null;

            try
            {
                using (Database dbTerminal = new Database())
                {
                    dbTerminal.ProcedureName = "[Comercial_Lis_UserControl_Contenedor_NaveViaje]";
                    dbTerminal.AddParameter("@vi_CodigoAduana", DbType.String, ParameterDirection.Input, oBE_Tabla.sIdValor);
                    dbTerminal.AddParameter("@vi_IdNavViaje", DbType.Int32, ParameterDirection.Input, oBE_Tabla.iCodigoSecundario);
                    dbTerminal.AddParameter("@vi_Descripcion", DbType.String, ParameterDirection.Input, oBE_Tabla.sDescripcion);
                    dbTerminal.AddParameter("@vi_numeropagina", DbType.Int32, ParameterDirection.Input, oBE_Tabla.NPagina);
                    dbTerminal.AddParameter("@vi_numeroregistros", DbType.Int32, ParameterDirection.Input, oBE_Tabla.NRegistros);
                    dbTerminal.AddParameter("@vo_totalregistros", DbType.Int32, ParameterDirection.Output, oBE_Tabla.NTotalRegistros);
                    reader = dbTerminal.GetDataReader();

                    List<BE_TablaUc> Lst_Tabla = new List<BE_TablaUc>();
                    while (reader.Read())
                    {
                        Lst_Tabla.Add(Pu_ListarTabla.ListarUc(reader));

                    }
                    reader.Close();
                    oBE_Tabla.NTotalRegistros = Convert.ToInt32(dbTerminal.GetParameter("@vo_totalregistros").ToString());
                    return Lst_Tabla;
                }
            }
            catch (Exception e)
            {
                return null;
            }
        }
        public List<BE_TablaUc> ListarTablaUsercControlContenedorNaveViaje_EIR(BE_Tabla oBE_Tabla)
        {
            IDataReader reader = null;

            try
            {
                using (Database dbTerminal = new Database())
                {
                    dbTerminal.ProcedureName = "[Comercial_Lis_UserControl_Contenedor_NaveViaje_EIR]";
                    dbTerminal.AddParameter("@vi_CodigoAduana", DbType.String, ParameterDirection.Input, oBE_Tabla.sIdValor);
                    dbTerminal.AddParameter("@vi_IdNavViaje", DbType.Int32, ParameterDirection.Input, oBE_Tabla.iCodigoSecundario);
                    dbTerminal.AddParameter("@vi_IdOperacion", DbType.Int32, ParameterDirection.Input, oBE_Tabla.iIdOperacion);
                    dbTerminal.AddParameter("@vi_Descripcion", DbType.String, ParameterDirection.Input, oBE_Tabla.sDescripcion);
                    dbTerminal.AddParameter("@vi_numeropagina", DbType.Int32, ParameterDirection.Input, oBE_Tabla.NPagina);
                    dbTerminal.AddParameter("@vi_numeroregistros", DbType.Int32, ParameterDirection.Input, oBE_Tabla.NRegistros);
                    dbTerminal.AddParameter("@vo_totalregistros", DbType.Int32, ParameterDirection.Output, oBE_Tabla.NTotalRegistros);
                    reader = dbTerminal.GetDataReader();

                    List<BE_TablaUc> Lst_Tabla = new List<BE_TablaUc>();
                    while (reader.Read())
                    {
                        Lst_Tabla.Add(Pu_ListarTabla.ListarUc(reader));

                    }
                    reader.Close();
                    oBE_Tabla.NTotalRegistros = Convert.ToInt32(dbTerminal.GetParameter("@vo_totalregistros").ToString());
                    return Lst_Tabla;
                }
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public List<BE_TablaUc> ListarTablaUsercControlContenedorNaveViaje_EIR_Retiro(BE_Tabla oBE_Tabla)
        {
            IDataReader reader = null;

            try
            {
                using (Database dbTerminal = new Database())
                {
                    dbTerminal.ProcedureName = "[Comercial_Lis_UserControl_Contenedor_NaveViaje_EIR_Retiro]";
                    dbTerminal.AddParameter("@vi_CodigoAduana", DbType.String, ParameterDirection.Input, oBE_Tabla.sIdValor);
                    dbTerminal.AddParameter("@vi_IdNavViaje", DbType.Int32, ParameterDirection.Input, oBE_Tabla.iCodigoSecundario);
                    dbTerminal.AddParameter("@vi_IdOperacion", DbType.Int32, ParameterDirection.Input, oBE_Tabla.iIdOperacion);
                    dbTerminal.AddParameter("@vi_Descripcion", DbType.String, ParameterDirection.Input, oBE_Tabla.sDescripcion);
                    dbTerminal.AddParameter("@vi_numeropagina", DbType.Int32, ParameterDirection.Input, oBE_Tabla.NPagina);
                    dbTerminal.AddParameter("@vi_numeroregistros", DbType.Int32, ParameterDirection.Input, oBE_Tabla.NRegistros);
                    dbTerminal.AddParameter("@vo_totalregistros", DbType.Int32, ParameterDirection.Output, oBE_Tabla.NTotalRegistros);
                    reader = dbTerminal.GetDataReader();

                    List<BE_TablaUc> Lst_Tabla = new List<BE_TablaUc>();
                    while (reader.Read())
                    {
                        Lst_Tabla.Add(Pu_ListarTabla.ListarUc(reader));

                    }
                    reader.Close();
                    oBE_Tabla.NTotalRegistros = Convert.ToInt32(dbTerminal.GetParameter("@vo_totalregistros").ToString());
                    return Lst_Tabla;
                }
            }
            catch (Exception e)
            {
                return null;
            }
        }
        public List<BE_TablaUc> ListarTablaUsercControlDespachador(BE_Tabla oBE_Tabla)
        {
            IDataReader reader = null;

            try
            {
                using (Database dbTerminal = new Database())
                {
                    dbTerminal.ProcedureName = "[Operacion_Lis_UserControl_Despachador]";
                    dbTerminal.AddParameter("@vi_CodigoAduana", DbType.String, ParameterDirection.Input, oBE_Tabla.sIdValor);
                    dbTerminal.AddParameter("@vi_Descripcion", DbType.String, ParameterDirection.Input, oBE_Tabla.sDescripcion);
                    dbTerminal.AddParameter("@vi_numeropagina", DbType.Int32, ParameterDirection.Input, oBE_Tabla.NPagina);
                    dbTerminal.AddParameter("@vi_numeroregistros", DbType.Int32, ParameterDirection.Input, oBE_Tabla.NRegistros);
                    dbTerminal.AddParameter("@vo_totalregistros", DbType.Int32, ParameterDirection.Output, oBE_Tabla.NTotalRegistros);
                    reader = dbTerminal.GetDataReader();

                    List<BE_TablaUc> Lst_Tabla = new List<BE_TablaUc>();
                    while (reader.Read())
                    {
                        Lst_Tabla.Add(Pu_ListarTabla.ListarUc(reader));

                    }
                    reader.Close();
                    oBE_Tabla.NTotalRegistros = Convert.ToInt32(dbTerminal.GetParameter("@vo_totalregistros").ToString());
                    return Lst_Tabla;
                }
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public List<BE_TablaUc> ListarTablaUsercControlAutRetiro_MovBal(BE_Tabla oBE_Tabla)
        {
            IDataReader reader = null;

            try

            {
                using (Database dbTerminal = new Database())
                {
                    dbTerminal.ProcedureName = "[Administracion_Lis_TablaValorUserControl_AutorizacionRetiro_MovBal]";
                    dbTerminal.AddParameter("@IdTabla", DbType.String, ParameterDirection.Input, oBE_Tabla.sIdValor);
                    dbTerminal.AddParameter("@idTablaPadre", DbType.String, ParameterDirection.Input, oBE_Tabla.sDescripcion);
                    dbTerminal.AddParameter("@IdValor", DbType.String, ParameterDirection.Input, oBE_Tabla.sDescripcion);
                    dbTerminal.AddParameter("@ValVcr_Descripcion", DbType.String, ParameterDirection.Input, oBE_Tabla.sDescripcion);
                    dbTerminal.AddParameter("@vi_numeropagina", DbType.Int32, ParameterDirection.Input, oBE_Tabla.NPagina);
                    dbTerminal.AddParameter("@vi_numeroregistros", DbType.Int32, ParameterDirection.Input, oBE_Tabla.NRegistros);
                    dbTerminal.AddParameter("@vo_totalregistros", DbType.Int32, ParameterDirection.Output, oBE_Tabla.NTotalRegistros);
                    reader = dbTerminal.GetDataReader();

                    List<BE_TablaUc> Lst_Tabla = new List<BE_TablaUc>();
                    while (reader.Read())
                    {
                        Lst_Tabla.Add(Pu_ListarTabla.ListarUc(reader));

                    }
                    reader.Close();
                    oBE_Tabla.NTotalRegistros = Convert.ToInt32(dbTerminal.GetParameter("@vo_totalregistros").ToString());
                    return Lst_Tabla;
                }
            }
            catch (Exception e)
            {
                return null;
            }
        }
     

        public List<BE_TablaUc> ListarTablaUsercControlAutRetiro(BE_Tabla oBE_Tabla)
        {
            IDataReader reader = null;

            try
            {
                using (Database dbTerminal = new Database())
                {
                    dbTerminal.ProcedureName = "[Administracion_Lis_TablaValorUserControl_AutorizacionRetiro]";
                    dbTerminal.AddParameter("@IdTabla", DbType.String, ParameterDirection.Input, oBE_Tabla.iIdTabla);
                    dbTerminal.AddParameter("@idTablaPadre", DbType.String, ParameterDirection.Input, oBE_Tabla.iIdTablaPadre);
                    dbTerminal.AddParameter("@IdValor", DbType.String, ParameterDirection.Input, oBE_Tabla.sIdValor);
                    dbTerminal.AddParameter("@ValVcr_Descripcion", DbType.String, ParameterDirection.Input, oBE_Tabla.sDescripcion);
                    dbTerminal.AddParameter("@vi_numeropagina", DbType.Int32, ParameterDirection.Input, oBE_Tabla.NPagina);
                    dbTerminal.AddParameter("@vi_numeroregistros", DbType.Int32, ParameterDirection.Input, oBE_Tabla.NRegistros);
                    dbTerminal.AddParameter("@vo_totalregistros", DbType.Int32, ParameterDirection.Output, oBE_Tabla.NTotalRegistros);
                    reader = dbTerminal.GetDataReader();

                    List<BE_TablaUc> Lst_Tabla = new List<BE_TablaUc>();
                    while (reader.Read())
                    {
                        Lst_Tabla.Add(Pu_ListarTabla.ListarUc(reader));

                    }
                    reader.Close();
                    oBE_Tabla.NTotalRegistros = Convert.ToInt32(dbTerminal.GetParameter("@vo_totalregistros").ToString());
                    return Lst_Tabla;
                }
            }
            catch (Exception e)
            {
                return null;
            }
        }
        public List<BE_TablaUc> ListarTablaUsercControlAutRetiro_PesajeCS(BE_Tabla oBE_Tabla)
        {
            IDataReader reader = null;

            try
            {
                using (Database dbTerminal = new Database())
                {
                    dbTerminal.ProcedureName = "[Administracion_Lis_TablaValorUserControl_AutorizacionRetiro_PesajeCS]";
                    dbTerminal.AddParameter("@IdTabla", DbType.String, ParameterDirection.Input, oBE_Tabla.iIdTabla);
                    dbTerminal.AddParameter("@idTablaPadre", DbType.String, ParameterDirection.Input, oBE_Tabla.iIdTablaPadre);
                    dbTerminal.AddParameter("@IdValor", DbType.String, ParameterDirection.Input, oBE_Tabla.sIdValor);
                    dbTerminal.AddParameter("@ValVcr_Descripcion", DbType.String, ParameterDirection.Input, oBE_Tabla.sDescripcion);
                    dbTerminal.AddParameter("@vi_numeropagina", DbType.Int32, ParameterDirection.Input, oBE_Tabla.NPagina);
                    dbTerminal.AddParameter("@vi_numeroregistros", DbType.Int32, ParameterDirection.Input, oBE_Tabla.NRegistros);
                    dbTerminal.AddParameter("@vo_totalregistros", DbType.Int32, ParameterDirection.Output, oBE_Tabla.NTotalRegistros);
                    reader = dbTerminal.GetDataReader();

                    List<BE_TablaUc> Lst_Tabla = new List<BE_TablaUc>();
                    while (reader.Read())
                    {
                        Lst_Tabla.Add(Pu_ListarTabla.ListarUc(reader));

                    }
                    reader.Close();
                    oBE_Tabla.NTotalRegistros = Convert.ToInt32(dbTerminal.GetParameter("@vo_totalregistros").ToString());
                    return Lst_Tabla;
                }
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public List<BE_TablaUc> ListarTablaUsercControlNave(BE_Tabla oBE_Tabla)
        {
            IDataReader reader = null;

            try
            {
                using (Database dbTerminal = new Database())
                {
                    dbTerminal.ProcedureName = "[Comercial_Lis_UserControl_Nave]";
                    dbTerminal.AddParameter("@vi_CodigoAduana", DbType.String, ParameterDirection.Input, oBE_Tabla.sIdValor);
                    dbTerminal.AddParameter("@vi_Descripcion", DbType.String, ParameterDirection.Input, oBE_Tabla.sDescripcion);
                    dbTerminal.AddParameter("@vi_numeropagina", DbType.Int32, ParameterDirection.Input, oBE_Tabla.NPagina);
                    dbTerminal.AddParameter("@vi_numeroregistros", DbType.Int32, ParameterDirection.Input, oBE_Tabla.NRegistros);
                    dbTerminal.AddParameter("@vo_totalregistros", DbType.Int32, ParameterDirection.Output, oBE_Tabla.NTotalRegistros);
                    reader = dbTerminal.GetDataReader();

                    List<BE_TablaUc> Lst_Tabla = new List<BE_TablaUc>();
                    while (reader.Read())
                    {
                        Lst_Tabla.Add(Pu_ListarTabla.ListarUc(reader));

                    }
                    reader.Close();
                    oBE_Tabla.NTotalRegistros = Convert.ToInt32(dbTerminal.GetParameter("@vo_totalregistros").ToString());
                    return Lst_Tabla;
                }
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public String ListarRegistroPorID(String sCodigo,String sTabla,String sCampoCondicion, String SCampoMostrar)
        {
            DataSet oDataSet = null;
            String sValores = null;
            try
            {
                using (Database dbTerminal = new Database())
                {
                    dbTerminal.ProcedureName = "[Proceso_Lis_Busqueda_Por_Codigo]";
                    dbTerminal.AddParameter("@vi_Codigo", DbType.String, ParameterDirection.Input, sCodigo);
                    dbTerminal.AddParameter("@vi_NombreTabla", DbType.String, ParameterDirection.Input, sTabla);
                    dbTerminal.AddParameter("@vi_NombreCampoCondicion", DbType.String, ParameterDirection.Input, sCampoCondicion);
                    dbTerminal.AddParameter("@vi_NombreCampoMostrado", DbType.String, ParameterDirection.Input, SCampoMostrar);

                    oDataSet = dbTerminal.GetDataSet();

                    if (oDataSet.Tables[0].Rows.Count > 0)
                    {
                        for (int i = 0; i < oDataSet.Tables[0].Columns.Count; i++)
                        {
                            sValores = sValores + oDataSet.Tables[0].Rows[0][i] + "|";
                        }
                    }

                    return sValores;
                }
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public List<BE_Menu> Listar_Opcion_Menu(Int32 iIdGrupo)
        {
            IDataReader reader = null;

            try
            {
                using (Database dbTerminal = new Database())
                {
                    dbTerminal.ProcedureName = "[Menu_Lis_Opciones]";
                    dbTerminal.AddParameter("@vi_Grupo", DbType.Int32, ParameterDirection.Input, iIdGrupo);                    
                    reader = dbTerminal.GetDataReader();

                    List<BE_Menu> Lst_BE_Menu = new List<BE_Menu>();
                    while (reader.Read())
                    {
                        Lst_BE_Menu.Add(Pu_ListarTabla.ListarMenu(reader));

                    }
                    reader.Close();                  
                    return Lst_BE_Menu;
                }
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public String Validar_usuario(String sUsuario, String sClave)
        {             
            try
            {
                using (Database dbTerminal = new Database())
                {
                    dbTerminal.ProcedureName = "[Usuario_Lis_Valida]";
                    dbTerminal.AddParameter("@vi_Usuario", DbType.String, ParameterDirection.Input, sUsuario);
                    dbTerminal.AddParameter("@vi_Clave", DbType.String, ParameterDirection.Input, sClave);                    
                    dbTerminal.AddParameter("@vo_Retorno", DbType.String, ParameterDirection.Output, String.Empty,300);
                    dbTerminal.Execute();                   
                    return  dbTerminal.GetParameter("@vo_Retorno").ToString();                     
                }
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public List <BE_Menu> Listar_Opcion_Boton(Int32 iIdMenu, Int32 iIdGrupo)
        {
            IDataReader reader = null;


            try
            {
                using (Database dbTerminal = new Database())
                {
                    dbTerminal.ProcedureName = "[Menu_Lis_OpcionesBoton]";
                    dbTerminal.AddParameter("@vi_IdMenu", DbType.Int32, ParameterDirection.Input, iIdMenu);
                    dbTerminal.AddParameter("@vi_IdGrupo", DbType.Int32, ParameterDirection.Input, iIdGrupo);

                    reader = dbTerminal.GetDataReader();

                    List<BE_Menu> Lst_BE_Menu = new List<BE_Menu>();
                    while (reader.Read())
                    {
                        Lst_BE_Menu.Add(Pu_ListarTabla.ListarBoton(reader));

                    }
                    reader.Close();
                    return Lst_BE_Menu;
                }
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public String Web_Validar_Usuario(String sUsuario, String sClave)
        {
            try
            {
                using (WebDatabase dbTerminal = new WebDatabase())
                {
                    dbTerminal.ProcedureName = "[Web_Seguridad_Usuario_Lis_Valida]";
                    dbTerminal.AddParameter("@vi_Usuario", DbType.String, ParameterDirection.Input, sUsuario);
                    dbTerminal.AddParameter("@vi_Clave", DbType.String, ParameterDirection.Input, sClave);
                    dbTerminal.AddParameter("@vo_Retorno", DbType.String, ParameterDirection.Output, String.Empty, 300);
                    dbTerminal.Execute();
                    return dbTerminal.GetParameter("@vo_Retorno").ToString();
                }
            }
            catch (Exception e)
            {
                return null;
            }
        }


        public List<BE_Tabla> ListarBanco(int iIdBanco)
        {
            IDataReader reader = null;

            try
            {
                using (Database dbTerminal = new Database())
                {
                    dbTerminal.ProcedureName = "[Documentacion_Lis_Banco]";
                    dbTerminal.AddParameter("@vi_IdAduana", DbType.Int32, ParameterDirection.Input, iIdBanco);
                    reader = dbTerminal.GetDataReader();
                }
                List<BE_Tabla> Lst_Tabla = new List<BE_Tabla>();
                while (reader.Read())
                {
                    Lst_Tabla.Add(Pu_ListarTabla.ListarBanco(reader));

                }
                reader.Close();
                return Lst_Tabla;

            }
            catch (Exception e)
            {
                return null;
            }
        }

        public List<BE_Tabla> ListarRegimen()
        {
            IDataReader reader = null;

            try
            {
                using (Database dbTerminal = new Database())
                {
                    dbTerminal.ProcedureName = "[Documentacion_Lis_Regimen]";
                    reader = dbTerminal.GetDataReader();
                }
                List<BE_Tabla> Lst_Tabla = new List<BE_Tabla>();
                while (reader.Read())
                {
                    Lst_Tabla.Add(Pu_ListarTabla.ListarRegimen(reader));

                }
                reader.Close();
                return Lst_Tabla;

            }
            catch (Exception e)
            {
                return null;
            }
        }

        


    }

}
