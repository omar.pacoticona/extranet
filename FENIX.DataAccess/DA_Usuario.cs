﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using FENIX.BusinessEntity;
using FENIX.Common;

namespace FENIX.DataAccess
{
    public class DA_Usuario
    {
        public String ValidaUsuario(BE_Usuario oBE_Usuario, String sMaquina, String sIPMaquina)
        {
            try
            {
                using (WebDatabase dbTerminal = new WebDatabase())
                {
                    dbTerminal.ProcedureName = "[Seguridad_Lis_ValidaUsuario]";
                    dbTerminal.AddParameter("@vi_Usuario", DbType.String, ParameterDirection.Input, oBE_Usuario.sUsuario);
                    dbTerminal.AddParameter("@vi_Clave", DbType.String, ParameterDirection.Input, oBE_Usuario.sClave);
                    dbTerminal.AddParameter("@vi_NewClave", DbType.String, ParameterDirection.Input, oBE_Usuario.sNuevaClave);
                    dbTerminal.AddParameter("@vi_NewClaveOriginal", DbType.String, ParameterDirection.Input, oBE_Usuario.sClave);                    
                    dbTerminal.AddParameter("@vi_Sistema", DbType.String, ParameterDirection.Input, oBE_Usuario.sSistema);
                    dbTerminal.AddParameter("@vi_ValidarCambio", DbType.String, ParameterDirection.Input, oBE_Usuario.sSolicitarCambioClave);
                    dbTerminal.AddParameter("@vi_CambioClaveOlvidada", DbType.String, ParameterDirection.Input, oBE_Usuario.sSolicitarNewClaveOlvidada);                     
                    dbTerminal.AddParameter("@vo_Retorno", DbType.String, ParameterDirection.Output, String.Empty, 300);
                    dbTerminal.AddParameter("@vi_Maquina", DbType.String, ParameterDirection.Input, sMaquina);
                    dbTerminal.AddParameter("@vi_IpMaquina", DbType.String, ParameterDirection.Input, sIPMaquina);
                    dbTerminal.Execute();
                    return dbTerminal.GetParameter("@vo_Retorno").ToString();
                }
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public String ModificaClave(BE_Usuario oBE_Usuario)
        {
            try
            {
                using (WebDatabase dbTerminal = new WebDatabase())
                {
                    dbTerminal.ProcedureName = "[Seguridad_Upd_ModificaClave]";
                    dbTerminal.AddParameter("@vi_Usuario", DbType.String, ParameterDirection.Input, oBE_Usuario.sUsuario);
                    dbTerminal.AddParameter("@vi_NuevaClave", DbType.String, ParameterDirection.Input, oBE_Usuario.sNuevaClave);
                    dbTerminal.AddParameter("@vi_NuevaClaveConf", DbType.String, ParameterDirection.Input, oBE_Usuario.sNuevaClaveConf);
                    dbTerminal.AddParameter("@vi_Sistema", DbType.String, ParameterDirection.Input, oBE_Usuario.sSistema);
                    dbTerminal.AddParameter("@vo_Retorno", DbType.String, ParameterDirection.Output, String.Empty, 300);
                    dbTerminal.Execute();
                    return dbTerminal.GetParameter("@vo_Retorno").ToString();
                }
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public void GrabaInicioSesion(String sUsuario, String sVarSesion)
        {
            try
            {
                using (WebDatabase dbTerminal = new WebDatabase())
                {
                    dbTerminal.ProcedureName = "[Seguridad_Upd_InicioSesion]";
                    dbTerminal.AddParameter("@vi_Usuario", DbType.String, ParameterDirection.Input, sUsuario);
                    dbTerminal.AddParameter("@vi_VarSesion", DbType.String, ParameterDirection.Input, sVarSesion);
                    //dbTerminal.AddParameter("@vo_Retorno", DbType.String, ParameterDirection.Output, String.Empty, 300);
                    dbTerminal.Execute();
                   // return dbTerminal.GetParameter("@vo_Retorno").ToString();
                }
            }
            catch (Exception e)
            {
                ResultadoTransaccionTx oResultadoTransaccionTx = new ResultadoTransaccionTx(ResultadoTransaccionTx.TipoResultado.Error, e);
                oResultadoTransaccionTx.RegistrarError(e);
            }
        }


        public void HabilitarUsuario(Int32 IdUsuario)
        {
            try
            {
                using (WebDatabase dbTerminal = new WebDatabase())
                {
                    dbTerminal.ProcedureName = "[Extranet_Habilitar_Usuario]";
                    dbTerminal.AddParameter("@IdUsuario", DbType.Int32, ParameterDirection.Input, IdUsuario);                 
                    dbTerminal.Execute();
                }
            }
            catch (Exception e)
            {
                ResultadoTransaccionTx oResultadoTransaccionTx = new ResultadoTransaccionTx(ResultadoTransaccionTx.TipoResultado.Error, e);
                oResultadoTransaccionTx.RegistrarError(e);
            }

        }

        public List<BE_Usuario> AccesoMenu(BE_Usuario oBE_Usuario)
        {
            IDataReader reader = null;
            try
            {
                using (Database dbTerminal = new Database())
                {
                    dbTerminal.ProcedureName = "[Seguridad_Lis_AccesoMenu]";
                    dbTerminal.AddParameter("@vi_Usuario", DbType.String, ParameterDirection.Input, oBE_Usuario.sUsuario);
                    dbTerminal.AddParameter("@vi_Sistema", DbType.String, ParameterDirection.Input, oBE_Usuario.sSistema);

                    reader = dbTerminal.GetDataReader();

                    BE_Usuario oEntidad;

                    List<BE_Usuario> List_Usuario = new List<BE_Usuario>();
                    while (reader.Read())
                    {
                        oEntidad = new BE_Usuario();
                        oEntidad.iPadreMenuId = Int32.Parse(reader["PadreMenuId"].ToString());
                        oEntidad.sDescripcionPadre = reader["DescripcionPadre"].ToString();
                        oEntidad.iPosicion = Int32.Parse(reader["Posicion"].ToString());
                        oEntidad.iMenuId = Int32.Parse(reader["MenuId"].ToString());
                        oEntidad.sDescripcion = reader["Descripcion"].ToString();
                        oEntidad.sUrl = reader["Url"].ToString();

                        List_Usuario.Add(oEntidad);
                    }
                    reader.Close();

                    return List_Usuario;
                }
            }
            catch (Exception e)
            {
                ResultadoTransaccionTx oResultadoTransaccionTx = new ResultadoTransaccionTx(ResultadoTransaccionTx.TipoResultado.Error, e);
                oResultadoTransaccionTx.RegistrarError(e);
                return null;
            }
        }


        //public String ReiniciaClave(BE_Usuario oBE_Usuario)
        //{
        //    try
        //    {
        //        using (WebDatabase dbTerminal = new WebDatabase())
        //        {
        //            dbTerminal.ProcedureName = "[Seguridad_Upd_ReiniciaClave]";
        //            dbTerminal.AddParameter("@vi_Usuario", DbType.String, ParameterDirection.Input, oBE_Usuario.sUsuario);
        //            dbTerminal.AddParameter("@vi_Clave", DbType.String, ParameterDirection.Input, oBE_Usuario.sClave);
        //            dbTerminal.AddParameter("@vi_Sistema", DbType.String, ParameterDirection.Input, oBE_Usuario.sSistema);
        //            dbTerminal.AddParameter("@vo_Retorno", DbType.String, ParameterDirection.Output, String.Empty, 300);
        //            dbTerminal.Execute();
        //            return dbTerminal.GetParameter("@vo_Retorno").ToString();
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        return null;
        //    }
        //}


        public List<BE_Usuario> ListaConfSeguridad()
        {
            IDataReader reader = null;
            try
            {
                using (Database dbTerminal = new Database())
                {
                    dbTerminal.ProcedureName = "[Seguridad_Lis_ConfExtranet]";

                    reader = dbTerminal.GetDataReader();

                    BE_Usuario oEntidad;

                    List<BE_Usuario> List_Usuario = new List<BE_Usuario>();
                    while (reader.Read())
                    {
                        oEntidad = new BE_Usuario();
                        oEntidad.iDiasCambioClave = Int32.Parse(reader["iDiasCambioPassw"].ToString());
                        oEntidad.iNumClavesVerificar = Int32.Parse(reader["iCantPasswVerificar"].ToString());
                        oEntidad.iMmBloqueoVecesFallido = Int32.Parse(reader["iMmLimiteTiempoFallido"].ToString());
                        oEntidad.iMmDesBloqueoUsuario = Int32.Parse(reader["iMmTiempoDesbloquear"].ToString());
                        oEntidad.iMnCierreSesion = Int32.Parse(reader["iTiempoInactividadPag"].ToString());
                        oEntidad.iMaximoSolicitudVolantes = Int32.Parse(reader["iMaximoSolicitudVolantes"].ToString());

                        List_Usuario.Add(oEntidad);
                    }
                    reader.Close();

                    return List_Usuario;
                }
            }
            catch (Exception e)
            {
                ResultadoTransaccionTx oResultadoTransaccionTx = new ResultadoTransaccionTx(ResultadoTransaccionTx.TipoResultado.Error, e);
                oResultadoTransaccionTx.RegistrarError(e);
                return null;
            }
        }

        public List<BE_Usuario> ListaDatosUsuario(Int32 IdUsuario, String Usuario)
        {            
	        IDataReader reader = null;
            try
            {
                using (Database dbTerminal = new Database())
                {
                    dbTerminal.ProcedureName = "[Seguridad_Lis_BuscaUsuario]";
                    dbTerminal.AddParameter("@IdUsuario", DbType.String, ParameterDirection.Input, IdUsuario);
                    dbTerminal.AddParameter("@viUsuario", DbType.String, ParameterDirection.Input, Usuario);

                    reader = dbTerminal.GetDataReader();

                    BE_Usuario oEntidad;

                    List<BE_Usuario> List_Usuario = new List<BE_Usuario>();
                    while (reader.Read())
                    {
                        oEntidad = new BE_Usuario();
                        oEntidad.iIdUsuario = Convert.ToInt32(reader["UsuarioId"].ToString());
                        oEntidad.sUsuario = reader["Usuario"].ToString();
                        oEntidad.sClave = reader["Clave"].ToString();
                        oEntidad.sDescripcion = reader["ApellidosNombres"].ToString();
                        oEntidad.sDNI = reader["NroDNI"].ToString();
                        oEntidad.sSesionIniciada = reader["InicioSession"].ToString();
                        oEntidad.sCorreo = reader["CorreoElectronico"].ToString();                        
                        
                        List_Usuario.Add(oEntidad);
                    }
                    reader.Close();

                    return List_Usuario;
                }
            }
            catch (Exception e)
            {
                ResultadoTransaccionTx oResultadoTransaccionTx = new ResultadoTransaccionTx(ResultadoTransaccionTx.TipoResultado.Error, e);
                oResultadoTransaccionTx.RegistrarError(e);
                return null;
            }
        }



        public void EnviaCorreoNewClave(BE_Usuario oBE_Usuario)
        {
            try
            {
                using (WebDatabase dbTerminal = new WebDatabase())
                {
                    dbTerminal.ProcedureName = "[Seguridad_Lis_NuevaClave_Correo]";
                    dbTerminal.AddParameter("@id_Usuario", DbType.String, ParameterDirection.Input, oBE_Usuario.iIdUsuario);
                    dbTerminal.AddParameter("@vi_correo", DbType.String, ParameterDirection.Input, oBE_Usuario.sCorreo);
                    dbTerminal.AddParameter("@vi_NombreUsuario", DbType.String, ParameterDirection.Input, oBE_Usuario.sDescripcion);
                    //dbTerminal.AddParameter("@vo_Retorno", DbType.String, ParameterDirection.Output, String.Empty, 300);
                    dbTerminal.Execute();
                    //return dbTerminal.GetParameter("@vo_Retorno").ToString();
                }
            }
            catch (Exception e)
            {
                ResultadoTransaccionTx oResultadoTransaccionTx = new ResultadoTransaccionTx(ResultadoTransaccionTx.TipoResultado.Error, e);
                oResultadoTransaccionTx.RegistrarError(e);
            }
        }

        public List<BE_Usuario> ListarUsuarios(String sCliente, String sUsuario,string dni)
        {
            IDataReader reader = null;
            try
            {
                using (Database dbTerminal = new Database())
                {
                    dbTerminal.ProcedureName = "Extranet_LisUsuarios";
                    dbTerminal.AddParameter("@vi_Cliente", DbType.String, ParameterDirection.Input, sCliente);
                    dbTerminal.AddParameter("@vi_Usuario", DbType.String, ParameterDirection.Input, sUsuario);
                    dbTerminal.AddParameter("@vi_dni", DbType.String, ParameterDirection.Input, dni);

                    reader = dbTerminal.GetDataReader();

                    BE_Usuario oEntidad;

                    List<BE_Usuario> List_Usuario = new List<BE_Usuario>();
                    while (reader.Read())
                    {
                        oEntidad = new BE_Usuario();
                        oEntidad.iIdUsuario = Convert.ToInt32(reader["UsuarioId"].ToString());
                        oEntidad.sUsuario = reader["Usuario"].ToString();
                        oEntidad.sNomUsuario = reader["NomUsuario"].ToString();
                        oEntidad.sDNI = reader["NroDNI"].ToString();
                        oEntidad.iIdCliente = Convert.ToInt32(reader["IdCliente"].ToString());
                        oEntidad.sCliente = reader["Cliente"].ToString();
                        oEntidad.sCorreo = reader["Correo"].ToString();
                        oEntidad.sFecha = reader["FechaCreacion"].ToString();
                        oEntidad.isHabilitado = Convert.ToBoolean(reader["Habilitado"].ToString());
                      //  oEntidad.sHabilitado = reader["Habilitado"].ToString();
                        oEntidad.sEsCliente = reader["EsCliente"].ToString();
                        oEntidad.sRucCliente = reader["RUCCliente"].ToString();
                        oEntidad.iTerCondiciones =Convert.ToInt32(reader["idTerCondiciones"].ToString());

                        List_Usuario.Add(oEntidad);
                    }
                    reader.Close();

                    return List_Usuario;
                }
            }
            catch (Exception e)
            {
                ResultadoTransaccionTx oResultadoTransaccionTx = new ResultadoTransaccionTx(ResultadoTransaccionTx.TipoResultado.Error, e);
                oResultadoTransaccionTx.RegistrarError(e);
                return null;
            }
        }

        public String GrabarUsuario(BE_Usuario oBE_Usuario)
        {
            try
            {
                using (WebDatabase dbTerminal = new WebDatabase())
                {
                    dbTerminal.ProcedureName = "[Extranet_Upd_Usuario]";
                    dbTerminal.AddParameter("@vi_IdUsuario", DbType.Int32, ParameterDirection.Input, oBE_Usuario.iIdUsuario);
                    dbTerminal.AddParameter("@vi_Usuario", DbType.String, ParameterDirection.Input, oBE_Usuario.sUsuario);
                    dbTerminal.AddParameter("@vi_NomUsuario", DbType.String, ParameterDirection.Input, oBE_Usuario.sNomUsuario);
                    dbTerminal.AddParameter("@vi_NroDni", DbType.String, ParameterDirection.Input, oBE_Usuario.sDNI);
                    dbTerminal.AddParameter("@vi_Correo", DbType.String, ParameterDirection.Input, oBE_Usuario.sCorreo);
                    dbTerminal.AddParameter("@vi_RucCliente", DbType.String, ParameterDirection.Input, oBE_Usuario.sRucCliente);
                    dbTerminal.AddParameter("@vi_EsCliente", DbType.String, ParameterDirection.Input, oBE_Usuario.sEsCliente);
                    dbTerminal.AddParameter("@vi_UsuarioReg", DbType.String, ParameterDirection.Input, oBE_Usuario.sUsuarioActualiza);
                    dbTerminal.AddParameter("@vi_NomPc", DbType.String, ParameterDirection.Input, oBE_Usuario.sNombrePc);
                    dbTerminal.AddParameter("@vi_idTerCondiciones", DbType.Int32, ParameterDirection.Input, oBE_Usuario.iTerCondiciones);
                    dbTerminal.AddParameter("@vo_resultado", DbType.String, ParameterDirection.Output, String.Empty, 100); 
                    dbTerminal.Execute();
                    return dbTerminal.GetParameter("@vo_resultado").ToString();
                }
            }
            catch (Exception e)
            {
                return null;
            }
        }


        public String AnularUsuario(BE_Usuario oBE_Usuario)
        {
            try
            {
                using (WebDatabase dbTerminal = new WebDatabase())
                {
                    dbTerminal.ProcedureName = "[Extranet_Del_Usuario]";
                    dbTerminal.AddParameter("@vi_IdUsuario", DbType.Int32, ParameterDirection.Input, oBE_Usuario.iIdUsuario);                    
                    dbTerminal.AddParameter("@vi_UsuarioReg", DbType.String, ParameterDirection.Input, oBE_Usuario.sUsuarioActualiza);
                    dbTerminal.AddParameter("@vi_NomPc", DbType.String, ParameterDirection.Input, oBE_Usuario.sNombrePc);
                    dbTerminal.AddParameter("@vo_resultado", DbType.String, ParameterDirection.Output, String.Empty, 100);
                    dbTerminal.Execute();
                    return dbTerminal.GetParameter("@vo_resultado").ToString();
                }
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public String VerificaAccesoProcesoEspecial(String sProceso, String sUsuario)
        {
            try
            {
                using (Database dbTerminal = new Database())
                {
                    dbTerminal.ProcedureName = "[Seguridad_Lis_ProcesosEspeciales]";
                    dbTerminal.AddParameter("@Proceso", DbType.String, ParameterDirection.Input, sProceso);
                    dbTerminal.AddParameter("@Usuario", DbType.String, ParameterDirection.Input, sUsuario);
                    dbTerminal.AddParameter("@Permiso", DbType.String, ParameterDirection.Output, String.Empty, 20);

                    dbTerminal.Execute();

                    return dbTerminal.GetParameter("@Permiso").ToString();
                }
            }
            catch (Exception e)
            {
                return "0|Ocurrio un error en la capa de datos";
            }
        }


    }
}