﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FENIX.BusinessEntity;
using System.Data;
using FENIX.Common;
using System.Data.SqlClient;
using FENIX.DataAccess.Publicacion;
namespace FENIX.DataAccess
{
    public class DA_DocumentoPago
    {

        public BE_DocumentoPagoList ListarDocumentoPago(BE_DocumentoPago oBE_DocumentoPago)
        {
            IDataReader reader = null;
            try
            {
                using (Database dbTerminal = new Database())
                {
                    //dbTerminal.ProcedureName = "[EXTRANET_Lis_DocumentoPago_2QA]";
                    dbTerminal.ProcedureName = "[EXTRANET_Lis_DocumentoPago_2]";
                    dbTerminal.AddParameter("@vi_IdSuperNegocio",DbType.Int32,ParameterDirection.Input,oBE_DocumentoPago.sNegocio); //Dep Temporal
                    dbTerminal.AddParameter("@vi_IdClienteAduana", DbType.Int32, ParameterDirection.Input, oBE_DocumentoPago.idAgenciaAduana); //Dep Temporal
                    dbTerminal.AddParameter("@vi_DocPag_Serie", DbType.String, ParameterDirection.Input, oBE_DocumentoPago.sSerie);
                    dbTerminal.AddParameter("@vi_DocPag_Numero", DbType.String, ParameterDirection.Input, oBE_DocumentoPago.sNumero);
                    dbTerminal.AddParameter("@vi_NumeroDO", DbType.String, ParameterDirection.Input, oBE_DocumentoPago.sNroDocumentoOrigen);
                    //dbTerminal.AddParameter("@anioFactura", DbType.Int32, ParameterDirection.Input, oBE_DocumentoPago.anioFactura);
                    //dbTerminal.AddParameter("@mesFactura", DbType.Int32, ParameterDirection.Input, oBE_DocumentoPago.mesFactura);
                    dbTerminal.AddParameter("@FechaIni", DbType.String, ParameterDirection.Input, oBE_DocumentoPago.sFechaIni);
                    dbTerminal.AddParameter("@FechaFin", DbType.String, ParameterDirection.Input, oBE_DocumentoPago.sFechaFin);
                    dbTerminal.AddParameter("@vi_Volante", DbType.String, ParameterDirection.Input, oBE_DocumentoPago.sNroVolante);
                    dbTerminal.AddParameter("@vi_Contenedor", DbType.String, ParameterDirection.Input, oBE_DocumentoPago.sNroContenedor);
                    dbTerminal.AddParameter("@vi_TipoFactura", DbType.Int32, ParameterDirection.Input, -1);
                    dbTerminal.AddParameter("@vi_Oficina", DbType.Int32, ParameterDirection.Input, 1);
                    dbTerminal.AddParameter("@vi_numeropagina", DbType.Int32, ParameterDirection.Input, oBE_DocumentoPago.NPagina);
                    dbTerminal.AddParameter("@vi_numeroregistros", DbType.Int32, ParameterDirection.Input, oBE_DocumentoPago.NRegistros);
                    dbTerminal.AddParameter("@vo_totalregistros", DbType.Int32, ParameterDirection.Output, oBE_DocumentoPago.NtotalRegistros);
                    //  dbTerminal.AddParameter("@vi_IdTipoDocumentoPago",DbType.Int32, ParameterDirection.Input,oBE_DocumentoPago.iIdTipoDocumentoPago);

                    reader = dbTerminal.GetDataReader();

                    BE_DocumentoPagoList ListDocPago = new BE_DocumentoPagoList();
                    while (reader.Read())
                    {
                        ListDocPago.Add(Pu_DocumentoPago.ListarDocumentosPago(reader));
                    }
                    reader.Close();
                    oBE_DocumentoPago.NtotalRegistros = Convert.ToInt32(dbTerminal.GetParameter("@vo_totalregistros"));
                    return ListDocPago;

                }
            }
            catch(Exception e)
            {
                ResultadoTransaccionTx oResultadoTransaccionTx = new ResultadoTransaccionTx(ResultadoTransaccionTx.TipoResultado.Error, e);
                oResultadoTransaccionTx.RegistrarError(e);
                return null;
            }
        }
        public List<BE_TipoDocumentoPago> ListarTipoDocumentoPago() {
            IDataReader reader = null;
            try
            {
                using (Database dbTerminal = new Database())
                {
                    dbTerminal.ProcedureName = "[EXTRANET_Lis_TipoDocumentoPago]";
                    reader = dbTerminal.GetDataReader();

                    List<BE_TipoDocumentoPago> listTipoDocumentoPago = new List<BE_TipoDocumentoPago>();
                    //BE_TipoDocumentoPago oBE_TipoDocumentoPago = new BE_TipoDocumentoPago();
                    //oBE_TipoDocumentoPago.iIdTipoDocumentoPago = -1;
                    //oBE_TipoDocumentoPago.sTipoDocumentoPago = "SELECCIONE";
                    //listTipoDocumentoPago.Add(oBE_TipoDocumentoPago);

                    while (reader.Read())
                    {
                        listTipoDocumentoPago.Add(Pu_DocumentoPago.ListarTipoDocumentoPago(reader));
                    }
                    reader.Close();
                    return listTipoDocumentoPago;
                }
            }
            catch(Exception e) {
                ResultadoTransaccionTx oResultadoTransaccionTx = new ResultadoTransaccionTx(ResultadoTransaccionTx.TipoResultado.Error, e);
                oResultadoTransaccionTx.RegistrarError(e);
                return null;
            }
        }

        public List<BE_LineaNegocio> ListarLineaNegocio()
        {
            IDataReader reader = null;
            try
            {
                using (Database dbTerminal = new Database())
                {
                    dbTerminal.ProcedureName = "[EXTRANET_Lis_DocumentoPago_2_LineaNegocio]";

                    reader = dbTerminal.GetDataReader();

                    List<BE_LineaNegocio> listLineaNegocio = new List<BE_LineaNegocio>();
                    BE_LineaNegocio oBE_LineaNegocio = new BE_LineaNegocio();
                    oBE_LineaNegocio.idLinea = 1;
                    oBE_LineaNegocio.LineaNegocio = "SELECCIONE";
                    listLineaNegocio.Add(oBE_LineaNegocio);

                    while (reader.Read())
                    {
                        listLineaNegocio.Add(Pu_DocumentoPago.ListarLineaNegocio(reader));
                    }
                    reader.Close();

                    return listLineaNegocio;

                }

            }
            catch (Exception e)
            {
                ResultadoTransaccionTx oResultadoTransaccionTx = new ResultadoTransaccionTx(ResultadoTransaccionTx.TipoResultado.Error, e);
                oResultadoTransaccionTx.RegistrarError(e);
                return null;
            }
        }

    }
}
