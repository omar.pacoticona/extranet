﻿using System;
using System.Collections.Generic;
using System.Text;
using FENIX.BusinessEntity;
using System.Data;
using FENIX.Common;

namespace FENIX.DataAccess
{
    public class DA_Indicador
    {

        #region "Transaccion"

        #endregion


        #region "Listado"
            public List<BE_Indicador> Listar(BE_Indicador oBE_Indicador)
            {
                IDataReader reader = null;
                try
                {
                    using (Database dbTerminal = new Database())
                    {
                        dbTerminal.ProcedureName = "[Extranet_Lis_Indicador]";
                        dbTerminal.AddParameter("@vi_AnioPeriodo", DbType.Int32, ParameterDirection.Input, oBE_Indicador.iAnho);
                        dbTerminal.AddParameter("@vi_MesPeriodo", DbType.Int32, ParameterDirection.Input, oBE_Indicador.iMes);
                        dbTerminal.AddParameter("@vi_Agrupacion", DbType.Int32, ParameterDirection.Input, oBE_Indicador.iAgrupacion);
                        dbTerminal.AddParameter("@vi_Usuario", DbType.String, ParameterDirection.Input, oBE_Indicador.sUsuario);
                        dbTerminal.AddParameter("@vi_Modulo", DbType.String, ParameterDirection.Input, oBE_Indicador.sModulo);

                        reader = dbTerminal.GetDataReader();

                        List<BE_Indicador> Lis_Indicador = new List<BE_Indicador>();
                        while (reader.Read())
                        {
                            Lis_Indicador.Add(Pu_Indicador.Listar(reader));
                        }
                        reader.Close();

                        return Lis_Indicador;
                    }
                }
                catch (Exception e)
                {
                    ResultadoTransaccionTx oResultadoTransaccionTx = new ResultadoTransaccionTx(ResultadoTransaccionTx.TipoResultado.Error, e);
                    oResultadoTransaccionTx.RegistrarError(e);
                    return null;
                }
            }

            public List<BE_Indicador> ListarAnual(BE_Indicador oBE_Indicador)
            {
                IDataReader reader = null;
                try
                {
                    using (Database dbTerminal = new Database())
                    {
                        dbTerminal.ProcedureName = "[Extranet_Lis_Indicador_Anual]";
                        dbTerminal.AddParameter("@vi_AnioPeriodo", DbType.Int32, ParameterDirection.Input, oBE_Indicador.iAnho);
                        dbTerminal.AddParameter("@vi_MesPeriodo", DbType.Int32, ParameterDirection.Input, oBE_Indicador.iMes);
                        dbTerminal.AddParameter("@vi_Agrupacion", DbType.Int32, ParameterDirection.Input, oBE_Indicador.iAgrupacion);
                        dbTerminal.AddParameter("@vi_Usuario", DbType.String, ParameterDirection.Input, oBE_Indicador.sUsuario);
                        dbTerminal.AddParameter("@vi_Modulo", DbType.String, ParameterDirection.Input, oBE_Indicador.sModulo);
                        dbTerminal.AddParameter("@vi_PorValor", DbType.Int32, ParameterDirection.Input, oBE_Indicador.iDatoMostrar);

                        reader = dbTerminal.GetDataReader();

                        List<BE_Indicador> Lis_Indicador = new List<BE_Indicador>();
                        while (reader.Read())
                        {
                            Lis_Indicador.Add(Pu_Indicador.ListarAnual(reader));
                        }
                        reader.Close();

                        return Lis_Indicador;
                    }
                }
                catch (Exception e)
                {
                    ResultadoTransaccionTx oResultadoTransaccionTx = new ResultadoTransaccionTx(ResultadoTransaccionTx.TipoResultado.Error, e);
                    oResultadoTransaccionTx.RegistrarError(e);
                    return null;
                }
            }

            public List<BE_Indicador> ListarDetalle(BE_Indicador oBE_Indicador)
            {
                IDataReader reader = null;
                try
                {
                    using (Database dbTerminal = new Database())
                    {
                        dbTerminal.ProcedureName = "[Extranet_Lis_IndicadorDetalle]";
                        dbTerminal.AddParameter("@vi_AnioPeriodo", DbType.Int32, ParameterDirection.Input, oBE_Indicador.iAnho);
                        dbTerminal.AddParameter("@vi_MesPeriodo", DbType.Int32, ParameterDirection.Input, oBE_Indicador.iMes);
                        dbTerminal.AddParameter("@vi_IdTipoIndicador", DbType.Int32, ParameterDirection.Input, oBE_Indicador.iIdIndicador);
                        dbTerminal.AddParameter("@vi_Usuario", DbType.String, ParameterDirection.Input, oBE_Indicador.sUsuario);
                        dbTerminal.AddParameter("@vi_ValorTotal", DbType.Int32, ParameterDirection.Input, oBE_Indicador.iValorTotal);

                        reader = dbTerminal.GetDataReader();

                        List<BE_Indicador> Lis_Indicador = new List<BE_Indicador>();
                        while (reader.Read())
                        {
                            Lis_Indicador.Add(Pu_Indicador.ListarDetalle(reader));
                        }
                        reader.Close();

                        return Lis_Indicador;
                    }
                }
                catch (Exception e)
                {
                    ResultadoTransaccionTx oResultadoTransaccionTx = new ResultadoTransaccionTx(ResultadoTransaccionTx.TipoResultado.Error, e);
                    oResultadoTransaccionTx.RegistrarError(e);
                    return null;
                }
            }

            public String AprobarDesaprobar(BE_Indicador oBE_Indicador)
            {
                try
                {
                    using (Database dbTerminal = new Database())
                    {
                        dbTerminal.ProcedureName = "[Extranet_Ins_ControlIndicador]";
                        dbTerminal.AddParameter("@vi_ConInd_int_Anio", DbType.Int32, ParameterDirection.Input, oBE_Indicador.iAnho);
                        dbTerminal.AddParameter("@vi_ConInd_int_Mes", DbType.Int32, ParameterDirection.Input, oBE_Indicador.iMes);
                        dbTerminal.AddParameter("@vi_Indicador", DbType.Int32, ParameterDirection.Input, oBE_Indicador.iIdIndicador);
                        dbTerminal.AddParameter("@vi_ConInd_vch_Estado", DbType.String, ParameterDirection.Input, oBE_Indicador.sEstado);
                        dbTerminal.AddParameter("@Usuario", DbType.String, ParameterDirection.Input, oBE_Indicador.sUsuario);
                        dbTerminal.AddParameter("@NombrePc", DbType.String, ParameterDirection.Input, oBE_Indicador.sNombrePc);
                        dbTerminal.AddParameter("@vo_Retorno", DbType.String, ParameterDirection.Output, String.Empty, 300);
                        dbTerminal.Execute();

                        return dbTerminal.GetParameter("@vo_Retorno").ToString();
                    }
                }
                catch (Exception e)
                {
                    return "0|Error en la capa de datos";
                }
            }

            public List<BE_Indicador> ListarGraficoTotal(BE_Indicador oBE_Indicador)
            {
                IDataReader reader = null;
                try
                {
                    using (Database dbTerminal = new Database())
                    {
                        dbTerminal.ProcedureName = "[Extranet_Lis_IndicadorGraficoTotal]";
                        dbTerminal.AddParameter("@vi_AnioPeriodo", DbType.Int32, ParameterDirection.Input, oBE_Indicador.iAnho);
                        dbTerminal.AddParameter("@vi_Semestre", DbType.Int32, ParameterDirection.Input, oBE_Indicador.iMes);
                        dbTerminal.AddParameter("@vi_Usuario", DbType.String, ParameterDirection.Input, oBE_Indicador.sUsuario);
                        dbTerminal.AddParameter("@vi_TipoReporte", DbType.Int32, ParameterDirection.Input, oBE_Indicador.iAgrupacion);

                        reader = dbTerminal.GetDataReader();

                        List<BE_Indicador> Lis_Indicador = new List<BE_Indicador>();
                        while (reader.Read())
                        {
                            Lis_Indicador.Add(Pu_Indicador.ListarGraficoTotal(reader));
                        }
                        reader.Close();

                        return Lis_Indicador;
                    }
                }
                catch (Exception e)
                {
                    ResultadoTransaccionTx oResultadoTransaccionTx = new ResultadoTransaccionTx(ResultadoTransaccionTx.TipoResultado.Error, e);
                    oResultadoTransaccionTx.RegistrarError(e);
                    return null;
                }
            }

            public List<BE_Indicador> ListarGrafico(BE_Indicador oBE_Indicador)
            {
                IDataReader reader = null;
                try
                {
                    using (Database dbTerminal = new Database())
                    {
                        dbTerminal.ProcedureName = "[Extranet_Lis_IndicadorGrafico]";
                        //dbTerminal.AddParameter("@vi_AnioPeriodo", DbType.Int32, ParameterDirection.Input, oBE_Indicador.iAnho);
                        //dbTerminal.AddParameter("@vi_MesPeriodo", DbType.Int32, ParameterDirection.Input, oBE_Indicador.iMes);
                        //dbTerminal.AddParameter("@vi_Agrupacion", DbType.Int32, ParameterDirection.Input, oBE_Indicador.iAgrupacion);
                        //dbTerminal.AddParameter("@vi_Usuario", DbType.String, ParameterDirection.Input, oBE_Indicador.sUsuario);

                        reader = dbTerminal.GetDataReader();

                        List<BE_Indicador> Lis_Indicador = new List<BE_Indicador>();
                        while (reader.Read())
                        {
                            Lis_Indicador.Add(Pu_Indicador.ListarGrafico(reader));
                        }
                        reader.Close();

                        return Lis_Indicador;
                    }
                }
                catch (Exception e)
                {
                    ResultadoTransaccionTx oResultadoTransaccionTx = new ResultadoTransaccionTx(ResultadoTransaccionTx.TipoResultado.Error, e);
                    oResultadoTransaccionTx.RegistrarError(e);
                    return null;
                }
            }
        #endregion
    }
}
