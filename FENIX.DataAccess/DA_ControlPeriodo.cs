﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FENIX.BusinessEntity;
using System.Data;

namespace FENIX.DataAccess
{
    public class DA_ControlPeriodo
    {
        #region "Transaccion"

        public String Insertar(BE_ControlPeriodo oBE_ControlPeriodo)
        {
            try
            {
                using (Database dbTerminal = new Database())
                {
                    dbTerminal.ProcedureName = "[Extranet_Ins_ControlPeriodo]";
                    dbTerminal.AddParameter("@vi_IdTipoCierre", DbType.Int32, ParameterDirection.Input, oBE_ControlPeriodo.iIdTipoCierre);
                    dbTerminal.AddParameter("@vi_IdTipoProceso", DbType.Int32, ParameterDirection.Input, oBE_ControlPeriodo.iIdTipoProceso);
                    dbTerminal.AddParameter("@vi_PerOpe_int_AnioPeriodo", DbType.Int32, ParameterDirection.Input, oBE_ControlPeriodo.iAnioPeriodo);
                    dbTerminal.AddParameter("@vi_PerOpe_int_MesPeriodo", DbType.Int32, ParameterDirection.Input, oBE_ControlPeriodo.iMesPeriodo);
                    dbTerminal.AddParameter("@vi_PerOpe_int_CantidadDias", DbType.Int32, ParameterDirection.Input, oBE_ControlPeriodo.iCantidadDias);
                    dbTerminal.AddParameter("@vi_PerOpe_dt_Fecha", DbType.DateTime, ParameterDirection.Input, oBE_ControlPeriodo.dtFecha);
                    dbTerminal.AddParameter("@Usuario", DbType.String, ParameterDirection.Input, oBE_ControlPeriodo.sUsuario);
                    dbTerminal.AddParameter("@NombrePc", DbType.String, ParameterDirection.Input, oBE_ControlPeriodo.sNombrePc);
                    dbTerminal.AddParameter("@vo_Retorno", DbType.String, ParameterDirection.Output, String.Empty, 300);
                    dbTerminal.Execute();

                    return dbTerminal.GetParameter("@vo_Retorno").ToString();
                }
            }
            catch (Exception e)
            {
                //ResultadoTransaccionTx oResultadoTransaccionTx = null;
                //oResultadoTransaccionTx.RegistrarError(e);
                return "0|Error en la capa de datos";
            }
        }

        public String Actualizar(BE_ControlPeriodo oBE_ControlPeriodo)
        {
            try
            {
                using (Database dbTerminal = new Database())
                {
                    dbTerminal.ProcedureName = "[Extranet_Upd_ControlPeriodo_Habilitar]";
                    dbTerminal.AddParameter("@vi_IdTipoCierre", DbType.Int32, ParameterDirection.Input, oBE_ControlPeriodo.iIdTipoCierre);
                    dbTerminal.AddParameter("@vi_IdTipoProceso", DbType.Int32, ParameterDirection.Input, oBE_ControlPeriodo.iIdTipoProceso);
                    dbTerminal.AddParameter("@vi_CtlPer_int_AnioPeriodo", DbType.Int32, ParameterDirection.Input, oBE_ControlPeriodo.iAnioPeriodo);
                    dbTerminal.AddParameter("@vi_CtlPer_int_MesPeriodo", DbType.Int32, ParameterDirection.Input, oBE_ControlPeriodo.iMesPeriodo);
                    dbTerminal.AddParameter("@vi_CtlPer_int_CantidadDias", DbType.Int32, ParameterDirection.Input, oBE_ControlPeriodo.iCantidadDias);
                    dbTerminal.AddParameter("@vi_CtlPer_dt_Fecha", DbType.DateTime, ParameterDirection.Input, oBE_ControlPeriodo.dtFecha);
                    dbTerminal.AddParameter("@Usuario", DbType.String, ParameterDirection.Input, oBE_ControlPeriodo.sUsuario);
                    dbTerminal.AddParameter("@NombrePc", DbType.String, ParameterDirection.Input, oBE_ControlPeriodo.sNombrePc);
                    dbTerminal.AddParameter("@vo_Retorno", DbType.String, ParameterDirection.Output, String.Empty, 300);
                    dbTerminal.Execute();

                    return dbTerminal.GetParameter("@vo_Retorno").ToString();
                }
            }
            catch (Exception e)
            {
                //ResultadoTransaccionTx oResultadoTransaccionTx = null;
                //oResultadoTransaccionTx.RegistrarError(e);
                return "0|Error en la capa de datos";
            }
        }

        public String CerrarPeriodo(BE_ControlPeriodo oBE_ControlPeriodo)
        {
            try
            {
                using (Database dbTerminal = new Database())
                {
                    dbTerminal.ProcedureName = "[Extranet_Upd_ControlPeriodo_CierreManual]";
                    dbTerminal.AddParameter("@vi_IdTipoCierre", DbType.Int32, ParameterDirection.Input, oBE_ControlPeriodo.iIdTipoCierre);
                    dbTerminal.AddParameter("@vi_CtlPer_int_AnioPeriodo", DbType.Int32, ParameterDirection.Input, oBE_ControlPeriodo.iAnioPeriodo);
                    dbTerminal.AddParameter("@vi_CtlPer_int_MesPeriodo", DbType.Int32, ParameterDirection.Input, oBE_ControlPeriodo.iMesPeriodo);
                    dbTerminal.AddParameter("@Usuario", DbType.String, ParameterDirection.Input, oBE_ControlPeriodo.sUsuario);
                    dbTerminal.AddParameter("@NombrePc", DbType.String, ParameterDirection.Input, oBE_ControlPeriodo.sNombrePc);
                    dbTerminal.AddParameter("@vo_Retorno", DbType.String, ParameterDirection.Output, String.Empty, 300);
                    dbTerminal.Execute();

                    return dbTerminal.GetParameter("@vo_Retorno").ToString();
                }
            }
            catch (Exception e)
            {
                //ResultadoTransaccionTx oResultadoTransaccionTx = null;
                //oResultadoTransaccionTx.RegistrarError(e);
                return "0|Error en la capa de datos";
            }
        }

        #endregion

        #region "Listado"

        public List<BE_ControlPeriodo> Listar(BE_ControlPeriodo oBE_ControlPeriodo)
        {
            IDataReader reader = null;

            try
            {
                using (Database dbTerminal = new Database())
                {
                    dbTerminal.ProcedureName = "[Extranet_Lis_ControlPeriodo]";
                    dbTerminal.AddParameter("@vi_TipoCierre", DbType.Int32, ParameterDirection.Input, oBE_ControlPeriodo.iIdTipoCierre);
                    dbTerminal.AddParameter("@vi_AnioPeriodo", DbType.Int32, ParameterDirection.Input, oBE_ControlPeriodo.iAnioPeriodo);
                    dbTerminal.AddParameter("@vi_MesPeriodo", DbType.Int32, ParameterDirection.Input, oBE_ControlPeriodo.iMesPeriodo);

                    reader = dbTerminal.GetDataReader();

                    List<BE_ControlPeriodo> Lst_ControlPeriodo = new List<BE_ControlPeriodo>();
                    while (reader.Read())
                    {
                        Lst_ControlPeriodo.Add(Pu_ControlPeriodo.Listar(reader));
                    }

                    reader.Close();
                    oBE_ControlPeriodo.NTotalRegistros = Convert.ToInt32(dbTerminal.GetParameter("@vo_totalregistros"));
                    return Lst_ControlPeriodo;
                }

            }
            catch (Exception e)
            {
                return null;
            }
        }

        public List<BE_ControlPeriodo> ListarLog(BE_ControlPeriodo oBE_ControlPeriodo)
        {
            IDataReader reader = null;

            try
            {
                using (Database dbTerminal = new Database())
                {
                    dbTerminal.ProcedureName = "[Extranet_Lis_ControlPeriodoLog]";
                    dbTerminal.AddParameter("@vi_TipoCierre", DbType.Int32, ParameterDirection.Input, oBE_ControlPeriodo.iIdTipoCierre);
                    dbTerminal.AddParameter("@vi_AnioPeriodo", DbType.Int32, ParameterDirection.Input, oBE_ControlPeriodo.iAnioPeriodo);
                    dbTerminal.AddParameter("@vi_MesPeriodo", DbType.Int32, ParameterDirection.Input, oBE_ControlPeriodo.iMesPeriodo);

                    reader = dbTerminal.GetDataReader();

                    List<BE_ControlPeriodo> Lst_ControlPeriodo = new List<BE_ControlPeriodo>();
                    while (reader.Read())
                    {
                        Lst_ControlPeriodo.Add(Pu_ControlPeriodo.ListarLog(reader));
                    }

                    reader.Close();
                    oBE_ControlPeriodo.NTotalRegistros = Convert.ToInt32(dbTerminal.GetParameter("@vo_totalregistros"));
                    return Lst_ControlPeriodo;
                }

            }
            catch (Exception e)
            {
                return null;
            }
        }

        #endregion
    }
}
