﻿using System;
using System.Collections.Generic;
using System.Text;
using FENIX.Common;
using FENIX.BusinessEntity;

using System.Data;

namespace FENIX.DataAccess
{
    public class DA_Contrato
    {
        #region "Transaccion"
        public String Insertar(BE_Contrato oBE_Contrato)
        {

            try
            {

                using (Database dbTerminal = new Database())
                {
                    dbTerminal.ProcedureName = "[Comercial_Ins_Contrato_New]";
                    dbTerminal.AddParameter("@IdVendedor", DbType.Int32, ParameterDirection.Input, oBE_Contrato.iIdVendedor);
                    dbTerminal.AddParameter("@IdTipoContrato", DbType.Int32, ParameterDirection.Input, Convert.ToInt32(oBE_Contrato.sIdTipoContrato));
                    dbTerminal.AddParameter("@IdRolCliente", DbType.Int32, ParameterDirection.Input, oBE_Contrato.iIdRolCliente);
                    dbTerminal.AddParameter("@IdSocio", DbType.Int32, ParameterDirection.Input, oBE_Contrato.iIdSocio);
                    dbTerminal.AddParameter("@IdRolSocio", DbType.Int32, ParameterDirection.Input, oBE_Contrato.iIdRolSocio);
                    dbTerminal.AddParameter("@IdTipoOperacion", DbType.Int32, ParameterDirection.Input, oBE_Contrato.iIdTipoOperacion);
                    dbTerminal.AddParameter("@IdCliente", DbType.Int32, ParameterDirection.Input, oBE_Contrato.iIdCliente);
                    dbTerminal.AddParameter("@IdLineaNegocio", DbType.Int32, ParameterDirection.Input, Convert.ToInt32(oBE_Contrato.sIdLineaNegocio));
                    dbTerminal.AddParameter("@Con_Dt_FechaInicio", DbType.DateTime, ParameterDirection.Input, oBE_Contrato.sFechaInicio);
                    dbTerminal.AddParameter("@Con_Dt_FechaFin", DbType.DateTime, ParameterDirection.Input, oBE_Contrato.sFechaFin);
                    dbTerminal.AddParameter("@Con_Chr_Situacion", DbType.String, ParameterDirection.Input, oBE_Contrato.sSituacion);
                    dbTerminal.AddParameter("@Con_Vch_Observacion", DbType.String, ParameterDirection.Input, oBE_Contrato.sObservacion);
                    dbTerminal.AddParameter("@vi_IdLineaMaritima", DbType.Int32, ParameterDirection.Input, oBE_Contrato.iIdLineaMartima);
                    dbTerminal.AddParameter("@Usuario", DbType.String, ParameterDirection.Input, oBE_Contrato.sUsuario);
                    dbTerminal.AddParameter("@NombrePc", DbType.String, ParameterDirection.Input, oBE_Contrato.sNombrePc);
                    dbTerminal.AddParameter("@IdContrato", DbType.String, ParameterDirection.Output, String.Empty, 200);
                    dbTerminal.AddParameter("@vi_comisionista", DbType.String, ParameterDirection.Input, oBE_Contrato.sDescripcionComisionista);
                    dbTerminal.Execute();

                    return dbTerminal.GetParameter("@IdContrato").ToString();
                }
            }
            catch (Exception e)
            {
                ResultadoTransaccionTx oResultadoTransaccionTx = null;
                oResultadoTransaccionTx.RegistrarError(e);
                return "0|Error en la capa de datos";
            }
        }
        public int InsertarContratoDetalle(BE_Contrato oBE_Contrato, BE_Tarifa oBE_Tarifa)
        {
            try
            {
                int vl_iCodigo = 0;
                using (Database dbTerminal = new Database())
                {
                    dbTerminal.ProcedureName = "[Comercial_Ins_ContratoDetalle_New]";
                    dbTerminal.AddParameter("@IdTarifa", DbType.Int32, ParameterDirection.Input, oBE_Contrato.iIdTarifa);
                    dbTerminal.AddParameter("@IdContrato", DbType.String, ParameterDirection.Input, oBE_Contrato.iIdContrato);
                    dbTerminal.AddParameter("@Monto", DbType.Decimal, ParameterDirection.Input, oBE_Contrato.dMonto);
                    dbTerminal.AddParameter("@DiasLibre", DbType.Int32, ParameterDirection.Input, oBE_Contrato.iDiasLibres);
                    dbTerminal.AddParameter("@ConDet_chr_FlagTarifaSli", DbType.String, ParameterDirection.Input, oBE_Contrato.sTarifaSli);
                    dbTerminal.AddParameter("@Usuario", DbType.String, ParameterDirection.Input, oBE_Contrato.sUsuario);
                    dbTerminal.AddParameter("@NombrePc", DbType.String, ParameterDirection.Input, oBE_Contrato.sNombrePc);
                    dbTerminal.AddParameter("@vl_IdContratoDetalle", DbType.Int32, ParameterDirection.Output, vl_iCodigo);
                    dbTerminal.AddParameter("@vi_IdModalidad", DbType.Int32, ParameterDirection.Input, oBE_Tarifa.iIdModalidad);
                    dbTerminal.AddParameter("@vi_IdMoneda", DbType.Decimal, ParameterDirection.Input, oBE_Tarifa.iIdMoneda);
                    dbTerminal.AddParameter("@vi_Tar_Num_MontoMinimo", DbType.Decimal, ParameterDirection.Input, oBE_Tarifa.dMontoMinimo);
                    dbTerminal.AddParameter("@vi_Tar_Num_MontoMaximo", DbType.Decimal, ParameterDirection.Input, oBE_Tarifa.dMontoMaximo);
                    dbTerminal.AddParameter("@vi_Tar_Chr_FlagMandatorio", DbType.String, ParameterDirection.Input, oBE_Tarifa.sFlagMandatorio);
                    dbTerminal.AddParameter("@vi_Tar_Chr_FlagLimite", DbType.String, ParameterDirection.Input, oBE_Tarifa.sFlagLimite);
                    dbTerminal.AddParameter("@vi_Tar_Num_Costo", DbType.Decimal, ParameterDirection.Input, oBE_Tarifa.dCosto);
                    dbTerminal.AddParameter("@vi_IdTarifa_Origen", DbType.Int32, ParameterDirection.Input, oBE_Tarifa.iIdTarifaOrigen);
                    dbTerminal.AddParameter("@vi_Retroactivo", DbType.String, ParameterDirection.Input, oBE_Tarifa.sFlagAlmacenajeRetroactivo);
                    dbTerminal.AddParameter("@vi_DiasLibreAlmaPeli", DbType.String, ParameterDirection.Input, oBE_Tarifa.sDiasLibPeligrosa);
                    dbTerminal.AddParameter("@vi_Asume", DbType.Int32, ParameterDirection.Input, oBE_Tarifa.iIdAsume);

                    dbTerminal.Execute();

                    Int32.TryParse(dbTerminal.GetParameter("@vl_IdContratoDetalle").ToString(), out vl_iCodigo);
                    return vl_iCodigo;
                }
            }
            catch (Exception e)
            {
                ResultadoTransaccionTx oResultadoTransaccionTx = null;
                oResultadoTransaccionTx.RegistrarError(e);
                return 0;
            }
        }
        public ResultadoTransaccionTx Actualizar(BE_Contrato oBE_Contrato, BE_Tarifa oBE_Tarifa)
        {

            try
            {
                using (Database dbTerminal = new Database())
                {

                    dbTerminal.ProcedureName = "[Comercial_Upd_Contrato_New]";
                    dbTerminal.AddParameter("@IdVendedor", DbType.Int32, ParameterDirection.Input, oBE_Contrato.iIdVendedor);
                    dbTerminal.AddParameter("@IdTipoContrato", DbType.Int32, ParameterDirection.Input, Convert.ToInt32(oBE_Contrato.sIdTipoContrato));
                    dbTerminal.AddParameter("@IdRolCliente", DbType.Int32, ParameterDirection.Input, oBE_Contrato.iIdRolCliente);
                    dbTerminal.AddParameter("@IdSocio", DbType.Int32, ParameterDirection.Input, oBE_Contrato.iIdSocio);
                    dbTerminal.AddParameter("@IdRolSocio", DbType.Int32, ParameterDirection.Input, oBE_Contrato.iIdRolSocio);
                    dbTerminal.AddParameter("@IdTipoOperacion", DbType.Int32, ParameterDirection.Input, oBE_Contrato.iIdTipoOperacion);
                    dbTerminal.AddParameter("@IdCliente", DbType.Int32, ParameterDirection.Input, oBE_Contrato.iIdCliente);
                    dbTerminal.AddParameter("@IdLineaNegocio", DbType.Int32, ParameterDirection.Input, Convert.ToInt32(oBE_Contrato.sIdLineaNegocio));
                    dbTerminal.AddParameter("@Con_Dt_FechaInicio", DbType.DateTime, ParameterDirection.Input, oBE_Contrato.sFechaInicio);
                    dbTerminal.AddParameter("@Con_Dt_FechaFin", DbType.DateTime, ParameterDirection.Input, oBE_Contrato.sFechaFin);
                    dbTerminal.AddParameter("@Con_Chr_Situacion", DbType.String, ParameterDirection.Input, oBE_Contrato.sSituacion);
                    dbTerminal.AddParameter("@Con_Vch_Observacion", DbType.String, ParameterDirection.Input, oBE_Contrato.sObservacion);
                    dbTerminal.AddParameter("@vi_IdLineaMaritima", DbType.Int32, ParameterDirection.Input, oBE_Contrato.iIdLineaMartima);
                    dbTerminal.AddParameter("@Usuario", DbType.String, ParameterDirection.Input, oBE_Contrato.sUsuario);
                    dbTerminal.AddParameter("@NombrePc", DbType.String, ParameterDirection.Input, oBE_Contrato.sNombrePc);
                    dbTerminal.AddParameter("@IdContrato", DbType.Int32, ParameterDirection.Input, oBE_Contrato.iIdContrato);
                    dbTerminal.AddParameter("@IdComisionista", DbType.Int32, ParameterDirection.Input, oBE_Contrato.iIdComisionista);
                    dbTerminal.AddParameter("@vi_comisionista", DbType.String, ParameterDirection.Input, oBE_Contrato.sDescripcionComisionista);
                    // registrar detalle 
                    dbTerminal.AddParameter("@vi_FlagLimite", DbType.String, ParameterDirection.Input, oBE_Tarifa.sFlagLimite);
                    dbTerminal.AddParameter("@vi_FlagMandatorio", DbType.String, ParameterDirection.Input, oBE_Tarifa.sFlagMandatorio);
                    dbTerminal.AddParameter("@vi_MontoMaximo", DbType.Decimal, ParameterDirection.Input, oBE_Tarifa.dMontoMaximo);
                    dbTerminal.AddParameter("@vi_MontoMinimo", DbType.Decimal, ParameterDirection.Input, oBE_Tarifa.dMontoMinimo);
                    dbTerminal.AddParameter("@vi_Costo", DbType.Decimal, ParameterDirection.Input, oBE_Tarifa.dCosto);
                    dbTerminal.AddParameter("@vi_DiasLibresAlmacenaje", DbType.Int32, ParameterDirection.Input, oBE_Tarifa.iDiasLibresAlmacenaje);
                    dbTerminal.AddParameter("@vi_Monto", DbType.Decimal, ParameterDirection.Input, oBE_Tarifa.dMonto);
                    dbTerminal.AddParameter("@vi_IdMoneda", DbType.Int32, ParameterDirection.Input, oBE_Tarifa.iIdMoneda);
                    dbTerminal.AddParameter("@vi_IdModalidad", DbType.Int32, ParameterDirection.Input, oBE_Tarifa.iIdModalidad);
                    dbTerminal.AddParameter("@vi_IdTarifa", DbType.Int32, ParameterDirection.Input, oBE_Tarifa.iIdTarifa);
                    dbTerminal.AddParameter("@vi_Retroactivo", DbType.String, ParameterDirection.Input, oBE_Tarifa.sFlagAlmacenajeRetroactivo);
                    dbTerminal.AddParameter("@vi_DiasLibAlmaPeli", DbType.String, ParameterDirection.Input, oBE_Tarifa.sDiasLibPeligrosa);

                    dbTerminal.Execute();


                    return new ResultadoTransaccionTx(ResultadoTransaccionTx.TipoResultado.Exito, null);
                }
            }
            catch (Exception e)
            {
                return new ResultadoTransaccionTx(ResultadoTransaccionTx.TipoResultado.Error, e);
            }
        }
        public ResultadoTransaccionTx Eliminar(BE_Contrato oBE_Contrato)
        {

            try
            {
                using (Database dbTerminal = new Database())
                {
                    dbTerminal.ProcedureName = "[Comercial_Del_Contrato]";
                    dbTerminal.AddParameter("@IdContrato", DbType.Int32, ParameterDirection.Input, oBE_Contrato.iIdContrato);
                    dbTerminal.AddParameter("@Usuario", DbType.String, ParameterDirection.Input, oBE_Contrato.sUsuario);
                    dbTerminal.AddParameter("@NombrePc", DbType.String, ParameterDirection.Input, oBE_Contrato.sNombrePc);
                    dbTerminal.Execute();
                    return new ResultadoTransaccionTx(ResultadoTransaccionTx.TipoResultado.Exito, null);
                }
            }
            catch (Exception e)
            {
                return new ResultadoTransaccionTx(ResultadoTransaccionTx.TipoResultado.Error, e);
            }
        }
        public ResultadoTransaccionTx EliminarContratoDetalle(BE_Contrato oBE_Contrato)
        {

            try
            {
                using (Database dbTerminal = new Database())
                {
                    dbTerminal.ProcedureName = "[Comercial_Del_ContratoDetalle]";
                    dbTerminal.AddParameter("@IdConDet", DbType.String, ParameterDirection.Input, oBE_Contrato.iIdConDet);
                    dbTerminal.AddParameter("@Usuario", DbType.String, ParameterDirection.Input, oBE_Contrato.sUsuario);
                    dbTerminal.AddParameter("@NombrePc", DbType.String, ParameterDirection.Input, oBE_Contrato.sNombrePc);
                    dbTerminal.Execute();
                    return new ResultadoTransaccionTx(ResultadoTransaccionTx.TipoResultado.Exito, null);
                }
            }
            catch (Exception e)
            {
                return new ResultadoTransaccionTx(ResultadoTransaccionTx.TipoResultado.Error, e);
            }
        }
        public ResultadoTransaccionTx AprobarDesaprobarContrato(Int32 iIdContrato, string sSituacion)
        {

            try
            {
                using (Database dbTerminal = new Database())
                {
                    dbTerminal.ProcedureName = "[Comercial_Upd_AprobarContrato]";
                    dbTerminal.AddParameter("@IdContrato", DbType.Int32, ParameterDirection.Input, iIdContrato);
                    dbTerminal.AddParameter("@Situacion", DbType.String, ParameterDirection.Input, sSituacion);

                    dbTerminal.Execute();
                    return new ResultadoTransaccionTx(ResultadoTransaccionTx.TipoResultado.Exito, null);
                }
            }
            catch (Exception e)
            {
                return new ResultadoTransaccionTx(ResultadoTransaccionTx.TipoResultado.Error, e);
            }
        }

        public String VerificaAccesoProcesoEspecial(String sProceso, String sUsuario)
        {
            try
            {
                using (Database dbTerminal = new Database())
                {
                    dbTerminal.ProcedureName = "[Seguridad_Lis_ProcesosEspeciales]";
                    dbTerminal.AddParameter("@Proceso", DbType.String, ParameterDirection.Input, sProceso);
                    dbTerminal.AddParameter("@Usuario", DbType.String, ParameterDirection.Input, sUsuario);
                    dbTerminal.AddParameter("@Permiso", DbType.String, ParameterDirection.Output, String.Empty, 1);

                    dbTerminal.Execute();

                    return dbTerminal.GetParameter("@Permiso").ToString();
                }
            }
            catch (Exception e)
            {
                return "0|Ocurrio un error en la capa de datos";
            }
        }

        public String CopiarContrato(BE_Contrato oBE_Contrato)
        {
            try
            {
                using (Database dbTerminal = new Database())
                {
                    dbTerminal.ProcedureName = "[Comercial_Ins_ContratoCopiar]";
                    dbTerminal.AddParameter("@IdContrato", DbType.Int32, ParameterDirection.Input, oBE_Contrato.iIdContrato);
                    dbTerminal.AddParameter("@IdRolCliente", DbType.Int32, ParameterDirection.Input, oBE_Contrato.iIdRolCliente);
                    dbTerminal.AddParameter("@IdCliente", DbType.Int32, ParameterDirection.Input, oBE_Contrato.iIdCliente);
                    dbTerminal.AddParameter("@IdVendedor", DbType.Int32, ParameterDirection.Input, oBE_Contrato.iIdVendedor);
                    dbTerminal.AddParameter("@Usuario", DbType.String, ParameterDirection.Input, oBE_Contrato.sUsuario);
                    dbTerminal.AddParameter("@NombrePC", DbType.String, ParameterDirection.Input, oBE_Contrato.sNombrePc);
                    dbTerminal.AddParameter("@Fechavigencia", DbType.DateTime, ParameterDirection.Input, oBE_Contrato.sFechaFin);
                    dbTerminal.AddParameter("@Retorno", DbType.String, ParameterDirection.Output, string.Empty, 200);

                    dbTerminal.Execute();

                    return dbTerminal.GetParameter("@Retorno").ToString();
                }
            }
            catch (Exception e)
            {
                return "0|Ocurrio un error en la capa de datos";
            }
        }

        public String InsDireccionarContrato(Int32 iIdContrato, Int32 iIdDocOri)
        {

            try
            {
                using (Database dbTerminal = new Database())
                {
                    dbTerminal.ProcedureName = "Comercial_Upd_DocumentoOrigenContrato";
                    dbTerminal.AddParameter("@IdDocOri", DbType.Int32, ParameterDirection.Input, iIdDocOri);
                    dbTerminal.AddParameter("@idContrato", DbType.Int32, ParameterDirection.Input, iIdContrato);
                    dbTerminal.AddParameter("@vo_Retorno", DbType.String, ParameterDirection.Output, string.Empty, 200);

                    dbTerminal.Execute();
                    return dbTerminal.GetParameter("@vo_Retorno").ToString();

                }
            }
            catch (Exception e)
            {

                return "0|Ocurrio un error en la capa de datos " + e.Message;
            }
        }
        public ResultadoTransaccionTx ReasignarContrato(Int32 sIdContrato, Int32 iIdvendedor, String sIdUsuario)
        {

            try
            {
                using (Database dbTerminal = new Database())
                {
                    dbTerminal.ProcedureName = "Comercial_Upd_ReasignacionContrato";
                    dbTerminal.AddParameter("@IdContrato", DbType.Int32, ParameterDirection.Input, sIdContrato);
                    dbTerminal.AddParameter("@IdVendedor", DbType.Int32, ParameterDirection.Input, iIdvendedor);
                    dbTerminal.AddParameter("@Usuario", DbType.String, ParameterDirection.Input, sIdUsuario);
                    dbTerminal.Execute();
                    return new ResultadoTransaccionTx(ResultadoTransaccionTx.TipoResultado.Exito, null);
                }
            }
            catch (Exception e)
            {
                return new ResultadoTransaccionTx(ResultadoTransaccionTx.TipoResultado.Error, e);
            }
        }


        #endregion

        #region "Listado"
        public BE_TarifaList ListarContratoDetalle(int iIdContrato)
        {
            IDataReader reader = null;

            try
            {
                using (Database dbTerminal = new Database())
                {
                    dbTerminal.ProcedureName = "[Comercial_Lis_ContratoDetalle]";
                    dbTerminal.AddParameter("@IdContrato", DbType.Int32, ParameterDirection.Input, iIdContrato);

                    reader = dbTerminal.GetDataReader();


                    BE_TarifaList Lst_ContratoDetalle = new BE_TarifaList();
                    while (reader.Read())
                    {
                        Lst_ContratoDetalle.Add(Pu_Contrato.ListarDetalle(reader));

                    }
                    reader.Close();
                    return Lst_ContratoDetalle;
                }

            }
            catch (Exception e)
            {
                return null;
            }
        }
        public BE_Contrato ListarContratoPorId(int iIdContrato)
        {
            IDataReader reader = null;
            try
            {
                using (Database dbTerminal = new Database())
                {
                    dbTerminal.ProcedureName = "[Comercial_Lis_Contrato_PorID_New]";
                    dbTerminal.AddParameter("@IdContrato", DbType.Int32, ParameterDirection.Input, iIdContrato);

                    reader = dbTerminal.GetDataReader();


                    List<BE_Contrato> Lst_BE_Contrato = new List<BE_Contrato>();
                    while (reader.Read())
                    {
                        Lst_BE_Contrato.Add(Pu_Contrato.ListarPorId(reader));

                    }
                    reader.Close();
                    return Lst_BE_Contrato[0];
                }

            }
            catch (Exception e)
            {
                return null;
            }
        }
        public List<BE_Contrato> Listar(BE_Contrato oBE_Contrato)
        {
            IDataReader reader = null;

            try
            {
                using (Database dbTerminal = new Database())
                {
                    dbTerminal.ProcedureName = "[Comercial_Lis_Contrato_New]";
                    dbTerminal.AddParameter("@IdContrato", DbType.String, ParameterDirection.Input, oBE_Contrato.sIdContrato);
                    dbTerminal.AddParameter("@Client_RazonSocial", DbType.String, ParameterDirection.Input, oBE_Contrato.sDescripcionCliente);
                    dbTerminal.AddParameter("@Socio_RazonSocial", DbType.String, ParameterDirection.Input, oBE_Contrato.sDescripcionSocio);
                    dbTerminal.AddParameter("@Vendedor_ApeNom", DbType.String, ParameterDirection.Input, oBE_Contrato.sDescripcionVendedor);
                    dbTerminal.AddParameter("@idLineaNegocio", DbType.String, ParameterDirection.Input, Convert.ToString(oBE_Contrato.sIdLineaNegocio));
                    dbTerminal.AddParameter("@IdTipoContrato", DbType.String, ParameterDirection.Input, Convert.ToString(oBE_Contrato.sIdTipoContrato));
                    dbTerminal.AddParameter("@vi_numeropagina", DbType.Int32, ParameterDirection.Input, oBE_Contrato.NPagina);
                    dbTerminal.AddParameter("@vi_numeroregistros", DbType.Int32, ParameterDirection.Input, oBE_Contrato.NRegistros);
                    dbTerminal.AddParameter("@vo_totalregistros", DbType.Int32, ParameterDirection.Output, oBE_Contrato.NTotalRegistros);
                    dbTerminal.AddParameter("@Comisionista", DbType.String, ParameterDirection.Input, oBE_Contrato.sDescripcionComisionista);
                    dbTerminal.AddParameter("@NroContrato", DbType.String, ParameterDirection.Input, oBE_Contrato.sNroContrato);
                    dbTerminal.AddParameter("@LineaMaritima", DbType.String, ParameterDirection.Input, oBE_Contrato.sLineaMaritima);

                    reader = dbTerminal.GetDataReader();


                    List<BE_Contrato> Lst_Contrato = new List<BE_Contrato>();
                    while (reader.Read())
                    {
                        Lst_Contrato.Add(Pu_Contrato.Listar(reader));

                    }
                    reader.Close();
                    oBE_Contrato.NTotalRegistros = Convert.ToInt32(dbTerminal.GetParameter("@vo_totalregistros"));
                    return Lst_Contrato;
                }

            }
            catch (Exception e)
            {
                return null;
            }
        }
        public List<BE_TablaUc> ListarTarifaMaster_Contrato(BE_Tabla oBE_Tabla)
        {
            IDataReader reader = null;

            try
            {
                using (Database dbTerminal = new Database())
                {
                    dbTerminal.ProcedureName = "[Comercial_Lis_UserControl_TarifaMaster_Contrato]";
                    dbTerminal.AddParameter("@IdTabla", DbType.Int32, ParameterDirection.Input, oBE_Tabla.iIdTabla);
                    dbTerminal.AddParameter("@idTablaPadre", DbType.Int32, ParameterDirection.Input, oBE_Tabla.iIdTablaPadre);
                    dbTerminal.AddParameter("@IdValor", DbType.String, ParameterDirection.Input, oBE_Tabla.sIdValor);
                    dbTerminal.AddParameter("@ValVcr_Descripcion", DbType.String, ParameterDirection.Input, oBE_Tabla.sDescripcion);
                    dbTerminal.AddParameter("@vi_numeropagina", DbType.Int32, ParameterDirection.Input, oBE_Tabla.NPagina);
                    dbTerminal.AddParameter("@vi_numeroregistros", DbType.Int32, ParameterDirection.Input, oBE_Tabla.NRegistros);
                    dbTerminal.AddParameter("@vo_totalregistros", DbType.Int32, ParameterDirection.Output, oBE_Tabla.NTotalRegistros);
                    reader = dbTerminal.GetDataReader();

                    List<BE_TablaUc> Lst_Tabla = new List<BE_TablaUc>();
                    while (reader.Read())
                    {
                        Lst_Tabla.Add(Pu_ListarTabla.ListarUc(reader));

                    }
                    reader.Close();
                    oBE_Tabla.NTotalRegistros = Convert.ToInt32(dbTerminal.GetParameter("@vo_totalregistros").ToString());
                    return Lst_Tabla;
                }
            }
            catch (Exception e)
            {
                return null;
            }
        }
        public List<BE_TablaUc> ListarTarifaMaster(BE_Tabla oBE_Tabla)
        {
            IDataReader reader = null;

            try
            {
                using (Database dbTerminal = new Database())
                {
                    dbTerminal.ProcedureName = "[Comercial_Lis_UserControl_TarifaMaster]";
                    dbTerminal.AddParameter("@IdTabla", DbType.Int32, ParameterDirection.Input, oBE_Tabla.iIdTabla);
                    dbTerminal.AddParameter("@idTablaPadre", DbType.Int32, ParameterDirection.Input, oBE_Tabla.iIdTablaPadre);
                    dbTerminal.AddParameter("@IdValor", DbType.String, ParameterDirection.Input, oBE_Tabla.sIdValor);
                    dbTerminal.AddParameter("@ValVcr_Descripcion", DbType.String, ParameterDirection.Input, oBE_Tabla.sDescripcion);
                    dbTerminal.AddParameter("@vi_numeropagina", DbType.Int32, ParameterDirection.Input, oBE_Tabla.NPagina);
                    dbTerminal.AddParameter("@vi_numeroregistros", DbType.Int32, ParameterDirection.Input, oBE_Tabla.NRegistros);
                    dbTerminal.AddParameter("@vo_totalregistros", DbType.Int32, ParameterDirection.Output, oBE_Tabla.NTotalRegistros);
                    reader = dbTerminal.GetDataReader();

                    List<BE_TablaUc> Lst_Tabla = new List<BE_TablaUc>();
                    while (reader.Read())
                    {
                        Lst_Tabla.Add(Pu_ListarTabla.ListarUc(reader));

                    }
                    reader.Close();
                    oBE_Tabla.NTotalRegistros = Convert.ToInt32(dbTerminal.GetParameter("@vo_totalregistros").ToString());
                    return Lst_Tabla;
                }
            }
            catch (Exception e)
            {
                return null;
            }
        }
        public List<BE_TablaUc> ListarTarifaMaster_libre(BE_Tabla oBE_Tabla)
        {
            IDataReader reader = null;

            try
            {
                using (Database dbTerminal = new Database())
                {
                    dbTerminal.ProcedureName = "[Comercial_Lis_UserControl_TarifaMaster_FacturaLibre]";
                    dbTerminal.AddParameter("@IdTabla", DbType.Int32, ParameterDirection.Input, oBE_Tabla.iIdTabla);
                    dbTerminal.AddParameter("@idTablaPadre", DbType.Int32, ParameterDirection.Input, oBE_Tabla.iIdTablaPadre);
                    dbTerminal.AddParameter("@IdValor", DbType.String, ParameterDirection.Input, oBE_Tabla.sIdValor);
                    dbTerminal.AddParameter("@ValVcr_Descripcion", DbType.String, ParameterDirection.Input, oBE_Tabla.sDescripcion);
                    dbTerminal.AddParameter("@vi_numeropagina", DbType.Int32, ParameterDirection.Input, oBE_Tabla.NPagina);
                    dbTerminal.AddParameter("@vi_numeroregistros", DbType.Int32, ParameterDirection.Input, oBE_Tabla.NRegistros);
                    dbTerminal.AddParameter("@vo_totalregistros", DbType.Int32, ParameterDirection.Output, oBE_Tabla.NTotalRegistros);
                    reader = dbTerminal.GetDataReader();

                    List<BE_TablaUc> Lst_Tabla = new List<BE_TablaUc>();
                    while (reader.Read())
                    {
                        Lst_Tabla.Add(Pu_ListarTabla.ListarUc(reader));

                    }
                    reader.Close();
                    oBE_Tabla.NTotalRegistros = Convert.ToInt32(dbTerminal.GetParameter("@vo_totalregistros").ToString());
                    return Lst_Tabla;
                }
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public List<BE_TablaUc> ListarCliente(BE_Cliente oBE_Cliente)
        {
            IDataReader reader = null;

            try
            {
                using (Database dbTerminal = new Database())
                {
                    dbTerminal.ProcedureName = "[Comercial_Lis_UserControl_Cliente]";
                    dbTerminal.AddParameter("@NumDocumento", DbType.String, ParameterDirection.Input, oBE_Cliente.sNumDocumento);
                    dbTerminal.AddParameter("@Razon_Social", DbType.String, ParameterDirection.Input, oBE_Cliente.sRazon_Social);
                    dbTerminal.AddParameter("@IdRol", DbType.Int32, ParameterDirection.Input, oBE_Cliente.iIdRol);
                    dbTerminal.AddParameter("@vi_numeropagina", DbType.Int32, ParameterDirection.Input, oBE_Cliente.NPagina);
                    dbTerminal.AddParameter("@vi_numeroregistros", DbType.Int32, ParameterDirection.Input, oBE_Cliente.NRegistros);
                    dbTerminal.AddParameter("@vo_totalregistros", DbType.Int32, ParameterDirection.Output, oBE_Cliente.NTotalRegistros);

                    reader = dbTerminal.GetDataReader();

                    List<BE_TablaUc> Lst_Tabla = new List<BE_TablaUc>();
                    while (reader.Read())
                    {
                        Lst_Tabla.Add(Pu_ListarTabla.ListarUc(reader));

                    }
                    reader.Close();
                    oBE_Cliente.NTotalRegistros = Convert.ToInt32(dbTerminal.GetParameter("@vo_totalregistros"));

                    return Lst_Tabla;
                }
            }
            catch (Exception e)
            {
                ResultadoTransaccionTx oResultadoTransaccionTx = null;
                oResultadoTransaccionTx.RegistrarError(e);
                return null;
            }
        }
        public List<BE_TablaUc> ListarVendedor(BE_Tabla oBE_Tabla)
        {
            IDataReader reader = null;

            try
            {
                using (Database dbTerminal = new Database())
                {
                    dbTerminal.ProcedureName = "[Comercial_Lis_UserControl_Vendedor]";
                    dbTerminal.AddParameter("@IdTabla", DbType.Int32, ParameterDirection.Input, oBE_Tabla.iIdTabla);
                    dbTerminal.AddParameter("@idTablaPadre", DbType.Int32, ParameterDirection.Input, oBE_Tabla.iIdTablaPadre);
                    dbTerminal.AddParameter("@IdValor", DbType.String, ParameterDirection.Input, oBE_Tabla.sIdValor);
                    dbTerminal.AddParameter("@ValVcr_Descripcion", DbType.String, ParameterDirection.Input, oBE_Tabla.sDescripcion);
                    dbTerminal.AddParameter("@vi_numeropagina", DbType.Int32, ParameterDirection.Input, oBE_Tabla.NPagina);
                    dbTerminal.AddParameter("@vi_numeroregistros", DbType.Int32, ParameterDirection.Input, oBE_Tabla.NRegistros);
                    dbTerminal.AddParameter("@vo_totalregistros", DbType.Int32, ParameterDirection.Output, oBE_Tabla.NTotalRegistros);

                    reader = dbTerminal.GetDataReader();

                    List<BE_TablaUc> Lst_Tabla = new List<BE_TablaUc>();
                    while (reader.Read())
                    {
                        Lst_Tabla.Add(Pu_ListarTabla.ListarUc(reader));

                    }
                    reader.Close();
                    oBE_Tabla.NTotalRegistros = Convert.ToInt32(dbTerminal.GetParameter("@vo_totalregistros"));

                    return Lst_Tabla;
                }
            }
            catch (Exception e)
            {
                ResultadoTransaccionTx oResultadoTransaccionTx = null;
                oResultadoTransaccionTx.RegistrarError(e);
                return null;
            }
        }


        public List<BE_TablaUc> ListarClienteRol(BE_Cliente oBE_Cliente)
        {
            IDataReader reader = null;

            try
            {
                using (Database dbTerminal = new Database())
                {
                    dbTerminal.ProcedureName = "[COM_Lis_CliRol]";
                    dbTerminal.AddParameter("@IdCliente", DbType.Int32, ParameterDirection.Input, Convert.ToInt32(oBE_Cliente.sIdCliente));                   
                    dbTerminal.AddParameter("@IdRol", DbType.Int32, ParameterDirection.Input, oBE_Cliente.iIdRol);

                    reader = dbTerminal.GetDataReader();

                    List<BE_TablaUc> Lst_Tabla = new List<BE_TablaUc>();
                    while (reader.Read())
                    {
                        Lst_Tabla.Add(Pu_ListarTabla.ListarUc(reader));

                    }
                    reader.Close();
                    
                    return Lst_Tabla;
                    
                }
            }
            catch (Exception e)
            {
                ResultadoTransaccionTx oResultadoTransaccionTx = null;
                oResultadoTransaccionTx.RegistrarError(e);
                return null;
            }
        }
        //public List<BE_DireccionamientoContrato> ListarContratoPorBl(BE_DocumentoOrigen oBE_DocumentoOrigen)
        //{
        //    IDataReader reader = null;

        //    try
        //    {
        //        using (Database dbTerminal = new Database())
        //        {
        //            dbTerminal.ProcedureName = "[Comercial_lis_DocumentoOrigenContrato_New]";
        //            dbTerminal.AddParameter("@DocOri_vch_NumeroDO", DbType.String, ParameterDirection.Input, oBE_DocumentoOrigen.sNumeroDo);
        //            dbTerminal.AddParameter("@DocOri_vch_NumeroDOMadre", DbType.String, ParameterDirection.Input, oBE_DocumentoOrigen.sNumeroDOMadre);
        //            dbTerminal.AddParameter("@Idvolante", DbType.String, ParameterDirection.Input, oBE_DocumentoOrigen.sIdVolante);
        //            dbTerminal.AddParameter("@vi_numeropagina", DbType.Int32, ParameterDirection.Input, oBE_DocumentoOrigen.NPagina);
        //            dbTerminal.AddParameter("@vi_numeroregistros", DbType.Int32, ParameterDirection.Input, oBE_DocumentoOrigen.NRegistros);
        //            dbTerminal.AddParameter("@vi_FechaEstimadaIni", DbType.DateTime, ParameterDirection.Input, oBE_DocumentoOrigen.dtFechaEtaInicio);
        //            dbTerminal.AddParameter("@vi_FechaEstimadaFin", DbType.DateTime, ParameterDirection.Input, oBE_DocumentoOrigen.dtFechaEtaFinal);
        //            dbTerminal.AddParameter("@vi_TipoDireccionado", DbType.String, ParameterDirection.Input, oBE_DocumentoOrigen.sTipoDireccionado);

        //            dbTerminal.AddParameter("@vo_totalregistros", DbType.Int32, ParameterDirection.Output, oBE_DocumentoOrigen.NTotalRegistros);



        //            reader = dbTerminal.GetDataReader();

        //            List<BE_DireccionamientoContrato> Lst_Tabla = new List<BE_DireccionamientoContrato>();
        //            while (reader.Read())
        //            {
        //                Lst_Tabla.Add(Pu_Contrato.ListarDireccionamientoCnt(reader));

        //            }
        //            reader.Close();
        //            oBE_DocumentoOrigen.NTotalRegistros = Convert.ToInt32(dbTerminal.GetParameter("@vo_totalregistros"));

        //            return Lst_Tabla;
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        ResultadoTransaccionTx oResultadoTransaccionTx = null;
        //        oResultadoTransaccionTx.RegistrarError(e);
        //        return null;
        //    }
        //}

        public List<BE_Contrato> ListarReasignacionContrato(BE_Contrato oBE_Contrato)
        {
            IDataReader reader = null;
            try
            {
                using (Database dbTerminal = new Database())
                {

                    dbTerminal.ProcedureName = "[Comercial_Lis_ReasignacionContrato]";
                    dbTerminal.AddParameter("@IdContrato", DbType.String, ParameterDirection.Input, oBE_Contrato.sIdContrato);
                    dbTerminal.AddParameter("@IdVendedor", DbType.String, ParameterDirection.Input, oBE_Contrato.sIdVendedor);
                    dbTerminal.AddParameter("@idLineaNegocio", DbType.String, ParameterDirection.Input, oBE_Contrato.sIdLineaNegocio);
                    dbTerminal.AddParameter("@IdTipoContrato", DbType.String, ParameterDirection.Input, oBE_Contrato.sIdTipoContrato);
                    dbTerminal.AddParameter("@vi_numeropagina", DbType.Int32, ParameterDirection.Input, oBE_Contrato.NPagina);
                    dbTerminal.AddParameter("@vi_numeroregistros", DbType.Int32, ParameterDirection.Input, oBE_Contrato.NRegistros);
                    dbTerminal.AddParameter("@vo_totalregistros", DbType.Int32, ParameterDirection.Output, oBE_Contrato.NTotalRegistros);

                    reader = dbTerminal.GetDataReader();


                    List<BE_Contrato> Lst_BE_Contrato = new List<BE_Contrato>();
                    while (reader.Read())
                    {
                        Lst_BE_Contrato.Add(Pu_Contrato.ListarContratoReasignacion(reader));

                    }
                    reader.Close();
                    oBE_Contrato.NTotalRegistros = Convert.ToInt32(dbTerminal.GetParameter("@vo_totalregistros"));
                    return Lst_BE_Contrato;
                }

            }
            catch (Exception e)
            {
                ResultadoTransaccionTx oResultadoTransaccionTx = null;
                oResultadoTransaccionTx.RegistrarError(e);
                return null;
            }
        }

        //public List<BE_DireccionamientoContrato> ListarReasignacionContratoHistorico(Int32 iIdContrato)
        //{
        //    IDataReader reader = null;
        //    try
        //    {
        //        using (Database dbTerminal = new Database())
        //        {

        //            dbTerminal.ProcedureName = "[Comercial_lis_ReasignacionHistorico]";
        //            dbTerminal.AddParameter("@IdContrato", DbType.String, ParameterDirection.Input, iIdContrato);


        //            reader = dbTerminal.GetDataReader();
        //        }

        //        List<BE_DireccionamientoContrato> Lst_BE_Contrato = new List<BE_DireccionamientoContrato>();
        //        while (reader.Read())
        //        {
        //            Lst_BE_Contrato.Add(Pu_Contrato.ListarHistoricoReasignacion(reader));

        //        }
        //        reader.Close();

        //        return Lst_BE_Contrato;


        //    }
        //    catch (Exception e)
        //    {
        //        ResultadoTransaccionTx oResultadoTransaccionTx = null;
        //        oResultadoTransaccionTx.RegistrarError(e);
        //        return null;
        //    }
        //}


        #endregion
    }
}
