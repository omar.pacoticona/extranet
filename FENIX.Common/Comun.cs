using System;
using System.Web;
using System.Collections.Generic;
using System.Text;

using System.IO;



namespace FENIX.Common
{

    public static class Comun
    {
        public static string PrimeraMayuscula(string pOracion)
        {
            string res = string.Empty;
            pOracion = pOracion.Trim();
            foreach (string s in pOracion.Split(Convert.ToChar(32)))
            {
                if (!string.IsNullOrEmpty(s))
                {
                    res = string.Format("{0} {1}", res, (s.Substring(0, 1).ToUpper() + s.Substring(1).ToLower()));
                }
            }
            return res.Trim();

        }

        public enum ErrorValidacion
        {
            Error = 1,
            Exito = 0,
        }
        public enum Botones
        {
            Nuevo = 1,
            Grabar = 2,
            Actualizar = 3,
            Eliminar = 4,
            Imprimir = 5,
            Listar = 6,
            Salir = 7,
        }

        public enum Busqueda
        {
            Contenedor = 1,
            Cliente = 2,
            DocumentoOrigen = 3,
            Volante = 4,
            Dua = 5,
            NaveViajeRumbo = 6,
            Tarifa = 7,
            RolCliente = 8,
            Servicio = 9,
            TipoContenedor = 10,
            TipoCarga = 11,
            Embalaje = 12,
            Costo = 13,
            Socio = 14,
            Vendedor = 15,
            Direccionante = 16,
            LineaMaritima = 17,
            NaveViajeRumboProvisional = 18,
            Puerto = 19,
            Carga = 20,
            UnidadMedida = 21,
            PlacaVehiculo = 22,
            Chofer = 23,
            DocumentoOrigen_MovBalanza = 24,
            PlacaTarja = 25,
            ContenedorNaveViaje = 26,
            Inspector = 27,
            DocumentoOrigenDetalleCargaSuelta = 28,
            Despachador = 29,
            DocumentoOrigenDetalleTarjaVehicular = 30,
            AutorizacionRetiro = 31,
            Zona = 32,
            DocumentoOrigen_Dua = 33,
            DocumentoOrigen_Previo = 34,
            DocumentoOrigenDet_Previo = 35,
            DocumentoOrigenDet_MalEstado = 36,
            Inmovilizacion_Precedente = 37,
            DocumentoOrigenDesglose = 38,
            DocumentoOrigenDet_InvLib = 39,
            DocumentoOrigenDet_InvLibPrec = 40,
            ContenedorNaveViaje_Eir = 41,
            DocumentoOrigen_Volante = 42,
            DocumentoOrigen_ProcesoEsp = 43,
            DocumentoOrigen_Liquidacion = 44,
            DocumentoOrigen_Bookin = 45,
            DocumentoOrigen_BookingDet_CS = 46,
            NaveViajeRumboExportacion = 47,
            DocumentoOrigen_Bookin_Roleo = 48,
            AutorizacionRetiro_MovBal = 49,
            ContenedorNaveViaje_EirRetiro = 50,
            DocumentoOrigenDetalleTarjaVehicular_Retiro = 51,
            AutorizacionRetiro_PesajeCs = 52,
            Nave = 53,
            DocumentoOrigen_InvLib = 54,
            DocumentoOrigenFinales = 55,
            Tarifa_Libre = 56,
            Tarifa_Contrato = 57,
            DocumentoOrigen_Liquidacion_Fact = 58

        }

    
        
        public class Metodos
        {
           
          

            public static String CrearDirectorios(String sRuta, String sIdExp)
            {
                try
                {
                    string sRutaCarpeta = string.Empty;
                    string sCarpetaDDMMYY = DateTime.Now.ToString("ddMMyyyy");
                    string sCarpetaDia = DateTime.Now.Day.ToString();

                    string[] sDirectorio = (sRuta + "\\" + sCarpetaDDMMYY + "\\" + sIdExp).Split('\\');

                    for (int i = 0; i < sDirectorio.Length; i++)
                    {
                        sRutaCarpeta = sRutaCarpeta + sDirectorio[i] + "\\";

                        if (System.IO.Directory.Exists(sRutaCarpeta) == false)
                        {
                            System.IO.Directory.CreateDirectory(sRutaCarpeta);
                        }
                    }
                    return sRutaCarpeta;

                }
                catch (Exception e)
                {
                    return " Error en CrearDirectorios :" + e.Message.ToString();
                }

            }
          
        }
    }
}
