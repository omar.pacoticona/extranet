using System;

namespace FENIX.Common
{
    public class ResultadoTransaccionTx
    {
        #region " Enumeraciones "
        public enum TipoResultado
        { 
            Error,
            Exito,
            Existe
        }
        public enum eTipoError
        { 
            UnKnow = 0,
            Unique = 2627,
            Integridad = 4815,
            LongColumn = 4815,
        }
        #endregion

        public ResultadoTransaccionTx (TipoResultado pTipoResultado, Exception pException)
        {
            _enuTipoResultado = pTipoResultado;
            _objException = pException;
            _TipoError = 0;
            if (pException != null)
            {
                try
                {
                    _TipoError = (pException as System.Data.SqlClient.SqlException).Number;

                }
                catch { }
            }
        }  
        public void RegistrarError(Exception pException)
        {
        
        }

        public void RegistrarError(Exception pException,String sErrorMsg)
        {

        }



        private TipoResultado _enuTipoResultado;
        private Exception _objException;
        private int _TipoError;

        public int TipoError
        {
            get { return _TipoError; }
            set { _TipoError = value; }
        }

        public Exception ObjException
        {
            get { return _objException; }
            set { _objException = value; }
        }

        public TipoResultado EnuTipoResultado
        {
            get { return _enuTipoResultado; }
            set { _enuTipoResultado = value; }
        }

    }

    public class ResultadoInterfase
    {
        #region " Enumeraciones "
        public enum TipoResultado
        {
            Error,
            Exito,
            Existe
        }
        public enum eTipoError
        {
            UnKnow = 0,
            Unique = 2627,
            Integridad = 4815,
            LongColumn = 4815,
        }
        #endregion

        public ResultadoInterfase(TipoResultado pTipoResultado, Exception pException)
        {
            _enuTipoResultado = pTipoResultado;
            _objException = pException;
            _TipoError = String.Empty;
            if (pException != null)
            {
                try
                {
                    _TipoError = pException.ToString();

                }
                catch { }
            }
        }
        private TipoResultado _enuTipoResultado;
        private Exception _objException;
        private String _TipoError;

        public String TipoError
        {
            get { return _TipoError; }
            set { _TipoError = value; }
        }

        public Exception ObjException
        {
            get { return _objException; }
            set { _objException = value; }
        }

        public TipoResultado EnuTipoResultado
        {
            get { return _enuTipoResultado; }
            set { _enuTipoResultado = value; }
        }

    }


   
}
