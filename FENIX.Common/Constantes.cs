using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;

namespace FENIX.Common
   
{

   
    public class Constantes
    {        
        public const string sAcepto = "1";
        public const string sCancelo = "0";
        public const string sTituloRolCliente = "Consulta de Rol";
        public const string FFADEV_TERMINAL = "FFADEV_TERMINAL";
        public const string Titulo_Servicio = "Buscar Servicios";
        public const string Titulo_Costo = "Buscar Costo";
        public const string Titulo_TipoCnt = "Buscar Tipo de Contenedor";
        public const string Titulo_Carga = "Buscar Cargas";
        public const string Titulo_Embalaje = "Buscar Embalaje";
        public const string Titulo_Linea = "Buscar Linea";
        public const string Titulo_CentroCosto = "Buscar Centro Costo";
        public const string sNuevo = "Nuevo";
        public const string sConsulta = "Consulta";
        public const string sGrabar = "Grabar";
        public const string sActualizar = "Actualizar";
        public const string sEliminar = "Eliminar";
        public const string sLiquidacion = "Liquidacion";

        public const int iPageSize = 30;
        public const int iPageSizeDO = 100;

        public const int iAtendidoDet =1;
        public const int iPendienteDet = 2;
        public const int iFacturadoDet = 4;
        public const int iRechazadoDet = 5;
        public const int iLiquidadoDet = 6;
        public const int iIdServicioEnergia = 129;

        public static string RutaGrabarAct = ConfigurationManager.AppSettings["RutaImbGrabarActivo"];
        public static string RutaGrabarIna = ConfigurationManager.AppSettings["RutaImbGrabarInactivo"];
        public static string RutaNuevoAct = ConfigurationManager.AppSettings["RutaImbNuevoActivo"];
        public static string RutaNuevoIna = ConfigurationManager.AppSettings["RutaImbNuevoInactivo"];
        public static string RutaModificarIna = ConfigurationManager.AppSettings["RutaImbModificarInactivo"];
        public static string RutaModificarAct = ConfigurationManager.AppSettings["RutaImbModificarActivo"];
        public static string RutaEliminarIna = ConfigurationManager.AppSettings["RutaImbEliminarInactivo"];
        public static string RutaEliminarAct = ConfigurationManager.AppSettings["RutaImbEliminarActivo"];
        public static string RutaCalendario = ConfigurationManager.AppSettings["RutaCalendario"];
        public static string RutaFondoFenix = ConfigurationManager.AppSettings["RutaFondoFenix"];

        public static string RutaTxADI_Impo = ConfigurationManager.AppSettings["RutaTxADI_Impo"];
        public static string RutaTxBSF_Impo = ConfigurationManager.AppSettings["RutaTxBSF_Impo"];
        public static string RutaTxDTD_Impo = ConfigurationManager.AppSettings["RutaTxDTD_Impo"];
        public static string RutaTxICAI_Impo = ConfigurationManager.AppSettings["RutaTxICAI_Impo"];
        public static string RutaTxMailWare_Impo = ConfigurationManager.AppSettings["RutaTxMailWare_Impo"];
        public static string RutaTxTAD_Impo = ConfigurationManager.AppSettings["RutaTxTAD_Impo"];
        public static string RutaTxEXP_Impo = ConfigurationManager.AppSettings["RutaTxEXP_Impo"];
        public static string RutaTxEXP_Expo = ConfigurationManager.AppSettings["RutaTxEXP_Expo"];
        public static string RutaTxDUA_Expo = ConfigurationManager.AppSettings["RutaTxDUA_Expo"];

        public static string RutaZipICAI_Impo = ConfigurationManager.AppSettings["RutaZipICAI_Impo"];
        public static string RutaZipEXP_Impo = ConfigurationManager.AppSettings["RutaZipEXP_Impo"];
        public static string RutaZipEXP_Expo = ConfigurationManager.AppSettings["RutaZipEXP_Expo"];
        public static string RutaZipDUA_Expo = ConfigurationManager.AppSettings["RutaZipDUA_Expo"];

        
        public static string NombreEXP_Impo = ConfigurationManager.AppSettings["NombreEXP_Impo"];
        public static string NombreEXP_Expo = ConfigurationManager.AppSettings["NombreEXP_Expo"];
        public static string NombreDUA_Expo = ConfigurationManager.AppSettings["NombreDUA_Expo"];
        public static string RutaDescomprimir = ConfigurationManager.AppSettings["RutaDescomprimir"];

        public static string RutaNDetalle = ConfigurationManager.AppSettings["RutaNDetalle"];
        public static string RutaNDesglose = ConfigurationManager.AppSettings["RutaNDesglose"];
        public static string RutaDescargaRuc = ConfigurationManager.AppSettings["RutaDescargaRuc"];
        public static string RutaDescargaDua = ConfigurationManager.AppSettings["RutaDescargaDua"];
        public static string RutaZipExe = ConfigurationManager.AppSettings["RutaZipExe"];
       
                
        
        public static string sAnioActual = DateTime.Now.Year.ToString();
        
       
        public const int iSinLiquidar= 1;
        public const int iLiquidadoParcial = 2;
        public const int iLiquidado = 3;
        public const string sNormal = "1";
        public const string sValorizado = "2";
        public const int iHora = 53;

        public const int NaveProvisional = 1;
        public const int NaveDefinitivo = 2;
        public const int NaveRechazado = 3;
        public const int iFCL = 75;
        public const int iLCL = 76;
        public const int iRODANTE = 77;
        public const int iSUELTA = 78;

        public const int CliRol_Embarcador = 101;
        public const int CliRol_Consignatario = 102;
        public const string sEstadoAutorizacion = "Vencida";


        public const string sTab_Puerto = "[Documentacion.Puerto]";
        public const string sCamCon_Puerto = "Puerto_vch_CodigoAduana";
        public const string sCamMos_Puerto = "IdPuerto,Puerto_vch_CodigoAduana,Puerto_vch_Descripcion";

        public const string sTab_Embalaje = "[Documentacion.TipoEmbalaje]";
        public const string sCamCon_Embalaje = "TipEmb_vch_DescripcionCorta";
        public const string sCamMos_Embalaje = "IdTipoEmbalaje,TipEmb_vch_DescripcionCorta,TipEmb_vch_Descripcion";

        public const string sTab_Carga = "[Documentacion.TipoCarga]";
        public const string sCamCon_Carga = "convert(int,IdCodigoGits)";
        public const string sCamMos_Carga = "IdTipoCarga,IdCodigoGits,TipCar_vch_Descripcion";

        public const string sTab_DocumentoOrigen = "[Documentacion.DocumentoOrigen]";
        public const string sCamCon_DocumentoOrigen = "DocOri_chr_estado='1' AND DocOri_vch_NumeroDo";
        public const string sCamMos_DocumentoOrigen = "IdDocOri,DocOri_vch_NumeroDo";

        public const string sTab_Cliente = "[Comercial.Cliente]";
        public const string sCamCon_Cliente = "  Client_chr_Estado=1 and Client_vch_NumDocumento";
        public const string sCamMos_Cliente = "IdCliente,Client_vch_Razon_Social,Client_vch_NumDocumento,Client_vch_DirecciónFiscal";

        public const string sTab_Linea = "[Documentacion.LineaMaritima]";
        public const string sCamCon_Linea = "IdCodigoGits";
        public const string sCamMos_Linea = "IdLineaMaritima,LinMar_vch_Descripcion,IdCodigoGits";

        public const string sTab_TCarga = "[Documentacion.Carga]";
        public const string sCamCon_TCarga = "IdCodigoGits";
        public const string sCamMos_TCarga = "IdCarga,Car_vch_Descripcion,IdCodigoGits";

        public const string sTab_Servicio = "[Comercial.Servicio]";
        public const string sCamCon_Servicio = "IdServicio";
        public const string sCamMos_Servicio = "IdServicio,Ser_Vch_Descripcion";


        //public const string sTab_TarVehi_In = " [Operacion.MovimientoBalanza]	MovBal " +
        //                                    " inner Join [Operacion.Vehiculo] vehi on " +
        //                                    " Vehi.IdVehiculo	 = MovBal.IdVehiculo " +			
        //                                    " left join [Documentacion.AutorizacionRetiro] AutRet" +
        //                                    "  on MovBal.IdAutRet = AutRet.IdAutRet";

        //public const string sCamCon_TarVehi_In = " MovBal.IdMovimiento	in (1,2) " + 
        //                                      " and  isnull(MovBal.Movbal_chr_IndicadorCerrado,'0')	= '0' " +
        //                                      " and	 MovBal.Movbal_chr_Estado		                = '1' " +
        //                                      " and  vehi.Vehicu_vch_Placa			                ";
        //public const string sCamMos_TarVehi_In = "vehi.idVehiculo	,vehi.Vehicu_vch_Placa,movBal.IdMovBal,MovBal.IdMovimiento,AutRet.AutRet_vch_Numero";

        //public const string sTab_TarVehi_Sa = " [Operacion.MovimientoBalanza]	MovBal " +
        //                                   " inner Join [Operacion.Vehiculo] vehi on " +
        //                                   " Vehi.IdVehiculo	 = MovBal.IdVehiculo " +
        //                                   " left join [Documentacion.AutorizacionRetiro] AutRet" +
        //                                   "  on MovBal.IdAutRet = AutRet.IdAutRet";

        //public const string sCamCon_TarVehi_Sa = " MovBal.IdMovimiento	in (3,4) " +
        //                                      " and  isnull(MovBal.Movbal_chr_IndicadorCerrado,'0')	= '0' " +
        //                                      " and	 MovBal.Movbal_chr_Estado		                = '1' " +
        //                                      " and  vehi.Vehicu_vch_Placa			                ";
        //public const string sCamMos_TarVehi_Sa = "vehi.idVehiculo	,vehi.Vehicu_vch_Placa,movBal.IdMovBal,MovBal.IdMovimiento,AutRet.AutRet_vch_Numero";



    }

    

    public enum TipoSolicitud
    {
        Normal = 0,
        Automatico = 1,
        Web = 2,
        Presencial = 3    
    }

    public enum LineaNegocio
    {
        DepositoTemporal = 89,
        DepositoAduanero = 89

    }
    //public enum ConsultaComun
    //{
    //    CampoCondicion = 1,
    //    SpConsultaDua = 2,
    //    DocumentoOrigen = 3,
    //    FieldMostrado = 

    //}

    public enum TipoMovimientoCs
    {
        Ingreso =   1,
        Salida  =   2,
        Llenado =   3,
        Apertura    =4,
        Separacion_Desdoble =5

    }
    public enum TipoOperacion
    {
        Importacion = 66,
        Exportacion = 65,     
    }
    public enum Situacion_PreLiquidacion
    {
        Normal = 0,
        Peligrosa = 1,
        Sin_Tarifa = 2,
        Pendiente = 3
    }

    public enum Tarifa_TipoModalidad
    {
        Dias = 79,
        Unidad = 80,
        Hora = 81,
        Cantidad = 82,
        Peso = 83
    }

    public enum DocumentoExpediente_Estado
    {
        Pendiente = 1,
        Rechazado = 2,
        Transmitido = 3,
        Aceptado = 4,
    }

    public enum ModalidadCobro
    {
        Por_Dias = 79,
        Por_Uno = 80,
        Por_Hora = 81,
        Por_Cantidad = 82,
        Por_Peso = 83,
        Por_Volumen = 84,      
    }


}
