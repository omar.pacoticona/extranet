
using System;

namespace FENIX.Common
{
    [Serializable]
    public class Paginador
    {
        #region " Campos "
        private int _NPagina;
        private int _NTotalRegistros;
        private int _NRegistros;
        private int _NFilaInicio;
        private int _iIdLineaNegocio;
        private string _sNombrePc = string.Empty;
        private string _sUsuario = string.Empty;
        #endregion

        #region " Propiedades "
        public int iIdLineaNegocio
        {
            get { return _iIdLineaNegocio; }
            set { _iIdLineaNegocio = value; }
        }
        public string sUsuario
        {
            get { return _sUsuario; }
            set { _sUsuario = value; }
        }


        public string sNombrePc
        {
            get { return _sNombrePc; }
            set { _sNombrePc = value; }
        }

        public int NPagina
        {
            get { return _NPagina; }
            set { _NPagina = value; }
        }
        public int NTotalRegistros
        {
            get { return _NTotalRegistros; }
            set { _NTotalRegistros = value; }
        }
        public int NRegistros
        {
            get { return _NRegistros; }
            set { _NRegistros = value; }
        }
        public int NFilaInicio
        {
            get { return _NFilaInicio; }
            set { _NFilaInicio = value; }
        }        
        #endregion

    }
}
