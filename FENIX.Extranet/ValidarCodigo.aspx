﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ValidarCodigo.aspx.cs" Inherits="ValidarCodigo" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager runat="server"></asp:ScriptManager>
    <div>
    <asp:UpdatePanel ID="upMain" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <table cellpadding="0" cellspacing="0" border="0px" align="center" width="100%" >
                <tr>
                    <td align="center">
                        <img src="Imagenes/login_logo.jpg" />
                        <br />
                    </td>
                </tr>
                <tr>
                    <td align="center">
                       <h3> Ingresar DNI</h3>
                    </td>
                </tr>
                <tr>
                    <td align="center">
                        <asp:TextBox ID="txtCodigo" runat="server" Height="21px" Width="203px" MaxLength="12" Font-Size="15px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td align="center">
                        <asp:ImageButton ID="ImbIngreso" runat="server" ImageUrl="~/Imagenes/formulario/btn_Procesar.jpg" OnClick="ImbIngreso_Click"/>
                    </td>
                </tr>
                <tr>
                    <td>

                    </td>
                </tr>
                 <tr>
                    <td align="center">
                       <h3> Ingresar Placa</h3>
                    </td>
                </tr>
                <tr>
                    <td align="center">
                        <asp:TextBox ID="txtPlaca" runat="server" Height="21px" Width="203px" MaxLength="12" Font-Size="15px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td align="center">
                        <asp:ImageButton ID="ImbPlaca" runat="server" ImageUrl="~/Imagenes/formulario/btn_Procesar.jpg" OnClick="ImbPlaca_Click"/>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
    </div>
    </form>
</body>
</html>
