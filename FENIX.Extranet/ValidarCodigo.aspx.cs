﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using QNET.Web.UI.Controls;
using FENIX.BusinessEntity;
using FENIX.BusinessLogic;
using System.Text;
using FENIX.Common;
using System.Collections.Generic;
using System.Drawing;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.IO;
public partial class ValidarCodigo : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void ImbIngreso_Click(object sender, ImageClickEventArgs e)
    {
        String Resp = "";
        String Codigo = txtCodigo.Text;
        if (Codigo == "")
        {
            Mensaje("Ingrese DNI para Verificación");
            return;
        }
        BL_DocumentoOrigen objNegocio = new BL_DocumentoOrigen();
        Resp = objNegocio.ValidarDNI(Codigo);
        Mensaje(Resp);
    }

    void Mensaje(String SMS)
    {
        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "", "<script>alert('" + SMS + "');</script>", false);
    }

    protected void ImbPlaca_Click(object sender, ImageClickEventArgs e)
    {
        String Resp = "";
        String Codigo = txtPlaca.Text;
        if (Codigo == "")
        {
            Mensaje("Ingrese Placa para Verificación");
            return;
        }
        BL_DocumentoOrigen objNegocio = new BL_DocumentoOrigen();
        Resp = objNegocio.ValidarPlaca(Codigo);
        Mensaje(Resp);
    }
}