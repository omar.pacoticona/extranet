﻿using FENIX.BusinessEntity;
using FENIX.BusinessLogic;
using System;
using System.Collections.Generic;

/*MAS INFO*/
/* https://pspdfkit.com/blog/2019/html-to-pdf-in-javascript/ */

public partial class Extranet_Documentacion_AutorizacionRetiroPDF : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        ViewState["id"] = Request.QueryString["id"];
        if (!Page.IsPostBack)
        {
            try
            {
                int id = Convert.ToInt32(ViewState["id"].ToString());
                listargrilla(id);
                listarLBl(id);
            }
            catch
            {
                Page.ClientScript.RegisterStartupScript(Page.GetType(), "Message Box", "<script language = 'javascript'>alert('--');window.close();</script>");
            }

        }
    }

        void listarLBl(int id)
        {
            BL_AutorizacionRetiro oBL_AutorizacionRetiro = new BL_AutorizacionRetiro();
            BE_AutorizacionRetiro oBE_AutorizacionRetiro = new BE_AutorizacionRetiro();

            oBE_AutorizacionRetiro = oBL_AutorizacionRetiro.ListarAutorizacionRetiroPorID_Rep(id);
            oBE_AutorizacionRetiro.sUsuario = GlobalEntity.Instancia.Usuario;
            lblUserImp.Text = oBE_AutorizacionRetiro.sUsuario;
            lblUserReg.Text = oBE_AutorizacionRetiro.RepUserRegi;
            lblFechaImpr.Text = String.Format("{0:dd/MM/yyyy}", DateTime.Today);


            lblnroAutRet.Text = oBE_AutorizacionRetiro.RepNroAutRet;
            lblManifiesto.Text = oBE_AutorizacionRetiro.RepNumeroManifiesto;//oBE_AutorizacionRetiro.sAnoManifiesto + " - " + oBE_AutorizacionRetiro.sNroManifiesto;
            lblNavViajeRumbo.Text = oBE_AutorizacionRetiro.RepNaveViaje;
            lblOperacion.Text = oBE_AutorizacionRetiro.RepOperacion;
            lblFechaDescarga.Text = String.Format("{0:dd/MM/yyyy}", oBE_AutorizacionRetiro.RepFechaFinDescarga);
            lblConsignatario.Text = oBE_AutorizacionRetiro.RepConsignatario;
            lblAgencia.Text = oBE_AutorizacionRetiro.RepAgenciaAduana;
            lblDespachador.Text = oBE_AutorizacionRetiro.RepDespachador;
            lblFechaEmision.Text = String.Format("{0:dd/MM/yyyy}", oBE_AutorizacionRetiro.RepFecha);
            lblNumeroDua.Text = oBE_AutorizacionRetiro.RepNumeroDUA;
            lblVolante.Text = oBE_AutorizacionRetiro.RepVolante;
            lblFechaVigencia.Text = String.Format("{0:dd/MM/yyyy}", oBE_AutorizacionRetiro.RepFechaVigencia);
            LblFechaMaxVigenc.Text = String.Format("{0:dd/MM/yyyy}", oBE_AutorizacionRetiro.RepFMaxVigencia);

        }

        void listargrilla(int id)
        {
            BL_AutorizacionRetiro oBL_AutorizacionRetiro = new BL_AutorizacionRetiro();
            List<BE_AutorizacionRetiro> lista = oBL_AutorizacionRetiro.ListarAutorizacionRetiroPorID_Det_Rep(id);

        double pesoNeto = 0, pesoBruto = 0, pesoTara = 0;
        decimal dCantidad=0;
            int i = 0;

            for (i = 0; i < lista.Count; i++)
            {
                pesoNeto = pesoNeto + lista[i].RepPesoNeto;
                pesoTara = +lista[0].RepTara;
                dCantidad = dCantidad + lista[i].RepCantidad;
            }

            pesoBruto = pesoNeto + pesoTara;
            lblCantidad.Text = dCantidad.ToString();
            lblPesoNeto.Text = pesoNeto.ToString() + ".00";
            lblPesoBruto.Text = pesoBruto.ToString() + ".00";



            GrvListado.DataSource = lista;
            GrvListado.DataBind();

        }
    }