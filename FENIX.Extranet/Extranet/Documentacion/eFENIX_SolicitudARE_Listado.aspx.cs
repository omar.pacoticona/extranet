﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Script.Serialization;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using FENIX.BusinessEntity;
using FENIX.BusinessLogic;
using FENIX.Common;
using Newtonsoft.Json;
using SpreadsheetLight;

namespace Documentacion
{
    public partial class eFENIX_AutorizacionRetiro : System.Web.UI.Page
    {


        BL_AutorizacionRetiro oBL_AutorizacionRetiro = new BL_AutorizacionRetiro();
        List<BE_AutorizacionRetiro> oBE_AutorizacionRetiroList = new List<BE_AutorizacionRetiro>();
        BE_AutorizacionRetiro oBE_AutorizacionRetiro = new BE_AutorizacionRetiro();

        BL_DocumentoOrigen oBL_DocumentoOrigen = new BL_DocumentoOrigen();
        List<BE_DocumentoOrigen> oBE_DocumentoOrigen = new List<BE_DocumentoOrigen>();
        BE_DocumentoOrigen oBE_DocOrigen = new BE_DocumentoOrigen();

        int IdAgente = 0;



        private void MensajeScript(string SMS)
        {
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "", "<script>alert('" + SMS + "');</script>", false);
        }

        protected void Page_Load(object sender, EventArgs e)
        {

            String tiempo = (Convert.ToInt32(GlobalEntity.Instancia.TiempoDuracionSesion.ToString()) * 1000 * 60).ToString();
            Session["Reset"] = true;

            ScriptManager scriptManager = ScriptManager.GetCurrent(this.Page);

            txtIdSolicitud.Attributes.Add("onkeypress", "javascript:return fc_Enter(event,'txtIdSolicitud');");
            txtBlBusqueda.Attributes.Add("onkeypress", "javascript:return fc_Enter(event,'txtBlBusqueda');");
            txtClienteBusqueda.Attributes.Add("onkeypress", "javascript:return fc_Enter(event,'txtClienteBusqueda');");
            IdAgente = GlobalEntity.Instancia.IdCliente;

            ViewState["id"] = Request.QueryString["id"];


            if (!Page.IsPostBack)
            {
                txtP_datepickerInicio.Text = "01" + DateTime.Now.ToString("dd/MM/yyyy").Substring(2, 8);
                txtP_datepickerFin.Text = DateTime.Now.ToString("dd/MM/yyyy");

                oBE_AutorizacionRetiroList.Add(oBE_AutorizacionRetiro);
                GrvSolicitud.DataSource = oBE_AutorizacionRetiroList;
                GrvSolicitud.DataBind();
                ViewState["usuario"] = GlobalEntity.Instancia.Usuario;
                //ViewState["Index"] = "0";
                try
                {

                    if (ViewState["id"].ToString() != null)
                    {
                        MensajeScript("Solicitud Generada N°: " + ViewState["id"].ToString());

                        txtIdSolicitud.Text = ViewState["id"].ToString();
                        btnBuscarSolicitud_Click(null, null);
                    }
                }
                catch (Exception ex)
                {

                }


                TabPanel3.Enabled = false;
                tbListaDetalle.ActiveTabIndex = 0;



                ScriptManager.RegisterStartupScript(this, GetType(), "mensaje", "SessionExpireAlert(" + tiempo + ");", true);
            }
            else
                ScriptManager.RegisterStartupScript(this, GetType(), "mensaje", "myStopFunction();", true);

            ViewState["imgBtnUplBL"] = "0";
            ViewState["imgBtnUplEndoseLN"] = "0";
            ViewState["imgBtnUplSenasa"] = "0";
            ViewState["imgBtnUplMedioPago"] = "0";
        }


        protected void btn_cerrar_Click(object sender, EventArgs e)
        {
            if (GlobalEntity.Instancia.CerrarExtranet == "S")
            {
                BL_Usuario oBL_Usuario = new BL_Usuario();
                oBL_Usuario.GrabaInicioSesion(GlobalEntity.Instancia.Usuario, "0");
                Session.Abandon();
                FormsAuthentication.SignOut();
                Response.Redirect("../../eFENIX_Login.aspx");
                Response.End();
            }
        }


        #region "Tab-listar Solicitud ARE"

        protected void btnNuevaSol_Click(object sender, EventArgs e)
        {
            Response.Redirect("eFENIX_SolicitudARE_Registro.aspx");
        }

        protected void btnBuscarSolicitud_Click(object sender, EventArgs e)
        {
            try
            {
                oBE_AutorizacionRetiro.IdSolicitudAre = string.IsNullOrEmpty(txtIdSolicitud.Text) ? 0 : Convert.ToInt32(txtIdSolicitud.Text);
                oBE_AutorizacionRetiro.sNumeroDO = txtBlBusqueda.Text;
                oBE_AutorizacionRetiro.SCliente = txtClienteBusqueda.Text;
                oBE_AutorizacionRetiro.dtFechaFin = Convert.ToDateTime(txtP_datepickerFin.Text);
                oBE_AutorizacionRetiro.dtFechaInicio = Convert.ToDateTime(txtP_datepickerInicio.Text);
                oBE_AutorizacionRetiro.SUsuario = GlobalEntity.Instancia.Usuario;
                oBE_AutorizacionRetiro.iIdAgencia = IdAgente;
                oBE_AutorizacionRetiro.idSituacion = Convert.ToInt32(dplSituacion.SelectedValue);

                if (dplTipoSolicitud.SelectedValue.Equals("1"))
                {
                    var result = oBL_AutorizacionRetiro.Ext_SO_AutorizacionRetiro(oBE_AutorizacionRetiro, 1);
                    oBE_AutorizacionRetiroList = result.Item1;
                }
                else
                {
                    var result = oBL_AutorizacionRetiro.Ext_SO_AutorizacionRetiro(oBE_AutorizacionRetiro, 7);
                    oBE_AutorizacionRetiroList = result.Item1;
                }


                if (oBE_AutorizacionRetiroList.Count > 0)
                {
                    GrvSolicitud.DataSource = oBE_AutorizacionRetiroList;
                    GrvSolicitud.DataBind();
                }
                else
                {
                    oBE_AutorizacionRetiroList.Add(oBE_AutorizacionRetiro);
                    GrvSolicitud.DataSource = oBE_AutorizacionRetiroList;
                    GrvSolicitud.DataBind();
                }


                tbListaDetalle.ActiveTabIndex = 0;
                TabPanel3.Enabled = false;
            }
            catch (Exception)
            {

            }


        }

        List<BE_AutorizacionRetiro> listarDetalleSolicitud(int idSolicitud)
        {
            oBE_AutorizacionRetiro.IdSolicitudAre = idSolicitud;

            var res = oBL_AutorizacionRetiro.Ext_SO_AutorizacionRetiroDet(oBE_AutorizacionRetiro, 1);


            return res.Item1;
        }
        protected void tbListaDetalle_ActiveTabChanged(object sender, EventArgs e)
        {
            AjaxControlToolkit.TabContainer oTabContainer = (AjaxControlToolkit.TabContainer)sender;

            if (oTabContainer.ActiveTabIndex == 0)
            {
                limpiar();
                TabPanel3.Enabled = false;

            }
            if (oTabContainer.ActiveTabIndex == 1)
            {
                limpiar();
                try
                {
                    if (vl_idSituacion.Value.Equals("3"))
                    {
                        FileUploadBL.Enabled = true;
                        FileUploadEndoseLN.Enabled = true;
                        FileUploadSenasa.Enabled = true;
                        FileUploadMedioPago.Enabled = true;
                        btnSolRevicion.Enabled = true;
                        btnActLiquidacion.Enabled = true;
                        TabPanel3.Enabled = true;
                    }
                    else
                    {
                        FileUploadBL.Enabled = false;
                        FileUploadEndoseLN.Enabled = false;
                        FileUploadSenasa.Enabled = false;
                        FileUploadMedioPago.Enabled = false;
                        btnSolRevicion.Enabled = false;
                        btnActLiquidacion.Enabled = false;
                        TabPanel3.Enabled = false;
                    }


                    if (vl_idSituacion.Value.Equals("0")|| vl_idSituacion.Value.Equals("1")||  vl_idSituacion.Value.Equals("4"))
                    {
                        TabPanel3.Enabled = true;
                    }

                    string idSolicitudRet = vi_IdSolicitudAretiro.Value;
                    txtVolanteList.Text = vi_nroVolante.Value;
                    txtBlList.Text = vi_nroNumeroDO.Value;

                    verPago(Convert.ToInt32(idSolicitudRet));
                    oBE_AutorizacionRetiroList = listarDetalleSolicitud(Convert.ToInt32(idSolicitudRet));

                    lblBlNombreArchivoAdj.Font.Size = 6;

                    if (oBE_AutorizacionRetiroList.Count > 0)
                    {

                        lblDespachador.Text = oBE_AutorizacionRetiroList[0].sDniDespachador + " | " + oBE_AutorizacionRetiroList[0].sNombreDespachador;
                        txtObsCliente.Text = oBE_AutorizacionRetiroList[0].sObservacionCliente;
                        txtObsRevision.Text = oBE_AutorizacionRetiroList[0].sObservacionResp;

                        if (oBE_AutorizacionRetiroList[0].iTipoSolicitud == 1)
                        {
                            for (int i = 0; i < oBE_AutorizacionRetiroList.Count; i++)
                            {
                                if (oBE_AutorizacionRetiroList[i].iTipoDocAdjuntado == 1)
                                {
                                    lblUplBLID.Text = oBE_AutorizacionRetiroList[i].IdSolicitudAreDet.ToString();
                                    lblBlNombreArchivoAdj.Text = oBE_AutorizacionRetiroList[i].sRuta;
                                    lblBlEstadoArchivoAdj.Text = oBE_AutorizacionRetiroList[i].SSituacion;
                                    colorsLabel(lblBlEstadoArchivoAdj, oBE_AutorizacionRetiroList[i].idSituacion);
                                    if (oBE_AutorizacionRetiroList[i].idSituacion == 3)
                                    {
                                        FileUploadBL.Enabled = true;
                                    } else { FileUploadBL.Enabled = false; }
                                }
                                if (oBE_AutorizacionRetiroList[i].iTipoDocAdjuntado == 2)
                                {
                                    lblUplEndoseLNID.Text = oBE_AutorizacionRetiroList[i].IdSolicitudAreDet.ToString();
                                    lblEndoseNombreArchivoAdj.Text = oBE_AutorizacionRetiroList[i].sRuta;
                                    lblEndoseEstadoArchivoAdj.Text = oBE_AutorizacionRetiroList[i].SSituacion;
                                    colorsLabel(lblEndoseEstadoArchivoAdj, oBE_AutorizacionRetiroList[i].idSituacion);
                                    if (oBE_AutorizacionRetiroList[i].idSituacion == 3)
                                    {
                                        FileUploadEndoseLN.Enabled = true;
                                    } else { FileUploadEndoseLN.Enabled = false; }

                                }
                                if (oBE_AutorizacionRetiroList[i].iTipoDocAdjuntado == 3)
                                {
                                    lblUplSenasaID.Text = oBE_AutorizacionRetiroList[i].IdSolicitudAreDet.ToString();
                                    lblSenasaNombreArchivoAdj.Text = oBE_AutorizacionRetiroList[i].sRuta;
                                    lblSenasaEstadoArchivoAdj.Text = oBE_AutorizacionRetiroList[i].SSituacion;
                                    colorsLabel(lblSenasaEstadoArchivoAdj, oBE_AutorizacionRetiroList[i].idSituacion);
                                    if (oBE_AutorizacionRetiroList[i].idSituacion == 3)
                                    {
                                        FileUploadSenasa.Enabled = true;
                                    }else { FileUploadSenasa.Enabled = false; }
                                }
                                if (oBE_AutorizacionRetiroList[i].iTipoDocAdjuntado == 4)
                                {
                                    lblUplMedioPagoID.Text = oBE_AutorizacionRetiroList[i].IdSolicitudAreDet.ToString();
                                    lblMedioPagoNombreArchivoAdj.Text = oBE_AutorizacionRetiroList[i].sRuta;
                                    lblMedioPagoEstadoArchivoAdj.Text = oBE_AutorizacionRetiroList[i].SSituacion;
                                    colorsLabel(lblMedioPagoEstadoArchivoAdj, oBE_AutorizacionRetiroList[i].idSituacion);
                                    if (oBE_AutorizacionRetiroList[i].idSituacion == 3)
                                    {
                                        FileUploadMedioPago.Enabled = true;
                                    }else { FileUploadMedioPago.Enabled = false; }
                                }
                            }
                        }

                        if (oBE_AutorizacionRetiroList[0].iTipoSolicitud == 2)
                        {
                            for (int i = 0; i < oBE_AutorizacionRetiroList.Count; i++)
                            {
                                if (oBE_AutorizacionRetiroList[i].iTipoDocAdjuntado == 5)
                                {
                                    lblUplMedioPagoID.Text = oBE_AutorizacionRetiroList[i].idSolicitudAreDet.ToString();
                                    lblMedioPagoNombreArchivoAdj.Text = oBE_AutorizacionRetiroList[i].sRuta;
                                    lblMedioPagoEstadoArchivoAdj.Text = oBE_AutorizacionRetiroList[i].SSituacion;
                                    colorsLabel(lblMedioPagoEstadoArchivoAdj, oBE_AutorizacionRetiroList[i].idSituacion);
                                    if (oBE_AutorizacionRetiroList[i].idSituacion == 3)
                                    {
                                        FileUploadMedioPago.Visible = true;
                                    }  else { FileUploadMedioPago.Visible = false; }
                                }
                            }
                        }



                    }
          


                    numeroCola(Convert.ToInt32(idSolicitudRet));
                    verLigados(Convert.ToInt32(idSolicitudRet));
                }
                catch (Exception ex)
                {

                }


            }


        }

        void colorsLabel(Label lbl, int accion)
        {

            if (accion == 0)
            {
                lbl.ForeColor = Color.Black;
                lbl.Font.Size = 6;
            }
            else if (accion == 1)
            {
                lbl.ForeColor = Color.Yellow;
                lbl.Font.Size = 6;
            }
            else if (accion == 2)
            {
                lbl.ForeColor = Color.Green;
                lbl.Font.Size = 6;
            }
            else if (accion == 3)
            {
                lbl.ForeColor = Color.Red;
                lbl.Font.Size = 6;
            }
        }

        protected void TabContainer1_ActiveTabChanged(object sender, EventArgs e)
        {

        }

        protected void GrvSolicitud_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataKey dataKey;
                dataKey = this.GrvSolicitud.DataKeys[e.Row.RowIndex];
                BE_AutorizacionRetiro oitem = (e.Row.DataItem as BE_AutorizacionRetiro);

                CheckBox chkSeleccion = (CheckBox)e.Row.FindControl("ChkSeleccionar");
                e.Row.Style["cursor"] = "pointer";

                e.Row.Attributes["onclick"] = String.Format("javascript:fc_SeleccionaFilaSimpleUc(this,document.getElementById('{0}'),'{1}','{2}','{3}','{4}','{5}');", chkSeleccion.ClientID, oitem.IdSolicitudAre, oitem.sNumeroDO, oitem.sVolanteNumero, oitem.iTipoSolicitud,oitem.idSituacion);

                e.Row.Attributes["ondblclick"] = String.Format("javascript: SetActiveTab({0}, 1);", tbListaDetalle.ClientID);


                ImageButton imgAsignarse = e.Row.FindControl("Image") as ImageButton;


                e.Row.Cells[12].Visible = false;

                if (oitem.idSituacion == 0)
                {
                    //   e.Row.Cells[12].Visible = false;
                }
                if (oitem.idSituacion == 1)
                {
                    e.Row.Cells[10].BackColor = Color.Yellow;
                    e.Row.Cells[10].ForeColor = Color.DarkBlue;
                    // e.Row.Cells[12].Visible = false;
                }
                else if (oitem.idSituacion == 2)
                {
                    e.Row.Cells[10].BackColor = Color.Green;
                    e.Row.Cells[10].ForeColor = Color.White;
                    e.Row.Cells[12].Visible = true;
                }
                else if (oitem.idSituacion == 3)
                {
                    e.Row.Cells[10].BackColor = Color.Red;
                    e.Row.Cells[10].ForeColor = Color.White;
                    //  e.Row.Cells[12].Controls.Remove(imgAsignarse);
                }


            }
        }

        //protected Comun.ErrorValidacion RecuperarCodigo()
        //{
        //    String vl_iCodigo, vl_idAutRet;
        //    Comun.ErrorValidacion oErrorValidacion;

        //    oErrorValidacion = Comun.ErrorValidacion.Error;
        //    //if (oBotones == Comun.Botones.Actualizar)
        //    //{
        //    foreach (GridViewRow GrvRow in GrvSolicitud.Rows)
        //    {
        //        if ((GrvRow.FindControl("ChkSeleccionar") as CheckBox).Checked)
        //        {
        //            vl_iCodigo = GrvSolicitud.DataKeys[GrvRow.RowIndex].Values["sAutRetNumero"].ToString();
        //            vl_idAutRet = GrvSolicitud.DataKeys[GrvRow.RowIndex].Values["idAutRet"].ToString();
        //            ViewState["vl_iCodigo"] = vl_iCodigo;
        //            ViewState["vl_idAutRet"] = vl_idAutRet;
        //            oErrorValidacion = Comun.ErrorValidacion.Exito;
        //            return oErrorValidacion;

        //        }
        //        //}

        //    }
        //    return oErrorValidacion;
        //}

        protected void GrvSolicitud_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            Int32 idAutRet = 0;
            String pagina = "";
            GridViewRow fila;
            fila = (GridViewRow)(((ImageButton)e.CommandSource).NamingContainer);
            int rowindex = fila.RowIndex;

            if (e.CommandName == "OpenPDF")
            {

                idAutRet = Convert.ToInt32(GrvSolicitud.DataKeys[fila.RowIndex].Values["idAutRet"].ToString());

                ScriptManager.RegisterStartupScript(Page, typeof(Page), "OpenWindow", String.Format("javascript: fc_ImprimirPdf('{0}');", idAutRet.ToString()), true);

            }


        }

        void verPago(int idSolicitudAre)
        {
            txtDatosPago1.ForeColor = Color.Black;
            txtDatosPago2.ForeColor = Color.Black;
            string resultad1 = "", resultad2 = "";

            oBE_AutorizacionRetiro.IdSolicitudAre = idSolicitudAre;

            var result = oBL_AutorizacionRetiro.Ext_SO_AutorizacionRetiro(oBE_AutorizacionRetiro, 11);
            oBE_AutorizacionRetiroList = result.Item1;


            if (oBE_AutorizacionRetiroList.Count > 0)
            {
                if (oBE_AutorizacionRetiroList[0].sTipoPago.Equals("Credito"))
                {
                    resultad1 = "TIPO DE PAGO \t: " + oBE_AutorizacionRetiroList[0].sTipoPago.ToUpper() + "\n" +
                                "RUC FACTURAR\t: " + oBE_AutorizacionRetiroList[0].sRucCliente;


                    resultad2 = "N° PRE-LIQ.\t\t: " + oBE_AutorizacionRetiroList[0].idLiquidacion + "\n" +
                                "FECHA PRE-LIQ. \t: " + oBE_AutorizacionRetiroList[0].dtFechaLiquidacion + "\n" +
                                "MONTO PRE-LIQ \t: " + oBE_AutorizacionRetiroList[0].dTotalLiquidacion + "\n" +
                                "DUA \t\t\t: " + oBE_AutorizacionRetiroList[0].sNumeroDua;

                    txtDatosPago1.Text = resultad1;
                    txtDatosPago2.Text = resultad2;
                }
                if (oBE_AutorizacionRetiroList[0].sTipoPago.Equals("Contado"))
                {
                    string moneda = "";
                    if (oBE_AutorizacionRetiroList[0].IdMoneda == 23)
                    {
                        moneda = "SOLES";
                    }
                    if (oBE_AutorizacionRetiroList[0].IdMoneda == 24)
                    {
                        moneda = "DOLARES";
                    }

                    resultad1 =
                               "TIPO DE PAGO \t: " + oBE_AutorizacionRetiroList[0].sTipoPago.ToUpper() + "\n" +
                               "FORMA DE PAGO\t: " + oBE_AutorizacionRetiroList[0].sFormaPago + "\n" +
                               "MONTO DE PAGO\t: " + oBE_AutorizacionRetiroList[0].dMontoPago + "\n" +
                               "RUC FACTURAR\t: " + oBE_AutorizacionRetiroList[0].sRucCliente + "\n" +
                               "N° OPERACIÓN \t: " + oBE_AutorizacionRetiroList[0].sNumeroOperacion + "\n" +
                               "MONEDA \t \t: " + moneda + "\n" +
                               "BANCO \t \t \t: " + oBE_AutorizacionRetiroList[0].sBanco;

                    resultad2 = "N° PRE-LIQ.\t\t: " + oBE_AutorizacionRetiroList[0].idLiquidacion + "\n" +
                                "FECHA PRE-LIQ. \t: " + oBE_AutorizacionRetiroList[0].dtFechaLiquidacion + "\n" +
                                "MONTO PRE-LIQ \t: " + oBE_AutorizacionRetiroList[0].dTotalLiquidacion + "\n" +
                                "DUA \t\t\t: " + oBE_AutorizacionRetiroList[0].sNumeroDua;

                    txtDatosPago1.Text = resultad1;
                    txtDatosPago2.Text = resultad2;

                }
            }
        }

        void verLigados(int vl_idSolicitud)
        {

            oBL_AutorizacionRetiro = new BL_AutorizacionRetiro();
            oBE_AutorizacionRetiro = new BE_AutorizacionRetiro();
            oBE_AutorizacionRetiroList = new List<BE_AutorizacionRetiro>();


            oBE_AutorizacionRetiro.IdSolicitudAre = vl_idSolicitud;

            var result = oBL_AutorizacionRetiro.Ext_SO_AutorizacionRetiro(oBE_AutorizacionRetiro, 14);

            oBE_AutorizacionRetiroList = result.Item1;

            grvGrillaLigados.DataSource = oBE_AutorizacionRetiroList;
            grvGrillaLigados.DataBind();
        }


        void numeroCola(int idSolicitudAre)
        {
            //oBE_AutorizacionRetiro.IdSolicitudAre = idSolicitudAre;
            var result = oBL_AutorizacionRetiro.Ext_SO_AutorizacionRetiro(oBE_AutorizacionRetiro, 12);
            oBE_AutorizacionRetiroList = result.Item1;

            int cantAdelante = 0;
            int posicion = 0;

            if (oBE_AutorizacionRetiroList.Count > 0)
            {
                int i = 0;
                for (i = 0; i < oBE_AutorizacionRetiroList.Count; i++)
                {
                    if (oBE_AutorizacionRetiroList[i].IdSolicitudAre == idSolicitudAre)
                    {
                        posicion = oBE_AutorizacionRetiroList[i].iNumeroOrden;
                        break;
                    }
                    cantAdelante++;
                }
            }


            lblPosicion.Text = " :  " + posicion;
        }

        protected void imgBtnUplBL_Click(object sender, EventArgs e)
        {

            var result = cargarArchivo(FileUploadBL, IdAgente.ToString(),"BL");

            if (result.Item2 == 1)
            {
                lblBlNombreArchivoAdj.Text = result.Item1;


                oBE_AutorizacionRetiro.idSolicitudAreDet = Convert.ToInt32(lblUplBLID.Text);
                oBE_AutorizacionRetiro.IdSolicitudAre = Convert.ToInt32(vi_IdSolicitudAretiro.Value);
                oBE_AutorizacionRetiro.iTipoDocAdjuntado = 1;
                oBE_AutorizacionRetiro.sRuta = result.Item1;
                oBE_AutorizacionRetiro.SUsuario = ViewState["usuario"].ToString();

                var res = oBL_AutorizacionRetiro.Ext_SO_AutorizacionRetiroDet(oBE_AutorizacionRetiro, 2);

                lblUplBLID.Text = res.Item2;
                lblBlEstadoArchivoAdj.Text = "Enviado";
                lblBlEstadoArchivoAdj.ForeColor = Color.Black;
            }
            else if (result.Item2 == 2)
            {
                MensajeScript("Ingrese un archivo en Formato PDF");
            }

        }
        protected void imgBtnUplEndoseLN_Click(object sender, EventArgs e)
        {


            var result = cargarArchivo(FileUploadEndoseLN, IdAgente.ToString(),"ED");

            if (result.Item2 == 1)
            {
                lblEndoseNombreArchivoAdj.Text = result.Item1;


                oBE_AutorizacionRetiro.idSolicitudAreDet = Convert.ToInt32(lblUplEndoseLNID.Text);
                oBE_AutorizacionRetiro.IdSolicitudAre = Convert.ToInt32(vi_IdSolicitudAretiro.Value);
                oBE_AutorizacionRetiro.iTipoDocAdjuntado = 2;
                oBE_AutorizacionRetiro.sRuta = result.Item1;
                oBE_AutorizacionRetiro.SUsuario = ViewState["usuario"].ToString();

                var res = oBL_AutorizacionRetiro.Ext_SO_AutorizacionRetiroDet(oBE_AutorizacionRetiro, 2);

                lblUplEndoseLNID.Text = res.Item2;
                lblEndoseEstadoArchivoAdj.Text = "Enviado";
                lblEndoseEstadoArchivoAdj.ForeColor = Color.Black;
            }
            else if (result.Item2 == 2)
            {
                MensajeScript("Ingrese un archivo en Formato PDF");
            }

        }
        protected void imgBtnUplSenasa_Click(object sender, EventArgs e)
        {

            var result = cargarArchivo(FileUploadSenasa, IdAgente.ToString(),"SE");

            if (result.Item2 == 1)
            {
                lblSenasaNombreArchivoAdj.Text = result.Item1;


                oBE_AutorizacionRetiro.idSolicitudAreDet = Convert.ToInt32(lblUplSenasaID.Text);
                oBE_AutorizacionRetiro.IdSolicitudAre = Convert.ToInt32(vi_IdSolicitudAretiro.Value);
                oBE_AutorizacionRetiro.iTipoDocAdjuntado = 3;
                oBE_AutorizacionRetiro.sRuta = result.Item1;
                oBE_AutorizacionRetiro.SUsuario = ViewState["usuario"].ToString();

                var res = oBL_AutorizacionRetiro.Ext_SO_AutorizacionRetiroDet(oBE_AutorizacionRetiro, 2);

                lblUplSenasaID.Text = res.Item2;
                lblSenasaEstadoArchivoAdj.Text = "Enviado";
                lblSenasaEstadoArchivoAdj.ForeColor = Color.Black;
            }
            else if (result.Item2 == 2)
            {
                MensajeScript("Ingrese un archivo en Formato PDF");
            }
        }
        protected void imgBtnUplMedioPago_Click(object sender, EventArgs e)
        {

            var result = cargarArchivo(FileUploadMedioPago, IdAgente.ToString(),"PG") ;

            if (result.Item2 == 1)
            {
                lblMedioPagoNombreArchivoAdj.Text = result.Item1;


                oBE_AutorizacionRetiro.idSolicitudAreDet = Convert.ToInt32(lblUplMedioPagoID.Text);
                oBE_AutorizacionRetiro.IdSolicitudAre = Convert.ToInt32(vi_IdSolicitudAretiro.Value);
                oBE_AutorizacionRetiro.iTipoDocAdjuntado = 4;
                oBE_AutorizacionRetiro.sRuta = result.Item1;
                oBE_AutorizacionRetiro.SUsuario = ViewState["usuario"].ToString();

                var res = oBL_AutorizacionRetiro.Ext_SO_AutorizacionRetiroDet(oBE_AutorizacionRetiro, 2);

                lblUplMedioPagoID.Text = res.Item2;
                lblMedioPagoEstadoArchivoAdj.Text = "Enviado";
                lblMedioPagoEstadoArchivoAdj.ForeColor = Color.Black;
            }
            else if (result.Item2 == 2)
            {
                MensajeScript("Ingrese un archivo en Formato PDF");
            }
        }
        Tuple<string, int> cargarArchivo(FileUpload fileUpload, string IdAgente,string prefijo)
        {
            string ruta = ConfigurationManager.AppSettings["Are_Solicitud"] + IdAgente + "/";

            string filename = "";
            int estado = 0;
            string fecha = DateTime.Now.ToString("yyMMddHmmss");
            try
            {
                if (fileUpload.HasFile)
                {
                    if (fileUpload.PostedFile.ContentType == "application/pdf")
                    {
                        //COMPROBAR SI EXISTE RUTA 
                        if (!File.Exists(ruta))
                        {
                            //SI NO EXISTE CREAR LA RUTA
                            DirectoryInfo directory = Directory.CreateDirectory(ruta);
                        }

                        //GUARDA INFORMACION
                        filename = Path.GetFileName(prefijo+"_"+fecha + ".pdf");
                        fileUpload.SaveAs(ruta + filename);
                        estado = 1;
                    }
                    else
                    {
                        estado = 2;
                    }
                }


            }
            catch (Exception ex)
            {
                //MensajeScript("eror de upload");
                estado = 0;
            }

            return Tuple.Create(filename, estado);
        }

        public static void Descargar(System.Web.HttpResponse sResponse, String sRuta, String sNombreArchivo)
        {
            try
            {
                sResponse.ContentType = "application/octet-stream";
                sResponse.AddHeader("Content-Disposition", "attachment; filename=" + sNombreArchivo);
                sResponse.Clear();
                sResponse.WriteFile(sRuta + sNombreArchivo);
                sResponse.End();
            }
            catch (Exception ex)
            {

            }
        }
        protected void btnDesBL_Click(object sender, EventArgs e)
        {
            try
            {
                if (lblBlNombreArchivoAdj.Text.Length > 1)
                {
                    string ruta = ConfigurationManager.AppSettings["Are_Solicitud"] + IdAgente + "/";
                    Descargar(Response, ruta, lblBlNombreArchivoAdj.Text);
                }

            }
            catch (Exception) { }
        }
        protected void btnDesEndose_Click(object sender, EventArgs e)
        {
            try
            {
                if (lblEndoseNombreArchivoAdj.Text.Length > 1)
                {
                    string ruta = ConfigurationManager.AppSettings["Are_Solicitud"] + IdAgente + "/";
                    Descargar(Response, ruta, lblEndoseNombreArchivoAdj.Text);
                }

            }
            catch (Exception) { }
        }
        protected void btnDesSenasa_Click(object sender, EventArgs e)
        {
            try
            {
                if (lblSenasaNombreArchivoAdj.Text.Length > 1)
                {
                    string ruta = ConfigurationManager.AppSettings["Are_Solicitud"] + IdAgente + "/";
                    Descargar(Response, ruta, lblSenasaNombreArchivoAdj.Text);
                }

            }
            catch (Exception) { }
        }
        protected void btnDesMedioPago_Click(object sender, EventArgs e)
        {
            try
            {
                if (lblMedioPagoNombreArchivoAdj.Text.Length > 1)
                {
                    string ruta = ConfigurationManager.AppSettings["Are_Solicitud"] + IdAgente + "/";
                    Descargar(Response, ruta, lblMedioPagoNombreArchivoAdj.Text);
                }              
            }
            catch (Exception) { }

        }



        protected void GrvSolicitud_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
        protected void btnSolRevicion_Click(object sender, EventArgs e)
        {
            try
            {
                oBE_AutorizacionRetiro = new BE_AutorizacionRetiro();


                oBE_AutorizacionRetiro.sObservacion = txtObsCliente.Text;
                oBE_AutorizacionRetiro.IdSolicitudAre = Convert.ToInt32(vi_IdSolicitudAretiro.Value);// idSolicitudAre;


                var result = oBL_AutorizacionRetiro.Ext_SO_AutorizacionRetiro(oBE_AutorizacionRetiro, 15);
                oBE_AutorizacionRetiroList = result.Item1;
            }
            catch (Exception) { }


        }
        protected void btnActLiquidacion_Click(object sender, EventArgs e)
        {

            lblIdLiquidacion.Text = "";
            lblMonto.Text = "";
            lblFecha.Text = "";
            try
            {
                oBE_DocOrigen = new BE_DocumentoOrigen();

                oBE_DocOrigen.IdSolicitudAre =Convert.ToInt32(vi_IdSolicitudAretiro.Value);
                oBE_DocumentoOrigen = oBL_DocumentoOrigen.ListarDO_AR(oBE_DocOrigen, 5);
                if (oBE_DocumentoOrigen.Count > 0)
                {

                    lblFecha.Text = oBE_DocumentoOrigen[0].sFechCalculo;
                    lblMonto.Text = oBE_DocumentoOrigen[0].dtMontoLiquidacion.ToString();
                    lblIdLiquidacion.Text = oBE_DocumentoOrigen[0].idliquidacion.ToString();

                    mpLiquidacion.Show();
                }
            }
            catch (Exception) { }  

        }
        protected void btnSoliRevLiq_Click(object sender, EventArgs e)
        {
            //tbListaDetalle.ActiveTabIndex = 2;

            try
            {
                oBE_AutorizacionRetiro = new BE_AutorizacionRetiro();

                oBE_AutorizacionRetiro.sObservacion = txtObsCliente.Text;
                oBE_AutorizacionRetiro.IdSolicitudAre = Convert.ToInt32(vi_IdSolicitudAretiro.Value);// idSolicitudAre;
                oBE_AutorizacionRetiro.idLiquidacion = Convert.ToInt32(lblIdLiquidacion.Text);

                var result = oBL_AutorizacionRetiro.Ext_SO_AutorizacionRetiro(oBE_AutorizacionRetiro, 16);
                oBE_AutorizacionRetiroList = result.Item1;
            }
            catch (Exception) { }

        }

        void limpiar()
        {
            lblBlNombreArchivoAdj.Text = "";
            lblBlEstadoArchivoAdj.Text = "";
            lblUplBLID.Text = "0";


            lblEndoseNombreArchivoAdj.Text = "";
            lblEndoseEstadoArchivoAdj.Text = "";
            lblUplEndoseLNID.Text = "0";

            lblSenasaEstadoArchivoAdj.Text = "";
            lblSenasaNombreArchivoAdj.Text = "";
            lblUplSenasaID.Text = "0";

            lblMedioPagoNombreArchivoAdj.Text = "";
            lblMedioPagoEstadoArchivoAdj.Text = "";
            lblUplMedioPagoID.Text = "0";


            txtVolanteList.Text = "";
            txtBlList.Text = "";
            lblPosicion.Text = "";


            FileUploadBL.Enabled = true;
            FileUploadEndoseLN.Enabled = true;
            FileUploadSenasa.Enabled = true;
            FileUploadMedioPago.Enabled = true;

        }
    }
}



#endregion










