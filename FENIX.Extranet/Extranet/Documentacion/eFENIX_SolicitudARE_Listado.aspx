﻿<%@ Page Language="C#"
    MasterPageFile="~/Extranet/Seguridad/eFENIX_Principal.master"
    AutoEventWireup="true"
    CodeFile="eFENIX_SolicitudARE_Listado.aspx.cs"
    Title="ARE-Autorizacion de Retiro Electronico"
    Inherits="Documentacion.eFENIX_AutorizacionRetiro" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title></title>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js" type="text/javascript"></script>
    <script src="../../Script/Java_Script/jsSolicitudARE.js"></script>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentCab" runat="Server">
    <span class="texto_titulo">Tu Sesion Expirara en &nbsp; 
        <span class="texto_titulo" id="seconds"></span>
        <span>&nbsp;seg.</span>
    </span>
    <asp:Button ID="btn_cerrar" runat="server" Text="cerrar"
        OnClick="btn_cerrar_Click" Style="display: none;" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CntMaster" runat="Server">
    <!DOCTYPE html>

    <html>
    <body>
        <div class="form_titulo" style="width: 100%; height: 23px; vertical-align: inherit">
            <p style="margin-top: 1.5px; font-size: 14px;">CONSULTA DE SOLICITUD DE AUTORIZACIÓN DE RETIRO ELECTRÓNICA (ARE)</p>
        </div>
        <div id="tabs-container">
            <fieldset style="width: 99%; background-color: white;">
                <legend style="background-color: #0069AE; font-weight: 700;">Datos de Busqueda</legend>
                <div id="divBusqueda_tab1">
                    <table class="form_bandeja" style="margin-top: 5px; margin-bottom: 5px; width: 100%;">
                        <tr>
                            <td>
                                <asp:Label runat="server">Nº Sol.</asp:Label></td>
                            <td>
                                <asp:TextBox ID="txtIdSolicitud" runat="server" Style="text-transform: uppercase" type="number"
                                    Width="135px"></asp:TextBox>
                               &nbsp;
                                        <asp:Label runat="server">BL/Volante</asp:Label>
                                <asp:TextBox ID="txtBlBusqueda" runat="server" Style="text-transform: uppercase" placeholder="BL O VOLANTE"
                                    Width="130px"></asp:TextBox>
                            </td>
                            <td>TIPO :
                            </td>

                            <td>
                                <asp:DropDownList ID="dplTipoSolicitud" runat="server" Width="130px">
                                    <asp:ListItem Value="1">Generacion</asp:ListItem>
                                    <asp:ListItem Value="2">Renovacion</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td>
                                <asp:Label runat="server">Inicio Fecha </asp:Label></td>
                            <td>
                                <asp:TextBox ID="txtP_datepickerInicio" ReadOnly="True" runat="server" MaxLength="10" Style="text-transform: uppercase"
                                    Width="135px"></asp:TextBox>
                                <asp:Image runat="server" ImageUrl="~/Imagenes/calendario.gif" />
                            </td>
                            <td>
                                <asp:Button ID="btnNuevaSol" runat="server" Text="Nueva  Solicitud" Height="30px" CssClass="bootonClas" OnClick="btnNuevaSol_Click" />
                            </td>

                        </tr>
                        <tr>
                            <td>
                                <asp:Label runat="server">Cliente</asp:Label>

                            </td>
                            <td>
                                <asp:TextBox ID="txtClienteBusqueda" runat="server" Style="text-transform: uppercase"
                                    Width="352px"></asp:TextBox>
                            </td>
                            <td>Estado</td>
                            <td>
                                <asp:DropDownList ID="dplSituacion" runat="server" Width="130px">
                                    <asp:ListItem Value="-1">Todos</asp:ListItem>
                                    <asp:ListItem Value="0">Enviado</asp:ListItem>
                                    <asp:ListItem Value="1">En Proceso</asp:ListItem>
                                    <asp:ListItem Value="2">Aprobado</asp:ListItem>
                                    <asp:ListItem Value="3">Rechazado</asp:ListItem>
                                    <asp:ListItem Value="4">Confirmado</asp:ListItem>
                                </asp:DropDownList></td>
                            <td>
                                <asp:Label runat="server">Fin&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Fecha </asp:Label>
                            </td>

                            <td>
                                <asp:TextBox ID="txtP_datepickerFin" runat="server" Style="text-transform: uppercase" ReadOnly="True"
                                    Width="135px"></asp:TextBox>
                                <asp:Image runat="server" ImageUrl="~/Imagenes/calendario.gif" />
                            </td>
                            <td>
                                <asp:Button ID="btnBuscarSolicitud" runat="server" Text="Buscar Solicitud" Height="30px" CssClass="bootonClas" OnClick="btnBuscarSolicitud_Click" />

                                &nbsp;&nbsp;                           
                            </td>
                        </tr>
                    </table>
                </div>
            </fieldset>
            <br />

            <fieldset style="width: 99%; background-color: white;">
                <legend style="background-color: #0069AE; font-weight: 700;">Datos</legend>
                <table>
                    <tr valign="top" style="height: 50%; width: 100px;">
                        <td>
                            <ajaxToolkit:TabContainer ID="tbListaDetalle" runat="server" ActiveTabIndex="1"
                                OnActiveTabChanged="tbListaDetalle_ActiveTabChanged" AutoPostBack="True" BorderColor="Yellow">
                                <ajaxToolkit:TabPanel ID="TabPanel2" runat="server" HeaderText="Listado de Solicitudes" ScrollBars="Vertical">
                                    <ContentTemplate>
                                        <table>
                                            <tr>
                                                <td>
                                                    <asp:UpdatePanel runat="server" UpdateMode="Conditional">
                                                        <ContentTemplate>
                                                            <asp:Panel ID="Panel2" runat="server" BackColor="White" Height="180px" ScrollBars="Vertical" Width="1020px">
                                                                <asp:GridView ID="GrvSolicitud" runat="server" AutoGenerateColumns="False" DataKeyNames="IdSolicitudAre,sVolanteNumero,sNumeroDO,idSituacion,iTipoSolicitud,idAutRet" SkinID="GrillaConsulta" Width="100%"
                                                                    OnRowDataBound="GrvSolicitud_RowDataBound" OnRowCommand="GrvSolicitud_RowCommand" OnSelectedIndexChanged="GrvSolicitud_SelectedIndexChanged">
                                                                    <Columns>
                                                                        <asp:TemplateField>
                                                                            <ItemTemplate>
                                                                                <asp:CheckBox ID="ChkSeleccionar" runat="server" />
                                                                            </ItemTemplate>
                                                                            <HeaderStyle BackColor="#0069ae" Width="20px" />
                                                                            <ItemStyle Width="20px" />
                                                                        </asp:TemplateField>
                                                                        <asp:BoundField DataField="IdSolicitudAre" HeaderText="Nº Solicitud" />
                                                                        <asp:BoundField DataField="dtFechaRegistro" HeaderText="F. Solicitud" />
                                                                        <asp:BoundField DataField="sVolanteNumero" HeaderText="Nº Volante" />
                                                                        <asp:BoundField DataField="sNumeroDO" HeaderText="Numero Documento" />
                                                                        <asp:BoundField DataField="SCliente" HeaderText="Cliente">
                                                                            <ItemStyle Width="10%" Font-Size="XX-Small" />
                                                                        </asp:BoundField>
                                                                        <asp:BoundField DataField="sAutRetNumero" HeaderText="Nº Aut. Retiro" />
                                                                        <asp:BoundField DataField="idAutRet" HeaderText="Id.Aut.Ret."
                                                                            SortExpression="iIdAutRet" Visible="false" />
                                                                        <asp:BoundField DataField="dtAutRetFecha" HeaderText="F. Emision" />
                                                                        <asp:BoundField DataField="dtAutRetFechaVigencia" HeaderText="F. Vigencia" />
                                                                        <asp:BoundField DataField="sEstadoTramite" HeaderText="Estado" />
                                                                        <asp:BoundField DataField="sTipoSolicitud" HeaderText="Tipo Solicitud" />
                                                                        <asp:TemplateField ShowHeader="False">
                                                                            <ItemTemplate>
                                                                                <asp:ImageButton ID="Image" runat="server" CausesValidation="false" Visible="true"
                                                                                    CommandName="OpenPDF" ImageUrl="~/Script/Imagenes/IconButton/filePDF2.png" ToolTip="Ver PDF" />
                                                                            </ItemTemplate>
                                                                            <ItemStyle Width="1%" HorizontalAlign="Center" />
                                                                        </asp:TemplateField>
                                                                    </Columns>
                                                                </asp:GridView>
                                                            </asp:Panel>
                                                        </ContentTemplate>
                                                    </asp:UpdatePanel>
                                                </td>
                                                <td>&nbsp;&nbsp;                                                             
                                                </td>
                                            </tr>
                                        </table>

                                    </ContentTemplate>

                                </ajaxToolkit:TabPanel>
                                <ajaxToolkit:TabPanel ID="TabPanel3" runat="server" HeaderText="Detalle Solicitud">
                                    <ContentTemplate>
                                        <asp:Panel ID="Panel3" runat="server" BackColor="White" Height="300px" ScrollBars="Vertical">
                                            <table>
                                                <tr>
                                                    <td>
                                                        <table>

                                                            <tr>
                                                                <td>
                                                                    <table>
                                                                        <tr>
                                                                            <td>
                                                                                <table>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <asp:Label Font-Size="XX-Small" runat="server">Volante</asp:Label>
                                                                                        </td>
                                                                                        <td>
                                                                                            <asp:TextBox Font-Size="Smaller" ID="txtVolanteList" ReadOnly="True" runat="server" Style="text-transform: uppercase" Width="135px"></asp:TextBox>
                                                                                        </td>
                                                                                        <td>&nbsp;&nbsp; &nbsp;&nbsp;
                                                                                        </td>
                                                                                        <td>
                                                                                            <asp:Label Font-Size="XX-Small" runat="server">BL</asp:Label>
                                                                                        </td>
                                                                                        <td>
                                                                                            <asp:TextBox ID="txtBlList" runat="server" ReadOnly="True" Style="text-transform: uppercase" Width="135px"></asp:TextBox>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <asp:Panel ID="Panel6" runat="server" BackColor="White" Width="100%">
                                                                                    <table border="1" style="font-size: 10px; border-color: #0069AE">
                                                                                        <tr style="color: ivory; background-color: #0069AE">
                                                                                            <th style="font-size: 8px;">Documento</th>
                                                                                            <th>Nombre Archivo</th>
                                                                                            <th></th>
                                                                                            <th>Estado</th>
                                                                                            <th style="width: 100px">Subsanar</th>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <asp:Label runat="server" ID="lblUplBLID" Text="0" Visible="False"></asp:Label>

                                                                                            <td>BL</td>
                                                                                            <td>
                                                                                                <asp:Label runat="server" ID="lblBlNombreArchivoAdj" Text=" " Font-Size="XX-Small"></asp:Label>

                                                                                            </td>
                                                                                            <td>
                                                                                                <asp:ImageButton runat="server" ID="btnDesBL" ImageUrl="~/Script/Imagenes/IconButton/descargar.png" OnClick="btnDesBL_Click" Width="15px" />

                                                                                            </td>
                                                                                            <td>
                                                                                                <asp:Label runat="server" ID="lblBlEstadoArchivoAdj" Text=" " Font-Size="XX-Small"></asp:Label>

                                                                                            </td>
                                                                                            <td>
                                                                                                <asp:FileUpload ID="FileUploadBL" runat="server" Width="100px" accept="application/pdf" onchange="saveFileUpl('FileUploadBL');" Font-Size="XX-Small" />

                                                                                                <asp:ImageButton runat="server" ID="imgBtnUplBL" ImageUrl="~/Imagenes/save.png" OnClick="imgBtnUplBL_Click" Width="1px" />

                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <asp:Label runat="server" ID="lblUplEndoseLNID" Text="0" Visible="False"></asp:Label>

                                                                                            <td style="font-size: 8px;">Carta Liberacion</td>
                                                                                            <td>
                                                                                                <asp:Label runat="server" ID="lblEndoseNombreArchivoAdj" Text=" " Font-Size="XX-Small"></asp:Label>

                                                                                            </td>
                                                                                            <td>
                                                                                                <asp:ImageButton runat="server" ID="btnDesEndose" ImageUrl="~/Script/Imagenes/IconButton/descargar.png" OnClick="btnDesEndose_Click" Width="15px" />

                                                                                            </td>
                                                                                            <td>
                                                                                                <asp:Label runat="server" ID="lblEndoseEstadoArchivoAdj" Text=" " Font-Size="XX-Small"></asp:Label>

                                                                                            </td>
                                                                                            <td>
                                                                                                <asp:FileUpload ID="FileUploadEndoseLN" runat="server" Width="100px" accept="application/pdf" onchange="saveFileUpl('FileUploadEndoseLN');" Font-Size="XX-Small" />

                                                                                                <asp:ImageButton runat="server" ID="imgBtnUplEndoseLN" ImageUrl="~/Imagenes/save.png" OnClick="imgBtnUplEndoseLN_Click" Width="1px" />

                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <asp:Label runat="server" ID="lblUplSenasaID" Text="0" Visible="False"></asp:Label>

                                                                                            <td style="font-size: 8px;">Senasa</td>
                                                                                            <td>
                                                                                                <asp:Label runat="server" ID="lblSenasaNombreArchivoAdj" Text=" " Font-Size="XX-Small"></asp:Label>

                                                                                            </td>
                                                                                            <td>
                                                                                                <asp:ImageButton runat="server" ID="btnDesSenasa" ImageUrl="~/Script/Imagenes/IconButton/descargar.png" OnClick="btnDesSenasa_Click" Width="15px" />

                                                                                            </td>
                                                                                            <td>
                                                                                                <asp:Label runat="server" ID="lblSenasaEstadoArchivoAdj" Text=" " Font-Size="XX-Small"></asp:Label>

                                                                                            </td>
                                                                                            <td>
                                                                                                <asp:FileUpload ID="FileUploadSenasa" runat="server" Width="100px" accept="application/pdf" onchange="saveFileUpl('FileUploadSenasa');" Font-Size="XX-Small" />

                                                                                                <asp:ImageButton runat="server" ID="imgBtnUplSenasa" ImageUrl="~/Imagenes/save.png" OnClick="imgBtnUplSenasa_Click" Width="1px" />

                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <asp:Label runat="server" ID="lblUplMedioPagoID" Text="0" Visible="False"></asp:Label>

                                                                                            <td style="font-size: 8px;">Medio de Pago</td>
                                                                                            <td>
                                                                                                <asp:Label runat="server" ID="lblMedioPagoNombreArchivoAdj" Text=" " Font-Size="XX-Small"></asp:Label>

                                                                                            </td>
                                                                                            <td>
                                                                                                <asp:ImageButton runat="server" ID="btnDesMedioPago" ImageUrl="~/Script/Imagenes/IconButton/descargar.png" OnClick="btnDesMedioPago_Click" Width="15px" />

                                                                                            </td>
                                                                                            <td>
                                                                                                <asp:Label runat="server" ID="lblMedioPagoEstadoArchivoAdj" Text=" " Font-Size="XX-Small"></asp:Label>

                                                                                            </td>
                                                                                            <td>
                                                                                                <asp:FileUpload ID="FileUploadMedioPago" runat="server" Width="100px" accept="application/pdf" onchange="saveFileUpl('FileUploadMedioPago');" Font-Size="XX-Small" />

                                                                                                <asp:ImageButton runat="server" ID="imgBtnUplMedioPago" ImageUrl="~/Imagenes/save.png" OnClick="imgBtnUplMedioPago_Click" Width="1px" />

                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </asp:Panel>

                                                                            </td>
                                                                        </tr>
                                                                        <tr align="center">
                                                                            <td>
                                                                                <table>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <asp:Button ID="btnSolRevicion" runat="server" Text="Enviar Archivos Para Una Nueva Revision" Height="25px" CssClass="bootonClas" OnClick="btnSolRevicion_Click" Font-Size="Small" />

                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>


                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <table>
                                                                        <tr>
                                                                            <td>
                                                                                <asp:Label runat="server">Comentarios del cliente :</asp:Label>

                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <asp:TextBox ID="txtObsCliente" runat="server" Height="50px" TextMode="MultiLine" Width="400px" TabIndex="18"></asp:TextBox>

                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <table>
                                                                        <tr>
                                                                            <td>
                                                                                <asp:Label runat="server">Despachador :</asp:Label>

                                                                            </td>
                                                                            <td>
                                                                                <asp:Label ID="lblDespachador" runat="server" Font-Size="XX-Small" Text=" "></asp:Label>

                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr align="center">
                                                                <td>Lista de Ligados
                                                                </td>
                                                            </tr>
                                                            <tr id="trLigados" runat="server" align="center">
                                                                <td runat="server">
                                                                    <div class="DivListaGrilla">
                                                                        <asp:UpdatePanel runat="server" UpdateMode="Conditional">
                                                                            <ContentTemplate>
                                                                                <asp:Panel ID="Panel1" runat="server" ScrollBars="Vertical" Width="99%">
                                                                                    <asp:GridView ID="grvGrillaLigados" runat="server" AutoGenerateColumns="False" DataKeyNames=" sVolanteNumero, sNumeroDO" HeaderStyle-BackColor="#0069ae" HeaderStyle-CssClass="clasAlto" SkinID="GrillaConsulta">
                                                                                        <Columns>
                                                                                            <asp:BoundField DataField="sVolanteNumero" HeaderText="Nº Volante" />
                                                                                            <asp:BoundField DataField="sNumeroDO" HeaderText="Numero Documento" />
                                                                                            <asp:BoundField DataField="SCliente" HeaderText="Cliente">
                                                                                                <ItemStyle Width="10%" Font-Size="XX-Small" />
                                                                                            </asp:BoundField>
                                                                                            <asp:BoundField DataField="sNumeroDua" HeaderText="DUA" />
                                                                                        </Columns>
                                                                                    </asp:GridView>
                                                                                </asp:Panel>

                                                                            </ContentTemplate>
                                                                        </asp:UpdatePanel>

                                                                    </div>
                                                                </td>
                                                            </tr>

                                                        </table>
                                                    </td>
                                                    <td>
                                                        <table style="margin-top: auto;">
                                                            <tr>
                                                                <td>
                                                                    <table>
                                                                        <tr>
                                                                            <td>
                                                                                <asp:Label Font-Size="XX-Small" runat="server">N° Posicion en la Cola</asp:Label>

                                                                            </td>
                                                                            <td>
                                                                                <asp:Label Font-Size="XX-Small" runat="server" Text=" " ID="lblPosicion"></asp:Label>

                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <asp:Label runat="server">Comentarios de la Revision :</asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <asp:TextBox ID="txtObsRevision" runat="server" Height="50px" TabIndex="18" TextMode="MultiLine" Width="515px"></asp:TextBox>

                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>Datos de Pago 
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <asp:TextBox ID="txtDatosPago1" runat="server" Height="90px" TabIndex="18" TextMode="MultiLine" Width="245px"></asp:TextBox>

                                                                    <asp:TextBox ID="txtDatosPago2" runat="server" Height="90px" TabIndex="18" TextMode="MultiLine" Width="245px"></asp:TextBox>

                                                                </td>
                                                            </tr>
                                                            <tr align="center">
                                                                <td>
                                                                    <asp:Button ID="btnActLiquidacion" runat="server" Text="Actualizar Liqidacion" Height="25px" CssClass="bootonClas" OnClick="btnActLiquidacion_Click" />

                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:Panel>



                                    </ContentTemplate>



                                </ajaxToolkit:TabPanel>



                            </ajaxToolkit:TabContainer>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label runat="server" Text="**Para ver el estado de la solicitud, seleccione la fila y haga doble clic**" ForeColor="Green" Font-Size="XX-Small"></asp:Label>
                        </td>
                    </tr>
                </table>
            </fieldset>




            <asp:HiddenField ID="vl_idDocOri" runat="server" />
            <asp:HiddenField ID="vl_idCliente" runat="server" />
            <asp:HiddenField ID="vl_idAgenciaAduana" runat="server" />
            <asp:HiddenField ID="vi_IdSolicitudAre" runat="server" />
            <asp:HiddenField ID="vi_idDespachador" runat="server" />
            <asp:HiddenField ID="vi_IdSolicitudAretiro" runat="server" />
            <asp:HiddenField ID="vi_nroNumeroDO" runat="server" />
            <asp:HiddenField ID="vi_nroVolante" runat="server" />
            <asp:HiddenField ID="vl_iTipoSolicitud" runat="server" />
            <asp:HiddenField ID="vl_idSituacion" runat="server" />
            <asp:HiddenField ID="HiddenField10" runat="server" />
        </div>
    </body>
    </html>


    <!--loading-->

    <asp:Panel ID="pnlLoading" runat="server" CssClass="PanelPopup" Width="350px" BorderWidth="1"
        BorderColor="#004b8e">
        <asp:UpdatePanel ID="UpdatePanel11" runat="server" style="width: 100px; padding-left: 1px; align-content: center;">
            <ContentTemplate>
                <table border="0" style="width: 350px; background-color: #EFEFF1" class="cuerpoComision">
                    <tr>
                        <td>

                            <%-- <asp:TextBox ID="txtMsjPop" runat="server" TextMode="MultiLine" Width="330px" TabIndex="18" Text="Cargando ...!!"></asp:TextBox>--%>
                            <asp:Label ID="txtMsjPop" runat="server" Text="Cargando ...!!"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table>
                                <tr>
                                    <td style="width: 120px;"></td>
                                    <td>
                                        <asp:Button ID="btnCerrarPop" runat="server" Text="Aceptar" Width="94px" Height="30px" CssClass="bootonClas" OnClientClick="return ClosePopup()" Visible="false" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>

    <ajaxToolkit:ModalPopupExtender ID="mpLoading" runat="server"
        CancelControlID="Button2" Enabled="True" PopupControlID="pnlLoading"
        TargetControlID="Button3">
    </ajaxToolkit:ModalPopupExtender>
    <asp:Button ID="Button2" Style="display: none;" runat="server" Text="[Open Proxy]" />
    <asp:Button ID="Button3" Style="display: none;" runat="server" Text="[Close Proxy]" />
    <!-- END -loading-->


    <!--Actualizar Liquidacion-->
    <asp:Panel ID="pnlLiquidacion" runat="server" CssClass="PanelPopup" Width="350px" BorderWidth="1"
        BorderColor="#004b8e">
        <asp:UpdatePanel ID="UpdatePanel1" runat="server" style="width: 400px; padding-left: 1px; align-content: center;">
            <ContentTemplate>
                <table border="0" style="width: 350px; background-color: #EFEFF1" class="cuerpoComision">
                    <tr>
                        <td>
                            <table>
                                <tr>
                                    <td>.</td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <table>
                                <tr>
                                    <th>
                                        <asp:Label ID="Label3" runat="server" Text="N° Liquidacion"></asp:Label></th>
                                    <th>:</th>
                                    <td align="left">
                                        <asp:Label ID="lblIdLiquidacion" runat="server" Text="0"></asp:Label></td>
                                </tr>
                                <tr>
                                    <th align="right">
                                        <asp:Label ID="Label4" runat="server" Text="Monto"></asp:Label></th>
                                    <th>:</th>
                                    <td align="left">
                                        <asp:Label ID="lblMonto" runat="server" Text="0"></asp:Label></td>
                                </tr>
                                <tr>
                                    <th>
                                        <asp:Label ID="Label6" runat="server" Text="Fecha Calculo"></asp:Label></th>
                                    <th>:</th>
                                    <td>
                                        <asp:Label ID="lblFecha" runat="server" Text="0"></asp:Label></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <table>
                                <tr>
                                    <td>
                                        <asp:Button ID="btnSoliRevLiq" runat="server" Text="Volver a Solicitar Revisión" Height="25px" CssClass="bootonClas" Font-Size="Small" OnClick="btnSoliRevLiq_Click" />
                                    </td>
                                    <td>
                                        <asp:Button ID="Button5" runat="server" Text="Cerrar" Height="25px" CssClass="bootonClas" Font-Size="Small" OnClientClick="return ClosePopupLiq();" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>

    <ajaxToolkit:ModalPopupExtender ID="mpLiquidacion" runat="server"
        CancelControlID="Button7" Enabled="True" PopupControlID="pnlLiquidacion"
        TargetControlID="Button8">
    </ajaxToolkit:ModalPopupExtender>
    <asp:Button ID="Button7" Style="display: none;" runat="server" Text="[Open Proxy]" />
    <asp:Button ID="Button8" Style="display: none;" runat="server" Text="[Close Proxy]" />
    <!--Actualizar Liquidacion-->



    <script language="javascript" type="text/javascript">


        var tiempo = 0;
        var seconds = 0;

        var myVar = setInterval(function () {
            clearInterval(myVar);
        }, 1000);

        var myTiempo = setTimeout(function () {
            clearInterval(myTiempo);
        }, 1);

        function myStopFunction() {
            clearInterval(myVar);
            seconds = '<%=GlobalEntity.Instancia.TiempoDuracionSesion%>' * 60;
            tiempo = '<%=GlobalEntity.Instancia.TiempoDuracionSesion%>' * 1000 * 60;

            CuentaTiempo(tiempo);
        }

        function CuentaTiempo(tiempo) {
            clearTimeout(myTiempo);
            myTiempo = setTimeout(function () {

                //window.location = "../Seguridad/eFENIX_Login.aspx";
            }, tiempo);
        }



        function SessionExpireAlert(timeout) {
            seconds = timeout / 1000;
            document.getElementsByName("seconds").innerHTML = seconds;

            myvar = setInterval(function () {
                seconds--;
                document.getElementById("seconds").innerHTML = seconds;
            }, 1000);

            CuentaTiempo(timeout);
        };



        var objFilaAnt = null;
        var backgroundColorFilaAnt = "";

        function fc_SeleccionaFilaSimpleUc(objFila, chkID, p1, p2, p3, p4, p5) {
            try {
                if (objFilaAnt != null) {
                    objFilaAnt.style.backgroundColor = backgroundColorFilaAnt;
                }
                objFilaAnt = objFila;
                backgroundColorFilaAnt = objFila.style.backgroundColor;
                objFila.style.backgroundColor = "#c4e4ff";

                document.getElementById('<%=vi_IdSolicitudAretiro.ClientID%>').value = p1;
                document.getElementById('<%=vi_nroNumeroDO.ClientID%>').value = p2;
                document.getElementById('<%=vi_nroVolante.ClientID%>').value = p3;
                document.getElementById('<%=vl_iTipoSolicitud.ClientID%>').value = p4;
                document.getElementById('<%=vl_idSituacion.ClientID%>').value = p5;
                f = document.forms[0].elements;
                for (x = 0; x < f.length; x++) {
                    obj = f[x];
                    if (obj.type == 'checkbox' && obj.id.replace("<%=GrvSolicitud.ClientID %>", "") != obj.id) {
                        if (obj.id != chkID)
                            obj.checked = false;
                    }
                }
                chkID.checked = true;
            }
            catch (e) {
                error = e.message;
            }
        }


        function SetActiveTab(tabControl, tabNumber) {
            OpenPopup();
            var ctrl = $find('<%=tbListaDetalle.ClientID%>');
            ctrl.set_activeTab(ctrl.get_tabs()[tabNumber]);
        }

        function SoloEnteros(e) {
            var tecla = document.all ? tecla = e.keyCode : tecla = e.which;
            return ((tecla > 47 && tecla < 58) || tecla == 44 || tecla == 46);
        }

        function fc_ImprimirPdf(sCodigo) {
            iwidth = 850;
            iHeight = 650;
            url = "AutorizacionRetiroPDF.aspx?id=" + sCodigo;
            window.open(url, 'AR', 'toolbar=no,left=0,top=0,width=' + iwidth + 'px,' + 'height=' + iHeight + 'px, directories=no, status=no, scrollbars=no, resizable=no, menubar=no');
            window.blur();
            return false;
        }

        function fc_Enter(e, sObjeto) {
            if (event.keyCode == 13) {
                if (sObjeto == 'txtIdSolicitud') {
                    document.getElementById("<%=btnBuscarSolicitud.ClientID%>").click();
                } else if (sObjeto == 'txtBlBusqueda') {
                    document.getElementById("<%=btnBuscarSolicitud.ClientID%>").click();
                } else if (sObjeto == 'txtClienteBusqueda') {
                    document.getElementById("<%=btnBuscarSolicitud.ClientID%>").click();
                }

                return false;
            }
        }


        function OpenPopup() {

            document.getElementById('<%=txtMsjPop.ClientID %>').innerHTML = 'Cargando...!!';
            document.getElementById("<%=Button3.ClientID%>").click();
            document.getElementById("<%=txtMsjPop.ClientID %>").style.color = "#0E0D0D";

        }
        function ClosePopup() {
            document.getElementById("<%=Button2.ClientID%>").click();
            document.getElementById('<%=txtMsjPop.ClientID %>').innerHTML = 'Cargando...!!';
            document.getElementById("<%=txtMsjPop.ClientID %>").style.color = "#0E0D0D";
        }



        function OpenPopupLiq() {
            document.getElementById("<%=Button8.ClientID%>").click();

        }
        function ClosePopupLiq() {
            document.getElementById("<%=Button7.ClientID%>").click();

        }



        function saveFileUpl(vi_uplNombre) {
            OpenPopup();

            if (vi_uplNombre == 'FileUploadBL') {
                document.getElementById("<%=imgBtnUplBL.ClientID%>").click();
            }
            if (vi_uplNombre == 'FileUploadEndoseLN') {
                document.getElementById("<%=imgBtnUplEndoseLN.ClientID%>").click();
            }
            if (vi_uplNombre == 'FileUploadSenasa') {
                document.getElementById("<%=imgBtnUplSenasa.ClientID%>").click();
            }
            if (vi_uplNombre == 'FileUploadMedioPago') {
                document.getElementById("<%=imgBtnUplMedioPago.ClientID%>").click();
            }
        }


    </script>


    <style type="text/css">
        #reservationsList {
            width: 500px;
            max-height: 600px;
            background: #fff;
            overflow: auto;
        }

        /*STRIPE LIST*/
        ul.stripedList {
            margin: 0;
            padding: 0;
            list-style: none;
        }

        .stripedList li {
            display: block;
            text-decoration: none;
            color: #333;
            font-family: Arial,Helvetica,sans-serif;
            font-size: 12px;
            line-height: 20px;
            height: 20px;
        }

            .stripedList li span {
                display: inline-block;
                border-right: 1px solid #ccc;
                overflow: hidden;
                text-overflow: ellipsis;
                padding: 0 10px;
                height: 20px;
            }

        .stripedList .evenRow {
            background: #f2f2f2;
        }

        .c1 {
            width: 55px;
        }

        .c2 {
            width: 70px;
        }

        .c3 {
            width: 130px;
        }

        .c4 {
            width: 15px;
        }

        .cLast {
            border: 0 !important;
        }

        .auto-style2 {
            width: 157px;
        }

        .auto-style3 {
            height: 26px;
        }

        .auto-style4 {
            width: 439px;
        }

        .bootonClas {
            background-color: #0069AE !important;
            border-color: #0069AE !important;
            color: #fff !important;
            font-size: 15px;
            font-family: Arial,Helvetica,sans-serif;
            font-weight: bold;
        }

        .form_titulo {
            border-width: 1px;
            border-style: solid;
            border-color: #C9C7C8;
            background-color: #0069ae; /* #005cab;*/
            padding: 4px;
            color: #E6E6E6;
            text-transform: uppercase;
            font-family: "Calibri";
            font-weight: bold;
        }
    </style>

</asp:Content>
