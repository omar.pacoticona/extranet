﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="AutorizacionRetiroPDF.aspx.cs" Inherits="Extranet_Documentacion_AutorizacionRetiroPDF" %>

<!DOCTYPE html>
<html>
<head>
    <title>Autorizacion Retiro</title>
    <style>
    </style>
    <script src="https://code.jquery.com/jquery-1.12.4.min.js" integrity="sha256-ZosEbRLbNQzLpnKIkEdrPv7lOy9C27hHQ+Xp8a4MxAQ=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.5/jspdf.min.js"> </script>
    <script src="../../Script/Java_Script/html2pdf.bundle.min.js"></script>
    <script type="text/javascript" src="../../Script/Java_Script/qrcode.js"></script>
    


</head>


<body>
    <button onclick="generatePDF()">Descargar</button>

    <form class="invoice" id="invoice" style="max-width: none; width: 785px; padding: 30px 0px 0px 40px;" runat="server">

        <table>
            <tr>
                <%--     <td style="height: 90px; width:  230px; background: url( '../../Imagenes/logo.jpg');">--%>
                <td>
                    <img src="../../Imagenes/logo.jpg" width="180" />
                </td>
                <td style="font-size: 18px;">
                    <table>
                        <tr>
                            <td>
                                <b>AUTORIZACION DE RETIRO N°   </b>
                            </td>
                            <td>
                                <asp:Label runat="server" ID="lblnroAutRet" Text="00000000000"></asp:Label>
                            </td>
                        </tr>
                        <tr style="font-size: xx-small">
                            <td>
                                User. Reg: <asp:Label runat="server" ID="lblUserReg" Font-Size="XX-Small" Text=""></asp:Label>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                User. Imp: <asp:Label runat="server" ID="lblUserImp" Font-Size="XX-Small" Text=""></asp:Label>
                            </td>
                            <td>
                                Fec. Reg: <asp:Label runat="server" ID="lblFechaImpr" Font-Size="XX-Small" Text=""></asp:Label>
                            </td>
                        </tr>
                    </table>

                </td>
                <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
                </td>
                <td>
                    <table>
                           <tr>
                            <td id="qrcode" style="pointer-events: none;">                            
                            </td>                           
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <table>
            <tr>
                <td>&nbsp;&nbsp;&nbsp;
                </td>
            </tr>
        </table>
        <table style="border: solid 1px #000000; width: 700px;">
            <tr>
                <td>
                    <b>Manifiesto:</b>
                </td>
                <td>
                    <asp:Label runat="server" ID="lblManifiesto" Text="2020-436"></asp:Label>
                </td>
                <td>
                    <b>Operacion:</b>
                </td>
                <td>
                    <asp:Label runat="server" ID="lblOperacion" Text="DESCARGA"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <b>Nav/Viaje/Rumbo:</b>
                </td>
                <td style="width: 320px;">
                    <asp:Label runat="server" ID="lblNavViajeRumbo" Text="CSCL EAST CHINA SEA -436-WB"></asp:Label>
                </td>
                <td>
                    <b>Fecha Descarga:</b>
                </td>
                <td>
                    <asp:Label runat="server" ID="lblFechaDescarga" Text="23/02/2020"></asp:Label>
                </td>
            </tr>
        </table>
        <br />
        <table style="border: solid 1px #000000; width: 700px;">
            <tr>
                <td>
                    <b>Consignatario:</b>
                </td>
                <td style="width: 300px;">
                    <asp:Label runat="server" ID="lblConsignatario" Font-Size="Smaller" Text=" "></asp:Label>
                </td>
                <td>
                    <b>Nro. DUA</b>
                </td>
                <td>
                    <asp:Label runat="server" ID="lblNumeroDua" Font-Size="Smaller" Text=""></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <b>Agencia Aduana</b>
                </td>
                <td>
                    <asp:Label runat="server" ID="lblAgencia" Font-Size="Smaller" Text=""></asp:Label>
                </td>
                <td>
                    <b>Nro. Volante</b>
                </td>
                <td>
                    <asp:Label runat="server" ID="lblVolante" Font-Size="Smaller" Text=""></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <b>Despachador:</b>
                </td>
                <td>
                    <asp:Label runat="server" ID="lblDespachador" Font-Size="Smaller" Text=" "></asp:Label>
                </td>
                <td>
                    <b>Fecha Vigencia</b>
                </td>
                <td>
                    <asp:Label runat="server" ID="lblFechaVigencia" Font-Size="Smaller" Text=""></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <b>Fecha de Emision:</b>
                </td>
                <td>
                    <asp:Label runat="server" ID="lblFechaEmision" Font-Size="Smaller" Text=""></asp:Label>
                </td>
                <td>
                    <b>Fecha Max. Vigencia</b>
                </td>
                <td>
                    <asp:Label runat="server" ID="LblFechaMaxVigenc" Font-Size="Smaller" Text=""></asp:Label>
                </td>
            </tr>
        </table>
        <br />
        <table>
            <asp:GridView ID="GrvListado" runat="server" HeaderStyle-BackColor="#0069ae" Style="width: 700px;" HeaderStyle-Height="40px" AutoGenerateColumns="False" >
                <Columns>
                    <asp:BoundField DataField="RepItem" HeaderText="Item" SortExpression="RepItem" >
                        <ItemStyle  horizontalalign="Center" Font-Size="Small" CssClass="border: solid 1px #0E0D0D;" />
                    </asp:BoundField>
                    <asp:BoundField DataField="RepNumeroDO" HeaderText="Numero DO" SortExpression="Numero DO" >
                         <ItemStyle  horizontalalign="Center" Font-Size="Small" />
                    </asp:BoundField>
                    <asp:BoundField DataField="RepCondicion" HeaderText="Cond" SortExpression="Cond" >
                         <ItemStyle  horizontalalign="Center" Font-Size="Small" />
                    </asp:BoundField>
                    <asp:BoundField DataField="RepChasisContenedor" HeaderText="Chasis/Contenedor" SortExpression="Chasis/Contenedor" >
                        <ItemStyle  horizontalalign="Center" Font-Size="Small" />
                    </asp:BoundField>
                    <asp:BoundField DataField="RepTipoContenedor" HeaderText="Tipo CNT" SortExpression="Tipo CNT" >
                        <ItemStyle  horizontalalign="Center" Font-Size="Small" />
                    </asp:BoundField>
                    <asp:BoundField DataField="RepTamano" HeaderText="Tño" SortExpression="Tño" >
                         <ItemStyle  horizontalalign="Center" Font-Size="Small" />
                    </asp:BoundField>
                    <asp:BoundField DataField="RepCantidad" HeaderText="Cant." SortExpression="Cant." >  
                        <ItemStyle  Width="2%" horizontalalign="Center" Font-Size="Small"  />
                    </asp:BoundField>
                    <asp:BoundField DataField="RepTara" HeaderText="Tara" SortExpression="Tara" >
                       <ItemStyle  horizontalalign="Center" Font-Size="Small" />
                    </asp:BoundField>
                    <asp:BoundField DataField="RepPesoNeto" HeaderText="Peso Neto" SortExpression="Peso Neto" >
                       <ItemStyle  horizontalalign="Center" Font-Size="Small" />
                    </asp:BoundField>
                    <asp:BoundField DataField="RepCarga" HeaderText="Carga">
                          <ItemStyle Width="10%" Font-Size="XX-Small" horizontalalign="Center" />
                     </asp:BoundField>
                    <asp:BoundField DataField="RepReferencia" HeaderText="Referecia" SortExpression="Referecia" >
                          <ItemStyle Font-Size="Small" horizontalalign="Center" />
                     </asp:BoundField>
                </Columns>
            </asp:GridView>

        </table>
        <br />
        <table>
            <tr>
                <td style="width: 350px;">&nbsp;</td>
                <td>
                    <table>
                        <tr>
                            <td>
                                <table>
                                    <tr>
                                        <td>TOTALES: 
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td>
                                <table border cellpadding="2" cellspacing="0">
                                    <tr>
                                        <td>Cantidad
                                        </td>
                                        <td>Peso Neto
                                        </td>
                                        <td>Peso Bruto
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Label runat="server" ID="lblCantidad" Font-Size="Smaller" Text=" "></asp:Label>
                                        </td>
                                        <td>
                                            <asp:Label runat="server" ID="lblPesoNeto" Font-Size="Smaller" Text=" "></asp:Label>
                                        </td>
                                        <td>
                                            <asp:Label runat="server" ID="lblPesoBruto" Font-Size="Smaller" Text=" "></asp:Label>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>

        <div id="qrcode1" style="pointer-events: none;padding:40px 0px 0px 242px;""></div>
    </form>


</body>

</html>
<script>
    window.onload = function () {
        var userInput = document.getElementById('lblnroAutRet').innerHTML;

        var qrvalor = btoa(userInput);

        var qrcode = new QRCode("qrcode", {
            text: qrvalor,
            width: 100,
            height: 100,
            colorDark: "black",//"#0069ae",
            colorLight: "white",
            correctLevel: QRCode.CorrectLevel.H
        });
        var qrcode1 = new QRCode("qrcode1", {
            text: qrvalor,
            width: 200,
            height: 200,
            colorDark: "black",
            colorLight: "white",
            correctLevel: QRCode.CorrectLevel.H
        });
        myStopFunction();
    }

    function generatePDF() {
        // Choose the element that our invoice is rendered in.
        const element = document.getElementById("invoice");
        // Choose the element and save the PDF for our user.
        html2pdf()
            .set({ html2canvas: { scale: 4 } })
            .from(element)
            .save();
        myStopFunction();
    }


    var tiempo = 0;
    var seconds = 0;

    var myVar = setInterval(function () {
        clearInterval(myVar);
    }, 1000);

    var myTiempo = setTimeout(function () {
        clearInterval(myTiempo);
    }, 1);

    function myStopFunction() {
        clearInterval(myVar);
        seconds = '<%=GlobalEntity.Instancia.TiempoDuracionSesion%>' * 60;
        tiempo = '<%=GlobalEntity.Instancia.TiempoDuracionSesion%>' * 1000 * 60;

        CuentaTiempo(tiempo);
    }

    function CuentaTiempo(tiempo) {
        clearTimeout(myTiempo);
        myTiempo = setTimeout(function () {
            // window.location.close();//= "../Seguridad/eFENIX_Login.aspx";
            window.close();
        }, tiempo);
    }



    function SessionExpireAlert(timeout) {
        seconds = timeout / 1000;
        //document.getElementsByName("seconds").innerHTML = seconds;

        myvar = setInterval(function () {
            seconds--;
            //  document.getElementById("seconds").innerHTML = seconds;
        }, 1000);

        CuentaTiempo(timeout);
    };

    function inhabilitar() {
        alert("Esta función está inhabilitada.\n\nPerdone las molestias");
        return false;
    }

    document.oncontextmenu = inhabilitar;

</script>


