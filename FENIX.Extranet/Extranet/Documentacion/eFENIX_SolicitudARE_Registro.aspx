﻿<%@ Page Language="C#"
    MasterPageFile="~/Extranet/Seguridad/eFENIX_Principal.master"
    AutoEventWireup="true"
    CodeFile="eFENIX_SolicitudARE_Registro.aspx.cs"
    Title="ARE-Autorizacion de Retiro Electronico"
    Inherits="Documentacion.eFENIX_AutorizacionRetiro" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title></title>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js" type="text/javascript"></script>
    <script src="../../Script/Java_Script/jsSolicitudARE.js"></script>

    <link href="../../Script/Hojas_Estilo/Preload1.css" rel="stylesheet" />



</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentCab" runat="Server">
    <span class="texto_titulo">Tu Sesion Expirara en &nbsp; 
        <span class="texto_titulo" id="seconds"></span>
        <span>&nbsp;seg.</span>
    </span>
    <asp:Button ID="btn_cerrar" runat="server" Text="cerrar"
        OnClick="btn_cerrar_Click" Style="display: none;" />

</asp:Content>





<asp:Content ID="Content2" ContentPlaceHolderID="CntMaster" runat="Server">
    <!DOCTYPE html>

    <html>
    <body>




        <div class="form_titulo" style="width: 100%; height: 23px; vertical-align: inherit">
            <p style="margin-top: 1.5px; font-size: 14px;">SOLICITUD DE AUTORIZACIÓN DE RETIRO ELECTRÓNICA (ARE)</p>
        </div>
        <div id="tabs-container">
            <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0" OnActiveTabChanged="TabContainer1_ActiveTabChanged" AutoPostBack="True"
                Width="100%">

                <ajaxToolkit:TabPanel ID="tbPanelBusqueda" runat="server" HeaderText="Paso N°1">
                    <ContentTemplate>
                        <asp:Panel ID="Panessl4" runat="server" BackColor="White" Height="470px" ScrollBars="Vertical" Width="99%">
                            <fieldset style="width: 98%; background-color: white;">
                                <legend style="background-color: #0069AE; font-weight: 700;">Datos de la Busqueda</legend>
                                <div>
                                    <table class="form_bandeja" style="margin-top: 5px; margin-bottom: 5px; width: 100%;">
                                        <tr>
                                            <td colspan="3">
                                                <asp:TextBox ID="txtCodAlmacenDA" runat="server" Style="text-transform: uppercase" type="number" Width="33px" placeholder="118" ReadOnly="True">118</asp:TextBox>

                                                <asp:DropDownList ID="dplAnio" runat="server" Width="70px">
                                                    <asp:ListItem Value="2020">2020</asp:ListItem>
                                                    <asp:ListItem Value="2021">2021</asp:ListItem>
                                                </asp:DropDownList>

                                                <asp:DropDownList ID="DplRegimen" runat="server" Width="180px"></asp:DropDownList>

                                                <asp:TextBox ID="txtDua" runat="server" Style="text-transform: uppercase" type="number" Width="100px" placeholder="Número Dua"></asp:TextBox>
                                            </td>
                                            <td>
                                                <asp:Label runat="server">BL/Volante</asp:Label>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="TxtDocumentoM" runat="server" Style="text-transform: uppercase" Width="135px" placeholder="BL o Volante"></asp:TextBox>
                                            </td>
                                            <td>
                                                <asp:ImageButton ID="BtnConsultaDo" runat="server" ImageUrl="~/Imagenes/buscar.png" OnClick="BtnConsultaDo_Click" OnClientClick="return OpenPopup();" />

                                            </td>
                                            <td>
                                                <asp:Button ID="btnNuevaSolicitud" runat="server" Text="Nueva Solicitud" Height="30px" CssClass="bootonClas" OnClick="btnNuevaSolicitud_Click" />
                                                <asp:Button ID="btnRenovacion" runat="server" Text="Solicitar Renovacion" Height="30px" CssClass="bootonClas" OnClick="btnRenovacion_Click" />
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </fieldset>
                            <br />
                            <fieldset style="width: 98%; background-color: white;">
                                <legend style="background-color: #0069AE; font-weight: 700;">Resultado Busqueda</legend>

                                <table>
                                    <tr>
                                        <td>
                                            <table>
                                                <tr>
                                                    <td>
                                                        <table>
                                                            <tr style="height: 50%; width: 99%;" valign="top">
                                                                <td colspan="3">
                                                                    <asp:Label runat="server">CLIENTE:</asp:Label>
                                                                </td>
                                                                <td colspan="3">
                                                                    <asp:TextBox ID="txtCliente" runat="server" ReadOnly="True" Style="text-transform: uppercase" Width="235px"></asp:TextBox>
                                                                </td>
                                                                <td>&nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; </td>
                                                                <td colspan="3">
                                                                    <asp:Label runat="server">AGENCIA DE ADUANA</asp:Label>
                                                                </td>
                                                                <td colspan="3">
                                                                    <asp:TextBox ID="txtAgenciaAduana" runat="server" ReadOnly="True" Style="text-transform: uppercase" Width="235px"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="3">
                                                                    <asp:Label runat="server">LÍNEA</asp:Label>
                                                                </td>
                                                                <td colspan="3">
                                                                    <asp:TextBox ID="txtLinea" runat="server" ReadOnly="True" Style="text-transform: uppercase" Width="235px"></asp:TextBox>
                                                                </td>
                                                                <td></td>
                                                                <td colspan="3">
                                                                    <asp:Label runat="server">FECHA Levante</asp:Label>
                                                                </td>
                                                                <td colspan="3">
                                                                    <asp:TextBox ID="txtResultadoLevante" runat="server" ReadOnly="True" Style="text-transform: uppercase" Width="235px"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="3">
                                                                    <asp:Label runat="server">N° LIQUIDACIÓN</asp:Label>
                                                                </td>
                                                                <td colspan="3">
                                                                    <asp:TextBox ID="txtLiquidacion" runat="server" ReadOnly="True" Style="text-transform: uppercase" Width="235px"></asp:TextBox>
                                                                </td>
                                                                <td></td>
                                                                <td colspan="3">
                                                                    <asp:Label runat="server">MONTO LIQUIDACIÓN</asp:Label>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txtMontoLiquidacion" runat="server" ReadOnly="True" Style="text-transform: uppercase" Width="235px"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <table>
                                                            <tr style="height: 50%; width: 1000px;" valign="top">
                                                                <td colspan="4">
                                                                    <asp:UpdatePanel runat="server" UpdateMode="Conditional">
                                                                        <ContentTemplate>
                                                                            <asp:Panel ID="Panel1" runat="server" BackColor="White" Height="100px" ScrollBars="Vertical" Width="780px">
                                                                                <asp:GridView ID="grvListadoSolicitud" runat="server" AutoGenerateColumns="False" DataKeyNames="iIdDocOriDet" SkinID="GrillaConsulta" Width="100%">
                                                                                    <Columns>
                                                                                        <asp:BoundField DataField="sNumeroDO" HeaderText="Documento" />
                                                                                        <asp:BoundField DataField="iItem" HeaderText="Item" />
                                                                                        <asp:BoundField DataField="sContenedor" HeaderText="CTN/CHS/CS" />
                                                                                        <asp:BoundField DataField="sTamanoCnt" HeaderText="Tamaño" />
                                                                                        <asp:BoundField DataField="sTipoCnt" HeaderText="Tipo" />
                                                                                        <asp:BoundField DataField="sCondicionCarga" HeaderText="Condicion" />
                                                                                    </Columns>
                                                                                </asp:GridView>
                                                                            </asp:Panel>
                                                                        </ContentTemplate>
                                                                    </asp:UpdatePanel>
                                                                </td>
                                                                <td></td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <asp:Label ID="rsptSunat" runat="server" Text=" " Font-Size="XX-Small" Font-Italic="True"></asp:Label>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td>
                                            <table style="text-align: center;" runat="server" id="tbAgregar" visible="False">
                                                <tr runat="server">
                                                    <td style="width: 80px" runat="server">&nbsp; </td>
                                                    <td runat="server">
                                                        <asp:ImageButton ID="imgBtnAgregarGrilla" runat="server" ImageUrl="~/Imagenes/mas.png" ToolTip="AGREGAR" OnClick="imgBtnAgregarGrilla_Click" />
                                                    </td>
                                                </tr>
                                                <tr runat="server">
                                                    <td runat="server">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </td>
                                                    <td runat="server">&nbsp;
                                                        <asp:Label ID="lblNombreIconos" runat="server" Text="Agregar"></asp:Label></td>
                                                </tr>
                                            </table>
                                            <table style="text-align: center" id="tbSiguiente" runat="server">
                                                <tr runat="server">
                                                    <td style="width: 80px" runat="server">&nbsp; </td>
                                                    <td runat="server">
                                                        <asp:ImageButton ID="btnSiguiente2" runat="server" ImageUrl="~/Imagenes/siguiente.png" ToolTip="Siguiente" OnClick="btnSiguiente_Click" />
                                                    </td>
                                                </tr>
                                                <tr runat="server">
                                                    <td runat="server">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </td>
                                                    <td runat="server">&nbsp;
                                                        <asp:Label ID="Label2" runat="server" Text="Siguiente"></asp:Label></td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>

                                </table>

                            </fieldset>
                            <br />
                            Es Ligado?
                            <asp:CheckBox runat="server" ID="chkLigado" AutoPostBack="True" OnCheckedChanged="chkLigado_OnCheckedChanged" />
                            <br />
                            <br />
                            <fieldset style="width: 98%; background-color: white;" id="fildsetDatosA" runat="server" visible="False">
                                <legend style="background-color: #0069AE; font-weight: 700;">DATOS AGREGADOS</legend>
                                <table>
                                    <tr>
                                        <td colspan="4">
                                            <asp:UpdatePanel runat="server" UpdateMode="Conditional">
                                                <ContentTemplate>
                                                    <asp:Panel ID="Panel2" runat="server" BackColor="White" Height="100px" ScrollBars="Vertical" Width="780px">
                                                        <asp:GridView ID="grvListadoAdd" runat="server" AutoGenerateColumns="False" DataKeyNames="iIdDocOri,idliquidacion,iIdCliente,sDua,sNumeroDO,sVolante,sMonto,sCliente,iItem,idAduana,sDuaAnio,sRegimen,sDuaNumero" SkinID="GrillaConsulta" Width="100%"
                                                            OnRowCommand="grvListadoAdd_RowCommand">
                                                            <Columns>
                                                                <asp:TemplateField>
                                                                    <ItemTemplate>
                                                                        <input id="chkSeleccionar" type="checkbox" />
                                                                    </ItemTemplate>
                                                                    <HeaderStyle BackColor="#0069ae" Width="20px" />
                                                                    <ItemStyle Width="20px" />
                                                                </asp:TemplateField>
                                                                <asp:BoundField DataField="iItem" HeaderText="N°" />
                                                                <asp:BoundField DataField="sNumeroDO" HeaderText="Documento BL" />
                                                                <asp:BoundField DataField="sVolante" HeaderText="VOLANTE" />
                                                                <asp:BoundField DataField="sCliente" HeaderText="CLIENTE" />
                                                                <asp:BoundField DataField="sDua" HeaderText="DUA/DAM" />
                                                                <asp:BoundField DataField="idliquidacion" HeaderText="LIQUIDACION" />
                                                                <asp:BoundField DataField="sMonto" HeaderText="MONTO" />
                                                                <asp:TemplateField ShowHeader="False">
                                                                    <ItemTemplate>
                                                                        <asp:ImageButton ID="Image" runat="server" CausesValidation="false" Visible="true" CommandArgument='<%# Eval("iItem") %>'
                                                                            CommandName="Eliminar" ImageUrl="~/Script/Imagenes/i.p.delete.gif" ToolTip="Quitar" />
                                                                    </ItemTemplate>
                                                                    <ItemStyle Width="1%" HorizontalAlign="Center" />
                                                                </asp:TemplateField>
                                                            </Columns>
                                                        </asp:GridView>
                                                    </asp:Panel>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </td>
                                        <td>
                                            <table>
                                                <tr>
                                                    <td style="width: 80px">&nbsp;
                                                    </td>
                                                    <td>
                                                        <asp:ImageButton ID="btnSiguiente" runat="server" ImageUrl="~/Imagenes/siguiente.png" OnClick="btnSiguiente_Click" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td></td>
                                                    <td>SIGUIENTE
                                                    </td>
                                                </tr>
                                            </table>


                                        </td>
                                    </tr>
                                </table>
                            </fieldset>
                        </asp:Panel>
                    </ContentTemplate>



                </ajaxToolkit:TabPanel>

                <ajaxToolkit:TabPanel ID="tbPanelAdj" runat="server" HeaderText="PASO N°2">
                    <ContentTemplate>
                        <asp:Panel ID="Panel4" runat="server" BackColor="White" Height="470px" ScrollBars="Vertical" Width="99%">
                            <fieldset style="background-color: white;" class="auto-style9">
                                <legend style="background-color: #0069AE; font-weight: 700;">Datos de la Busqueda</legend>
                                <div id="divBusqueda">
                                    <table class="form_bandeja" style="margin-top: 5px; margin-bottom: 5px; width: 100%;">

                                        <tr>
                                            <td class="auto-style2">
                                                <asp:Label runat="server">BL</asp:Label>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtBlAdj" runat="server" Style="text-transform: uppercase" ReadOnly="True"
                                                    Width="235px"></asp:TextBox>
                                            </td>
                                            <td class="auto-style2">
                                                <asp:Label runat="server">Fecha levante</asp:Label>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtFechaLevante" runat="server" Style="text-transform: uppercase" Width="235px" ReadOnly="True"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="auto-style2">
                                                <asp:Label runat="server">CLIENTE:</asp:Label>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtClienteAdj" ReadOnly="True" runat="server" Style="text-transform: uppercase" Width="235px"></asp:TextBox>

                                            </td>
                                            <td class="auto-style2">
                                                <asp:Label runat="server">AGENCIA DE ADUANA</asp:Label>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtAgenciAdj" ReadOnly="True" runat="server" Style="text-transform: uppercase" Width="235px"></asp:TextBox>

                                            </td>
                                            <td>
                                                <asp:Button ID="btnGuardarAdj" runat="server" Text="Guardar" Width="94px" Height="30px" CssClass="bootonClas" OnClick="btnGuardarAdj_Click" />
                                            </td>
                                        </tr>

                                    </table>

                                </div>

                            </fieldset>

                            <fieldset style="width: 99%; background-color: white;">
                                <legend style="background-color: #0069AE; font-weight: 700;">Adjuntar documentos</legend>
                                <table>
                                    <tr valign="top" style="height: 50%; width: 1000px;">
                                        <td colspan="4">
                                            <asp:UpdatePanel runat="server" UpdateMode="Conditional">
                                                <ContentTemplate>
                                                    <asp:Panel ID="Panel5" runat="server" BackColor="White" Height="150px" ScrollBars="Vertical" Width="1000px">
                                                        <table border="1">
                                                            <tr style="color: ivory; background-color: #0069AE">
                                                                <%--<th>id</th>--%>
                                                                <th>Documento</th>
                                                                <th>Adjuntar</th>
                                                                <th>Accion</th>
                                                                <th>Nombre Archivo</th>
                                                            </tr>
                                                            <tr>
                                                                <%--   <td> </td>--%>
                                                                <asp:Label runat="server" ID="lblUplBLID" Text="0" Visible="false"></asp:Label>

                                                                <td>BL
                                                                </td>
                                                                <td>
                                                                    <asp:FileUpload ID="FileUploadBL" runat="server" Width="320px" Enabled="false" accept="application/pdf" onchange="saveFileUpl('FileUploadBL');" />
                                                                </td>
                                                                <td align="center">
                                                                    <asp:ImageButton runat="server" ID="imgBtnUplBL" ImageUrl="~/Imagenes/save.png" OnClick="imgBtnUplBL_Click" />
                                                                    <asp:ImageButton runat="server" ID="imgBtnUplBLupd" ImageUrl="~/Imagenes/cambiar.png" OnClick="imgBtnUplBLupd_Click" Visible="false" />
                                                                </td>
                                                                <td>
                                                                    <asp:Label runat="server" ID="lblUplBL" Text=" "></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <%--   <td>  </td>--%>
                                                                <asp:Label runat="server" ID="lblUplEndoseLNID" Text="0" Visible="false"></asp:Label>

                                                                <td>Carta de Liberacion</td>
                                                                <td>
                                                                    <asp:FileUpload ID="FileUploadEndoseLN" runat="server" Width="320px" Enabled="false" accept="application/pdf" onchange="saveFileUpl('FileUploadEndoseLN');" />
                                                                </td>
                                                                <td align="center">
                                                                    <asp:ImageButton runat="server" ID="imgBtnUplEndoseLN" ImageUrl="~/Imagenes/save.png" OnClick="imgBtnUplEndoseLN_Click" Enabled="true" />
                                                                    <asp:ImageButton runat="server" ID="imgBtnUplEndoseLNupd" ImageUrl="~/Imagenes/cambiar.png" OnClick="imgBtnUplEndoseLNupd_Click" Visible="false" />
                                                                </td>
                                                                <td>
                                                                    <asp:Label runat="server" ID="lblUplEndoseLN" Text=" "></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <%-- <td> </td>--%>
                                                                <asp:Label runat="server" ID="lblUplSenasaID" Text="0" Visible="false"></asp:Label>

                                                                <td>Senasa</td>
                                                                <td>
                                                                    <asp:FileUpload ID="FileUploadSenasa" runat="server" Width="320px" Enabled="false" accept="application/pdf" onchange="saveFileUpl('FileUploadSenasa');" />
                                                                </td>
                                                                <td align="center">
                                                                    <asp:ImageButton runat="server" ID="imgBtnUplSenasa" ImageUrl="~/Imagenes/save.png" OnClick="imgBtnUplSenasa_Click" Enabled="true" />
                                                                    <asp:ImageButton runat="server" ID="imgBtnUplSenasaupd" ImageUrl="~/Imagenes/cambiar.png" OnClick="imgBtnUplSenasaupd_Click" Visible="false" />
                                                                </td>
                                                                <td>
                                                                    <asp:Label runat="server" ID="lblUplSenasa" Text=" "></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <%-- <td> </td>--%>
                                                                <asp:Label runat="server" ID="lblUplMedioPagoID" Text="0" Visible="false"></asp:Label>

                                                                <td>Medio de Pago</td>
                                                                <td>
                                                                    <asp:FileUpload ID="FileUploadMedioPago" runat="server" Width="320px" Enabled="false" accept="application/pdf" onchange="saveFileUpl('FileUploadMedioPago');" />
                                                                </td>
                                                                <td align="center">
                                                                    <asp:ImageButton runat="server" ID="imgBtnUplMedioPago" ImageUrl="~/Imagenes/save.png" OnClick="imgBtnUplMedioPago_Click" Enabled="true" />
                                                                    <asp:ImageButton runat="server" ID="imgBtnUplMedioPagoupd" ImageUrl="~/Imagenes/cambiar.png" OnClick="imgBtnUplMedioPagoupd_Click" Visible="false" />
                                                                </td>
                                                                <td>
                                                                    <asp:Label runat="server" ID="lblUplMedioPago" Text=" "></asp:Label>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </asp:Panel>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <table border="0">
                                                <tr>
                                                    <td>
                                                        <asp:Label runat="server">&nbsp;&nbsp;RUC a FACTURAR</asp:Label>
                                                    </td>
                                                    <th>
                                                        <asp:TextBox ID="txtRucFac" runat="server" Style="text-transform: uppercase" Width="100px" MaxLength="11" type="number"></asp:TextBox>
                                                    </th>
                                                    <th>&nbsp;
                                                         <asp:ImageButton ID="btnRucFac" runat="server" ImageUrl="~/Imagenes/buscar.png" OnClick="btnRucFac_Click" />
                                                    </th>
                                                    <th class="auto-style4">
                                                        <asp:Label runat="server" Text=" " ID="lblRucFacturar"></asp:Label>
                                                    </th>
                                                </tr>
                                                <tr>
                                                    <th>
                                                        <asp:Label runat="server">&nbsp;&nbsp;DNI DESPACHADOR</asp:Label>
                                                    </th>
                                                    <th>
                                                        <asp:TextBox ID="txtDniDespachador" runat="server" Style="text-transform: uppercase" Width="100px"></asp:TextBox>
                                                    </th>
                                                    <th>&nbsp;
                                                        <asp:ImageButton ID="btnDespachador" runat="server" ImageUrl="~/Imagenes/buscar.png" OnClick="btnDespachador_Click" />
                                                    </th>
                                                    <th class="auto-style4">
                                                        <asp:Label runat="server" Text=" " ID="lblDespachador"></asp:Label>
                                                        <div id="loading"></div>
                                                    </th>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </fieldset>
                            <br />
                            <fieldset style="width: 99%; background-color: white;">
                                <legend style="background-color: #0069AE; font-weight: 700;">Datos de Pago</legend>
                                <table>
                                    <tr>
                                        <td>
                                            <table border="0">
                                                <tr>
                                                    <th align="right">
                                                        <asp:Label runat="server">TIPO DE PAGO</asp:Label>
                                                    </th>
                                                    <th align="left">
                                                        <asp:DropDownList ID="DplTipoCobro" runat="server" Width="230px" OnSelectedIndexChanged="DplTipoCobro_SelectedIndexChanged" AutoPostBack="True">
                                                        </asp:DropDownList>
                                                    </th>
                                                    <th>
                                                        <asp:Label ID="lblMontoPago" runat="server">Monto</asp:Label></th>
                                                    <th>
                                                        <asp:TextBox ID="txtMontoPago" runat="server" Style="text-transform: uppercase" Width="100px" type="number" step="any"></asp:TextBox></th>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Label runat="server" ID="lblFormaPago">Forma de pago</asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:DropDownList ID="DplFormaPago" runat="server" Width="230px">
                                                            <asp:ListItem Value="-1">[Seleccionar]</asp:ListItem>
                                                            <asp:ListItem Value="3">DEPOSITO EN CUENTA</asp:ListItem>
                                                            <asp:ListItem Value="8">TRANSFERENCIA</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </td>
                                                    <td>
                                                        <asp:Label runat="server" ID="lblNoperacion">&nbsp;&nbsp; N° Op.</asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="txtNumeroOperacion" runat="server" Style="text-transform: uppercase" Width="100px"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="right">
                                                        <asp:Label runat="server" ID="lblBanco">Banco</asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:DropDownList ID="DplBanco" runat="server" Width="230px"></asp:DropDownList>
                                                    </td>
                                                    <td>
                                                        <asp:Label runat="server" ID="lblMoneda">&nbsp;&nbsp;Moneda</asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:DropDownList ID="DplMonedaPago" runat="server" Width="112px">
                                                            <asp:ListItem Value="-1">[Seleccionar]</asp:ListItem>
                                                            <asp:ListItem Value="23">SOLES</asp:ListItem>
                                                            <asp:ListItem Value="24">US Dolar</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 520px;" align="justify">
                                            <asp:Label runat="server" ID="lblComentarios">Comentarios/OBS</asp:Label>
                                            <asp:TextBox ID="txtObservacion" runat="server" Height="100%" TextMode="MultiLine" Width="400px" TabIndex="12"></asp:TextBox>
                                        </td>
                                        <td></td>
                                    </tr>
                                </table>
                            </fieldset>

                        </asp:Panel>
                    </ContentTemplate>
                </ajaxToolkit:TabPanel>

                <ajaxToolkit:TabPanel ID="tbRenovacion" runat="server" HeaderText="RENOVACION">
                    <ContentTemplate>
                        <asp:Panel ID="Panel3" runat="server" BackColor="White" Height="470px" ScrollBars="Vertical" Width="99%">

                            <table border="0" style="background-color: #EFEFF1" class="auto-style10">
                                <tr>
                                    <td>
                                        <table border="0" style="width: 99%; height: 50px; background-color: #EFEFF1;" class="cuerpoComision">
                                            <tr style="background-color: #0069AE; color: #FFFFFF">
                                                <td colspan="4" align="center" style="height: 21px">
                                                    <strong>SOLICITUD DE RENOVACION</strong>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 100px">AUT. RETIRO
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="TxtNumeroPO" runat="server" Width="110px"></asp:TextBox>
                                                    <asp:ImageButton ID="imbBtnBuscarAutRet" runat="server" ImageUrl="~/Imagenes/buscar.png" OnClick="imbBtnBuscarAutRet_Click"  OnClientClick="return OpenPopup();" />
                                                    <asp:TextBox ID="TxtIdAutRet" Style="display: none" runat="server" MaxLength="10" Width="24px"></asp:TextBox>
                                                    <asp:Label runat="server" ID="lblAutRet" Text="0"></asp:Label>
                                                </td>
                                                <td>F. CREACION
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtFechaCreacion" runat="server" MaxLength="10" Width="110px"
                                                        Enabled="False"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 80px">SITUACION
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="TxtSituacionPO" runat="server" Enabled="False" Width="110px"></asp:TextBox>

                                                </td>

                                                <td>F.ACTUAL HABILITACION</td>
                                                <td>
                                                    <asp:TextBox ID="txtFechaHabilitado" runat="server" MaxLength="10" Width="110px"
                                                        Enabled="False"></asp:TextBox>

                                                </td>
                                            </tr>
                                            <tr>
                                                <td>ESTADO
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="TxtEstadoVigenciaPO" runat="server" Width="110px" Enabled="False"></asp:TextBox>
                                                </td>
                                                <td>F.MAX VIGENCIA</td>
                                                <td>
                                                    <asp:TextBox ID="TxtFechaVigenciaMaximaPop" runat="server" MaxLength="10" Width="110px"
                                                        Enabled="False"></asp:TextBox>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table border="0" style="width: 700px; height: 50px; background-color: #EFEFF1;" class="cuerpoComision">
                                            <tr>

                                                <td style="width: 120px">MEDIO DE PAGO</td>
                                                <td style="width: 220px">
                                                    <asp:FileUpload ID="FileUploadMedioPagoR" runat="server" Width="380px" accept="application/pdf" />
                                                </td>
                                                <td style="width: 50px" align="center">
                                                </td>
                                                <td>
                                                    <asp:Label runat="server" ID="lblNombreArchivoR" Text=" "></asp:Label>

                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>

                            <fieldset style="width: 95%; background-color: white;" runat="server" id="fielPagoRenovacion">
                                <legend style="background-color: #0069AE; font-weight: 700;">Datos de Pago</legend>
                                <table width="99%;">
                                    <tr>
                                        <td>
                                            <table border="0">
                                                <tr>
                                                    <th align="right">
                                                        <asp:Label runat="server" Width="230px">TIPO DE PAGO</asp:Label>
                                                    </th>
                                                    <th align="left">
                                                        <asp:DropDownList ID="DplTipoCobroR" runat="server" Width="230px" OnSelectedIndexChanged="DplTipoCobroR_SelectedIndexChanged" AutoPostBack="True">
                                                        </asp:DropDownList>
                                                    </th>
                                                    <th>
                                                        <asp:Label ID="lblMontoPagoR" runat="server">Monto</asp:Label></th>
                                                    <th>
                                                        <asp:TextBox ID="txtMontoPagoR" runat="server" Style="text-transform: uppercase" Width="230px" type="number" step="any"></asp:TextBox></th>
                                                </tr>
                                                <tr align="right">
                                                    <td>
                                                        <asp:Label runat="server" ID="lblFormaPagoR">Forma de pago</asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:DropDownList ID="DplFormaPagoR" runat="server" Width="230px">
                                                            <asp:ListItem Value="-1">[Seleccionar]</asp:ListItem>
                                                            <asp:ListItem Value="3">DEPOSITO EN CUENTA</asp:ListItem>
                                                            <asp:ListItem Value="8">TRANSFERENCIA</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </td>
                                                    <td>
                                                        <asp:Label runat="server" ID="lblNoperacionR">&nbsp;&nbsp; N° Op.</asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="txtNumeroOperacionR" runat="server" Style="text-transform: uppercase" Width="230px"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="right">
                                                        <asp:Label runat="server" ID="lblBancoR">Banco</asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:DropDownList ID="DplBancoR" runat="server" Width="230px"></asp:DropDownList>
                                                    </td>
                                                    <td>
                                                        <asp:Label runat="server" ID="lblMonedaR">&nbsp;&nbsp;Moneda</asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:DropDownList ID="DplMonedaPagoR" runat="server" Width="245px">
                                                            <asp:ListItem Value="-1">[Seleccionar]</asp:ListItem>
                                                            <asp:ListItem Value="23">SOLES</asp:ListItem>
                                                            <asp:ListItem Value="24">US Dolar</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                            <td align="center">
                                        <asp:Button ID="btnSolicitarRenovacion" runat="server" Text="SOLICITAR" Height="30px" CssClass="bootonClas" OnClick="btnSolicitarRenovacion_Click" />
                                    </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 820px;" align="center">
                                            <asp:Label runat="server" ID="Label8">Comentarios/OBS</asp:Label>
                                            <asp:TextBox ID="txtObservacionR" runat="server" Height="100%" TextMode="MultiLine" Width="760px" TabIndex="12"></asp:TextBox>
                                        </td>
                                        <td></td>
                                    </tr>
                                </table>
                            </fieldset>
                        </asp:Panel>
                    </ContentTemplate>
                </ajaxToolkit:TabPanel>


            </ajaxToolkit:TabContainer>
            <asp:HiddenField ID="vl_idDocOri" runat="server" />
            <asp:HiddenField ID="vl_idCliente" runat="server" />
            <asp:HiddenField ID="vl_idAgenciaAduana" runat="server" />
            <asp:HiddenField ID="vi_IdSolicitudAre" runat="server" />
            <asp:HiddenField ID="vi_idDespachador" runat="server" />
            <asp:HiddenField ID="vi_IdSolicitudAretiro" runat="server" />
            <asp:HiddenField ID="vi_nroNumeroDO" runat="server" />
            <asp:HiddenField ID="vi_nroVolante" runat="server" />
            <asp:HiddenField ID="vi_idCliente" runat="server" />
            <asp:HiddenField ID="vl_sDua" runat="server" />
            <asp:HiddenField ID="vl_sDocOrigen" runat="server" />
            <asp:HiddenField ID="vl_anio" runat="server" />
            <asp:HiddenField ID="vl_regimen" runat="server" />
            <asp:HiddenField ID="vl_dua" runat="server" />
            <asp:HiddenField ID="vl_idAduana" runat="server" />
            <asp:HiddenField ID="vl_sRuta" runat="server" />
        </div>

    </body>
    </html>




    <!--loading-->

    <asp:Panel ID="pnlLoading" runat="server" CssClass="PanelPopup" Width="350px" BorderWidth="1"
        BorderColor="#004b8e">
        <asp:UpdatePanel ID="UpdatePanel11" runat="server" style="width: 100px; padding-left: 1px; align-content: center;">
            <ContentTemplate>
                <table border="0" style="width: 350px; background-color: #EFEFF1" class="cuerpoComision">
                    <tr>
                        <td>

                            <%-- <asp:TextBox ID="txtMsjPop" runat="server" TextMode="MultiLine" Width="330px" TabIndex="18" Text="Cargando ...!!"></asp:TextBox>--%>
                            <asp:Label ID="txtMsjPop" runat="server" Text="Cargando ...!!"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table>
                                <tr>
                                    <td style="width: 120px;"></td>
                                    <td>
                                        <asp:Button ID="btnCerrarPop" runat="server" Text="Aceptar" Width="94px" Height="30px" CssClass="bootonClas" OnClientClick="return ClosePopup()" Visible="false" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>

    <ajaxToolkit:ModalPopupExtender ID="mpLoading" runat="server"
        CancelControlID="Button2" Enabled="True" PopupControlID="pnlLoading"
        TargetControlID="Button3">
    </ajaxToolkit:ModalPopupExtender>
    <asp:Button ID="Button2" Style="display: none;" runat="server" Text="[Open Proxy]" />
    <asp:Button ID="Button3" Style="display: none;" runat="server" Text="[Close Proxy]" />
    <!-- END -loading-->








    <script language="javascript" type="text/javascript">


        var tiempo = 0;
        var seconds = 0;

        var myVar = setInterval(function () {
            clearInterval(myVar);
        }, 1000);

        var myTiempo = setTimeout(function () {
            clearInterval(myTiempo);
        }, 1);

        function myStopFunction() {
            clearInterval(myVar);
            seconds = '<%=GlobalEntity.Instancia.TiempoDuracionSesion%>' * 60;
            tiempo = '<%=GlobalEntity.Instancia.TiempoDuracionSesion%>' * 1000 * 60;

            CuentaTiempo(tiempo);
        }

        function CuentaTiempo(tiempo) {
            clearTimeout(myTiempo);
            myTiempo = setTimeout(function () {

                //window.location = "../Seguridad/eFENIX_Login.aspx";
            }, tiempo);
        }



        function SessionExpireAlert(timeout) {
            seconds = timeout / 1000;
            document.getElementsByName("seconds").innerHTML = seconds;

            myvar = setInterval(function () {
                seconds--;
                document.getElementById("seconds").innerHTML = seconds;
            }, 1000);

            CuentaTiempo(timeout);
        };



        var objFilaAnt = null;
        var backgroundColorFilaAnt = "";






        function SoloEnteros(e) {
            var tecla = document.all ? tecla = e.keyCode : tecla = e.which;
            return ((tecla > 47 && tecla < 58) || tecla == 44 || tecla == 46);
        }


        function fc_Enter(e, sObjeto) {
            if (event.keyCode == 13) {
                if (sObjeto == 'TxtDocumentoM') {
                    document.getElementById("<%=BtnConsultaDo.ClientID%>").click();
                } else if (sObjeto == 'txtDniDespachador') {
                    document.getElementById("<%=btnDespachador.ClientID%>").click();
                } else if (sObjeto == 'txtRucFac') {
                    document.getElementById("<%=btnRucFac.ClientID%>").click();
                } else if (sObjeto == 'txtDua') {
                    document.getElementById("<%=BtnConsultaDo.ClientID%>").click();
                } else if (sObjeto == 'TxtNumeroPO') {
                    document.getElementById("<%=imbBtnBuscarAutRet.ClientID%>").click();
                }
                
                return false;
            }
        }


        function saveFileUpl(vi_uplNombre) {
            OpenPopup();

            if (vi_uplNombre == 'FileUploadBL') {
                document.getElementById("<%=imgBtnUplBL.ClientID%>").click();
            }
            if (vi_uplNombre == 'FileUploadEndoseLN') {
                document.getElementById("<%=imgBtnUplEndoseLN.ClientID%>").click();
            }
            if (vi_uplNombre == 'FileUploadSenasa') {
                document.getElementById("<%=imgBtnUplSenasa.ClientID%>").click();
            }
            if (vi_uplNombre == 'FileUploadMedioPago') {
                document.getElementById("<%=imgBtnUplMedioPago.ClientID%>").click();
            }
        }


        function SoloEnteros(e) {
            var tecla = document.all ? tecla = e.keyCode : tecla = e.which;
            return (tecla > 47 && tecla < 58);
        }

        function OpenPopup() {

            document.getElementById('<%=txtMsjPop.ClientID %>').innerHTML = 'Cargando...!!';
            document.getElementById("<%=Button3.ClientID%>").click();
            document.getElementById("<%=txtMsjPop.ClientID %>").style.color = "#0E0D0D";

        }
        function ClosePopup() {
            document.getElementById("<%=Button2.ClientID%>").click();
            document.getElementById('<%=txtMsjPop.ClientID %>').innerHTML = 'Cargando...!!';
            document.getElementById("<%=txtMsjPop.ClientID %>").style.color = "#0E0D0D";
        }

        function muestra_oculta(id1, id2, id3) {

            if (document.getElementById) { //se obtiene el id
                var el1 = document.getElementById(id1); //se define la variable "el" igual a nuestro div
                el1.style.display = (el1.style.display == 'none') ? 'block' : 'none'; //damos un atributo display:none que oculta el div

                var el2 = document.getElementById(id2); //se define la variable "el" igual a nuestro div
                el2.style.display = (el2.style.display == 'none') ? 'block' : 'none'; //damos un atributo display:none que oculta el div

                var el3 = document.getElementById(id3); //se define la variable "el" igual a nuestro div
                el3.style.display = (el3.style.display == 'none') ? 'block' : 'none'; //damos un atributo display:none que oculta el div

            }
        }

    </script>


    <style type="text/css">
        #reservationsList {
            width: 500px;
            max-height: 600px;
            background: #fff;
            overflow: auto;
        }

        /*STRIPE LIST*/
        ul.stripedList {
            margin: 0;
            padding: 0;
            list-style: none;
        }

        .stripedList li {
            display: block;
            text-decoration: none;
            color: #333;
            font-family: Arial,Helvetica,sans-serif;
            font-size: 12px;
            line-height: 20px;
            height: 20px;
        }

            .stripedList li span {
                display: inline-block;
                border-right: 1px solid #ccc;
                overflow: hidden;
                text-overflow: ellipsis;
                padding: 0 10px;
                height: 20px;
            }

        .stripedList .evenRow {
            background: #f2f2f2;
        }

        .c1 {
            width: 55px;
        }

        .c2 {
            width: 70px;
        }

        .c3 {
            width: 130px;
        }

        .c4 {
            width: 15px;
        }

        .cLast {
            border: 0 !important;
        }

        .auto-style2 {
            width: 147px;
        }

        .auto-style4 {
            width: 439px;
        }

        .bootonClas {
            background-color: #0069AE !important;
            border-color: #0069AE !important;
            color: #fff !important;
            font-size: 15px;
            font-family: Arial,Helvetica,sans-serif;
            font-weight: bold;
        }

        .auto-style9 {
            width: 99%;
            margin-top: 0px;
        }
        .auto-style10 {
            width: 99%;
            height: 10px;
        }
    </style>

</asp:Content>

