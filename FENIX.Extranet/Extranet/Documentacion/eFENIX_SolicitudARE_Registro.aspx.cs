﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;
using FENIX.BusinessEntity;
using FENIX.BusinessLogic;
using Newtonsoft.Json;
using System.Web.Security;
using System.Configuration;
using System.Threading;

namespace Documentacion
{
    public partial class eFENIX_AutorizacionRetiro : System.Web.UI.Page
    {

        BL_DocumentoOrigen oBL_DocumentoOrigen = new BL_DocumentoOrigen();
        List<BE_DocumentoOrigen> oBE_DocumentoOrigen = new List<BE_DocumentoOrigen>();
        BE_DocumentoOrigen oBE_DocOrigen = new BE_DocumentoOrigen();

        BL_AutorizacionRetiro oBL_AutorizacionRetiro = new BL_AutorizacionRetiro();
        List<BE_AutorizacionRetiro> oBE_AutorizacionRetiroList = new List<BE_AutorizacionRetiro>();
        BE_AutorizacionRetiro oBE_AutorizacionRetiro = new BE_AutorizacionRetiro();
        int IdAgente = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            String tiempo = (Convert.ToInt32(GlobalEntity.Instancia.TiempoDuracionSesion.ToString()) * 1000 * 60).ToString();
            Session["Reset"] = true;

            ScriptManager scriptManager = ScriptManager.GetCurrent(this.Page);
            scriptManager.RegisterPostBackControl(this.imgBtnUplBL);
            scriptManager.RegisterPostBackControl(this.imgBtnUplEndoseLN);
            scriptManager.RegisterPostBackControl(this.imgBtnUplSenasa);
            scriptManager.RegisterPostBackControl(this.imgBtnUplMedioPago);
            scriptManager.RegisterPostBackControl(this.BtnConsultaDo);
            scriptManager.RegisterPostBackControl(this.DplTipoCobroR);



            TxtDocumentoM.Attributes.Add("onkeypress", "javascript:return fc_Enter(event,'TxtDocumentoM');");

            //txtAnio.Attributes.Add("onkeypress", "javascript:return fc_Enter(event,'txtAnio');");
            //txtRegimen.Attributes.Add("onkeypress", "javascript:return fc_Enter(event,'txtRegimen');");
            txtDua.Attributes.Add("onkeypress", "javascript:return fc_Enter(event,'txtDua');");

            txtDniDespachador.Attributes.Add("onkeypress", "javascript:return fc_Enter(event,'txtDniDespachador');");
            txtRucFac.Attributes.Add("onkeypress", "javascript:return fc_Enter(event,'txtRucFac');");
            TxtNumeroPO.Attributes.Add("onkeypress", "javascript:return fc_Enter(event,'TxtNumeroPO');");


            IdAgente = GlobalEntity.Instancia.IdCliente;


            if (!Page.IsPostBack)
            {
                tbRenovacion.Enabled = false;
                tbPanelAdj.Enabled = false;
                TabContainer1.ActiveTabIndex = 0;

                listarDroplist();


                oBE_DocumentoOrigen.Add(oBE_DocOrigen);
                grvListadoSolicitud.DataSource = oBE_DocumentoOrigen;
                grvListadoSolicitud.DataBind();


                ViewState["usuario"] = GlobalEntity.Instancia.Usuario;

                ScriptManager.RegisterStartupScript(this, GetType(), "mensaje", "SessionExpireAlert(" + tiempo + ");", true);
            }
            else
                ScriptManager.RegisterStartupScript(this, GetType(), "mensaje", "myStopFunction();", true);

            ViewState["imgBtnUplBL"] = "0";
            ViewState["imgBtnUplEndoseLN"] = "0";
            ViewState["imgBtnUplSenasa"] = "0";
            ViewState["imgBtnUplMedioPago"] = "0";
        }


        protected void btn_cerrar_Click(object sender, EventArgs e)
        {
            if (GlobalEntity.Instancia.CerrarExtranet == "S")
            {
                BL_Usuario oBL_Usuario = new BL_Usuario();
                oBL_Usuario.GrabaInicioSesion(GlobalEntity.Instancia.Usuario, "0");
                Session.Abandon();
                FormsAuthentication.SignOut();
                Response.Redirect("../../eFENIX_Login.aspx");
                Response.End();
            }
        }
        private void MensajeScript(string SMS)
        {
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "", "<script>alert('" + SMS + "');</script>", false);


        }

        protected void TabContainer1_ActiveTabChanged(object sender, EventArgs e)
        {
            AjaxControlToolkit.TabContainer oTabContainer = (AjaxControlToolkit.TabContainer)sender;

            if (oTabContainer.ActiveTabIndex == 0)
            {
                btnNuevaSolicitud.Enabled = true;
            }
            else if (oTabContainer.ActiveTabIndex == 2)
            {
                cargarCombo();
            }
        }


        #region "Tab-Buscar BL"




        protected void btnNuevaSolicitud_Click(object sender, EventArgs e)
        {
            Response.Redirect("eFENIX_SolicitudARE_Registro.aspx");

        }

        protected void BtnConsultaDo_Click(object sender, EventArgs e)
        {
            habilitarDesabilitar(true, 0, 2);

            txtMsjPop.Text = "Cargando ..!!!";
            txtMsjPop.ForeColor = Color.Empty;
            btnCerrarPop.Visible = false;


            var resultValidate = validateSiguienteBtn(3);

            if (resultValidate.Item1 == false)
            {
                string mensaje = resultValidate.Item2;

                txtMsjPop.Text = mensaje + "<br/> ";
                txtMsjPop.ForeColor = Color.Red;
                mpLoading.Show();
                btnCerrarPop.Visible = true;
                return;
            }
            else
            {
                txtMsjPop.Text = "Cargando ..!!!";
                txtMsjPop.ForeColor = Color.Empty;
                btnCerrarPop.Visible = false;
            }


            buscarDocOrigen(TxtDocumentoM.Text.Trim());
            buscarWSdua();
        }

        void buscarWSdua()
        {
            rsptSunat.Text = "";
            rsptSunat.ForeColor = Color.Empty;

            if (DplRegimen.SelectedValue.Equals("10"))
            {
                string[] respuesta = consultaSunatWS(dplAnio.SelectedValue.ToString(), txtDua.Text).Split('|');

                if (respuesta[0].Equals("1"))
                {
                    txtResultadoLevante.Text = Convert.ToDateTime(respuesta[1]).ToString();
                    txtResultadoLevante.ForeColor = Color.Green;
                    txtFechaLevante.Text = respuesta[1];

                    if (respuesta[2].Equals(vl_sDocOrigen.Value))
                    {
                        rsptSunat.Text = "";
                        rsptSunat.ForeColor = Color.Empty;
                    }
                    else
                    {
                        rsptSunat.Text = "**EL BL ingresado (" + vl_sDocOrigen.Value + ") no coincide con el BL (" + respuesta[2] + ") obtenido de SUNAT**";
                        rsptSunat.ForeColor = Color.Red;
                    }

                }
                else
                {
                    txtResultadoLevante.Text = "SUNAT no responde, es posible que no tenga fechaLevante ";
                    txtResultadoLevante.ForeColor = Color.Red;
                    //crearSolicitud(2, null);
                    //return;
                }

            }
            else
            {
                // crearSolicitud(2, "");
            }

            vl_sDua.Value = dplAnio.SelectedValue + "-" + DplRegimen.SelectedValue + "-" + txtDua.Text;
            vl_anio.Value = dplAnio.SelectedValue;
            vl_regimen.Value = DplRegimen.SelectedValue;
            vl_dua.Value = txtDua.Text;

        }

        void cargarDatosGrilla()
        {
            BL_DocumentoOrigen oBL_DocumentoOrigen_g = new BL_DocumentoOrigen();
            List<BE_DocumentoOrigen> oBE_DocumentoOrigen_g = new List<BE_DocumentoOrigen>();
            BE_DocumentoOrigen oBE_DocOrigen_g = new BE_DocumentoOrigen();

            int iItem = 0;


            foreach (GridViewRow GrvRow in grvListadoAdd.Rows)
            {
                try
                {
                    oBE_DocOrigen_g = new BE_DocumentoOrigen();
                    oBE_DocOrigen_g.iItem = Convert.ToInt32(grvListadoAdd.DataKeys[GrvRow.RowIndex].Values["iItem"]);
                    iItem = Convert.ToInt32(grvListadoAdd.DataKeys[GrvRow.RowIndex].Values["iItem"]);
                    oBE_DocOrigen_g.iIdDocOri = Convert.ToInt32(grvListadoAdd.DataKeys[GrvRow.RowIndex].Values["iIdDocOri"]);
                    oBE_DocOrigen_g.idliquidacion = Convert.ToInt32(grvListadoAdd.DataKeys[GrvRow.RowIndex].Values["idliquidacion"]);
                    oBE_DocOrigen_g.iIdCliente = Convert.ToInt32(grvListadoAdd.DataKeys[GrvRow.RowIndex].Values["iIdCliente"]);
                    oBE_DocOrigen_g.sNumeroDO = grvListadoAdd.DataKeys[GrvRow.RowIndex].Values["sNumeroDO"].ToString();
                    oBE_DocOrigen_g.sVolante = grvListadoAdd.DataKeys[GrvRow.RowIndex].Values["sVolante"].ToString();
                    oBE_DocOrigen_g.sCliente = grvListadoAdd.DataKeys[GrvRow.RowIndex].Values["sCliente"].ToString();
                    oBE_DocOrigen_g.sDua = grvListadoAdd.DataKeys[GrvRow.RowIndex].Values["sDua"].ToString();
                    oBE_DocOrigen_g.sMonto = grvListadoAdd.DataKeys[GrvRow.RowIndex].Values["sMonto"].ToString();

                    oBE_DocOrigen_g.sDuaAnio = grvListadoAdd.DataKeys[GrvRow.RowIndex].Values["sDuaAnio"].ToString();
                    oBE_DocOrigen_g.sRegimen = grvListadoAdd.DataKeys[GrvRow.RowIndex].Values["sRegimen"].ToString();
                    oBE_DocOrigen_g.sDuaNumero = grvListadoAdd.DataKeys[GrvRow.RowIndex].Values["sDuaNumero"].ToString();

                    oBE_DocumentoOrigen_g.Add(oBE_DocOrigen_g);
                    oBE_DocOrigen_g = null;
                }
                catch (Exception) { }
            }

            oBE_DocOrigen_g = new BE_DocumentoOrigen();

            oBE_DocOrigen_g.iItem = iItem + 1;
            oBE_DocOrigen_g.iIdDocOri = Convert.ToInt32(vl_idDocOri.Value);
            oBE_DocOrigen_g.idliquidacion = Convert.ToInt32(txtLiquidacion.Text);
            oBE_DocOrigen_g.iIdCliente = Convert.ToInt32(vl_idCliente.Value);
            oBE_DocOrigen_g.sNumeroDO = vl_sDocOrigen.Value;
            oBE_DocOrigen_g.sVolante = "";
            oBE_DocOrigen_g.sCliente = txtCliente.Text;
            oBE_DocOrigen_g.sDua = vl_sDua.Value;
            oBE_DocOrigen_g.sMonto = txtMontoLiquidacion.Text;

            //oBE_DocOrigen_g.idAduana = txtMontoLiquidacion.Text;
            oBE_DocOrigen_g.sDuaAnio = vl_anio.Value;
            oBE_DocOrigen_g.sRegimen = vl_regimen.Value;
            oBE_DocOrigen_g.sDuaNumero = vl_dua.Value;




            oBE_DocumentoOrigen_g.Add(oBE_DocOrigen_g);
            oBE_DocOrigen_g = null;

            grvListadoAdd.DataSource = oBE_DocumentoOrigen_g;
            grvListadoAdd.DataBind();

        }



        protected void imgBtnAgregarGrilla_Click(object sender, EventArgs e)
        {
            try
            {
                cargarDatosGrilla();
            }
            catch { }
           
        }

        protected void grvListadoAdd_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            BL_DocumentoOrigen oBL_DocumentoOrigen_g = new BL_DocumentoOrigen();
            List<BE_DocumentoOrigen> oBE_DocumentoOrigen_g = new List<BE_DocumentoOrigen>();
            BE_DocumentoOrigen oBE_DocOrigen_g = new BE_DocumentoOrigen();


            switch (e.CommandName)
            {
                case "Eliminar":
                    Int32 iItem = Convert.ToInt32(e.CommandArgument.ToString());

                    foreach (GridViewRow GrvRow in grvListadoAdd.Rows)
                    {
                        try
                        {
                            oBE_DocOrigen_g = new BE_DocumentoOrigen();

                            oBE_DocOrigen_g.iItem = Convert.ToInt32(grvListadoAdd.DataKeys[GrvRow.RowIndex].Values["iItem"]);
                            oBE_DocOrigen_g.iIdDocOri = Convert.ToInt32(grvListadoAdd.DataKeys[GrvRow.RowIndex].Values["iIdDocOri"]);
                            oBE_DocOrigen_g.idliquidacion = Convert.ToInt32(grvListadoAdd.DataKeys[GrvRow.RowIndex].Values["idliquidacion"]);
                            oBE_DocOrigen_g.iIdCliente = Convert.ToInt32(grvListadoAdd.DataKeys[GrvRow.RowIndex].Values["iIdCliente"]);
                            oBE_DocOrigen_g.sNumeroDO = grvListadoAdd.DataKeys[GrvRow.RowIndex].Values["sNumeroDO"].ToString();
                            oBE_DocOrigen_g.sVolante = grvListadoAdd.DataKeys[GrvRow.RowIndex].Values["sVolante"].ToString();
                            oBE_DocOrigen_g.sCliente = grvListadoAdd.DataKeys[GrvRow.RowIndex].Values["sCliente"].ToString();
                            oBE_DocOrigen_g.sDua = grvListadoAdd.DataKeys[GrvRow.RowIndex].Values["sDua"].ToString();
                            oBE_DocOrigen_g.sMonto = grvListadoAdd.DataKeys[GrvRow.RowIndex].Values["sMonto"].ToString();

                            oBE_DocOrigen_g.sDuaAnio = grvListadoAdd.DataKeys[GrvRow.RowIndex].Values["sDuaAnio"].ToString();
                            oBE_DocOrigen_g.sRegimen = grvListadoAdd.DataKeys[GrvRow.RowIndex].Values["sRegimen"].ToString();
                            oBE_DocOrigen_g.sDuaNumero = grvListadoAdd.DataKeys[GrvRow.RowIndex].Values["sDuaNumero"].ToString();

                            oBE_DocumentoOrigen_g.Add(oBE_DocOrigen_g);
                            oBE_DocOrigen_g = null;
                        }
                        catch (Exception) { }
                    }

                    oBE_DocumentoOrigen_g.RemoveAll(delegate (BE_DocumentoOrigen obj) { return obj.iItem == iItem; });

                    grvListadoAdd.DataSource = oBE_DocumentoOrigen_g;
                    grvListadoAdd.DataBind();

                    break;

            }
        }

        protected void btnSiguiente_Click(object sender, EventArgs e)
        {
            var resultValidate = validateSiguienteBtn(2);

            if (resultValidate.Item1 == false)
            {
                string mensaje = resultValidate.Item2;


                txtMsjPop.Text = mensaje + "<br/> ";
                txtMsjPop.ForeColor = Color.Red;
                mpLoading.Show();
                btnCerrarPop.Visible = true;
                return;

            }


            try
            {
                DateTime fechLevante = Convert.ToDateTime(txtFechaLevante.Text);

                crearSolicitud(1, fechLevante.ToString());
            }
            catch (Exception)
            {
                crearSolicitud(2, "");

            }


        }

        void buscarDocOrigen(String numeroDo)
        {
            vl_idDocOri.Value = "";
            vl_idCliente.Value = "";
            vl_idAgenciaAduana.Value = "";
            vl_sDocOrigen.Value = "";
            vl_anio.Value = "";
            vl_regimen.Value = "";
            vl_dua.Value = "";


            oBE_DocOrigen.sNumeroDO = numeroDo;
            oBE_DocOrigen.iIdAgenciaAduana = IdAgente;
            oBE_DocumentoOrigen = oBL_DocumentoOrigen.ListarDO_AR(oBE_DocOrigen, 1);

            string res = "";
            res = oBE_DocOrigen.sRetorno;

            if (res != "")
            {
                MensajeScript(res);
            }


            try
            {

                if (oBE_DocumentoOrigen.Count > 0)
                {
                    vl_sDocOrigen.Value = oBE_DocumentoOrigen[0].sNumeroDO;

                    txtCliente.Text = oBE_DocumentoOrigen[0].sCliente_Descripcion;
                    txtAgenciaAduana.Text = oBE_DocumentoOrigen[0].sAgenciaAdu_Descripcion;
                    txtLinea.Text = oBE_DocumentoOrigen[0].sLineaMaritima;
                    txtLiquidacion.Text = oBE_DocumentoOrigen[0].idliquidacion.ToString();
                    txtResultadoLevante.Text = "";// oBE_DocumentoOrigen[0].dtTerminoDescarga.ToString("dd/M/yyyy HH:mm tt");


                    vl_idDocOri.Value = Convert.ToString(oBE_DocumentoOrigen[0].iIdDocOri);
                    vl_idCliente.Value = Convert.ToString(oBE_DocumentoOrigen[0].iIdCliente);
                    vl_idAgenciaAduana.Value = Convert.ToString(oBE_DocumentoOrigen[0].iIdAgenciaAduana);

                    txtBlAdj.Text = oBE_DocumentoOrigen[0].sNumeroDO;
                    txtClienteAdj.Text = oBE_DocumentoOrigen[0].sCliente_Descripcion;
                    txtAgenciAdj.Text = oBE_DocumentoOrigen[0].sAgenciaAdu_Descripcion;

                    txtMontoLiquidacion.Text = oBE_DocumentoOrigen[0].dtMontoLiquidacion.ToString();
                    listarDocOrigenDet(oBE_DocumentoOrigen[0].iIdDocOri);
                }
            }
            catch (Exception ex) { }


        }

        void listarDocOrigenDet(int? iIdDocOri)
        {

            oBE_DocOrigen.iIdDocOri = iIdDocOri;
            oBE_DocumentoOrigen = oBL_DocumentoOrigen.ListarDO_AR(oBE_DocOrigen, 2);

            grvListadoSolicitud.DataSource = oBE_DocumentoOrigen;
            grvListadoSolicitud.DataBind();

        }

        void crearSolicitud(int accion, string dfechaLevante)
        {
            if (accion == 1) { oBE_AutorizacionRetiro.dtFechaLevante = Convert.ToDateTime(dfechaLevante); }


            int vll_idDocOri = 0, vll_iIdCliente = 0, vll_idLiquidacion = 0, i = 0;
            string vll_sDuaNumero = String.Empty, vll_sDuaAnio = String.Empty, vll_IdRegimen = String.Empty;


            if (chkLigado.Checked == true) //LIGADO
            {
                foreach (GridViewRow GrvRow in grvListadoAdd.Rows)
                {
                    try
                    {
                        if (i == 0)
                        {
                            vll_idDocOri = Convert.ToInt32(grvListadoAdd.DataKeys[GrvRow.RowIndex].Values["iIdDocOri"]);
                            vll_idLiquidacion = Convert.ToInt32(grvListadoAdd.DataKeys[GrvRow.RowIndex].Values["idliquidacion"]);
                            vll_iIdCliente = Convert.ToInt32(grvListadoAdd.DataKeys[GrvRow.RowIndex].Values["iIdCliente"]);
                            vll_sDuaAnio = grvListadoAdd.DataKeys[GrvRow.RowIndex].Values["sDuaAnio"].ToString();
                            vll_IdRegimen = grvListadoAdd.DataKeys[GrvRow.RowIndex].Values["sRegimen"].ToString();
                            vll_sDuaNumero = grvListadoAdd.DataKeys[GrvRow.RowIndex].Values["sDuaNumero"].ToString();

                            i = 1;
                        }
                    }
                    catch (Exception) { }
                }

                oBE_AutorizacionRetiro.idDocOri = vll_idDocOri;
                oBE_AutorizacionRetiro.iIdCliente = vll_iIdCliente;
                oBE_AutorizacionRetiro.idLiquidacion = vll_idLiquidacion;
                oBE_AutorizacionRetiro.sDuaAnio = vll_sDuaAnio;
                oBE_AutorizacionRetiro.IdRegimen = Convert.ToInt32(vll_IdRegimen);
                oBE_AutorizacionRetiro.sDuaNumero = vll_sDuaNumero;
            }
            else
            {
                oBE_AutorizacionRetiro.idDocOri = Convert.ToInt32(vl_idDocOri.Value);
                oBE_AutorizacionRetiro.iIdCliente = Convert.ToInt32(vl_idCliente.Value);
                oBE_AutorizacionRetiro.idLiquidacion = Convert.ToInt32(txtLiquidacion.Text);
                oBE_AutorizacionRetiro.idAduana = Convert.ToInt32(txtCodAlmacenDA.Text);
                oBE_AutorizacionRetiro.sDuaAnio = vl_anio.Value;
                oBE_AutorizacionRetiro.IdRegimen = Convert.ToInt32(vl_regimen.Value);
                oBE_AutorizacionRetiro.sDuaNumero = vl_dua.Value;
            }

            oBE_AutorizacionRetiro.iIdAgencia = IdAgente;
            oBE_AutorizacionRetiro.SUsuario = ViewState["usuario"].ToString();

            var result = oBL_AutorizacionRetiro.Ext_SO_AutorizacionRetiro(oBE_AutorizacionRetiro, 2);

            vi_IdSolicitudAre.Value = result.Item2;

            if (result.Item2 != "")
            {


                tbPanelAdj.Enabled = true;
                //tbPanelBusqueda.Enabled = false;
                TabContainer1.ActiveTabIndex = 1;
                habilitarDesabilitar(false, 2, 0);

            }


            //tbPrincipal.ActiveTabIndex = 0;
        }

        #region Consulta WS

        string consultaSunatWS(string vi_anio, string vi_dua)
        {
            try
            {
                VerificarLevanteDua resultClas = new VerificarLevanteDua();
                using (var client = new HttpClient())
                {
                    levanteImput p = new levanteImput { anio = vi_anio, nroDua = vi_dua };
                    client.BaseAddress = new Uri(ConfigurationManager.AppSettings["Are_SunatWS"]);
                    var response = client.PostAsync("api/VerificarLevante", new StringContent(
                            new JavaScriptSerializer().Serialize(p), Encoding.UTF8, "application/json")).Result;
                    string resultContent = response.Content.ReadAsStringAsync().Result;

                    if (response.IsSuccessStatusCode)
                    {
                        resultClas = JsonConvert.DeserializeObject<VerificarLevanteDua>(resultContent);
                    }

                }

                return resultClas.StatusCode + "|" + resultClas.fechaLevante + "|" + resultClas.BlCode;
            }
            catch (Exception)
            {
                return "1|0|";
            }

        }

        #endregion

        #region "VALIDACIONES"
        public Tuple<Boolean, string> validateSiguienteBtn(int tipo)
        {
            txtResultadoLevante.Text = "";
            string mensaje = "";
            Boolean isValid = true;


            if (tipo == 1)//btn guardar
            {
                if (string.IsNullOrEmpty(txtRucFac.Text))
                {
                    mensaje = mensaje + " Ingrese Ruc a Facturar <br/>";
                    txtRucFac.Focus();
                    isValid = false;
                }

                if (txtRucFac.Text.Length != 11)
                {
                    mensaje = mensaje + " El Ruc debe de tener 11 digitos <br/>";
                    txtRucFac.Focus();
                    isValid = false;
                }

                if (lblUplBLID.Text.Equals("0"))
                {
                    mensaje = mensaje + " Debe adjuntar el BL <br/>";
                    isValid = false;

                }


                if (vi_idDespachador.Value.Equals("") || lblDespachador.Text.Length < 1)
                {
                    mensaje = mensaje + " Ingrese Despachador <br/>";
                    txtDniDespachador.Focus();
                    isValid = false;
                }

                if (DplTipoCobro.SelectedIndex == 0)
                {
                    mensaje = mensaje + " Ingrese el Tipo de Pago <br/>";
                    DplTipoCobro.Focus();
                    isValid = false;
                }

                if (DplTipoCobro.SelectedIndex == 2)//contado
                {

                    if (DplFormaPago.SelectedValue.Equals("-1"))
                    {
                        mensaje = mensaje + " Ingrese Forma de Pago <br/>";
                        DplFormaPago.Focus();
                        isValid = false;
                    }

                    if (DplBanco.SelectedValue.Equals("-1"))
                    {
                        mensaje = mensaje + " Ingrese el Banco <br/>";
                        DplBanco.Focus();
                        isValid = false;
                    }

                    if (DplMonedaPago.SelectedValue.Equals("-1"))
                    {
                        mensaje = mensaje + " Ingrese la Moneda <br/>";
                        DplMonedaPago.Focus();
                        isValid = false;
                    }
                    if (string.IsNullOrEmpty(txtMontoPago.Text))
                    {
                        mensaje = mensaje + " Ingrese el Monto Pago <br/>";
                        txtMontoPago.Focus();
                        isValid = false;
                    }
                    if (string.IsNullOrEmpty(txtNumeroOperacion.Text))
                    {
                        mensaje = mensaje + " Ingrese el Numero Operacion <br/>";
                        txtNumeroOperacion.Focus();
                        isValid = false;
                    }

                    //if (chkLigado.Checked == true)
                    //{
                    //    int liquidacion = 0;

                    //    foreach (GridViewRow GrvRow in grvListadoAdd.Rows)
                    //    {
                    //        liquidacion = Convert.ToInt32(grvListadoAdd.DataKeys[GrvRow.RowIndex].Values["idliquidacion"]);

                    //        if (liquidacion == 0)
                    //        {
                    //            mensaje = mensaje + "  No tiene pre-liquidacion uno de los ITEM <br/>";
                    //            isValid = false;
                    //        }
                    //    }
                    //}


                    if (chkLigado.Checked == false)
                    {
                        if (txtLiquidacion.Text.Equals("0"))
                        {
                            mensaje = mensaje + "  No tiene pre-liquidacion <br/>";
                            isValid = false;
                        }
                    }

                }
                if (DplTipoCobro.SelectedValue.Equals("2"))//contado
                {
                    if (lblUplMedioPagoID.Text.Equals("0"))
                    {
                        mensaje = mensaje + " Debe de adjuntar el medio de pago <br/>";
                        isValid = false;
                    }
                }
            }
            if (tipo == 2)
            {

                if (chkLigado.Checked == false)
                {
                    if (String.IsNullOrEmpty(vl_idDocOri.Value))
                    {
                        mensaje = mensaje + " Debe de Ingresar el BL <br/>";
                        TxtDocumentoM.Focus();
                        TxtDocumentoM.BorderColor = Color.Red;
                        isValid = false;
                    }
                    else { TxtDocumentoM.BorderColor = Color.Green; }


                    if (String.IsNullOrEmpty(vl_anio.Value) || String.IsNullOrEmpty(vl_regimen.Value) || String.IsNullOrEmpty(vl_dua.Value))
                    {
                        mensaje = mensaje + " Debe de Ingresar la DUA <br/>";
                        txtDua.Focus();
                        txtDua.BorderColor = Color.Red;
                        isValid = false;
                    }
                    else { txtDua.BorderColor = Color.Green; }
                }

                if (chkLigado.Checked == true)
                {
                    if (grvListadoAdd.Rows.Count == 0)
                    {
                        mensaje = mensaje + " Debe Ingresar como minimo un ITEM <br/>";
                        isValid = false;
                    }
                }



            }
            if (tipo == 3)
            {
                if (String.IsNullOrEmpty(txtDua.Text))
                {
                    mensaje = mensaje + " Debe de Ingresar el numero de DUA/DAM <br/>";
                    txtDua.Focus();
                    txtDua.BorderColor = Color.Red;
                    isValid = false;
                }
                else { txtDua.BorderColor = Color.Green; }


                if (String.IsNullOrEmpty(TxtDocumentoM.Text))
                {
                    mensaje = mensaje + " Debe de Ingresar  BL <br/>";
                    TxtDocumentoM.Focus();
                    TxtDocumentoM.BorderColor = Color.Red;
                    isValid = false;
                }
                else { TxtDocumentoM.BorderColor = Color.Green; }

            }
            return Tuple.Create(isValid, mensaje);
        }

        protected void chkLigado_OnCheckedChanged(object sender, EventArgs e)
        {
            CheckBox cb = new CheckBox();
            if (chkLigado.Checked == true)//es ligado
            {
                this.fildsetDatosA.Visible = true;
                this.tbSiguiente.Visible = false;
                this.tbAgregar.Visible = true;
            }
            if (chkLigado.Checked == false) //no es ligado
            {
                this.fildsetDatosA.Visible = false;
                this.tbSiguiente.Visible = true;
                this.tbAgregar.Visible = false;
            }
        }

        #endregion

        #endregion

        #region "Tab-Adjuntar documentos"

        Tuple<string, int> cargarArchivo(FileUpload fileUpload, string IdAgente, string prefijo)
        {
            string ruta = ConfigurationManager.AppSettings["Are_Solicitud"] + IdAgente + "/";

            string filename = "";
            int estado = 0;
            string fecha = DateTime.Now.ToString("ddMMyyHmmss");
            try
            {
                if (fileUpload.HasFile)
                {
                    if (fileUpload.PostedFile.ContentType == "application/pdf")
                    {
                        //COMPROBAR SI EXISTE RUTA 
                        if (!File.Exists(ruta))
                        {
                            //SI NO EXISTE CREAR LA RUTA
                            DirectoryInfo directory = Directory.CreateDirectory(ruta);
                        }

                        //GUARDA INFORMACION
                        //filename = Path.GetFileName(fecha + "_" + fileUpload.FileName);
                        filename = Path.GetFileName(prefijo + "_" + fecha + ".pdf");
                        fileUpload.SaveAs(ruta + filename);
                        estado = 1;
                    }
                    else
                    {
                        estado = 2;
                    }
                }


            }
            catch (Exception ex)
            {
                //MensajeScript("eror de upload");
                estado = 0;
            }

            return Tuple.Create(filename, estado);
        }


        protected void imgBtnUplBL_Click(object sender, EventArgs e)
        {

            var result = cargarArchivo(FileUploadBL, IdAgente.ToString(), "BL");

            if (result.Item2 == 1)
            {
                lblUplBL.Text = FileUploadBL.FileName;
                UploadIconoEnabled(1, 2);

                oBE_AutorizacionRetiro.idSolicitudAreDet = Convert.ToInt32(lblUplBLID.Text);
                oBE_AutorizacionRetiro.IdSolicitudAre = Convert.ToInt32(vi_IdSolicitudAre.Value);
                oBE_AutorizacionRetiro.iTipoDocAdjuntado = 1;
                oBE_AutorizacionRetiro.sRuta = result.Item1;
                oBE_AutorizacionRetiro.SUsuario = ViewState["usuario"].ToString();

                var res = oBL_AutorizacionRetiro.Ext_SO_AutorizacionRetiroDet(oBE_AutorizacionRetiro, 2);

                lblUplBLID.Text = res.Item2;

            }
            else if (result.Item2 == 2)
            {
                MensajeScript("Ingrese un archivo en Formato PDF");
            }

        }
        protected void imgBtnUplEndoseLN_Click(object sender, EventArgs e)
        {


            var result = cargarArchivo(FileUploadEndoseLN, IdAgente.ToString(), "ED");

            if (result.Item2 == 1)
            {
                lblUplEndoseLN.Text = FileUploadEndoseLN.FileName;
                UploadIconoEnabled(2, 2);

                oBE_AutorizacionRetiro.idSolicitudAreDet = Convert.ToInt32(lblUplEndoseLNID.Text);
                oBE_AutorizacionRetiro.IdSolicitudAre = Convert.ToInt32(vi_IdSolicitudAre.Value);
                oBE_AutorizacionRetiro.iTipoDocAdjuntado = 2;
                oBE_AutorizacionRetiro.sRuta = result.Item1;
                oBE_AutorizacionRetiro.SUsuario = ViewState["usuario"].ToString();

                var res = oBL_AutorizacionRetiro.Ext_SO_AutorizacionRetiroDet(oBE_AutorizacionRetiro, 2);

                lblUplEndoseLNID.Text = res.Item2;
            }
            else if (result.Item2 == 2)
            {
                MensajeScript("Ingrese un archivo en Formato PDF");
            }

        }
        protected void imgBtnUplSenasa_Click(object sender, EventArgs e)
        {

            var result = cargarArchivo(FileUploadSenasa, IdAgente.ToString(), "SE");

            if (result.Item2 == 1)
            {
                lblUplSenasa.Text = FileUploadSenasa.FileName;
                UploadIconoEnabled(3, 2);

                oBE_AutorizacionRetiro.idSolicitudAreDet = Convert.ToInt32(lblUplSenasaID.Text);
                oBE_AutorizacionRetiro.IdSolicitudAre = Convert.ToInt32(vi_IdSolicitudAre.Value);
                oBE_AutorizacionRetiro.iTipoDocAdjuntado = 3;
                oBE_AutorizacionRetiro.sRuta = result.Item1;
                oBE_AutorizacionRetiro.SUsuario = ViewState["usuario"].ToString();

                var res = oBL_AutorizacionRetiro.Ext_SO_AutorizacionRetiroDet(oBE_AutorizacionRetiro, 2);

                lblUplSenasaID.Text = res.Item2;
            }
            else if (result.Item2 == 2)
            {
                MensajeScript("Ingrese un archivo en Formato PDF");
            }
        }
        protected void imgBtnUplMedioPago_Click(object sender, EventArgs e)
        {

            var result = cargarArchivo(FileUploadMedioPago, IdAgente.ToString(), "PG");

            if (result.Item2 == 1)
            {
                lblUplMedioPago.Text = FileUploadMedioPago.FileName;
                UploadIconoEnabled(4, 2);

                oBE_AutorizacionRetiro.idSolicitudAreDet = Convert.ToInt32(lblUplMedioPagoID.Text);
                oBE_AutorizacionRetiro.IdSolicitudAre = Convert.ToInt32(vi_IdSolicitudAre.Value);
                oBE_AutorizacionRetiro.iTipoDocAdjuntado = 4;
                oBE_AutorizacionRetiro.sRuta = result.Item1;
                oBE_AutorizacionRetiro.SUsuario = ViewState["usuario"].ToString();

                var res = oBL_AutorizacionRetiro.Ext_SO_AutorizacionRetiroDet(oBE_AutorizacionRetiro, 2);

                lblUplMedioPagoID.Text = res.Item2;
            }
            else if (result.Item2 == 2)
            {
                MensajeScript("Ingrese un archivo en Formato PDF");
            }
        }

        /**/

        protected void imgBtnUplBLupd_Click(object sender, ImageClickEventArgs e)
        {
            UploadIconoEnabled(1, 1);
        }
        protected void imgBtnUplEndoseLNupd_Click(object sender, ImageClickEventArgs e)
        {
            UploadIconoEnabled(2, 1);
        }
        protected void imgBtnUplSenasaupd_Click(object sender, ImageClickEventArgs e)
        {
            UploadIconoEnabled(3, 1);
        }
        protected void imgBtnUplMedioPagoupd_Click(object sender, ImageClickEventArgs e)
        {
            UploadIconoEnabled(4, 1);
        }

        void UploadIconoEnabled(int accion, int tipo)
        {
            if (tipo == 1)
            {
                imgBtnUplBL.Enabled = false;
                imgBtnUplEndoseLN.Enabled = false;
                imgBtnUplSenasa.Enabled = false;
                imgBtnUplMedioPago.Enabled = false;

                FileUploadBL.Enabled = false;
                FileUploadEndoseLN.Enabled = false;
                FileUploadSenasa.Enabled = false;
                FileUploadMedioPago.Enabled = false;

                imgBtnUplBL.Visible = false;
                imgBtnUplEndoseLN.Visible = false;
                imgBtnUplSenasa.Visible = false;
                imgBtnUplMedioPago.Visible = false;


                imgBtnUplBLupd.Visible = true;
                imgBtnUplEndoseLNupd.Visible = true;
                imgBtnUplSenasaupd.Visible = true;
                imgBtnUplMedioPagoupd.Visible = true;

                if (accion == 1)
                {
                    ViewState["imgBtnUplBL"] = "1";
                    imgBtnUplBL.Enabled = true;
                    imgBtnUplBL.Visible = true;
                    imgBtnUplBLupd.Visible = false;
                    FileUploadBL.Enabled = true;
                }
                if (accion == 2)
                {
                    ViewState["imgBtnUplEndoseLN"] = "1";
                    imgBtnUplEndoseLN.Enabled = true;
                    imgBtnUplEndoseLN.Visible = true;
                    imgBtnUplEndoseLNupd.Visible = false;
                    FileUploadEndoseLN.Enabled = true;
                }
                if (accion == 3)
                {
                    ViewState["imgBtnUplSenasa"] = "1";
                    imgBtnUplSenasa.Enabled = true;
                    imgBtnUplSenasa.Visible = true;
                    imgBtnUplSenasaupd.Visible = false;
                    FileUploadSenasa.Enabled = true;
                }
                if (accion == 4)
                {
                    ViewState["imgBtnUplMedioPago"] = "1";
                    imgBtnUplMedioPago.Enabled = true;
                    imgBtnUplMedioPago.Visible = true;
                    imgBtnUplMedioPagoupd.Visible = false;
                    FileUploadMedioPago.Enabled = true;
                }

            }

            if (tipo == 2)
            {
                if (accion == 1)
                {
                    FileUploadBL.Enabled = false;
                    imgBtnUplBL.Visible = false;
                    imgBtnUplBLupd.Visible = true;

                    //imgBtnUplEndoseLN.Enabled = true;
                    //FileUploadEndoseLN.Enabled = true;

                    ViewState["imgBtnUplBL"] = "0";
                }
                if (accion == 2)
                {

                    FileUploadEndoseLN.Enabled = false;
                    imgBtnUplEndoseLN.Visible = false;
                    imgBtnUplEndoseLNupd.Visible = true;

                    //imgBtnUplSenasa.Enabled = true;
                    //FileUploadSenasa.Enabled = true;

                    ViewState["imgBtnUplEndoseLN"] = "0";
                }
                if (accion == 3)
                {
                    FileUploadSenasa.Enabled = false;
                    imgBtnUplSenasa.Visible = false;
                    imgBtnUplSenasaupd.Visible = true;

                    //imgBtnUplMedioPago.Enabled = true;
                    //FileUploadMedioPago.Enabled = true;

                    ViewState["imgBtnUplSenasa"] = "0";
                }
                if (accion == 4)
                {
                    FileUploadMedioPago.Enabled = false;
                    imgBtnUplMedioPago.Visible = false;
                    imgBtnUplMedioPagoupd.Visible = true;

                    ViewState["imgBtnUplMedioPago"] = "0";
                }
            }

            if (tipo == 3)//(desabilita y habilita)
            {
                if (accion == 1) //desabilita todo 
                {
                    imgBtnUplBL.Enabled = false;
                    imgBtnUplEndoseLN.Enabled = false;
                    imgBtnUplSenasa.Enabled = false;
                    imgBtnUplMedioPago.Enabled = false;

                    FileUploadBL.Enabled = false;
                    FileUploadEndoseLN.Enabled = false;
                    FileUploadSenasa.Enabled = false;
                    FileUploadMedioPago.Enabled = false;

                    imgBtnUplBL.Enabled = false;
                    imgBtnUplEndoseLN.Enabled = false;
                    imgBtnUplSenasa.Enabled = false;
                    imgBtnUplMedioPago.Enabled = false;

                    imgBtnUplBLupd.Enabled = false;
                    imgBtnUplEndoseLNupd.Enabled = false;
                    imgBtnUplSenasaupd.Enabled = false;
                    imgBtnUplMedioPagoupd.Enabled = false;
                }

                if (accion == 2)//habilita valores para un nuevo registro
                {
                    //FileUploadBL.Enabled = true;
                    //FileUploadEndoseLN.Enabled = true;
                    //FileUploadSenasa.Enabled = true;
                    //FileUploadMedioPago.Enabled = true;


                    //no visible los btn para cambiar
                    imgBtnUplBLupd.Visible = false;
                    imgBtnUplEndoseLNupd.Visible = false;
                    imgBtnUplSenasaupd.Visible = false;
                    imgBtnUplMedioPagoupd.Visible = false;

                    //hbailitados los btn para cambiar
                    imgBtnUplBLupd.Enabled = true;
                    imgBtnUplEndoseLNupd.Enabled = true;
                    imgBtnUplSenasaupd.Enabled = true;
                    imgBtnUplMedioPagoupd.Enabled = true;



                    //hbailitados los botones guardar
                    //imgBtnUplBL.Enabled = true;
                    imgBtnUplEndoseLN.Enabled = true;
                    imgBtnUplSenasa.Enabled = true;
                    imgBtnUplMedioPago.Enabled = true;

                    //visible los botones guardar 
                    imgBtnUplBL.Visible = true;
                    imgBtnUplEndoseLN.Visible = true;
                    imgBtnUplSenasa.Visible = true;
                    imgBtnUplMedioPago.Visible = true;

                    //limpiar los lbl_id y los lbl_nombreArchivo

                    lblUplBL.Text = "";
                    lblUplEndoseLN.Text = "";
                    lblUplSenasa.Text = "";
                    lblUplMedioPago.Text = "";

                    lblUplBLID.Text = "";
                    lblUplEndoseLNID.Text = "";
                    lblUplSenasaID.Text = "";
                    lblUplMedioPagoID.Text = "";

                    txtBlAdj.Text = "";
                    txtClienteAdj.Text = "";
                    txtFechaLevante.Text = "";
                    txtAgenciAdj.Text = "";
                    lblDespachador.Text = "";
                }

            }

        }

        protected void btnDespachador_Click(object sender, ImageClickEventArgs e)
        {
            oBE_DocOrigen.sDni = txtDniDespachador.Text;
            oBE_DocOrigen.sRucAgencia = "";
            oBE_DocumentoOrigen = oBL_DocumentoOrigen.ListarDO_AR(oBE_DocOrigen, 3);

            if (oBE_DocumentoOrigen.Count > 0)
            {

                lblDespachador.Text = oBE_DocumentoOrigen[0].sNombreDespachador;

                vi_idDespachador.Value = Convert.ToString(oBE_DocumentoOrigen[0].IdDespachador);

            }
            else
            {
                lblDespachador.Text = "";
                vi_idDespachador.Value = "";
            }
        }
        protected void btnRucFac_Click(object sender, ImageClickEventArgs e)
        {
            oBE_DocOrigen.sRuc = txtRucFac.Text;
            oBE_DocumentoOrigen = oBL_DocumentoOrigen.ListarDO_AR(oBE_DocOrigen, 4);

            if (oBE_DocumentoOrigen.Count > 0)
            {
                lblRucFacturar.Text = oBE_DocumentoOrigen[0].sRazonSocial;
                txtRucFac.Text = oBE_DocumentoOrigen[0].sRuc;
                vi_idCliente.Value = Convert.ToString(oBE_DocumentoOrigen[0].iIdCliente);
            }
            else
            {
                lblRucFacturar.Text = "";
                //txtRucFac.Text = "";
                vi_idCliente.Value = "";
            }
        }

        void listarDroplist()
        {

            BL_DocumentoPago oBL_DocumentoPago = new BL_DocumentoPago();
            List<BE_DocumentoPago> oBE_DocumentoPago = new List<BE_DocumentoPago>();
            List<BE_Tabla> olst_TablaTipo = new List<BE_Tabla>();

            int i = 0;

            olst_TablaTipo = null;
            i = 0;
            DplBanco.Items.Clear();
            olst_TablaTipo = oBL_DocumentoPago.ListarBanco(0);
            DplBanco.Items.Add(new ListItem("[Seleccionar]", "-1"));
            for (i = 0; i < olst_TablaTipo.Count; ++i)
            {
                DplBanco.Items.Add(new ListItem(olst_TablaTipo[i].sBanco_Descripcion, olst_TablaTipo[i].sIdBanco.ToString()));
            }
            DplBanco.SelectedIndex = -1;



            //olst_TablaTipo = null;
            //i = 0;
            //DplFormaPago.Items.Clear();
            //olst_TablaTipo = oBL_DocumentoPago.ListarFormaDePago();
            //DplFormaPago.Items.Add(new ListItem("[Seleccionar]", "-1"));
            //for (i = 0; i < olst_TablaTipo.Count; ++i)
            //{
            //    DplFormaPago.Items.Add(new ListItem(olst_TablaTipo[i].sDescripcion, olst_TablaTipo[i].iIdValor.ToString()));
            //}
            //DplFormaPago.SelectedIndex = -1;



            olst_TablaTipo = null;
            i = 0;
            DplTipoCobro.Items.Clear();
            olst_TablaTipo = oBL_DocumentoPago.ListarTipoPago();
            DplTipoCobro.Items.Add(new ListItem("[Seleccionar]", "-1"));
            for (i = 0; i < olst_TablaTipo.Count; ++i)
            {
                if (olst_TablaTipo[i].iIdValor != 3)
                {
                    DplTipoCobro.Items.Add(new ListItem(olst_TablaTipo[i].sDescripcion, olst_TablaTipo[i].iIdValor.ToString()));
                }

            }
            DplTipoCobro.SelectedIndex = -1;





            olst_TablaTipo = null;
            i = 0;
            DplRegimen.Items.Clear();
            olst_TablaTipo = oBL_DocumentoPago.ListarRegimen();
            //DplRegimen.Items.Add(new ListItem("[Seleccionar]", "-1"));
            for (i = 0; i < olst_TablaTipo.Count; ++i)
            {
                DplRegimen.Items.Add(new ListItem(olst_TablaTipo[i].sRegimen_Descripcion, olst_TablaTipo[i].sIdRegimen.ToString()));
            }
            DplRegimen.SelectedIndex = 0;




            habilitarDesabilitar(false, 5, -1);
        }

        protected void DplTipoCobro_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (DplTipoCobro.SelectedIndex == 2)//credito
            {
                habilitarDesabilitar(true, 5, -1);
            }
            else //(DplTipoCobro.SelectedIndex == 2)//contado
            {
                habilitarDesabilitar(false, 5, -1);
            }

        }
        protected void btnGuardarAdj_Click(object sender, EventArgs e)
        {

            try
            {

                var validate = validateSiguienteBtn(1);

                if (!validate.Item1)
                {

                    txtMsjPop.Text = validate.Item2 + "<br/> ";
                    txtMsjPop.ForeColor = Color.Red;
                    mpLoading.Show();
                    btnCerrarPop.Visible = true;
                    return;
                }
                else
                {
                    txtMsjPop.Text = "Cargando ..!!!";
                    txtMsjPop.ForeColor = Color.Empty;
                    btnCerrarPop.Visible = false;
                }

                oBE_AutorizacionRetiro.idTipoCobro = Convert.ToInt32(DplTipoCobro.SelectedValue);
                if (DplTipoCobro.SelectedValue.Equals("2"))//contado
                {

                    oBE_AutorizacionRetiro.idFormaPago = Convert.ToInt32(DplFormaPago.SelectedValue);
                    oBE_AutorizacionRetiro.sBanco = DplBanco.SelectedValue;
                    oBE_AutorizacionRetiro.IdMoneda = Convert.ToInt32(DplMonedaPago.SelectedValue);
                    oBE_AutorizacionRetiro.sNumeroOperacion = txtNumeroOperacion.Text;

                    oBE_AutorizacionRetiro.dMontoPago = Convert.ToDecimal(txtMontoPago.Text);


                }

                oBE_AutorizacionRetiro.sObservacion = txtObservacion.Text;
                oBE_AutorizacionRetiro.sRuc = txtRucFac.Text.Trim();
                oBE_AutorizacionRetiro.IdSolicitudAre = Convert.ToInt32(vi_IdSolicitudAre.Value);
                oBE_AutorizacionRetiro.IIdDespachador = Convert.ToInt32(vi_idDespachador.Value);
                oBE_AutorizacionRetiro.SUsuario = ViewState["usuario"].ToString();

                var result = oBL_AutorizacionRetiro.Ext_SO_AutorizacionRetiro(oBE_AutorizacionRetiro, 4);



                vi_IdSolicitudAre.Value = result.Item2;


                if (chkLigado.Checked == true)
                {
                    oBE_AutorizacionRetiro = new BE_AutorizacionRetiro();

                    foreach (GridViewRow GrvRow in grvListadoAdd.Rows)
                    {
                        oBE_AutorizacionRetiro.IdSolicitudAre = Convert.ToInt32(result.Item2);
                        oBE_AutorizacionRetiro.idDocOri = Convert.ToInt32(grvListadoAdd.DataKeys[GrvRow.RowIndex].Values["iIdDocOri"]);
                        oBE_AutorizacionRetiro.idLiquidacion = Convert.ToInt32(grvListadoAdd.DataKeys[GrvRow.RowIndex].Values["idliquidacion"]);
                        oBE_AutorizacionRetiro.iIdCliente = Convert.ToInt32(grvListadoAdd.DataKeys[GrvRow.RowIndex].Values["iIdCliente"]);
                        oBE_AutorizacionRetiro.sDuaAnio = grvListadoAdd.DataKeys[GrvRow.RowIndex].Values["sDuaAnio"].ToString();
                        oBE_AutorizacionRetiro.IdRegimen = Convert.ToInt32(grvListadoAdd.DataKeys[GrvRow.RowIndex].Values["sRegimen"].ToString());
                        oBE_AutorizacionRetiro.sDuaNumero = grvListadoAdd.DataKeys[GrvRow.RowIndex].Values["sDuaNumero"].ToString();

                        oBE_AutorizacionRetiro.SUsuario = ViewState["usuario"].ToString();
                        var result1 = oBL_AutorizacionRetiro.Ext_SO_AutorizacionRetiro(oBE_AutorizacionRetiro, 13);
                    }



                }

                //TabContainer1.ActiveTabIndex = 0;
                habilitarDesabilitar(false, 3, 0);


                //string msj = "Solicitud Gnerada N°: " + result.Item2;
                //MensajeScript("hola");
                Response.Redirect("eFENIX_SolicitudARE_Listado.aspx?id=" + result.Item2);

            }
            catch (Exception ex)
            {


            }


        }
        /**/
        #endregion


        #region "Solicitar Renovacion"

        protected void btnRenovacion_Click(object sender, EventArgs e)
        {
   
            tbPanelBusqueda.Enabled = false;
            TabContainer1.ActiveTabIndex = 2;
            tbRenovacion.Enabled = true;
            //mpRenovacion.Show();
            cargarCombo();
        }
        protected void DplTipoCobroR_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (DplTipoCobroR.SelectedValue.Equals("2"))//credito
            {
                habilitarDesabilitar(true, 6, -1);
            }
            //else if (DplTipoCobroR.SelectedValue.Equals("3"))//credito
            //{
            //    habilitarDesabilitar(false, 6, -1);
            //}
            else //(DplTipoCobro.SelectedIndex == 2)//contado
            {
                habilitarDesabilitar(false, 6, -1);
            }

        }
        protected void imgCerrarPopRenovac_Click(object sender, ImageClickEventArgs e)
        {
            //mpRenovacion.Hide();
        }

        protected void btnSolicitarRenovacion_Click(object sender, EventArgs e)
        {

            if (lblAutRet.Text.Equals("0"))
            {
                mensajePop("Ingrese Autorizacion Retiro!<br/> ", Color.Red);
                return;
            }


            BL_AutorizacionRetiro oBL_AutorizacionRetiro = new BL_AutorizacionRetiro();
            List<BE_AutorizacionRetiro> oBE_AutorizacionRetiroList = new List<BE_AutorizacionRetiro>();
            BE_AutorizacionRetiro oBE_AutorizacionRetiro = new BE_AutorizacionRetiro();



            if (DplTipoCobroR.SelectedValue.Equals("1"))//credito
            {
                oBE_AutorizacionRetiro.sAutRetNumero = lblAutRet.Text.Trim();
                oBE_AutorizacionRetiro.SUsuario = GlobalEntity.Instancia.Usuario;
                oBE_AutorizacionRetiro.sObservacion = txtObservacionR.Text;
                oBE_AutorizacionRetiro.idTipoCobro = Convert.ToInt32(DplTipoCobroR.SelectedValue);


                var res = oBL_AutorizacionRetiro.Ext_SO_AutorizacionRetiro(oBE_AutorizacionRetiro, 17);

                mensajePop("Solicitud de Renovacion Generada<br/> ", Color.Green);
            }
            else if (DplTipoCobroR.SelectedValue.Equals("2"))//contado
            {


                var result = cargarArchivo(FileUploadMedioPagoR, IdAgente.ToString(), "PG");


                if (result.Item2 == 1|| vl_sRuta.Value.Length>0)
                {
                    if(vl_sRuta.Value.Length > 0)
                    {
                       
                    }
                    else if(result.Item2==1)
                    {
                        vl_sRuta.Value = result.Item1;
                        lblNombreArchivoR.Text= result.Item1;
                    }
                   

                    if (DplTipoCobroR.SelectedValue.Equals("2"))//contado
                    {
                        string mensaje = "";
                        Boolean isValid = true;

                        if (DplBancoR.SelectedValue.Equals("-1"))
                        {
                            mensaje = mensaje + " Ingrese el Banco <br/>";
                            DplBancoR.Focus();
                            isValid = false;
                        }
                        if (DplFormaPagoR.SelectedValue.Equals("-1"))
                        {
                            mensaje = mensaje + " Ingrese la Forma de Pago <br/>";
                            DplFormaPagoR.Focus();
                            isValid = false;
                        }
                        if (DplMonedaPagoR.SelectedValue.Equals("-1"))
                        {
                            mensaje = mensaje + " Ingrese la Moneda <br/>";
                            DplMonedaPagoR.Focus();
                            isValid = false;
                        }
                        if (string.IsNullOrEmpty(txtMontoPagoR.Text))
                        {
                            mensaje = mensaje + " Ingrese el Monto Pago <br/>";
                            txtMontoPagoR.Focus();
                            isValid = false;
                        }
                        if (string.IsNullOrEmpty(txtNumeroOperacionR.Text))
                        {
                            mensaje = mensaje + " Ingrese el Numero Operacion <br/>";
                            txtNumeroOperacionR.Focus();
                            isValid = false;
                        }

                        if (isValid==false)
                        {
                            mensajePop(mensaje, Color.Red);
                            return;
                        }
                    }


                    //lblUplMedioPago.Text = FileUploadMedioPagoR.FileName;

                    oBE_AutorizacionRetiro.idTipoCobro = Convert.ToInt32(DplTipoCobroR.SelectedValue);

                    if (DplTipoCobroR.SelectedValue.Equals("2"))//contado
                    {

                        oBE_AutorizacionRetiro.idFormaPago = Convert.ToInt32(DplFormaPagoR.SelectedValue);
                        oBE_AutorizacionRetiro.sBanco = DplBancoR.SelectedValue;
                        oBE_AutorizacionRetiro.IdMoneda = Convert.ToInt32(DplMonedaPagoR.SelectedValue);
                        oBE_AutorizacionRetiro.sNumeroOperacion = txtNumeroOperacionR.Text;
                        oBE_AutorizacionRetiro.sObservacion = txtObservacionR.Text;
                        oBE_AutorizacionRetiro.dMontoPago = Convert.ToDecimal(txtMontoPagoR.Text);


                    }
                    oBE_AutorizacionRetiro.sObservacion = txtObservacionR.Text;
                    oBE_AutorizacionRetiro.sAutRetNumero = lblAutRet.Text.Trim();
                    oBE_AutorizacionRetiro.iTipoDocAdjuntado = 5;
                    

                    if (vl_sRuta.Value.Length > 0)
                    {
                        oBE_AutorizacionRetiro.sRuta =  vl_sRuta.Value;
                    }
                    else
                    {
                        oBE_AutorizacionRetiro.sRuta = result.Item1;
                    }

                   
                    oBE_AutorizacionRetiro.SUsuario = GlobalEntity.Instancia.Usuario;

                    var res = oBL_AutorizacionRetiro.Ext_SO_AutorizacionRetiro(oBE_AutorizacionRetiro, 17);


                    mensajePop("Solicitud de Renovacion Generada<br/> ", Color.Red);

                }
                else if (result.Item2 == 2)
                {
                    mensajePop("Ingrese un archivo en Formato PDF<br/> ", Color.Red);
                }
                else if (result.Item2 == 0)
                {
                    mensajePop("Adjunte Medio de Pago<br/> ", Color.Red);
                }

            }
            else if (DplTipoCobroR.SelectedValue.Equals("3"))
            {
                oBE_AutorizacionRetiro.sAutRetNumero = lblAutRet.Text.Trim();
                oBE_AutorizacionRetiro.SUsuario = GlobalEntity.Instancia.Usuario;
                oBE_AutorizacionRetiro.idFormaPago = Convert.ToInt32(DplTipoCobroR.SelectedValue);
                oBE_AutorizacionRetiro.idTipoCobro = Convert.ToInt32(DplTipoCobroR.SelectedValue);

                var res = oBL_AutorizacionRetiro.Ext_SO_AutorizacionRetiro(oBE_AutorizacionRetiro, 17);

                mensajePop("Solicitud de Renovacion Generada<br/> ", Color.Green);
            }
            else
            {
                mensajePop("Seleccione tipo de pago<br/> ", Color.Red);
            }

        }
        protected void imbBtnBuscarAutRet_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(TxtNumeroPO.Text.Trim())){
                    lblAutRet.Text = "0";
                    mensajePop("Ingrese un Numero de Autorizacion Retiro<br/> ", Color.Red);
                }

                oBE_AutorizacionRetiro = oBL_AutorizacionRetiro.ListarProcesoPorID(TxtNumeroPO.Text, IdAgente);
                TxtIdAutRet.Text = oBE_AutorizacionRetiro.IIdAutRet.ToString();
                TxtEstadoVigenciaPO.Text = oBE_AutorizacionRetiro.sEstadoVigencia;
                TxtSituacionPO.Text = oBE_AutorizacionRetiro.SSituacion;

                txtFechaCreacion.Text = oBE_AutorizacionRetiro.dFechaCreacion.ToString();
                txtFechaHabilitado.Text = oBE_AutorizacionRetiro.dFechaHabilitado.ToString();
                TxtFechaVigenciaMaximaPop.Text = oBE_AutorizacionRetiro.dFechaMaxVigencia.ToString();

                lblAutRet.Text = TxtNumeroPO.Text;


                if (oBE_AutorizacionRetiro.SSituacion == null)
                {
                    fielPagoRenovacion.Visible = false;
                    lblAutRet.Text = "0";
                    return;
                }

                if (oBE_AutorizacionRetiro.SSituacion.Equals("Atendido Total"))
                {
                    fielPagoRenovacion.Visible = false;
                    lblAutRet.Text = "0";
                    return;
                }
                else
                {
                    fielPagoRenovacion.Visible = true;
                    return;
                }
            }
            catch 
            {
                fielPagoRenovacion.Visible = false;
                lblAutRet.Text = "0";
                return;

            }



            //mpRenovacion.Show();
        }
        void cargarCombo()
        {
            BL_DocumentoPago oBL_DocumentoPago = new BL_DocumentoPago();
            List<BE_DocumentoPago> oBE_DocumentoPago = new List<BE_DocumentoPago>();
            List<BE_Tabla> olst_TablaTipo = new List<BE_Tabla>();

            int i = 0;

            olst_TablaTipo = null;
            i = 0;
            DplBancoR.Items.Clear();
            olst_TablaTipo = oBL_DocumentoPago.ListarBanco(0);
            DplBancoR.Items.Add(new ListItem("[Seleccionar]", "-1"));
            for (i = 0; i < olst_TablaTipo.Count; ++i)
            {
                DplBancoR.Items.Add(new ListItem(olst_TablaTipo[i].sBanco_Descripcion, olst_TablaTipo[i].sIdBanco.ToString()));
            }
            DplBancoR.SelectedIndex = -1;


            olst_TablaTipo = null;
            i = 0;
            DplTipoCobroR.Items.Clear();
            olst_TablaTipo = oBL_DocumentoPago.ListarTipoPago();
            DplTipoCobroR.Items.Add(new ListItem("[Seleccionar]", "-1"));
            DplTipoCobroR.Items.Add(new ListItem("Sin Pago", "3"));
            for (i = 0; i < olst_TablaTipo.Count; ++i)
            {
                if (olst_TablaTipo[i].iIdValor != 3)
                {
                    DplTipoCobroR.Items.Add(new ListItem(olst_TablaTipo[i].sDescripcion, olst_TablaTipo[i].iIdValor.ToString()));
                }

            }
            DplTipoCobroR.SelectedIndex = -1;

            habilitarDesabilitar(false, 6, -1);
            fielPagoRenovacion.Visible = false;
        }
        #endregion




        void mensajePop(string smsj, Color colrs)
        {
            txtMsjPop.Text = smsj;
            txtMsjPop.ForeColor = colrs;
            mpLoading.Show();
            btnCerrarPop.Visible = true;
            return;
        }





        void habilitarDesabilitar(Boolean pValor, int accion, int tipo)
        {

            if (accion == 2)
            {
                TxtDocumentoM.Enabled = pValor;
                dplAnio.Enabled = pValor;
                DplRegimen.Enabled = pValor;
                txtDua.Enabled = pValor;

                btnSiguiente.Enabled = pValor;
                BtnConsultaDo.Enabled = pValor;
                btnSiguiente2.Enabled = pValor;
                imgBtnAgregarGrilla.Enabled = pValor;
                chkLigado.Enabled = pValor;


                FileUploadBL.Enabled = true;
                FileUploadEndoseLN.Enabled = true;
                FileUploadSenasa.Enabled = true;
                FileUploadMedioPago.Enabled = true;

            }
            if (tipo == 2)
            {
                txtCliente.Text = "";
                txtAgenciaAduana.Text = "";
                txtLinea.Text = "";
                txtResultadoLevante.Text = "";
                txtLiquidacion.Text = "";
                txtResultadoLevante.Text = "";
                txtMontoLiquidacion.Text = "";


                dplAnio.BorderColor = Color.Empty;

                txtDua.BorderColor = Color.Empty;
                txtLiquidacion.BorderColor = Color.Empty;


                oBE_DocumentoOrigen.Add(oBE_DocOrigen);
                grvListadoSolicitud.DataSource = oBE_DocumentoOrigen;
                grvListadoSolicitud.DataBind();
            }
            if (accion == 3)
            {
                txtDniDespachador.Enabled = pValor;
                btnDespachador.Enabled = pValor;
                btnGuardarAdj.Enabled = pValor;

                DplTipoCobro.Enabled = pValor;
                DplFormaPago.Enabled = pValor;
                DplBanco.Enabled = pValor;
                DplMonedaPago.Enabled = pValor;
                txtNumeroOperacion.Enabled = pValor;
                txtObservacion.Enabled = pValor;
                lblFormaPago.Enabled = pValor;
                lblBanco.Enabled = pValor;
                lblComentarios.Enabled = pValor;
                lblMoneda.Enabled = pValor;
                lblNoperacion.Enabled = pValor;


                UploadIconoEnabled(1, 3);//desabilita todo

            }

            if (accion == 4)
            {
                txtDniDespachador.Enabled = pValor;
                btnDespachador.Enabled = pValor;
                btnGuardarAdj.Enabled = pValor;

                TxtDocumentoM.Enabled = pValor;
                dplAnio.Enabled = pValor;
                DplRegimen.Enabled = pValor;
                txtDua.Enabled = pValor;

                btnSiguiente.Enabled = pValor;
                BtnConsultaDo.Enabled = pValor;

                txtDniDespachador.Text = "";
                TxtDocumentoM.Text = "";


                txtDua.Text = "";

                txtCliente.Text = "";
                txtAgenciaAduana.Text = "";
                txtLinea.Text = "";
                txtResultadoLevante.Text = "";
                txtLiquidacion.Text = "";


                lblUplBLID.Text = "";
                lblUplEndoseLNID.Text = "";
                lblUplSenasaID.Text = "";
                lblUplMedioPagoID.Text = "";



                txtDua.BorderColor = Color.Empty;
                txtLiquidacion.BorderColor = Color.Empty;

                oBE_DocumentoOrigen.Add(oBE_DocOrigen);
                grvListadoSolicitud.DataSource = oBE_DocumentoOrigen;
                grvListadoSolicitud.DataBind();

                DplTipoCobro.Enabled = pValor;

                if (pValor == true)
                {
                    pValor = false;

                    DplFormaPago.Enabled = pValor;
                    DplBanco.Enabled = pValor;
                    DplMonedaPago.Enabled = pValor;
                    txtNumeroOperacion.Enabled = pValor;
                    txtObservacion.Enabled = pValor;
                    lblFormaPago.Enabled = pValor;
                    lblBanco.Enabled = pValor;
                    lblComentarios.Enabled = pValor;
                    lblMoneda.Enabled = pValor;
                    lblNoperacion.Enabled = pValor;


                    DplFormaPago.SelectedIndex = 0;
                    DplBanco.SelectedIndex = 0;
                    DplMonedaPago.SelectedIndex = 0;
                    DplTipoCobro.SelectedIndex = 0;

                    DplFormaPago.Visible = pValor;
                    DplBanco.Visible = pValor;
                    DplMonedaPago.Visible = pValor;
                    txtNumeroOperacion.Visible = pValor;
                    txtObservacion.Visible = pValor;
                    lblFormaPago.Visible = pValor;
                    lblBanco.Visible = pValor;
                    lblComentarios.Visible = pValor;
                    lblMoneda.Visible = pValor;
                    lblNoperacion.Visible = pValor;

                    pValor = true;
                }


                UploadIconoEnabled(2, 3);//Nuevo Registro


            }

            if (accion == 5)//datos de pago
            {
                DplFormaPago.Visible = pValor;
                DplBanco.Visible = pValor;
                DplMonedaPago.Visible = pValor;
                txtNumeroOperacion.Visible = pValor;
                //txtObservacion.Visible = pValor;
                lblFormaPago.Visible = pValor;
                lblBanco.Visible = pValor;
                //lblComentarios.Visible = pValor;
                lblMoneda.Visible = pValor;
                lblNoperacion.Visible = pValor;

                DplFormaPago.Enabled = pValor;
                DplBanco.Enabled = pValor;
                DplMonedaPago.Enabled = pValor;

                txtNumeroOperacion.Enabled = pValor;
                // txtObservacion.Enabled = pValor;
                txtMontoPago.Visible = pValor;
                lblMontoPago.Visible = pValor;
            }


            if (accion == 6)//datos de pago-Renovacion
            {
                DplFormaPagoR.Visible = pValor;
                DplBancoR.Visible = pValor;
                DplMonedaPagoR.Visible = pValor;
                txtNumeroOperacionR.Visible = pValor;
                //txtObservacion.Visible = pValor;
                lblFormaPagoR.Visible = pValor;
                lblBancoR.Visible = pValor;
                //lblComentarios.Visible = pValor;
                lblMonedaR.Visible = pValor;
                lblNoperacionR.Visible = pValor;

                DplFormaPagoR.Enabled = pValor;
                DplBancoR.Enabled = pValor;
                DplMonedaPagoR.Enabled = pValor;

                txtNumeroOperacionR.Enabled = pValor;
                // txtObservacion.Enabled = pValor;
                txtMontoPagoR.Visible = pValor;
                lblMontoPagoR.Visible = pValor;
                FileUploadMedioPagoR.Visible = pValor;
            }

        }


        class levanteImput
        {
            public string anio { get; set; }
            public string nroDua { get; set; }
        }
        class VerificarLevanteDua
        {
            public string StatusCode { get; set; }
            public string fechaLevante { get; set; }
            public string BlCode { get; set; }

        }
    }

}


