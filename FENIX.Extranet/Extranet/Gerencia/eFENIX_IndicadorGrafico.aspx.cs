﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Telerik.Charting;
using Telerik.Charting.Styles;
using System.Drawing;
using System.Collections.Generic;
using FENIX.BusinessEntity;
using FENIX.BusinessLogic;

//using Infragistics.UltraChart.Resources.Appearance;
//using Infragistics.UltraChart.Core;
//using Infragistics.UltraChart.Core.Primitives;
//using Infragistics.UltraChart.Core.Util;
//using Infragistics.UltraChart.Shared.Styles;
//using Infragistics.UltraChart.Shared.Events;
//using Infragistics.UltraChart.Core.Layers;

public partial class Extranet_Gerencia_eFENIX_IndicadorGrafico : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        String tiempo = (Convert.ToInt32(GlobalEntity.Instancia.TiempoDuracionSesion.ToString()) * 1000 * 60).ToString();
        Session["Reset"] = true;

        if (!Page.IsPostBack)
        {
            ListarDropDowList();

            if (DateTime.Now.Month == 1)
            {
                DplAnioPeriodo.SelectedValue = (DateTime.Now.Year - 1).ToString();
                //DplMesPeriodo.SelectedValue = "12";
            }
            else
            {
                DplAnioPeriodo.SelectedValue = DateTime.Now.Year.ToString();
                //DplMesPeriodo.SelectedValue = (DateTime.Now.Month - 1).ToString();
            }

            ScriptManager.RegisterStartupScript(this, GetType(), "mensaje", "SessionExpireAlert(" + tiempo + ");", true);
        }
        else
            ScriptManager.RegisterStartupScript(this, GetType(), "mensaje", "myStopFunction();", true);
    }

    protected void ListarDropDowList()
    {
        try
        {
            DplAnioPeriodo.Items.Clear();
            for (int i = 2014; i <= DateTime.Now.Year; i++)
            {
                DplAnioPeriodo.Items.Add(new ListItem(i.ToString(), i.ToString()));
            }
            DplAnioPeriodo.SelectedIndex = 0;

            DplNegocio.Items.Clear();
            //DplNegocio.Items.Add(new ListItem("Venta Acumulada (S/.)", "1"));
            DplNegocio.Items.Add(new ListItem("Utilidad Bruta (S/.)", "2"));
            DplNegocio.Items.Add(new ListItem("Utilidad x m2 (S/.)", "3"));
            DplNegocio.Items.Add(new ListItem("Rotación (Días)", "4"));
            DplNegocio.Items.Add(new ListItem("Ocupabilidad (Porcentaje)", "5"));

            DplSemestre.Items.Clear();
            DplSemestre.Items.Add(new ListItem("1er Semestre", "1"));
            DplSemestre.Items.Add(new ListItem("2do Semestre", "2"));
        }
        catch (Exception e)
        {

        }
    }

    protected void ImgBtnBuscar_Click(object sender, ImageClickEventArgs e)
    {
        DvGraficoPie.Visible = false;
        DvGraficoBar.Visible = false;

        List<BE_Indicador> LstIndicador = new List<BE_Indicador>();
        BL_Indicador oBL_Indicador = new BL_Indicador();
        BE_Indicador oBE_Indicador = new BE_Indicador();

        oBE_Indicador.iAnho = Convert.ToInt32(DplAnioPeriodo.SelectedValue);
        oBE_Indicador.iMes = Convert.ToInt32(DplSemestre.SelectedValue);
        oBE_Indicador.sUsuario = "CSORIANO";//GlobalEntity.Instancia.Usuario.ToString();
        oBE_Indicador.iAgrupacion = Convert.ToInt32(DplNegocio.SelectedValue);

        LstIndicador = oBL_Indicador.ListarGraficoTotal(oBE_Indicador);

        if ((DplNegocio.SelectedValue == "2") || (DplNegocio.SelectedValue == "3"))
        {
            if (RblModoGrafico.Items[0].Selected)
            {
                InitRadChartPie(LstIndicador);
                DvGraficoPie.Visible = true;
            }
            else
            {
                InitRadChart(LstIndicador);
                DvGraficoBar.Visible = true;
            }
        }
        else if ((DplNegocio.SelectedValue == "4") || (DplNegocio.SelectedValue == "5"))
        {
            InitRadChart(LstIndicador);
            DvGraficoBar.Visible = true;
        }

        //InitChart(LstIndicador);
    }

    protected String NombreMes(int pMes)
    {
        String NombreMes = String.Empty;

        switch (pMes)
        {
            case 1: { NombreMes = "Enero"; break; }
            case 2: { NombreMes = "Febrero"; break; }
            case 3: { NombreMes = "Marzo"; break; }
            case 4: { NombreMes = "Abril"; break; }
            case 5: { NombreMes = "Mayo"; break; }
            case 6: { NombreMes = "Junio"; break; }
            case 7: { NombreMes = "Julio"; break; }
            case 8: { NombreMes = "Agosto"; break; }
            case 9: { NombreMes = "Setiembre"; break; }
            case 10: { NombreMes = "Octubre"; break; }
            case 11: { NombreMes = "Noviembre"; break; }
            case 12: { NombreMes = "Diciembre"; break; }
        }

        return NombreMes;
    }

    //Primer Gráfico
    void InitRadChartPie(List<BE_Indicador> LstIndicador)
    {
        RadChart2.Visible = true;

        ViewState["Ls_Indicador"] = LstIndicador;

        RadChart2.Clear();
        RadChart2.ChartTitle.TextBlock.Text = DplNegocio.SelectedItem.Text + " - " + DplSemestre.SelectedItem.Text + " (" + RblModoGrafico.SelectedItem.Text + ")";
        RadChart2.Skin = "Colorful";// "LightGreen";

        // SETTING AXISES 
        //SetXAxisPie(LstIndicador);
        RadChart2.PlotArea.XAxis.LayoutMode = ChartAxisLayoutMode.Between;
        RadChart2.PlotArea.YAxis.AutoScale = false;

        ChartSeriesType TipoGrafico;
        TipoGrafico = ChartSeriesType.Pie;

        // SETTING SERIES
        ChartSeries series1 = new ChartSeries("Carga Contenedor", TipoGrafico);
        series1.Appearance.LegendDisplayMode = ChartSeriesLegendDisplayMode.ItemLabels;
        
        SetSeriesAppearancePie(series1);
        RadChart2.Series.Add(series1);

        Double SumaPorc = 0;
        Int32 i = 1;

        foreach (BE_Indicador item in LstIndicador)
        {
            Double Valor = 0, MesPorc = 0;
            
            Valor = Convert.ToDouble(item.dMes);
            MesPorc = Convert.ToDouble(item.dMesPorc);

            if (i == 3)
                if (SumaPorc + MesPorc > 0 )
                    if (SumaPorc + MesPorc != 100)
                        MesPorc = 100 - SumaPorc;

            SumaPorc = SumaPorc + MesPorc;

            series1.AddItem(Valor, Valor.ToString("###,###,###") + " - (" + MesPorc.ToString() + "%)");            

            i++;
        }

        series1.Items[0].Name = "Dep. Temp. Contenedor";
        series1.Items[0].Appearance.FillStyle.MainColor = Color.SteelBlue;
        series1.Items[0].Appearance.FillStyle.FillType = FillType.Solid;

        series1.Items[1].Name = "Dep. Temp. Carga Suelta y Rodante";
        series1.Items[1].Appearance.FillStyle.MainColor = Color.IndianRed;
        series1.Items[1].Appearance.FillStyle.FillType = FillType.Solid;

        series1.Items[2].Name = "Dep. Aduanero y Alm. Simple";
        series1.Items[2].Appearance.FillStyle.MainColor = Color.ForestGreen;
        series1.Items[2].Appearance.FillStyle.FillType = FillType.Solid;

        //SetYAxis(Convert.ToInt32(ValorMin), Convert.ToInt32(ValorMax));

        //foreach (LabelItem legendItem in RadChart1.Legend.Items)
        //{
        //    legendItem.ActiveRegion.Tooltip = "Click here";
        //}
    }

    static void SetSeriesAppearancePie(ChartSeries series)
    {
        series.Appearance.Border.Color = Color.Black;
        series.Appearance.LabelAppearance.Border.Color = Color.Black;
        series.Appearance.LabelAppearance.FillStyle.MainColor = Color.White;
        series.Appearance.TextAppearance.TextProperties.Font = new Font("Arial", 9);
        series.Appearance.TextAppearance.TextProperties.Color = Color.Black;
        series.Appearance.LabelAppearance.Dimensions.Margins = new ChartMargins(2);
        series.DefaultLabelValue = "#Y{N0}";
        series.Appearance.BarWidthPercent = 30;
    }

    protected void RadChart2_Click(object sender, ChartClickEventArgs e)
    {
        List<BE_Indicador> LstIndicador = new List<BE_Indicador>();

        if (ViewState["Ls_Indicador"] != null)
        {
            LstIndicador = (List<BE_Indicador>)ViewState["Ls_Indicador"];
        }
        else
        {
            ViewState["Ls_Indicador"] = LstIndicador;
        }
    }

    //Segundo Gráfico
    void InitRadChart(List<BE_Indicador> LstIndicador)
    {
        //Control ocultar y mostrar columnas del detalle
        int iMes = 0;
        if (DplSemestre.SelectedValue == "1")
            iMes = 1;
        else
            iMes = 7;

        GrvDetalle.Columns[0].HeaderText = DplNegocio.SelectedItem.Text;
        if ((DplNegocio.SelectedValue == "2") || (DplNegocio.SelectedValue == "3"))
            RadChart1.ChartTitle.TextBlock.Text = DplNegocio.SelectedItem.Text + " - " + DplSemestre.SelectedItem.Text + " (" + RblModoGrafico.SelectedItem.Text + ")";
        else
            RadChart1.ChartTitle.TextBlock.Text = DplNegocio.SelectedItem.Text + " - " + DplSemestre.SelectedItem.Text;        

        for (int iCol = 1; iCol <= 6; iCol++)
        {
            GrvDetalle.Columns[iCol].HeaderText = NombreMes(iMes);
            iMes = iMes + 1;
        }

        LstIndicador[0].sNombreIndicador = "Dep. Temp. Contenedor";
        LstIndicador[1].sNombreIndicador = "Dep. Temp. Carga Suelta y Rodante";
        if ((DplNegocio.SelectedValue == "1") || (DplNegocio.SelectedValue == "2") || (DplNegocio.SelectedValue == "3"))
        {
            LstIndicador[2].sNombreIndicador = "Dep. Aduanero y Alm. Simple";
        }
        else if (DplNegocio.SelectedValue == "5")
        {
            LstIndicador[2].sNombreIndicador = "Dep. Aduanero y Alm. Simple (Patio)";
            LstIndicador[3].sNombreIndicador = "Dep. Aduanero y Alm. Simple (Hangar)";
        }

        GrvDetalle.DataSource = LstIndicador;
        GrvDetalle.DataBind();

        //if (DplNegocio.SelectedValue == "5")
        //    GrvDetalle.Visible = true;
        //else
        //    GrvDetalle.Visible = false;

        RadChart1.Visible = true;

        RadChart1.Clear();

        RadChart1.Skin = "Colorful";

        Int32 CantidadMes = 6;

        // SETTING AXISES 
        SetXAxis(CantidadMes);
        RadChart1.PlotArea.XAxis.LayoutMode = ChartAxisLayoutMode.Between;
        RadChart1.PlotArea.YAxis.AutoScale = false;

        ChartSeriesType TipoGrafico;
        if ((DplNegocio.SelectedValue == "2") || (DplNegocio.SelectedValue == "3"))
            TipoGrafico = ChartSeriesType.Bar;
        else //((DplNegocio.SelectedValue == "4") || (DplNegocio.SelectedValue == "5"))
            TipoGrafico = ChartSeriesType.Line;

        // SETTING SERIES 
        ChartSeries series1 = new ChartSeries("Dep. Temp. Contenedor", TipoGrafico);
        SetSeriesAppearance(series1);
        RadChart1.Series.Add(series1);
        series1.Appearance.FillStyle.MainColor = Color.SteelBlue;
        series1.Appearance.FillStyle.FillType = FillType.Solid;

        ChartSeries series2 = new ChartSeries("Dep. Temp. Carga Suelta y Rodante", TipoGrafico);
        SetSeriesAppearance(series2);
        RadChart1.Series.Add(series2);
        series2.Appearance.FillStyle.MainColor = Color.IndianRed;
        series2.Appearance.FillStyle.FillType = FillType.Solid;

        ChartSeries series3;
        if (DplNegocio.SelectedValue == "5")
            series3 = new ChartSeries("Dep. Aduanero y Alm. Simple (Patio)", TipoGrafico);
        else
            series3 = new ChartSeries("Dep. Aduanero y Alm. Simple", TipoGrafico);

        ChartSeries series4 = new ChartSeries("Dep. Aduanero y Alm. Simple (Hangar)", TipoGrafico);

        if (DplNegocio.SelectedValue != "4")
        {
            SetSeriesAppearance(series3);
            RadChart1.Series.Add(series3);
            series3.Appearance.FillStyle.MainColor = Color.ForestGreen;
            series3.Appearance.FillStyle.FillType = FillType.Solid;
        }

        if (DplNegocio.SelectedValue == "5")
        {
            SetSeriesAppearance(series4);
            RadChart1.Series.Add(series4);
            series4.Appearance.FillStyle.MainColor = Color.Purple;
            series4.Appearance.FillStyle.FillType = FillType.Solid;
        }

        if (DplNegocio.SelectedValue == "5")
        {
            series1.Appearance.LabelAppearance.FillStyle.MainColor = Color.Transparent;
            series2.Appearance.LabelAppearance.FillStyle.MainColor = Color.Transparent;
            series3.Appearance.LabelAppearance.FillStyle.MainColor = Color.Transparent;
            series4.Appearance.LabelAppearance.FillStyle.MainColor = Color.Transparent;

            //series1.Appearance.TextAppearance.TextProperties.Color = Color.Transparent;
            //series2.Appearance.TextAppearance.TextProperties.Color = Color.Transparent;
            //series3.Appearance.TextAppearance.TextProperties.Color = Color.Transparent;
            //series4.Appearance.TextAppearance.TextProperties.Color = Color.Transparent;

            GrvDetalle.Columns[7].Visible = false;
        }

        Double ValorMax = 0, ValorMin = 1000;

        #region Barrido

        foreach (BE_Indicador item in LstIndicador)
        {
            Double Valor = 0;
            //Serie 1
            if ((item.iIdIndicador == 27) || (item.iIdIndicador == 29) || (item.iIdIndicador == 1) || (item.iIdIndicador == 2))
            {
                for (int i = 1; i <= CantidadMes; i++)
                {
                    if (i == 1)
                    {
                        Valor = Convert.ToDouble(item.dMes8);
                        series1.AddItem(Valor);

                        if (Valor > ValorMax)
                            ValorMax = Valor;

                        if (Valor < ValorMin)
                            ValorMin = Valor;
                    }

                    if (i == 2)
                    {
                        Valor = Convert.ToDouble(item.dMes9);
                        series1.AddItem(Valor);

                        if (Valor > ValorMax)
                            ValorMax = Valor;

                        if (Valor < ValorMin)
                            ValorMin = Valor;
                    }

                    if (i == 3)
                    {
                        Valor = Convert.ToDouble(item.dMes10);
                        series1.AddItem(Valor);

                        if (Valor > ValorMax)
                            ValorMax = Valor;

                        if (Valor < ValorMin)
                            ValorMin = Valor;
                    }

                    if (i == 4)
                    {
                        Valor = Convert.ToDouble(item.dMes11);
                        series1.AddItem(Valor);

                        if (Valor > ValorMax)
                            ValorMax = Valor;

                        if (Valor < ValorMin)
                            ValorMin = Valor;
                    }

                    if (i == 5)
                    {
                        Valor = Convert.ToDouble(item.dMes12);
                        series1.AddItem(Valor);

                        if (Valor > ValorMax)
                            ValorMax = Valor;

                        if (Valor < ValorMin)
                            ValorMin = Valor;
                    }

                    if (i == 6)
                    {
                        Valor = Convert.ToDouble(item.dMes13);
                        series1.AddItem(Valor);

                        if (Valor > ValorMax)
                            ValorMax = Valor;

                        if (Valor < ValorMin)
                            ValorMin = Valor;
                    }
                }
            }

            //Serie 2 
            if ((item.iIdIndicador == 32) || (item.iIdIndicador == 34) || (item.iIdIndicador == 23) || (item.iIdIndicador == 25))
            {
                for (int i = 1; i <= CantidadMes; i++)
                {
                    if (i == 1)
                    {
                        Valor = Convert.ToDouble(item.dMes8);
                        series2.AddItem(Valor);

                        if (Valor > ValorMax)
                            ValorMax = Valor;

                        if (Valor < ValorMin)
                            ValorMin = Valor;
                    }

                    if (i == 2)
                    {
                        Valor = Convert.ToDouble(item.dMes9);
                        series2.AddItem(Valor);

                        if (Valor > ValorMax)
                            ValorMax = Valor;

                        if (Valor < ValorMin)
                            ValorMin = Valor;
                    }

                    if (i == 3)
                    {
                        Valor = Convert.ToDouble(item.dMes10);
                        series2.AddItem(Valor);

                        if (Valor > ValorMax)
                            ValorMax = Valor;

                        if (Valor < ValorMin)
                            ValorMin = Valor;
                    }

                    if (i == 4)
                    {
                        Valor = Convert.ToDouble(item.dMes11);
                        series2.AddItem(Valor);

                        if (Valor > ValorMax)
                            ValorMax = Valor;

                        if (Valor < ValorMin)
                            ValorMin = Valor;
                    }

                    if (i == 5)
                    {
                        Valor = Convert.ToDouble(item.dMes12);
                        series2.AddItem(Valor);

                        if (Valor > ValorMax)
                            ValorMax = Valor;

                        if (Valor < ValorMin)
                            ValorMin = Valor;
                    }

                    if (i == 6)
                    {
                        Valor = Convert.ToDouble(item.dMes13);
                        series2.AddItem(Valor);

                        if (Valor > ValorMax)
                            ValorMax = Valor;

                        if (Valor < ValorMin)
                            ValorMin = Valor;
                    }
                }
            }

            if (DplNegocio.SelectedValue != "4")
            {
                //Serie 3
                if ((item.iIdIndicador == 49) || (item.iIdIndicador == 46) || (item.iIdIndicador == 51))
                {
                    for (int i = 1; i <= CantidadMes; i++)
                    {
                        if (i == 1)
                        {
                            Valor = Convert.ToDouble(item.dMes8);
                            series3.AddItem(Valor);

                            if (Valor > ValorMax)
                                ValorMax = Valor;

                            if (Valor < ValorMin)
                                ValorMin = Valor;
                        }

                        if (i == 2)
                        {
                            Valor = Convert.ToDouble(item.dMes9);
                            series3.AddItem(Valor);

                            if (Valor > ValorMax)
                                ValorMax = Valor;

                            if (Valor < ValorMin)
                                ValorMin = Valor;
                        }

                        if (i == 3)
                        {
                            Valor = Convert.ToDouble(item.dMes10);
                            series3.AddItem(Valor);

                            if (Valor > ValorMax)
                                ValorMax = Valor;

                            if (Valor < ValorMin)
                                ValorMin = Valor;
                        }

                        if (i == 4)
                        {
                            Valor = Convert.ToDouble(item.dMes11);
                            series3.AddItem(Valor);

                            if (Valor > ValorMax)
                                ValorMax = Valor;

                            if (Valor < ValorMin)
                                ValorMin = Valor;
                        }

                        if (i == 5)
                        {
                            Valor = Convert.ToDouble(item.dMes12);
                            series3.AddItem(Valor);

                            if (Valor > ValorMax)
                                ValorMax = Valor;

                            if (Valor < ValorMin)
                                ValorMin = Valor;
                        }

                        if (i == 6)
                        {
                            Valor = Convert.ToDouble(item.dMes13);
                            series3.AddItem(Valor);

                            if (Valor > ValorMax)
                                ValorMax = Valor;

                            if (Valor < ValorMin)
                                ValorMin = Valor;
                        }
                    }
                }
            }

            if (DplNegocio.SelectedValue == "5")
            {
                //Serie 4
                if (item.iIdIndicador == 52)
                {
                    for (int i = 1; i <= CantidadMes; i++)
                    {
                        if (i == 1)
                        {
                            Valor = Convert.ToDouble(item.dMes8);
                            series4.AddItem(Valor);

                            if (Valor > ValorMax)
                                ValorMax = Valor;

                            if (Valor < ValorMin)
                                ValorMin = Valor;
                        }

                        if (i == 2)
                        {
                            Valor = Convert.ToDouble(item.dMes9);
                            series4.AddItem(Valor);

                            if (Valor > ValorMax)
                                ValorMax = Valor;

                            if (Valor < ValorMin)
                                ValorMin = Valor;
                        }

                        if (i == 3)
                        {
                            Valor = Convert.ToDouble(item.dMes10);
                            series4.AddItem(Valor);

                            if (Valor > ValorMax)
                                ValorMax = Valor;

                            if (Valor < ValorMin)
                                ValorMin = Valor;
                        }

                        if (i == 4)
                        {
                            Valor = Convert.ToDouble(item.dMes11);
                            series4.AddItem(Valor);

                            if (Valor > ValorMax)
                                ValorMax = Valor;

                            if (Valor < ValorMin)
                                ValorMin = Valor;
                        }

                        if (i == 5)
                        {
                            Valor = Convert.ToDouble(item.dMes12);
                            series4.AddItem(Valor);

                            if (Valor > ValorMax)
                                ValorMax = Valor;

                            if (Valor < ValorMin)
                                ValorMin = Valor;
                        }

                        if (i == 6)
                        {
                            Valor = Convert.ToDouble(item.dMes13);
                            series4.AddItem(Valor);

                            if (Valor > ValorMax)
                                ValorMax = Valor;

                            if (Valor < ValorMin)
                                ValorMin = Valor;
                        }
                    }
                }
            }
        }
        #endregion

        SetYAxis(Convert.ToInt32(ValorMin), Convert.ToInt32(ValorMax));

        foreach (LabelItem legendItem in RadChart1.Legend.Items)
        {
            legendItem.ActiveRegion.Tooltip = "Click here";
        }
    }

    void SetXAxis(Int32 CantidadMes)
    {
        RadChart1.PlotArea.XAxis.AutoScale = false;

        Int32 InicioMes = DplSemestre.SelectedValue == "1" ? 0 : 6;

        if (RadChart1.PlotArea.XAxis.Items.Count >= CantidadMes)
        {
            // JUST CHANGING THE LABES OF THE X AXIS.

            for (int i = 0; i < CantidadMes; i++)
            {
                String MesStr = NombreMes(i + 1 + InicioMes);
                RadChart1.PlotArea.XAxis[i].TextBlock.Text = MesStr;
            }
        }
        else
        {
            // ADDING NEW ELEMENTS TO THE X AXIS. 
            RadChart1.PlotArea.XAxis.Clear();
            for (int i = 1; i <= CantidadMes; i++)
            {
                String MesStr = NombreMes(i + InicioMes);
                RadChart1.PlotArea.XAxis.AddItem(MesStr);
            }
        }
    }

    void SetYAxis(int ValorMin, int ValorMax)
    {
        Int32 Intervalo = 0;

        // ADDING NEW VALUES TO THE Y AXIS. 
        if (DplNegocio.SelectedValue == "2")
        {
            ValorMax = 1000000;
            ValorMin = -200000;
            Intervalo = 200000;
        }
        else if (DplNegocio.SelectedValue == "3")
        {
            ValorMax = 70;
            ValorMin = -10;
            Intervalo = 10;
        }
        else if (DplNegocio.SelectedValue == "4")
        {
            ValorMax = 30;
            ValorMin = 0;
            Intervalo = 5;
        }
        else if (DplNegocio.SelectedValue == "5")
        {
            if (DplSemestre.SelectedValue == "1")
            {
                ValorMax = 130;
                ValorMin = 50;
                Intervalo = 10;
            }
            else
            {
                ValorMax = 140;
                ValorMin = 0;
                Intervalo = 20;
            }
        }

        //if (ValorMax > 100000)
        //{
        //    ValorMax = 1000000;
        //    Intervalo = 200000;
        //}
        //else
        //{
        //    ValorMax = 70;
        //    Intervalo = 10;
        //}

        //if (ValorMin >= 0)
        //    ValorMin = 0;
        //else
        //{
        //    if (ValorMin >= (Intervalo * (-1)))
        //    {
        //        ValorMin = Intervalo * (-1);
        //    }
        //    else
        //    {
        //        ValorMin = Convert.ToInt32((ValorMin / Intervalo));
        //    }
        //}

        RadChart1.PlotArea.YAxis.AddRange(ValorMin, ValorMax, Intervalo);
    }

    static void SetSeriesAppearance(ChartSeries series)
    {
        series.Appearance.Border.Color = Color.Black;
        //series.Appearance.LabelAppearance.Border.Color = Color.Black;
        series.Appearance.LabelAppearance.FillStyle.MainColor = Color.White;
        series.Appearance.TextAppearance.TextProperties.Font = new Font("Calibri", 8);
        series.Appearance.TextAppearance.TextProperties.Color = Color.Black;
        //series.Appearance.LabelAppearance.Dimensions.Margins = new ChartMargins(2);
        series.DefaultLabelValue = "#Y{N0}";
        series.Appearance.BarWidthPercent = 60;
    }

    protected void DplNegocio_SelectedIndexChanged(object sender, EventArgs e)
    {
        if ((DplNegocio.SelectedValue == "2") || (DplNegocio.SelectedValue == "3"))
            RblModoGrafico.Enabled = true;
        else
            RblModoGrafico.Enabled = false;
    }

    protected void btn_cerrar_Click(object sender, EventArgs e)
    {
        if (GlobalEntity.Instancia.CerrarExtranet == "S")
        {
            BL_Usuario oBL_Usuario = new BL_Usuario();
            oBL_Usuario.GrabaInicioSesion(GlobalEntity.Instancia.Usuario, "0");
            Session.Abandon();
            FormsAuthentication.SignOut();
            Response.Redirect("../../eFENIX_Login.aspx");
            Response.End();
        }
    }

    //protected void InitChart(List<BE_Indicador> LstIndicador)
    //{
    //    //DataTable dt;
    //    NumericSeries series1;

    //    List<NumericSeries> series = new List<NumericSeries>();

    //    UltraChart1.ColorModel.ModelStyle = ColorModels.CustomSkin;
    //    UltraChart1.ColorModel.Skin.PEs.Add(new PaintElement(Color.SteelBlue));
    //    UltraChart1.ColorModel.Skin.PEs.Add(new PaintElement(Color.IndianRed));
    //    UltraChart1.ColorModel.Skin.PEs.Add(new PaintElement(Color.ForestGreen));

    //    //UltraChart1.ChartType = ChartType.ColumnChart3D;
    //    UltraChart1.ChartType = ChartType.ColumnChart;
    //    //UltraChart1.ChartType = ChartType.Composite;

    //    //ChartArea myChartArea = new ChartArea();
    //    //UltraChart1.CompositeChart.ChartAreas.Add(myChartArea);

    //    //AxisItem axisX = new AxisItem();
    //    //axisX.OrientationType = AxisNumber.X_Axis;
    //    //axisX.DataType = AxisDataType.String;
    //    //axisX.SetLabelAxisType = SetLabelAxisType.GroupBySeries;
    //    //axisX.Labels.ItemFormatString = "<ITEM_LABEL>";
    //    //axisX.Labels.Orientation = TextOrientation.VerticalLeftFacing;

    //    //AxisItem axisY = new AxisItem();
    //    //axisY.OrientationType = AxisNumber.Y_Axis;
    //    //axisY.DataType = AxisDataType.Numeric;
    //    //axisY.Labels.ItemFormatString = "<DATA_VALUE:0.#>";

    //    //myChartArea.Axes.Add(axisX);
    //    //myChartArea.Axes.Add(axisY);

    //    ////Enero
    //    //series1 = new NumericSeries();
    //    //series2 = new NumericSeries();
    //    //series3 = new NumericSeries();

    //    //series1.Points.Add(new NumericDataPoint((Double)LstIndicador[0].dMes8, "Point A", false));
    //    //series2.Points.Add(new NumericDataPoint((Double)LstIndicador[1].dMes8, "Point A", false));
    //    //series3.Points.Add(new NumericDataPoint((Double)LstIndicador[2].dMes8, "Point A", false));

    //    //series1.DataBind();
    //    //series2.DataBind();
    //    //series3.DataBind();

    //    //UltraChart1.Series.Add(series1);
    //    //UltraChart1.Series.Add(series2);
    //    //UltraChart1.Series.Add(series3);

    //    //3D
    //    foreach (BE_Indicador item in LstIndicador)
    //    {
    //    //for (int i = 1; i <= 6; i++)
    //    //{
    //        //dt = new DataTable();
    //        //dt.Columns.Add("Serie", typeof(string));
    //        //dt.Columns.Add("Valor", typeof(double));

    //        //dt.Rows.Add("Enero", item.dMes8);
    //        //dt.Rows.Add("Febrero", item.dMes9);
    //        //dt.Rows.Add("Marzo", item.dMes10);
    //        //dt.Rows.Add("Abril", item.dMes11);
    //        //dt.Rows.Add("Mayo", item.dMes12);
    //        //dt.Rows.Add("Junio", item.dMes13);

    //        //series1 = new NumericSeries();
    //        //series1.Data.DataSource = dt;
    //        //series1.Data.LabelColumn = "Serie";
    //        //series1.Data.ValueColumn = "Valor";

    //        series1 = new NumericSeries();
    //        series1.Points.Add(new NumericDataPoint((Double)item.dMes8, "Point A", false));
    //        series1.Points.Add(new NumericDataPoint((Double)item.dMes9, "Point B", false));
    //        series1.Points.Add(new NumericDataPoint((Double)item.dMes10, "Point C", false));
    //        series1.Points.Add(new NumericDataPoint((Double)item.dMes11, "Point D", false));
    //        series1.Points.Add(new NumericDataPoint((Double)item.dMes12, "Point E", false));
    //        series1.Points.Add(new NumericDataPoint((Double)item.dMes13, "Point F", false));

    //        //series1 = new NumericSeries();
    //        //series1.Points.Add(new NumericDataPoint((Double)LstIndicador[0].dMes8, "Point A", false));
    //        //series1.Points.Add(new NumericDataPoint((Double)LstIndicador[1].dMes8, "Point B", false));
    //        //series1.Points.Add(new NumericDataPoint((Double)LstIndicador[2].dMes8, "Point C", false));

    //        series1.DataBind();
    //        series.Add(series1);

    //        series1.Label = item.sNombreSerie;

    //        //UltraChart1.Series.Add(series1); 
    //        UltraChart1.CompositeChart.Series.Add(series1);
    //    }

    //    //ChartLayerAppearance myColumnLayer = new ChartLayerAppearance();
    //    //myColumnLayer.ChartType = ChartType.ColumnChart;
    //    //myColumnLayer.ChartArea = myChartArea;
    //    //myColumnLayer.AxisX = axisX;
    //    //myColumnLayer.AxisY = axisY;
    //    //myColumnLayer.Series.Add(series[0]);
    //    //myColumnLayer.Series.Add(series[1]);
    //    //myColumnLayer.Series.Add(series[2]);
    //    //UltraChart1.CompositeChart.ChartLayers.Add(myColumnLayer);

    //    //CompositeLegend myLegend = new CompositeLegend();
    //    //myLegend.ChartLayers.Add(myColumnLayer);
    //    //myLegend.Bounds = new Rectangle(0, 75, 20, 25);
    //    //myLegend.BoundsMeasureType = MeasureType.Percentage;
    //    //myLegend.PE.ElementType = PaintElementType.Gradient;
    //    //myLegend.PE.FillGradientStyle = GradientStyle.ForwardDiagonal;
    //    //myLegend.PE.Fill = Color.CornflowerBlue;
    //    //myLegend.PE.FillStopColor = Color.Transparent;
    //    //myLegend.Border.CornerRadius = 10;
    //    //myLegend.Border.Thickness = 0;
    //    //UltraChart1.CompositeChart.Legends.Add(myLegend);

    //    //UltraChart1.Series.Add(series[0]);
    //    //UltraChart1.Series.Add(series[1]);
    //    //UltraChart1.Series.Add(series[2]);
        
    //    UltraChart1.DataBind();
        
    //    //series[0].Label = "Dep. Tem. Contenedor";
    //    //series[1].Label = "Dep. Tem. Carga Suelta y Rodante";
    //    //series[2].Label = "Dep. Aduanero y Simple";   
    //}

    //protected void UltraChart1_FillSceneGraph(object sender, Infragistics.UltraChart.Shared.Events.FillSceneGraphEventArgs e)
    //{
        

    //}
}
