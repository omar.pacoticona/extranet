﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using FENIX.BusinessEntity;
using FENIX.BusinessLogic;
using System.Drawing;
using System.Collections.Generic;

public partial class Extranet_Gerencia_eFENIX_PeriodoOperativo : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        TxtDiasMovBal.Attributes.Add("onkeypress", "javascript:return SoloEnteros(event);");
        TxtDiasFechaMovBal.Attributes.Add("onkeypress", "javascript:return SoloEnteros(event);");
        TxtDiasPesosMovBal.Attributes.Add("onkeypress", "javascript:return SoloEnteros(event);");
        TxtDiasFechaBalLib.Attributes.Add("onkeypress", "javascript:return SoloEnteros(event);");


        String tiempo = (Convert.ToInt32(GlobalEntity.Instancia.TiempoDuracionSesion.ToString()) * 1000 * 60).ToString();
        Session["Reset"] = true;

        if (!Page.IsPostBack)
        {
            ListarDropDowList();

            if (DateTime.Now.Month == 1)
            {
                DplAnioPeriodo.SelectedValue = (DateTime.Now.Year - 1).ToString();
                DplMesPeriodo.SelectedValue = "12";
            }
            else
            {
                DplAnioPeriodo.SelectedValue = DateTime.Now.Year.ToString();
                DplMesPeriodo.SelectedValue = (DateTime.Now.Month - 1).ToString();
            }

            //CargarMes(Convert.ToInt32(DplAnioPeriodo.SelectedValue));
            CargaPeriodo();
            ScriptManager.RegisterStartupScript(this, GetType(), "mensaje", "SessionExpireAlert(" + tiempo + ");", true);
        }
        else
            ScriptManager.RegisterStartupScript(this, GetType(), "mensaje", "myStopFunction();", true);
    }

    #region "Método Controles"

    protected void ListarDropDowList()
    {
        try
        {
            DplAnioPeriodo.Items.Clear();
            for (int i = 2013; i <= DateTime.Now.Year; i++)
            {
                DplAnioPeriodo.Items.Add(new ListItem(i.ToString(), i.ToString()));
            }
            DplAnioPeriodo.SelectedIndex = 0;

            DplMesPeriodo.Items.Clear();
            DplMesPeriodo.Items.Add(new ListItem("Enero", "1"));
            DplMesPeriodo.Items.Add(new ListItem("Febrero", "2"));
            DplMesPeriodo.Items.Add(new ListItem("Marzo", "3"));
            DplMesPeriodo.Items.Add(new ListItem("Abril", "4"));
            DplMesPeriodo.Items.Add(new ListItem("Mayo", "5"));
            DplMesPeriodo.Items.Add(new ListItem("Junio", "6"));
            DplMesPeriodo.Items.Add(new ListItem("Julio", "7"));
            DplMesPeriodo.Items.Add(new ListItem("Agosto", "8"));
            DplMesPeriodo.Items.Add(new ListItem("Setiembre", "9"));
            DplMesPeriodo.Items.Add(new ListItem("Octubre", "10"));
            DplMesPeriodo.Items.Add(new ListItem("Noviembre", "11"));
            DplMesPeriodo.Items.Add(new ListItem("Diciembre", "12"));
        }
        catch (Exception e)
        {
        }
    }

    #endregion

    protected void ChkMovBal_CheckedChanged(object sender, EventArgs e)
    {
        TxtDiasMovBal.Enabled = ChkMovBal.Checked;
        TxtDiasMovBal.BackColor = ChkMovBal.Checked ? Color.White : Color.WhiteSmoke;
        TxtDiasMovBal.Focus();
    }

    protected void ChkFechaMovBal_CheckedChanged(object sender, EventArgs e)
    {
        TxtDiasFechaMovBal.Enabled = ChkFechaMovBal.Checked;
        TxtDiasFechaMovBal.BackColor = ChkFechaMovBal.Checked ? Color.White : Color.WhiteSmoke;
        TxtDiasFechaMovBal.Focus();
    }

    protected void ChkPesosMovBal_CheckedChanged(object sender, EventArgs e)
    {
        TxtDiasPesosMovBal.Enabled = ChkPesosMovBal.Checked;
        TxtDiasPesosMovBal.BackColor = ChkPesosMovBal.Checked ? Color.White : Color.WhiteSmoke;
        TxtDiasPesosMovBal.Focus();
    }

    protected void ChkFechaBalLib_CheckedChanged(object sender, EventArgs e)
    {
        TxtDiasFechaBalLib.Enabled = ChkFechaBalLib.Checked;
        TxtDiasFechaBalLib.BackColor = ChkFechaBalLib.Checked ? Color.White : Color.WhiteSmoke;
        TxtDiasFechaBalLib.Focus();
    }

    protected void ImgBtnGrabar_Click(object sender, ImageClickEventArgs e)
    {
        BL_ControlPeriodo oBL_ControlPeriodo = new BL_ControlPeriodo();
        BE_ControlPeriodo oBE_ControlPeriodo = new BE_ControlPeriodo();

        oBE_ControlPeriodo.iIdTipoCierre = 1;
        oBE_ControlPeriodo.iAnioPeriodo = Convert.ToInt32(DplAnioPeriodo.SelectedValue);
        oBE_ControlPeriodo.iMesPeriodo = Convert.ToInt32(DplMesPeriodo.SelectedValue);
        oBE_ControlPeriodo.dtFecha = DateTime.Now;
        oBE_ControlPeriodo.sUsuario = GlobalEntity.Instancia.Usuario;
        oBE_ControlPeriodo.sNombrePc = GlobalEntity.Instancia.NombrePc;

        int vl_iCodigo = 1;
        String vl_sMessage = string.Empty;
        String[] oResultadoTransaccionTx;
        Boolean Indicador = false;

        if (ChkMovBal.Checked)
        {
            if (vl_iCodigo > 0)
            {
                if (String.IsNullOrEmpty(TxtDiasMovBal.Text))
                {
                    ScriptManager.RegisterStartupScript(UpControles, GetType(), "mensaje", String.Format(Resources.Resource.MsgJavaError, "Debe ingresar el dia"), true);
                    TxtDiasMovBal.Focus();
                    return;
                }
                else
                {
                    if ((Convert.ToInt32(TxtDiasMovBal.Text) < 0) || (Convert.ToInt32(TxtDiasMovBal.Text) > 10))
                    {
                        ScriptManager.RegisterStartupScript(UpControles, GetType(), "mensaje", String.Format(Resources.Resource.MsgJavaError, "Debe ingresar un dia entre 1 y 10"), true);
                        TxtDiasMovBal.Focus();
                        return;
                    }
                }

                oBE_ControlPeriodo.iIdTipoProceso = 3; //Habilitar Movimiento de Balanza               
                oBE_ControlPeriodo.iCantidadDias = Convert.ToInt32(TxtDiasMovBal.Text);

                oResultadoTransaccionTx = oBL_ControlPeriodo.Actualizar(oBE_ControlPeriodo).Split('|');
                vl_sMessage = string.Empty;

                for (int i = 0; i < oResultadoTransaccionTx.Length; i++)
                {
                    if (i == 0)
                    {
                        vl_iCodigo = Convert.ToInt32(oResultadoTransaccionTx[i]);
                    }
                    else
                    {
                        vl_sMessage = oResultadoTransaccionTx[i];
                    }
                }

                Indicador = true;
            }
        }

        if (ChkFechaMovBal.Checked)
        {
            if (vl_iCodigo > 0)
            {
                if (String.IsNullOrEmpty(TxtDiasFechaMovBal.Text))
                {
                    ScriptManager.RegisterStartupScript(UpControles, GetType(), "mensaje", String.Format(Resources.Resource.MsgJavaError, "Debe ingresar el dia"), true);
                    TxtDiasFechaMovBal.Focus();
                    return;
                }
                else
                {
                    if ((Convert.ToInt32(TxtDiasFechaMovBal.Text) < 0) || (Convert.ToInt32(TxtDiasFechaMovBal.Text) > 10))
                    {
                        ScriptManager.RegisterStartupScript(UpControles, GetType(), "mensaje", String.Format(Resources.Resource.MsgJavaError, "Debe ingresar un dia entre 1 y 10"), true);
                        TxtDiasFechaMovBal.Focus();
                        return;
                    }
                }

                oBE_ControlPeriodo.iIdTipoProceso = 2; //Habilitar Fecha Movimiento de Balanza    
                oBE_ControlPeriodo.iCantidadDias = Convert.ToInt32(TxtDiasFechaMovBal.Text);

                oResultadoTransaccionTx = oBL_ControlPeriodo.Actualizar(oBE_ControlPeriodo).Split('|');
                vl_sMessage = string.Empty;

                for (int i = 0; i < oResultadoTransaccionTx.Length; i++)
                {
                    if (i == 0)
                    {
                        vl_iCodigo = Convert.ToInt32(oResultadoTransaccionTx[i]);
                    }
                    else
                    {
                        vl_sMessage = oResultadoTransaccionTx[i];
                    }
                }

                Indicador = true;
            }
        }

        if (ChkPesosMovBal.Checked)
        {
            if (vl_iCodigo > 0)
            {
                if (String.IsNullOrEmpty(TxtDiasPesosMovBal.Text))
                {
                    ScriptManager.RegisterStartupScript(UpControles, GetType(), "mensaje", String.Format(Resources.Resource.MsgJavaError, "Debe ingresar el dia"), true);
                    TxtDiasPesosMovBal.Focus();
                    return;
                }
                else
                {
                    if ((Convert.ToInt32(TxtDiasPesosMovBal.Text) < 0) || (Convert.ToInt32(TxtDiasPesosMovBal.Text) > 10))
                    {
                        ScriptManager.RegisterStartupScript(UpControles, GetType(), "mensaje", String.Format(Resources.Resource.MsgJavaError, "Debe ingresar un dia entre 1 y 10"), true);
                        TxtDiasPesosMovBal.Focus();
                        return;
                    }
                }

                oBE_ControlPeriodo.iIdTipoProceso = 1; //Habilitar Pesos Movimiento de Balanza    
                oBE_ControlPeriodo.iCantidadDias = Convert.ToInt32(TxtDiasPesosMovBal.Text);

                oResultadoTransaccionTx = oBL_ControlPeriodo.Actualizar(oBE_ControlPeriodo).Split('|');
                vl_sMessage = string.Empty;

                for (int i = 0; i < oResultadoTransaccionTx.Length; i++)
                {
                    if (i == 0)
                    {
                        vl_iCodigo = Convert.ToInt32(oResultadoTransaccionTx[i]);
                    }
                    else
                    {
                        vl_sMessage = oResultadoTransaccionTx[i];
                    }
                }

                Indicador = true;
            }
        }

        if (ChkFechaBalLib.Checked)
        {
            if (vl_iCodigo > 0)
            {
                if (String.IsNullOrEmpty(TxtDiasFechaBalLib.Text))
                {
                    ScriptManager.RegisterStartupScript(UpControles, GetType(), "mensaje", String.Format(Resources.Resource.MsgJavaError, "Debe ingresar el dia"), true);
                    TxtDiasFechaBalLib.Focus();
                    return;
                }
                else
                {
                    if ((Convert.ToInt32(TxtDiasFechaBalLib.Text) < 0) || (Convert.ToInt32(TxtDiasFechaBalLib.Text) > 10))
                    {
                        ScriptManager.RegisterStartupScript(UpControles, GetType(), "mensaje", String.Format(Resources.Resource.MsgJavaError, "Debe ingresar un dia entre 1 y 10"), true);
                        TxtDiasFechaBalLib.Focus();
                        return;
                    }
                }

                oBE_ControlPeriodo.iIdTipoProceso = 4; //Cambia Fecha Balanza Libre   
                oBE_ControlPeriodo.iCantidadDias = Convert.ToInt32(TxtDiasFechaBalLib.Text);

                oResultadoTransaccionTx = oBL_ControlPeriodo.Actualizar(oBE_ControlPeriodo).Split('|');
                vl_sMessage = string.Empty;

                for (int i = 0; i < oResultadoTransaccionTx.Length; i++)
                {
                    if (i == 0)
                    {
                        vl_iCodigo = Convert.ToInt32(oResultadoTransaccionTx[i]);
                    }
                    else
                    {
                        vl_sMessage = oResultadoTransaccionTx[i];
                    }
                }

                Indicador = true;
            }
        }

        if (Indicador)
        {
            if (vl_iCodigo < 1)
            {
                ScriptManager.RegisterStartupScript(UpControles, GetType(), "mensaje", String.Format(Resources.Resource.MsgJavaError, vl_sMessage), true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(UpControles, GetType(), "mensaje", String.Format(Resources.Resource.MsgJavaExito, vl_sMessage), true);
                CargaPeriodo();
            }
        }
        else
        {
            ScriptManager.RegisterStartupScript(UpControles, GetType(), "mensaje", String.Format(Resources.Resource.MsgJavaAlerta, "Debe seleccionar una de las opciones"), true);
        }
    }

    protected void DplAnioPeriodo_SelectedIndexChanged(object sender, EventArgs e)
    {
        CargaPeriodo();
    }

    protected void DplMesPeriodo_SelectedIndexChanged(object sender, EventArgs e)
    {
        CargaPeriodo();
    }

    private void CargaPeriodo()
    {
        IniciarControles();

        Int32 iAnioPeriodo = Convert.ToInt32(DplAnioPeriodo.SelectedValue);
        Int32 iMesPeriodo = Convert.ToInt32(DplMesPeriodo.SelectedValue);

        BL_ControlPeriodo oBL_ControlPeriodo = new BL_ControlPeriodo();
        BE_ControlPeriodo oBE_ControlPeriodo = new BE_ControlPeriodo();
        List<BE_ControlPeriodo> LstControlPeriodo = new List<BE_ControlPeriodo>();

        oBE_ControlPeriodo.iIdTipoCierre = 1; //Operativo
        oBE_ControlPeriodo.iAnioPeriodo = iAnioPeriodo;
        oBE_ControlPeriodo.iMesPeriodo = iMesPeriodo;

        LstControlPeriodo = oBL_ControlPeriodo.Listar(oBE_ControlPeriodo);

        foreach (BE_ControlPeriodo item in LstControlPeriodo)
        {
            if (item.iIdTipoProceso == 1) //Pesos Balanza
            {
                if ((item.sEstado == "1") && (!String.IsNullOrEmpty(item.iCantidadDias.ToString())))
                {
                    ChkPesosMovBal.Checked = true;
                    TxtDiasPesosMovBal.Text = item.iCantidadDias.ToString();
                    TxtDiasPesosMovBal.BackColor = Color.White;
                    TxtDiasPesosMovBal.Enabled = true;

                    LblDiasPesosMovBal.Text = item.dtFecha.HasValue ? "Vence el " + item.dtFecha.Value.AddDays(item.iCantidadDias.Value).ToShortDateString() : String.Empty;
                }
            }

            if (item.iIdTipoProceso == 2) //Fecha Balanza
            {
                if ((item.sEstado == "1") && (!String.IsNullOrEmpty(item.iCantidadDias.ToString())))
                {
                    ChkFechaMovBal.Checked = true;
                    TxtDiasFechaMovBal.Text = item.iCantidadDias.ToString();
                    TxtDiasFechaMovBal.BackColor = Color.White;
                    TxtDiasFechaMovBal.Enabled = true;

                    LblDiasFechaMovBal.Text = item.dtFecha.HasValue ? "Vence el " + item.dtFecha.Value.AddDays(item.iCantidadDias.Value).ToShortDateString() : String.Empty;
                }
            }

            if (item.iIdTipoProceso == 3) //Movimiento Balanza
            {
                if ((item.sEstado == "1") && (!String.IsNullOrEmpty(item.iCantidadDias.ToString())))
                {
                    ChkMovBal.Checked = true;
                    TxtDiasMovBal.Text = item.iCantidadDias.ToString();
                    TxtDiasMovBal.BackColor = Color.White;
                    TxtDiasMovBal.Enabled = true;

                    LblFechaMovBal.Text = item.dtFecha.HasValue ? "Vence el " + item.dtFecha.Value.AddDays(item.iCantidadDias.Value).ToShortDateString() : String.Empty;
                }
            }

            if (item.iIdTipoProceso == 4) //Balanza Libre
            {
                if ((item.sEstado == "1") && (!String.IsNullOrEmpty(item.iCantidadDias.ToString())))
                {
                    ChkFechaBalLib.Checked = true;
                    TxtDiasFechaBalLib.Text = item.iCantidadDias.ToString();
                    TxtDiasFechaBalLib.BackColor = Color.White;
                    TxtDiasFechaBalLib.Enabled = true;

                    LblDiasFechaBalLib.Text = item.dtFecha.HasValue ? "Vence el " + item.dtFecha.Value.AddDays(item.iCantidadDias.Value).ToShortDateString() : String.Empty;
                }
            }
        }
    }

    protected void IniciarControles()
    {
        ChkMovBal.Checked = false;
        ChkFechaMovBal.Checked = false;
        ChkPesosMovBal.Checked = false;
        ChkFechaBalLib.Checked = false;

        TxtDiasMovBal.Text = String.Empty;
        TxtDiasFechaMovBal.Text = String.Empty;
        TxtDiasPesosMovBal.Text = String.Empty;
        TxtDiasFechaBalLib.Text = String.Empty;

        TxtDiasMovBal.BackColor = Color.WhiteSmoke;
        TxtDiasFechaMovBal.BackColor = Color.WhiteSmoke;
        TxtDiasPesosMovBal.BackColor = Color.WhiteSmoke;
        TxtDiasFechaBalLib.BackColor = Color.WhiteSmoke;

        LblDiasFechaBalLib.Text = String.Empty;
        LblDiasFechaMovBal.Text = String.Empty;
        LblDiasPesosMovBal.Text = String.Empty;
        LblFechaMovBal.Text = String.Empty;
    }

    protected void BtnCerrarPeriodo_Click(object sender, EventArgs e)
    {
        BL_ControlPeriodo oBL_ControlPeriodo = new BL_ControlPeriodo();
        BE_ControlPeriodo oBE_ControlPeriodo = new BE_ControlPeriodo();

        oBE_ControlPeriodo.iIdTipoCierre = 1; //Operativo
        oBE_ControlPeriodo.iAnioPeriodo = Convert.ToInt32(DplAnioPeriodo.SelectedValue);
        oBE_ControlPeriodo.iMesPeriodo = Convert.ToInt32(DplMesPeriodo.SelectedValue);

        oBE_ControlPeriodo.sUsuario = GlobalEntity.Instancia.Usuario;
        oBE_ControlPeriodo.sNombrePc = GlobalEntity.Instancia.NombrePc;

        String vl_sMessage = string.Empty;
        String[] oResultadoTransaccionTx;
        int vl_iCodigo = 0;

        oResultadoTransaccionTx = oBL_ControlPeriodo.CerrarPeriodo(oBE_ControlPeriodo).Split('|');
        vl_sMessage = string.Empty;

        for (int i = 0; i < oResultadoTransaccionTx.Length; i++)
        {
            if (i == 0)
            {
                vl_iCodigo = Convert.ToInt32(oResultadoTransaccionTx[i]);
            }
            else
            {
                vl_sMessage = oResultadoTransaccionTx[i];
            }
        }

        if (vl_iCodigo < 1)
        {
            ScriptManager.RegisterStartupScript(UpControles, GetType(), "mensaje", String.Format(Resources.Resource.MsgJavaError, vl_sMessage), true);
        }
        else
        {
            ScriptManager.RegisterStartupScript(UpControles, GetType(), "mensaje", String.Format(Resources.Resource.MsgJavaExito, vl_sMessage), true);
            IniciarControles();
        }
    }

    protected void BtnVerHistorial_Click(object sender, EventArgs e)
    {
        BL_ControlPeriodo oBL_ControlPeriodo = new BL_ControlPeriodo();
        BE_ControlPeriodo oBE_ControlPeriodo = new BE_ControlPeriodo();

        oBE_ControlPeriodo.iAnioPeriodo = Convert.ToInt32(DplAnioPeriodo.SelectedValue);
        oBE_ControlPeriodo.iMesPeriodo = Convert.ToInt32(DplMesPeriodo.SelectedValue);
        oBE_ControlPeriodo.iIdTipoCierre = 1;
        GrvDetalle.DataSource = oBL_ControlPeriodo.ListarLog(oBE_ControlPeriodo);
        GrvDetalle.DataBind();

        if (GrvDetalle.Rows.Count == 0)
        {
            List<BE_ControlPeriodo> LstControlPeriodo = new List<BE_ControlPeriodo>();
            LstControlPeriodo.Add(oBE_ControlPeriodo);
            GrvDetalle.EmptyDataText = "La vista detallada no está disponible";
            GrvDetalle.DataSource = LstControlPeriodo;
            GrvDetalle.DataBind();
        }

        LblPeriodo.Text = DplAnioPeriodo.SelectedValue + " - " + DplMesPeriodo.SelectedItem.Text;

        ScriptManager.RegisterStartupScript(UpdatePanel2, GetType(), "AbrirPopup", String.Format("javascript: OpenPopub();"), true);
    }

    protected void btn_cerrar_Click(object sender, EventArgs e)
    {
        if (GlobalEntity.Instancia.CerrarExtranet == "S")
        {
            BL_Usuario oBL_Usuario = new BL_Usuario();
            oBL_Usuario.GrabaInicioSesion(GlobalEntity.Instancia.Usuario, "0");
            Session.Abandon();
            FormsAuthentication.SignOut();
            Response.Redirect("../../eFENIX_Login.aspx");
            Response.End();
        }
    }
}
