﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using FENIX.BusinessEntity;
using FENIX.BusinessLogic;
using System.Windows.Forms;
using System.Drawing;
using System.Web.Security;
using FENIX.DataAccess;
using FENIX.Common;
using System.Web.UI.WebControls;
using System.IO;
using System.Text;
using System.Web.UI.HtmlControls;

public partial class Extranet_Gerencia_eFENIX_Liquidaciones : System.Web.UI.Page
{

    BE_AcuerdoComision oBE_AcuerdoComision;

    private void SCA_MsgInformacion(string strError)
    {
        MensajeScript(strError);
    }

    private void MensajeScript(string SMS)
    {
        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "", "<script>alert('" + SMS + "');</script>", false);
    }


    protected void Page_Load(object sender, EventArgs e)
    {
        String tiempo = (Convert.ToInt32(GlobalEntity.Instancia.TiempoDuracionSesion.ToString()) * 1000 * 60).ToString();
        Session["Reset"] = true;
        if (!Page.IsPostBack)
        {
           
            if (GrvListado.Rows.Count == 0)
            {
                BE_Cliente oBE_Cliente = new BE_Cliente();
                oBE_Cliente.sIdCliente = GlobalEntity.Instancia.IdCliente.ToString();
                //idLblComisionista.Text =  GlobalEntity.Instancia.NombreCliente.ToString();
                if (GlobalEntity.Instancia.NombreCliente.Length >= 80)
                    idLblComisionista.Text = GlobalEntity.Instancia.NombreCliente.Substring(0, 80);
                else
                    idLblComisionista.Text =  GlobalEntity.Instancia.NombreCliente;
            }
            // ImgBtnGrabar.Enabled = false;
            //btnAceptar.Enabled = false;
            ScriptManager.RegisterStartupScript(this, GetType(), "mensaje", "SessionExpireAlert(" + tiempo + ");", true);
        }
        else
            ScriptManager.RegisterStartupScript(this, GetType(), "mensaje", "myStopFunction();", true);

    }


    protected void btn_cerrar_Click(object sender, EventArgs e)
    {
        if (GlobalEntity.Instancia.CerrarExtranet == "S")
        {
            BL_Usuario oBL_Usuario = new BL_Usuario();
            oBL_Usuario.GrabaInicioSesion(GlobalEntity.Instancia.Usuario, "0");
            Session.Abandon();
            FormsAuthentication.SignOut();
            Response.Redirect("../../eFENIX_Login.aspx");
            Response.End();
        }
    }
    

    protected void ImgBtnBuscar_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            LlenarGrilla();

        }
        catch (Exception ex)
        {

        }

    }


    public void LlenarGrilla()
    {
        try
        {
            lblTotalRegistros.Text = "0";
            lblRegistrosSelecc.Text = "0";
            lblImportSelec.Text = "0";
            BE_AcuerdoComision oBE_AcuerdoComision;
            BL_AcuerdoComision oBL_AcuerdoComision;
            oBL_AcuerdoComision = new BL_AcuerdoComision();
            oBE_AcuerdoComision = new BE_AcuerdoComision();
            Int32 TotalRegistros = 0;
          

            oBE_AcuerdoComision.iIdComisionista = Convert.ToInt32(GlobalEntity.Instancia.IdCliente);
            oBE_AcuerdoComision.iIdMoneda = 23;

            List<BE_AcuerdoComision> oLista_Acuerdo = new List<BE_AcuerdoComision>();

            oLista_Acuerdo = oBL_AcuerdoComision.ListarDocumentoParaLiquidarComision(oBE_AcuerdoComision);
            TotalRegistros = oLista_Acuerdo.Count();
            lblTotalRegistros.Text = TotalRegistros.ToString();

            GrvListado.DataSource = oLista_Acuerdo;
            GrvListado.DataBind();
            //ImgBtnGrabar.Enabled = true;
            UpdatePanel1.Update();
        }

        catch (Exception ex)
        {

        }
    }    

    protected void GrvListado_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        Int32 TotalRegistros = 0;
        Int32 RegistrosSelecc = 0;
        decimal? ImporteTotal = 0;

        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            

            DataKey dataKey;
            dataKey = this.GrvListado.DataKeys[e.Row.RowIndex];
            BE_AcuerdoComision oitem = (e.Row.DataItem as BE_AcuerdoComision);
            System.Web.UI.WebControls.CheckBox chkSeleccion = (System.Web.UI.WebControls.CheckBox)e.Row.FindControl("chkSeleccionar");
            System.Web.UI.WebControls.CheckBox chkSeleccionGroup = (System.Web.UI.WebControls.CheckBox)e.Row.FindControl("chkAllQuitarGrp");


            decimal dSubTotal = 0;
            decimal.TryParse(e.Row.Cells[15].Text, out dSubTotal);
            e.Row.Cells[15].Text = dSubTotal.ToString("N2");
            //TotalRegistros += 1;

            //if (oitem.iIdDocOriDet != null)
            //{
            //    for (int c = 1; c < e.Row.Cells.Count; c++)
            //    {
            //chkSeleccion.Checked = true;

            //if (oitem.iIdDocOriDet == null)
            //{
            //    e.Row.Cells[0].Visible = false;
            //    e.Row.Cells[14].Visible = false;
            //}

            if (dataKey.Values["sSituacion"].ToString() == "Falta Pagar") // Alguna factura de todo el contenedor falta pagar.
             {
                       
                        chkSeleccion.Checked = false;
                        chkSeleccion.Enabled = false;
                        e.Row.Cells[10].ToolTip = "Facturas pendientes de pago";
               
                e.Row.Cells[4].ForeColor = Color.FromName("#B9B8C");
                e.Row.Cells[5].ForeColor = Color.FromName("#B9B8C");
                e.Row.Cells[6].ForeColor = Color.FromName("#B9B8C");                
                e.Row.Cells[7].ForeColor = Color.FromName("#B9B8C");
                e.Row.Cells[8].ForeColor = Color.FromName("#B9B8C");
                e.Row.Cells[9].ForeColor = Color.FromName("#B9B8C");
                e.Row.Cells[10].ForeColor = Color.FromName("#B9B8C");
                e.Row.Cells[11].ForeColor = Color.FromName("#B9B8C");
                e.Row.Cells[13].ForeColor = Color.FromName("#B9B8C");
                e.Row.Cells[14].ForeColor = Color.FromName("#B9B8C");
                e.Row.Cells[15].ForeColor = Color.FromName("#B9B8C");

                e.Row.Cells[4].ToolTip = "Facturas pendientes de pago";
                e.Row.Cells[5].ToolTip = "Facturas pendientes de pago";
                e.Row.Cells[6].ToolTip = "Facturas pendientes de pago";
                e.Row.Cells[7].ToolTip = "Facturas pendientes de pago";
                e.Row.Cells[8].ToolTip = "Facturas pendientes de pago";
                e.Row.Cells[9].ToolTip = "Facturas pendientes de pago";
                e.Row.Cells[10].ToolTip = "Facturas pendientes de pago";
                e.Row.Cells[11].ToolTip = "Facturas pendientes de pago";
                e.Row.Cells[13].ToolTip = "Facturas pendientes de pago";
                e.Row.Cells[14].ToolTip = "Facturas pendientes de pago";
            }

            if (dataKey.Values["sSituacion"].ToString() == "Pagado")
             {
                chkSeleccion.Checked = true;
                chkSeleccion.Enabled = true;    
               
                e.Row.Cells[4].ToolTip = "Todas las facturas pagadas.";
                e.Row.Cells[5].ToolTip = "Todas las facturas pagadas.";
                e.Row.Cells[6].ToolTip = "Todas las facturas pagadas.";
                e.Row.Cells[7].ToolTip = "Todas las facturas pagadas.";
                e.Row.Cells[8].ToolTip = "Todas las facturas pagadas.";
                e.Row.Cells[9].ToolTip = "Todas las facturas pagadas.";
                e.Row.Cells[10].ToolTip = "Todas las facturas pagadas.";
                e.Row.Cells[11].ToolTip = "Todas las facturas pagadas.";
                e.Row.Cells[13].ToolTip = "Todas las facturas pagadas.";
                e.Row.Cells[14].ToolTip = "Todas las facturas pagadas.";
                //e.Row.Cells[13].BackColor = System.Drawing.Color.LightGreen; 

                // e.Row.Cells[13].BackColor = Color.FromName("#F39999");

                e.Row.Cells[4].ForeColor = Color.FromName("#166797");
                e.Row.Cells[5].ForeColor = Color.FromName("#166797");
                e.Row.Cells[6].ForeColor = Color.FromName("#166797");
                e.Row.Cells[7].ForeColor = Color.FromName("#166797");
                e.Row.Cells[8].ForeColor = Color.FromName("#166797");
                e.Row.Cells[9].ForeColor = Color.FromName("#166797");
                e.Row.Cells[10].ForeColor = Color.FromName("#166797");
                e.Row.Cells[11].ForeColor = Color.FromName("#166797");
                e.Row.Cells[13].ForeColor = Color.FromName("#166797");
                e.Row.Cells[14].ForeColor = Color.FromName("#166797");
                e.Row.Cells[15].ForeColor = Color.FromName("#166797");

                //  e.Row.Cells[10].ToolTip = "Todas las facturas pagadas.";                
                RegistrosSelecc = Convert.ToInt32(lblRegistrosSelecc.Text) + 1;
                lblRegistrosSelecc.Text = RegistrosSelecc.ToString();
                ImporteTotal = Convert.ToDecimal(lblImportSelec.Text) + oitem.dImporte;

                String ImporTotal;
                decimal dSubTotall = 0;
                decimal.TryParse(ImporteTotal.ToString(), out dSubTotall);
                ImporTotal = dSubTotall.ToString("N2");

                lblImportSelec.Text = ImporTotal.ToString();
            }

             if (dataKey.Values["sTiempoSituacion"].ToString() == "PorVencer" && dataKey.Values["sSituacion"].ToString() == "Falta Pagar") // La factura no esta pagada y esta por cumplirse el plazo de los 60 dias
             {
                        chkSeleccion.Checked = false;
                        chkSeleccion.Enabled = false;
                        // e.Row.Cells[10].ForeColor = Color.FromName("#CA3634");
               
                e.Row.Cells[10].ToolTip = "Facturas pendiente de pago - Plazo de liquidacion proximo al vencimiento.";

                e.Row.Cells[4].ToolTip = "Facturas pendiente de pago - Plazo de liquidacion proximo al vencimiento.";
                e.Row.Cells[5].ToolTip = "Facturas pendiente de pago - Plazo de liquidacion proximo al vencimiento.";
                e.Row.Cells[6].ToolTip = "Facturas pendiente de pago - Plazo de liquidacion proximo al vencimiento.";
                e.Row.Cells[7].ToolTip = "Facturas pendiente de pago - Plazo de liquidacion proximo al vencimiento.";
                e.Row.Cells[8].ToolTip = "Facturas pendiente de pago - Plazo de liquidacion proximo al vencimiento.";
                e.Row.Cells[9].ToolTip = "Facturas pendiente de pago - Plazo de liquidacion proximo al vencimiento.";
                e.Row.Cells[10].ToolTip = "Facturas pendiente de pago - Plazo de liquidacion proximo al vencimiento.";
                e.Row.Cells[11].ToolTip = "Facturas pendiente de pago - Plazo de liquidacion proximo al vencimiento.";
                e.Row.Cells[13].ToolTip = "Facturas pendiente de pago - Plazo de liquidacion proximo al vencimiento.";

                e.Row.Cells[4].ForeColor = Color.FromName("#CA3634");
                e.Row.Cells[5].ForeColor = Color.FromName("#CA3634");
                e.Row.Cells[6].ForeColor = Color.FromName("#CA3634");
                e.Row.Cells[7].ForeColor = Color.FromName("#CA3634");
                e.Row.Cells[8].ForeColor = Color.FromName("#CA3634");
                e.Row.Cells[9].ForeColor = Color.FromName("#CA3634");
                e.Row.Cells[10].ForeColor = Color.FromName("#CA3634");
                e.Row.Cells[11].ForeColor = Color.FromName("#CA3634");
                e.Row.Cells[13].ForeColor = Color.FromName("#CA3634");
                e.Row.Cells[14].ForeColor = Color.FromName("#CA3634");
                e.Row.Cells[15].ForeColor = Color.FromName("#CA3634");

            }

              if (dataKey.Values["sTiempoSituacion"].ToString() == "PorVencer" && dataKey.Values["sSituacion"].ToString() == "Pagado") // La factura ya esta pagada y esta por cumplirse el plazo de los 60 dias
              {
                    chkSeleccion.Checked = true;
                    chkSeleccion.Enabled = true;
               

                e.Row.Cells[4].ToolTip = "Plazo de liquidacion proximo al vencimiento.";
                e.Row.Cells[5].ToolTip = "Plazo de liquidacion proximo al vencimiento.";
                e.Row.Cells[6].ToolTip = "Plazo de liquidacion proximo al vencimiento.";
                e.Row.Cells[7].ToolTip = "Plazo de liquidacion proximo al vencimiento.";
                e.Row.Cells[8].ToolTip = "Plazo de liquidacion proximo al vencimiento.";
                e.Row.Cells[9].ToolTip = "Plazo de liquidacion proximo al vencimiento.";
                e.Row.Cells[10].ToolTip = "Plazo de liquidacion proximo al vencimiento.";
                e.Row.Cells[11].ToolTip = "Plazo de liquidacion proximo al vencimiento.";
                e.Row.Cells[13].ToolTip = "Plazo de liquidacion proximo al vencimiento.";

                

                //e.Row.Cells[13].BackColor = Color.FromName("#F39999");

               //e.Row.Cells[13].ForeColor = System.Drawing.Color.Red;
               // e.Row.Cells[13].BackColor = System.Drawing.Color.Red;

                e.Row.Cells[4].ForeColor = Color.FromName("#CA3634");
                e.Row.Cells[5].ForeColor = Color.FromName("#CA3634");
                e.Row.Cells[6].ForeColor = Color.FromName("#CA3634");
                e.Row.Cells[7].ForeColor = Color.FromName("#CA3634");
                e.Row.Cells[8].ForeColor = Color.FromName("#CA3634");
                e.Row.Cells[9].ForeColor = Color.FromName("#CA3634");
                e.Row.Cells[10].ForeColor = Color.FromName("#CA3634");
                e.Row.Cells[11].ForeColor = Color.FromName("#CA3634");
                e.Row.Cells[13].ForeColor = Color.FromName("#CA3634");
                e.Row.Cells[14].ForeColor = Color.FromName("#CA3634");
                e.Row.Cells[15].ForeColor = Color.FromName("#CA3634");

            }

              if (dataKey.Values["sTiempoSituacion"].ToString() == "EnPlazo" && dataKey.Values["sSituacion"].ToString() == "Falta Pagar") // La factura no esta pagada y esta por cumplirse el plazo de los 60 dias
              {
                        chkSeleccion.Checked = false;
                        chkSeleccion.Enabled = false;
                        e.Row.Cells[10].ToolTip = "Facturas pendientes de pago";
                
                e.Row.Cells[4].ToolTip = "Facturas pendientes de pago";
                e.Row.Cells[5].ToolTip = "Facturas pendientes de pago";
                e.Row.Cells[6].ToolTip = "Facturas pendientes de pago";
                e.Row.Cells[7].ToolTip = "Facturas pendientes de pago";
                e.Row.Cells[8].ToolTip = "Facturas pendientes de pago";
                e.Row.Cells[9].ToolTip = "Facturas pendientes de pago";
                e.Row.Cells[13].ToolTip = "Facturas pendientes de pago";

                e.Row.Cells[4].ForeColor = Color.FromName("#939292");
                e.Row.Cells[5].ForeColor = Color.FromName("#939292");
                e.Row.Cells[6].ForeColor = Color.FromName("#939292");
                e.Row.Cells[7].ForeColor = Color.FromName("#939292");
                e.Row.Cells[8].ForeColor = Color.FromName("#939292");
                e.Row.Cells[9].ForeColor = Color.FromName("#939292");
                e.Row.Cells[10].ForeColor = Color.FromName("#939292");
                e.Row.Cells[11].ForeColor = Color.FromName("#939292");
                e.Row.Cells[13].ForeColor = Color.FromName("#939292");
                e.Row.Cells[14].ForeColor = Color.FromName("#939292");
                e.Row.Cells[15].ForeColor = Color.FromName("#939292");

            }


            UpdatePanel1.Update();
        }
    }

    //protected void btnCerrar_Click(object sender, EventArgs e)
    //{
    //    BE_AcuerdoComision oBE_Acuerdo = new BE_AcuerdoComision();
    //    BL_AcuerdoComision oBL_Acuerdo = new BL_AcuerdoComision();

    //    //if (ChekTipoFactura.Checked == true)
    //    //{
    //    //    oBE_Acuerdo.iIdComisionista = GlobalEntity.Instancia.IdCliente;
    //    //    oBE_Acuerdo.sUsuario = GlobalEntity.Instancia.Usuario;
    //    //    oBE_Acuerdo.sAceptaCondicion = ChekTipoFactura.Checked;
    //    //    String oRpt = oBL_Acuerdo.Ins_AceptaCondiciones(oBE_Acuerdo).ToString();
    //    //    if (oRpt == "1")
    //    //    {
    //    //        mCondiciones.Hide();
    //    //    }
    //    //    else
    //    //        SCA_MsgInformacion("Vuelva iniciar session porfavor.");


    //    //}
    //    //else
    //    //    SCA_MsgInformacion("Debe Aceptar Terminos y Condiciones.");
    //}

    protected void btnCancelar_Click(object sender, EventArgs e)
    {
        Response.Redirect("../Seguridad/eFENIX_Inicio.aspx");
    }

    //protected void ChekTipoFactura_CheckedChanged(object sender, EventArgs e)
    //{
    //    if (ChekTipoFactura.Checked == true)
    //    {
    //        btnAceptar.Enabled = true;
    //    }
    //    else
    //        btnAceptar.Enabled = false;
    //}

    protected void ImgBtnBuscarr_Click(object sender, EventArgs e)
    {

        try
        {
            lblTotalRegistros.Text = "0";
            lblRegistrosSelecc.Text = "0";
            lblImportSelec.Text = "0";
            LlenarGrilla();

        }
        catch (Exception ex)
        {

        }


    }

    protected void ImgBtnGrabar_Click(object sender, EventArgs e)
    {

        try
        {

            if(GlobalEntity.Instancia.Usuario == null || GlobalEntity.Instancia.Usuario == "")
            {
                SCA_MsgInformacion("Acaba de perder la session. Vuelva loguearse.");
                return;
            }
            Int32 oResp = 0;
            Int32 oRespDet = 0;
            BL_AcuerdoComision oBL_AcuerdoComision = new BL_AcuerdoComision();
            oBE_AcuerdoComision = new BE_AcuerdoComision();
            Int32 iIdLiquidacion = 0;
            Int32 IdComisionista = GlobalEntity.Instancia.IdCliente;
            
            if (GrvListado.Rows.Count == 0)
            {
                // oResp = -1;
                SCA_MsgInformacion("No hay registros seleccionados");
                return;
            }
            else
            {
                oBE_AcuerdoComision.iIdComisionista = IdComisionista;
                oBE_AcuerdoComision.sUsuario = GlobalEntity.Instancia.Usuario;
                oBE_AcuerdoComision.iTipoTran = 1;
                oResp = oBL_AcuerdoComision.GrabarLiquidacion(oBE_AcuerdoComision);
            }

            if (oResp > 0)
            {
                iIdLiquidacion = oResp;

                foreach (GridViewRow GrvRow in GrvListado.Rows)
                {
                    if ((GrvRow.FindControl("chkSeleccionar") as System.Web.UI.WebControls.CheckBox).Checked)
                    {
                        if (GrvListado.DataKeys[GrvRow.RowIndex].Values["sSituacion"].ToString() == "Pagado")
                        {
                            oBE_AcuerdoComision.iLiquidacion = iIdLiquidacion;
                            oBE_AcuerdoComision.iIdDocPagoDet = Convert.ToInt32(GrvListado.DataKeys[GrvRow.RowIndex].Values["iIdDocPagoDet"].ToString());
                            oBE_AcuerdoComision.iIdDocOriDet = Convert.ToInt32(GrvListado.DataKeys[GrvRow.RowIndex].Values["iIdDocOriDet"].ToString());
                            oBE_AcuerdoComision.IidAcuerdoComision = Convert.ToInt32(GrvListado.DataKeys[GrvRow.RowIndex].Values["IidAcuerdoComision"].ToString());

                            oBE_AcuerdoComision.sRetirado = GrvListado.DataKeys[GrvRow.RowIndex].Values["sRetirado"].ToString();
                            oBE_AcuerdoComision.sFacturado = GrvListado.DataKeys[GrvRow.RowIndex].Values["sFacturado"].ToString();
                            oBE_AcuerdoComision.IidTarifa = Convert.ToInt32(GrvListado.DataKeys[GrvRow.RowIndex].Values["IidTarifa"].ToString());
                            oBE_AcuerdoComision.iIdMoneda = 23;
                            oBE_AcuerdoComision.dImporte = Convert.ToDecimal(GrvListado.DataKeys[GrvRow.RowIndex].Values["dImporte"].ToString());
                            oBE_AcuerdoComision.sSituacion = GrvListado.DataKeys[GrvRow.RowIndex].Values["sSituacion"].ToString();

                            oBE_AcuerdoComision.sPqtSLI = GrvListado.DataKeys[GrvRow.RowIndex].Values["sPqtSLI"].ToString();
                            oBE_AcuerdoComision.iIdDetpaqSLI = Convert.ToInt32(GrvListado.DataKeys[GrvRow.RowIndex].Values["iIdDetpaqSLI"].ToString());
                            oBE_AcuerdoComision.sUsuario = GlobalEntity.Instancia.Usuario;

                            oRespDet = oBL_AcuerdoComision.GrabarLiquidacionDetalle(oBE_AcuerdoComision);
                        }

                    }

                }

                if (oRespDet == 1)
                {
                    String mensaje;
                    mensaje = "Liquidacion " + iIdLiquidacion + " generada correctamente." + "\\n";
                    mensaje += "Espere aprobación para generar su factura." +"\\n";
                    mensaje += "Obs: Para una nueva liquidacion genera nuevamente la consulta.";
                    SCA_MsgInformacion(mensaje);
                    lblTotalRegistros.Text = "0";
                    lblRegistrosSelecc.Text = "0";
                    lblImportSelec.Text = "0";
                    UpdatePanel1.Update();
                    //Limpiando
                    List<BE_AcuerdoComision> oLista_Acuerdo = new List<BE_AcuerdoComision>();
                    oBE_AcuerdoComision = new BE_AcuerdoComision();
                    //oLista_Acuerdo.Add(oBE_AcuerdoComision);
                    GrvListado.DataSource = oLista_Acuerdo;
                    GrvListado.DataBind();
                }

            }
            else
                SCA_MsgInformacion("No se realizo la Liquidacion");

        }
        catch (Exception ex)
        {
            SCA_MsgInformacion("Catch");
        }




    }

    protected void ImgBtnRecalcular_Click(object sender, EventArgs e)
    {
        try
        {
            BL_AcuerdoComision oBL_AcuerdoComision = new BL_AcuerdoComision();
            oBE_AcuerdoComision = new BE_AcuerdoComision();

            String sIdLiquidacion = String.Empty;
            Int32 oRespDet = 0;
            Int32 iLeido = 0;
            Int32 iSeleccionados = 0;
            decimal? Importe = 0;
            String ImporteTotal = "0";


            foreach (GridViewRow GrvRow in GrvListado.Rows)
            {
                if (GrvRow.RowType == DataControlRowType.DataRow)
                {
                    //if (GrvRow.Cells[3].Text != "Aprobado")
                    //{
                    //    LinkButton lbtn = row.FindControl("LinkButton1") as LinkButton;
                    //    lbtn.ForeColor = System.Drawing.Color.Red;
                    //}

                    if (GrvListado.DataKeys[GrvRow.RowIndex].Values["sSituacion"].ToString() == "Falta Pagar")
                    {
                        GrvRow.Cells[4].ForeColor = Color.FromName("#B9B8C");
                        GrvRow.Cells[5].ForeColor = Color.FromName("#B9B8C");
                        GrvRow.Cells[6].ForeColor = Color.FromName("#B9B8C");
                        GrvRow.Cells[7].ForeColor = Color.FromName("#B9B8C");
                        GrvRow.Cells[8].ForeColor = Color.FromName("#B9B8C");
                        GrvRow.Cells[9].ForeColor = Color.FromName("#B9B8C");
                        GrvRow.Cells[10].ForeColor = Color.FromName("#B9B8C");
                        GrvRow.Cells[11].ForeColor = Color.FromName("#B9B8C");
                        GrvRow.Cells[13].ForeColor = Color.FromName("#B9B8C");
                        GrvRow.Cells[14].ForeColor = Color.FromName("#B9B8C");
                    }

                    if(GrvListado.DataKeys[GrvRow.RowIndex].Values["sSituacion"].ToString() == "Pagado")
                    {
                       
                        GrvRow.Cells[4].ForeColor = Color.FromName("#166797");
                        GrvRow.Cells[5].ForeColor = Color.FromName("#166797");
                        GrvRow.Cells[6].ForeColor = Color.FromName("#166797");
                        GrvRow.Cells[7].ForeColor = Color.FromName("#166797");
                        GrvRow.Cells[8].ForeColor = Color.FromName("#166797");
                        GrvRow.Cells[9].ForeColor = Color.FromName("#166797");
                        GrvRow.Cells[10].ForeColor = Color.FromName("#166797");
                        GrvRow.Cells[11].ForeColor = Color.FromName("#166797");
                        GrvRow.Cells[13].ForeColor = Color.FromName("#166797");
                        GrvRow.Cells[14].ForeColor = Color.FromName("#166797");
                        GrvRow.Cells[15].ForeColor = Color.FromName("#166797");
                    }

                    if(GrvListado.DataKeys[GrvRow.RowIndex].Values["sTiempoSituacion"].ToString() == "PorVencer" && GrvListado.DataKeys[GrvRow.RowIndex].Values["sSituacion"].ToString() == "Falta Pagar")
                    {                        
                        GrvRow.Cells[4].ForeColor = Color.FromName("#CA3634");
                        GrvRow.Cells[5].ForeColor = Color.FromName("#CA3634");
                        GrvRow.Cells[6].ForeColor = Color.FromName("#CA3634");
                        GrvRow.Cells[7].ForeColor = Color.FromName("#CA3634");
                        GrvRow.Cells[8].ForeColor = Color.FromName("#CA3634");
                        GrvRow.Cells[9].ForeColor = Color.FromName("#CA3634");
                        GrvRow.Cells[10].ForeColor = Color.FromName("#CA3634");
                        GrvRow.Cells[11].ForeColor = Color.FromName("#CA3634");
                        GrvRow.Cells[13].ForeColor = Color.FromName("#CA3634");
                        GrvRow.Cells[14].ForeColor = Color.FromName("#CA3634");
                        GrvRow.Cells[15].ForeColor = Color.FromName("#CA3634");

                    }

                    if (GrvListado.DataKeys[GrvRow.RowIndex].Values["sTiempoSituacion"].ToString() == "PorVencer" && GrvListado.DataKeys[GrvRow.RowIndex].Values["sSituacion"].ToString() == "Pagado")
                    {
                        GrvRow.Cells[4].ForeColor = Color.FromName("#CA3634");
                        GrvRow.Cells[5].ForeColor = Color.FromName("#CA3634");
                        GrvRow.Cells[6].ForeColor = Color.FromName("#CA3634");
                        GrvRow.Cells[7].ForeColor = Color.FromName("#CA3634");
                        GrvRow.Cells[8].ForeColor = Color.FromName("#CA3634");
                        GrvRow.Cells[9].ForeColor = Color.FromName("#CA3634");
                        GrvRow.Cells[10].ForeColor = Color.FromName("#CA3634");
                        GrvRow.Cells[11].ForeColor = Color.FromName("#CA3634");
                        GrvRow.Cells[13].ForeColor = Color.FromName("#CA3634");
                        GrvRow.Cells[14].ForeColor = Color.FromName("#CA3634");
                        GrvRow.Cells[15].ForeColor = Color.FromName("#CA3634");

                    }

                    if (GrvListado.DataKeys[GrvRow.RowIndex].Values["sTiempoSituacion"].ToString() == "EnPlazo" && GrvListado.DataKeys[GrvRow.RowIndex].Values["sSituacion"].ToString() == "Falta Pagar")
                    {
                        GrvRow.Cells[4].ForeColor = Color.FromName("#939292");
                        GrvRow.Cells[5].ForeColor = Color.FromName("#939292");
                        GrvRow.Cells[6].ForeColor = Color.FromName("#939292");
                        GrvRow.Cells[7].ForeColor = Color.FromName("#939292");
                        GrvRow.Cells[8].ForeColor = Color.FromName("#939292");
                        GrvRow.Cells[9].ForeColor = Color.FromName("#939292");
                        GrvRow.Cells[10].ForeColor = Color.FromName("#939292");
                        GrvRow.Cells[11].ForeColor = Color.FromName("#939292");
                        GrvRow.Cells[13].ForeColor = Color.FromName("#939292");
                        GrvRow.Cells[14].ForeColor = Color.FromName("#939292");
                        GrvRow.Cells[15].ForeColor = Color.FromName("#939292");

                    }
                    
                }
                
                if ((GrvRow.FindControl("chkSeleccionar") as System.Web.UI.WebControls.CheckBox).Checked)
                {
                    oBE_AcuerdoComision.dImporte = Convert.ToDecimal(GrvListado.DataKeys[GrvRow.RowIndex].Values["dImporteVisual"].ToString());
                    Importe += oBE_AcuerdoComision.dImporte;
                    iSeleccionados += 1;                   
                }

                iLeido += 1;
            }
            decimal dSubTotal = 0;
            decimal.TryParse(Importe.ToString(), out dSubTotal);
            ImporteTotal = dSubTotal.ToString("N2");

            lblRegistrosSelecc.Text = iSeleccionados.ToString();
            lblImportSelec.Text = ImporteTotal;
            lblTotalRegistros.Text = iLeido.ToString();
            UpdatePanel1.Update();
        }
        catch (Exception ex)
        {
            SCA_MsgInformacion("Eror");
        }
    }

    protected void ImgBtnExportar_Click(object sender, EventArgs e)
    {
        try
        {
            if (GrvListado.Rows.Count == 0)
            {
                // oResp = -1;
                SCA_MsgInformacion("Primero genere la información a exportar en excel.");
                return;
            }
            LlenarGrilla();
            pExportaExcel(Response, GrvListado);
        }
        catch(Exception ex)
        {

        }
       
        

        //Response.Clear();
        //Response.Buffer = true;
        //Response.Charset = "";
        //Response.AddHeader("content-disposition", "attachment;filename=Data.xls");
        //Response.ContentType = "application/ms-excel";
        //StringWriter sw = new StringWriter();
        //HtmlTextWriter hw = new HtmlTextWriter(sw);
        //GrvListado.DataBind();
        //GrvListado.RenderControl(hw);
        //Response.Output.Write(sw.ToString());
        //Response.Flush();
        //Response.End();
    }
    
    public static  void pExportaExcel(System.Web.HttpResponse sResponse, GridView oDataGrid)
    {
        StringBuilder sb = new StringBuilder();
        StringWriter sw = new StringWriter(sb);
        HtmlTextWriter htw = new HtmlTextWriter(sw);

        Page mpage = new Page();
        HtmlForm mform = new HtmlForm();

        oDataGrid.EnableViewState = false;

        oDataGrid.SelectedIndex = -1;
        oDataGrid.DataBind();       
        oDataGrid.AllowPaging = false;

        oDataGrid.HeaderRow.Style.Add("background-color", "#0069ae");
      
        for (int i = 0; i < oDataGrid.HeaderRow.Cells.Count; i++)
        {
            oDataGrid.HeaderRow.Cells[i].Style.Add("background-color", "#0069ae");
            oDataGrid.HeaderRow.Cells[i].Style.Add("height", "40");
            oDataGrid.HeaderRow.Cells[0].Visible = false;
        }

      //  MiGridView.HeaderRow.Cells[1].Visible = false;
        for (int i = 0; i < oDataGrid.Rows.Count; i++)
            oDataGrid.Rows[i].Cells[0].Visible = false;
      

        mpage.EnableEventValidation = false;
        mpage.DesignerInitialize();
        mpage.Controls.Add(mform);
        mform.Controls.Add(oDataGrid);

        mpage.RenderControl(htw);

        sResponse.Clear();
        sResponse.Buffer = true;
        //sResponse.ContentType = "application/vnd.ms-excel";
        sResponse.ContentType = "application/ms-excel";
        sResponse.AddHeader("Content-Disposition", "attachment;filename=Data-Referencial.xls");
        sResponse.Charset = "UTF-8";
        sResponse.ContentEncoding = Encoding.Default;
        sResponse.Write(sb.ToString());
        //Page_Load(c, e);
        sResponse.End();
      
       
    }
    public override void VerifyRenderingInServerForm(System.Web.UI.Control control)
    {
        //base.VerifyRenderingInServerForm(control);
    }

}