﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FENIX.BusinessLogic;
using FENIX.BusinessEntity;

public partial class Extranet_Gerencia_eFENIX_AprobarDsctoAlmacenaje : System.Web.UI.Page
{
    private void SCA_MsgInformacion(string strError)
    {
        MensajeScript(strError);
    }

    private void MensajeScript(string SMS)
    {
        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "", "<script>alert('" + SMS + "');</script>", false);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            ViewState["Usuario"] = Request.QueryString["Usuario"];
            ViewState["IdDscto"] = Request.QueryString["IdDscto"];
            RblAprobacion.SelectedIndex = 1;

            BE_DocumentoOrigen oBE_DocumentoOrigen = new BE_DocumentoOrigen();
            BL_DocumentoOrigen oBL_DocumentoOrigen = new BL_DocumentoOrigen();
            List<BE_DocumentoOrigen> LstDocOrigen = new List<BE_DocumentoOrigen>();

            LstDocOrigen = oBL_DocumentoOrigen.BuscaDatosDsctoAlmac(Convert.ToInt32(ViewState["IdDscto"].ToString()));

            if (LstDocOrigen.Count > 0)
            {
                LbLCliente.Text = LstDocOrigen[0].sCliente.ToString().Trim().ToUpper();
                LbLAgCarga.Text = LstDocOrigen[0].sAgenciaCarga.ToString().Trim().ToUpper();
                LbLAgAduana.Text = LstDocOrigen[0].sAgenciaAduana.ToString().Trim().ToUpper();
                LblDocOrigen.Text = LstDocOrigen[0].sDocumentoHijo.ToString();
                LblFecha.Text = LstDocOrigen[0].sFechaSolicitud.ToString();
                LblDiasAlmacen.Text = LstDocOrigen[0].iDiasAlmacen.ToString();
                LblPorcentaje.Text = LstDocOrigen[0].iTasaDscto.ToString();
                LblDescuento.Text = LstDocOrigen[0].dImpDscto.ToString();
                btnAceptar.Enabled = true;
            }
            else
            {
                LbLCliente.Text = "Datos No Existen";
                LbLAgCarga.Text = "Datos No Existen";
                LbLAgAduana.Text = "Datos No Existen";
                LblDocOrigen.Text = "Datos No Existen";
                LblFecha.Text = "Datos No Existen";
                LblPorcentaje.Text = "0";
                LblDiasAlmacen.Text = "0";
                LblDescuento.Text = "0";
                RblAprobacion.Enabled = false;
                btnAceptar.Enabled = false;
            }
        }
    }

    protected void btnAceptar_Click(object sender, EventArgs e)
    {
        BE_DocumentoOrigen oBE_DocumentoOrigen = new BE_DocumentoOrigen();
        BL_DocumentoOrigen oBL_DocumentoOrigen = new BL_DocumentoOrigen();

        oBE_DocumentoOrigen.iIdDscto = Convert.ToInt32(ViewState["IdDscto"].ToString());
        oBE_DocumentoOrigen.sUsuario = ViewState["Usuario"].ToString();
        oBE_DocumentoOrigen.sAcepta = RblAprobacion.SelectedValue;

        Int32 vl_iCodigo = 0;
        String vl_sMessage = String.Empty;
        String[] sResultado = oBL_DocumentoOrigen.GrabarAutorizacDscto(oBE_DocumentoOrigen).Split('|');

        for (int i = 0; i < sResultado.Length; i++)
        {
            if (i == 0)
            {
                vl_iCodigo = Convert.ToInt32(sResultado[i]);
            }
            else
            {
                vl_sMessage += sResultado[i];
            }
        }
        SCA_MsgInformacion(vl_sMessage);

        btnAceptar.Enabled = false;

    }
}