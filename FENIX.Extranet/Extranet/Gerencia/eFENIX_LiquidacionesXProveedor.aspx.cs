﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using wsExtranet.SSRSws;
using System.Web.Services;
using System.Web.Script.Serialization;
using QNET.Web.UI.Controls;
using FENIX.BusinessEntity;
using FENIX.BusinessLogic;
using FENIX.DataAccess;
using System.Configuration;
using System.Web.Security;

public partial class Extranet_Gerencia_eFENIX_LiquidacionesXProveedor : System.Web.UI.Page
{
    String urlLiquidacion = ConfigurationManager.AppSettings["urlLiquidacion"].ToString();
    String userSSRS = ConfigurationManager.AppSettings["userSSRS"].ToString();
    String passwordSSRS = ConfigurationManager.AppSettings["passwordSSRS"].ToString();
    String domainSSRS = ConfigurationManager.AppSettings["domainSSRS"].ToString();

    private void SCA_MsgInformacion(string strError)
    {
        MensajeScript(strError);
    }

    private void MensajeScript(string SMS)
    {
        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "", "<script>alert('" + SMS + "');</script>", false);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        dnvListado.BindGridView += new QNET.Web.UI.Controls.DataNavigator.BindGridViewDelegate(BindGridLista);

        ScriptManager scriptManager = ScriptManager.GetCurrent(this.Page);
        scriptManager.RegisterPostBackControl(this.GrvListado);

        String tiempo = (Convert.ToInt32(GlobalEntity.Instancia.TiempoDuracionSesion.ToString()) * 1000 * 60).ToString();
        Session["Reset"] = true;

        if (!Page.IsPostBack)
        {
            txtLiquidacion.Attributes.Add("onkeypress", "javascript:return SoloEnteros(event);");
            if (GrvListado.Rows.Count == 0)
            {
                ListarDropt();

                BE_Cliente oBE_Cliente = new BE_Cliente();
                oBE_Cliente.sIdCliente = GlobalEntity.Instancia.IdCliente.ToString();
                idLblComisionista.Text = GlobalEntity.Instancia.NombreCliente.ToString();

            }
            ScriptManager.RegisterStartupScript(this, GetType(), "mensaje", "SessionExpireAlert(" + tiempo + ");", true);

        }
        else
            ScriptManager.RegisterStartupScript(this, GetType(), "mensaje", "SessionExpireAlert(" + tiempo + ");", true); //ScriptManager.RegisterStartupScript(this, GetType(), "mensaje", "myStopFunction();", true);

    }

    protected void btn_cerrar_Click(object sender, EventArgs e)
    {
        if (GlobalEntity.Instancia.CerrarExtranet == "S")
        {
            BL_Usuario oBL_Usuario = new BL_Usuario();
            oBL_Usuario.GrabaInicioSesion(GlobalEntity.Instancia.Usuario, "0");
            Session.Abandon();
            FormsAuthentication.SignOut();
            Response.Redirect("../../eFENIX_Login.aspx");
            Response.End();
        }
    }

    public void LlenarGrilla()
    {
        try
        {
            BE_AcuerdoComision oBE_AcuerdoComision;
            BL_AcuerdoComision oBL_AcuerdoComision;
            oBL_AcuerdoComision = new BL_AcuerdoComision();
            oBE_AcuerdoComision = new BE_AcuerdoComision();

            oBE_AcuerdoComision.iIdComisionista = Convert.ToInt32(GlobalEntity.Instancia.IdCliente);
            //oBE_AcuerdoComision.iIdMoneda = Convert.ToInt32(dplMonegaPagar.SelectedValue);

            GrvListado.PageSize = 50;
            oBE_AcuerdoComision.NPagina = dnvListado.CurrentPage;
            oBE_AcuerdoComision.NRegistros = GrvListado.PageSize;

            List<BE_AcuerdoComision> oLista_Acuerdo = new List<BE_AcuerdoComision>();
            oLista_Acuerdo = oBL_AcuerdoComision.Listar_LiquidacionXComisionista(oBE_AcuerdoComision);


            //GrvListado.Width = 1055;
            //GrvListado.PageSize = 10;
            dnvListado.Visible = (oLista_Acuerdo.Count() > 0);
            GrvListado.DataSource = oLista_Acuerdo;
            GrvListado.DataBind();
            upDocumentoOrigen.Update();

        }

        catch (Exception ex)
        {

        }
    }


    //protected void ImgBtnBuscar_Click(object sender, ImageClickEventArgs e)
    //{
    //    // LlenarGrilla();


    //    dnvListado.InvokeBind();

    //    if (GrvListado.Rows.Count == 0)
    //    {
    //        List<BE_AcuerdoComision> oLista_Acuerdo = new List<BE_AcuerdoComision>();
    //        BE_AcuerdoComision oBE_AcuerdoComision = new BE_AcuerdoComision();
    //        oLista_Acuerdo.Add(oBE_AcuerdoComision);
    //        GrvListado.DataSource = oLista_Acuerdo;
    //        GrvListado.DataBind();
    //    }
    //}

    //protected void btnConsulta_Click(object sender, EventArgs e)
    //{

    //    dnvListado.InvokeBind();

    //    if (GrvListado.Rows.Count == 0)
    //    {
    //        List<BE_AcuerdoComision> oLista_Acuerdo = new List<BE_AcuerdoComision>();
    //        BE_AcuerdoComision oBE_AcuerdoComision = new BE_AcuerdoComision();
    //        oLista_Acuerdo.Add(oBE_AcuerdoComision);
    //        GrvListado.DataSource = oLista_Acuerdo;
    //        GrvListado.DataBind();
    //    }
    //}

    //protected void ImgBtnBuscar_Click(object sender, EventArgs e)
    //{
    //    dnvListado.InvokeBind();

    //    if (GrvListado.Rows.Count == 0)
    //    {
    //        List<BE_AcuerdoComision> oLista_Acuerdo = new List<BE_AcuerdoComision>();
    //        BE_AcuerdoComision oBE_AcuerdoComision = new BE_AcuerdoComision();
    //        oLista_Acuerdo.Add(oBE_AcuerdoComision);
    //        GrvListado.DataSource = oLista_Acuerdo;
    //        GrvListado.DataBind();
    //    }
    //}

    protected void ImgBtnBuscar_Click(object sender, EventArgs e)
    {

        dnvListado.InvokeBind();

        if (GrvListado.Rows.Count == 0)
        {
            List<BE_AcuerdoComision> oLista_Acuerdo = new List<BE_AcuerdoComision>();
            BE_AcuerdoComision oBE_AcuerdoComision = new BE_AcuerdoComision();
            // oLista_Acuerdo.Add(oBE_AcuerdoComision);
            //GrvListado.DataSource = oLista_Acuerdo;
            // /GrvListado.DataBind();
        }


    }

    DataNavigatorParams BindGridLista(object sender, EventArgs e)
    {//
        BE_AcuerdoComision oBE_AcuerdoComision;
        BL_AcuerdoComision oBL_AcuerdoComision;
        List<BE_AcuerdoComision> oLista_Acuerdo = new List<BE_AcuerdoComision>();
        oBL_AcuerdoComision = new BL_AcuerdoComision();
        oBE_AcuerdoComision = new BE_AcuerdoComision();

        oBE_AcuerdoComision.iIdComisionista = Convert.ToInt32(GlobalEntity.Instancia.IdCliente);
        oBE_AcuerdoComision.iEstado = Convert.ToInt32(DplEstado.SelectedValue);
        oBE_AcuerdoComision.sMesIni = Request.Form["DatePickername"];
        oBE_AcuerdoComision.sMesFin = Request.Form["DatePickerFinname"];
        oBE_AcuerdoComision.sLiquidacion = txtLiquidacion.Text;

        var Inicio = Request.Form["DatePickername"];
        var Fin = Request.Form["DatePickerFinname"];

        if (Inicio == "")
            Inicio = Convert.ToString(DateTime.Today);

        if (Fin == "")
            Fin = Convert.ToString(DateTime.Today);


        if (Convert.ToDateTime(Inicio) > Convert.ToDateTime(Fin))
        {
            SCA_MsgInformacion("Fecha Fin no puede ser inferior a Fecha Inicio.");
        }
        else
        {


            GrvListado.PageSize = 50;
            oBE_AcuerdoComision.NPagina = dnvListado.CurrentPage;
            oBE_AcuerdoComision.NRegistros = GrvListado.PageSize;
            oLista_Acuerdo = oBL_AcuerdoComision.Listar_LiquidacionXComisionista(oBE_AcuerdoComision);

            if (oLista_Acuerdo.Count == 0 || oLista_Acuerdo == null)
            {
                SCA_MsgInformacion("No hay registros encontrados");
            }

            ViewState["oBE_AcuerdoComisionList"] = oLista_Acuerdo;

            //GrvListado.Width                             = 1055;       
            dnvListado.Visible = (oLista_Acuerdo.Count() > 0);
            //upDocumentoOrigen.Update();
        }

        return new DataNavigatorParams(oLista_Acuerdo, oBE_AcuerdoComision.NtotalRegistros);
    }

    protected void GrvListado_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        Int32 idLiquidacion = 0;
        String NombreZip = String.Empty;
        String verLiq = urlLiquidacion;
        String password = passwordSSRS;
        String domain = domainSSRS;
        if (e.CommandName == "OpenLiquidacion")
        {
            GridViewRow fila;
            fila = (GridViewRow)(((ImageButton)e.CommandSource).NamingContainer);
            idLiquidacion = Convert.ToInt32(GrvListado.DataKeys[fila.RowIndex].Values["iLiquidacion"]);
            //idLiquidacion = Convert.ToInt32(GrvListado.DataKeys[Convert.ToInt32(e.CommandArgument)].Values["iLiquidacion"].ToString());
            NombreZip = "Liquidacion - " + idLiquidacion.ToString();

            ReportExecutionService rs = new ReportExecutionService();
            rs.Credentials = new System.Net.NetworkCredential("userReport", "Reportes2019@", "FSAFARGOLINE107");
            rs.LoadReport(verLiq, null);

            List<ParameterValue> parameters = new List<ParameterValue>();
            parameters.Add(new ParameterValue { Name = "IdLiquidacion", Value = idLiquidacion.ToString() });
            rs.SetExecutionParameters(parameters.ToArray(), "en-US");
            String deviceInfo = "<DeviceInfo><Toolbar>False</Toolbar></DeviceInfo>";
            String mimeType;
            String encoding;
            String[] streamId;
            Warning[] warning;
            // Byte[] result = rs.Render("PDF", deviceInfo, out mimeType, out encoding, out encoding, out warning, out streamId);
            var result = rs.Render("PDF", deviceInfo, out mimeType, out encoding, out encoding, out warning, out streamId);



            Response.ClearHeaders();
            Response.Clear();
            Response.ContentType = "application/pdf";
            Response.AddHeader("content-disposition", "attachment;filename=" + NombreZip + ".pdf");
            Response.Buffer = true;
            Response.Cache.SetCacheability(System.Web.HttpCacheability.NoCache);
            Response.BinaryWrite(result);
            Response.Flush();
            HttpContext.Current.ApplicationInstance.CompleteRequest();
            Response.End();
            Response.Close();

        }

        if (e.CommandName == "OpenExcel")
        {
            GridViewRow fila;
            fila = (GridViewRow)(((ImageButton)e.CommandSource).NamingContainer);
            idLiquidacion = Convert.ToInt32(GrvListado.DataKeys[fila.RowIndex].Values["iLiquidacion"]);
            //idLiquidacion = Convert.ToInt32(GrvListado.DataKeys[Convert.ToInt32(e.CommandArgument)].Values["iLiquidacion"].ToString());
            NombreZip = "Liquidacion - " + idLiquidacion.ToString();

            ReportExecutionService rs = new ReportExecutionService();
            rs.Credentials = new System.Net.NetworkCredential("userReport", "Reportes2019@", "FSAFARGOLINE107");
            rs.LoadReport(verLiq, null);

            List<ParameterValue> parameters = new List<ParameterValue>();
            parameters.Add(new ParameterValue { Name = "IdLiquidacion", Value = idLiquidacion.ToString() });
            rs.SetExecutionParameters(parameters.ToArray(), "en-US");
            String deviceInfo = @"<DeviceInfo><Toolbar>False</Toolbar></DeviceInfo>";
            String mimeType;
            String encoding;
            String[] streamId;
            Warning[] warning;
            // Byte[] result = rs.Render("PDF", deviceInfo, out mimeType, out encoding, out encoding, out warning, out streamId);
            var result = rs.Render("EXCELOPENXML", deviceInfo, out mimeType, out encoding, out encoding, out warning, out streamId);



            Response.ClearHeaders();
            Response.Clear();
            Response.ContentType = "application/octet-stream";
            Response.AddHeader("content-disposition", "attachment;filename=" + NombreZip + ".xlsx");
            Response.Buffer = true;
            Response.Cache.SetCacheability(System.Web.HttpCacheability.NoCache);
            Response.BinaryWrite(result);
            Response.Flush();
            HttpContext.Current.ApplicationInstance.CompleteRequest();
            Response.End();
            Response.Close();

        }

    }



    protected void ListarDropt()
    {
        BL_Presupuesto oBL_Presupuesto = new BL_Presupuesto();
        BL_AcuerdoComision oBL_Acuerdo = new BL_AcuerdoComision();

        DplEstado.DataSource = oBL_Acuerdo.Listar_EstadosXLiquidacion();
        DplEstado.DataValueField = "iIdValor";
        DplEstado.DataTextField = "sValor";
        DplEstado.DataBind();
        DplEstado.Items.Add(new ListItem("[Todos]", "-1"));
        DplEstado.SelectedIndex = DplEstado.Items.IndexOf(DplEstado.Items.FindByValue("-1"));

    }


    protected void GrvListado_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            DataKey dataKey = this.GrvListado.DataKeys[e.Row.RowIndex];
            CheckBox chkSeleccion = (CheckBox)e.Row.FindControl("chkSeleccionar");
            TextBox txtSerie = (TextBox)e.Row.FindControl("TxtSerie");
            TextBox TxtFacturaComisionista = (TextBox)e.Row.FindControl("TxtFacturaComisionista");
            ImageButton btnRgistrarFactura = (ImageButton)e.Row.FindControl("ImgBRegistrarFactura");
            BE_AcuerdoComision oitem = (e.Row.DataItem as BE_AcuerdoComision);

            chkSeleccion.Attributes.Add("onclick", "HabilitarUno(this);");

            BE_AcuerdoComision oBE_AcuerdoComision;
            BL_AcuerdoComision oBL_AcuerdoComision;
            oBL_AcuerdoComision = new BL_AcuerdoComision();
            oBE_AcuerdoComision = new BE_AcuerdoComision();

            decimal dSubTotal = 0;
            decimal.TryParse(e.Row.Cells[3].Text, out dSubTotal);
            e.Row.Cells[3].Text = dSubTotal.ToString("N2");

            if (oitem.iIdSituacion != 0)
            {
                for (int c = 1; c < e.Row.Cells.Count; c++)
                {
                    txtSerie.Enabled = false;
                    TxtFacturaComisionista.Enabled = false;
                    chkSeleccion.Visible = false;



                    e.Row.Cells[c].Attributes.Add("onclick", string.Format("HabilitarUno(document.getElementById('{0}'));", chkSeleccion.ClientID));

                    if (dataKey.Values["iIdSituacion"].ToString() == "1") //PEND. PAGO
                    {
                        txtSerie.Enabled = false;
                        TxtFacturaComisionista.Enabled = false;
                        btnRgistrarFactura.Visible = false;
                        //e.Row.Cells[9].Visible = true;
                    }

                    if (dataKey.Values["iIdSituacion"].ToString() == "7" || dataKey.Values["iIdSituacion"].ToString() == "8" || dataKey.Values["iIdSituacion"].ToString() == "9") //PENDIENTE AUTORIZAR 7- JEFE COM / 8- G COMER / 9-GERT GEN
                    {
                        txtSerie.Enabled = false;
                        TxtFacturaComisionista.Enabled = false;
                        btnRgistrarFactura.Visible = false;
                        e.Row.ToolTip = "Espere aprobación para ingresar su factura";
                        //e.Row.Cells[9].Visible = true;
                    }

                    if (dataKey.Values["iIdSituacion"].ToString() == "5") //PENDIENTE REGISTRO DE FACTURA
                    {
                        txtSerie.Enabled = true;
                        TxtFacturaComisionista.Enabled = true;
                        chkSeleccion.Visible = true;
                        e.Row.Cells[8].Visible = true;
                        //e.Row.Cells[9].Visible = true;
                        //e.Row.Cells[8].Visible = true;
                    }

                    if (dataKey.Values["iIdSituacion"].ToString() == "4") //Anulado
                    {
                        txtSerie.Enabled = false;
                        TxtFacturaComisionista.Enabled = false;
                        chkSeleccion.Visible = false;
                        btnRgistrarFactura.ImageUrl = "../../Script/Imagenes/IconButton/checkTrueFactura.png";
                        btnRgistrarFactura.Visible = false;
                        //e.Row.Cells[9].Visible = true;
                    }

                    if (dataKey.Values["iIdSituacion"].ToString() == "3" || dataKey.Values["iIdSituacion"].ToString() == "10") //Pagado o Pend Solicitud AP.
                    {
                        txtSerie.Enabled = false;
                        TxtFacturaComisionista.Enabled = false;
                        chkSeleccion.Visible = false;
                        btnRgistrarFactura.Visible = false;
                        //e.Row.Cells[9].Visible = true;
                    }
                    if (dataKey.Values["iIdSituacion"].ToString() == "6") //Desaprobado
                    {
                        txtSerie.Enabled = false;
                        TxtFacturaComisionista.Enabled = false;
                        chkSeleccion.Visible = false;
                        btnRgistrarFactura.Visible = false;
                        //e.Row.Cells[9].Visible = true;
                    }
                }
            }
            if (oitem.iIdSituacion == 0)
            {
                e.Row.Cells[8].Visible = false;
                e.Row.Cells[9].Visible = false;
                e.Row.Cells[0].Visible = false;
                e.Row.Cells[7].Visible = false;
                e.Row.Cells[1].Visible = false;
                e.Row.Cells[2].Visible = false;
                e.Row.Cells[3].Visible = false;
                e.Row.Cells[4].Visible = false;
                e.Row.Cells[5].Visible = false;
                e.Row.Cells[6].Visible = false;
            }
        }
    }



    [WebMethod]
    public static string RegistrarFactura_Liquidacion(Int32 iLiquidacion, String sNroSerie, String sNroFactura, String smensaje)
    {


        Int32 IdLiquidacion;
        String sSerie;
        String sFactura;
        IdLiquidacion = iLiquidacion;
        sSerie = sNroSerie;
        sFactura = sNroFactura;

        BE_AcuerdoComision oBE_AcuerdoComision;
        BL_AcuerdoComision oBL_AcuerdoComision;
        oBL_AcuerdoComision = new BL_AcuerdoComision();
        oBE_AcuerdoComision = new BE_AcuerdoComision();
        oBE_AcuerdoComision.iLiquidacion = IdLiquidacion;
        oBE_AcuerdoComision.iIdDocComisionista = 0;
        oBE_AcuerdoComision.iIdComisionista = Convert.ToInt32(GlobalEntity.Instancia.IdCliente);
        oBE_AcuerdoComision.iTipoDoc = 1; //Factura
        oBE_AcuerdoComision.sDocNroSerie = sSerie;
        oBE_AcuerdoComision.sDocNroComisionista = "0000";
        oBE_AcuerdoComision.sDocNroComisionista += sFactura;

        oBE_AcuerdoComision.sNomPc = "";
        oBE_AcuerdoComision.sUsuario = GlobalEntity.Instancia.Usuario;



        String[] oRpt = oBL_AcuerdoComision.GrabaDocsComisionista(oBE_AcuerdoComision).ToString().Split('|');
        String Respuesta = oRpt[1];

        string myJsonString = (new JavaScriptSerializer()).Serialize(Respuesta);
        return myJsonString;

    }
}