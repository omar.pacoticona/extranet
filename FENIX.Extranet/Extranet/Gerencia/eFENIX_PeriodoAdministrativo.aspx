﻿<%@ Page Language="C#" MasterPageFile="~/Extranet/Seguridad/eFENIX_Principal.master"
    AutoEventWireup="true" CodeFile="eFENIX_PeriodoAdministrativo.aspx.cs" Inherits="Extranet_Operaciones_eFENIX_PeriodoAdministrativo"
    Title="Periodo Administrativo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CntMaster" runat="Server">
    <table cellspacing="0" cellpadding="0" style="width: 100%;">
        <tr>
            <td class="form_titulo" style="width: 85%">
                HABILITAR PERIODO - ADMINISTRATIVO
            </td>
            <td align="right" class="form_titulo_verde" style="width: 15%">
                <asp:TextBox ID="LblTotal" Text="" style="background-color: #0069ae; color: #FFFFFF" ReadOnly="true" BorderColor="#0069ae" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td valign="top" colspan="2">
                <table class="form_bandeja" style="margin-top: 5px; margin-bottom: 5px; width: 100%;">
                    <tr>
                        <td colspan="5">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 6%;">
                            Periodo
                        </td>
                        <td style="width: 6%;">
                            <asp:DropDownList ID="DplAnioPeriodo" runat="server" AutoPostBack="True" 
                                onselectedindexchanged="DplAnioPeriodo_SelectedIndexChanged">
                            </asp:DropDownList>
                        </td>
                        <td style="width: 6%;">
                            <asp:DropDownList ID="DplMesPeriodo" runat="server" Width="100px" 
                                AutoPostBack="True" onselectedindexchanged="DplMesPeriodo_SelectedIndexChanged">
                            </asp:DropDownList>
                        </td>
                        <td style="color: #008000; text-transform: none;">
                            Solo se puede habilitar un periodo anterior.
                        </td>
                        <td align="right">
                            <asp:ImageButton ID="BtnVerHistorial" runat="server" ImageUrl="~/Imagenes/Formulario/btn_historial.JPG"
                                OnClick="BtnVerHistorial_Click" />   
                        </td>
                    </tr>
                    <tr>
                        <td colspan="5">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td valign="top" colspan="2">
                <ajax:UpdatePanel ID="UpControles" UpdateMode="Conditional" runat="server">
                    <ContentTemplate>
                        <table class="form_alterno" style="margin-top: 5px; margin-bottom: 5px; width: 100%;">
                            <tr>
                                <td colspan="14">
                                    PROCESOS
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 2%;">
                                </td>
                                <td style="width: 10%;" colspan="2">
                                    Factura
                                </td>
                                <td style="width: 3%;">
                                </td>
                                <td style="width: 4%;">
                                </td>
                                <td style="width: 4%;">
                                </td>
                                <td style="width: 12%;">
                                </td>
                                <td style="width: 5%;">
                                </td>
                                <td style="width: 10%;" colspan="2">
                                    Nota de Crédito
                                </td>
                                <td style="width: 3%;">
                                </td>
                                <td style="width: 4%;">
                                </td>
                                <td style="width: 4%;">
                                </td>
                                <td>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                </td>
                                <td style="width: 2%; height: 26px;">
                                </td>
                                <td style="height: 26px">
                                    Serie 001
                                </td>
                                <td style="height: 26px">
                                    <asp:CheckBox ID="ChkFac001" runat="server" OnCheckedChanged="ChkFac001_CheckedChanged"
                                        AutoPostBack="True" />
                                </td>
                                <td style="height: 26px">
                                    días
                                </td>
                                <td style="height: 26px">
                                    <asp:TextBox ID="TxtDiasFac001" Width="30px" runat="server" MaxLength="2" Enabled="false" BackColor="WhiteSmoke"></asp:TextBox>
                                </td>
                                <td>
                                    <asp:Label ID="LblDiasFac001" runat="server"></asp:Label>
                                </td>
                                <td>
                                    &nbsp;</td>
                                <td style="width: 2%; height: 26px;">
                                    &nbsp;</td>
                                <td>
                                    Serie 001</td>
                                <td>
                                    <asp:CheckBox ID="ChkNC001" runat="server" AutoPostBack="True" 
                                        OnCheckedChanged="ChkNC001_CheckedChanged" />
                                </td>
                                <td>
                                    días</td>
                                <td>
                                    <asp:TextBox ID="TxtDiasNC001" runat="server" BackColor="WhiteSmoke" 
                                        Enabled="false" MaxLength="2" Width="30px"></asp:TextBox>
                                </td>
                                <td>
                                    <asp:Label ID="LblDiasNC001" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                </td>
                                <td>
                                </td>
                                <td>
                                    Serie 005
                                </td>
                                <td>
                                    <asp:CheckBox ID="ChkFac005" runat="server" OnCheckedChanged="ChkFac005_CheckedChanged"
                                        AutoPostBack="True" />
                                </td>
                                <td>
                                    días
                                </td>
                                <td>
                                    <asp:TextBox ID="TxtDiasFac005" Width="30px" runat="server" MaxLength="2" Enabled="false" BackColor="WhiteSmoke"></asp:TextBox>
                                </td>
                                <td>
                                    <asp:Label ID="LblDiasFac005" runat="server"></asp:Label>
                                </td>
                                <td>
                                    &nbsp;</td>
                                <td>
                                    &nbsp;</td>
                                <td>
                                    Serie 005</td>
                                <td>
                                    <asp:CheckBox ID="ChkNC005" runat="server" AutoPostBack="True" 
                                        OnCheckedChanged="ChkNC005_CheckedChanged" />
                                </td>
                                <td>
                                    días</td>
                                <td>
                                    <asp:TextBox ID="TxtDiasNC005" runat="server" BackColor="WhiteSmoke" 
                                        Enabled="false" MaxLength="2" Width="30px"></asp:TextBox>
                                </td>
                                <td>
                                    <asp:Label ID="LblDiasNC005" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                </td>
                                <td>
                                </td>
                                <td>
                                    Serie 006
                                </td>
                                <td>
                                    <asp:CheckBox ID="ChkFac006" runat="server" AutoPostBack="True" 
                                        OnCheckedChanged="ChkFac006_CheckedChanged" />
                                </td>
                                <td>
                                    días
                                </td>
                                <td>
                                    <asp:TextBox ID="TxtDiasFac006" Width="30px" runat="server" MaxLength="2" Enabled="false" BackColor="WhiteSmoke"></asp:TextBox>
                                </td>
                                <td>
                                    <asp:Label ID="LblDiasFac006" runat="server"></asp:Label>
                                </td>
                                <td>
                                    &nbsp;</td>
                                <td colspan="2">
                                    Nota de Débito</td>
                                <td>
                                    &nbsp;</td>
                                <td>
                                    &nbsp;</td>
                                <td>
                                    &nbsp;</td>
                                <td>
                                    &nbsp;</td>
                            </tr>
                            <tr>
                                <td>
                                </td>
                                <td>
                                </td>
                                <td>
                                    Serie 007
                                </td>
                                <td>
                                    <asp:CheckBox ID="ChkFac007" runat="server" AutoPostBack="True" 
                                        OnCheckedChanged="ChkFac007_CheckedChanged" />
                                </td>
                                <td>
                                    días
                                </td>
                                <td>
                                    <asp:TextBox ID="TxtDiasFac007" Width="30px" runat="server" MaxLength="2" Enabled="false" BackColor="WhiteSmoke"></asp:TextBox>
                                </td>
                                <td>
                                    <asp:Label ID="LblDiasFac007" runat="server"></asp:Label>
                                </td>
                                <td>
                                    &nbsp;</td>
                                <td>
                                    &nbsp;</td>
                                <td>
                                    Serie 001</td>
                                <td>
                                    <asp:CheckBox ID="ChkND001" runat="server" AutoPostBack="True" 
                                        OnCheckedChanged="ChkND001_CheckedChanged" />
                                </td>
                                <td>
                                    días</td>
                                <td>
                                    <asp:TextBox ID="TxtDiasND001" runat="server" BackColor="WhiteSmoke" 
                                        Enabled="false" MaxLength="2" Width="30px"></asp:TextBox>
                                </td>
                                <td>
                                    <asp:Label ID="LblDiasND001" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                </td>
                                <td>
                                </td>
                                <td>
                                    Serie 008
                                </td>
                                <td>
                                    <asp:CheckBox ID="ChkFac008" runat="server" AutoPostBack="True" 
                                        OnCheckedChanged="ChkFac008_CheckedChanged" />
                                </td>
                                <td>
                                    días
                                </td>
                                <td>
                                    <asp:TextBox ID="TxtDiasFac008" Width="30px" runat="server" MaxLength="2" Enabled="false" BackColor="WhiteSmoke"></asp:TextBox>
                                </td>
                                <td>
                                    <asp:Label ID="LblDiasFac008" runat="server"></asp:Label>
                                </td>
                                <td>
                                    &nbsp;</td>
                                <td>&nbsp;</td>
                                <td>
                                </td>
                                <td>
                                </td>
                                <td>
                                </td>
                                <td>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                </td>
                                <td colspan="2">
                                    Boleta
                                </td>
                                <td>
                                </td>
                                <td>
                                </td>
                                <td>
                                </td>
                                <td>
                                </td>
                                <td>
                                    &nbsp;</td>
                                <td colspan="2">
                                    facturac.electronica</td>
                                <td>
                                    &nbsp;</td>
                                <td>
                                    &nbsp;</td>
                                <td>
                                    &nbsp;</td>
                                <td>
                                    &nbsp;</td>
                            </tr>
                            <tr>
                                <td>
                                </td>
                                <td>
                                </td>
                                <td>
                                    Serie 001
                                </td>
                                <td>
                                    <asp:CheckBox ID="ChkBol001" runat="server" AutoPostBack="True" 
                                        OnCheckedChanged="ChkBol001_CheckedChanged" />
                                </td>
                                <td>
                                    días
                                </td>
                                <td>
                                    <asp:TextBox ID="TxtDiasBol001" Width="30px" runat="server" MaxLength="2" Enabled="false" BackColor="WhiteSmoke"></asp:TextBox>
                                </td>
                                <td>
                                    <asp:Label ID="LblDiasBol001" runat="server"></asp:Label>
                                </td>
                                <td>
                                    &nbsp;</td>
                                <td>
                                    &nbsp;</td>
                                <td>
                                    series todas</td>
                                <td>
                                    <asp:CheckBox ID="ChkFE" runat="server" AutoPostBack="True" 
                                        OnCheckedChanged="ChkFE_CheckedChanged" />
                                </td>
                                <td>
                                    días</td>
                                <td>
                                    <asp:TextBox ID="TxtDiasFE" runat="server" BackColor="WhiteSmoke" 
                                        Enabled="false" MaxLength="2" Width="30px"></asp:TextBox>
                                </td>
                                <td>
                                    <asp:Label ID="LblDiasFE" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="14">
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td align="center" colspan="13">
                                    <asp:ImageButton ID="ImgBtnGrabar" runat="server" ImageUrl="~/Imagenes/Formulario/btn_aceptar.JPG" 
                                        onclick="ImgBtnGrabar_Click" />
                                </td>
                                <td>
                                    &nbsp;</td>
                            </tr>
                            <tr>
                                <td colspan="14">
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td align="center" colspan="13">
                                    Si desea cerrar el periodo haga click <asp:LinkButton ID="BtnCerrarPeriodo" 
                                        runat="server" Text="Aqui" onclick="BtnCerrarPeriodo_Click"></asp:LinkButton>
                                </td>
                                <td align="left">
                                    &nbsp;</td>
                            </tr>
                        </table>
                    </ContentTemplate>
                    <Triggers>
                        <ajax:AsyncPostBackTrigger ControlID="DplAnioPeriodo" EventName="SelectedIndexChanged" />
                        <ajax:AsyncPostBackTrigger ControlID="DplMesPeriodo" EventName="SelectedIndexChanged" />
<asp:AsyncPostBackTrigger ControlID="DplAnioPeriodo" EventName="SelectedIndexChanged"></asp:AsyncPostBackTrigger>
<asp:AsyncPostBackTrigger ControlID="DplMesPeriodo" EventName="SelectedIndexChanged"></asp:AsyncPostBackTrigger>
<asp:AsyncPostBackTrigger ControlID="DplAnioPeriodo" EventName="SelectedIndexChanged"></asp:AsyncPostBackTrigger>
<asp:AsyncPostBackTrigger ControlID="DplMesPeriodo" EventName="SelectedIndexChanged"></asp:AsyncPostBackTrigger>
<asp:AsyncPostBackTrigger ControlID="DplAnioPeriodo" EventName="SelectedIndexChanged"></asp:AsyncPostBackTrigger>
<asp:AsyncPostBackTrigger ControlID="DplMesPeriodo" EventName="SelectedIndexChanged"></asp:AsyncPostBackTrigger>
<asp:AsyncPostBackTrigger ControlID="DplAnioPeriodo" EventName="SelectedIndexChanged"></asp:AsyncPostBackTrigger>
<asp:AsyncPostBackTrigger ControlID="DplMesPeriodo" EventName="SelectedIndexChanged"></asp:AsyncPostBackTrigger>
                    </Triggers>
                </ajax:UpdatePanel>
            </td>
        </tr>
    </table>
    
    <%-- Popup - Historial de Cambios--%>
    <asp:Panel ID="__PopPanel__" runat="server" CssClass="modalBackground" Style="left: 0px;
        top: 0px; display: none; position: fixed; z-index: 10000; display: none;" />
    
    <asp:Panel ID="pnlDetalle" runat="server" Style="width: 600px; height: 310px; 
        position: fixed; z-index: 100002; border: dimgray 1px solid; background-color: White; display: none;">
        <ajax:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <%--<asp:HiddenField ID="hdVarTiempoSesion" runat="server" />--%>
                <table border="0" cellpadding="0" cellspacing="0" width="100%" style="padding: 5px;">
                    <tr valign="top" class="form_titulo" style="background-color: #0069ae; height: 15px; text-transform: none;">
                        <td style="padding: 5px;">
                            Historial de Control Administrativo
                        </td>
                    </tr>
                    <tr>
                        <td valign="top" colspan="2">
                            <table class="form_bandeja" style="width: 100%;">
                                <tr>
                                    <td style="width:55px">
                                        Periodo:
                                    </td>
                                    <td>
                                        <asp:Label runat="server" ID="LblPeriodo" Text=""></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr style="height: 180px;">
                        <td>
                            <div style="overflow: auto; width: 100%; height: 100%">
                                <asp:GridView ID="GrvDetalle" runat="server" AutoGenerateColumns="False" DataKeyNames="sUsuario"
                                    SkinID="GrillaConsulta" Width="97%"><%--OnRowDataBound="GrvDetalle_RowDataBound"--%>
                                    <Columns>
                                        <asp:BoundField DataField="sUsuario" HeaderText="Usuario">
                                            <ItemStyle Width="30%" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="sTipoProceso" HeaderText="Tipo Proceso">
                                            <ItemStyle Width="40%" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="dtFecha" HeaderText="Fecha">
                                            <ItemStyle Width="25%" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="iCantidadDias" HeaderText="Días">
                                            <ItemStyle Width="5%" />
                                        </asp:BoundField>
                                    </Columns>
                                </asp:GridView>
                            </div>
                        </td>
                    </tr>
                    <tr valign="bottom">
                        <td valign="bottom" colspan="2">
                            <table style="width: 100%;">
                                <tr>
                                    <td style="text-align: right;">
                                        <asp:Button ID="btnCancelarVerDetalle" runat="server" CssClass="botonAceptar"
                                            Style="border-width: 0px;" OnClientClick="javascript: return fc_CerrarVerDetalle();" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </ContentTemplate>
            <Triggers>
                <ajax:AsyncPostBackTrigger ControlID="BtnVerHistorial" EventName="Click" />
            </Triggers>
        </ajax:UpdatePanel>
    </asp:Panel>

    <script language="javascript" type="text/javascript">

        var tiempo = 0;
        var seconds = 0;

        var myVar = setInterval(function () {
            clearInterval(myVar);
        }, 1000);
        
        var myTiempo =  setTimeout(function () {
            clearInterval(myTiempo);
            }, 1);

        function myStopFunction() {
            clearInterval(myVar);
            seconds = '<%=GlobalEntity.Instancia.TiempoDuracionSesion%>' * 60;
            tiempo = '<%=GlobalEntity.Instancia.TiempoDuracionSesion%>' * 1000 * 60;
           
            CuentaTiempo(tiempo);
        }

        function CuentaTiempo(tiempo) {
            clearTimeout(myTiempo);
            myTiempo = setTimeout(function () {
                document.getElementById('<%=btn_cerrar.ClientID%>').click();
                //window.location = "../Seguridad/eFENIX_Login.aspx";
            }, tiempo);
        }



        function SessionExpireAlert(timeout) {
            seconds = timeout / 1000;
            document.getElementsByName("seconds").innerHTML = seconds;

            myvar = setInterval(function () {
                seconds--;
                document.getElementById("seconds").innerHTML = seconds;
            }, 1000);

            CuentaTiempo(timeout);
        };


        //        function ResetSession() {
        //            //Redirect to refresh Session.
        //            window.location = window.location.href;
        //        }




        function OpenPopub() {
            Mostrar('block');
        }

        function Mostrar(estilo) {
            document.getElementById('<%=pnlDetalle.ClientID%>').style.left = (screen.width - 350) / 2 + 'px';
            document.getElementById('<%=pnlDetalle.ClientID%>').style.top = (screen.height - 350) / 2 + 'px';

            document.getElementById('<%=__PopPanel__.ClientID%>').style.width = screen.width + 'px';
            document.getElementById('<%=__PopPanel__.ClientID%>').style.height = screen.height + 'px';

            document.getElementById('<%=__PopPanel__.ClientID%>').style.display = estilo;
            document.getElementById('<%=pnlDetalle.ClientID%>').style.display = estilo;
        }

        function fc_CerrarVerDetalle() {
            Mostrar('none');
            return false;
        }


        var goodexit = false;
        window.onbeforeunload = confirmRegisterExit;

        function confirmRegisterExit(evt) {
            if (!goodexit) {
                //return "Si vas abandonar el sistema haz click en Cerrar Sesion!!";
                document.getElementById('<%=btn_cerrar.ClientID%>').click();
            }
        }


    </script>
</asp:Content>


<asp:Content ID="Content3" runat="server" contentplaceholderid="ContentCab">
    <span class="texto_titulo" >Tu Sesion Expirara en &nbsp; 
        <span class="texto_titulo" id="seconds"> </span>
        <span>&nbsp;seg.</span>
     </span>
     <asp:Button ID="btn_cerrar" runat="server" Text="cerrar" 
        onclick="btn_cerrar_Click"  Style="display: none;" />
    
</asp:Content>



