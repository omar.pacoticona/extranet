﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FENIX.DataAccess;
using FENIX.Common;
using FENIX.BusinessEntity;
using FENIX.BusinessLogic;

public partial class Extranet_Gerencia_eFENIX_AprobarLiquidacion : System.Web.UI.Page
{

    private void SCA_MsgInformacion(string strError)
    {
        MensajeScript(strError);
    }

    private void MensajeScript(string SMS)
    {
        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "", "<script>alert('" + SMS + "');</script>", false);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            ViewState["Usuario"] = Request.QueryString["Usuario"];
            ViewState["IdLiquidacion"] = Request.QueryString["IdLiquidacion"];
            RblAprobacion.SelectedIndex = 1;

            BE_AcuerdoComision oBE_AcuerdoComision = new BE_AcuerdoComision();
            BL_AcuerdoComision oBL_AcuerdoComision = new BL_AcuerdoComision();
            List<BE_AcuerdoComision> LstLiquidacion = new List<BE_AcuerdoComision>();

            LstLiquidacion = oBL_AcuerdoComision.ListarDatosLiquidacion(Convert.ToInt32(ViewState["IdLiquidacion"].ToString()));

            if (LstLiquidacion.Count > 0)
            {
                
                lblNumeroLiquidacion.Text = LstLiquidacion[0].iLiquidacion.ToString().ToUpper();
                lblComisionista.Text = LstLiquidacion[0].sComisionista.ToString().ToUpper();
                lblImporte.Text = LstLiquidacion[0].dImporte.ToString().ToUpper();
                lblEjecutivo.Text = LstLiquidacion[0].sEjecutivo.ToString().ToUpper();
                lblEstado.Text = LstLiquidacion[0].sApto.ToString().ToUpper();
            }
            else
            {
                lblNumeroLiquidacion.Text = "";
                lblComisionista.Text = "";
                lblImporte.Text = "";
                lblEjecutivo.Text = "";
                lblEstado.Text = "";
                btnAceptar.Enabled = false;
                RblAprobacion.Enabled = false;
                lblAprobar.Text = "LIQUIDACION SE ENCUENTRA APROBADA";
            }
        }

    }

    //protected void ImgBtnAceptar_Click(object sender, ImageClickEventArgs e)
    //{

    //    BL_AcuerdoComision oBL_AcComision = new BL_AcuerdoComision();
    //    BE_AcuerdoComision oBE_AcuerdoComision = new BE_AcuerdoComision();


    //    Int32 idLiquidacion = Convert.ToInt32(ViewState["IdLiquidacion"]);
    //    oBE_AcuerdoComision.iLiquidacion = idLiquidacion;
    //    oBE_AcuerdoComision.sUsuario = ViewState["Usuario"].ToString();
    //    // iTipoDoc = tipo Operacion;                       
    //    //ACTUALIZAMOS A ESTADO "APROBADO"
    //    if (RblAprobacion.SelectedValue == "S")
    //    {
    //        String[] oResp = oBL_AcComision.AutorizarLiquidacion(oBE_AcuerdoComision).ToString().Split('|');

    //        if (oResp[0] == "1")
    //        {
    //            SCA_MsgInformacion(oResp[1]);
    //        }
    //        if (oResp[0] == "2")
    //        {
    //            SCA_MsgInformacion(oResp[1]);
    //        }

    //        if (oResp[0] == "-1")
    //        {
    //            SCA_MsgInformacion(oResp[1]);
    //        }

    //    }
    //    else
    //    {
    //        SCA_MsgInformacion("Estramos trabajando en esta opcion");
    //    }




    //}

    protected void btnAceptar_Click(object sender, EventArgs e)
    {
        BL_AcuerdoComision oBL_AcComision = new BL_AcuerdoComision();
        BE_AcuerdoComision oBE_AcuerdoComision = new BE_AcuerdoComision();


        Int32 idLiquidacion = Convert.ToInt32(ViewState["IdLiquidacion"]);
        oBE_AcuerdoComision.iLiquidacion = idLiquidacion;
        oBE_AcuerdoComision.sUsuario = ViewState["Usuario"].ToString();
        // iTipoDoc = tipo Operacion;                       
        //ACTUALIZAMOS A ESTADO "APROBADO"
        if (RblAprobacion.SelectedValue == "S")
        {
            String[] oResp = oBL_AcComision.AutorizarLiquidacion(oBE_AcuerdoComision).ToString().Split('|');

            if (oResp[0] == "1")
            {
                SCA_MsgInformacion(oResp[1]);
                btnAceptar.Enabled = false;
            }
            if (oResp[0] == "2")
            {
                SCA_MsgInformacion(oResp[1]);
            }

            if (oResp[0] == "-1")
            {
                SCA_MsgInformacion(oResp[1]);
            }

        }
        else
        {
            SCA_MsgInformacion("Estramos trabajando en esta opcion");
        }

    }
}