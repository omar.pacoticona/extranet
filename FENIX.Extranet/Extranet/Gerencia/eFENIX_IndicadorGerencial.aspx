﻿<%@ Page Language="C#" MasterPageFile="~/Extranet/Seguridad/eFENIX_Principal.master"
    AutoEventWireup="true" CodeFile="eFENIX_IndicadorGerencial.aspx.cs" Inherits="Extranet_Gerencia_eFENIX_IndicadorGerencial"
    Title="Indicador Gerencial" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="CntMaster" runat="Server">
    <table cellspacing="0" cellpadding="0" style="width: 100%;">
        <tr>
            <td class="form_titulo" style="width: 85%">
                <asp:TextBox ID="LblTitulo" Text="" ReadOnly="true" style="background-color: #0069ae; border: #0069ae; color: #FFFFFF; font-family: Calibri; font-size: 13px; font-weight: bold; text-transform: uppercase;" runat="server"></asp:TextBox>
            </td>
            <td align="right" class="form_titulo_verde" style="width: 15%">
                <asp:TextBox ID="LblTotal" Text="" style="background-color: #0069ae; color: #FFFFFF" ReadOnly="true" BorderColor="#0069ae" runat="server"></asp:TextBox>
            </td>
        </tr>
        
        <tr>
            <td valign="top" colspan="2">
                <table class="form_bandeja" style="margin-top: 5px; margin-bottom: 5px; width: 100%;">
                    <tr>
                        <td colspan="7">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 6%;">
                            Periodo
                        </td>
                        <td style="width: 6%;">
                            <asp:DropDownList ID="DplAnioPeriodo" runat="server" AutoPostBack="True" OnSelectedIndexChanged="DplAnioPeriodo_SelectedIndexChanged">
                            </asp:DropDownList>
                        </td>
                        <td style="width: 16%;">
                            <asp:DropDownList ID="DplMesPeriodo" runat="server" Width="100px" AutoPostBack="True" OnSelectedIndexChanged="DplMesPeriodo_SelectedIndexChanged">
                            </asp:DropDownList>
                        </td>
                        <td style="width: 11%;">
                            Línea de Negocio
                        </td>
                        <td style="width: 35%;">
                            <asp:DropDownList ID="DplNegocio" runat="server" Width="320px" AutoPostBack="True" OnSelectedIndexChanged="DplNegocio_SelectedIndexChanged"></asp:DropDownList>
                        </td>
                        <td style="width: 10%;">
                            <asp:ImageButton ID="ImgBtnBuscar" runat="server" ImageUrl="~/Imagenes/Formulario/btn_consultar_1.JPG"
                                OnClick="ImgBtnBuscar_Click" />
                        </td>
                        <td style="width: 14%;">
                            <asp:ImageButton ID="ImgBtnActualizar" OnClientClick="return Imprimir();" runat="server" ImageUrl="~/Imagenes/Formulario/btn_imprimir.JPG"
                                OnClick="ImgBtnActualizar_Click" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="7">
                            &nbsp;
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        
        <tr valign="top" style="height: 100%">
            <td colspan="5">
                <ajax:UpdatePanel ID="upDocumentoOrigen" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div style="overflow: auto; width: 100%; height: 100%">
                            <asp:GridView ID="GrvListado" runat="server" AutoGenerateColumns="False" DataKeyNames="iIdIndicador,sNombreIndicador,sValor,sPropietario,sVerDetalle,sFormula,sEstado,sCumplimiento,sColor,sTooltip,sComplemento"
                                SkinID="GrillaConsultaUnColor" Width="100%" OnRowDataBound="GrvListado_RowDataBound" OnRowCommand="GrvListado_RowCommand">
                                <Columns>
                                    <asp:TemplateField> 
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkSeleccionar" runat="server"/>
                                        </ItemTemplate>
                                        <HeaderStyle BackColor="#0069ae" Width="3%" />
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="sNombreIndicador" HeaderText="Indicador" ItemStyle-HorizontalAlign="Left">
                                        <ItemStyle Width="44%" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="sValor" HeaderText="Valor" ItemStyle-HorizontalAlign="Right">
                                        <ItemStyle Width="8%" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="sObjetivo" HeaderText="Objetivo" ItemStyle-HorizontalAlign="Right">
                                        <ItemStyle Width="8%" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="sCumplimiento" HeaderText="% Avance" ItemStyle-HorizontalAlign="Center">
                                        <ItemStyle Width="6%" />
                                    </asp:BoundField>
                                    <asp:TemplateField HeaderText="" >
                                        <ItemTemplate>
                                            <asp:Image ID="Image" ImageUrl="" runat="server" />
                                        </ItemTemplate>
                                        <HeaderStyle Width="1%" />
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="sEstado" HeaderText="Estado" ItemStyle-HorizontalAlign="Center">
                                        <ItemStyle Width="10%" />
                                    </asp:BoundField>
                                    <asp:ButtonField ButtonType="Image" CommandName="AprobarDesaprobar" HeaderText="Aprobar" ImageUrl="~/Script/Imagenes/IconButton/Checked_icon.png" Text="Aprobar" ItemStyle-HorizontalAlign="Center">
                                        <ItemStyle Width="6%" />
                                    </asp:ButtonField>
                                    <asp:BoundField DataField="sPropietario" HeaderText="Propietario" ItemStyle-HorizontalAlign="Center">
                                        <ItemStyle Width="9%" />
                                    </asp:BoundField>
                                    <asp:ButtonField ButtonType="Image" CommandName="OpenVerDetalle" HeaderText="Detalle" ItemStyle-HorizontalAlign="Center" ImageUrl="~/Script/Imagenes/IconButton/Calendar_Upd.png" Text="Ver Detalle">
                                        <ItemStyle Width="6%" />
                                    </asp:ButtonField>
                                </Columns>
                            </asp:GridView>
                        </div>
                    </ContentTemplate>
                    <Triggers>
                        <ajax:AsyncPostBackTrigger ControlID="GrvListado" EventName="SelectedIndexChanged" />
                        <ajax:AsyncPostBackTrigger ControlID="DplNegocio" EventName="SelectedIndexChanged" />
                        <ajax:AsyncPostBackTrigger ControlID="DplAnioPeriodo" EventName="SelectedIndexChanged" />
                        <ajax:AsyncPostBackTrigger ControlID="DplMesPeriodo" EventName="SelectedIndexChanged" />
                        <ajax:AsyncPostBackTrigger ControlID="ImgBtnBuscar" EventName="Click" />
                        <ajax:AsyncPostBackTrigger ControlID="dnvListado" EventName="PageChanged" />
                        <ajax:AsyncPostBackTrigger ControlID="dnvListado" EventName="NextPage" />
                        <ajax:AsyncPostBackTrigger ControlID="dnvListado" EventName="PreviousPage" />
                        <ajax:AsyncPostBackTrigger ControlID="dnvListado" EventName="FirstPage" />  
                        <ajax:AsyncPostBackTrigger ControlID="dnvListado" EventName="LastPage" />     
                    </Triggers>
                </ajax:UpdatePanel>
            </td>
        </tr>
        
        <tr valign="top" style="height: 2%;">
            <td colspan="5">
               <ajax:UpdatePanel ID="upDnvListado" runat="server">
                    <ContentTemplate>
                        <FENIX:DataNavigator ID="dnvListado" runat="server" AllowCustomPaging="True" ButtonText="Ir" 
                            ForeColor="White" Font-Size="11px" BackColor="#0069ae" 
                            CurrentPage="1" CustomClientFunction="" EnableAskingAfterPage="False" EnableCustomScript="False"
                            GridViewId="GrvListado" ImagesPath="~/Imagenes/DN/" IsParentShowWindow="False"
                            MessageAskingAfterPage="" PageIndicatorFormat="Página {0} / {1}" Visible="False"
                            Width="100%" WidthButtonIr="" WidthTextBoxNumPagina="25px" />
                    </ContentTemplate>
                    <Triggers>
                        <ajax:AsyncPostBackTrigger ControlID="dnvListado" EventName="PageChanged" />
                        <ajax:AsyncPostBackTrigger ControlID="ImgBtnBuscar" EventName="Click" />
                    </Triggers>
                </ajax:UpdatePanel>
            </td>
        </tr> 
    </table>


    <%-- Popup - Cambio de Contraseña--%>
    <asp:Panel ID="__PopPanel__" runat="server" CssClass="modalBackground" Style="left: 0px;
        top: 0px; display: none; position: fixed; z-index: 10000; display: none;" />

    <asp:Panel ID="pnlDetalle" runat="server" Style="display: none; width: 840px; height: 390px;
        position: fixed; z-index: 100002; border: dimgray 1px solid; background-color: White;" DefaultButton="btnCancelarVerDetalle">
        <ajax:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <table border="0" cellpadding="0" cellspacing="0" width="100%" style="padding: 5px;">
                    <tr valign="top" class="form_titulo" style="background-color: #0069ae; height: 15px; text-transform: none;">
                        <td style="padding: 5px;">
                            Detalle del Indicador
                        </td>
                    </tr>
                    <tr>
                        <td valign="top" colspan="2">
                            <table class="form_bandeja" style="width: 100%;">
                                <tr>
                                    <td style="width: 10%;">
                                        Indicador:
                                    </td>
                                    <td style="width: 90%;" colspan="3">
                                        &nbsp;&nbsp;&nbsp;<asp:Label runat="server" ID="LblIndicador" Text=""></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Periodo:
                                    </td>
                                    <td style="width: 40%;">
                                        &nbsp;&nbsp;&nbsp;<asp:Label runat="server" ID="LblPeriodo" Text=""></asp:Label>
                                    </td>
                                    <td style="width: 10%; text-align: right;">
                                        Total:
                                    </td>
                                    <td style="width: 40%;">
                                        &nbsp;&nbsp;&nbsp;<asp:Label runat="server" ID="LblTotalGerenal" Text=""></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr style="height: 255px;">
                        <td>
                            <div style="overflow: auto; width: 100%; height: 100%">
                                <asp:GridView ID="GrvDetalle" runat="server" AutoGenerateColumns="False" DataKeyNames="sNombreIndicador"
                                    SkinID="GrillaConsultaUnColor" Width="98%" OnRowDataBound="GrvDetalle_RowDataBound" >
                                    <Columns>
                                        <asp:TemplateField> 
                                            <ItemTemplate>
                                                <asp:CheckBox ID="chkSeleccionarDet" runat="server"/>
                                            </ItemTemplate>
                                            <HeaderStyle BackColor="#0069ae" Width="2%" />
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="sNombreIndicador" HeaderText="Descripcion" HeaderStyle-HorizontalAlign="Left" >
                                            <ItemStyle Width="34%" HorizontalAlign="Left" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="sSemana1" HeaderText="Sem 1" ItemStyle-HorizontalAlign="Right">
                                            <ItemStyle Width="7%" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="sSemana2" HeaderText="Sem 2" ItemStyle-HorizontalAlign="Right">
                                            <ItemStyle Width="7%" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="sSemana3" HeaderText="Sem 3" ItemStyle-HorizontalAlign="Right">
                                            <ItemStyle Width="7%" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="sSemana4" HeaderText="Sem 4" ItemStyle-HorizontalAlign="Right">
                                            <ItemStyle Width="7%" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="sSemana5" HeaderText="Sem 5" ItemStyle-HorizontalAlign="Right">
                                            <ItemStyle Width="7%" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="sSemana6" HeaderText="Sem 6" ItemStyle-HorizontalAlign="Right">
                                            <ItemStyle Width="7%" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="sTotalItem" HeaderText="Sub Total" ItemStyle-HorizontalAlign="Right">
                                            <ItemStyle Width="11%" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="sPorcentaje" HeaderText="%" ItemStyle-HorizontalAlign="Right">
                                            <ItemStyle Width="10%" />
                                        </asp:BoundField>
                                    </Columns>
                                </asp:GridView>
                            </div>
                        </td>
                    </tr>
                    <tr style="height: 15px;">
                        <td valign="bottom" colspan="2">
                            <table style="width: 100%;">
                                <tr>
                                    <td style="text-align: right;">
                                        <asp:Button ID="btnCancelarVerDetalle" runat="server" CssClass="botonAceptar"
                                            Style="border-width: 0px;" OnClientClick="javascript: return fc_CerrarVerDetalle();" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </ContentTemplate>
            <Triggers>
                <ajax:AsyncPostBackTrigger ControlID="GrvListado" EventName="RowCommand" />
            </Triggers>
        </ajax:UpdatePanel>
    </asp:Panel>
    

    <script language="javascript" type="text/javascript">
        var tiempo = 0;
        var seconds = 0;

        var myVar = setInterval(function () {
            clearInterval(myVar);
        }, 1000);

        var myTiempo = setTimeout(function () {
            clearInterval(myTiempo);
        }, 1);

        function myStopFunction() {
            clearInterval(myVar);
            seconds = '<%=GlobalEntity.Instancia.TiempoDuracionSesion%>' * 60;
            tiempo = '<%=GlobalEntity.Instancia.TiempoDuracionSesion%>' * 1000 * 60;

            CuentaTiempo(tiempo);
        }

        function CuentaTiempo(tiempo) {
            clearTimeout(myTiempo);
            myTiempo = setTimeout(function () {
                document.getElementById('<%=btn_cerrar.ClientID%>').click();
                //window.location = "../Seguridad/eFENIX_Login.aspx";
            }, tiempo);
        }



        function SessionExpireAlert(timeout) {
            seconds = timeout / 1000;
            document.getElementsByName("seconds").innerHTML = seconds;

            myvar = setInterval(function () {
                seconds--;
                document.getElementById("seconds").innerHTML = seconds;
            }, 1000);

            CuentaTiempo(timeout);
        };


        var goodexit = false;
        window.onbeforeunload = confirmRegisterExit;
        function confirmRegisterExit(evt) {
            if (!goodexit) {
                //return "Si vas abandonar el sistema haz click en Cerrar Sesion!!";
                document.getElementById('<%=btn_cerrar.ClientID%>').click();
            }
        }



        var objFilaAnt = null;
        var backgroundColorFilaAnt = "";

        function fc_SeleccionaFilaSimple(objFila, objrowIndex, chkID) {
            try {

                if (objFilaAnt != null) {
                    objFilaAnt.style.backgroundColor = backgroundColorFilaAnt;
                }
                objFilaAnt = objFila;
                backgroundColorFilaAnt = objFila.style.backgroundColor;
                objFila.style.backgroundColor = "#c4e4ff";
            }

            catch (e) {
                error = e.message;
            }
        }

        function HabilitarUno(chk) {
            f = document.forms[0].elements;
            for (x = 0; x < f.length; x++) {
                obj = f[x];
                if (obj.type == 'checkbox') {
                    obj.checked = false;
                }
            }
            chk.enabled;
            chk.checked = true;
        }

        
        function OpenPopub() {
            Mostrar('block');
        }
        
        function Mostrar(estilo) {
            document.getElementById('<%=pnlDetalle.ClientID%>').style.left = (screen.width - 550) / 2 + 'px';
            document.getElementById('<%=pnlDetalle.ClientID%>').style.top = (screen.height - 500) / 2 + 'px';

            document.getElementById('<%=__PopPanel__.ClientID%>').style.width = screen.width + '0px';
            document.getElementById('<%=__PopPanel__.ClientID%>').style.height = screen.height + '0px';

            document.getElementById('<%=__PopPanel__.ClientID%>').style.display = estilo;
            document.getElementById('<%=pnlDetalle.ClientID%>').style.display = estilo;
        }
        
        function fc_CerrarVerDetalle() {
            Mostrar('none');
            return false;
        }

        function Imprimir() {
            sAnioPeriodo = document.getElementById('<%=DplAnioPeriodo.ClientID%>').value;
            sMesPeriodo = document.getElementById('<%=DplMesPeriodo.ClientID%>').value;
            sNegocio = document.getElementById('<%=DplNegocio.ClientID%>').value;
            sUsuario = '<%=GlobalEntity.Instancia.Usuario%>';

            strUrl = "&vi_AnioPeriodo=" + sAnioPeriodo + "&vi_MesPeriodo=" + sMesPeriodo + "&vi_Agrupacion=" + sNegocio + "&vi_Usuario=" + sUsuario;
            url = '<%=Convert.ToString(ConfigurationManager.AppSettings["ServerReporting"])%>/pages/ReportViewer.aspx?%2fFENIX.Reporte%2fFENIX_Extranet_Indicadores&rs:Command=Render&rc:Parameters=False' + strUrl;
            window.open(url, 'Tarifa_Imp', 'toolbar=no,left=0,top=0,width=' + screen.width + ',height=' + screen.height + ', directories=no, status=no, scrollbars=yes, resizable=yes, menubar=no');
            window.blur();
            return false;
        }
    </script>
</asp:Content>
<asp:Content ID="Content3" runat="server" contentplaceholderid="ContentCab">

    <span class="texto_titulo" >Tu Sesion Expirara en &nbsp; 
        <span class="texto_titulo" id="seconds"> </span>
        <span>&nbsp;seg.</span>
     </span>
     <asp:Button ID="btn_cerrar" runat="server" Text="cerrar" 
        onclick="btn_cerrar_Click"  Style="display: none;" />
</asp:Content>

