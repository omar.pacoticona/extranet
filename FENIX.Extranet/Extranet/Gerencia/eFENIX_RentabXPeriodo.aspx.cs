﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using FENIX.BusinessEntity;
using FENIX.BusinessLogic;
using System.Windows.Forms;
using System.Drawing;

public partial class Extranet_Gerencia_eFENIX_RentabXPeriodo : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
         String tiempo = (Convert.ToInt32(GlobalEntity.Instancia.TiempoDuracionSesion.ToString()) * 1000 * 60).ToString();
         Session["Reset"] = true;

         if (!Page.IsPostBack)
         {
             ListarDropDowList();
             
                         
             if (Request.QueryString["Periodo"] != null)
                 DplPeriodo.SelectedValue = Request.QueryString["Periodo"];
             else
                 DplPeriodo.SelectedValue = DateTime.Now.Year.ToString();


             if (Request.QueryString["rIni"] != null)
                 DplMesIni.SelectedValue = Request.QueryString["rIni"];

             if (Request.QueryString["rFin"] != null)
             {
                 DplMesFin.SelectedValue = Request.QueryString["rFin"];
                 pMuestraDetalle();
             }
             else
             {
                 DplMesFin.SelectedValue = DateTime.Now.Month.ToString();

                 List<BE_Costos> lsBE_Costos = new List<BE_Costos>();
                 BE_Costos oBE_Costos = new BE_Costos();
                 lsBE_Costos.Add(oBE_Costos);
                 GrvListado.DataSource = lsBE_Costos;
                 GrvListado.DataBind();
             }

             ScriptManager.RegisterStartupScript(this, GetType(), "mensaje", "SessionExpireAlert(" + tiempo + ");", true);
         }
         else
             ScriptManager.RegisterStartupScript(this, GetType(), "mensaje", "myStopFunction();", true);
    }



    #region "Método Controles"
    protected void ListarDropDowList()
    {
        try
        {
            DplPeriodo.Items.Clear();
            for (int i = 2013; i <= DateTime.Now.Year; i++)
            {
                DplPeriodo.Items.Add(new ListItem(i.ToString(), i.ToString()));
            }
            DplPeriodo.SelectedIndex = 0;


            BL_Presupuesto oBL_Presupuesto = new BL_Presupuesto();

            DplMesIni.DataSource = oBL_Presupuesto.ListarDatosTabla(124);
            DplMesIni.DataValueField = "iIdValor";
            DplMesIni.DataTextField = "sDescripcion";
            DplMesIni.DataBind();
            DplMesIni.SelectedIndex = 0;


            DplMesFin.DataSource = oBL_Presupuesto.ListarDatosTabla(124);
            DplMesFin.DataValueField = "iIdValor";
            DplMesFin.DataTextField = "sDescripcion";
            DplMesFin.DataBind();
            DplMesFin.SelectedIndex = 0;


        }
        catch (Exception e)
        {

        }
    }
    #endregion


    private void SCA_MsgInformacion(string strError)
    {
        MensajeScript(strError);
    }

    private void MensajeScript(string SMS)
    {
        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "", "<script>alert('" + SMS + "');</script>", false);
    }

    protected void btn_cerrar_Click(object sender, EventArgs e)
    {
        if (GlobalEntity.Instancia.CerrarExtranet == "S")
        {
            BL_Usuario oBL_Usuario = new BL_Usuario();
            oBL_Usuario.GrabaInicioSesion(GlobalEntity.Instancia.Usuario, "0");
            Session.Abandon();
            FormsAuthentication.SignOut();
            Response.Redirect("../../eFENIX_Login.aspx");
            Response.End();
        }
    }


    protected void pMuestraDetalle()
    {
        BE_Costos oBE_Costos = new BE_Costos();
        BL_Costos oBL_Costos = new BL_Costos();

        oBE_Costos.sPeriodo = DplPeriodo.SelectedValue;
        oBE_Costos.iMesIni = Convert.ToInt32(DplMesIni.SelectedValue);
        oBE_Costos.iMesFin = Convert.ToInt32(DplMesFin.SelectedValue);

        List<BE_Costos> lista = oBL_Costos.ListarVtasXPeriodo(oBE_Costos);

        Decimal dVentas = 0;
        Int32 iCnt = 0;
        Int32 iTn = 0;
        Decimal dCostoOpe = 0;
        Decimal dCostoAdm = 0;
        Decimal dUtilidad = 0;
        Decimal dVtaEsperada = 0;

        for (int i = 0; i <= lista.Count-1; i++)
        {
            iCnt += Convert.ToInt32(lista[i].iTotalCnt.ToString());
            iTn += Convert.ToInt32(lista[i].iTotalTn.ToString());
            dVentas += Convert.ToDecimal(lista[i].dVentas.ToString());
            
            dCostoOpe += Convert.ToDecimal(lista[i].dCostoOpe.ToString());
            dCostoAdm += Convert.ToDecimal(lista[i].dCostoAdm.ToString());
            dUtilidad += Convert.ToDecimal(lista[i].dUtilidad.ToString());
            dVtaEsperada += Convert.ToDecimal(lista[i].dVtaEsperada.ToString());
        }

        oBE_Costos.iMes = -1;
        oBE_Costos.sMes = "TOTALES (MILES US$)";
        oBE_Costos.iTotalCnt = iCnt;
        oBE_Costos.iTotalTn = iTn;
        oBE_Costos.dVentas = dVentas;
        oBE_Costos.dCostoOpe = dCostoOpe;
        oBE_Costos.dCostoAdm = dCostoAdm;
        oBE_Costos.dUtilidad = dUtilidad;
        oBE_Costos.dVtaEsperada = dVtaEsperada;

    /*    if (iPresupuesto > 0)        
            oBE_Presupuesto.dPorc = Decimal.Round(((iVentas / iPresupuesto) - 1) * 100, 1);

        if (iVentasAnt > 0)
            oBE_Presupuesto.dPorcAnt = Decimal.Round(((iVentas / iVentasAnt) - 1) * 100, 1);
        else
            oBE_Presupuesto.dPorcAnt = 100;*/


        lista.Add(oBE_Costos);

        GrvListado.DataSource = lista;
        GrvListado.DataBind();
    }

    protected void GrvListado_RowCreated(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Header)
        {
            GridView HeaderGrid = (GridView)sender;
            GridViewRow HeaderGridRow = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Insert);

            TableCell HeaderCell = new TableCell();

            HeaderCell.Text = "Mes";
            HeaderGridRow.Cells.Add(HeaderCell);


            HeaderCell = new TableCell();
            HeaderCell.Text = "Movimiento Carga";
            HeaderCell.ColumnSpan = 2;
            HeaderGridRow.Cells.Add(HeaderCell);


            HeaderCell = new TableCell();
            HeaderCell.Text = "Costo US$";
            HeaderCell.ColumnSpan = 2;
            HeaderGridRow.Cells.Add(HeaderCell);


            HeaderCell = new TableCell();
            HeaderCell.Text = "Ventas ";
            HeaderGridRow.Cells.Add(HeaderCell);


            HeaderCell = new TableCell();
            HeaderCell.Text = "Utilidad";
            HeaderGridRow.Cells.Add(HeaderCell);


            HeaderCell = new TableCell();
            HeaderCell.Text = "Vta.Esperada";
            HeaderGridRow.Cells.Add(HeaderCell);


            HeaderCell = new TableCell();
            HeaderCell.Text = "Detalle";
            HeaderCell.ColumnSpan = 2;
            HeaderGridRow.Cells.Add(HeaderCell);

            GrvListado.Controls[0].Controls.AddAt(0, HeaderGridRow);
            
        }
    }



    protected void ImgBtnBuscar_Click(object sender, ImageClickEventArgs e)
    {
        pMuestraDetalle();
    }

    protected void GrvListado_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        List<BE_Tabla> olst_TablaTipo = new List<BE_Tabla>();
       /* BL_Costos oBL_Costos = new BL_Costos();*/
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            DataKey dataKey = this.GrvListado.DataKeys[e.Row.RowIndex];
            BE_Costos oitem = (e.Row.DataItem as BE_Costos);

            if (oitem.iMes == 0)
                e.Row.Visible = false;

            e.Row.Cells[5].BackColor = ColorTranslator.FromHtml("#A8E7B6");
            e.Row.Cells[6].BackColor = ColorTranslator.FromHtml("#A8E7B6");

           /* if (oitem.dPorc < 0)
                e.Row.Cells[3].ForeColor = System.Drawing.Color.Red;

            if (oitem.dPorcAnt < 0)
                e.Row.Cells[5].ForeColor = System.Drawing.Color.Red;*/


            if (oitem.iMes == -1)  //Total
            {
                e.Row.Cells[8].Visible = false;
                e.Row.Cells[9].Visible = false;

                for (int c = 0; c < e.Row.Cells.Count; c++)
                {
                    e.Row.Cells[c].Font.Size = 10;
                    e.Row.Cells[c].Font.Bold = true;

                    e.Row.Cells[c].BackColor = System.Drawing.Color.LightGray;
                }
            }
            else
            {
                ImageButton ibtn = (ImageButton)e.Row.Cells[8].Controls[0];
                ibtn.OnClientClick = String.Format("AbrirPagina('{0}');", dataKey.Values["iMes"].ToString());

                ImageButton ibtnServicio = (ImageButton)e.Row.Cells[9].Controls[0];
                ibtnServicio.OnClientClick = String.Format("AbrirPagServicio('{0}');", dataKey.Values["iMes"].ToString());
            }
        }
        else if (e.Row.RowType == DataControlRowType.Header)
        {
            e.Row.Cells[5].BackColor = ColorTranslator.FromHtml("#0069ae");
            e.Row.Cells[6].BackColor = ColorTranslator.FromHtml("#0069ae");
        }
    }

    //protected void GrvListado_RowCommand(object sender, GridViewCommandEventArgs e)
    //{
    //    if (e.CommandName == "LNegocio")
    //    {
    //        Int32 iMes = Convert.ToInt32( GrvListado.DataKeys[Convert.ToInt32(e.CommandArgument)].Values["iMes"].ToString() );
    //      //  Response.Redirect("../Gerencia/eFENIX_VtasXLinea.aspx?Periodo="+ DplPeriodo.SelectedValue+"&mRangoIni="+ DplMesIni.SelectedValue +"&mRangoFin="+ DplMesFin.SelectedValue +"&mIni="+ iMes+"&mFin="+iMes);
    //        Response.Redirect("../Gerencia/eFENIX_VtasXLinea.aspx?Periodo=" + DplPeriodo.SelectedValue + "&mIni=" + iMes + "&mFin=" + iMes);
    //    }
    //}
}