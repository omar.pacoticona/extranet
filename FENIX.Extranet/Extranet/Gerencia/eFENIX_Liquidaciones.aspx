﻿<%@ Page Language="C#"  MasterPageFile="~/Extranet/Seguridad/eFENIX_Principal.master"
    AutoEventWireup="true" CodeFile="eFENIX_Liquidaciones.aspx.cs" Inherits="Extranet_Gerencia_eFENIX_Liquidaciones" %>


    <asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
         <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js" type="text/javascript"></script>
          <script src="../../Script/Java_Script/jsAcuerdoComision.js"></script>
        <link href="../../Script/Hojas_Estilo/Preload.css" rel="stylesheet" />
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
      
    </asp:Content>
    <asp:Content ID="Content3" ContentPlaceHolderID="ContentCab" Runat="Server">
        
        <span class="texto_titulo" >Tu Sesion Expirara en &nbsp; 
            <span class="texto_titulo" id="seconds"> </span>
            <span>&nbsp;seg.</span>
         </span>
         <asp:Button ID="btn_cerrar" runat="server" Text="cerrar" 
            onclick="btn_cerrar_Click"  Style="display: none;" />
    </asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="CntMaster" Runat="Server">

     <table cellspacing="0" cellpadding="0" style="width: 100%;">
        <tr>
            <td class="form_titulo" style="width: 85%">
                <asp:TextBox ID="LblTitulo" Text="Registro de Liquidaciones" ReadOnly="true" 
                    style="background-color: #0069ae; border: #0069ae; color: #FFFFFF; font-family: Calibri; font-size: 13px; font-weight: bold; text-transform: uppercase;" 
                    runat="server" Width="231px" ></asp:TextBox>
            </td>           
        </tr>
        
        <tr>
            <td valign="top" colspan="2">
                <table class="form_bandeja" style="margin-top:5px; margin-bottom: 5px; width: 100%; height:50px;">
                    <tr>
                        <%--<td colspan="8">
                            &nbsp;
                        </td> --%>  
                        <td style="width: 9%;">
                           &nbsp; &nbsp;  Proveedor  &nbsp;:  
                        </td>
                        <td colspan="1" style="width: 51%;">
                           <asp:Label ID="idLblComisionista" runat="server" Text="Proveedor" maxlength="10"
                                 Visible="true"></asp:Label>
                             <%--<input type="text" id="idLblComisionista" runat="server" name="NameidLblComisionista" maxlength="10" value="Hola" />--%>
                        </td>
                         <%--<td colspan="1" style="width: 5%;">
                            &nbsp;
                             &nbsp;
                             &nbsp;
                             &nbsp;
                             </td>
                                    --%>           
                        <%--<td style="width: 20%; height:50%;">
                            <div style="margin-left:10%">--%>
                                 <%--<asp:ImageButton ID="ImgBtnBuscar" runat="server" ImageUrl="~/Imagenes/Formulario/GenerarComision.png" OnClick="ImgBtnBuscar_Click" Height="28px" Width="77px" />--%>
                                <%--<asp:ImageButton ID="ImgBtnGrabar" runat="server" ImageUrl="~/Imagenes/Formulario/btn_grabar.JPG" OnClick="ImgBtnGrabar_Click" OnClientClick="javascript: return  ValidarCheck();"/> --%>
                                <%-- <asp:Button ID="ImgBtnBuscarr"  runat="server" Text="Generar" OnClick="ImgBtnBuscarr_Click" class="btn btn-primary" BackColor="#0069ae"/> &nbsp;
                                <asp:Button ID="ImgBtnGrabarr"  runat="server" Text="Grabar" OnClick="ImgBtnGrabar_Click" class="btn btn-primary" BackColor="#0069ae" OnClientClick="javascript: return  ValidarCheck();"/> 
                                  
                            </div>   
                         </td>--%>
                        <td style="width:100px;">
                            <%--<div style="margin-left:63%;">--%>
                              <asp:Button ID="ImgBtnBuscarr" style="margin-left:0px; width:94px;" runat="server" Text=" Generar " OnClick="ImgBtnBuscarr_Click" class="btn btn-primary 2" BackColor="#0069ae"/> &nbsp;
                               <%-- </div>--%>
                         </td>
                        <td style="width:30px;">
                           
                               <asp:Button ID="ImgBtnGrabarr" style="width:100px;" runat="server" Text="Grabar" OnClick="ImgBtnGrabar_Click" class="btn btn-primary" BackColor="#0069ae" OnClientClick="javascript: return  ValidarCheck();"/> 
                                 
                        </td>
                        <td style="width:20px;">
                               <asp:Button ID="ImgBtnRecalcular"  runat="server" Text="Recalcular" class="btn btn-primary" BackColor="#0069ae" OnClick="ImgBtnRecalcular_Click" /> 
                                
                        </td>
                        <td style="width:20px;">
                               <asp:Button ID="ImgBtnExportar"  runat="server" Text="Exportar" class="btn btn-primary" BackColor="#0069ae" OnClick="ImgBtnExportar_Click" />                                 
                        </td>
                     
                    </tr>
                   
                    </table>
            </td>
        </tr>
      
        <tr valign="top" style="height: 70%">
            <td colspan="5">
                <ajax:updatepanel ID="upDocumentoOrigen" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>                         
                        <div style="overflow: auto; width: 100%; height: 350px;">
                            <asp:GridView ID="GrvListado" runat="server" AutoGenerateColumns="False" 
                                DataKeyNames="iIdDocOriDet,iIdDocPagoDet,IidAcuerdoComision,sRetirado,sFacturado,IidTarifa,dImporte,sSituacion,sPqtSLI,iIdDetpaqSLI,sTiempoSituacion,dImporteVisual"
                                SkinID="GrillaConsulta" Width="100%" OnRowDataBound="GrvListado_RowDataBound">
                                <Columns>
                                    <%--El Width era 100 con todos los registro de la grilla--%>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            <asp:CheckBox runat="server" ID="chkAllQuitarGrp" onclick="SelectGroupChecks(this.id)" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkSeleccionar" runat="server" AutoPostBack="false" />
                                        </ItemTemplate>
                                        <ItemStyle Width="1%" />
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="iIdDocPagoDet" HeaderText="Item" ItemStyle-HorizontalAlign="Center" Visible="false">
                                        <ItemStyle Width="5px" />
                                    </asp:BoundField>
                                     <asp:BoundField DataField="iIdDetpaqSLI" HeaderText="Item" ItemStyle-HorizontalAlign="Center" Visible="false">
                                        
                                    </asp:BoundField>
                                   <asp:BoundField DataField="iIdDocOriDet" HeaderText="Item" ItemStyle-HorizontalAlign="Center" Visible="false">
                                       
                                    </asp:BoundField>
                                    <asp:BoundField DataField="sDocMaster" HeaderText="MBL" ItemStyle-HorizontalAlign="Center">
                                        
                                    </asp:BoundField>
                                    <asp:BoundField DataField="sDocHijo" HeaderText="BL/BK" ItemStyle-HorizontalAlign="Center">
                                      <%--  <ItemStyle Width="10px" />--%>
                                    </asp:BoundField>
                                    <asp:BoundField DataField="sVolante" HeaderText="Volante" ItemStyle-HorizontalAlign="Center">
                                        
                                    </asp:BoundField>
                                    <asp:BoundField DataField="sConsignatario" HeaderText="Consignatario/Embarcador" ItemStyle-HorizontalAlign="center">
                                        
                                    </asp:BoundField>
                                    <asp:BoundField DataField="sCondicion" HeaderText="Condicion" ItemStyle-HorizontalAlign="Center">
                                        
                                    </asp:BoundField>
                                    <asp:BoundField DataField="sContenedor" HeaderText="  Contenedor/Chasis  " ItemStyle-HorizontalAlign="Center">
                                        
                                    </asp:BoundField>
                                    <asp:BoundField DataField="sFechaIngreso" HeaderText="Ingreso" ItemStyle-HorizontalAlign="Center">
                                        
                                    </asp:BoundField>
                                     <asp:BoundField DataField="sRetirado" HeaderText="Retiro" ItemStyle-HorizontalAlign="Center">
                                       
                                    </asp:BoundField>
                                    <asp:BoundField DataField="sFacturado" HeaderText="Facturado" ItemStyle-HorizontalAlign="Center" Visible="false">
                                        
                                    </asp:BoundField>                                    
                                    <asp:BoundField DataField="sMoneda" HeaderText="Moneda" ItemStyle-HorizontalAlign="Center" Visible="false">

                                    </asp:BoundField>
                                    <asp:BoundField DataField="sEstado" HeaderText="Estado" ItemStyle-HorizontalAlign="Center">

                                    </asp:BoundField> 
                                    <asp:BoundField DataField="dImporteVisual" HeaderText="Importe S/." ItemStyle-HorizontalAlign="Center">
                                    </asp:BoundField> 

                                    <asp:BoundField DataField="dImporte" HeaderText="Importe S/." ItemStyle-HorizontalAlign="Center" Visible="false">                                        
                                    </asp:BoundField>
                                    <asp:BoundField DataField="IidAcuerdoComision" HeaderText="AcuerdoComision" ItemStyle-HorizontalAlign="Center" Visible="false">
                                        
                                    </asp:BoundField>
                                    <asp:BoundField DataField="sNroFactura" HeaderText="Factura" ItemStyle-HorizontalAlign="Center" Visible="false">
                                        
                                    </asp:BoundField>
                                    <asp:BoundField DataField="sNroContrato" HeaderText="Contrato" ItemStyle-HorizontalAlign="Center" Visible="false">
                                       
                                    </asp:BoundField>
                                    <asp:BoundField DataField="IidTarifa" HeaderText="Tarifa" ItemStyle-HorizontalAlign="Center" Visible="false">
                                       
                                    </asp:BoundField>
                                    <asp:BoundField DataField="sServicio" HeaderText="Servicio" ItemStyle-HorizontalAlign="Center" Visible="false">
                                       
                                    </asp:BoundField>
                                     <asp:BoundField DataField="sPqtSLI" HeaderText="PqteSLI" ItemStyle-HorizontalAlign="Center" Visible="false">
                                           </asp:BoundField>
                                       <asp:BoundField DataField="sSituacion" HeaderText="Situacion" ItemStyle-HorizontalAlign="Center" Visible="false">
                                        
                                    </asp:BoundField>
                                </Columns>
                            </asp:GridView>
                        </div>
                    </ContentTemplate>
                    <Triggers>       
                         <ajax:AsyncPostBackTrigger ControlID="GrvListado" EventName="SelectedIndexChanged" />  
                        <asp:AsyncPostBackTrigger ControlID="GrvListado" EventName="SelectedIndexChanged"></asp:AsyncPostBackTrigger>
                        <asp:AsyncPostBackTrigger ControlID="GrvListado" EventName="SelectedIndexChanged"></asp:AsyncPostBackTrigger>
                       
                          <asp:AsyncPostBackTrigger ControlID="ImgBtnGrabarr" EventName="Click"></asp:AsyncPostBackTrigger>
                             <asp:AsyncPostBackTrigger ControlID="ImgBtnBuscarr" EventName="Click"></asp:AsyncPostBackTrigger>
                        <asp:AsyncPostBackTrigger ControlID="ImgBtnRecalcular" EventName="Click"></asp:AsyncPostBackTrigger>
                       <%--<asp:AsyncPostBackTrigger ControlID="ImgBtnExportar" EventName="Click"></asp:AsyncPostBackTrigger>--%>
                        
                    </Triggers>
                </ajax:updatepanel>
            </td>
         </tr>
        
        <tr>
            <td style="width:100%;">
                 <ajax:updatepanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional" >
                    <ContentTemplate> 
                 <table class="form_bandeja" style="margin-top:5px; margin-bottom: 5px; width: 100%; height:20px;">
                     <tr>
                         <td style="width:400px;">
                              Total de registros : <asp:Label ID="lblTotalRegistros" runat="server" Text="0"  Visible="true"></asp:Label>
                         </td>
                         <td style="width:400px;">
                         &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Registros seleccionados : <asp:Label ID="lblRegistrosSelecc" runat="server" Text="0"  Visible="true"></asp:Label>
                        </td>
            <td style="width:400px;">
                      Importe seleccionado : <asp:Label ID="lblImportSelec" runat="server" Text="0"  Visible="true"></asp:Label>
            </td>
                     </tr>

                 </table>
                        </ContentTemplate>
                     </ajax:updatepanel>

                
            </td>
            <%--<td style="width:400px;">
                 Registros seleccionados : <asp:Label ID="lblRegistrosSelecc" runat="server" Text="0"  Visible="true"></asp:Label>
            </td>
            <td style="width:400px;">
                 Importe seleccionado : <asp:Label ID="lblImportSelec" runat="server" Text="0"  Visible="true"></asp:Label>
            </td>--%>
        </tr>
    </table>



<%--         <asp:Panel ID="pnlCondiciones" runat="server" CssClass="PanelPopup" Width="700px" Height="398px" BorderStyle="Groove">
              <div  class="panel-heading" runat="server">      
            <ajax:UpdatePanel ID="udpCondiciones" UpdateMode="Conditional" runat="server"  AutoPostBack="true">               
                <ContentTemplate>    
                    <table cellpadding="0" cellspacing="0" class="table" style="width: 100%; margin-bottom:10px; height: 29px;" >

                       <tr>
                                    <td class="form_titulo" style="width: 85%; height:10px;">                                        
                                        <label  runat="server" style="background-color: #0069ae; color: #FFFFFF; font-family: Calibri; 
                                            font-size: 20px; font-weight: bold; text-transform: uppercase; align-content:center; margin-left:250px;" >CONDICIONES DE USO </label>
                                      
                                    </td>

                       </tr> 
                       </table>
                    <table cellpadding="0" cellspacing="0" style="width: 100%; margin-bottom:1px;" class="table">
                        <tr class="info">                          
                            <td  style="width: 85%;">                               
                                <p style="font-size:12px;">asaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa 
                                    aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa
                                    aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa
                                    aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa
                                    aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa
                                    aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa
                                    aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa
                                    aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa
                                    aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa
                                    aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa
                                    aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa
                                </p>
                                
                            </td>
                         </tr>
                    </table>
                      <table cellpadding="0" cellspacing="0" class="TabMantenimiento" style="width: 100%; height: 40px">
                                    <tr>
                                        <td style="height: 38px">
                                                        <asp:CheckBox ID="ChekTipoFactura" runat="server" Text="" OnCheckedChanged="ChekTipoFactura_CheckedChanged" AutoPostBack="true" />&nbsp;&nbsp;
                                                         <asp:Label ID="lblCondiciones" runat="server" Text="He leído y acepto los términos y condiciones de uso." ForeColor="Black"></asp:Label>
                                          </td>
                                    </tr>
                                     <tr>
                                                  <td style="width:15%; text-align:center; height: 33px;">                                                      
                                                   <asp:Button ID="btnAceptar"  runat="server" Text="Aceptar" OnClick="btnCerrar_Click" class="btn btn-primary active"/>   &nbsp;&nbsp;                                             
                                                     <asp:Button ID="btnCancelar" runat="server" Text="Cancelar" OnClick="btnCancelar_Click"  class="btn btn-primary active"/>
                                                </td>
                                            
                                     </tr>
                                           
                        </table>

                </ContentTemplate>
                 <Triggers>      
                     

                    </Triggers>
            </ajax:UpdatePanel>
              </div>
        </asp:Panel>

          <ajaxToolkit:ModalPopupExtender ID="mCondiciones" runat="server" DynamicServicePath=""
        Enabled="True" PopupControlID="pnlCondiciones" TargetControlID="btnCerrarCondicion" CancelControlID=""
        BackgroundCssClass="modalBackground">
    </ajaxToolkit:ModalPopupExtender>

       <asp:Button ID="btnCerrarCondicion" Style="display: none;" runat="server" Text="[Open Proxy]" />--%>






    <script language="javascript" type="text/javascript">

         var tiempo = 0;
        var seconds = 0;

        var myVar = setInterval(function () {
            clearInterval(myVar);
        }, 1000);

        var myTiempo = setTimeout(function () {
            clearInterval(myTiempo);
        }, 1);

        function myStopFunction() {
            clearInterval(myVar);
            seconds = '<%=GlobalEntity.Instancia.TiempoDuracionSesion%>' * 60;
            tiempo = '<%=GlobalEntity.Instancia.TiempoDuracionSesion%>' * 1000 * 60;

            CuentaTiempo(tiempo);
        }

        function CuentaTiempo(tiempo) {
            clearTimeout(myTiempo);
            myTiempo = setTimeout(function () {
                document.getElementById('<%=btn_cerrar.ClientID%>').click();
                //window.location = "../Seguridad/eFENIX_Login.aspx";
            }, tiempo);
        }

         function SessionExpireAlert(timeout) {
            seconds = timeout / 1000;
            document.getElementsByName("seconds").innerHTML = seconds;

            myvar = setInterval(function () {
                seconds--;
                document.getElementById("seconds").innerHTML = seconds;
            }, 1000);

            CuentaTiempo(timeout);
        };

        function SelectGroupChecks(ParentID) {
            j = 0;
            k = 1;
            idCheck = ParentID.replace('All', '');
            objCheck = null;
            frm = document.forms[0];

            for (i = 0; i < frm.elements.length; i++) {
                obj = frm.elements[i];

                if ((obj.type == 'checkbox')) {
                    j = j + 1;
                    if (obj.disabled != 1) {
                        obj.checked = document.getElementById(ParentID).checked;
                    } else {
                    }
                }
            }
        }

           function fc_Aprobar()
        {           
            return confirm('¿Seguro de generar la liquidación?');
        }


    </script>



    </asp:Content>
