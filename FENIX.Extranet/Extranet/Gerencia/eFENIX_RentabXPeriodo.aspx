﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Extranet/Seguridad/eFENIX_Principal.master" AutoEventWireup="true" CodeFile="eFENIX_RentabXPeriodo.aspx.cs" Inherits="Extranet_Gerencia_eFENIX_RentabXPeriodo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentCab" Runat="Server">
    <span class="texto_titulo" >Tu Sesion Expirara en &nbsp; 
        <span class="texto_titulo" id="seconds"> </span>
        <span>&nbsp;seg.</span>
     </span>
     <asp:Button ID="btn_cerrar" runat="server" Text="cerrar" 
        onclick="btn_cerrar_Click"  Style="display: none;" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="CntMaster" Runat="Server">
    <table cellspacing="0" cellpadding="0" style="width: 100%;">
        <tr>
            <td class="form_titulo" style="width: 85%">
                <asp:TextBox ID="LblTitulo" Text="Rentabilidad Mensual" ReadOnly="true" 
                    style="background-color: #0069ae; border: #0069ae; color: #FFFFFF; font-family: Calibri; font-size: 13px; font-weight: bold; text-transform: uppercase;" 
                    runat="server" Width="231px"></asp:TextBox>
            </td>
            <td align="right" class="form_titulo_verde" style="width: 15%">
                <asp:TextBox ID="LblTotal" Text="" style="background-color: #0069ae; color: #FFFFFF" ReadOnly="true" BorderColor="#0069ae" runat="server"></asp:TextBox>
            </td>
        </tr>
        
        <tr>
            <td valign="top" colspan="2">
                <table class="form_bandeja" style="margin-top: 5px; margin-bottom: 5px; width: 100%;">
                    <tr>
                        <td >
                            Periodo
                        </td>
                        <td >
                            <asp:DropDownList ID="DplPeriodo" runat="server" >
                            </asp:DropDownList>
                        </td>
                        <td style="width: 11%;">
                            Mes INICIAL
                        </td>
                        <td >
                            <asp:DropDownList ID="DplMesIni" runat="server" Width="100px" >
                            </asp:DropDownList>
                        </td>
                        <td style="width: 11%;">
                            Mes FINAL</td>
                        <td >
                            <asp:DropDownList ID="DplMesFin" runat="server" Width="100px" >
                            </asp:DropDownList>
                        </td>
                        <td >
                            <asp:ImageButton ID="ImgBtnBuscar" runat="server" ImageUrl="~/Imagenes/Formulario/btn_consultar_1.JPG" OnClick="ImgBtnBuscar_Click" />
                        </td>
                    </tr>
                    </table>
            </td>
        </tr>
        
        <tr valign="top" style="height: 100%">
            <td colspan="5">
                <ajax:UpdatePanel ID="upDocumentoOrigen" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div style="overflow: auto; width: 100%; height: 100%">
                            <asp:GridView ID="GrvListado" runat="server" AutoGenerateColumns="False" DataKeyNames="iMes"
                                SkinID="GrillaConsultaUnColor" Width="100%" 
                                OnRowCreated="GrvListado_RowCreated" 
                                onrowdatabound="GrvListado_RowDataBound">
                                <Columns>
                                    <%--<asp:TemplateField> 
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkSeleccionar" runat="server"/>
                                        </ItemTemplate>
                                        <HeaderStyle BackColor="#2BA143" Width="3%" />
                                    </asp:TemplateField>--%>
                                    <asp:BoundField DataField="sMes" HeaderText="" ItemStyle-HorizontalAlign="Left">
                                        <ItemStyle Width="18%" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="iTotalCnt" HeaderText="TEUS" ItemStyle-HorizontalAlign="Right">
                                        <ItemStyle Width="10%" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="iTotalTn" HeaderText="TN" ItemStyle-HorizontalAlign="Right">
                                        <ItemStyle Width="10%" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="dCostoOpe" HeaderText="Operativo" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:N}">
                                        <ItemStyle Width="10%" />
                                    </asp:BoundField>                                    
                                    <asp:BoundField DataField="dCostoAdm" HeaderText="Administr." ItemStyle-HorizontalAlign="Right" DataFormatString="{0:N}" >
                                        <ItemStyle Width="10%" />
                                    </asp:BoundField>                                    
                                    <asp:BoundField DataField="dVentas" HeaderText="Miles US$" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:N}" >
                                        <ItemStyle Width="10%" />
                                    </asp:BoundField>                                    
                                    <asp:BoundField DataField="dUtilidad" HeaderText="Miles US$" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:N}">
                                        <ItemStyle Width="10%" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="dVtaEsperada" HeaderText="Miles US$" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:N}">
                                        <ItemStyle Width="10%" />
                                    </asp:BoundField>
                                    
                                    <asp:ButtonField ButtonType="Image" CommandName="LNegocio" HeaderText="L.Negocio" ImageUrl="../../Imagenes/Negocio.png" Text="Aprobar" ItemStyle-HorizontalAlign="Center">
                                        <ItemStyle Width="6%" />
                                    </asp:ButtonField>
                                    <asp:ButtonField ButtonType="Image" CommandName="Servicio" HeaderText="Servicios" ItemStyle-HorizontalAlign="Center" ImageUrl="../../Imagenes/Sevicio.png" Text="Ver Detalle">
                                        <ItemStyle Width="6%" />
                                    </asp:ButtonField>
                                </Columns>
                            </asp:GridView>
                        </div>
                    </ContentTemplate>
                    <Triggers>
                        <ajax:AsyncPostBackTrigger ControlID="GrvListado" EventName="SelectedIndexChanged" />                        
                        <ajax:AsyncPostBackTrigger ControlID="ImgBtnBuscar" EventName="Click" />
<asp:AsyncPostBackTrigger ControlID="GrvListado" EventName="SelectedIndexChanged"></asp:AsyncPostBackTrigger>
<asp:AsyncPostBackTrigger ControlID="ImgBtnBuscar" EventName="Click"></asp:AsyncPostBackTrigger>
<asp:AsyncPostBackTrigger ControlID="GrvListado" EventName="SelectedIndexChanged"></asp:AsyncPostBackTrigger>
<asp:AsyncPostBackTrigger ControlID="ImgBtnBuscar" EventName="Click"></asp:AsyncPostBackTrigger>
                    </Triggers>
                </ajax:UpdatePanel>
            </td>
        </tr>
        
        
    </table>


    <script language="javascript" type="text/javascript">
        var tiempo = 0;
        var seconds = 0;

        var myVar = setInterval(function () {
            clearInterval(myVar);
        }, 1000);

        var myTiempo = setTimeout(function () {
            clearInterval(myTiempo);
        }, 1);

        function myStopFunction() {
            clearInterval(myVar);
            seconds = '<%=GlobalEntity.Instancia.TiempoDuracionSesion%>' * 60;
            tiempo = '<%=GlobalEntity.Instancia.TiempoDuracionSesion%>' * 1000 * 60;

            CuentaTiempo(tiempo);
        }

        function CuentaTiempo(tiempo) {
            clearTimeout(myTiempo);
            myTiempo = setTimeout(function () {
                document.getElementById('<%=btn_cerrar.ClientID%>').click();
                //window.location = "../Seguridad/eFENIX_Login.aspx";
            }, tiempo);
        }



        function SessionExpireAlert(timeout) {
            seconds = timeout / 1000;
            document.getElementsByName("seconds").innerHTML = seconds;

            myvar = setInterval(function () {
                seconds--;
                document.getElementById("seconds").innerHTML = seconds;
            }, 1000);

            CuentaTiempo(timeout);
        };


        var goodexit = false;
        window.onbeforeunload = confirmRegisterExit;
        function confirmRegisterExit(evt) {
            if (!goodexit) {
                //return "Si vas abandonar el sistema haz click en Cerrar Sesion!!";
                document.getElementById('<%=btn_cerrar.ClientID%>').click();
            }
        }


        function AbrirPagina(iMes) {
            sPeriodo = document.getElementById('<%=DplPeriodo.ClientID%>').value;
            sMesIni = document.getElementById('<%=DplMesIni.ClientID%>').value;
            sMesFin = document.getElementById('<%=DplMesFin.ClientID%>').value;            
            window.location.href = "eFENIX_RentabXLinea.aspx?Periodo=" + sPeriodo + "&rIni=" + sMesIni + "&rFin=" + sMesFin + "&mIni=" + iMes + "&mFin="+ iMes+ "&VentanaAnt=P";
        }

        function AbrirPagServicio(iMes) {
            sPeriodo = document.getElementById('<%=DplPeriodo.ClientID%>').value;
            sMesIni = document.getElementById('<%=DplMesIni.ClientID%>').value;
            sMesFin = document.getElementById('<%=DplMesFin.ClientID%>').value;
            window.location.href = "eFENIX_RentabXServicio.aspx?Periodo=" + sPeriodo + "&rIni=" + sMesIni + "&rFin=" + sMesFin + "&mIni=" + iMes + "&mFin=" + iMes + "&Ventana=P";
        }

    </script>
</asp:Content>

