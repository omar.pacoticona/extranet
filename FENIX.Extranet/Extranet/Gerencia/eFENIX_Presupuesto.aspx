﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Extranet/Seguridad/eFENIX_Principal.master" AutoEventWireup="true" CodeFile="eFENIX_Presupuesto.aspx.cs" Inherits="Extranet_Gerencia_eFENIX_Presupuesto" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CntMaster" Runat="Server">
    <table cellspacing="0" cellpadding="0" style="width: 100%;">
        <tr>
            <td class="form_titulo" style="width: 85%">
                <asp:TextBox ID="LblTitulo" Text="Mantenimiento - Presupuesto Anual" ReadOnly="true" 
                    style="background-color: #0069ae; border: #0069ae; color: #FFFFFF; font-family: Calibri; font-size: 13px; font-weight: bold; text-transform: uppercase;" 
                    runat="server" Width="231px"></asp:TextBox>
            </td>
            <td align="right" class="form_titulo_verde" style="width: 15%">
                <asp:TextBox ID="LblTotal" Text="" style="background-color: #0069ae; color: #FFFFFF" ReadOnly="true" BorderColor="#0069ae" runat="server"></asp:TextBox>
            </td>
        </tr>
        
        <tr>
            <td valign="top" colspan="2">
                <table class="form_bandeja" style="margin-top: 5px; margin-bottom: 5px; width: 100%;">
                    <tr>
                        <td style="width: 6%;">
                            Periodo
                        </td>
                        <td style="width: 6%;">
                            <asp:DropDownList ID="DplPeriodo" runat="server" AutoPostBack="True" 
                                onselectedindexchanged="DplPeriodo_SelectedIndexChanged">
                            </asp:DropDownList>
                        </td>
                        <td style="width: 16%;">
                            &nbsp;</td>
                        <td style="width: 11%;">
                            
                            &nbsp;</td>
                        <td style="width: 35%;">
                            importes registrados en miles de DoLARES</td>
                        <td style="width: 10%;">
                            <asp:ImageButton ID="ImgBtnBuscar" runat="server" ImageUrl="~/Imagenes/Formulario/btn_consultar_1.JPG"
                                OnClick="ImgBtnBuscar_Click" />
                        </td>
                        <td style="width: 14%;">
                            &nbsp;</td>
                    </tr>                   
                </table>
            </td>
        </tr>
        
        <tr valign="top" style="height: 100%">
            <td colspan="5">
                <ajax:UpdatePanel ID="upNegocio" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <%--<div style="overflow: auto; width: 100%; height: 100%">--%>
                            <asp:GridView ID="GrvListado" runat="server" AutoGenerateColumns="False" DataKeyNames="iIdPresupuesto,iIdLineaNegocio, sNegocio"
                                SkinID="GrillaConsultaUnColor" Width="100%" ShowFooter="true" 
                            onrowdatabound="GrvListado_RowDataBound" 
                            onrowediting="GrvListado_RowEditing" 
                            onrowcancelingedit="GrvListado_RowCancelingEdit" 
                            onrowupdating="GrvListado_RowUpdating" 
                            onrowcommand="GrvListado_RowCommand">
                                <Columns>                                    
                                    <asp:TemplateField HeaderText="" Visible="false">                            
                                        <ItemTemplate>
                                            <asp:Label ID="iIdLineaNegocio" runat="server" Text='<%# Eval("iIdLineaNegocio") %>'></asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:TextBox ID="TxtIdGrupo" runat="server" Text='<%# Bind("iIdLineaNegocio") %>' ></asp:TextBox>
                                        </EditItemTemplate>
                                        <FooterTemplate >
                                            <asp:textbox ID="TxtIdGrupo" runat="server"/>
                                        </FooterTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField  HeaderText="Negocio" HeaderStyle-Width="150px">
                                        <ItemTemplate>
                                            <asp:Label ID="LblNegocio" runat ="server" Text='<%# Eval("sNegocio") %>' /> 
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:DropDownList ID="DplLineaNegocioEdit" runat="server" Enabled="false" Width="150px"/>
                                        </EditItemTemplate>
                                        <FooterTemplate>
                                            <asp:DropDownList ID="DplLineaNegocioNew" Width="180px" runat="server">
                                            </asp:DropDownList>
                                        </FooterTemplate>
                                        
                                    </asp:TemplateField>                                                                        
                                                                                                                                         
                                    <asp:TemplateField HeaderText="Enero" HeaderStyle-Width="60px" ItemStyle-HorizontalAlign="Right" >
                                        <ItemTemplate >
                                            <%--<asp:Label ID="LblMes1" runat ="server" Text='<%# Eval("iMes1") %>' />--%>
                                            <asp:Label ID="LblMes1" runat ="server" Text='<%# DataBinder.Eval(Container.DataItem, "iMes1", "{0:#,###}") %>' />
                                        </ItemTemplate>
                              
                                        <EditItemTemplate>
                                            <%--<asp:TextBox ID="TxtMes1" runat="server" Width="45px" Text='<%# Bind("iMes1") %>'></asp:TextBox>--%>
                                            <asp:TextBox ID="TxtMes1" runat="server" Width="45px" Text='<%# DataBinder.Eval(Container.DataItem, "iMes1", "{0:####}") %>'  />
                                        </EditItemTemplate>
                                        <FooterTemplate>
                                            <asp:TextBox ID="TxtMes1New" Width="60px" runat="server" SkinID="txtNormal" Style="text-align: right"></asp:TextBox>
                                        </FooterTemplate>
                                        
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Febrero" HeaderStyle-Width="60px" ItemStyle-HorizontalAlign="Right">
                                        <ItemTemplate>
                                            <asp:Label ID="LblMes2" runat ="server" Text='<%# DataBinder.Eval(Container.DataItem, "iMes2", "{0:#,###}") %>' />
                                        </ItemTemplate>
                              
                                        <EditItemTemplate>
                                            <asp:TextBox ID="TxtMes2" runat="server" Width="45px" Text='<%# DataBinder.Eval(Container.DataItem, "iMes2", "{0:####}") %>'  />
                                        </EditItemTemplate>
                                        <FooterTemplate>
                                            <asp:TextBox ID="TxtMes2New" Width="60px" runat="server" SkinID="txtNormal" Style="text-align: right"></asp:TextBox>
                                        </FooterTemplate>
                                        
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Marzo" HeaderStyle-Width="60px" ItemStyle-HorizontalAlign="Right">
                                        <ItemTemplate>
                                            <asp:Label ID="LblMes3" runat ="server" Text='<%# DataBinder.Eval(Container.DataItem, "iMes3", "{0:#,###}") %>' />
                                        </ItemTemplate>
                              
                                        <EditItemTemplate>
                                            <asp:TextBox ID="TxtMes3" runat="server" Width="45px" Text='<%# DataBinder.Eval(Container.DataItem, "iMes3", "{0:####}") %>'  />
                                        </EditItemTemplate>
                                        <FooterTemplate>
                                            <asp:TextBox ID="TxtMes3New" Width="60px" runat="server" SkinID="txtNormal" Style="text-align: right"></asp:TextBox>
                                        </FooterTemplate>
                                        
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Abril" HeaderStyle-Width="60px" ItemStyle-HorizontalAlign="Right">
                                        <ItemTemplate>
                                            <asp:Label ID="LblMes4" runat ="server" Text='<%# DataBinder.Eval(Container.DataItem, "iMes4", "{0:#,###}") %>' />
                                        </ItemTemplate>
                              
                                        <EditItemTemplate>
                                            <asp:TextBox ID="TxtMes4" runat="server" Width="45px" Text='<%# DataBinder.Eval(Container.DataItem, "iMes4", "{0:####}") %>'  />
                                        </EditItemTemplate>
                                        <FooterTemplate>
                                            <asp:TextBox ID="TxtMes4New" Width="60px" runat="server" SkinID="txtNormal" Style="text-align: right"></asp:TextBox>
                                        </FooterTemplate>
                                        
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Mayo" HeaderStyle-Width="60px" ItemStyle-HorizontalAlign="Right">
                                        <ItemTemplate>
                                            <asp:Label ID="LblMes5" runat ="server" Text='<%# DataBinder.Eval(Container.DataItem, "iMes5", "{0:#,###}") %>' />
                                        </ItemTemplate>
                              
                                        <EditItemTemplate>
                                            <asp:TextBox ID="TxtMes5" runat="server" Width="45px" Text='<%# DataBinder.Eval(Container.DataItem, "iMes5", "{0:####}") %>'  />
                                        </EditItemTemplate>
                                        <FooterTemplate>
                                            <asp:TextBox ID="TxtMes5New" Width="60px" runat="server" SkinID="txtNormal" Style="text-align: right"></asp:TextBox>
                                        </FooterTemplate>
                                        
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Junio" HeaderStyle-Width="60px" ItemStyle-HorizontalAlign="Right">
                                        <ItemTemplate>
                                            <asp:Label ID="LblMes6" runat ="server" Text='<%# DataBinder.Eval(Container.DataItem, "iMes6", "{0:#,###}") %>' />
                                        </ItemTemplate>
                              
                                        <EditItemTemplate>
                                            <asp:TextBox ID="TxtMes6" runat="server" Width="45px" Text='<%# DataBinder.Eval(Container.DataItem, "iMes6", "{0:####}") %>'  />
                                        </EditItemTemplate>
                                        <FooterTemplate>
                                            <asp:TextBox ID="TxtMes6New" Width="60px" runat="server" SkinID="txtNormal" Style="text-align: right"></asp:TextBox>
                                        </FooterTemplate>
                                        
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Julio" HeaderStyle-Width="60px" ItemStyle-HorizontalAlign="Right">
                                        <ItemTemplate>
                                            <asp:Label ID="LblMes7" runat ="server" Text='<%# DataBinder.Eval(Container.DataItem, "iMes7", "{0:#,###}") %>' />
                                        </ItemTemplate>
                              
                                        <EditItemTemplate>
                                            <asp:TextBox ID="TxtMes7" runat="server" Width="45px" Text='<%# DataBinder.Eval(Container.DataItem, "iMes7", "{0:####}") %>'  />
                                        </EditItemTemplate>
                                        <FooterTemplate>
                                            <asp:TextBox ID="TxtMes7New" Width="60px" runat="server" SkinID="txtNormal" Style="text-align: right"></asp:TextBox>
                                        </FooterTemplate>
                                        
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Agosto" HeaderStyle-Width="60px" ItemStyle-HorizontalAlign="Right">
                                        <ItemTemplate>
                                            <asp:Label ID="LblMes8" runat ="server" Text='<%# DataBinder.Eval(Container.DataItem, "iMes8", "{0:#,###}") %>' />
                                        </ItemTemplate>
                              
                                        <EditItemTemplate>
                                            <asp:TextBox ID="TxtMes8" runat="server" Width="45px" Text='<%# DataBinder.Eval(Container.DataItem, "iMes8", "{0:####}") %>'  />
                                        </EditItemTemplate>
                                        <FooterTemplate>
                                            <asp:TextBox ID="TxtMes8New" Width="60px" runat="server" SkinID="txtNormal" Style="text-align: right"></asp:TextBox>
                                        </FooterTemplate>
                                        
                                    </asp:TemplateField>
                                    
                                    <asp:TemplateField HeaderText="Set." HeaderStyle-Width="60px" ItemStyle-HorizontalAlign="Right">
                                        <ItemTemplate>
                                            <asp:Label ID="LblMes9" runat ="server" Text='<%# DataBinder.Eval(Container.DataItem, "iMes9", "{0:#,###}") %>' />
                                        </ItemTemplate>
                              
                                        <EditItemTemplate>
                                            <asp:TextBox ID="TxtMes9" runat="server" Width="45px" Text='<%# DataBinder.Eval(Container.DataItem, "iMes9", "{0:####}") %>'  />
                                        </EditItemTemplate>
                                        <FooterTemplate>
                                            <asp:TextBox ID="TxtMes9New" Width="60px" runat="server" SkinID="txtNormal" Style="text-align: right"></asp:TextBox>
                                        </FooterTemplate>
                                        
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Oct." HeaderStyle-Width="60px" ItemStyle-HorizontalAlign="Right">
                                        <ItemTemplate>
                                            <asp:Label ID="LblMes10" runat ="server" Text='<%# DataBinder.Eval(Container.DataItem, "iMes10", "{0:#,###}") %>' />
                                        </ItemTemplate>
                              
                                        <EditItemTemplate>
                                            <asp:TextBox ID="TxtMes10" runat="server" Width="45px" Text='<%# DataBinder.Eval(Container.DataItem, "iMes10", "{0:####}") %>'  />
                                        </EditItemTemplate>
                                        <FooterTemplate>
                                            <asp:TextBox ID="TxtMes10New" Width="60px" runat="server" SkinID="txtNormal" Style="text-align: right"></asp:TextBox>
                                        </FooterTemplate>
                                        
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Nov." HeaderStyle-Width="60px" ItemStyle-HorizontalAlign="Right">
                                        <ItemTemplate>
                                            <asp:Label ID="LblMes11" runat ="server" Text='<%# DataBinder.Eval(Container.DataItem, "iMes11", "{0:#,###}") %>' />
                                        </ItemTemplate>
                              
                                        <EditItemTemplate>
                                            <asp:TextBox ID="TxtMes11" runat="server" Width="45px" Text='<%# DataBinder.Eval(Container.DataItem, "iMes11", "{0:####}") %>'  />
                                        </EditItemTemplate>
                                        <FooterTemplate>
                                            <asp:TextBox ID="TxtMes11New" Width="60px" runat="server" SkinID="txtNormal" Style="text-align: right"></asp:TextBox>
                                        </FooterTemplate>
                                        
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Dic." HeaderStyle-Width="60px" ItemStyle-HorizontalAlign="Right">
                                        <ItemTemplate>
                                            <asp:Label ID="LblMes12" runat ="server" Text='<%# DataBinder.Eval(Container.DataItem, "iMes12", "{0:#,###}") %>' />
                                        </ItemTemplate>
                              
                                        <EditItemTemplate>
                                            <asp:TextBox ID="TxtMes12" runat="server" Width="45px" Text='<%# DataBinder.Eval(Container.DataItem, "iMes12", "{0:####}") %>'  />
                                        </EditItemTemplate>
                                        <FooterTemplate>
                                            <asp:TextBox ID="TxtMes12New" Width="60px" runat="server" SkinID="txtNormal" Style="text-align: right"></asp:TextBox>
                                        </FooterTemplate>
                                        
                                    </asp:TemplateField>

                                    <asp:TemplateField ShowHeader="False"  HeaderStyle-Width="22px">                                        
                                        <ItemTemplate>
                                            <asp:ImageButton ID="ImageButton1" runat="server" CausesValidation="False" Tooltip="Editar"
                                                            CommandName="Edit" ImageUrl="~/Imagenes/modificar.PNG"  />
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:ImageButton ID="btnUpdate" runat="server" CausesValidation="True" 
                                                  CommandName="Update" ImageUrl="~/Imagenes/grabar.PNG"  />
                                                  &nbsp;<asp:ImageButton ID="btnCancel" runat="server" CausesValidation="False" 
                                                  CommandName="Cancel" ImageUrl="~/Imagenes/salir.PNG"  />
                                        </EditItemTemplate>
                                        
                                        <FooterTemplate>                               
                                           <asp:ImageButton ID="btnAgregar" runat="server" ImageUrl="~/Imagenes/agregar.png" OnClick="btnAgregar_Click" /> 
                                        </FooterTemplate>
                                        <%--<ItemStyle Width="20px" />--%>
                                    </asp:TemplateField>
                                    <%--<asp:ButtonField ButtonType="Image" CommandName="Eliminar" ImageUrl="~/Imagenes/eliminar.png" Text="Botón" />--%>
                                    <asp:TemplateField>
                                        <ItemTemplate>

                                            <asp:ImageButton ID="imgEliminar" runat="server" CommandName="Eliminar" ImageUrl="~/Imagenes/eliminar.png" Tooltip="Eliminar"
                                             CommandArgument="<%# ((GridViewRow) Container).RowIndex %>" OnClientClick="return confirm('Esta Seguro de Eliminar el Registro?');"  />

                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <EditRowStyle BackColor="#CCCCCC" Width="0px" />
                            </asp:GridView>
                       <%--</div>--%>
                    </ContentTemplate>                    
                </ajax:UpdatePanel>
            </td>
        </tr>
        
        <tr>
            <td colspan="2" style=" height: 50px;">
                <table class="form_bandeja" style="margin-top: 0px; margin-bottom: 0px; width: 100%; height:100%;">
                    <tr>
                        <td style="width: 6%;">
                            
                        </td>
                        <td style="width: 6%;">
                            &nbsp;</td>
                        <td style="width: 16%;">
                            &nbsp;</td>
                        <td style="width: 11%;">
                            
                            &nbsp;</td>
                        <td style="width: 35%;">
                        </td>
                        <td style="width: 10%;">
                            &nbsp;</td>
                        <td style="width: 14%;">
                            &nbsp;</td>
                    </tr>                   
                </table>
            </td>
        </tr>
        <tr valign="top" style="height: 100%">
            <td colspan="5">
                <ajax:UpdatePanel ID="UpVendedor" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <%--<div style="overflow: auto; width: 100%; height: 100%">--%>
                            <asp:GridView ID="GrvListado2" runat="server" AutoGenerateColumns="False" DataKeyNames="iIdPresupuesto,iIdVendedor, sVendedor"
                                SkinID="GrillaConsultaUnColor" Width="100%" ShowFooter="true" 
                            onrowdatabound="GrvListado2_RowDataBound" 
                            onrowediting="GrvListado2_RowEditing" 
                            onrowcancelingedit="GrvListado2_RowCancelingEdit" 
                            onrowupdating="GrvListado2_RowUpdating" 
                            onrowcommand="GrvListado2_RowCommand">
                                <Columns>                                    
                                    <asp:TemplateField HeaderText="" Visible="false">                            
                                        <ItemTemplate>
                                            <asp:Label ID="iIdVendedor" runat="server" Text='<%# Eval("iIdVendedor") %>'></asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:TextBox ID="TxtIdVendedor" runat="server" Text='<%# Bind("iIdVendedor") %>' ></asp:TextBox>
                                        </EditItemTemplate>
                                        <FooterTemplate >
                                            <asp:textbox ID="TxtIdVendedor" runat="server"/>
                                        </FooterTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField  HeaderText="Vendedor" HeaderStyle-Width="150px">
                                        <ItemTemplate>
                                            <asp:Label ID="LblVendedor" runat ="server" Text='<%# Eval("sVendedor") %>' /> 
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:DropDownList ID="DplVendedor" runat="server" Enabled="false" Width="150px"/>
                                        </EditItemTemplate>
                                        <FooterTemplate>
                                            <asp:DropDownList ID="DplVendedorNew" Width="180px" runat="server">
                                            </asp:DropDownList>
                                        </FooterTemplate>
                                        
                                    </asp:TemplateField>                                                                        
                                                                                                                                         
                                    <asp:TemplateField HeaderText="Enero" HeaderStyle-Width="60px" ItemStyle-HorizontalAlign="Right" >
                                        <ItemTemplate >
                                            <%--<asp:Label ID="LblMes1" runat ="server" Text='<%# Eval("decMes1") %>' />--%>
                                            <asp:Label ID="LblMes1" runat ="server" Text='<%# DataBinder.Eval(Container.DataItem, "iMes1", "{0:#,###}") %>' />
                                        </ItemTemplate>
                              
                                        <EditItemTemplate>
                                            <%--<asp:TextBox ID="TxtMes1" runat="server" Width="45px" Text='<%# Bind("decMes1") %>'></asp:TextBox>--%>
                                            <asp:TextBox ID="TxtMes1" runat="server" Width="45px" Text='<%# DataBinder.Eval(Container.DataItem, "iMes1", "{0:####}") %>'  />
                                        </EditItemTemplate>
                                        <FooterTemplate>
                                            <asp:TextBox ID="TxtMes1New" Width="60px" runat="server" SkinID="txtNormal" Style="text-align: right"></asp:TextBox>
                                        </FooterTemplate>
                                        
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Febrero" HeaderStyle-Width="60px" ItemStyle-HorizontalAlign="Right">
                                        <ItemTemplate>
                                            <asp:Label ID="LblMes2" runat ="server" Text='<%# DataBinder.Eval(Container.DataItem, "iMes2", "{0:#,###}") %>' />
                                        </ItemTemplate>
                              
                                        <EditItemTemplate>
                                            <asp:TextBox ID="TxtMes2" runat="server" Width="45px" Text='<%# DataBinder.Eval(Container.DataItem, "iMes2", "{0:####}") %>'  />
                                        </EditItemTemplate>
                                        <FooterTemplate>
                                            <asp:TextBox ID="TxtMes2New" Width="60px" runat="server" SkinID="txtNormal" Style="text-align: right"></asp:TextBox>
                                        </FooterTemplate>
                                        
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Marzo" HeaderStyle-Width="60px" ItemStyle-HorizontalAlign="Right">
                                        <ItemTemplate>
                                            <asp:Label ID="LblMes3" runat ="server" Text='<%# DataBinder.Eval(Container.DataItem, "iMes3", "{0:#,###}") %>' />
                                        </ItemTemplate>
                              
                                        <EditItemTemplate>
                                            <asp:TextBox ID="TxtMes3" runat="server" Width="45px" Text='<%# DataBinder.Eval(Container.DataItem, "iMes3", "{0:####}") %>'  />
                                        </EditItemTemplate>
                                        <FooterTemplate>
                                            <asp:TextBox ID="TxtMes3New" Width="60px" runat="server" SkinID="txtNormal" Style="text-align: right"></asp:TextBox>
                                        </FooterTemplate>
                                        
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Abril" HeaderStyle-Width="60px" ItemStyle-HorizontalAlign="Right">
                                        <ItemTemplate>
                                            <asp:Label ID="LblMes4" runat ="server" Text='<%# DataBinder.Eval(Container.DataItem, "iMes4", "{0:#,###}") %>' />
                                        </ItemTemplate>
                              
                                        <EditItemTemplate>
                                            <asp:TextBox ID="TxtMes4" runat="server" Width="45px" Text='<%# DataBinder.Eval(Container.DataItem, "iMes4", "{0:####}") %>'  />
                                        </EditItemTemplate>
                                        <FooterTemplate>
                                            <asp:TextBox ID="TxtMes4New" Width="60px" runat="server" SkinID="txtNormal" Style="text-align: right"></asp:TextBox>
                                        </FooterTemplate>
                                        
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Mayo" HeaderStyle-Width="60px" ItemStyle-HorizontalAlign="Right">
                                        <ItemTemplate>
                                            <asp:Label ID="LblMes5" runat ="server" Text='<%# DataBinder.Eval(Container.DataItem, "iMes5", "{0:#,###}") %>' />
                                        </ItemTemplate>
                              
                                        <EditItemTemplate>
                                            <asp:TextBox ID="TxtMes5" runat="server" Width="45px" Text='<%# DataBinder.Eval(Container.DataItem, "iMes5", "{0:####}") %>'  />
                                        </EditItemTemplate>
                                        <FooterTemplate>
                                            <asp:TextBox ID="TxtMes5New" Width="60px" runat="server" SkinID="txtNormal" Style="text-align: right"></asp:TextBox>
                                        </FooterTemplate>
                                        
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Junio" HeaderStyle-Width="60px" ItemStyle-HorizontalAlign="Right">
                                        <ItemTemplate>
                                            <asp:Label ID="LblMes6" runat ="server" Text='<%# DataBinder.Eval(Container.DataItem, "iMes6", "{0:#,###}") %>' />
                                        </ItemTemplate>
                              
                                        <EditItemTemplate>
                                            <asp:TextBox ID="TxtMes6" runat="server" Width="45px" Text='<%# DataBinder.Eval(Container.DataItem, "iMes6", "{0:####}") %>'  />
                                        </EditItemTemplate>
                                        <FooterTemplate>
                                            <asp:TextBox ID="TxtMes6New" Width="60px" runat="server" SkinID="txtNormal" Style="text-align: right"></asp:TextBox>
                                        </FooterTemplate>
                                        
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Julio" HeaderStyle-Width="60px" ItemStyle-HorizontalAlign="Right">
                                        <ItemTemplate>
                                            <asp:Label ID="LblMes7" runat ="server" Text='<%# DataBinder.Eval(Container.DataItem, "iMes7", "{0:#,###}") %>' />
                                        </ItemTemplate>
                              
                                        <EditItemTemplate>
                                            <asp:TextBox ID="TxtMes7" runat="server" Width="45px" Text='<%# DataBinder.Eval(Container.DataItem, "iMes7", "{0:####}") %>'  />
                                        </EditItemTemplate>
                                        <FooterTemplate>
                                            <asp:TextBox ID="TxtMes7New" Width="60px" runat="server" SkinID="txtNormal" Style="text-align: right"></asp:TextBox>
                                        </FooterTemplate>
                                        
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Agosto" HeaderStyle-Width="60px" ItemStyle-HorizontalAlign="Right">
                                        <ItemTemplate>
                                            <asp:Label ID="LblMes8" runat ="server" Text='<%# DataBinder.Eval(Container.DataItem, "iMes8", "{0:#,###}") %>' />
                                        </ItemTemplate>
                              
                                        <EditItemTemplate>
                                            <asp:TextBox ID="TxtMes8" runat="server" Width="45px" Text='<%# DataBinder.Eval(Container.DataItem, "iMes8", "{0:####}") %>'  />
                                        </EditItemTemplate>
                                        <FooterTemplate>
                                            <asp:TextBox ID="TxtMes8New" Width="60px" runat="server" SkinID="txtNormal" Style="text-align: right"></asp:TextBox>
                                        </FooterTemplate>
                                        
                                    </asp:TemplateField>
                                    
                                    <asp:TemplateField HeaderText="Set." HeaderStyle-Width="60px" ItemStyle-HorizontalAlign="Right">
                                        <ItemTemplate>
                                            <asp:Label ID="LblMes9" runat ="server" Text='<%# DataBinder.Eval(Container.DataItem, "iMes9", "{0:#,###}") %>' />
                                        </ItemTemplate>
                              
                                        <EditItemTemplate>
                                            <asp:TextBox ID="TxtMes9" runat="server" Width="45px" Text='<%# DataBinder.Eval(Container.DataItem, "iMes9", "{0:####}") %>'  />
                                        </EditItemTemplate>
                                        <FooterTemplate>
                                            <asp:TextBox ID="TxtMes9New" Width="60px" runat="server" SkinID="txtNormal" Style="text-align: right"></asp:TextBox>
                                        </FooterTemplate>
                                        
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Oct." HeaderStyle-Width="60px" ItemStyle-HorizontalAlign="Right">
                                        <ItemTemplate>
                                            <asp:Label ID="LblMes10" runat ="server" Text='<%# DataBinder.Eval(Container.DataItem, "iMes10", "{0:#,###}") %>' />
                                        </ItemTemplate>
                              
                                        <EditItemTemplate>
                                            <asp:TextBox ID="TxtMes10" runat="server" Width="45px" Text='<%# DataBinder.Eval(Container.DataItem, "iMes10", "{0:####}") %>'  />
                                        </EditItemTemplate>
                                        <FooterTemplate>
                                            <asp:TextBox ID="TxtMes10New" Width="60px" runat="server" SkinID="txtNormal" Style="text-align: right"></asp:TextBox>
                                        </FooterTemplate>
                                        
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Nov." HeaderStyle-Width="60px" ItemStyle-HorizontalAlign="Right">
                                        <ItemTemplate>
                                            <asp:Label ID="LblMes11" runat ="server" Text='<%# DataBinder.Eval(Container.DataItem, "iMes11", "{0:#,###}") %>' />
                                        </ItemTemplate>
                              
                                        <EditItemTemplate>
                                            <asp:TextBox ID="TxtMes11" runat="server" Width="45px" Text='<%# DataBinder.Eval(Container.DataItem, "iMes11", "{0:####}") %>'  />
                                        </EditItemTemplate>
                                        <FooterTemplate>
                                            <asp:TextBox ID="TxtMes11New" Width="60px" runat="server" SkinID="txtNormal" Style="text-align: right"></asp:TextBox>
                                        </FooterTemplate>
                                        
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Dic." HeaderStyle-Width="60px" ItemStyle-HorizontalAlign="Right">
                                        <ItemTemplate>
                                            <asp:Label ID="LblMes12" runat ="server" Text='<%# DataBinder.Eval(Container.DataItem, "iMes12", "{0:#,###}") %>' />
                                        </ItemTemplate>
                              
                                        <EditItemTemplate>
                                            <asp:TextBox ID="TxtMes12" runat="server" Width="45px" Text='<%# DataBinder.Eval(Container.DataItem, "iMes12", "{0:####}") %>'  />
                                        </EditItemTemplate>
                                        <FooterTemplate>
                                            <asp:TextBox ID="TxtMes12New" Width="60px" runat="server" SkinID="txtNormal" Style="text-align: right"></asp:TextBox>
                                        </FooterTemplate>
                                        
                                    </asp:TemplateField>

                                    <asp:TemplateField ShowHeader="False"  HeaderStyle-Width="22px">                                        
                                        <ItemTemplate>
                                            <asp:ImageButton ID="ImageButton1" runat="server" CausesValidation="False" Tooltip="Editar"
                                                  CommandName="Edit" ImageUrl="~/Imagenes/modificar.PNG"  />
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:ImageButton ID="btnUpdate" runat="server" CausesValidation="True" 
                                                  CommandName="Update" ImageUrl="~/Imagenes/grabar.PNG"  />
                                                  &nbsp;<asp:ImageButton ID="btnCancel" runat="server" CausesValidation="False" 
                                                  CommandName="Cancel" ImageUrl="~/Imagenes/salir.PNG"  />
                                        </EditItemTemplate>
                                        
                                        <FooterTemplate>                               
                                           <asp:ImageButton ID="btnAgregar2" runat="server" ImageUrl="~/Imagenes/agregar.png" OnClick="btnAgregar2_Click" /> 
                                        </FooterTemplate>                                        
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imgEliminar" runat="server" CommandName="Eliminar" ImageUrl="~/Imagenes/eliminar.png" Tooltip="Eliminar"
                                             CommandArgument="<%# ((GridViewRow) Container).RowIndex %>" OnClientClick="return confirm('Esta Seguro de Eliminar el Registro?');"  />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <EditRowStyle BackColor="#CCCCCC" Width="0px" />
                            </asp:GridView>
                       <%--</div>--%>
                    </ContentTemplate>                    
                </ajax:UpdatePanel>
            </td>
        </tr>
    </table>


    <script language="javascript" type="text/javascript">
        var tiempo = 0;
        var seconds = 0;

        var myVar = setInterval(function () {
            clearInterval(myVar);
        }, 1000);

        var myTiempo = setTimeout(function () {
            clearInterval(myTiempo);
        }, 1);

        function myStopFunction() {
            clearInterval(myVar);
            seconds = '<%=GlobalEntity.Instancia.TiempoDuracionSesion%>' * 60;
            tiempo = '<%=GlobalEntity.Instancia.TiempoDuracionSesion%>' * 1000 * 60;

            CuentaTiempo(tiempo);
        }

        function CuentaTiempo(tiempo) {
            clearTimeout(myTiempo);
            myTiempo = setTimeout(function () {
                document.getElementById('<%=btn_cerrar.ClientID%>').click();
                //window.location = "../Seguridad/eFENIX_Login.aspx";
            }, tiempo);
        }



        function SessionExpireAlert(timeout) {
            seconds = timeout / 1000;
            document.getElementsByName("seconds").innerHTML = seconds;

            myvar = setInterval(function () {
                seconds--;
                document.getElementById("seconds").innerHTML = seconds;
            }, 1000);

            CuentaTiempo(timeout);
        };


        var goodexit = false;
        window.onbeforeunload = confirmRegisterExit;
        function confirmRegisterExit(evt) {
            if (!goodexit) {
                //return "Si vas abandonar el sistema haz click en Cerrar Sesion!!";
                document.getElementById('<%=btn_cerrar.ClientID%>').click();
            }
        }

    </script>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentCab" Runat="Server">
    <span class="texto_titulo" >Tu Sesion Expirara en &nbsp; 
        <span class="texto_titulo" id="seconds"> </span>
        <span>&nbsp;seg.</span>
     </span>
     <asp:Button ID="btn_cerrar" runat="server" Text="cerrar" 
        onclick="btn_cerrar_Click"  Style="display: none;" />
     
</asp:Content>

