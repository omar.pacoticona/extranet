﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using FENIX.BusinessEntity;
using FENIX.BusinessLogic;
using System.Drawing;
using System.Collections.Generic;

public partial class Extranet_Operaciones_eFENIX_PeriodoAdministrativo : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {



        TxtDiasFac001.Attributes.Add("onkeypress", "javascript:return SoloEnteros(event);");
        TxtDiasFac005.Attributes.Add("onkeypress", "javascript:return SoloEnteros(event);");
        TxtDiasFac006.Attributes.Add("onkeypress", "javascript:return SoloEnteros(event);");
        TxtDiasFac007.Attributes.Add("onkeypress", "javascript:return SoloEnteros(event);");
        TxtDiasFac008.Attributes.Add("onkeypress", "javascript:return SoloEnteros(event);");
        TxtDiasBol001.Attributes.Add("onkeypress", "javascript:return SoloEnteros(event);");
        TxtDiasNC001.Attributes.Add("onkeypress", "javascript:return SoloEnteros(event);");
        TxtDiasND001.Attributes.Add("onkeypress", "javascript:return SoloEnteros(event);");

        //hdVarTiempoSesion.Value = (Convert.ToInt32( GlobalEntity.Instancia.TiempoDuracionSesion.ToString()) * 1000 * 60).ToString();
        String tiempo = (Convert.ToInt32(GlobalEntity.Instancia.TiempoDuracionSesion.ToString()) * 1000 * 60).ToString();
        Session["Reset"] = true;
        

        if (!Page.IsPostBack)
        {
            ListarDropDowList();

            if (DateTime.Now.Month == 1)
            {
                DplAnioPeriodo.SelectedValue = (DateTime.Now.Year - 1).ToString();
                DplMesPeriodo.SelectedValue = "12";
            }
            else
            {
                DplAnioPeriodo.SelectedValue = DateTime.Now.Year.ToString();
                DplMesPeriodo.SelectedValue = (DateTime.Now.Month - 1).ToString();
            }            
            CargaPeriodo();
            ScriptManager.RegisterStartupScript(this, GetType(), "mensaje", "SessionExpireAlert(" + tiempo + ");", true);
        }
        else
            ScriptManager.RegisterStartupScript(this, GetType(), "mensaje", "myStopFunction();", true);
          
        
    }

    #region "Método Controles"

    protected void ListarDropDowList()
    {
        try
        {
            DplAnioPeriodo.Items.Clear();
            for (int i = 2013; i <= DateTime.Now.Year; i++)
            {
                DplAnioPeriodo.Items.Add(new ListItem(i.ToString(), i.ToString()));
            }
            DplAnioPeriodo.SelectedIndex = 0;

            DplMesPeriodo.Items.Clear();
            DplMesPeriodo.Items.Add(new ListItem("Enero", "1"));
            DplMesPeriodo.Items.Add(new ListItem("Febrero", "2"));
            DplMesPeriodo.Items.Add(new ListItem("Marzo", "3"));
            DplMesPeriodo.Items.Add(new ListItem("Abril", "4"));
            DplMesPeriodo.Items.Add(new ListItem("Mayo", "5"));
            DplMesPeriodo.Items.Add(new ListItem("Junio", "6"));
            DplMesPeriodo.Items.Add(new ListItem("Julio", "7"));
            DplMesPeriodo.Items.Add(new ListItem("Agosto", "8"));
            DplMesPeriodo.Items.Add(new ListItem("Setiembre", "9"));
            DplMesPeriodo.Items.Add(new ListItem("Octubre", "10"));
            DplMesPeriodo.Items.Add(new ListItem("Noviembre", "11"));
            DplMesPeriodo.Items.Add(new ListItem("Diciembre", "12"));
        }
        catch (Exception e)
        {
        }
    }

    #endregion

    protected void ChkFac001_CheckedChanged(object sender, EventArgs e)
    {
        TxtDiasFac001.Enabled = ChkFac001.Checked;
        TxtDiasFac001.BackColor = ChkFac001.Checked ? Color.White : Color.WhiteSmoke;
        TxtDiasFac001.Focus();
    }

    protected void ChkFac005_CheckedChanged(object sender, EventArgs e)
    {
        TxtDiasFac005.Enabled = ChkFac005.Checked;
        TxtDiasFac005.BackColor = ChkFac005.Checked ? Color.White : Color.WhiteSmoke;
        TxtDiasFac005.Focus();
    }

    protected void ChkFac006_CheckedChanged(object sender, EventArgs e)
    {
        TxtDiasFac006.Enabled = ChkFac006.Checked;
        TxtDiasFac006.BackColor = ChkFac006.Checked ? Color.White : Color.WhiteSmoke;
        TxtDiasFac006.Focus();
    }

    protected void ChkFac007_CheckedChanged(object sender, EventArgs e)
    {
        TxtDiasFac007.Enabled = ChkFac007.Checked;
        TxtDiasFac007.BackColor = ChkFac007.Checked ? Color.White : Color.WhiteSmoke;
        TxtDiasFac007.Focus();
    }

    protected void ChkFac008_CheckedChanged(object sender, EventArgs e)
    {
        TxtDiasFac008.Enabled = ChkFac008.Checked;
        TxtDiasFac008.BackColor = ChkFac008.Checked ? Color.White : Color.WhiteSmoke;
        TxtDiasFac008.Focus();
    }

    protected void ChkBol001_CheckedChanged(object sender, EventArgs e)
    {
        TxtDiasBol001.Enabled = ChkBol001.Checked;
        TxtDiasBol001.BackColor = ChkBol001.Checked ? Color.White : Color.WhiteSmoke;
        TxtDiasBol001.Focus();
    }

    protected void ChkNC001_CheckedChanged(object sender, EventArgs e)
    {
        TxtDiasNC001.Enabled = ChkNC001.Checked;
        TxtDiasNC001.BackColor = ChkNC001.Checked ? Color.White : Color.WhiteSmoke;
        TxtDiasNC001.Focus();
    }

    protected void ChkNC005_CheckedChanged(object sender, EventArgs e)
    {
        TxtDiasNC005.Enabled = ChkNC005.Checked;
        TxtDiasNC005.BackColor = ChkNC005.Checked ? Color.White : Color.WhiteSmoke;
        TxtDiasNC005.Focus();
    }

    protected void ChkND001_CheckedChanged(object sender, EventArgs e)
    {
        TxtDiasND001.Enabled = ChkND001.Checked;
        TxtDiasND001.BackColor = ChkND001.Checked ? Color.White : Color.WhiteSmoke;
        TxtDiasND001.Focus();
    }

    protected void ChkFE_CheckedChanged(object sender, EventArgs e)
    {
        TxtDiasFE.Enabled = ChkFE.Checked;
        TxtDiasFE.BackColor = ChkFE.Checked ? Color.White : Color.WhiteSmoke;
        TxtDiasFE.Focus();
    }


    protected void ImgBtnGrabar_Click(object sender, ImageClickEventArgs e)
    {
        BL_ControlPeriodo oBL_ControlPeriodo = new BL_ControlPeriodo();
        BE_ControlPeriodo oBE_ControlPeriodo = new BE_ControlPeriodo();

        oBE_ControlPeriodo.iIdTipoCierre = 2; //Cierre Administrativo
        oBE_ControlPeriodo.iAnioPeriodo = Convert.ToInt32(DplAnioPeriodo.SelectedValue);
        oBE_ControlPeriodo.iMesPeriodo = Convert.ToInt32(DplMesPeriodo.SelectedValue);
        oBE_ControlPeriodo.dtFecha = DateTime.Now;
        oBE_ControlPeriodo.sUsuario = GlobalEntity.Instancia.Usuario;
        oBE_ControlPeriodo.sNombrePc = GlobalEntity.Instancia.NombrePc;

        int vl_iCodigo = 1;
        String vl_sMessage = string.Empty;
        String[] oResultadoTransaccionTx;
        Boolean Indicador = false;

        if (ChkFac001.Checked)
        {
            if (vl_iCodigo > 0)
            {
                if (String.IsNullOrEmpty(TxtDiasFac001.Text))
                {
                    ScriptManager.RegisterStartupScript(UpControles, GetType(), "mensaje", String.Format(Resources.Resource.MsgJavaError, "Debe ingresar el dia"), true);
                    TxtDiasFac001.Focus();
                    return;
                }
                else
                {
                    if ((Convert.ToInt32(TxtDiasFac001.Text) < 0) || (Convert.ToInt32(TxtDiasFac001.Text) > 10))
                    {
                        ScriptManager.RegisterStartupScript(UpControles, GetType(), "mensaje", String.Format(Resources.Resource.MsgJavaError, "Debe ingresar un dia entre 1 y 10"), true);
                        TxtDiasFac001.Focus();
                        return;
                    }
                }

                oBE_ControlPeriodo.iIdTipoProceso = 5; //Factura Serie 001
                oBE_ControlPeriodo.iCantidadDias = Convert.ToInt32(TxtDiasFac001.Text);

                oResultadoTransaccionTx = oBL_ControlPeriodo.Actualizar(oBE_ControlPeriodo).Split('|');
                vl_sMessage = string.Empty;

                for (int i = 0; i < oResultadoTransaccionTx.Length; i++)
                {
                    if (i == 0)
                    {
                        vl_iCodigo = Convert.ToInt32(oResultadoTransaccionTx[i]);
                    }
                    else
                    {
                        vl_sMessage = oResultadoTransaccionTx[i];
                    }
                }

                Indicador = true;
            }
        }

        if (ChkFac005.Checked)
        {
            if (vl_iCodigo > 0)
            {
                if (String.IsNullOrEmpty(TxtDiasFac005.Text))
                {
                    ScriptManager.RegisterStartupScript(UpControles, GetType(), "mensaje", String.Format(Resources.Resource.MsgJavaError, "Debe ingresar el dia"), true);
                    TxtDiasFac005.Focus();
                    return;
                }
                else
                {
                    if ((Convert.ToInt32(TxtDiasFac005.Text) < 0) || (Convert.ToInt32(TxtDiasFac005.Text) > 10))
                    {
                        ScriptManager.RegisterStartupScript(UpControles, GetType(), "mensaje", String.Format(Resources.Resource.MsgJavaError, "Debe ingresar un dia entre 1 y 10"), true);
                        TxtDiasFac005.Focus();
                        return;
                    }
                }

                oBE_ControlPeriodo.iIdTipoProceso = 6; //Factura Serie 005
                oBE_ControlPeriodo.iCantidadDias = Convert.ToInt32(TxtDiasFac005.Text);

                oResultadoTransaccionTx = oBL_ControlPeriodo.Actualizar(oBE_ControlPeriodo).Split('|');
                vl_sMessage = string.Empty;

                for (int i = 0; i < oResultadoTransaccionTx.Length; i++)
                {
                    if (i == 0)
                    {
                        vl_iCodigo = Convert.ToInt32(oResultadoTransaccionTx[i]);
                    }
                    else
                    {
                        vl_sMessage = oResultadoTransaccionTx[i];
                    }
                }

                Indicador = true;
            }
        }

        if (ChkFac006.Checked)
        {
            if (vl_iCodigo > 0)
            {
                if (String.IsNullOrEmpty(TxtDiasFac006.Text))
                {
                    ScriptManager.RegisterStartupScript(UpControles, GetType(), "mensaje", String.Format(Resources.Resource.MsgJavaError, "Debe ingresar el dia"), true);
                    TxtDiasFac006.Focus();
                    return;
                }
                else
                {
                    if ((Convert.ToInt32(TxtDiasFac006.Text) < 0) || (Convert.ToInt32(TxtDiasFac006.Text) > 10))
                    {
                        ScriptManager.RegisterStartupScript(UpControles, GetType(), "mensaje", String.Format(Resources.Resource.MsgJavaError, "Debe ingresar un dia entre 1 y 10"), true);
                        TxtDiasFac006.Focus();
                        return;
                    }
                }

                oBE_ControlPeriodo.iIdTipoProceso = 7; //Factura Serie 006 
                oBE_ControlPeriodo.iCantidadDias = Convert.ToInt32(TxtDiasFac006.Text);

                oResultadoTransaccionTx = oBL_ControlPeriodo.Actualizar(oBE_ControlPeriodo).Split('|');
                vl_sMessage = string.Empty;

                for (int i = 0; i < oResultadoTransaccionTx.Length; i++)
                {
                    if (i == 0)
                    {
                        vl_iCodigo = Convert.ToInt32(oResultadoTransaccionTx[i]);
                    }
                    else
                    {
                        vl_sMessage = oResultadoTransaccionTx[i];
                    }
                }

                Indicador = true;
            }
        }

        if (ChkFac007.Checked)
        {
            if (vl_iCodigo > 0)
            {
                if (String.IsNullOrEmpty(TxtDiasFac007.Text))
                {
                    ScriptManager.RegisterStartupScript(UpControles, GetType(), "mensaje", String.Format(Resources.Resource.MsgJavaError, "Debe ingresar el dia"), true);
                    TxtDiasFac007.Focus();
                    return;
                }
                else
                {
                    if ((Convert.ToInt32(TxtDiasFac007.Text) < 0) || (Convert.ToInt32(TxtDiasFac007.Text) > 10))
                    {
                        ScriptManager.RegisterStartupScript(UpControles, GetType(), "mensaje", String.Format(Resources.Resource.MsgJavaError, "Debe ingresar un dia entre 1 y 10"), true);
                        TxtDiasFac007.Focus();
                        return;
                    }
                }

                oBE_ControlPeriodo.iIdTipoProceso = 8; //Factura Serie 007
                oBE_ControlPeriodo.iCantidadDias = Convert.ToInt32(TxtDiasFac007.Text);

                oResultadoTransaccionTx = oBL_ControlPeriodo.Actualizar(oBE_ControlPeriodo).Split('|');
                vl_sMessage = string.Empty;

                for (int i = 0; i < oResultadoTransaccionTx.Length; i++)
                {
                    if (i == 0)
                    {
                        vl_iCodigo = Convert.ToInt32(oResultadoTransaccionTx[i]);
                    }
                    else
                    {
                        vl_sMessage = oResultadoTransaccionTx[i];
                    }
                }

                Indicador = true;
            }
        }

        if (ChkFac008.Checked)
        {
            if (vl_iCodigo > 0)
            {
                if (String.IsNullOrEmpty(TxtDiasFac008.Text))
                {
                    ScriptManager.RegisterStartupScript(UpControles, GetType(), "mensaje", String.Format(Resources.Resource.MsgJavaError, "Debe ingresar el dia"), true);
                    TxtDiasFac008.Focus();
                    return;
                }
                else
                {
                    if ((Convert.ToInt32(TxtDiasFac008.Text) < 0) || (Convert.ToInt32(TxtDiasFac008.Text) > 10))
                    {
                        ScriptManager.RegisterStartupScript(UpControles, GetType(), "mensaje", String.Format(Resources.Resource.MsgJavaError, "Debe ingresar un dia entre 1 y 10"), true);
                        TxtDiasFac008.Focus();
                        return;
                    }
                }

                oBE_ControlPeriodo.iIdTipoProceso = 13; //Factura Serie 008
                oBE_ControlPeriodo.iCantidadDias = Convert.ToInt32(TxtDiasFac008.Text);

                oResultadoTransaccionTx = oBL_ControlPeriodo.Actualizar(oBE_ControlPeriodo).Split('|');
                vl_sMessage = string.Empty;

                for (int i = 0; i < oResultadoTransaccionTx.Length; i++)
                {
                    if (i == 0)
                    {
                        vl_iCodigo = Convert.ToInt32(oResultadoTransaccionTx[i]);
                    }
                    else
                    {
                        vl_sMessage = oResultadoTransaccionTx[i];
                    }
                }

                Indicador = true;
            }
        }

        if (ChkBol001.Checked)
        {
            if (vl_iCodigo > 0)
            {
                if (String.IsNullOrEmpty(TxtDiasBol001.Text))
                {
                    ScriptManager.RegisterStartupScript(UpControles, GetType(), "mensaje", String.Format(Resources.Resource.MsgJavaError, "Debe ingresar el dia"), true);
                    TxtDiasBol001.Focus();
                    return;
                }
                else
                {
                    if ((Convert.ToInt32(TxtDiasBol001.Text) < 0) || (Convert.ToInt32(TxtDiasBol001.Text) > 10))
                    {
                        ScriptManager.RegisterStartupScript(UpControles, GetType(), "mensaje", String.Format(Resources.Resource.MsgJavaError, "Debe ingresar un dia entre 1 y 10"), true);
                        TxtDiasBol001.Focus();
                        return;
                    }
                }

                oBE_ControlPeriodo.iIdTipoProceso = 9; //Boleta Serie 001
                oBE_ControlPeriodo.iCantidadDias = Convert.ToInt32(TxtDiasBol001.Text);

                oResultadoTransaccionTx = oBL_ControlPeriodo.Actualizar(oBE_ControlPeriodo).Split('|');
                vl_sMessage = string.Empty;

                for (int i = 0; i < oResultadoTransaccionTx.Length; i++)
                {
                    if (i == 0)
                    {
                        vl_iCodigo = Convert.ToInt32(oResultadoTransaccionTx[i]);
                    }
                    else
                    {
                        vl_sMessage = oResultadoTransaccionTx[i];
                    }
                }

                Indicador = true;
            }
        }

        if (ChkNC001.Checked)
        {
            if (vl_iCodigo > 0)
            {
                if (String.IsNullOrEmpty(TxtDiasNC001.Text))
                {
                    ScriptManager.RegisterStartupScript(UpControles, GetType(), "mensaje", String.Format(Resources.Resource.MsgJavaError, "Debe ingresar el dia"), true);
                    TxtDiasNC001.Focus();
                    return;
                }
                else
                {
                    if ((Convert.ToInt32(TxtDiasNC001.Text) < 0) || (Convert.ToInt32(TxtDiasNC001.Text) > 10))
                    {
                        ScriptManager.RegisterStartupScript(UpControles, GetType(), "mensaje", String.Format(Resources.Resource.MsgJavaError, "Debe ingresar un dia entre 1 y 10"), true);
                        TxtDiasNC001.Focus();
                        return;
                    }
                }

                oBE_ControlPeriodo.iIdTipoProceso = 10; //Nota de Credito Serie 001
                oBE_ControlPeriodo.iCantidadDias = Convert.ToInt32(TxtDiasNC001.Text);

                oResultadoTransaccionTx = oBL_ControlPeriodo.Actualizar(oBE_ControlPeriodo).Split('|');
                vl_sMessage = string.Empty;

                for (int i = 0; i < oResultadoTransaccionTx.Length; i++)
                {
                    if (i == 0)
                    {
                        vl_iCodigo = Convert.ToInt32(oResultadoTransaccionTx[i]);
                    }
                    else
                    {
                        vl_sMessage = oResultadoTransaccionTx[i];
                    }
                }

                Indicador = true;
            }
        }

        if (ChkNC005.Checked)
        {
            if (vl_iCodigo > 0)
            {
                if (String.IsNullOrEmpty(TxtDiasNC005.Text))
                {
                    ScriptManager.RegisterStartupScript(UpControles, GetType(), "mensaje", String.Format(Resources.Resource.MsgJavaError, "Debe ingresar el dia"), true);
                    TxtDiasNC005.Focus();
                    return;
                }
                else
                {
                    if ((Convert.ToInt32(TxtDiasNC005.Text) < 0) || (Convert.ToInt32(TxtDiasNC005.Text) > 10))
                    {
                        ScriptManager.RegisterStartupScript(UpControles, GetType(), "mensaje", String.Format(Resources.Resource.MsgJavaError, "Debe ingresar un dia entre 1 y 10"), true);
                        TxtDiasNC005.Focus();
                        return;
                    }
                }

                oBE_ControlPeriodo.iIdTipoProceso = 12; //Nota de Credito Serie 005
                oBE_ControlPeriodo.iCantidadDias = Convert.ToInt32(TxtDiasNC005.Text);

                oResultadoTransaccionTx = oBL_ControlPeriodo.Actualizar(oBE_ControlPeriodo).Split('|');
                vl_sMessage = string.Empty;

                for (int i = 0; i < oResultadoTransaccionTx.Length; i++)
                {
                    if (i == 0)
                    {
                        vl_iCodigo = Convert.ToInt32(oResultadoTransaccionTx[i]);
                    }
                    else
                    {
                        vl_sMessage = oResultadoTransaccionTx[i];
                    }
                }

                Indicador = true;
            }
        }

        if (ChkND001.Checked)
        {
            if (vl_iCodigo > 0)
            {
                if (String.IsNullOrEmpty(TxtDiasND001.Text))
                {
                    ScriptManager.RegisterStartupScript(UpControles, GetType(), "mensaje", String.Format(Resources.Resource.MsgJavaError, "Debe ingresar el dia"), true);
                    TxtDiasND001.Focus();
                    return;
                }
                else
                {
                    if ((Convert.ToInt32(TxtDiasND001.Text) < 0) || (Convert.ToInt32(TxtDiasND001.Text) > 10))
                    {
                        ScriptManager.RegisterStartupScript(UpControles, GetType(), "mensaje", String.Format(Resources.Resource.MsgJavaError, "Debe ingresar un dia entre 1 y 10"), true);
                        TxtDiasND001.Focus();
                        return;
                    }
                }

                oBE_ControlPeriodo.iIdTipoProceso = 11; //Nota de Debito Serie 001
                oBE_ControlPeriodo.iCantidadDias = Convert.ToInt32(TxtDiasND001.Text);

                oResultadoTransaccionTx = oBL_ControlPeriodo.Actualizar(oBE_ControlPeriodo).Split('|');
                vl_sMessage = string.Empty;

                for (int i = 0; i < oResultadoTransaccionTx.Length; i++)
                {
                    if (i == 0)
                    {
                        vl_iCodigo = Convert.ToInt32(oResultadoTransaccionTx[i]);
                    }
                    else
                    {
                        vl_sMessage = oResultadoTransaccionTx[i];
                    }
                }

                Indicador = true;
            }
        }


        if (ChkFE.Checked)
        {
            if (oBE_ControlPeriodo.iMesPeriodo == 12)
                oBE_ControlPeriodo.dtFecha = new DateTime(oBE_ControlPeriodo.iAnioPeriodo+1, 1, 1).AddDays(-1);
            else
                oBE_ControlPeriodo.dtFecha = new DateTime(oBE_ControlPeriodo.iAnioPeriodo, oBE_ControlPeriodo.iMesPeriodo+1, 1).AddDays(-1);

            if (vl_iCodigo > 0)
            {
                if (String.IsNullOrEmpty(TxtDiasFE.Text))
                {
                    ScriptManager.RegisterStartupScript(UpControles, GetType(), "mensaje", String.Format(Resources.Resource.MsgJavaError, "Debe ingresar el dia"), true);
                    TxtDiasFac001.Focus();
                    return;
                }
                else
                {
                    if ((Convert.ToInt32(TxtDiasFE.Text) < 0) || (Convert.ToInt32(TxtDiasFE.Text) > 5))
                    {
                        ScriptManager.RegisterStartupScript(UpControles, GetType(), "mensaje", String.Format(Resources.Resource.MsgJavaError, "Debe ingresar un dia entre 1 y 5"), true);
                        TxtDiasFE.Focus();
                        return;
                    }
                }

                oBE_ControlPeriodo.iIdTipoProceso = 14; //Factura Electronica
                oBE_ControlPeriodo.iCantidadDias = Convert.ToInt32(TxtDiasFE.Text);

                oResultadoTransaccionTx = oBL_ControlPeriodo.Actualizar(oBE_ControlPeriodo).Split('|');
                vl_sMessage = string.Empty;

                for (int i = 0; i < oResultadoTransaccionTx.Length; i++)
                {
                    if (i == 0)
                    {
                        vl_iCodigo = Convert.ToInt32(oResultadoTransaccionTx[i]);
                    }
                    else
                    {
                        vl_sMessage = oResultadoTransaccionTx[i];
                    }
                }

                Indicador = true;
            }
        }





        if (Indicador)
        {
            if (vl_iCodigo < 1)
            {
                ScriptManager.RegisterStartupScript(UpControles, GetType(), "mensaje", String.Format(Resources.Resource.MsgJavaError, vl_sMessage), true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(UpControles, GetType(), "mensaje", String.Format(Resources.Resource.MsgJavaExito, vl_sMessage), true);
            }
        }
        else
        {
            ScriptManager.RegisterStartupScript(UpControles, GetType(), "mensaje", String.Format(Resources.Resource.MsgJavaAlerta, "Debe seleccionar una de las opciones"), true);
        }
    }







    protected void DplAnioPeriodo_SelectedIndexChanged(object sender, EventArgs e)
    {
        CargaPeriodo();
    }

    protected void DplMesPeriodo_SelectedIndexChanged(object sender, EventArgs e)
    {
        CargaPeriodo();
    }

    private void CargaPeriodo()
    {
        IniciarControles();

        Int32 iAnioPeriodo = Convert.ToInt32(DplAnioPeriodo.SelectedValue);
        Int32 iMesPeriodo = Convert.ToInt32(DplMesPeriodo.SelectedValue);

        BL_ControlPeriodo oBL_ControlPeriodo = new BL_ControlPeriodo();
        BE_ControlPeriodo oBE_ControlPeriodo = new BE_ControlPeriodo();
        List<BE_ControlPeriodo> LstControlPeriodo = new List<BE_ControlPeriodo>();

        oBE_ControlPeriodo.iIdTipoCierre = 2; //Administrativo
        oBE_ControlPeriodo.iAnioPeriodo = iAnioPeriodo;
        oBE_ControlPeriodo.iMesPeriodo = iMesPeriodo;

        LstControlPeriodo = oBL_ControlPeriodo.Listar(oBE_ControlPeriodo);

        foreach (BE_ControlPeriodo item in LstControlPeriodo)
        {
            if (item.iIdTipoProceso == 5) //Factura Serie 001
            {
                if ((item.sEstado == "1") && (!String.IsNullOrEmpty(item.iCantidadDias.ToString())))
                {
                    ChkFac001.Checked = true;
                    TxtDiasFac001.Text = item.iCantidadDias.ToString();
                    TxtDiasFac001.BackColor = Color.White;
                    TxtDiasFac001.Enabled = true;

                    LblDiasFac001.Text = item.dtFecha.HasValue ? "Vence el " + item.dtFecha.Value.AddDays(item.iCantidadDias.Value).ToShortDateString() : String.Empty;
                }
            }

            if (item.iIdTipoProceso == 6) //Factura Serie 005
            {
                if ((item.sEstado == "1") && (!String.IsNullOrEmpty(item.iCantidadDias.ToString())))
                {
                    ChkFac005.Checked = true;
                    TxtDiasFac005.Text = item.iCantidadDias.ToString();
                    TxtDiasFac005.BackColor = Color.White;
                    TxtDiasFac005.Enabled = true;

                    LblDiasFac005.Text = item.dtFecha.HasValue ? "Vence el " + item.dtFecha.Value.AddDays(item.iCantidadDias.Value).ToShortDateString() : String.Empty;
                }
            }

            if (item.iIdTipoProceso == 7) //Factura Serie 006
            {
                if ((item.sEstado == "1") && (!String.IsNullOrEmpty(item.iCantidadDias.ToString())))
                {
                    ChkFac006.Checked = true;
                    TxtDiasFac006.Text = item.iCantidadDias.ToString();
                    TxtDiasFac006.BackColor = Color.White;
                    TxtDiasFac006.Enabled = true;

                    LblDiasFac006.Text = item.dtFecha.HasValue ? "Vence el " + item.dtFecha.Value.AddDays(item.iCantidadDias.Value).ToShortDateString() : String.Empty;
                }
            }

            if (item.iIdTipoProceso == 8) //Factura Serie 007
            {
                if ((item.sEstado == "1") && (!String.IsNullOrEmpty(item.iCantidadDias.ToString())))
                {
                    ChkFac007.Checked = true;
                    TxtDiasFac007.Text = item.iCantidadDias.ToString();
                    TxtDiasFac007.BackColor = Color.White;
                    TxtDiasFac007.Enabled = true;

                    LblDiasFac007.Text = item.dtFecha.HasValue ? "Vence el " + item.dtFecha.Value.AddDays(item.iCantidadDias.Value).ToShortDateString() : String.Empty;
                }
            }

            if (item.iIdTipoProceso == 9) //Boleta Serie 001
            {
                if ((item.sEstado == "1") && (!String.IsNullOrEmpty(item.iCantidadDias.ToString())))
                {
                    ChkBol001.Checked = true;
                    TxtDiasBol001.Text = item.iCantidadDias.ToString();
                    TxtDiasBol001.BackColor = Color.White;
                    TxtDiasBol001.Enabled = true;

                    LblDiasBol001.Text = item.dtFecha.HasValue ? "Vence el " + item.dtFecha.Value.AddDays(item.iCantidadDias.Value).ToShortDateString() : String.Empty;
                }
            }

            if (item.iIdTipoProceso == 10) //Nota de Credito Serie 001
            {
                if ((item.sEstado == "1") && (!String.IsNullOrEmpty(item.iCantidadDias.ToString())))
                {
                    ChkNC001.Checked = true;
                    TxtDiasNC001.Text = item.iCantidadDias.ToString();
                    TxtDiasNC001.BackColor = Color.White;
                    TxtDiasNC001.Enabled = true;

                    LblDiasNC001.Text = item.dtFecha.HasValue ? "Vence el " + item.dtFecha.Value.AddDays(item.iCantidadDias.Value).ToShortDateString() : String.Empty;
                }
            }

            if (item.iIdTipoProceso == 11) //Nota de Debito Serie 001
            {
                if ((item.sEstado == "1") && (!String.IsNullOrEmpty(item.iCantidadDias.ToString())))
                {
                    ChkND001.Checked = true;
                    TxtDiasND001.Text = item.iCantidadDias.ToString();
                    TxtDiasND001.BackColor = Color.White;
                    TxtDiasND001.Enabled = true;

                    LblDiasND001.Text = item.dtFecha.HasValue ? "Vence el " + item.dtFecha.Value.AddDays(item.iCantidadDias.Value).ToShortDateString() : String.Empty;
                }
            }

            if (item.iIdTipoProceso == 12) //Nota de Credito Serie 005
            {
                if ((item.sEstado == "1") && (!String.IsNullOrEmpty(item.iCantidadDias.ToString())))
                {
                    ChkNC005.Checked = true;
                    TxtDiasNC005.Text = item.iCantidadDias.ToString();
                    TxtDiasNC005.BackColor = Color.White;
                    TxtDiasNC005.Enabled = true;

                    LblDiasNC005.Text = item.dtFecha.HasValue ? "Vence el " + item.dtFecha.Value.AddDays(item.iCantidadDias.Value).ToShortDateString() : String.Empty;
                }
            }

            if (item.iIdTipoProceso == 13) //Factura Serie 008
            {
                if ((item.sEstado == "1") && (!String.IsNullOrEmpty(item.iCantidadDias.ToString())))
                {
                    ChkFac008.Checked = true;
                    TxtDiasFac008.Text = item.iCantidadDias.ToString();
                    TxtDiasFac008.BackColor = Color.White;
                    TxtDiasFac008.Enabled = true;

                    LblDiasFac008.Text = item.dtFecha.HasValue ? "Vence el " + item.dtFecha.Value.AddDays(item.iCantidadDias.Value).ToShortDateString() : String.Empty;
                }
            }

            if (item.iIdTipoProceso == 14) //Factura Electronica
            {
                if ((item.sEstado == "1") && (!String.IsNullOrEmpty(item.iCantidadDias.ToString())))
                {
                    ChkFE.Checked = true;
                    TxtDiasFE.Text = item.iCantidadDias.ToString();
                    TxtDiasFE.BackColor = Color.White;
                    TxtDiasFE.Enabled = true;

                    LblDiasFE.Text = item.dtFecha.HasValue ? "Vence el " + item.dtFecha.Value.AddDays(item.iCantidadDias.Value).ToShortDateString() : String.Empty;
                }
            }

        }
    }

    protected void IniciarControles()
    {
        ChkFac001.Checked = false;
        ChkFac005.Checked = false;
        ChkFac006.Checked = false;
        ChkFac007.Checked = false;
        ChkFac008.Checked = false;
        ChkBol001.Checked = false;
        ChkNC001.Checked = false;
        ChkNC005.Checked = false;
        ChkND001.Checked = false;
        ChkFE.Checked = false;

        TxtDiasFac001.Text = String.Empty;
        TxtDiasFac005.Text = String.Empty;
        TxtDiasFac006.Text = String.Empty;
        TxtDiasFac007.Text = String.Empty;
        TxtDiasFac008.Text = String.Empty;
        TxtDiasBol001.Text = String.Empty;
        TxtDiasNC001.Text = String.Empty;
        TxtDiasNC005.Text = String.Empty;
        TxtDiasND001.Text = String.Empty;
        TxtDiasFE.Text = String.Empty;

        TxtDiasFac001.BackColor = Color.WhiteSmoke;
        TxtDiasFac005.BackColor = Color.WhiteSmoke;
        TxtDiasFac006.BackColor = Color.WhiteSmoke;
        TxtDiasFac007.BackColor = Color.WhiteSmoke;
        TxtDiasFac008.BackColor = Color.WhiteSmoke;
        TxtDiasBol001.BackColor = Color.WhiteSmoke;
        TxtDiasNC001.BackColor = Color.WhiteSmoke;        
        TxtDiasNC005.BackColor = Color.WhiteSmoke;
        TxtDiasND001.BackColor = Color.WhiteSmoke;
        TxtDiasFE.BackColor = Color.WhiteSmoke;

        TxtDiasFac001.Enabled = false;
        TxtDiasFac005.Enabled = false;
        TxtDiasFac006.Enabled = false;
        TxtDiasFac007.Enabled = false;
        TxtDiasFac008.Enabled = false;
        TxtDiasBol001.Enabled = false;
        TxtDiasNC001.Enabled = false;
        TxtDiasNC005.Enabled = false;
        TxtDiasND001.Enabled = false;
        TxtDiasFE.Enabled = false;

        LblDiasFac001.Text = String.Empty;
        LblDiasFac005.Text = String.Empty;
        LblDiasFac006.Text = String.Empty;
        LblDiasFac007.Text = String.Empty;
        LblDiasFac008.Text = String.Empty;
        LblDiasBol001.Text = String.Empty;
        LblDiasNC001.Text = String.Empty;
        LblDiasNC005.Text = String.Empty;
        LblDiasND001.Text = String.Empty;
        LblDiasFE.Text = String.Empty;
    }

    protected void BtnCerrarPeriodo_Click(object sender, EventArgs e)
    {
        BL_ControlPeriodo oBL_ControlPeriodo = new BL_ControlPeriodo();
        BE_ControlPeriodo oBE_ControlPeriodo = new BE_ControlPeriodo();

        oBE_ControlPeriodo.iIdTipoCierre = 2; //Administrativo
        oBE_ControlPeriodo.iAnioPeriodo = Convert.ToInt32(DplAnioPeriodo.SelectedValue);
        oBE_ControlPeriodo.iMesPeriodo = Convert.ToInt32(DplMesPeriodo.SelectedValue);

        oBE_ControlPeriodo.sUsuario = GlobalEntity.Instancia.Usuario;
        oBE_ControlPeriodo.sNombrePc = GlobalEntity.Instancia.NombrePc;

        String vl_sMessage = string.Empty;
        String[] oResultadoTransaccionTx;
        int vl_iCodigo = 0;

        oResultadoTransaccionTx = oBL_ControlPeriodo.CerrarPeriodo(oBE_ControlPeriodo).Split('|');
        vl_sMessage = string.Empty;

        for (int i = 0; i < oResultadoTransaccionTx.Length; i++)
        {
            if (i == 0)
            {
                vl_iCodigo = Convert.ToInt32(oResultadoTransaccionTx[i]);
            }
            else
            {
                vl_sMessage = oResultadoTransaccionTx[i];
            }
        }

        if (vl_iCodigo < 1)
        {
            ScriptManager.RegisterStartupScript(UpControles, GetType(), "mensaje", String.Format(Resources.Resource.MsgJavaError, vl_sMessage), true);
        }
        else
        {
            ScriptManager.RegisterStartupScript(UpControles, GetType(), "mensaje", String.Format(Resources.Resource.MsgJavaExito, vl_sMessage), true);
            IniciarControles();
        }
    }

    protected void BtnVerHistorial_Click(object sender, EventArgs e)
    {
        BL_ControlPeriodo oBL_ControlPeriodo = new BL_ControlPeriodo();
        BE_ControlPeriodo oBE_ControlPeriodo = new BE_ControlPeriodo();

        oBE_ControlPeriodo.iAnioPeriodo = Convert.ToInt32(DplAnioPeriodo.SelectedValue);
        oBE_ControlPeriodo.iMesPeriodo = Convert.ToInt32(DplMesPeriodo.SelectedValue);
        oBE_ControlPeriodo.iIdTipoCierre = 2;
        GrvDetalle.DataSource = oBL_ControlPeriodo.ListarLog(oBE_ControlPeriodo);
        GrvDetalle.DataBind();

        if (GrvDetalle.Rows.Count == 0)
        {
            List<BE_ControlPeriodo> LstControlPeriodo = new List<BE_ControlPeriodo>();
            LstControlPeriodo.Add(oBE_ControlPeriodo);
            GrvDetalle.EmptyDataText = "La vista detallada no está disponible";
            GrvDetalle.DataSource = LstControlPeriodo;
            GrvDetalle.DataBind();
        }

        LblPeriodo.Text = DplAnioPeriodo.SelectedValue + " - " + DplMesPeriodo.SelectedItem.Text;

        ScriptManager.RegisterStartupScript(UpdatePanel2, GetType(), "AbrirPopup", String.Format("javascript: OpenPopub();"), true);
    }



    protected void btn_cerrar_Click(object sender, EventArgs e)
    {
        if (GlobalEntity.Instancia.CerrarExtranet == "S")
        {
            BL_Usuario oBL_Usuario = new BL_Usuario();
            oBL_Usuario.GrabaInicioSesion(GlobalEntity.Instancia.Usuario, "0");
            Session.Abandon();
            FormsAuthentication.SignOut();
            Response.Redirect("../../eFENIX_Login.aspx");
            Response.End();
        }
    }
}
