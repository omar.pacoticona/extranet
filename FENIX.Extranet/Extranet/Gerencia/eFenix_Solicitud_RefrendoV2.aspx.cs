﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using FENIX.BusinessLogic;
using FENIX.BusinessEntity;
using System.Web.UI.WebControls;
using System.Web.Security;
using System.Configuration;
using System.IO;

public partial class Extranet_Gerencia_eFenix_Solicitud_RefrendoV2 : System.Web.UI.Page
{
   
        public Int32 IdAgenAduana;
        protected void Page_Load(object sender, EventArgs e)
        {
            String tiempo = (Convert.ToInt32(GlobalEntity.Instancia.TiempoDuracionSesion.ToString()) * 1000 * 60).ToString();
            Session["Reset"] = true;

            ScriptManager scriptManager = ScriptManager.GetCurrent(this.Page);
            scriptManager.RegisterPostBackControl(this.imgBtnUplDua);

            scriptManager.RegisterPostBackControl(this.imgBtnUplBoking);
            scriptManager.RegisterPostBackControl(this.imgBtnUplTicekt);
            scriptManager.RegisterPostBackControl(this.imgBtnUplPackList);
            scriptManager.RegisterPostBackControl(this.imgBtnUplGuiaRem);

            if (!Page.IsPostBack)
            {
                IdAgenAduana = GlobalEntity.Instancia.IdCliente;
                listarDroplist();
            TabContainer1.ActiveTabIndex = 0;
            //tbPanelBusqueda.Enabled = false;
            //tbPanelAdj.Enabled = false;
            //TabContainer1.ActiveTabIndex = 0;

            // listarDroplist();


            //oBE_DocumentoOrigen.Add(oBE_DocOrigen);
            //grvListadoSolicitud.DataSource = oBE_DocumentoOrigen;
            //grvListadoSolicitud.DataBind();

            //ViewState["usuario"] = GlobalEntity.Instancia.Usuario;

            ScriptManager.RegisterStartupScript(this, GetType(), "mensaje", "SessionExpireAlert(" + tiempo + ");", true);
            }
            else
                ScriptManager.RegisterStartupScript(this, GetType(), "mensaje", "myStopFunction();", true);
        }

        private void SCA_MsgInformacion(string strError)
        {
            MensajeScript(strError);
        }

        private void MensajeScript(string SMS)
        {
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "", "<script>alert('" + SMS + "');</script>", false);


        }

    protected void TabContainer1_ActiveTabChanged(object sender, EventArgs e)
    {

    }

    protected void btn_cerrar_Click(object sender, EventArgs e)
        {
            if (GlobalEntity.Instancia.CerrarExtranet == "S")
            {
                BL_Usuario oBL_Usuario = new BL_Usuario();
                oBL_Usuario.GrabaInicioSesion(GlobalEntity.Instancia.Usuario, "0");
                Session.Abandon();
                FormsAuthentication.SignOut();
                Response.Redirect("../../eFENIX_Login.aspx");
                Response.End();
            }
        }

        Tuple<string, int> cargarArchivo(FileUpload fileUpload, string IdAgenAduana)
        {
            string ruta = ConfigurationManager.AppSettings["Refrendo_Solicitud"] + IdAgenAduana + "/";

            string filename = "";
            int estado = 0;
            string fecha = DateTime.Now.ToString("ddMMyyHmmss");
            try
            {
                if (fileUpload.HasFile)
                {
                    if (fileUpload.PostedFile.ContentType == "application/pdf")
                    {
                        //COMPROBAR SI EXISTE RUTA 
                        if (!File.Exists(ruta))
                        {
                            //SI NO EXISTE CREAR LA RUTA
                            DirectoryInfo directory = Directory.CreateDirectory(ruta);
                        }

                        //GUARDA INFORMACION
                        filename = Path.GetFileName(fecha + "_" + fileUpload.FileName);
                        fileUpload.SaveAs(ruta + filename);
                        estado = 1;
                    }
                    else
                    {
                        estado = 2;
                    }
                }

            }
            catch (Exception ex)
            {
                //MensajeScript("eror de upload");
                estado = 0;
            }

            return Tuple.Create(filename, estado);
        }

        protected void imgBtnUplDua_Click(object sender, ImageClickEventArgs e)

        {
            //ScriptManager scriptManager = ScriptManager.GetCurrent(this.Page);
            //scriptManager.RegisterPostBackControl(this.imgBtnUplDua);
            BE_Refrendo oBE_Refrendo = new BE_Refrendo();
            BL_Refrendo oBL_Refrendo = new BL_Refrendo();

            var result = cargarArchivo(FileUploadDua, IdAgenAduana.ToString());

            if (result.Item2 == 1)
            {
                lblUplDua.Text = FileUploadDua.FileName;
                // UploadIconoEnabled(1, 2);
                oBE_Refrendo.IdCliente = IdAgenAduana;
                oBE_Refrendo.sNombreArchivo = FileUploadDua.FileName;
                oBE_Refrendo.sRutaArchivo = result.Item1;
                oBE_Refrendo.iIdusuario = 0;
                oBE_Refrendo.sUsuario = GlobalEntity.Instancia.Usuario;
                oBE_Refrendo.iIdTipoDocumento = 1; //DUA
                var res = oBL_Refrendo.Extranet_Ins_ArchivosRefrendo(oBE_Refrendo);

                lblUplBLID.Text = res.Item2;

            }
            else if (result.Item2 == 2)
            {
                MensajeScript("Ingrese un archivo en Formato PDF");
            }
        }

        protected void ImgBtnBuscar_Click(object sender, EventArgs e)
        {
            BE_Refrendo oBE_Refrendo = new BE_Refrendo();
            BL_Refrendo oBL_Refrendo = new BL_Refrendo();
            List<BE_Refrendo> List_Refrendo = new List<BE_Refrendo>();
            oBE_Refrendo.sBooking = txtBooking.Text;

            List_Refrendo = oBL_Refrendo.ListarDatosLiquidacion(oBE_Refrendo);

            txtIdExportador.Text = List_Refrendo[0].IdExportador.ToString();
            txtExportador.Text = List_Refrendo[0].sExportador.ToString();
            txtIdAgenciaAduanas.Text = List_Refrendo[0].IdAgenciaAguanas.ToString();
            txtAgeniaAduanas.Text = List_Refrendo[0].sAgenciaAduanas.ToString();
            txtNave.Text = List_Refrendo[0].Nave.ToString();
            txtIdNave.Text = List_Refrendo[0].IdNave.ToString();
            txtViaje.Text = List_Refrendo[0].sViaje.ToString();
            txtRumbo.Text = List_Refrendo[0].sRumbo.ToString();


        }

        protected void BtnGrabar_Click(object sender, EventArgs e)
        {
            SCA_MsgInformacion(txtExportador.Text.ToString());
        }

        void listarDroplist()
        {

            BL_DocumentoPago oBL_DocumentoPago = new BL_DocumentoPago();
            List<BE_DocumentoPago> oBE_DocumentoPago = new List<BE_DocumentoPago>();
            List<BE_Tabla> olst_TablaTipo = new List<BE_Tabla>();

            int i = 0;

            olst_TablaTipo = null;
            i = 0;
            DplBanco.Items.Clear();
            olst_TablaTipo = oBL_DocumentoPago.ListarBanco(0);
            DplBanco.Items.Add(new ListItem("[Seleccionar]", "-1"));
            for (i = 0; i < olst_TablaTipo.Count; ++i)
            {
                DplBanco.Items.Add(new ListItem(olst_TablaTipo[i].sBanco_Descripcion, olst_TablaTipo[i].sIdBanco.ToString()));
            }
            DplBanco.SelectedIndex = -1;



            olst_TablaTipo = null;
            i = 0;
            DplFormaPago.Items.Clear();
            olst_TablaTipo = oBL_DocumentoPago.ListarFormaDePago();
            DplFormaPago.Items.Add(new ListItem("[Seleccionar]", "-1"));
            for (i = 0; i < olst_TablaTipo.Count; ++i)
            {
                DplFormaPago.Items.Add(new ListItem(olst_TablaTipo[i].sDescripcion, olst_TablaTipo[i].iIdValor.ToString()));
            }
            DplFormaPago.SelectedIndex = -1;



            olst_TablaTipo = null;
            i = 0;
            DplTipoPago.Items.Clear();
            olst_TablaTipo = oBL_DocumentoPago.ListarTipoPago();
            DplTipoPago.Items.Add(new ListItem("[Seleccionar]", "-1"));
            for (i = 0; i < olst_TablaTipo.Count; ++i)
            {
                if (olst_TablaTipo[i].iIdValor != 3)
                {
                    DplTipoPago.Items.Add(new ListItem(olst_TablaTipo[i].sDescripcion, olst_TablaTipo[i].iIdValor.ToString()));
                }

            }
            DplTipoPago.SelectedIndex = -1;


        }
    
}