﻿<%@ Page Language="C#" MasterPageFile="~/Extranet/Seguridad/eFENIX_Principal.master"
    AutoEventWireup="true" CodeFile="eFENIX_IndicadorComparativoAnual.aspx.cs" Inherits="Extranet_Gerencia_eFENIX_IndicadorComparativoAnual"
    Title="Indicador Comparativo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CntMaster" runat="Server">
    <table cellspacing="0" cellpadding="0" style="width: 100%;">
        <tr>

            <td class="form_titulo" style="width: 85%">
                INDICADOR COMPARATIVO
            </td>
            <td align="right" class="form_titulo_verde" style="width: 15%">
                <asp:TextBox ID="LblTotal" Text="" style="background-color: #0069ae; color: #FFFFFF" ReadOnly="true" BorderColor="#0069ae" runat="server"></asp:TextBox>
            </td>
        </tr>
        
        <tr>
            <td valign="top" colspan="2">
                <table class="form_bandeja" style="margin-top: 5px; margin-bottom: 5px; width: 100%;">
                    <tr>
                        <td colspan="7">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 6%;">
                            Periodo
                        </td>
                        <td style="width: 6%;">
                            <asp:DropDownList ID="DplAnioPeriodo" runat="server" AutoPostBack="True" OnSelectedIndexChanged="DplAnioPeriodo_SelectedIndexChanged">
                            </asp:DropDownList>
                        </td>
                        <td style="width: 16%;">
                            <asp:DropDownList ID="DplMesPeriodo" runat="server" Width="100px" AutoPostBack="True" OnSelectedIndexChanged="DplMesPeriodo_SelectedIndexChanged">
                            </asp:DropDownList>
                        </td>
                        <td style="width: 11%;">
                            Línea de Negocio
                        </td>
                        <td style="width: 35%;">
                            <asp:DropDownList ID="DplNegocio" runat="server" Width="320px" AutoPostBack="True" OnSelectedIndexChanged="DplNegocio_SelectedIndexChanged"></asp:DropDownList>
                        </td>
                        <td style="width: 10%;">
                            <asp:ImageButton ID="ImgBtnBuscar" runat="server" ImageUrl="~/Imagenes/Formulario/btn_consultar_1.JPG"
                                OnClick="ImgBtnBuscar_Click" />
                        </td>
                        <td style="width: 14%;">
                            
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 6%;">
                            Mostrar
                        </td>
                        <td style="width: 6%;" colspan="6">
                            <asp:DropDownList ID="DplDatoMostrar" runat="server" Width="170px" AutoPostBack="True" OnSelectedIndexChanged="DplDatoMostrar_SelectedIndexChanged">
                            </asp:DropDownList>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        
        <tr valign="top" style="height: 100%">
            <td colspan="5">
                <ajax:UpdatePanel ID="upDocumentoOrigen" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div style="overflow: auto; width: 100%; height: 100%">
                            <asp:GridView ID="GrvListado" runat="server" AutoGenerateColumns="False" DataKeyNames="iIdIndicador,sNombreIndicador,sPropietario,sObjetivo,sColor,sColor2,sColor3,sColor4,sColor5,sColor6,sColor7,sTooltip,
                                        sMes1,sMes2,sMes3,sMes4,sMes5,sMes6,sMes7,sMes8,sMes9,sMes10,sMes11,sMes12,sMes13,
                                        sObjetivoMes1,sObjetivoMes2,sObjetivoMes3,sObjetivoMes4,sObjetivoMes5,sObjetivoMes6,sObjetivoMes7,sObjetivoMes8,sObjetivoMes9,sObjetivoMes10,sObjetivoMes11,sObjetivoMes12,sObjetivoMes13,
                                        sObjetivo1,sObjetivo2,sObjetivo3,sObjetivo4,sObjetivo5,sObjetivo6,sObjetivo7,sObjetivo8,sObjetivo9,sObjetivo10,sObjetivo11,sObjetivo12,sObjetivo13"
                                SkinID="GrillaConsultaUnColor" Width="100%" OnRowDataBound="GrvListado_RowDataBound">
                                <Columns>
                                    <asp:TemplateField> 
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkSeleccionar" runat="server"/>
                                        </ItemTemplate>
                                        <HeaderStyle BackColor="#0069ae" Width="3%" />
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="sNombreIndicador" HeaderText="Indicador" ItemStyle-HorizontalAlign="Left">
                                        <ItemStyle Width="20%" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="sObjetivo" HeaderText="Objetivo" ItemStyle-HorizontalAlign="Right">
                                        <ItemStyle Width="5%" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="sMes1" HeaderText="Mes 1" ItemStyle-HorizontalAlign="Right">
                                        <ItemStyle Width="5%" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="sMes2" HeaderText="Mes 2" ItemStyle-HorizontalAlign="Right">
                                        <ItemStyle Width="5%" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="sMes3" HeaderText="Mes 3" ItemStyle-HorizontalAlign="Right">
                                        <ItemStyle Width="5%" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="sMes4" HeaderText="Mes 4" ItemStyle-HorizontalAlign="Right">
                                        <ItemStyle Width="5%" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="sMes5" HeaderText="Mes 5" ItemStyle-HorizontalAlign="Right">
                                        <ItemStyle Width="5%" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="sMes6" HeaderText="Mes 6" ItemStyle-HorizontalAlign="Right">
                                        <ItemStyle Width="5%" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="sMes7" HeaderText="Mes 7" ItemStyle-HorizontalAlign="Right">
                                        <ItemStyle Width="5%" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="sMes8" HeaderText="Mes 8" ItemStyle-HorizontalAlign="Right">
                                        <ItemStyle Width="5%" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="sMes9" HeaderText="Mes 9" ItemStyle-HorizontalAlign="Right">
                                        <ItemStyle Width="5%" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="sMes10" HeaderText="Mes 10" ItemStyle-HorizontalAlign="Right">
                                        <ItemStyle Width="5%" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="sMes11" HeaderText="Mes 11" ItemStyle-HorizontalAlign="Right">
                                        <ItemStyle Width="5%" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="sMes12" HeaderText="Mes 12" ItemStyle-HorizontalAlign="Right">
                                        <ItemStyle Width="5%" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="sMes13" HeaderText="Mes 13" ItemStyle-HorizontalAlign="Right">
                                        <ItemStyle Width="5%" />
                                    </asp:BoundField>
                                </Columns>
                            </asp:GridView>
                        </div>
                    </ContentTemplate>
                    <Triggers>
                        <ajax:AsyncPostBackTrigger ControlID="GrvListado" EventName="SelectedIndexChanged" />
                        <ajax:AsyncPostBackTrigger ControlID="DplNegocio" EventName="SelectedIndexChanged" />
                        <ajax:AsyncPostBackTrigger ControlID="DplAnioPeriodo" EventName="SelectedIndexChanged" />
                        <ajax:AsyncPostBackTrigger ControlID="DplMesPeriodo" EventName="SelectedIndexChanged" />
                        <ajax:AsyncPostBackTrigger ControlID="ImgBtnBuscar" EventName="Click" />
                        <ajax:AsyncPostBackTrigger ControlID="dnvListado" EventName="PageChanged" />
                        <ajax:AsyncPostBackTrigger ControlID="dnvListado" EventName="NextPage" />
                        <ajax:AsyncPostBackTrigger ControlID="dnvListado" EventName="PreviousPage" />
                        <ajax:AsyncPostBackTrigger ControlID="dnvListado" EventName="FirstPage" />  
                        <ajax:AsyncPostBackTrigger ControlID="dnvListado" EventName="LastPage" />     
                    </Triggers>
                </ajax:UpdatePanel>
            </td>
        </tr>
        
        <tr valign="top" style="height: 2%;">
            <td colspan="5">
               <ajax:UpdatePanel ID="upDnvListado" runat="server">
                    <ContentTemplate>
                        <FENIX:DataNavigator ID="dnvListado" runat="server" AllowCustomPaging="True" ButtonText="Ir" 
                            ForeColor="White" Font-Size="11px" BackColor="#0069ae" 
                            CurrentPage="1" CustomClientFunction="" EnableAskingAfterPage="False" EnableCustomScript="False"
                            GridViewId="GrvListado" ImagesPath="~/Imagenes/DN/" IsParentShowWindow="False"
                            MessageAskingAfterPage="" PageIndicatorFormat="Página {0} / {1}" Visible="False"
                            Width="100%" WidthButtonIr="" WidthTextBoxNumPagina="25px" />
                    </ContentTemplate>
                    <Triggers>
                        <ajax:AsyncPostBackTrigger ControlID="dnvListado" EventName="PageChanged" />
                        <ajax:AsyncPostBackTrigger ControlID="ImgBtnBuscar" EventName="Click" />
                    </Triggers>
                </ajax:UpdatePanel>
            </td>
        </tr> 
    </table>


    <%-- Popup - Cambio de Contraseña--%>
    <asp:Panel ID="__PopPanel__" runat="server" CssClass="modalBackground" Style="left: 0px;
        top: 0px; display: none; position: fixed; z-index: 10000; display: none;" />

    <asp:Panel ID="pnlDetalle" runat="server" Style="display: none; width: 800px; height: 390px;
        position: fixed; z-index: 100002; border: dimgray 1px solid; background-color: White;" DefaultButton="btnCancelarVerDetalle">
        <ajax:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <table border="0" cellpadding="0" cellspacing="0" width="100%" style="padding: 5px;">
                    <tr valign="top" class="form_titulo" style="background-color: #0069ae; height: 15px; text-transform: none;">
                        <td style="padding: 5px;">
                            Detalle del Indicador
                        </td>
                    </tr>
                    <tr>
                        <td valign="top" colspan="2">
                            <table class="form_bandeja" style="width: 100%;">
                                <tr>
                                    <td style="width: 10%;">
                                        Indicador:
                                    </td>
                                    <td style="width: 90%;" colspan="3">
                                        &nbsp;&nbsp;&nbsp;<asp:Label runat="server" ID="LblIndicador" Text=""></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Periodo:
                                    </td>
                                    <td style="width: 40%;">
                                        &nbsp;&nbsp;&nbsp;<asp:Label runat="server" ID="LblPeriodo" Text=""></asp:Label>
                                    </td>
                                    <td style="width: 10%; text-align: right;">
                                        Total:
                                    </td>
                                    <td style="width: 40%;">
                                        &nbsp;&nbsp;&nbsp;<asp:Label runat="server" ID="LblTotalGerenal" Text=""></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr style="height: 255px;">
                        <td>
                            <div style="overflow: auto; width: 100%; height: 100%">
                                <asp:GridView ID="GrvDetalle" runat="server" AutoGenerateColumns="False" DataKeyNames="sNombreIndicador"
                                    SkinID="GrillaConsultaUnColor" Width="98%" >
                                    <Columns>
                                        <asp:TemplateField> 
                                            <ItemTemplate>
                                                <asp:CheckBox ID="chkSeleccionarDet" runat="server"/>
                                            </ItemTemplate>
                                            <HeaderStyle BackColor="#0069ae" Width="2%" />
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="sNombreIndicador" HeaderText="Descripcion" HeaderStyle-HorizontalAlign="Left" >
                                            <ItemStyle Width="40%" HorizontalAlign="Left" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="sSemana1" HeaderText="Sem 1" ItemStyle-HorizontalAlign="Right">
                                            <ItemStyle Width="7%" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="sSemana2" HeaderText="Sem 2" ItemStyle-HorizontalAlign="Right">
                                            <ItemStyle Width="7%" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="sSemana3" HeaderText="Sem 3" ItemStyle-HorizontalAlign="Right">
                                            <ItemStyle Width="7%" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="sSemana4" HeaderText="Sem 4" ItemStyle-HorizontalAlign="Right">
                                            <ItemStyle Width="7%" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="sSemana5" HeaderText="Sem 5" ItemStyle-HorizontalAlign="Right">
                                            <ItemStyle Width="7%" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="sTotalItem" HeaderText="Sub Total" ItemStyle-HorizontalAlign="Right">
                                            <ItemStyle Width="13%" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="sPorcentaje" HeaderText="%" ItemStyle-HorizontalAlign="Right">
                                            <ItemStyle Width="12%" />
                                        </asp:BoundField>
                                    </Columns>
                                </asp:GridView>
                            </div>
                        </td>
                    </tr>
                    <tr style="height: 15px;">
                        <td valign="bottom" colspan="2">
                            <table style="width: 100%;">
                                <tr>
                                    <td style="text-align: right;">
                                        <asp:Button ID="btnCancelarVerDetalle" runat="server" CssClass="botonAceptar"
                                            Style="border-width: 0px;" OnClientClick="javascript: return fc_CerrarVerDetalle();" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </ContentTemplate>
            <Triggers>
                <ajax:AsyncPostBackTrigger ControlID="GrvListado" EventName="RowCommand" />
            </Triggers>
        </ajax:UpdatePanel>
    </asp:Panel>
    

    <script language="javascript" type="text/javascript">

        var tiempo = 0;
        var seconds = 0;

        var myVar = setInterval(function () {
            clearInterval(myVar);
        }, 1000);

        var myTiempo = setTimeout(function () {
            clearInterval(myTiempo);
        }, 1);

        function myStopFunction() {
            clearInterval(myVar);
            seconds = '<%=GlobalEntity.Instancia.TiempoDuracionSesion%>' * 60;
            tiempo = '<%=GlobalEntity.Instancia.TiempoDuracionSesion%>' * 1000 * 60;

            CuentaTiempo(tiempo);
        }

        function CuentaTiempo(tiempo) {
            clearTimeout(myTiempo);
            myTiempo = setTimeout(function () {
                document.getElementById('<%=btn_cerrar.ClientID%>').click();
                //window.location = "../Seguridad/eFENIX_Login.aspx";
            }, tiempo);
        }



        function SessionExpireAlert(timeout) {
            seconds = timeout / 1000;
            document.getElementsByName("seconds").innerHTML = seconds;

            myvar = setInterval(function () {
                seconds--;
                document.getElementById("seconds").innerHTML = seconds;
            }, 1000);

            CuentaTiempo(timeout);
        };

        var goodexit = false;
        window.onbeforeunload = confirmRegisterExit;
        function confirmRegisterExit(evt) {
            if (!goodexit) {
                //return "Si vas abandonar el sistema haz click en Cerrar Sesion!!";
                document.getElementById('<%=btn_cerrar.ClientID%>').click();
            }
        }

        var objFilaAnt = null;
        var backgroundColorFilaAnt = "";

        function fc_SeleccionaFilaSimple(objFila, objrowIndex, chkID) {
            try {

                if (objFilaAnt != null) {
                    objFilaAnt.style.backgroundColor = backgroundColorFilaAnt;
                }
                objFilaAnt = objFila;
                backgroundColorFilaAnt = objFila.style.backgroundColor;
                objFila.style.backgroundColor = "#c4e4ff";
            }

            catch (e) {
                error = e.message;
            }
        }

        function HabilitarUno(chk) {
            f = document.forms[0].elements;
            for (x = 0; x < f.length; x++) {
                obj = f[x];
                if (obj.type == 'checkbox') {
                    obj.checked = false;
                }
            }
            chk.enabled;
            chk.checked = true;
        }
        
        function OpenPopub() {
            Mostrar('block');
        }
        
        function Mostrar(estilo) {
            document.getElementById('<%=pnlDetalle.ClientID%>').style.left = (screen.width - 550) / 2 + 'px';
            document.getElementById('<%=pnlDetalle.ClientID%>').style.top = (screen.height - 500) / 2 + 'px';

            document.getElementById('<%=__PopPanel__.ClientID%>').style.width = screen.width + '0px';
            document.getElementById('<%=__PopPanel__.ClientID%>').style.height = screen.height + '0px';

            document.getElementById('<%=__PopPanel__.ClientID%>').style.display = estilo;
            document.getElementById('<%=pnlDetalle.ClientID%>').style.display = estilo;
        }
        
        function fc_CerrarVerDetalle() {
            Mostrar('none');
            return false;
        }
    </script>
</asp:Content>
<asp:Content ID="Content3" runat="server" contentplaceholderid="ContentCab">
    <span class="texto_titulo" >Tu Sesion Expirara en &nbsp; 
        <span class="texto_titulo" id="seconds"> </span>
        <span>&nbsp;seg.</span>
     </span>
     <asp:Button ID="btn_cerrar" runat="server" Text="cerrar" 
        onclick="btn_cerrar_Click"  Style="display: none;" />
</asp:Content>

