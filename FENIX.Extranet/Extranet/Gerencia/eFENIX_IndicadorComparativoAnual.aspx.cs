﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using QNET.Web.UI.Controls;
using FENIX.BusinessEntity;
using FENIX.BusinessLogic;
using FENIX.Common;
using System.Collections.Generic;
using System.Drawing;


public partial class Extranet_Gerencia_eFENIX_IndicadorComparativoAnual : System.Web.UI.Page
{
    #region "Evento Pagina"
        protected void Page_Load(object sender, EventArgs e)
        {
            dnvListado.BindGridView += new QNET.Web.UI.Controls.DataNavigator.BindGridViewDelegate(BindGridLista);

            String tiempo = (Convert.ToInt32(GlobalEntity.Instancia.TiempoDuracionSesion.ToString()) * 1000 * 60).ToString();
            Session["Reset"] = true;


            if (!Page.IsPostBack)
            {
                ListarDropDowList();

                if (DateTime.Now.Month == 1)
                {
                    DplAnioPeriodo.SelectedValue = (DateTime.Now.Year - 1).ToString();
                    DplMesPeriodo.SelectedValue = "12";
                }
                else
                {
                    DplAnioPeriodo.SelectedValue = DateTime.Now.Year.ToString();
                    DplMesPeriodo.SelectedValue = (DateTime.Now.Month - 1).ToString();
                }

                //Control ocultar y mostrar columnas del detalle
                int iMes = Convert.ToInt32(DplMesPeriodo.SelectedValue);
                String sAnio = Convert.ToString(Convert.ToInt32(DplAnioPeriodo.SelectedValue) - 1).Substring(2, 2);

                iMes = iMes - 1;
                for (int iCol = 14; iCol >= 2; iCol--)
                {
                    if (iCol > 8)
                    {
                        GrvListado.Columns[iCol + 1].Visible = false;
                        iMes = iMes + 1;
                    }
                    else
                    {
                        GrvListado.Columns[iCol + 1].Visible = true;
                        if (sAnio == "13") { GrvListado.Columns[iCol + 1].Visible = false; }
                        iMes = iMes + 1;
                        GrvListado.Columns[iCol + 1].HeaderText = NombreMes(iMes) + " " + sAnio;
                        if (iMes > 12)
                        {
                            sAnio = Convert.ToString(Convert.ToInt32(DplAnioPeriodo.SelectedValue)).Substring(2, 2);
                            iMes = 1;
                        }
                    }
                }

                if (GrvListado.Rows.Count == 0)
                {
                    List<BE_Indicador> lsBE_Indicador = new List<BE_Indicador>();
                    BE_Indicador oBE_Indicador = new BE_Indicador();
                    lsBE_Indicador.Add(oBE_Indicador);
                    GrvListado.DataSource = lsBE_Indicador;
                    GrvListado.DataBind();
                }

                ScriptManager.RegisterStartupScript(this, GetType(), "mensaje", "SessionExpireAlert(" + tiempo + ");", true);
            }
            else
                ScriptManager.RegisterStartupScript(this, GetType(), "mensaje", "myStopFunction();", true);
        }

        protected void GrvListado_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataKey dataKey;
                dataKey = this.GrvListado.DataKeys[e.Row.RowIndex];
                BE_Indicador oitem = (e.Row.DataItem as BE_Indicador);

                CheckBox chkSeleccion = (CheckBox)e.Row.FindControl("chkSeleccionar");
                chkSeleccion.Visible = true;

                if (dataKey.Values["iIdIndicador"] != null)
                {
                    if (oitem.iIdIndicador != 0) 
                    {
                        chkSeleccion.Attributes.Add("onclick", "HabilitarUno(this);");
                        
                        for (int c = 1; c < e.Row.Cells.Count; c++)
                        {
                            e.Row.Cells[c].Attributes.Add("onclick", string.Format("HabilitarUno(document.getElementById('{0}'));", chkSeleccion.ClientID));

                            if (c == 4) { e.Row.Cells[c].ToolTip = Convert.ToString(dataKey.Values["sObjetivo2"]); }
                            if (c == 5) { e.Row.Cells[c].ToolTip = Convert.ToString(dataKey.Values["sObjetivo3"]); }
                            if (c == 6) { e.Row.Cells[c].ToolTip = Convert.ToString(dataKey.Values["sObjetivo4"]); }
                            if (c == 7) { e.Row.Cells[c].ToolTip = Convert.ToString(dataKey.Values["sObjetivo5"]); }
                            if (c == 8) { e.Row.Cells[c].ToolTip = Convert.ToString(dataKey.Values["sObjetivo6"]); }
                            if (c == 9) { e.Row.Cells[c].ToolTip = Convert.ToString(dataKey.Values["sObjetivo7"]); }
                            if (c == 10) { e.Row.Cells[c].ToolTip = Convert.ToString(dataKey.Values["sObjetivo8"]); }
                            if (c == 11) { e.Row.Cells[c].ToolTip = Convert.ToString(dataKey.Values["sObjetivo9"]); }
                            if (c == 12) { e.Row.Cells[c].ToolTip = Convert.ToString(dataKey.Values["sObjetivo10"]); }
                            if (c == 13) { e.Row.Cells[c].ToolTip = Convert.ToString(dataKey.Values["sObjetivo11"]); }
                            if (c == 14) { e.Row.Cells[c].ToolTip = Convert.ToString(dataKey.Values["sObjetivo12"]); }
                            if (c == 15) { e.Row.Cells[c].ToolTip = Convert.ToString(dataKey.Values["sObjetivo13"]); }


                            if (dataKey.Values["sColor"] != null)
                            {
                                String sColor = dataKey.Values["sColor"].ToString();
                                String sMes1 = dataKey.Values["sMes1"].ToString();

                                if (sColor == "Verde")
                                {
                                    e.Row.Cells[3].BackColor = Color.Green;
                                    e.Row.Cells[3].ForeColor = Color.White;
                                }

                                if (sColor == "Ambar")
                                {
                                    e.Row.Cells[3].BackColor = Color.Orange;
                                    e.Row.Cells[3].ForeColor = Color.White;
                                }

                                if (sColor == "Rojo")
                                {
                                    e.Row.Cells[3].BackColor = Color.Red;
                                    e.Row.Cells[3].ForeColor = Color.White;
                                }
                                if ((sMes1 == ""))
                                {
                                    e.Row.Cells[3].BackColor = Color.White;
                                }
                                else
                                {
                                    e.Row.Cells[3].ToolTip = dataKey.Values["sTooltip"].ToString();
                                }
                            }//fin scolor

                            //
                            if (dataKey.Values["sColor2"] != null)
                            {
                                String sColor2 = dataKey.Values["sColor2"].ToString();
                                String sMes2 = dataKey.Values["sMes2"].ToString();

                                if (sColor2 == "Verde")
                                {
                                    e.Row.Cells[4].BackColor = Color.Green;
                                    e.Row.Cells[4].ForeColor = Color.White;
                                }

                                if (sColor2 == "Ambar")
                                {
                                    e.Row.Cells[4].BackColor = Color.Orange;
                                    e.Row.Cells[4].ForeColor = Color.White;
                                }

                                if (sColor2 == "Rojo")
                                {
                                    e.Row.Cells[4].BackColor = Color.Red;
                                    e.Row.Cells[4].ForeColor = Color.White;
                                }
                                if ((sMes2 == ""))
                                {
                                    e.Row.Cells[4].BackColor = Color.White;
                                }
                               
                            }//fin scolor2
                            //
                            if (dataKey.Values["sColor3"] != null)
                            {
                                String sColor3 = dataKey.Values["sColor3"].ToString();
                                String sMes3 = dataKey.Values["sMes3"].ToString();

                                if (sColor3 == "Verde")
                                {
                                    e.Row.Cells[5].BackColor = Color.Green;
                                    e.Row.Cells[5].ForeColor = Color.White;
                                }

                                if (sColor3 == "Ambar")
                                {
                                    e.Row.Cells[5].BackColor = Color.Orange;
                                    e.Row.Cells[5].ForeColor = Color.White;
                                }

                                if (sColor3 == "Rojo")
                                {
                                    e.Row.Cells[5].BackColor = Color.Red;
                                    e.Row.Cells[5].ForeColor = Color.White;
                                }
                                if ((sMes3 == ""))
                                {
                                    e.Row.Cells[5].BackColor = Color.White;
                                }
                              
                            }//fin scolor3

                            if (dataKey.Values["sColor4"] != null)
                            {
                                String sColor4 = dataKey.Values["sColor4"].ToString();
                                String sMes4 = dataKey.Values["sMes4"].ToString();

                                if (sColor4 == "Verde")
                                {
                                    e.Row.Cells[6].BackColor = Color.Green;
                                    e.Row.Cells[6].ForeColor = Color.White;
                                }

                                if (sColor4 == "Ambar")
                                {
                                    e.Row.Cells[6].BackColor = Color.Orange;
                                    e.Row.Cells[6].ForeColor = Color.White;
                                }

                                if (sColor4 == "Rojo")
                                {
                                    e.Row.Cells[6].BackColor = Color.Red;
                                    e.Row.Cells[6].ForeColor = Color.White;
                                }
                                if ((sMes4 == ""))
                                {
                                    e.Row.Cells[6].BackColor = Color.White;
                                }
                              
                            }//fin scolor

                            if (dataKey.Values["sColor5"] != null)
                            {
                                String sColor5 = dataKey.Values["sColor5"].ToString();
                                String sMes5 = dataKey.Values["sMes5"].ToString();

                                if (sColor5 == "Verde")
                                {
                                    e.Row.Cells[7].BackColor = Color.Green;
                                    e.Row.Cells[7].ForeColor = Color.White;
                                }

                                if (sColor5 == "Ambar")
                                {
                                    e.Row.Cells[7].BackColor = Color.Orange;
                                    e.Row.Cells[7].ForeColor = Color.White;
                                }

                                if (sColor5 == "Rojo")
                                {
                                    e.Row.Cells[7].BackColor = Color.Red;
                                    e.Row.Cells[7].ForeColor = Color.White;
                                }
                                if ((sMes5 == ""))
                                {
                                    e.Row.Cells[7].BackColor = Color.White;
                                }
                               
                            }//fin scolor5

                            if (dataKey.Values["sColor6"] != null)
                            {
                                String sColor6 = dataKey.Values["sColor6"].ToString();
                                String sMes6 = dataKey.Values["sMes6"].ToString();

                                if (sColor6 == "Verde")
                                {
                                    e.Row.Cells[8].BackColor = Color.Green;
                                    e.Row.Cells[8].ForeColor = Color.White;
                                }

                                if (sColor6 == "Ambar")
                                {
                                    e.Row.Cells[8].BackColor = Color.Orange;
                                    e.Row.Cells[8].ForeColor = Color.White;
                                }

                                if (sColor6 == "Rojo")
                                {
                                    e.Row.Cells[8].BackColor = Color.Red;
                                    e.Row.Cells[8].ForeColor = Color.White;
                                }
                                if ((sMes6 == ""))
                                {
                                    e.Row.Cells[8].BackColor = Color.White;
                                }
                              
                            }//fin scolor6
                            if (dataKey.Values["sColor7"] != null)
                            {
                                String sColor7 = dataKey.Values["sColor7"].ToString();
                                String sMes7 = dataKey.Values["sMes7"].ToString();

                                if (sColor7 == "Verde")
                                {
                                    e.Row.Cells[9].BackColor = Color.Green;
                                    e.Row.Cells[9].ForeColor = Color.White;
                                }

                                if (sColor7 == "Ambar")
                                {
                                    e.Row.Cells[9].BackColor = Color.Orange;
                                    e.Row.Cells[9].ForeColor = Color.White;
                                }

                                if (sColor7 == "Rojo")
                                {
                                    e.Row.Cells[9].BackColor = Color.Red;
                                    e.Row.Cells[9].ForeColor = Color.White;
                                }
                                if ((sMes7 == ""))
                                {
                                    e.Row.Cells[9].BackColor = Color.White;
                                }
                               
                            }//fin scolor7
                        }

                    }
                    else 
                    { 
                        chkSeleccion.Visible = false;

                        for (int c = 0; c < e.Row.Cells.Count; c++)
                        {
                            e.Row.Cells[c].Font.Bold = true;
                            e.Row.Cells[c].Font.Size = 8;
                            e.Row.Cells[c].BackColor = Color.LightGray;
                            e.Row.Cells[8].Text = String.Empty;
                        }
                    }

                    if (GlobalEntity.Instancia.Usuario.ToString() == "FMARDINI" || GlobalEntity.Instancia.Usuario.ToString() == "DBULLON")
                    {
                        if (GlobalEntity.Instancia.Usuario.ToString() != dataKey.Values["sPropietario"].ToString())
                        {
                            for (int c = 0; c < e.Row.Cells.Count; c++)
                            {
                                e.Row.Cells[c].Visible = false;
                            }
                        }
                    }

                    e.Row.Style["cursor"] = "pointer";
                }
                else
                {
                    e.Row.Visible = false;
                }
            }
        }

        protected void ImgBtnBuscar_Click(object sender, ImageClickEventArgs e)
        {
            dnvListado.InvokeBind();

            if (GrvListado.Rows.Count == 0)
            {
                List<BE_Indicador> lsBE_Indicador = new List<BE_Indicador>();
                BE_Indicador oBE_Indicador = new BE_Indicador();
                lsBE_Indicador.Add(oBE_Indicador);
                GrvListado.DataSource = lsBE_Indicador;
                GrvListado.DataBind();
            }
        }

        protected void DplNegocio_SelectedIndexChanged(object sender, EventArgs e)
        {
            dnvListado.InvokeBind();
        }

        protected void DplAnioPeriodo_SelectedIndexChanged(object sender, EventArgs e)
        {
            dnvListado.InvokeBind();
        }

        protected void DplMesPeriodo_SelectedIndexChanged(object sender, EventArgs e)
        {
            dnvListado.InvokeBind();
        }

        protected void DplDatoMostrar_SelectedIndexChanged(object sender, EventArgs e)
        {
            dnvListado.InvokeBind();
        }
    #endregion



    #region Metodo Transacciones
        DataNavigatorParams BindGridLista(object sender, EventArgs e)
        {
            BL_Indicador oBL_Indicador = new BL_Indicador();
            BE_Indicador oBE_Indicador = new BE_Indicador();
            
            //Control ocultar y mostrar columnas del detalle
            int iMes = Convert.ToInt32(DplMesPeriodo.SelectedValue);
            String sAnio = Convert.ToString(Convert.ToInt32(DplAnioPeriodo.SelectedValue) - 1).Substring(2,2);

            iMes = iMes - 1;
            for (int iCol = 14; iCol >= 2; iCol--)
            {
                if (iCol > 8)
                {
                    GrvListado.Columns[iCol + 1].Visible = false;
                    iMes = iMes + 1;
                }
                else
                {
                    GrvListado.Columns[iCol + 1].Visible = true;
                    if (sAnio == "13") { GrvListado.Columns[iCol + 1].Visible = false; }
                    iMes = iMes + 1;
                    GrvListado.Columns[iCol + 1].HeaderText = NombreMes(iMes) + " " + sAnio;
                    if (iMes > 12)
                    {
                        sAnio = Convert.ToString(Convert.ToInt32(DplAnioPeriodo.SelectedValue)).Substring(2, 2);
                        iMes = 1;
                    }
                }
            }

            oBE_Indicador.iAnho = Convert.ToInt32(DplAnioPeriodo.SelectedValue);
            oBE_Indicador.iMes = Convert.ToInt32(DplMesPeriodo.SelectedValue);
            oBE_Indicador.iAgrupacion = Convert.ToInt32(DplNegocio.SelectedValue);
            oBE_Indicador.sUsuario = GlobalEntity.Instancia.Usuario.ToString();
            oBE_Indicador.sModulo = "D";
            oBE_Indicador.iDatoMostrar = Convert.ToInt32(DplDatoMostrar.SelectedValue);

            GrvListado.PageSize = 12;
            oBE_Indicador.NPagina = dnvListado.CurrentPage;
            oBE_Indicador.NRegistros = GrvListado.PageSize;

            IList<BE_Indicador> lista = oBL_Indicador.ListarAnual(oBE_Indicador);
            dnvListado.Visible = (oBE_Indicador.NTotalRegistros > oBE_Indicador.NRegistros);
            LblTotal.Text = "Total de Registros: " + Convert.ToString(oBE_Indicador.NTotalRegistros);

            return new DataNavigatorParams(lista, oBE_Indicador.NTotalRegistros);
        }
    #endregion



    #region "Método Controles"
        protected void ListarDropDowList()
        {
            try
            {
                DplAnioPeriodo.Items.Clear();
                for (int i = 2013; i <= DateTime.Now.Year; i++)
                {
                    DplAnioPeriodo.Items.Add(new ListItem(i.ToString(), i.ToString()));
                }
                DplAnioPeriodo.SelectedIndex = 0;


                DplMesPeriodo.Items.Clear();
                DplMesPeriodo.Items.Add(new ListItem("Enero", "1"));
                DplMesPeriodo.Items.Add(new ListItem("Febrero", "2"));
                DplMesPeriodo.Items.Add(new ListItem("Marzo", "3"));
                DplMesPeriodo.Items.Add(new ListItem("Abril", "4"));
                DplMesPeriodo.Items.Add(new ListItem("Mayo", "5"));
                DplMesPeriodo.Items.Add(new ListItem("Junio", "6"));
                DplMesPeriodo.Items.Add(new ListItem("Julio", "7"));
                DplMesPeriodo.Items.Add(new ListItem("Agosto", "8"));
                DplMesPeriodo.Items.Add(new ListItem("Setiembre", "9"));
                DplMesPeriodo.Items.Add(new ListItem("Octubre", "10"));
                DplMesPeriodo.Items.Add(new ListItem("Noviembre", "11"));
                DplMesPeriodo.Items.Add(new ListItem("Diciembre", "12"));


                DplNegocio.Items.Add(new ListItem("Depósito Temporal Contenedores", "1"));
                DplNegocio.Items.Add(new ListItem("Depósito Temporal Carga Suelta y Rodante", "2"));
                DplNegocio.Items.Add(new ListItem("Depósito Aduanero y Almacenaje Simple", "3"));


                DplDatoMostrar.Items.Add(new ListItem("El Valor", "1"));
                DplDatoMostrar.Items.Add(new ListItem("El % de Cumplimiento", "2"));
            }
            catch (Exception e)
            {

            }
        }

        protected String NombreMes(int pMes)
        {
            String NombreMes = String.Empty;

            switch (pMes)
            {
                case 1: { NombreMes = "Ene"; break;}
                case 2: { NombreMes = "Feb"; break; }
                case 3: { NombreMes = "Mar"; break; }
                case 4: { NombreMes = "Abr"; break; }
                case 5: { NombreMes = "May"; break; }
                case 6: { NombreMes = "Jun"; break; }
                case 7: { NombreMes = "Jul"; break; }
                case 8: { NombreMes = "Ago"; break; }
                case 9: { NombreMes = "Set"; break; }
                case 10: { NombreMes = "Oct"; break; }
                case 11: { NombreMes = "Nov"; break; }
                case 12: { NombreMes = "Dic"; break; }
                case 13: { NombreMes = "Ene"; break; }
            }

            return NombreMes;
        }
    #endregion


        protected void btn_cerrar_Click(object sender, EventArgs e)
        {
            if (GlobalEntity.Instancia.CerrarExtranet == "S")
            {
                BL_Usuario oBL_Usuario = new BL_Usuario();
                oBL_Usuario.GrabaInicioSesion(GlobalEntity.Instancia.Usuario, "0");
                Session.Abandon();
                FormsAuthentication.SignOut();
                Response.Redirect("../../eFENIX_Login.aspx");
                Response.End();
            }
        }
}
