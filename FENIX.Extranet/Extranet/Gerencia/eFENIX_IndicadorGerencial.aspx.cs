﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using QNET.Web.UI.Controls;
using FENIX.BusinessEntity;
using FENIX.BusinessLogic;
using FENIX.Common;
using System.Collections.Generic;
using System.Drawing;




public partial class Extranet_Gerencia_eFENIX_IndicadorGerencial : System.Web.UI.Page
{
    #region "Evento Pagina"
        protected void Page_Load(object sender, EventArgs e)
        {
            dnvListado.BindGridView += new QNET.Web.UI.Controls.DataNavigator.BindGridViewDelegate(BindGridLista);

            String tiempo = (Convert.ToInt32(GlobalEntity.Instancia.TiempoDuracionSesion.ToString()) * 1000 * 60).ToString();
            Session["Reset"] = true;

            if (!Page.IsPostBack)
            {
                ViewState["Modulo"] = Request.QueryString["Modulo"];

                if (ViewState["Modulo"].ToString() == "F")
                {
                    LblTitulo.Text = "Indicador Gerencial";
                    GrvListado.Columns[5].Visible = true;
                    GrvListado.Columns[6].Visible = true;
                    GrvListado.Columns[7].Visible = true;
                }
                else
                {
                    LblTitulo.Text = "Directorio";
                    GrvListado.Columns[5].Visible = true;
                    GrvListado.Columns[6].Visible = false;
                    GrvListado.Columns[7].Visible = false;
                }

                GrvListado.Columns[6].Visible = false;
                if (GlobalEntity.Instancia.Usuario.ToString() == "FMARDINI" || GlobalEntity.Instancia.Usuario.ToString() == "DBULLON")
                {
                    GrvListado.Columns[6].Visible = true;
                }

                ListarDropDowList();

                if (DateTime.Now.Month == 1)
                {
                    DplAnioPeriodo.SelectedValue = (DateTime.Now.Year - 1).ToString();
                    DplMesPeriodo.SelectedValue = "12";
                }
                else
                {
                    DplAnioPeriodo.SelectedValue = DateTime.Now.Year.ToString();
                    DplMesPeriodo.SelectedValue = (DateTime.Now.Month - 1).ToString();
                }

                if (GrvListado.Rows.Count == 0)
                {
                    List<BE_Indicador> lsBE_Indicador = new List<BE_Indicador>();
                    BE_Indicador oBE_Indicador = new BE_Indicador();
                    lsBE_Indicador.Add(oBE_Indicador);
                    GrvListado.DataSource = lsBE_Indicador;
                    GrvListado.DataBind();
                }

                ScriptManager.RegisterStartupScript(this, GetType(), "mensaje", "SessionExpireAlert(" + tiempo + ");", true);
            }
            else
                ScriptManager.RegisterStartupScript(this, GetType(), "mensaje", "myStopFunction();", true);
        }

        protected void GrvListado_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataKey dataKey;
                dataKey = this.GrvListado.DataKeys[e.Row.RowIndex];
                BE_Indicador oitem = (e.Row.DataItem as BE_Indicador);

                CheckBox chkSeleccion = (CheckBox)e.Row.FindControl("chkSeleccionar");
                ImageButton ibtnGrabar = (ImageButton)e.Row.Cells[7].Controls[0];
                ImageButton ibtnVer = (ImageButton)e.Row.Cells[9].Controls[0];

                chkSeleccion.Visible = true;
                ibtnGrabar.Visible = true;
                ibtnVer.Visible = true; 

                if (dataKey.Values["iIdIndicador"] != null)
                {
                    if (oitem.iIdIndicador != 0) 
                    {
                        chkSeleccion.Attributes.Add("onclick", "HabilitarUno(this);");
                        for (int c = 1; c < e.Row.Cells.Count; c++)
                        {
                            e.Row.Cells[c].Attributes.Add("onclick", string.Format("HabilitarUno(document.getElementById('{0}'));", chkSeleccion.ClientID));
                        }
                    }
                    else 
                    { 
                        chkSeleccion.Visible = false;
                        ibtnGrabar.Visible = false;
                        ibtnVer.Visible = false; 

                        for (int c = 0; c < e.Row.Cells.Count; c++)
                        {
                            e.Row.Cells[c].Font.Bold = true;
                            e.Row.Cells[c].Font.Size = 8;
                            e.Row.Cells[c].BackColor = Color.LightGray;
                            e.Row.Cells[8].Text = String.Empty;
                        }
                    }

                    if (GlobalEntity.Instancia.Usuario.ToString() == "FMARDINI" || GlobalEntity.Instancia.Usuario.ToString() == "DBULLON")
                    {
                        if (GlobalEntity.Instancia.Usuario.ToString() != dataKey.Values["sPropietario"].ToString())
                        {
                            for (int c = 0; c < e.Row.Cells.Count; c++)
                            {
                                e.Row.Cells[c].Visible = false;
                            }
                        }
                    }

                    if (dataKey.Values["sVerDetalle"].ToString() == "N")
                    {
                        ibtnVer.Visible = false; 
                    }

                    if (dataKey.Values["sEstado"].ToString().ToUpper() == "PENDIENTE")
                    {
                        e.Row.Cells[6].ForeColor = Color.Red;
                    }
                    else
                    {
                        ibtnGrabar.Visible = false;
                    }

                    if (dataKey.Values["sFormula"].ToString() != String.Empty)
                    {
                        e.Row.Cells[6].Text = dataKey.Values["sFormula"].ToString();
                    }

                    System.Web.UI.WebControls.Image ibtnSemaforo = new System.Web.UI.WebControls.Image();
                    ibtnSemaforo = e.Row.Cells[5].FindControl("Image") as System.Web.UI.WebControls.Image;

                    if (oitem.iIdIndicador != 0)
                    {
                        e.Row.Cells[1].ToolTip = dataKey.Values["sComplemento"].ToString();
                        if (dataKey.Values["sColor"] != null)
                        {
                            String sColor = dataKey.Values["sColor"].ToString();
                            ibtnSemaforo.Visible = true;

                            if (sColor == "Verde")
                            {
                                ibtnSemaforo.ImageUrl = "~/Script/Imagenes/IconButton/circle_green.png";
                                e.Row.Cells[4].ForeColor = Color.Green;
                            }

                            if (sColor == "Ambar")
                            {
                                ibtnSemaforo.ImageUrl = "~/Script/Imagenes/IconButton/circle_yellow.png";
                                e.Row.Cells[4].ForeColor = Color.Orange;
                            }

                            if (sColor == "Rojo")
                            {
                                ibtnSemaforo.ImageUrl = "~/Script/Imagenes/IconButton/circle_red.png";
                                e.Row.Cells[4].ForeColor = Color.Red;
                            }

                            if (sColor == "" )
                            {
                                ibtnSemaforo.ImageUrl = "~/Script/Imagenes/IconButton/circle_red.png";
                                e.Row.Cells[4].ForeColor = Color.Red;
                                e.Row.Cells[2].Text = "0";
                                e.Row.Cells[4].Text = "0 %";

                                //ibtnSemaforo.Visible = false;
                                e.Row.Cells[4].ToolTip = "";
                                e.Row.Cells[5].ToolTip = "";
                                
                            }
                            else
                            {
                                e.Row.Cells[4].ToolTip = dataKey.Values["sTooltip"].ToString();
                                e.Row.Cells[5].ToolTip = dataKey.Values["sTooltip"].ToString();
                            }
                        }
                    }
                    else
                    {
                        ibtnSemaforo.Visible = false;
                    }

                    e.Row.Style["cursor"] = "pointer";
                }
                else
                {
                    e.Row.Visible = false;
                }
            }
        }

        protected void GrvListado_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            BL_Indicador oBL_Indicador = new BL_Indicador();
            BE_Indicador oBE_Indicador = new BE_Indicador();

            if (e.CommandName == "OpenVerDetalle")
            {
                String sNombreIndicador = GrvListado.DataKeys[Convert.ToInt32(e.CommandArgument)].Values["sNombreIndicador"].ToString();
                String sValorTotal = GrvListado.DataKeys[Convert.ToInt32(e.CommandArgument)].Values["sValor"].ToString();
                Int32 iAgrupacion = Convert.ToInt32(DplNegocio.SelectedValue);

                LblPeriodo.Text = DplMesPeriodo.SelectedItem.Text + " del " + DplAnioPeriodo.SelectedValue;
                LblIndicador.Text = sNombreIndicador;
                LblTotalGerenal.Text = sValorTotal;

                //Control ocultar y mostrar columnas del detalle
                GrvDetalle.Columns[2].HeaderText = "Sem 1";
                GrvDetalle.Columns[8].HeaderText = "Sub Total";
                GrvDetalle.Columns[2].Visible = true;
                GrvDetalle.Columns[3].Visible = true;
                GrvDetalle.Columns[4].Visible = true;
                GrvDetalle.Columns[5].Visible = true;
                GrvDetalle.Columns[6].Visible = true;
                GrvDetalle.Columns[7].Visible = true;
                GrvDetalle.Columns[8].Visible = true;
                GrvDetalle.Columns[9].Visible = true;

                //if (iAgrupacion == 1)
                //{
                    if (LblIndicador.Text.Substring(0, 1) == "A")
                    {
                        GrvDetalle.Columns[9].Visible = false;
                        GrvDetalle.Columns[8].HeaderText = "Promedio";
                    }
                    if (LblIndicador.Text.Substring(0, 1) == "B")
                    {
                        GrvDetalle.Columns[2].HeaderText = "Dato";
                        GrvDetalle.Columns[3].Visible = false;
                        GrvDetalle.Columns[4].Visible = false;
                        GrvDetalle.Columns[5].Visible = false;
                        GrvDetalle.Columns[6].Visible = false;
                        GrvDetalle.Columns[7].Visible = false;
                        GrvDetalle.Columns[8].Visible = false;
                        GrvDetalle.Columns[9].Visible = false;
                    }
                //}

                oBE_Indicador.iAnho = Convert.ToInt32(DplAnioPeriodo.SelectedValue);
                oBE_Indicador.iMes = Convert.ToInt32(DplMesPeriodo.SelectedValue);
                oBE_Indicador.iIdIndicador = Convert.ToInt32(GrvListado.DataKeys[Convert.ToInt32(e.CommandArgument)].Values["iIdIndicador"].ToString());
                oBE_Indicador.iValorTotal = Convert.ToInt32(Convert.ToDecimal(sValorTotal));
                GrvDetalle.DataSource = oBL_Indicador.ListarDetalle(oBE_Indicador);
                GrvDetalle.DataBind();

                if (GrvDetalle.Rows.Count == 0)
                {
                    List<BE_Indicador> lsBE_Indicador = new List<BE_Indicador>();
                    lsBE_Indicador.Add(oBE_Indicador);
                    GrvDetalle.EmptyDataText = "La vista detallada no está disponible";
                    GrvDetalle.DataSource = lsBE_Indicador;
                    GrvDetalle.DataBind();
                }

                ScriptManager.RegisterStartupScript(GrvListado, GetType(), "AbrirPopup", String.Format("javascript: OpenPopub();"), true);
            }

            if (e.CommandName == "AprobarDesaprobar")
            {
                oBE_Indicador.iAnho = Convert.ToInt32(DplAnioPeriodo.SelectedValue);
                oBE_Indicador.iMes = Convert.ToInt32(DplMesPeriodo.SelectedValue);
                oBE_Indicador.iIdIndicador = Convert.ToInt32(GrvListado.DataKeys[Convert.ToInt32(e.CommandArgument)].Values["iIdIndicador"].ToString());
                oBE_Indicador.sEstado = "1";
                oBE_Indicador.sUsuario = GlobalEntity.Instancia.Usuario;
                oBE_Indicador.sNombrePc = GlobalEntity.Instancia.NombrePc;

                int vl_iCodigo = 1;
                String vl_sMessage = string.Empty;
                String[] oResultadoTransaccionTx;

                oResultadoTransaccionTx = oBL_Indicador.AprobarDesaprobar(oBE_Indicador).Split('|');
                vl_sMessage = string.Empty;

                for (int i = 0; i < oResultadoTransaccionTx.Length; i++)
                {
                    if (i == 0)
                    {
                        vl_iCodigo = Convert.ToInt32(oResultadoTransaccionTx[i]);
                    }
                    else
                    {
                        vl_sMessage = oResultadoTransaccionTx[i];
                    }
                }

                if (vl_iCodigo < 1)
                {
                    ScriptManager.RegisterStartupScript(upDocumentoOrigen, GetType(), "mensaje", String.Format(Resources.Resource.MsgJavaError, vl_sMessage), true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(upDocumentoOrigen, GetType(), "mensaje", String.Format(Resources.Resource.MsgJavaExito, vl_sMessage), true);
                    dnvListado.InvokeBind();
                }
            }
        }

        protected void GrvDetalle_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataKey dataKey;
                dataKey = this.GrvDetalle.DataKeys[e.Row.RowIndex];
                BE_Indicador oitem = (e.Row.DataItem as BE_Indicador);

                if (dataKey.Values["sNombreIndicador"] != null)
                {
                    CheckBox chkSeleccionarDet = (CheckBox)e.Row.FindControl("chkSeleccionarDet");

                    if (oitem.sNombreIndicador != String.Empty)
                    {
                        chkSeleccionarDet.Attributes.Add("onclick", "HabilitarUno(this);");

                        for (int c = 1; c < e.Row.Cells.Count; c++)
                        {
                            e.Row.Cells[c].Attributes.Add("onclick", string.Format("HabilitarUno(document.getElementById('{0}'));", chkSeleccionarDet.ClientID));
                        }

                        if (oitem.sNombreIndicador == "Sub Total")
                        {
                            for (int c = 0; c < e.Row.Cells.Count; c++)
                            {
                                e.Row.Cells[c].Font.Bold = true;
                                e.Row.Cells[c].Font.Size = 8;
                                e.Row.Cells[c].BackColor = Color.LightGray;
                            }
                        }

                        //Color para las columans de sub totales
                        e.Row.Cells[8].Font.Bold = true;
                        e.Row.Cells[8].Font.Size = 8;
                        e.Row.Cells[8].BackColor = Color.LightGray;
                        e.Row.Cells[9].Font.Bold = true;
                        e.Row.Cells[9].Font.Size = 8;
                        e.Row.Cells[9].BackColor = Color.LightGreen;
                    }

                    e.Row.Style["cursor"] = "pointer";
                }
                else
                {
                    e.Row.Visible = false;
                }
            }
        }

        protected void ImgBtnBuscar_Click(object sender, ImageClickEventArgs e)
        {
            dnvListado.InvokeBind();

            if (GrvListado.Rows.Count == 0)
            {
                List<BE_Indicador> lsBE_Indicador = new List<BE_Indicador>();
                BE_Indicador oBE_Indicador = new BE_Indicador();
                lsBE_Indicador.Add(oBE_Indicador);
                GrvListado.DataSource = lsBE_Indicador;
                GrvListado.DataBind();
            }
        }

        protected void ImgBtnActualizar_Click(object sender, ImageClickEventArgs e)
        {

        }

        protected void ImgBtnGrabar_Click(object sender, ImageClickEventArgs e)
        {

        }

        protected void DplNegocio_SelectedIndexChanged(object sender, EventArgs e)
        {
            dnvListado.InvokeBind();
        }

        protected void DplAnioPeriodo_SelectedIndexChanged(object sender, EventArgs e)
        {
            dnvListado.InvokeBind();
        }

        protected void DplMesPeriodo_SelectedIndexChanged(object sender, EventArgs e)
        {
            dnvListado.InvokeBind();
        }


        protected void btn_cerrar_Click(object sender, EventArgs e)
        {
            if (GlobalEntity.Instancia.CerrarExtranet == "S")
            {
                BL_Usuario oBL_Usuario = new BL_Usuario();
                oBL_Usuario.GrabaInicioSesion(GlobalEntity.Instancia.Usuario, "0");
                Session.Abandon();
                FormsAuthentication.SignOut();
                Response.Redirect("../../eFENIX_Login.aspx");
                Response.End();
            }
        }

    #endregion




    #region Metodo Transacciones
        DataNavigatorParams BindGridLista(object sender, EventArgs e)
        {
            BL_Indicador oBL_Indicador = new BL_Indicador();
            BE_Indicador oBE_Indicador = new BE_Indicador();

            oBE_Indicador.iAnho = Convert.ToInt32(DplAnioPeriodo.SelectedValue);
            oBE_Indicador.iMes = Convert.ToInt32(DplMesPeriodo.SelectedValue);
            oBE_Indicador.iAgrupacion = Convert.ToInt32(DplNegocio.SelectedValue);
            oBE_Indicador.sUsuario = GlobalEntity.Instancia.Usuario.ToString();
            oBE_Indicador.sModulo = ViewState["Modulo"].ToString();

            GrvListado.PageSize = 12;
            oBE_Indicador.NPagina = dnvListado.CurrentPage;
            oBE_Indicador.NRegistros = GrvListado.PageSize;

            IList<BE_Indicador> lista = oBL_Indicador.Listar(oBE_Indicador);
            dnvListado.Visible = (oBE_Indicador.NTotalRegistros > oBE_Indicador.NRegistros);
            LblTotal.Text = "Total de Registros: " + Convert.ToString(oBE_Indicador.NTotalRegistros);

            return new DataNavigatorParams(lista, oBE_Indicador.NTotalRegistros);
        }
    #endregion



    #region "Método Controles"
        protected void ListarDropDowList()
        {
            try
            {
                DplAnioPeriodo.Items.Clear();
                for (int i = 2013; i <= DateTime.Now.Year; i++)
                {
                    DplAnioPeriodo.Items.Add(new ListItem(i.ToString(), i.ToString()));
                }
                DplAnioPeriodo.SelectedIndex = 0;


                DplMesPeriodo.Items.Clear();
                DplMesPeriodo.Items.Add(new ListItem("Enero", "1"));
                DplMesPeriodo.Items.Add(new ListItem("Febrero", "2"));
                DplMesPeriodo.Items.Add(new ListItem("Marzo", "3"));
                DplMesPeriodo.Items.Add(new ListItem("Abril", "4"));
                DplMesPeriodo.Items.Add(new ListItem("Mayo", "5"));
                DplMesPeriodo.Items.Add(new ListItem("Junio", "6"));
                DplMesPeriodo.Items.Add(new ListItem("Julio", "7"));
                DplMesPeriodo.Items.Add(new ListItem("Agosto", "8"));
                DplMesPeriodo.Items.Add(new ListItem("Setiembre", "9"));
                DplMesPeriodo.Items.Add(new ListItem("Octubre", "10"));
                DplMesPeriodo.Items.Add(new ListItem("Noviembre", "11"));
                DplMesPeriodo.Items.Add(new ListItem("Diciembre", "12"));


                DplNegocio.Items.Add(new ListItem("Depósito Temporal Contenedores", "1"));
                DplNegocio.Items.Add(new ListItem("Depósito Temporal Carga Suelta y Rodante", "2"));
                DplNegocio.Items.Add(new ListItem("Depósito Aduanero y Almacenaje Simple", "3"));

            }
            catch (Exception e)
            {

            }
        }
    #endregion
}
