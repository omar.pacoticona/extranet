﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="eFENIX_AprobarDsctoAlmacenaje.aspx.cs" Inherits="Extranet_Gerencia_eFENIX_AprobarDsctoAlmacenaje" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Aprobacion de Descuentos e-Fenix</title>

    <link href="../../Script/Hojas_Estilo/eFenix.css" rel="stylesheet" type="text/css" />
    <script language="javascript" type="text/javascript">
        function cerrarpagina() {
            window.open('', '_parent', '');
            window.close();
            return false;
        }
    </script>
</head>
<body style="margin:0px;">
    <form id="form1" runat="server">
        <ajax:ScriptManager ID="scmLogin"  runat="server">
        </ajax:ScriptManager>

        <div style="height: 100%; width: 100%;padding-top:100px;">
        
            <table cellpadding="0" cellspacing="0" border="0px" align="center" width="100%" >
                <tr>                   
                    <td style="text-align:center;" >
                        <img src="../../Imagenes/login_logo.jpg" />
                    </td>                    
                </tr>
                <tr style="height:195px;background-color:#243F90;">                    
                    <td>
                        <table cellpadding="0" cellspacing="1" style="width:100%;text-align:center;font-family:Calibri; color: #FFFFFF;">
                            <tr>
                                <td align="center" valign="middle" colspan="2">
                                   <h1>Aprobar Descuentos</h1>
                                </td>
                            </tr>
                            
                            <tr>
                                <td style="text-align:right;width:50%;">
                                    Cliente
                                </td>
                                <td style="padding-left:10px; text-align:left;">
                                    <asp:Label ID="LbLCliente" runat="server" Text="Label"></asp:Label>
                                </td>
                            </tr>
                            
                            <tr>
                                <td style="text-align:right;">
                                    Agencia Carga</td>
                                <td style="padding-left:10px; text-align:left;">
                                    <asp:Label ID="LbLAgCarga" runat="server" Text="Label"></asp:Label>
                                </td>
                            </tr>
                            
                            <tr>
                                <td style="text-align:right;">
                                    Agencia Aduana</td>
                                <td style="padding-left:10px; text-align:left;">
                                    <asp:Label ID="LbLAgAduana" runat="server" Text="Label"></asp:Label>
                                </td>
                            </tr>
                            
                            <tr>
                                <td style="text-align:right;">
                                   Doc.Origen
                                </td>
                                <td style="padding-left:10px; text-align:left;">
                                    <asp:Label ID="LblDocOrigen" runat="server" Text="Label"></asp:Label>
                                </td>
                            </tr>
                           
                            <tr>
                                <td style="text-align:right;">
                                    Calculado al </td>
                                <td style="padding-left:10px; text-align:left;">
                                    <asp:Label ID="LblFecha" runat="server" Text="Label"></asp:Label>
                                </td>
                            </tr>
                           
                            <tr>
                                <td style="text-align:right;">
                                    Dias Almacenaje</td>
                                <td style="padding-left:10px; text-align:left;">
                                    <asp:Label ID="LblDiasAlmacen" runat="server" Text="Label"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align:right;">
                                    % Dscto.</td>
                                <td style="padding-left:10px; text-align:left;">
                                    <asp:Label ID="LblPorcentaje" runat="server" Text="Label"></asp:Label>
                                </td>
                            </tr>
                            
                            <tr>
                                <td style="text-align:right;">
                                    Importe Dscto US$</td>
                                <td style="padding-left:10px; text-align:left;">
                                    <asp:Label ID="LblDescuento" runat="server" Text="Label"></asp:Label>
                                </td>
                            </tr>
                            
                            <tr>
                                <td style="text-align:right;">
                                    Aprueba</td>
                                <td style="padding-left:10px;">                                   
                                    <asp:RadioButtonList ID="RblAprobacion" runat="server" RepeatColumns="2">
                                        <asp:ListItem Value="S">SI</asp:ListItem>
                                        <asp:ListItem Value="N">NO</asp:ListItem>
                                    </asp:RadioButtonList>                                    
                                </td>
                            </tr>
                            </table>
                    
                    </td>                    
                </tr>
                <tr>                    
                    <td align="center" valign="middle">
                              <asp:Button ID="btnAceptar" runat="server" CssClass="botonAceptar" 
                                    style="border-width:0px;"                                     
                                    OnClick="btnAceptar_Click" /> 
                              
                              <asp:Button ID="btnCancelar" runat="server" CssClass="botonCancelar" 
                                    style="border-width:0px;" 
                                    OnClientClick="return cerrarpagina();"/> 
                            </td>                                       
                </tr>
            </table> 
            
                                                           
        </div>
    </form>
</body>
</html>
