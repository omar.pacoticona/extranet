<%@ Page Language="C#" MasterPageFile="~/Extranet/Seguridad/eFENIX_Principal.master"
    AutoEventWireup="true" CodeFile="eFENIX_PeriodoOperativo.aspx.cs" Inherits="Extranet_Gerencia_eFENIX_PeriodoOperativo"
    Title="Periodo Operativo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CntMaster" runat="Server">
    <table cellspacing="0" cellpadding="0" style="width: 100%;">
        <tr>
            <td class="form_titulo" style="width: 85%">
                HABILITAR PERIODO - OPERATIVO
            </td>
            <td align="right" class="form_titulo_verde" style="width: 15%">
                <asp:TextBox ID="LblTotal" Text="" style="background-color: #0069ae; color: #FFFFFF" ReadOnly="true" BorderColor="#0069ae" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td valign="top" colspan="2">
                <table class="form_bandeja" style="margin-top: 5px; margin-bottom: 5px; width: 100%;">
                    <tr>
                        <td colspan="5">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 6%;">
                            Periodo
                        </td>
                        <td style="width: 6%;">
                            <asp:DropDownList ID="DplAnioPeriodo" runat="server" AutoPostBack="True" 
                                onselectedindexchanged="DplAnioPeriodo_SelectedIndexChanged">
                            </asp:DropDownList>
                        </td>
                        <td style="width: 6%;">
                            <asp:DropDownList ID="DplMesPeriodo" runat="server" Width="100px" 
                                AutoPostBack="True" onselectedindexchanged="DplMesPeriodo_SelectedIndexChanged">
                            </asp:DropDownList>
                        </td>
                        <td colspan="3" style="color: #008000; text-transform: none;">
                            Solo se puede habilitar dos periodos anteriores.
                        </td>
                        <td align="right">   
                            <asp:ImageButton ID="BtnVerHistorial" runat="server" ImageUrl="~/Imagenes/Formulario/btn_historial.JPG"
                                OnClick="BtnVerHistorial_Click" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="5">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td valign="top" colspan="2">
                <ajax:UpdatePanel ID="UpControles" UpdateMode="Conditional" runat="server">
                    <ContentTemplate>
                        <table class="form_alterno" style="margin-top: 5px; margin-bottom: 5px; width: 100%;">
                            <tr>
                                <td colspan="6">
                                    PROCESOS
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 2%;">
                                </td>
                                <td style="width: 25%;">
                                    Habilitar Movimiento de Balanza
                                </td>
                                <td style="width: 3%;">
                                    <asp:CheckBox ID="ChkMovBal" runat="server" OnCheckedChanged="ChkMovBal_CheckedChanged"
                                        AutoPostBack="True" />
                                </td>
                                <td style="width: 4%;">
                                    d�as
                                </td>
                                <td style="width: 4%;">
                                    <asp:TextBox ID="TxtDiasMovBal" Width="30px" runat="server" MaxLength="2" Enabled="false" BackColor="WhiteSmoke"></asp:TextBox>
                                </td>
                                <td>
                                    <asp:Label ID="LblFechaMovBal" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                </td>
                                <td>
                                    Cambiar Fecha del Movimiento de Balanza
                                </td>
                                <td>
                                    <asp:CheckBox ID="ChkFechaMovBal" runat="server" OnCheckedChanged="ChkFechaMovBal_CheckedChanged"
                                        AutoPostBack="True" />
                                </td>
                                <td>
                                    d�as
                                </td>
                                <td>
                                    <asp:TextBox ID="TxtDiasFechaMovBal" Width="30px" runat="server" MaxLength="2" Enabled="false" BackColor="WhiteSmoke"></asp:TextBox>
                                </td>
                                <td>
                                    <asp:Label ID="LblDiasFechaMovBal" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                </td>
                                <td>
                                    Cambiar Pesos del Movimiento de Balanza
                                </td>
                                <td>
                                    <asp:CheckBox ID="ChkPesosMovBal" runat="server" OnCheckedChanged="ChkPesosMovBal_CheckedChanged"
                                        AutoPostBack="True" />
                                </td>
                                <td>
                                    d�as
                                </td>
                                <td>
                                    <asp:TextBox ID="TxtDiasPesosMovBal" Width="30px" runat="server" MaxLength="2" Enabled="false" BackColor="WhiteSmoke"></asp:TextBox>
                                </td>
                                <td>
                                    <asp:Label ID="LblDiasPesosMovBal" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                </td>
                                <td>
                                    Cambiar Fecha de Balanza Libre
                                </td>
                                <td>
                                    <asp:CheckBox ID="ChkFechaBalLib" runat="server" AutoPostBack="True" OnCheckedChanged="ChkFechaBalLib_CheckedChanged" />
                                </td>
                                <td>
                                    d�as
                                </td>
                                <td>
                                    <asp:TextBox ID="TxtDiasFechaBalLib" Width="30px" runat="server" MaxLength="2" Enabled="false" BackColor="WhiteSmoke"></asp:TextBox>
                                </td>
                                <td>
                                    <asp:Label ID="LblDiasFechaBalLib" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="6">
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td align="center" colspan="5">
                                    <asp:ImageButton ID="ImgBtnGrabar" runat="server" ImageUrl="~/Imagenes/Formulario/btn_aceptar.JPG"
                                        OnClick="ImgBtnGrabar_Click"/>
                                </td>
                                <td>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="6">
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td align="center" colspan="5">
                                    Si desea cerrar el periodo haga click <asp:LinkButton ID="BtnCerrarPeriodo" 
                                        runat="server" Text="Aqui" onclick="BtnCerrarPeriodo_Click"></asp:LinkButton>
                                </td>
                                <td>
                                </td>
                            </tr>
                        </table>
                    </ContentTemplate>
                    <Triggers>
                        <ajax:AsyncPostBackTrigger ControlID="DplAnioPeriodo" EventName="SelectedIndexChanged" />
                        <ajax:AsyncPostBackTrigger ControlID="DplMesPeriodo" EventName="SelectedIndexChanged" />
                    </Triggers>
                </ajax:UpdatePanel>
            </td>
        </tr>
    </table>
    
    <%-- Popup - Historial de Cambios--%>
    <asp:Panel ID="__PopPanel__" runat="server" CssClass="modalBackground" Style="left: 0px;
        top: 0px; display: none; position: fixed; z-index: 10000; display: none;" />
    
    <asp:Panel ID="pnlDetalle" runat="server" Style="width: 600px; height: 310px; 
        position: fixed; z-index: 100002; border: dimgray 1px solid; background-color: White; display: none;">
        <ajax:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <table border="0" cellpadding="0" cellspacing="0" width="100%" style="padding: 5px;">
                    <tr valign="top" class="form_titulo" style="background-color: #0069ae; height: 15px; text-transform: none;">
                        <td style="padding: 5px;">
                            Historial de Control Operativo
                        </td>
                    </tr>
                    <tr>
                        <td valign="top">
                            <table class="form_bandeja" style="width: 100%;">
                                <tr>
                                    <td style="width:55px">
                                        Periodo:
                                    </td>
                                    <td>
                                        <asp:Label runat="server" ID="LblPeriodo" Text=""></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr style="height: 180px;">
                        <td>
                            <div style="overflow: auto; width: 100%; height: 100%">
                                <asp:GridView ID="GrvDetalle" runat="server" AutoGenerateColumns="False" DataKeyNames="sUsuario"
                                    SkinID="GrillaConsulta" Width="97%"><%--OnRowDataBound="GrvDetalle_RowDataBound"--%>
                                    <Columns>
                                        <asp:BoundField DataField="sUsuario" HeaderText="Usuario">
                                            <ItemStyle Width="30%" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="sTipoProceso" HeaderText="Tipo Proceso">
                                            <ItemStyle Width="40%" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="dtFecha" HeaderText="Fecha">
                                            <ItemStyle Width="25%" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="iCantidadDias" HeaderText="D�as">
                                            <ItemStyle Width="5%" />
                                        </asp:BoundField>
                                    </Columns>
                                </asp:GridView>
                            </div>
                        </td>
                    </tr>
                    <tr valign="bottom">
                        <td valign="bottom">
                            <table style="width: 100%;">
                                <tr>
                                    <td style="text-align: right;">
                                        <asp:Button ID="btnCancelarVerDetalle" runat="server" CssClass="botonAceptar"
                                            Style="border-width: 0px;" OnClientClick="javascript: return fc_CerrarVerDetalle();" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </ContentTemplate>
            <Triggers>
                <ajax:AsyncPostBackTrigger ControlID="BtnVerHistorial" EventName="Click" />
            </Triggers>
        </ajax:UpdatePanel>
    </asp:Panel>
    
    <script language="javascript" type="text/javascript">

        var tiempo = 0;
        var seconds = 0;

        var myVar = setInterval(function () {
            clearInterval(myVar);
        }, 1000);

        var myTiempo = setTimeout(function () {
            clearInterval(myTiempo);
        }, 1);

        function myStopFunction() {
            clearInterval(myVar);
            seconds = '<%=GlobalEntity.Instancia.TiempoDuracionSesion%>' * 60;
            tiempo = '<%=GlobalEntity.Instancia.TiempoDuracionSesion%>' * 1000 * 60;

            CuentaTiempo(tiempo);
        }

        function CuentaTiempo(tiempo) {
            clearTimeout(myTiempo);
            myTiempo = setTimeout(function () {
                document.getElementById('<%=btn_cerrar.ClientID%>').click();
                //window.location = "../Seguridad/eFENIX_Login.aspx";
            }, tiempo);
        }



        function SessionExpireAlert(timeout) {
            seconds = timeout / 1000;
            document.getElementsByName("seconds").innerHTML = seconds;

            myvar = setInterval(function () {
                seconds--;
                document.getElementById("seconds").innerHTML = seconds;
            }, 1000);

            CuentaTiempo(timeout);
        };

        var goodexit = false;
        window.onbeforeunload = confirmRegisterExit;
        function confirmRegisterExit(evt) {
            if (!goodexit) {
                //return "Si vas abandonar el sistema haz click en Cerrar Sesion!!";
                document.getElementById('<%=btn_cerrar.ClientID%>').click();
            }
        }

        function OpenPopub() {
            Mostrar('block');
        }

        function Mostrar(estilo) {
            document.getElementById('<%=pnlDetalle.ClientID%>').style.left = (screen.width - 350) / 2 + 'px';
            document.getElementById('<%=pnlDetalle.ClientID%>').style.top = (screen.height - 350) / 2 + 'px';

            document.getElementById('<%=__PopPanel__.ClientID%>').style.width = screen.width + 'px';
            document.getElementById('<%=__PopPanel__.ClientID%>').style.height = screen.height + 'px';

            document.getElementById('<%=__PopPanel__.ClientID%>').style.display = estilo;
            document.getElementById('<%=pnlDetalle.ClientID%>').style.display = estilo;
        }

        function fc_CerrarVerDetalle() {
            Mostrar('none');
            return false;
        }
    </script>
</asp:Content>
<asp:Content ID="Content3" runat="server" contentplaceholderid="ContentCab">
    <span class="texto_titulo" >Tu Sesion Expirara en &nbsp; 
        <span class="texto_titulo" id="seconds"> </span>
        <span>&nbsp;seg.</span>
     </span>
     <asp:Button ID="btn_cerrar" runat="server" Text="cerrar" 
        onclick="btn_cerrar_Click"  Style="display: none;" />
</asp:Content>

