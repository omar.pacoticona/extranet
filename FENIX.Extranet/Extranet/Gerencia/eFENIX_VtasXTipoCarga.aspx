﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Extranet/Seguridad/eFENIX_Principal.master" AutoEventWireup="true" CodeFile="eFENIX_VtasXTipoCarga.aspx.cs" Inherits="Extranet_Gerencia_eFENIX_VtasXTipoCarga" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentCab" Runat="Server">
    <span class="texto_titulo" >Tu Sesion Expirara en &nbsp; 
        <span class="texto_titulo" id="seconds"> </span>
        <span>&nbsp;seg.</span>
     </span>
     <asp:Button ID="btn_cerrar" runat="server" Text="cerrar" 
        onclick="btn_cerrar_Click"  Style="display: none;" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="CntMaster" Runat="Server">
    <table cellspacing="0" cellpadding="0" style="width: 100%;">
        <tr>
            <td class="form_titulo" style="width: 85%">
                <asp:TextBox ID="LblTitulo" Text="Listado - Movimiento de Carga" ReadOnly="true" 
                    style="background-color: #0069ae; border: #0069ae; color: #FFFFFF; font-family: Calibri; font-size: 13px; font-weight: bold; text-transform: uppercase;" 
                    runat="server" Width="280px"></asp:TextBox>
            </td>
            <td align="right" class="form_titulo_verde" style="width: 15%">
                <asp:TextBox ID="LblTotal" Text="" style="background-color: #0069ae; color: #FFFFFF" ReadOnly="true" BorderColor="#0069ae" runat="server"></asp:TextBox>
            </td>
        </tr>
        
        <tr>
            <td valign="top" colspan="2">

                <table class="form_bandeja" style="margin-top: 5px; margin-bottom: 5px; width: 100%;">
                    <tr>
                        <td>
                            <table style="width: 100%">
                                <tr>
                                    <td>
                                        periodo</td>
                                    <td>
                            <asp:DropDownList ID="DplPeriodo" runat="server" >
                            </asp:DropDownList>
                                    </td>
                                    <td>
                                        mes inicial</td>
                                    <td>
                            <asp:DropDownList ID="DplMesIni" runat="server"  >
                            </asp:DropDownList>
                                    </td>
                                    <td>
                                        mes final</td>
                                    <td>
                            <asp:DropDownList ID="DplMesFin" runat="server" >
                            </asp:DropDownList>
                                    </td>
                                    <td>
                            <asp:ImageButton ID="ImgBtnBuscar" runat="server" ImageUrl="~/Imagenes/Formulario/btn_consultar_1.JPG" OnClick="ImgBtnBuscar_Click" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        negocio</td>
                                    <td>
                                        <asp:DropDownList ID="DplNegocio" runat="server" Width="240px" AppendDataBoundItems="true">
                                            <asp:ListItem Text="D.T.Contenedores (TEUS)" Value="1"></asp:ListItem>
                                            <asp:ListItem Text="D.T.C.Rodante (TN)" Value="2"></asp:ListItem>
                                            <asp:ListItem Text="D.T.C.Suelta  (TN)" Value="3"></asp:ListItem>                                            
                                        </asp:DropDownList>
                                    </td>
                                    <td>
                                        operacion</td>
                                    <td>
                            <asp:DropDownList ID="DplOperacion" runat="server" Width="120px" >
                            </asp:DropDownList>
                                    </td>
                                    <td>
                                        top</td>
                                    <td>
                            <asp:TextBox ID="txtTop" runat="server" Width="41px"></asp:TextBox>
                                    </td>
                                    <td>
                                        &nbsp;</td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    </table>
            </td>
        </tr>
        
        <tr valign="top" style="height: 100%">
            <td colspan="5">
                <ajax:UpdatePanel ID="upDocumentoOrigen" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div style="overflow: auto; width: 100%; height: 100%">
                            <asp:GridView ID="GrvListado" runat="server" AutoGenerateColumns="False" DataKeyNames="iItem, iIdCliente, dPorc"
                                SkinID="GrillaConsultaUnColor" Width="100%" onrowdatabound="GrvListado_RowDataBound">
                                <Columns>                                    
                                    <asp:BoundField DataField="iItem" HeaderText="Item" ItemStyle-HorizontalAlign="Center">
                                        <ItemStyle Width="4%" />
                                    </asp:BoundField>
                                     <asp:BoundField DataField="sCliente" HeaderText="Consignatario" ItemStyle-HorizontalAlign="Left">
                                        <ItemStyle Width="60%" />
                                    </asp:BoundField>
                                     
                                    <asp:BoundField DataField="iMes1" HeaderText="TEUS" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:N}">
                                        <ItemStyle Width="12%" />
                                    </asp:BoundField>
                                    
                                    <asp:BoundField DataField="iMes2" HeaderText="TEUS" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:N}">
                                        <ItemStyle Width="12%" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="dPorc" HeaderText="% Excedido" ItemStyle-HorizontalAlign="Center">
                                        <ItemStyle Width="12%" />
                                    </asp:BoundField>

                                </Columns>
                            </asp:GridView>
                        </div>
                    </ContentTemplate>
                    <Triggers>
                        <ajax:AsyncPostBackTrigger ControlID="GrvListado" EventName="SelectedIndexChanged" />                        
                        <ajax:AsyncPostBackTrigger ControlID="ImgBtnBuscar" EventName="Click" />
<asp:AsyncPostBackTrigger ControlID="GrvListado" EventName="SelectedIndexChanged"></asp:AsyncPostBackTrigger>
<asp:AsyncPostBackTrigger ControlID="ImgBtnBuscar" EventName="Click"></asp:AsyncPostBackTrigger>
<asp:AsyncPostBackTrigger ControlID="GrvListado" EventName="SelectedIndexChanged"></asp:AsyncPostBackTrigger>
<asp:AsyncPostBackTrigger ControlID="ImgBtnBuscar" EventName="Click"></asp:AsyncPostBackTrigger>
                    </Triggers>
                </ajax:UpdatePanel>
            </td>
        </tr>
        
        
    </table>


    <script language="javascript" type="text/javascript">
        var tiempo = 0;
        var seconds = 0;

        var myVar = setInterval(function () {
            clearInterval(myVar);
        }, 1000);

        var myTiempo = setTimeout(function () {
            clearInterval(myTiempo);
        }, 1);

        function myStopFunction() {
            clearInterval(myVar);
            seconds = '<%=GlobalEntity.Instancia.TiempoDuracionSesion%>' * 60;
            tiempo = '<%=GlobalEntity.Instancia.TiempoDuracionSesion%>' * 1000 * 60;

            CuentaTiempo(tiempo);
        }

        function CuentaTiempo(tiempo) {
            clearTimeout(myTiempo);
            myTiempo = setTimeout(function () {
                document.getElementById('<%=btn_cerrar.ClientID%>').click();
                //window.location = "../Seguridad/eFENIX_Login.aspx";
            }, tiempo);
        }



        function SessionExpireAlert(timeout) {
            seconds = timeout / 1000;
            document.getElementsByName("seconds").innerHTML = seconds;

            myvar = setInterval(function () {
                seconds--;
                document.getElementById("seconds").innerHTML = seconds;
            }, 1000);

            CuentaTiempo(timeout);
        };


        var goodexit = false;
        window.onbeforeunload = confirmRegisterExit;
        function confirmRegisterExit(evt) {
            if (!goodexit) {
                //return "Si vas abandonar el sistema haz click en Cerrar Sesion!!";
                document.getElementById('<%=btn_cerrar.ClientID%>').click();
            }
        }


//        function AbrirPagina(IdCliente, IdVendedor) {
//            iOperacion = document.getElementById('<%=DplOperacion.ClientID%>').value;
//            if (iOperacion == 0) {
//                sPeriodo = document.getElementById('<%=DplPeriodo.ClientID%>').value;
//                rIni = '<%=ViewState["MesRangoIni"]%>';
//                rFin = '<%=ViewState["MesRangoFin"]%>';
//                sMesIni = document.getElementById('<%=DplMesIni.ClientID%>').value;
//                sMesFin = document.getElementById('<%=DplMesFin.ClientID%>').value;
//                iLinea = document.getElementById('<%=DplNegocio.ClientID%>').value;
//                window.location.href = "eFENIX_VtasXLinea.aspx?Periodo=" + sPeriodo + "&rIni=" + rIni + "&rFin=" + rFin + "&mIni=" + sMesIni + "&mFin=" + sMesFin + "&IdCliente=" + IdCliente + "&IdVend=" + IdVendedor + "&Linea=" + iLinea;
//            }
//            else {
//                alert('Tipo de Operacion debe Ser TODOS');
//                return false;
//            }
//        }

        function SoloEnterosDecimales(e) {
            var tecla = document.all ? tecla = e.keyCode : tecla = e.which;
            return ((tecla > 47 && tecla < 58) || tecla == 44 || tecla == 46 || tecla == 45);
        }

//        function Retornar() {
//            sPeriodo = document.getElementById('<%=DplPeriodo.ClientID%>').value;
//            rIni = '<%=ViewState["MesRangoIni"]%>';
//            rFin = '<%=ViewState["MesRangoFin"]%>';
//            sMIni = document.getElementById('<%=DplMesIni.ClientID%>').value
//            sMFin = document.getElementById('<%=DplMesFin.ClientID%>').value
//            window.open("eFENIX_VtasXLinea.aspx?Periodo=" + sPeriodo + "&rIni=" + rIni + "&rFin=" + rFin + "&mIni=" + sMIni + "&mFin=" + sMFin, "_self");
//            return false;
//        }

    </script>
</asp:Content>

