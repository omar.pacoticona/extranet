﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Extranet/Seguridad/eFENIX_Principal.master" AutoEventWireup="true" CodeFile="eFENIX_RentabXServicio.aspx.cs" Inherits="Extranet_Gerencia_eFENIX_VtasXServicio" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentCab" Runat="Server">
    <span class="texto_titulo" >Tu Sesion Expirara en &nbsp; 
        <span class="texto_titulo" id="seconds"> </span>
        <span>&nbsp;seg.</span>
     </span>
     <asp:Button ID="btn_cerrar" runat="server" Text="cerrar" 
        onclick="btn_cerrar_Click"  Style="display: none;" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="CntMaster" Runat="Server">
    <table cellspacing="0" cellpadding="0" style="width: 100%;">
        <tr>
            <td class="form_titulo" style="width: 85%">
                <asp:TextBox ID="LblTitulo" Text="Rentabilidad por Servicio" ReadOnly="true" 
                    style="background-color: #0069ae; border: #0069ae; color: #FFFFFF; font-family: Calibri; font-size: 13px; font-weight: bold; text-transform: uppercase;" 
                    runat="server" Width="231px"></asp:TextBox>
            </td>
            <td align="right" class="form_titulo_verde" style="width: 15%">
                <asp:TextBox ID="LblTotal" Text="" style="background-color: #0069ae; color: #FFFFFF" ReadOnly="true" BorderColor="#0069ae" runat="server"></asp:TextBox>
            </td>
        </tr>
        
        <tr>
            <td valign="top" colspan="2">

                <table class="form_bandeja" style="margin-top: 5px; margin-bottom: 5px; width: 100%;">
                    <tr>
                        <td>
                            <table style="width: 100%">
                                <tr>
                                    <td>
                                        peeriodo</td>
                                    <td>
                            <asp:DropDownList ID="DplPeriodo" runat="server" >
                            </asp:DropDownList>
                                    </td>
                                    <td>
                                        mes inicial</td>
                                    <td>
                            <asp:DropDownList ID="DplMesIni" runat="server"  >
                            </asp:DropDownList>
                                    </td>
                                    <td>
                                        mes final</td>
                                    <td>
                            <asp:DropDownList ID="DplMesFin" runat="server" >
                            </asp:DropDownList>
                                    </td>
                                    <td>
                            <asp:ImageButton ID="ImgBtnBuscar" runat="server" ImageUrl="~/Imagenes/Formulario/btn_consultar_1.JPG" OnClick="ImgBtnBuscar_Click" />
                                    </td>
                                    <td>
                                <asp:ImageButton ID="ImgBtnBack" runat="server" 
                                ImageUrl="~/Imagenes/botones_accion/btn_salir.JPG" 
                                OnClientClick="return Retornar();" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        negocio&nbsp;</td>
                                    <td>
                                        <asp:DropDownList ID="DplNegocio" runat="server" Width="200px" AppendDataBoundItems="true">
                                            <asp:ListItem Text="Todos" Value="0"></asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                    <td>
                                        cliente</td>
                                    <td>
                            <asp:DropDownList ID="DplCliente" runat="server" Width="180px" AppendDataBoundItems="true">
                                <asp:ListItem Text="Todos" Value="0"></asp:ListItem>
                            </asp:DropDownList>
                                    </td>
                                    <td>
                                        TOP</td>
                                    <td>
                            <asp:TextBox ID="txtTop" runat="server" Width="41px"></asp:TextBox>
                                    </td>
                                    <td>
                                        &nbsp;</td>
                                    <td>
                                        &nbsp;</td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    </table>
            </td>
        </tr>
        
        <tr valign="top" style="height: 100%">
            <td colspan="5">
                <ajax:UpdatePanel ID="upDocumentoOrigen" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div style="overflow: auto; width: 100%; height: 100%">
                            <asp:GridView ID="GrvListado" runat="server" AutoGenerateColumns="False" DataKeyNames="iItem,iIdGrupo, iIdServicio"
                                SkinID="GrillaConsultaUnColor" Width="100%"  onrowdatabound="GrvListado_RowDataBound">
                                <Columns>                                    
                                    <asp:BoundField DataField="iItem" HeaderText="Item" ItemStyle-HorizontalAlign="Center">
                                        <ItemStyle Width="10px" />
                                    </asp:BoundField>
                                     <asp:BoundField DataField="sServicio" HeaderText="Servicio" ItemStyle-HorizontalAlign="Left">
                                        <ItemStyle Width="300px" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="dCostoOpe" HeaderText="Costo Operativo US$" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:N}">
                                        <ItemStyle Width="12%" />
                                    </asp:BoundField>                                    
                                    <asp:BoundField DataField="dCostoAdm" HeaderText="Costo Administr.US$" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:N}" >
                                        <ItemStyle Width="12%" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="dVentas" HeaderText="Ventas US$" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:N}">
                                        <ItemStyle Width="80px" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="dUtilidad" HeaderText="Utilidad US$" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:N}">
                                        <ItemStyle Width="12%" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="dVtaEsperada" HeaderText="Vta.Esperada US$" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:N}">
                                        <ItemStyle Width="12%" />
                                    </asp:BoundField>               
                                </Columns>
                            </asp:GridView>
                        </div>
                    </ContentTemplate>
                    <Triggers>
                        <ajax:AsyncPostBackTrigger ControlID="GrvListado" EventName="SelectedIndexChanged" />                        
                        <ajax:AsyncPostBackTrigger ControlID="ImgBtnBuscar" EventName="Click" />
<asp:AsyncPostBackTrigger ControlID="GrvListado" EventName="SelectedIndexChanged"></asp:AsyncPostBackTrigger>
<asp:AsyncPostBackTrigger ControlID="ImgBtnBuscar" EventName="Click"></asp:AsyncPostBackTrigger>
<asp:AsyncPostBackTrigger ControlID="GrvListado" EventName="SelectedIndexChanged"></asp:AsyncPostBackTrigger>
<asp:AsyncPostBackTrigger ControlID="ImgBtnBuscar" EventName="Click"></asp:AsyncPostBackTrigger>
                    </Triggers>
                </ajax:UpdatePanel>
            </td>
        </tr>
        
        
    </table>


    <script language="javascript" type="text/javascript">
        var tiempo = 0;
        var seconds = 0;

        var myVar = setInterval(function () {
            clearInterval(myVar);
        }, 1000);

        var myTiempo = setTimeout(function () {
            clearInterval(myTiempo);
        }, 1);

        function myStopFunction() {
            clearInterval(myVar);
            seconds = '<%=GlobalEntity.Instancia.TiempoDuracionSesion%>' * 60;
            tiempo = '<%=GlobalEntity.Instancia.TiempoDuracionSesion%>' * 1000 * 60;

            CuentaTiempo(tiempo);
        }

        function CuentaTiempo(tiempo) {
            clearTimeout(myTiempo);
            myTiempo = setTimeout(function () {
                document.getElementById('<%=btn_cerrar.ClientID%>').click();
                //window.location = "../Seguridad/eFENIX_Login.aspx";
            }, tiempo);
        }



        function SessionExpireAlert(timeout) {
            seconds = timeout / 1000;
            document.getElementsByName("seconds").innerHTML = seconds;

            myvar = setInterval(function () {
                seconds--;
                document.getElementById("seconds").innerHTML = seconds;
            }, 1000);

            CuentaTiempo(timeout);
        };


        var goodexit = false;
        window.onbeforeunload = confirmRegisterExit;
        function confirmRegisterExit(evt) {
            if (!goodexit) {
                //return "Si vas abandonar el sistema haz click en Cerrar Sesion!!";
                document.getElementById('<%=btn_cerrar.ClientID%>').click();
            }
        }


       

        function SoloEnterosDecimales(e) {
            var tecla = document.all ? tecla = e.keyCode : tecla = e.which;
            return ((tecla > 47 && tecla < 58) || tecla == 44 || tecla == 46 || tecla == 45);
        }

        function Retornar() {
            sPeriodo = document.getElementById('<%=DplPeriodo.ClientID%>').value;
            rIni = '<%=ViewState["MesRangoIni"]%>';
            rFin = '<%=ViewState["MesRangoFin"]%>';
            sVentanaAnt = '<%=ViewState["VentanaAnt"]%>';
            sVentana = '<%=ViewState["Ventana"]%>';
            iTop = '<%=ViewState["Top"]%>';
            sInicio = '<%=ViewState["Inicio"]%>';
            sMIni = document.getElementById('<%=DplMesIni.ClientID%>').value
            sMFin = document.getElementById('<%=DplMesFin.ClientID%>').value
            sCliente = document.getElementById('<%=DplCliente.ClientID%>').value            
            sNegocio = document.getElementById('<%=DplNegocio.ClientID%>').value
            if (sVentana == "L") {
                window.open("eFENIX_RentabXLinea.aspx?Periodo=" + sPeriodo + "&rIni=" + rIni + "&rFin=" + rFin + "&mIni=" + sMIni + "&mFin=" + sMFin + "&IdCliente=" + sCliente + "&Top=" + iTop + "&Inicio=" + sInicio + "&VentanaAnt=" + sVentanaAnt + "&Ventana=" + sVentana, "_self");            
            } else if (sVentana == "C") {
                window.open("eFENIX_RentabXCliente.aspx?Periodo=" + sPeriodo + "&rIni=" + rIni + "&rFin=" + rFin + "&mIni=" + sMIni + "&mFin=" + sMFin + "&Linea=" + sNegocio + "&Top=" + iTop + "&Inicio=" +sInicio + "&VentanaAnt=" + sVentanaAnt , "_self");
            } else if (sVentana == "P") {
                window.open("eFENIX_RentabXPeriodo.aspx?Periodo=" + sPeriodo + "&rIni=" + rIni + "&rFin=" + rFin, "_self");
            }
            return false;
        }

    </script>
</asp:Content>

