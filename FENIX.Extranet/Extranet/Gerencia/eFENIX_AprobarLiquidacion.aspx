﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="eFENIX_AprobarLiquidacion.aspx.cs" Inherits="Extranet_Gerencia_eFENIX_AprobarLiquidacion" %>

<!DOCTYPE  html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
      <link href="../../Script/Hojas_Estilo/eFenix.css" rel="stylesheet" type="text/css" />
    <title>Aprobacion de Liquidaciones</title>
</head>
<body>
    
	<%--<form id="formulario1" style="align-content: center; height: 100%; width: 100%;" runat="server">
        

		
		<div style="background: white;">
			<table>
				<tbody>
					<tr>
						<td>
							<img src="http://200.4.192.227/Imagenes/logo.jpg"  alt="logo_fargoline" style="margin-left:450px;width: 350px;;height: 100px;"/>
						</td>						
					</tr>
				</tbody>		
			</table>
		</div>
			
        <br />
        <br />
        <br />

		
			<table style="width: 100%">
					<tr>
						<td style="background: black; width: 100%; height:5px;" >
								
						</td>
					</tr>
			</table>				

		
			

       <form id="formulario4" style="background: red; width: 100%; height:240px;">
          
				<table style="background: #243F90; width: 500px;margin-left:480px;" runat="server">            

					<tr>
						<td>
                            <h2><asp:label id="Label12" runat="server" Text="APROBAR LIQUIDACIÓN" ForeColor="White"></asp:label></h2>	
							<asp:label id="lblxd" runat="server" Text="Numero de Liquidación :" ForeColor="White"></asp:label>	
                            <asp:label id="lblNumeroLiquidacion" runat="server" Text="125412" ForeColor="White"></asp:label>
                            <br />
                            <asp:label id="Label3" runat="server" Text="Comisionista :" ForeColor="White"></asp:label>
                             <asp:label id="lblComisionista" runat="server" Text="2020-12-12" ForeColor="White"></asp:label>
                            <br /> 
                            <asp:label id="Label2" runat="server" Text="Importe :" ForeColor="White"></asp:label>
                             <asp:label id="lblImporte" runat="server" Text="3000" ForeColor="White"></asp:label>
                             <br />
                            <asp:label id="Label6" runat="server" Text="Ejecutivo :" ForeColor="White"></asp:label>
                             <asp:label id="lblEjecutivo" runat="server" Text="2020-12-12" ForeColor="White"></asp:label>
                             <br />
                            <asp:label id="Label8" runat="server" Text="Fecha Emitida :" ForeColor="White"></asp:label>
                             <asp:label id="lblFechaEmitida" runat="server" Text="2020-12-12" ForeColor="White"></asp:label>
                             <br /> <br />
                            <asp:label id="Label10" runat="server" Text=""></asp:label>
                             <asp:label id="Label11" runat="server" Text=""></asp:label>
							 <br />
                            <asp:RadioButtonList ID="RblAprobacion" runat="server" RepeatColumns="2">
                                        <asp:ListItem Value="S">SI</asp:ListItem>
                                        <asp:ListItem Value="N">NO</asp:ListItem>
                            </asp:RadioButtonList>  
						</td>                        
					</tr>                 
                     <tr>                    
                    <td align="center" valign="middle">
                              <asp:Button ID="btnAceptar" runat="server" CssClass="botonAceptar" 
                                    style="border-width:0px;"                                     
                                    OnClick="btnAceptar_Click" /> 
                              
                              <asp:Button ID="btnCancelar" runat="server" CssClass="botonCancelar" 
                                    style="border-width:0px;" 
                                    OnClientClick="return cerrarpagina();"/> 
                            </td>                                       
                </tr>
				</table>	
		</form>
		
          <table style="width: 100%; margin-bottom:150px;">
					<tr>
						<td style="background: black; width: 100%; height:5px;" >	
                           
						</td>

					</tr>
              
				</table>	

            
	</form>--%>

      <form id="form1" runat="server">
        <ajax:ScriptManager ID="scmLogin"  runat="server">
        </ajax:ScriptManager>

        <div style="height: 20%; width: 100%;padding-top:30px;">
        
            <table cellpadding="0" cellspacing="0" border="0px" align="center" width="100%" >
                <tr>                   
                    <td style="text-align:center;" >
                        <img src="http://200.4.192.227/Imagenes/logo.jpg" />
                    </td>                    
                </tr>
                <tr style="height:195px;background-image : url('http://200.4.192.227/Imagenes/fondo_eFenix.jpg');">                    
                    <td>
                        <table cellpadding="0" cellspacing="1" style="width:100%;text-align:center;font-family:Calibri; color: #FFFFFF; height:400px;">
                            <tr>
                                <td align="center" valign="middle" colspan="2">
                                  <%-- <h2 style="color:black;" id="titulo" title="">Aprobar Liquidaciones</h2>--%>
                                    <h2 style="color:black;" id="titulo" title=""><asp:label  runat="server" id="lblAprobar" Text="Aprobar Liquidaciones"></asp:label></h2>
                                </td>
                            </tr>
                            
                            <tr>
                                <td style="text-align:right;width:50%;">
                                   <asp:label id="Label1" runat="server" Text="Numero de Liquidación :" ForeColor="Black"></asp:label>
                                </td>
                                <td style="padding-left:10px; text-align:left;">
                                   <asp:label id="lblNumeroLiquidacion" runat="server" Text="125412" ForeColor="Black"></asp:label>
                                </td>
                            </tr>
                            
                            <tr>
                                <td style="text-align:right;">
                                     <asp:label id="Label2" runat="server" Text="Comisionista :" ForeColor="Black"></asp:label> </td>
                                <td style="padding-left:10px; text-align:left;">
                                   <asp:label id="lblComisionista" runat="server" Text="2020-12-12" ForeColor="Black"></asp:label>
                                </td>
                            </tr>
                            
                            <tr>
                                <td style="text-align:right;">
                                       <asp:label id="Label3" runat="server" Text="Importe :" ForeColor="Black"></asp:label></td>
                                <td style="padding-left:10px; text-align:left;">
                                    <asp:label id="Label7" runat="server" Text="S/." ForeColor="Black"></asp:label>&nbsp;
                                   <asp:label id="lblImporte" runat="server" Text="3000" ForeColor="Black"></asp:label>
                                </td>
                            </tr>
                            
                            <tr>
                                <td style="text-align:right;">
                                   <asp:label id="Label4" runat="server" Text="Ejecutivo :" ForeColor="Black"></asp:label>
                                </td>
                                <td style="padding-left:10px; text-align:left;">
                                    <asp:label id="lblEjecutivo" runat="server" Text="2020-12-12" ForeColor="Black"></asp:label>
                                </td>
                            </tr>
                           
                            <tr>
                                <td style="text-align:right;">
                                   <asp:label id="Label5" runat="server" Text="Estado" ForeColor="Black"></asp:label> </td>
                                <td style="padding-left:10px; text-align:left;">
                                  <asp:label id="lblEstado" runat="server" Text="2020-12-12" ForeColor="Black"></asp:label>
                                </td>
                            </tr>
                           
                            <tr>
                                <td style="text-align:right;">
                                    <asp:label id="Label6" runat="server" Text="¿Aprobar la Liquidación?" ForeColor="Black"></asp:label></td>
                                <td style="padding-left:10px;">                                   
                                    <asp:RadioButtonList ID="RblAprobacion" runat="server" RepeatColumns="2" ForeColor="Black">
                                        <asp:ListItem Value="S">SI</asp:ListItem>
                                        <asp:ListItem Value="N">NO</asp:ListItem>
                                    </asp:RadioButtonList>                                    
                                </td>
                            </tr>
                            </table>
                    
                    </td>                    
                </tr>
                <tr>                    
                    <td align="center" valign="middle">
                              <asp:Button ID="btnAceptar" runat="server" CssClass="botonAceptar" 
                                    style="border-width:0px;"                                     
                                    OnClick="btnAceptar_Click" /> 
                              
                              <asp:Button ID="btnCancelar" runat="server" CssClass="botonCancelar" 
                                    style="border-width:0px;" 
                                    OnClientClick="return cerrarpagina();"/> 
                            </td>                                       
                </tr>
            </table> 
            
                                                           
        </div>
    </form>
</body>
</html>
