﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using FENIX.BusinessEntity;
using FENIX.BusinessLogic;


public partial class Extranet_Gerencia_eFENIX_Presupuesto : System.Web.UI.Page
{   

    protected void Page_Load(object sender, EventArgs e)
    {
        String tiempo = (Convert.ToInt32(GlobalEntity.Instancia.TiempoDuracionSesion.ToString()) * 1000 * 60).ToString();
        Session["Reset"] = true;

        if (!Page.IsPostBack)
        {
            ListarDropDowList();
            DplPeriodo.SelectedValue = DateTime.Now.Year.ToString();


            pMuestraDetalleNegocio(DplPeriodo.SelectedValue);
            pMuestraDetalleVendedor(DplPeriodo.SelectedValue);

            if (GrvListado.Rows.Count == 0)
            {
                List<BE_Presupuesto> lsBE_Presupuesto = new List<BE_Presupuesto>();
                BE_Presupuesto oBE_Presupuesto = new BE_Presupuesto();
                lsBE_Presupuesto.Add(oBE_Presupuesto);
                GrvListado.DataSource = lsBE_Presupuesto;
                GrvListado.DataBind();
            }

            if (GrvListado2.Rows.Count == 0)
            {
                List<BE_Presupuesto> lsBE_Presupuesto = new List<BE_Presupuesto>();
                BE_Presupuesto oBE_Presupuesto = new BE_Presupuesto();
                lsBE_Presupuesto.Add(oBE_Presupuesto);
                GrvListado2.DataSource = lsBE_Presupuesto;
                GrvListado2.DataBind();
            }
            ScriptManager.RegisterStartupScript(this, GetType(), "mensaje", "SessionExpireAlert(" + tiempo + ");", true);
        }
        else
        {           
            ScriptManager.RegisterStartupScript(this, GetType(), "mensaje", "myStopFunction();", true);
        }
    }


     #region "Método Controles"
        protected void ListarDropDowList()
        {
            try
            {
                DplPeriodo.Items.Clear();
                for (int i = 2013; i <= DateTime.Now.Year; i++)
                {
                    DplPeriodo.Items.Add(new ListItem(i.ToString(), i.ToString()));
                }
                DplPeriodo.SelectedIndex = 0;
                
            }
            catch (Exception e)
            {

            }
        }
     #endregion


        private void SCA_MsgInformacion(string strError)
        {
            MensajeScript(strError);
        }

        private void MensajeScript(string SMS)
        {
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "", "<script>alert('" + SMS + "');</script>", false);
        }

        protected void btn_cerrar_Click(object sender, EventArgs e)
        {
            if (GlobalEntity.Instancia.CerrarExtranet == "S")
            {
                BL_Usuario oBL_Usuario = new BL_Usuario();
                oBL_Usuario.GrabaInicioSesion(GlobalEntity.Instancia.Usuario, "0");
                Session.Abandon();
                FormsAuthentication.SignOut();
                Response.Redirect("../../eFENIX_Login.aspx");
                Response.End();
            }
        }

        protected void GrvListado_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            List<BE_Tabla> olst_TablaTipo = new List<BE_Tabla>();
            BL_Presupuesto oBL_Presupuesto = new BL_Presupuesto();
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataKey dataKey = this.GrvListado.DataKeys[e.Row.RowIndex];
                BE_Presupuesto oitem = (e.Row.DataItem as BE_Presupuesto);

                if (oitem.iIdLineaNegocio == 0 ) 
                    e.Row.Visible = false;


                if (oitem.iIdLineaNegocio == -1)  //Total
                {
                    e.Row.Cells[14].Visible = false;
                    e.Row.Cells[15].Visible = false;
                    e.Row.Cells[1].Font.Size = 10;

                    for (int c = 1; c < e.Row.Cells.Count; c++)
                    {
                        e.Row.Cells[c].BackColor = System.Drawing.Color.LightGray;
                        e.Row.Cells[c].Font.Bold = true;
                        
                    }
                }
            }
            else if (e.Row.RowType == DataControlRowType.Footer)
            {
                DropDownList DplLineaNegocioNew = e.Row.FindControl("DplLineaNegocioNew") as DropDownList;

                DplLineaNegocioNew.DataSource = oBL_Presupuesto.ListarLineaNegocio();
                DplLineaNegocioNew.DataValueField = "iIdValor";
                DplLineaNegocioNew.DataTextField = "sDescripcion";
                DplLineaNegocioNew.DataBind();                
                DplLineaNegocioNew.SelectedIndex = 0;


                for (int x = 1; x <= 12; x++)
                {
                    string xText = "TxtMes" + x.ToString().Trim() + "New";
                    (e.Row.FindControl(xText) as TextBox).Text = "0";
                }
            }
        }


        protected void GrvListado_RowEditing(object sender, GridViewEditEventArgs e)
        {        
            GrvListado.EditIndex = e.NewEditIndex ; //Cambiamos a modo editable
        
            GrvListado.Columns[15].Visible = false;
            GrvListado.Columns[14].HeaderStyle.Width = 60;

            pMuestraDetalleNegocio( DplPeriodo.SelectedValue);
            GrvListado.FooterRow.Visible = false;

            String iGrupoNegocio = (GrvListado.Rows[e.NewEditIndex].FindControl("txtIdGrupo") as TextBox).Text;
        
            DropDownList DplLineaNegocioEdit = (GrvListado.Rows[e.NewEditIndex].FindControl("DplLineaNegocioEdit") as DropDownList);

            if (DplLineaNegocioEdit != null)
            {
                BL_Presupuesto oBL_Presupuesto = new BL_Presupuesto();
                DplLineaNegocioEdit.DataSource = oBL_Presupuesto.ListarLineaNegocio();
                DplLineaNegocioEdit.DataValueField = "iIdValor";
                DplLineaNegocioEdit.DataTextField = "sDescripcion";
                DplLineaNegocioEdit.DataBind();  
            }
        
            DplLineaNegocioEdit.SelectedValue = iGrupoNegocio;
        }

        protected void pMuestraDetalleNegocio(string strPeriodo)
        {
            BE_Presupuesto oBE_Presupuesto = new BE_Presupuesto();
            BL_Presupuesto oBL_Presupuesto = new BL_Presupuesto();

            List<BE_Presupuesto> lista = oBL_Presupuesto.ListarPresupuesto(strPeriodo,"1");

            Int32 decMes1 = 0;
            Int32 decMes2 = 0;
            Int32 decMes3 = 0;
            Int32 decMes4 = 0;
            Int32 decMes5 = 0;
            Int32 decMes6 = 0;
            Int32 decMes7 = 0;
            Int32 decMes8 = 0;
            Int32 decMes9 = 0;
            Int32 decMes10 = 0;
            Int32 decMes11 = 0;
            Int32 decMes12 = 0;

            for (int i = 0; i <= lista.Count-1; i++)
            {                
                decMes1 += Convert.ToInt32(lista[i].iMes1.ToString());
                decMes2 += Convert.ToInt32(lista[i].iMes2.ToString());
                decMes3 += Convert.ToInt32(lista[i].iMes3.ToString());
                decMes4 += Convert.ToInt32(lista[i].iMes4.ToString());
                decMes5 += Convert.ToInt32(lista[i].iMes5.ToString());
                decMes6 += Convert.ToInt32(lista[i].iMes6.ToString());
                decMes7 += Convert.ToInt32(lista[i].iMes7.ToString());
                decMes8 += Convert.ToInt32(lista[i].iMes8.ToString());
                decMes9 += Convert.ToInt32(lista[i].iMes9.ToString());
                decMes10 += Convert.ToInt32(lista[i].iMes10.ToString());
                decMes11 += Convert.ToInt32(lista[i].iMes11.ToString());
                decMes12 += Convert.ToInt32(lista[i].iMes12.ToString());
            }

            oBE_Presupuesto.iIdPresupuesto = 0;
            oBE_Presupuesto.iIdLineaNegocio = -1;
            oBE_Presupuesto.sNegocio = "TOTALES (MILES US$)";
            oBE_Presupuesto.iMes1 = decMes1;
            oBE_Presupuesto.iMes2 = decMes2;
            oBE_Presupuesto.iMes3 = decMes3;
            oBE_Presupuesto.iMes4 = decMes4;
            oBE_Presupuesto.iMes5 = decMes5;
            oBE_Presupuesto.iMes6 = decMes6;
            oBE_Presupuesto.iMes7 = decMes7;
            oBE_Presupuesto.iMes8 = decMes8;
            oBE_Presupuesto.iMes9 = decMes9;
            oBE_Presupuesto.iMes10 = decMes10;
            oBE_Presupuesto.iMes11 = decMes11;
            oBE_Presupuesto.iMes12 = decMes12;


            lista.Add(oBE_Presupuesto);

            GrvListado.DataSource = lista;
            GrvListado.DataBind();
        }

        protected void btnAgregar_Click( Object sender, ImageClickEventArgs e)
        {

            BE_Presupuesto oBE_Presupuesto = new BE_Presupuesto();
            BL_Presupuesto oBL_Presupuesto = new BL_Presupuesto();

            GridViewRow filagrilla = GrvListado.FooterRow;

            Int32 dplNegocio = Convert.ToInt32( (filagrilla.FindControl("DplLineaNegocioNew") as DropDownList).SelectedValue );

            foreach (GridViewRow fila in GrvListado.Rows)
            {
                if (GrvListado.DataKeys[fila.RowIndex].Values["iIdLineaNegocio"].ToString() == dplNegocio.ToString() )
                {                    
                    SCA_MsgInformacion("El Negocio Ya Se Encuentra Ingresado");
                    return;
                }
            }

            
            for (int x = 1; x <= 12; x++)
            {
                string xText = "TxtMes" + x.ToString().Trim() + "New";
                Boolean bIsNumerico = Microsoft.VisualBasic.Information.IsNumeric((filagrilla.FindControl(xText) as TextBox).Text);
                if (bIsNumerico == false)
                    (filagrilla.FindControl(xText) as TextBox).Text = "0";
                else
                    (filagrilla.FindControl(xText) as TextBox).Text = Convert.ToDouble((filagrilla.FindControl(xText) as TextBox).Text).ToString();
            }

            
            Int32 decMes1 = Convert.ToInt32((filagrilla.FindControl("TxtMes1New") as TextBox).Text);
            Int32 decMes2 = Convert.ToInt32((filagrilla.FindControl("TxtMes2New") as TextBox).Text);
            Int32 decMes3 = Convert.ToInt32((filagrilla.FindControl("TxtMes3New") as TextBox).Text);
            Int32 decMes4 = Convert.ToInt32((filagrilla.FindControl("TxtMes4New") as TextBox).Text);
            Int32 decMes5 = Convert.ToInt32((filagrilla.FindControl("TxtMes5New") as TextBox).Text);
            Int32 decMes6 = Convert.ToInt32((filagrilla.FindControl("TxtMes6New") as TextBox).Text);
            Int32 decMes7 = Convert.ToInt32((filagrilla.FindControl("TxtMes7New") as TextBox).Text);
            Int32 decMes8 = Convert.ToInt32((filagrilla.FindControl("TxtMes8New") as TextBox).Text);
            Int32 decMes9 = Convert.ToInt32((filagrilla.FindControl("TxtMes9New") as TextBox).Text);
            Int32 decMes10 = Convert.ToInt32((filagrilla.FindControl("TxtMes10New") as TextBox).Text);
            Int32 decMes11 = Convert.ToInt32((filagrilla.FindControl("TxtMes11New") as TextBox).Text);
            Int32 decMes12 = Convert.ToInt32((filagrilla.FindControl("TxtMes12New") as TextBox).Text);

            if ( decMes1+decMes2+decMes3+decMes4+decMes5+decMes6+decMes7+decMes8+decMes9+decMes10+decMes11+decMes12 == 0 )
                SCA_MsgInformacion("Ingresar Montos"); 
            else
            {
                oBE_Presupuesto.iIdPresupuesto = 0;
                oBE_Presupuesto.sPeriodo = DplPeriodo.SelectedValue;
                oBE_Presupuesto.iIdLineaNegocio = dplNegocio ;                
                oBE_Presupuesto.iMes1 = decMes1;
                oBE_Presupuesto.iMes2 = decMes2;
                oBE_Presupuesto.iMes3 = decMes3;
                oBE_Presupuesto.iMes4 = decMes4;
                oBE_Presupuesto.iMes5 = decMes5;
                oBE_Presupuesto.iMes6 = decMes6;
                oBE_Presupuesto.iMes7 = decMes7;
                oBE_Presupuesto.iMes8 = decMes8;
                oBE_Presupuesto.iMes9 = decMes9;
                oBE_Presupuesto.iMes10 = decMes10;
                oBE_Presupuesto.iMes11 = decMes11;
                oBE_Presupuesto.iMes12 = decMes12;
                oBE_Presupuesto.sUsuario = GlobalEntity.Instancia.Usuario;
                oBE_Presupuesto.sNombrePC = GlobalEntity.Instancia.NombrePc;

                String[] sResultado = null;
                Int32 iCodigo = 0;
                String vl_sMessage = String.Empty;

                sResultado = oBL_Presupuesto.InsertarPresupuesto(oBE_Presupuesto).Split('|');

                for (int i = 0; i < sResultado.Length; i++)
                {
                    if (i == 0)
                    {
                        iCodigo = Convert.ToInt32(sResultado[i]);
                    }
                    else if (i == 1)
                    {
                        vl_sMessage += sResultado[i];
                    }
                }

                if (iCodigo < 0)
                    SCA_MsgInformacion(vl_sMessage);  


                pMuestraDetalleNegocio(DplPeriodo.SelectedValue); 
            }

        }

        
        protected void ImgBtnBuscar_Click(object sender, ImageClickEventArgs e)
        {
            pMuestraDetalleNegocio(DplPeriodo.SelectedValue);
            pMuestraDetalleVendedor(DplPeriodo.SelectedValue);
        }

        protected void GrvListado_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            BE_Presupuesto oBE_Presupuesto = new BE_Presupuesto();
            BL_Presupuesto oBL_Presupuesto = new BL_Presupuesto();

            GridViewRow filagrilla = GrvListado.Rows[e.RowIndex];

            decimal decTotal = 0;
            for (int x = 1; x <= 12; x++)
            {
                string xText = "TxtMes" + x.ToString().Trim();
                Boolean bIsNumerico = Microsoft.VisualBasic.Information.IsNumeric((filagrilla.FindControl(xText) as TextBox).Text);
                if (bIsNumerico == false)
                    (filagrilla.FindControl(xText) as TextBox).Text = "0";
                else
                    (filagrilla.FindControl(xText) as TextBox).Text = Convert.ToDouble((filagrilla.FindControl(xText) as TextBox).Text).ToString();

                decTotal+= Convert.ToDecimal((filagrilla.FindControl(xText) as TextBox).Text);
            }


            if (decTotal == 0)
                SCA_MsgInformacion("Ingresar Montos");
            else
            {
                oBE_Presupuesto.iIdPresupuesto = Convert.ToInt32(GrvListado.DataKeys[e.RowIndex].Values["iIdPresupuesto"].ToString());
                oBE_Presupuesto.iMes1 = Convert.ToInt32((filagrilla.FindControl("TxtMes1") as TextBox).Text);
                oBE_Presupuesto.iMes2 = Convert.ToInt32((filagrilla.FindControl("TxtMes2") as TextBox).Text);
                oBE_Presupuesto.iMes3 = Convert.ToInt32((filagrilla.FindControl("TxtMes3") as TextBox).Text);
                oBE_Presupuesto.iMes4 = Convert.ToInt32((filagrilla.FindControl("TxtMes4") as TextBox).Text);
                oBE_Presupuesto.iMes5 = Convert.ToInt32((filagrilla.FindControl("TxtMes5") as TextBox).Text);
                oBE_Presupuesto.iMes6 = Convert.ToInt32((filagrilla.FindControl("TxtMes6") as TextBox).Text);
                oBE_Presupuesto.iMes7 = Convert.ToInt32((filagrilla.FindControl("TxtMes7") as TextBox).Text);
                oBE_Presupuesto.iMes8 = Convert.ToInt32((filagrilla.FindControl("TxtMes8") as TextBox).Text);
                oBE_Presupuesto.iMes9 = Convert.ToInt32((filagrilla.FindControl("TxtMes9") as TextBox).Text);
                oBE_Presupuesto.iMes10 = Convert.ToInt32((filagrilla.FindControl("TxtMes10") as TextBox).Text);
                oBE_Presupuesto.iMes11 = Convert.ToInt32((filagrilla.FindControl("TxtMes11") as TextBox).Text);
                oBE_Presupuesto.iMes12 = Convert.ToInt32((filagrilla.FindControl("TxtMes12") as TextBox).Text);

                oBL_Presupuesto.InsertarPresupuesto(oBE_Presupuesto);

                GrvListado.EditIndex = -1;

                GrvListado.Columns[15].Visible = true;
                GrvListado.Columns[14].HeaderStyle.Width = 22;
                pMuestraDetalleNegocio(DplPeriodo.SelectedValue);
            }
        }


        protected void GrvListado_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            GrvListado.EditIndex = -1;

            GrvListado.Columns[15].Visible = true;
            GrvListado.Columns[14].HeaderStyle.Width = 22;

            pMuestraDetalleNegocio(DplPeriodo.SelectedValue);
        }


        protected void GrvListado_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            BE_Presupuesto oBE_Presupuesto = new BE_Presupuesto();
            BL_Presupuesto oBL_Presupuesto = new BL_Presupuesto();

            switch (e.CommandName)
            {
                case "Eliminar":                    
                    GridViewRow filagrilla = GrvListado.Rows[Convert.ToInt32( e.CommandArgument.ToString() )];

                    oBE_Presupuesto.iIdPresupuesto = Convert.ToInt32(GrvListado.DataKeys[ Convert.ToInt32(e.CommandArgument.ToString()) ].Values["iIdPresupuesto"].ToString());
                    oBE_Presupuesto.sUsuario = GlobalEntity.Instancia.Usuario;
                    oBE_Presupuesto.sNombrePC = GlobalEntity.Instancia.NombrePc;

                    String[] sResultado = null;
                    Int32 iCodigo = 0;
                    String vl_sMessage = String.Empty;

                    sResultado = oBL_Presupuesto.EliminarPresupuesto(oBE_Presupuesto).Split('|');

                    for (int i = 0; i < sResultado.Length; i++)
                    {
                        if (i == 0)                        
                            iCodigo = Convert.ToInt32(sResultado[i]);
                        
                        else if (i == 1)                        
                            vl_sMessage += sResultado[i];
                        
                    }

                    if (iCodigo < 0)
                        MensajeScript(vl_sMessage);
                    else
                        pMuestraDetalleNegocio(DplPeriodo.SelectedValue);

                    break;
            }
        }

    //***********************************************************************
        protected void GrvListado2_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            List<BE_Tabla> olst_TablaTipo = new List<BE_Tabla>();
            BL_Presupuesto oBL_Presupuesto = new BL_Presupuesto();
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataKey dataKey = this.GrvListado2.DataKeys[e.Row.RowIndex];
                BE_Presupuesto oitem = (e.Row.DataItem as BE_Presupuesto);

                if (oitem.iIdVendedor == 0)
                    e.Row.Visible = false;


                if (oitem.iIdVendedor == -1)  //Total
                {
                    e.Row.Cells[14].Visible = false;
                    e.Row.Cells[15].Visible = false;
                    e.Row.Cells[1].Font.Size = 10;

                    for (int c = 1; c < e.Row.Cells.Count; c++)
                    {
                        e.Row.Cells[c].BackColor = System.Drawing.Color.LightGray;
                        e.Row.Cells[c].Font.Bold = true;

                    }
                }
            }
            else if (e.Row.RowType == DataControlRowType.Footer)
            {
                DropDownList DplVendedor = e.Row.FindControl("DplVendedorNew") as DropDownList;

                DplVendedor.DataSource = oBL_Presupuesto.ListarVendedor();
                DplVendedor.DataValueField = "iIdVendedor";
                DplVendedor.DataTextField = "sVendedor";
                DplVendedor.DataBind();
                DplVendedor.SelectedIndex = 0;


                for (int x = 1; x <= 12; x++)
                {
                    string xText = "TxtMes" + x.ToString().Trim() + "New";
                    (e.Row.FindControl(xText) as TextBox).Text = "0";
                }
            }
        }



        protected void GrvListado2_RowEditing(object sender, GridViewEditEventArgs e)
        {
            GrvListado2.EditIndex = e.NewEditIndex; //Cambiamos a modo editable

            GrvListado2.Columns[15].Visible = false;
            GrvListado2.Columns[14].HeaderStyle.Width = 60;

            pMuestraDetalleVendedor(DplPeriodo.SelectedValue);
            GrvListado2.FooterRow.Visible = false;

            String iIdVendedor = (GrvListado2.Rows[e.NewEditIndex].FindControl("txtIdVendedor") as TextBox).Text;

            DropDownList DplVendedor = (GrvListado2.Rows[e.NewEditIndex].FindControl("DplVendedor") as DropDownList);

            if (DplVendedor != null)
            {
                BL_Presupuesto oBL_Presupuesto = new BL_Presupuesto();
                DplVendedor.DataSource = oBL_Presupuesto.ListarVendedor();
                DplVendedor.DataValueField = "iIdVendedor";
                DplVendedor.DataTextField = "sVendedor";
                DplVendedor.DataBind();
            }

            DplVendedor.SelectedValue = iIdVendedor;
        }

        protected void pMuestraDetalleVendedor(string strPeriodo)
        {
            BE_Presupuesto oBE_Presupuesto = new BE_Presupuesto();
            BL_Presupuesto oBL_Presupuesto = new BL_Presupuesto();

            List<BE_Presupuesto> lista = oBL_Presupuesto.ListarPresupuesto(strPeriodo, "2");

            Int32 decMes1 = 0;
            Int32 decMes2 = 0;
            Int32 decMes3 = 0;
            Int32 decMes4 = 0;
            Int32 decMes5 = 0;
            Int32 decMes6 = 0;
            Int32 decMes7 = 0;
            Int32 decMes8 = 0;
            Int32 decMes9 = 0;
            Int32 decMes10 = 0;
            Int32 decMes11 = 0;
            Int32 decMes12 = 0;

            for (int i = 0; i <= lista.Count - 1; i++)
            {
                decMes1 += Convert.ToInt32(lista[i].iMes1.ToString());
                decMes2 += Convert.ToInt32(lista[i].iMes2.ToString());
                decMes3 += Convert.ToInt32(lista[i].iMes3.ToString());
                decMes4 += Convert.ToInt32(lista[i].iMes4.ToString());
                decMes5 += Convert.ToInt32(lista[i].iMes5.ToString());
                decMes6 += Convert.ToInt32(lista[i].iMes6.ToString());
                decMes7 += Convert.ToInt32(lista[i].iMes7.ToString());
                decMes8 += Convert.ToInt32(lista[i].iMes8.ToString());
                decMes9 += Convert.ToInt32(lista[i].iMes9.ToString());
                decMes10 += Convert.ToInt32(lista[i].iMes10.ToString());
                decMes11 += Convert.ToInt32(lista[i].iMes11.ToString());
                decMes12 += Convert.ToInt32(lista[i].iMes12.ToString());
            }

            oBE_Presupuesto.iIdPresupuesto = 0;
            oBE_Presupuesto.iIdVendedor = -1;
            oBE_Presupuesto.sVendedor = "TOTALES (MILES US$)";
            oBE_Presupuesto.iMes1 = decMes1;
            oBE_Presupuesto.iMes2 = decMes2;
            oBE_Presupuesto.iMes3 = decMes3;
            oBE_Presupuesto.iMes4 = decMes4;
            oBE_Presupuesto.iMes5 = decMes5;
            oBE_Presupuesto.iMes6 = decMes6;
            oBE_Presupuesto.iMes7 = decMes7;
            oBE_Presupuesto.iMes8 = decMes8;
            oBE_Presupuesto.iMes9 = decMes9;
            oBE_Presupuesto.iMes10 = decMes10;
            oBE_Presupuesto.iMes11 = decMes11;
            oBE_Presupuesto.iMes12 = decMes12;


            lista.Add(oBE_Presupuesto);

            GrvListado2.DataSource = lista;
            GrvListado2.DataBind();
        }

        protected void btnAgregar2_Click(Object sender, ImageClickEventArgs e)
        {

            BE_Presupuesto oBE_Presupuesto = new BE_Presupuesto();
            BL_Presupuesto oBL_Presupuesto = new BL_Presupuesto();

            GridViewRow filagrilla = GrvListado2.FooterRow;

            Int32 dplVendedor = Convert.ToInt32((filagrilla.FindControl("DplVendedorNew") as DropDownList).SelectedValue);

            foreach (GridViewRow fila in GrvListado2.Rows)
            {
                if (GrvListado2.DataKeys[fila.RowIndex].Values["iIdVendedor"].ToString() == dplVendedor.ToString())
                {
                    SCA_MsgInformacion("El Vendedor Ya Se Encuentra Ingresado");
                    return;
                }
            }


            for (int x = 1; x <= 12; x++)
            {
                string xText = "TxtMes" + x.ToString().Trim() + "New";
                Boolean bIsNumerico = Microsoft.VisualBasic.Information.IsNumeric((filagrilla.FindControl(xText) as TextBox).Text);
                if (bIsNumerico == false)
                    (filagrilla.FindControl(xText) as TextBox).Text = "0";
                else
                    (filagrilla.FindControl(xText) as TextBox).Text = Convert.ToDouble((filagrilla.FindControl(xText) as TextBox).Text).ToString();
            }


            Int32 decMes1 = Convert.ToInt32((filagrilla.FindControl("TxtMes1New") as TextBox).Text);
            Int32 decMes2 = Convert.ToInt32((filagrilla.FindControl("TxtMes2New") as TextBox).Text);
            Int32 decMes3 = Convert.ToInt32((filagrilla.FindControl("TxtMes3New") as TextBox).Text);
            Int32 decMes4 = Convert.ToInt32((filagrilla.FindControl("TxtMes4New") as TextBox).Text);
            Int32 decMes5 = Convert.ToInt32((filagrilla.FindControl("TxtMes5New") as TextBox).Text);
            Int32 decMes6 = Convert.ToInt32((filagrilla.FindControl("TxtMes6New") as TextBox).Text);
            Int32 decMes7 = Convert.ToInt32((filagrilla.FindControl("TxtMes7New") as TextBox).Text);
            Int32 decMes8 = Convert.ToInt32((filagrilla.FindControl("TxtMes8New") as TextBox).Text);
            Int32 decMes9 = Convert.ToInt32((filagrilla.FindControl("TxtMes9New") as TextBox).Text);
            Int32 decMes10 = Convert.ToInt32((filagrilla.FindControl("TxtMes10New") as TextBox).Text);
            Int32 decMes11 = Convert.ToInt32((filagrilla.FindControl("TxtMes11New") as TextBox).Text);
            Int32 decMes12 = Convert.ToInt32((filagrilla.FindControl("TxtMes12New") as TextBox).Text);

            if (decMes1 + decMes2 + decMes3 + decMes4 + decMes5 + decMes6 + decMes7 + decMes8 + decMes9 + decMes10 + decMes11 + decMes12 == 0)
                SCA_MsgInformacion("Ingresar Montos");
            else
            {
                oBE_Presupuesto.iIdPresupuesto = 0;
                oBE_Presupuesto.sPeriodo = DplPeriodo.SelectedValue;
                oBE_Presupuesto.iIdVendedor = dplVendedor;
                oBE_Presupuesto.iMes1 = decMes1;
                oBE_Presupuesto.iMes2 = decMes2;
                oBE_Presupuesto.iMes3 = decMes3;
                oBE_Presupuesto.iMes4 = decMes4;
                oBE_Presupuesto.iMes5 = decMes5;
                oBE_Presupuesto.iMes6 = decMes6;
                oBE_Presupuesto.iMes7 = decMes7;
                oBE_Presupuesto.iMes8 = decMes8;
                oBE_Presupuesto.iMes9 = decMes9;
                oBE_Presupuesto.iMes10 = decMes10;
                oBE_Presupuesto.iMes11 = decMes11;
                oBE_Presupuesto.iMes12 = decMes12;
                oBE_Presupuesto.sUsuario = GlobalEntity.Instancia.Usuario;
                oBE_Presupuesto.sNombrePC = GlobalEntity.Instancia.NombrePc;

                String[] sResultado = null;
                Int32 iCodigo = 0;
                String vl_sMessage = String.Empty;

                sResultado = oBL_Presupuesto.InsertarPresupuesto(oBE_Presupuesto).Split('|');

                for (int i = 0; i < sResultado.Length; i++)
                {
                    if (i == 0)
                    {
                        iCodigo = Convert.ToInt32(sResultado[i]);
                    }
                    else if (i == 1)
                    {
                        vl_sMessage += sResultado[i];
                    }
                }

                if (iCodigo < 0)
                    SCA_MsgInformacion(vl_sMessage);


                pMuestraDetalleVendedor(DplPeriodo.SelectedValue);
            }

        }



        protected void GrvListado2_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            BE_Presupuesto oBE_Presupuesto = new BE_Presupuesto();
            BL_Presupuesto oBL_Presupuesto = new BL_Presupuesto();

            GridViewRow filagrilla = GrvListado2.Rows[e.RowIndex];

            decimal decTotal = 0;
            for (int x = 1; x <= 12; x++)
            {
                string xText = "TxtMes" + x.ToString().Trim();
                Boolean bIsNumerico = Microsoft.VisualBasic.Information.IsNumeric((filagrilla.FindControl(xText) as TextBox).Text);
                if (bIsNumerico == false)
                    (filagrilla.FindControl(xText) as TextBox).Text = "0";
                else
                    (filagrilla.FindControl(xText) as TextBox).Text = Convert.ToDouble((filagrilla.FindControl(xText) as TextBox).Text).ToString();

                decTotal += Convert.ToDecimal((filagrilla.FindControl(xText) as TextBox).Text);
            }


            if (decTotal == 0)
                SCA_MsgInformacion("Ingresar Montos");
            else
            {
                oBE_Presupuesto.iIdPresupuesto = Convert.ToInt32(GrvListado2.DataKeys[e.RowIndex].Values["iIdPresupuesto"].ToString());
                oBE_Presupuesto.iMes1 = Convert.ToInt32((filagrilla.FindControl("TxtMes1") as TextBox).Text);
                oBE_Presupuesto.iMes2 = Convert.ToInt32((filagrilla.FindControl("TxtMes2") as TextBox).Text);
                oBE_Presupuesto.iMes3 = Convert.ToInt32((filagrilla.FindControl("TxtMes3") as TextBox).Text);
                oBE_Presupuesto.iMes4 = Convert.ToInt32((filagrilla.FindControl("TxtMes4") as TextBox).Text);
                oBE_Presupuesto.iMes5 = Convert.ToInt32((filagrilla.FindControl("TxtMes5") as TextBox).Text);
                oBE_Presupuesto.iMes6 = Convert.ToInt32((filagrilla.FindControl("TxtMes6") as TextBox).Text);
                oBE_Presupuesto.iMes7 = Convert.ToInt32((filagrilla.FindControl("TxtMes7") as TextBox).Text);
                oBE_Presupuesto.iMes8 = Convert.ToInt32((filagrilla.FindControl("TxtMes8") as TextBox).Text);
                oBE_Presupuesto.iMes9 = Convert.ToInt32((filagrilla.FindControl("TxtMes9") as TextBox).Text);
                oBE_Presupuesto.iMes10 = Convert.ToInt32((filagrilla.FindControl("TxtMes10") as TextBox).Text);
                oBE_Presupuesto.iMes11 = Convert.ToInt32((filagrilla.FindControl("TxtMes11") as TextBox).Text);
                oBE_Presupuesto.iMes12 = Convert.ToInt32((filagrilla.FindControl("TxtMes12") as TextBox).Text);

                oBL_Presupuesto.InsertarPresupuesto(oBE_Presupuesto);

                GrvListado2.EditIndex = -1;

                GrvListado2.Columns[15].Visible = true;
                GrvListado2.Columns[14].HeaderStyle.Width = 22;
                pMuestraDetalleVendedor(DplPeriodo.SelectedValue);
            }
        }


        protected void GrvListado2_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            GrvListado2.EditIndex = -1;

            GrvListado2.Columns[15].Visible = true;
            GrvListado2.Columns[14].HeaderStyle.Width = 22;

            pMuestraDetalleVendedor(DplPeriodo.SelectedValue);
        }


        protected void GrvListado2_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            BE_Presupuesto oBE_Presupuesto = new BE_Presupuesto();
            BL_Presupuesto oBL_Presupuesto = new BL_Presupuesto();

            switch (e.CommandName)
            {
                case "Eliminar":
                    GridViewRow filagrilla = GrvListado2.Rows[Convert.ToInt32(e.CommandArgument.ToString())];

                    oBE_Presupuesto.iIdPresupuesto = Convert.ToInt32(GrvListado2.DataKeys[Convert.ToInt32(e.CommandArgument.ToString())].Values["iIdPresupuesto"].ToString());
                    oBE_Presupuesto.sUsuario = GlobalEntity.Instancia.Usuario;
                    oBE_Presupuesto.sNombrePC = GlobalEntity.Instancia.NombrePc;

                    String[] sResultado = null;
                    Int32 iCodigo = 0;
                    String vl_sMessage = String.Empty;

                    sResultado = oBL_Presupuesto.EliminarPresupuesto(oBE_Presupuesto).Split('|');

                    for (int i = 0; i < sResultado.Length; i++)
                    {
                        if (i == 0)
                            iCodigo = Convert.ToInt32(sResultado[i]);

                        else if (i == 1)
                            vl_sMessage += sResultado[i];

                    }

                    if (iCodigo < 0)
                        MensajeScript(vl_sMessage);
                    else
                        pMuestraDetalleVendedor(DplPeriodo.SelectedValue);

                    break;
            }
        }
        protected void DplPeriodo_SelectedIndexChanged(object sender, EventArgs e)
        {
            pMuestraDetalleNegocio(DplPeriodo.SelectedValue);
            pMuestraDetalleVendedor(DplPeriodo.SelectedValue);
        }
}