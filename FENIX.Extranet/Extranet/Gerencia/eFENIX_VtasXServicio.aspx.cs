﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using FENIX.BusinessEntity;
using FENIX.BusinessLogic;
using System.Windows.Forms;
using System.Drawing;

public partial class Extranet_Gerencia_eFENIX_VtasXServicio : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        txtTop.Attributes.Add("onkeypress", "javascript:return SoloEnterosDecimales(event);");

        String tiempo = (Convert.ToInt32(GlobalEntity.Instancia.TiempoDuracionSesion.ToString()) * 1000 * 60).ToString();
        Session["Reset"] = true;

        if (!Page.IsPostBack)
        {
            ListarDropDowList();
            

            txtTop.Text = "4";

            ViewState["MesRangoIni"] = Request.QueryString["rIni"];
            ViewState["MesRangoFin"] = Request.QueryString["rFin"];
            ViewState["VentanaAnt"] = Request.QueryString["VentanaAnt"];
            ViewState["Ventana"] = Request.QueryString["Ventana"];
            ViewState["Top"] = Request.QueryString["Top"];
            ViewState["Inicio"] = Request.QueryString["Inicio"];

            if (Request.QueryString["Periodo"] != null)
                DplPeriodo.SelectedValue = Request.QueryString["Periodo"];
            else
                DplPeriodo.SelectedValue = DateTime.Now.Year.ToString();

            if (Request.QueryString["mIni"] != null)
                DplMesIni.SelectedValue = Request.QueryString["mIni"];

            if (Request.QueryString["Linea"] != null)
                DplNegocio.SelectedValue = Request.QueryString["Linea"].ToString();

            if (Request.QueryString["IdCliente"] != null)
                DplCliente.SelectedValue = Request.QueryString["IdCliente"].ToString();

            if (Request.QueryString["IdVend"] != null)
                DplVendedor.SelectedValue = Request.QueryString["IdVend"].ToString();

            if (Request.QueryString["mFin"] != null)
            {
                DplMesFin.SelectedValue = Request.QueryString["mFin"];
                pMuestraDetalle();
                ImgBtnBack.Visible = true;
                DplNegocio.Enabled = false;
                DplCliente.Enabled = false;
                DplVendedor.Enabled = false;
            }
            else
            {
                DplMesFin.SelectedValue = DateTime.Now.Month.ToString();
                List<BE_Presupuesto> lsBE_Presupuesto = new List<BE_Presupuesto>();
                BE_Presupuesto oBE_Presupuesto = new BE_Presupuesto();
                lsBE_Presupuesto.Add(oBE_Presupuesto);

                GrvListado.DataSource = lsBE_Presupuesto;
                GrvListado.DataBind();

                ImgBtnBack.Visible = false;
                DplNegocio.Enabled = true;
                DplCliente.Enabled = true;
                DplVendedor.Enabled = true;
            }

            ScriptManager.RegisterStartupScript(this, GetType(), "mensaje", "SessionExpireAlert(" + tiempo + ");", true);
        }
        else
            ScriptManager.RegisterStartupScript(this, GetType(), "mensaje", "myStopFunction();", true);
    }


    protected void ListarDropDowList()
    {
        try
        {
            DplPeriodo.Items.Clear();
            for (int i = 2013; i <= DateTime.Now.Year; i++)
            {
                DplPeriodo.Items.Add(new ListItem(i.ToString(), i.ToString()));
            }
            DplPeriodo.SelectedIndex = 0;


            BL_Presupuesto oBL_Presupuesto = new BL_Presupuesto();

            DplMesIni.DataSource = oBL_Presupuesto.ListarDatosTabla(124);
            DplMesIni.DataValueField = "iIdValor";
            DplMesIni.DataTextField = "sDescripcion";
            DplMesIni.DataBind();
            DplMesIni.SelectedIndex = 0;


            DplMesFin.DataSource = oBL_Presupuesto.ListarDatosTabla(124);
            DplMesFin.DataValueField = "iIdValor";
            DplMesFin.DataTextField = "sDescripcion";
            DplMesFin.DataBind();
            DplMesFin.SelectedIndex = 0;

            DplNegocio.DataSource = oBL_Presupuesto.ListarDatosTabla(154);
            DplNegocio.DataValueField = "iIdValor";
            DplNegocio.DataTextField = "sDescripcion";
            DplNegocio.DataBind();
            DplNegocio.SelectedIndex = 0;

            DplVendedor.DataSource = oBL_Presupuesto.ListarVendedor();
            DplVendedor.DataValueField = "iIdVendedor";
            DplVendedor.DataTextField = "sVendedor";
            DplVendedor.DataBind();
            DplVendedor.SelectedIndex = 0;

            DplCliente.DataSource = oBL_Presupuesto.ListarCliente();
            DplCliente.DataValueField = "iIdCliente";
            DplCliente.DataTextField = "sCliente";
            DplCliente.DataBind();
            DplCliente.SelectedIndex = 0;
            


        }
        catch (Exception e)
        {

        }
    }

    private void SCA_MsgInformacion(string strError)
    {
        MensajeScript(strError);
    }

    private void MensajeScript(string SMS)
    {
        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "", "<script>alert('" + SMS + "');</script>", false);
    }

    protected void btn_cerrar_Click(object sender, EventArgs e)
    {
        if (GlobalEntity.Instancia.CerrarExtranet == "S")
        {
            BL_Usuario oBL_Usuario = new BL_Usuario();
            oBL_Usuario.GrabaInicioSesion(GlobalEntity.Instancia.Usuario, "0");
            Session.Abandon();
            FormsAuthentication.SignOut();
            Response.Redirect("../../eFENIX_Login.aspx");
            Response.End();
        }
    }


    protected void pMuestraDetalle()
    {
        BE_Presupuesto oBE_Presupuesto = new BE_Presupuesto();
        BL_Presupuesto oBL_Presupuesto = new BL_Presupuesto();

        oBE_Presupuesto.sPeriodo = DplPeriodo.SelectedValue;
        oBE_Presupuesto.iMesIni = Convert.ToInt32(DplMesIni.SelectedValue);
        oBE_Presupuesto.iMesFin = Convert.ToInt32(DplMesFin.SelectedValue);        
        oBE_Presupuesto.iIdLineaNegocio = Convert.ToInt32(DplNegocio.SelectedValue);
        oBE_Presupuesto.iIdCliente = Convert.ToInt32(DplCliente.SelectedValue);
        oBE_Presupuesto.iIdVendedor = Convert.ToInt32(DplVendedor.SelectedValue);
        oBE_Presupuesto.iLimite = Convert.ToInt32(txtTop.Text);

        List<BE_Presupuesto> lista = oBL_Presupuesto.ListarVtasXServicio(oBE_Presupuesto);


        Decimal iVentas = 0;
        Decimal dPorc = 0;

        for (int i = 0; i <= lista.Count - 1; i++)
        {
            if (lista[i].iIdServicio.ToString() == "-1")
            {
                iVentas += Convert.ToDecimal(lista[i].dVentas.ToString());
                dPorc += Convert.ToDecimal(lista[i].dPorc.ToString());
            }
        }

        oBE_Presupuesto.iItem = 999;
        oBE_Presupuesto.iIdGrupo = 0;
        oBE_Presupuesto.iIdServicio = -2;
        oBE_Presupuesto.sServicio = "TOTALES (MILES US$)";
        oBE_Presupuesto.dVentas = iVentas;
        oBE_Presupuesto.dPorc =  dPorc;
        lista.Add(oBE_Presupuesto);


        GrvListado.DataSource = lista;
        GrvListado.DataBind();
    }

    


    protected void ImgBtnBuscar_Click(object sender, ImageClickEventArgs e)
    {
        if (Microsoft.VisualBasic.Information.IsNumeric(txtTop.Text))
            pMuestraDetalle();
        else
        {
            txtTop.Text = "4";
            pMuestraDetalle();
        }
    }

    protected void GrvListado_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        List<BE_Tabla> olst_TablaTipo = new List<BE_Tabla>();
        BL_Presupuesto oBL_Presupuesto = new BL_Presupuesto();
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            DataKey dataKey = this.GrvListado.DataKeys[e.Row.RowIndex];
            BE_Presupuesto oitem = (e.Row.DataItem as BE_Presupuesto);

            if (oitem.iItem == 0)
                e.Row.Visible = false;
            else
            {
                e.Row.Cells[2].BackColor = ColorTranslator.FromHtml("#A8E7B6");
                if (oitem.iIdServicio == -1)  //SubTotal por Grupo
                {
                    e.Row.Cells[0].Text = "";
                    for (int c = 0; c < e.Row.Cells.Count; c++)
                    {
                        e.Row.Cells[c].Font.Size = 10;
                        e.Row.Cells[c].Font.Bold = true;

                        e.Row.Cells[c].BackColor = System.Drawing.Color.LightBlue;
                    }
                }
                else if (oitem.iIdServicio == -2)  //Total
                {
                    e.Row.Cells[0].Text = "";
                    
                    for (int c = 0; c < e.Row.Cells.Count; c++)
                    {
                        e.Row.Cells[c].Font.Size = 10;
                        e.Row.Cells[c].Font.Bold = true;

                        e.Row.Cells[c].BackColor = System.Drawing.Color.LightGray;
                    }
                }
                
            }
        }
        else if (e.Row.RowType == DataControlRowType.Header)
        {
            e.Row.Cells[2].BackColor = ColorTranslator.FromHtml("#0069ae");
        }
    }

}