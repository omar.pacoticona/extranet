﻿<%@ Page Language="C#"
    MasterPageFile="~/Extranet/Seguridad/eFENIX_Principal.master"
    AutoEventWireup="true" CodeFile="eFenix_Solicitud_RefrendoV2.aspx.cs" Inherits="Extranet_Gerencia_eFenix_Solicitud_RefrendoV2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title></title>

    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js" type="text/javascript"></script>
    <script src="../../Script/Java_Script/jsSolicitudARE.js"></script>

      <link href="../../Script/Hojas_Estilo/Preload1.css" rel="stylesheet" />


</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentCab" runat="Server">
    <span class="texto_titulo">Tu Sesion Expirara en &nbsp; 
        <span class="texto_titulo" id="seconds"></span>
        <span>&nbsp;seg.</span>
    </span>
    <asp:Button ID="btn_cerrar" runat="server" Text="cerrar"
        OnClick="btn_cerrar_Click" Style="display: none;" />

</asp:Content>



<asp:Content ID="Content2" ContentPlaceHolderID="CntMaster" runat="Server">
    <!DOCTYPE html>

    <html>
    <body>
        <div class="form_titulo" style="width: 100%; height: 23px; vertical-align: inherit">
            <p style="margin-top: 1.5px; font-size: 14px;">SOLICITUD DE AUTORIZACIÓN DE RETIRO ELECTRÓNICA (ARE)</p>
        </div>
        <div id="tabs-container">
            <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="1" OnActiveTabChanged="TabContainer1_ActiveTabChanged" AutoPostBack="True"
                Width="100%">

                <ajaxToolkit:TabPanel ID="tbPanelBusqueda" runat="server" HeaderText="Paso N°1">
                    <ContentTemplate>
                        <asp:Panel ID="Panessl4" runat="server" BackColor="White" Height="470px" ScrollBars="Vertical" Width="99%">
                            <fieldset style="width: 98%; background-color: white;">
                                <legend style="background-color: #0069AE; font-weight: 700;">Datos de la Busqueda</legend>
                                 <div id="divBusqueda">
                     <table class="form_bandeja" style="margin-top: 5px; margin-bottom: 5px; width: 100%;">
                         <tr> <td style="width:180px;"><asp:Label runat="server">Booking </asp:Label></td>
                             <td style="width:250px;">  <asp:TextBox ID="txtBooking" runat="server" MaxLength="20" Style="text-transform: uppercase"
                         Width="185px"></asp:TextBox></td>
                              <td style="width:250px;"><asp:Button ID="ImgBtnBuscar" runat="server" Text="Consultar" class="btn btn-primary" BackColor="#0069AE" OnClick="ImgBtnBuscar_Click" /> </td>
                              <td></td>
                         </tr>
                     </table>   

                  </div>
                            </fieldset>
                            <br />
                            <fieldset style="width: 98%; background-color: white;"><legend style="background-color: #0069AE; font-weight: 700;">Resultado Busqueda</legend>
                                  <table  class="form_bandeja" style=" width: 100%; margin-top:5px;">
         <tr valign="top" style="height: 50%;width:99%;">
            <td>
                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>
                        <table class="form_bandeja" style=" width: 100%; margin-top:5px;">
                            <tr>
                                <td style="width:250px;">
                                    <asp:Label runat="server">Exportador </asp:Label>
                                </td>
                                <td style="width:600px;">
                                    <asp:TextBox ID="txtExportador" runat="server" ReadOnly="" Style="text-transform: uppercase" Width="450px"></asp:TextBox>
                                    <asp:TextBox ID="txtIdExportador" runat="server" ReadOnly="" Style="text-transform: uppercase" Width="50px"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td style="width:250px;">
                                    <asp:Label runat="server">Agencia de Aduanas </asp:Label>
                                </td>
                                <td style="width:550px;">
                                    <asp:TextBox ID="txtAgeniaAduanas" runat="server" ReadOnly="" Style="text-transform: uppercase" Width="450px"></asp:TextBox>
                                    <asp:TextBox ID="txtIdAgenciaAduanas" runat="server" ReadOnly="" Style="text-transform: uppercase" Width="50px"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td style="width:250px;">
                                    <asp:Label runat="server"> Nave / Viaje / Rumbo </asp:Label>
                                </td>
                                <td style="width:550px;">
                                    <asp:TextBox ID="txtNave" runat="server" ReadOnly="" Style="text-transform: uppercase" Width="212px"></asp:TextBox>
                                    &nbsp; &nbsp;
                                    <asp:TextBox ID="txtViaje" runat="server" ReadOnly="" Style="text-transform: uppercase" Width="100px"></asp:TextBox>
                                    &nbsp; &nbsp;
                                    <asp:TextBox ID="txtRumbo" runat="server" ReadOnly="" Style="text-transform: uppercase" Width="100px"></asp:TextBox>
                                    <asp:TextBox ID="txtIdNave" runat="server" ReadOnly="" Style="text-transform: uppercase" Width="50px"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td style="width:250px;">
                                    <asp:Label runat="server">Cut Off / C. Suelta </asp:Label>
                                </td>
                                <td style="width:500px;">
                                    <asp:TextBox ID="txtCSuelta" runat="server" ReadOnly="" Style="text-transform: uppercase" Width="450px"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td style="width:250px;">
                                    <asp:Label runat="server">Cut Off / Refrigerante </asp:Label>
                                </td>
                                <td style="width:500px;">
                                    <asp:TextBox ID="txtRefrigerante" runat="server" ReadOnly="" Style="text-transform: uppercase" Width="450px"></asp:TextBox>
                                </td>
                            </tr>
                        </table>
                    </ContentTemplate>
                </asp:UpdatePanel>
                 </td>

        </tr> 
         </table>
         </fieldset>
                            <br>
                            </br>
                            <fieldset style="width:100%;background-color:white;">         
         <table  width="100%">
             <tr>
                 <td>
                     <%--<div style="float: right;">--%>
                  <asp:ImageButton ID="btnTab1" runat="server" autopostback="true" ImageUrl="~/Imagenes//btnNext.png"
                                 CssClass="btnNext" ToolTip="Siguiente"
                       ImageAlign="Right"/>  
                       <%--  </div>--%>
                 </td>
                  
              </tr>
         </table>

      </fieldset>    
                            
                        </asp:Panel>
                    </ContentTemplate>



                </ajaxToolkit:TabPanel>

                <ajaxToolkit:TabPanel ID="tbPanelAdj" runat="server" HeaderText="PASO N°2">
                    <HeaderTemplate>PASO N°2</HeaderTemplate>
                    <ContentTemplate>
                        <asp:Panel ID="Panel4" runat="server" BackColor="White" Height="470px" ScrollBars="Vertical" Width="99%">
                            <table width="100%">
          <tr>
              <td>
          
                               <div id="dvCargaSeleccionada" style="height:200px;width:99%;">
                                     <fieldset style="width: 99%; background-color: white;">
                                <legend style="background-color: #0069AE; font-weight: 700;">Adjuntar documentos</legend>
                                <table>
                                    <tr valign="top" style="height: 50%; width: 1000px;">
                                        <td colspan="4">
                                            <asp:UpdatePanel runat="server" UpdateMode="Conditional">
                                                <ContentTemplate>
                                                    <asp:Panel ID="Panel5" runat="server" BackColor="White" Height="150px" ScrollBars="Vertical" Width="1000px">
                                                        <table border="1">
                                                            <tr style="color: ivory; background-color: #0069AE">
                                                                <%--<th>id</th>--%>
                                                                <th>Documento</th>
                                                                <th>Adjuntar</th>
                                                                <th>Accion</th>
                                                                <th>Nombre Archivo</th>
                                                            </tr>
                                                            <tr>
                                                                 <asp:Label runat="server" ID="lblUplBLID" Text="0" Visible="false"></asp:Label>
                                                                
                                                                <td> DUA
                                                                </td>
                                                                <td>                                                                  
                                                                    <asp:FileUpload ID="FileUploadDua" runat="server" Width="320px" Enabled="true"  accept="application/pdf" />
                                                          <%-- onchange="saveFileUpl('FileUploadDua');"--%>
                                                                </td>
                                                                <td align="center">
                                                                    <asp:ImageButton runat="server" ID="imgBtnUplDua" ImageUrl="~/Imagenes/save.png" OnClick="imgBtnUplDua_Click"/>
                                                                 
                                                                    <asp:ImageButton runat="server" ID="imgBtnUplDuaupd" ImageUrl="~/Imagenes/cambiar.png" Visible="false" />
                                                                </td>
                                                                <td>
                                                                    <asp:Label runat="server" ID="lblUplDua" Text=" "></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                             <%--   <td>  </td>--%>
                                                                    <asp:Label runat="server" ID="lblUplBookingId" Text="0" Visible="false"></asp:Label>
                                                              
                                                                <td>BOOKING</td>
                                                                <td>
                                                                    <asp:FileUpload ID="FileUploadBooking" runat="server" Width="320px" Enabled="false" accept="application/pdf"  onchange="saveFileUpl('FileUploadBooking');" />
                                                                </td>
                                                                <td align="center">
                                                                    <asp:ImageButton runat="server" ID="imgBtnUplBoking" ImageUrl="~/Imagenes/save.png"  Enabled="true" />
                                                                    <asp:ImageButton runat="server" ID="imgBtnUplBokingUpd" ImageUrl="~/Imagenes/cambiar.png"  Visible="false" />
                                                                </td>
                                                                <td>
                                                                    <asp:Label runat="server" ID="lblUplBooking" Text=" "></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                               <%-- <td> </td>--%>
                                                                    <asp:Label runat="server" ID="lblUplTicketID" Text="0" Visible="false"></asp:Label>
                                                                
                                                                <td>TICKET DE PESO</td>
                                                                <td>
                                                                    <asp:FileUpload ID="FileUploaTicket" runat="server" Width="320px" Enabled="false" accept="application/pdf"  onchange="saveFileUpl('FileUploaTicket');" />
                                                                </td>
                                                                <td align="center">
                                                                    <asp:ImageButton runat="server" ID="imgBtnUplTicekt" ImageUrl="~/Imagenes/save.png"  Enabled="true" />
                                                                    <asp:ImageButton runat="server" ID="imgBtnUplTicektUpd" ImageUrl="~/Imagenes/cambiar.png" Visible="false" />
                                                                </td>
                                                                <td>
                                                                    <asp:Label runat="server" ID="lblUplTicket" Text=" "></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                               <%-- <td> </td>--%>
                                                                    <asp:Label runat="server" ID="lblUplPackListID" Text="0" Visible="false"></asp:Label>
                                                               
                                                                <td>PACKING LIST</td>
                                                                <td>
                                                                    <asp:FileUpload ID="FileUploadPackList" runat="server" Width="320px" Enabled="false" accept="application/pdf"  onchange="saveFileUpl('FileUploadPackList');"/>
                                                                </td>
                                                                <td align="center">
                                                                    <asp:ImageButton runat="server" ID="imgBtnUplPackList" ImageUrl="~/Imagenes/save.png" Enabled="true" />
                                                                    <asp:ImageButton runat="server" ID="imgBtnUplPackListUpd" ImageUrl="~/Imagenes/cambiar.png"  Visible="false" />
                                                                </td>
                                                                <td>
                                                                    <asp:Label runat="server" ID="lblUplPackList" Text=" "></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                               <%-- <td> </td>--%>
                                                                    <asp:Label runat="server" ID="lblUplGuiaRemID" Text="0" Visible="false"></asp:Label>
                                                               
                                                                <td>GUIA DE REMISION</td>
                                                                <td>
                                                                    <asp:FileUpload ID="FileUploadsGuiaRem" runat="server" Width="320px" Enabled="false" accept="application/pdf"  onchange="saveFileUpl('FileUploadsGuiaRem');"/>
                                                                </td>
                                                                <td align="center">
                                                                    <asp:ImageButton runat="server" ID="imgBtnUplGuiaRem" ImageUrl="~/Imagenes/save.png" Enabled="true" />
                                                                    <asp:ImageButton runat="server" ID="imgBtnUplGuiaRemUpd" ImageUrl="~/Imagenes/cambiar.png"  Visible="false" />
                                                                </td>
                                                                <td>
                                                                    <asp:Label runat="server" ID="lblUplGuiaRem" Text=" "></asp:Label>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </asp:Panel>
                                                </ContentTemplate>    
                                           </asp:UpdatePanel>
                                        </td>
                                    </tr>
                                    
                                </table>
                            </fieldset>
                                 </div>
                                
    
              </td>
            </tr>
          <tr>
              <td>
                  <fieldset  style="width:100%;background-color:white;">
     <legend style="background-color:#0069AE;font-weight:700;">Datos de pago</legend>
         <table  class="form_bandeja" style=" width: 100%; margin-top:5px;">
         <tr valign="top" style="height: 50%;width:99%;">
            <td>
                <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                    <ContentTemplate>
                        <table class="form_bandeja" style=" width: 100%; margin-top:5px;">
                            <tr>
                                <td style="width:150px;">
                                    <asp:Label ID="lblTipoPago" runat="server">Tipo de Pago </asp:Label>
                                </td>
                                <td style="width:200px;">
                                    <asp:DropDownList ID="DplTipoPago" runat="server" Width="200px">
                                    </asp:DropDownList>
                                </td>
                                <td style="width:150px;">
                                    <asp:Label ID="lblMonto" runat="server">Monto </asp:Label>
                                </td>
                                <td style="width:200px;">
                                    <asp:TextBox ID="txtMonto" runat="server" Style="text-transform: uppercase" Width="100px"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td style="width:150px;">
                                    <asp:Label ID="lblFormaPago" runat="server">Forma de Pago </asp:Label>
                                </td>
                                <td style="width:200px;">
                                    <asp:DropDownList ID="DplFormaPago" runat="server" Width="200px">
                                    </asp:DropDownList>
                                </td>
                                <td style="width:150px;">
                                    <asp:Label ID="lblNroOperacion" runat="server">N° Operacion </asp:Label>
                                </td>
                                <td style="width:200px;">
                                    <asp:TextBox ID="txtOperacion" runat="server" Style="text-transform: uppercase" Width="100px"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td style="width:150px;">
                                    <asp:Label ID="lblBanco" runat="server">Banco </asp:Label>
                                </td>
                                <td style="width:200px;">
                                    <asp:DropDownList ID="DplBanco" runat="server" Width="200px">
                                    </asp:DropDownList>
                                </td>
                                <td style="width:150px;">
                                    <asp:Label ID="lblMoneda" runat="server">Moneda </asp:Label>
                                </td>
                                <td style="width:200px;">
                                    <asp:DropDownList ID="DplMoneda" runat="server" Width="110px">
                                        <asp:ListItem Value="-1">[Seleccionar]</asp:ListItem>
                                        <asp:ListItem Value="23">SOLES</asp:ListItem>
                                        <asp:ListItem Value="24">US Dolar</asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                            </tr>
                        </table>
                        <table class="form_bandeja" style=" width: 100%; margin-top:1px;">
                            <tr>
                                <td style="width:100%;">
                                    <textarea id="TextArea1" runat="server" cols="70" placeholder="Ingrese Obervaciones" rows="2"></textarea> </td>
                            </tr>
                        </table>
                    </ContentTemplate>
                </asp:UpdatePanel>
                 </td>

        </tr> 
         </table>

     </fieldset>
              </td>
          </tr>
      </table>
      <table width="100%">
          <tr>
              <td>
               <div style="float:right;">
                  <asp:Button ID="BtnGrabar"  runat="server" Text="Grabar" class="btn btn-primary" BackColor="#0069AE" OnClick="BtnGrabar_Click" />
                   
                  </div>
              </td>
             
          </tr>
      </table>
                            
                        </asp:Panel>
                    </ContentTemplate>



                </ajaxToolkit:TabPanel>

            </ajaxToolkit:TabContainer>
            
        </div>

    </body>
    </html>


    <script language="javascript" type="text/javascript">


        var tiempo = 0;
        var seconds = 0;

        var myVar = setInterval(function () {
            clearInterval(myVar);
        }, 1000);

        var myTiempo = setTimeout(function () {
            clearInterval(myTiempo);
        }, 1);

        function myStopFunction() {
            clearInterval(myVar);
            seconds = '<%=GlobalEntity.Instancia.TiempoDuracionSesion%>' * 60;
            tiempo = '<%=GlobalEntity.Instancia.TiempoDuracionSesion%>' * 1000 * 60;

            CuentaTiempo(tiempo);
        }

        function CuentaTiempo(tiempo) {
            clearTimeout(myTiempo);
            myTiempo = setTimeout(function () {

                //window.location = "../Seguridad/eFENIX_Login.aspx";
            }, tiempo);
        }



        function SessionExpireAlert(timeout) {
            seconds = timeout / 1000;
            document.getElementsByName("seconds").innerHTML = seconds;

            myvar = setInterval(function () {
                seconds--;
                document.getElementById("seconds").innerHTML = seconds;
            }, 1000);

            CuentaTiempo(timeout);
        };



        var objFilaAnt = null;
        var backgroundColorFilaAnt = "";






        function SoloEnteros(e) {
            var tecla = document.all ? tecla = e.keyCode : tecla = e.which;
            return ((tecla > 47 && tecla < 58) || tecla == 44 || tecla == 46);
        }


      <%--  function fc_Enter(e, sObjeto) {
            if (event.keyCode == 13) {
                if (sObjeto == 'TxtDocumentoM') {
                    document.getElementById("<%=BtnConsultaDo.ClientID%>").click();
                } else if (sObjeto == 'txtDniDespachador') {
                    document.getElementById("<%=btnDespachador.ClientID%>").click();
                } else if (sObjeto == 'txtRucFac') {
                    document.getElementById("<%=btnRucFac.ClientID%>").click();
                }

                return false;
            }
        }

        function EnteRyValidaLongitud(e, campo, longitudMaxima) {
            try {
                if (campo.value.length > (longitudMaxima - 1)) {
                    return false;
                }
                else {
                    return true;
                }

                if (event.keyCode == 13) {
                    if (campo == 'txtDua') {
                        document.getElementById("<%=btnSiguiente.ClientID%>").click();
                    }
                    return false;
                }

            } catch (e) {
                return false;
            }
        }--%>

     


        function SoloEnteros(e) {
            var tecla = document.all ? tecla = e.keyCode : tecla = e.which;
            return (tecla > 47 && tecla < 58);
        }


    </script>


    <style type="text/css">
        #reservationsList {
            width: 500px;
            max-height: 600px;
            background: #fff;
            overflow: auto;
        }

        /*STRIPE LIST*/
        ul.stripedList {
            margin: 0;
            padding: 0;
            list-style: none;
        }

        .stripedList li {
            display: block;
            text-decoration: none;
            color: #333;
            font-family: Arial,Helvetica,sans-serif;
            font-size: 12px;
            line-height: 20px;
            height: 20px;
        }

            .stripedList li span {
                display: inline-block;
                border-right: 1px solid #ccc;
                overflow: hidden;
                text-overflow: ellipsis;
                padding: 0 10px;
                height: 20px;
            }

        .stripedList .evenRow {
            background: #f2f2f2;
        }

        .c1 {
            width: 55px;
        }

        .c2 {
            width: 70px;
        }

        .c3 {
            width: 130px;
        }

        .c4 {
            width: 15px;
        }

        .cLast {
            border: 0 !important;
        }

        .auto-style2 {
            width: 147px;
        }

        .auto-style3 {
            height: 26px;
        }

        .auto-style4 {
            width: 439px;
        }

        .bootonClas {
            background-color: #0069AE !important;
            border-color: #0069AE !important;
            color: #fff !important;
            font-size: 15px;
            font-family: Arial,Helvetica,sans-serif;
            font-weight: bold;
        }

        .auto-style8 {
            width: 268435488px;
        }
    </style>

</asp:Content>
