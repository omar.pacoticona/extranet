﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using FENIX.BusinessEntity;
using FENIX.BusinessLogic;
using System.Windows.Forms;
using System.Drawing;

public partial class Extranet_Gerencia_eFENIX_VtasXCliente : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        txtTop.Attributes.Add("onkeypress", "javascript:return SoloEnterosDecimales(event);");

        String tiempo = (Convert.ToInt32(GlobalEntity.Instancia.TiempoDuracionSesion.ToString()) * 1000 * 60).ToString();
        Session["Reset"] = true;

        if (!Page.IsPostBack)
        {
            ListarDropDowList();
            DplNegocio.Enabled = false;
           

            ViewState["MesRangoIni"] = Request.QueryString["rIni"];
            ViewState["MesRangoFin"] = Request.QueryString["rFin"];
            ViewState["VentanaAnt"] = Request.QueryString["VentanaAnt"];
            

            if (Request.QueryString["Top"] != null)
                txtTop.Text = Request.QueryString["Top"];
            else
                txtTop.Text = "10";

            if (Request.QueryString["Periodo"] != null)
                DplPeriodo.SelectedValue = Request.QueryString["Periodo"];
            else
                DplPeriodo.SelectedValue = DateTime.Now.Year.ToString();

            if (Request.QueryString["mIni"] != null)            
                DplMesIni.SelectedValue = Request.QueryString["mIni"];

            if (Request.QueryString["Linea"] != null)
                DplNegocio.SelectedValue = Request.QueryString["Linea"].ToString();

            if (Request.QueryString["mFin"] != null)
            {
                DplMesFin.SelectedValue = Request.QueryString["mFin"];
                
                //DplOperacion.Enabled = false;
                pMuestraDetalle();

                if (Request.QueryString["Inicio"] == "S")
                {
                    ImgBtnBack.Visible = false;
                    ViewState["Inicio"] = "S";
                }
                else
                    ImgBtnBack.Visible = true;
            }
            else
            {
                DplMesFin.SelectedValue = DateTime.Now.Month.ToString();

                List<BE_Presupuesto> lsBE_Presupuesto = new List<BE_Presupuesto>();
                BE_Presupuesto oBE_Presupuesto = new BE_Presupuesto();
                lsBE_Presupuesto.Add(oBE_Presupuesto);

                GrvListado.DataSource = lsBE_Presupuesto;
                GrvListado.DataBind();

                ImgBtnBack.Visible = false;

                ViewState["Inicio"] = "S";
            }

            ScriptManager.RegisterStartupScript(this, GetType(), "mensaje", "SessionExpireAlert(" + tiempo + ");", true);
        }
        else
            ScriptManager.RegisterStartupScript(this, GetType(), "mensaje", "myStopFunction();", true);
    }


    #region "Método Controles"
    protected void ListarDropDowList()
    {
        try
        {
            DplPeriodo.Items.Clear();
            for (int i = 2013; i <= DateTime.Now.Year; i++)
            {
                DplPeriodo.Items.Add(new ListItem(i.ToString(), i.ToString()));
            }
            DplPeriodo.SelectedIndex = 0;


            BL_Presupuesto oBL_Presupuesto = new BL_Presupuesto();

            DplMesIni.DataSource = oBL_Presupuesto.ListarDatosTabla(124);
            DplMesIni.DataValueField = "iIdValor";
            DplMesIni.DataTextField = "sDescripcion";
            DplMesIni.DataBind();
            DplMesIni.SelectedIndex = 0;


            DplMesFin.DataSource = oBL_Presupuesto.ListarDatosTabla(124);
            DplMesFin.DataValueField = "iIdValor";
            DplMesFin.DataTextField = "sDescripcion";
            DplMesFin.DataBind();
            DplMesFin.SelectedIndex = 0;

            DplNegocio.DataSource = oBL_Presupuesto.ListarDatosTabla(154);
            DplNegocio.DataValueField = "iIdValor";
            DplNegocio.DataTextField = "sDescripcion";
            DplNegocio.DataBind();            
            DplNegocio.SelectedIndex = 0;


            DplOperacion.Items.Clear();
            DplOperacion.Items.Add(new ListItem("Todos","0"));
            DplOperacion.Items.Add(new ListItem("Exportacion","65"));
            DplOperacion.Items.Add(new ListItem("Importacion","66"));
            DplOperacion.SelectedIndex = 0;


        }
        catch (Exception e)
        {

        }
    }
    #endregion


    private void SCA_MsgInformacion(string strError)
    {
        MensajeScript(strError);
    }

    private void MensajeScript(string SMS)
    {
        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "", "<script>alert('" + SMS + "');</script>", false);
    }

    protected void btn_cerrar_Click(object sender, EventArgs e)
    {
        if (GlobalEntity.Instancia.CerrarExtranet == "S")
        {
            BL_Usuario oBL_Usuario = new BL_Usuario();
            oBL_Usuario.GrabaInicioSesion(GlobalEntity.Instancia.Usuario, "0");
            Session.Abandon();
            FormsAuthentication.SignOut();
            Response.Redirect("../../eFENIX_Login.aspx");
            Response.End();
        }
    }


    protected void pMuestraDetalle()
    {
        BE_Presupuesto oBE_Presupuesto = new BE_Presupuesto();
        BL_Presupuesto oBL_Presupuesto = new BL_Presupuesto();

        oBE_Presupuesto.sPeriodo = DplPeriodo.SelectedValue;
        oBE_Presupuesto.iMesIni = Convert.ToInt32(DplMesIni.SelectedValue);
        oBE_Presupuesto.iMesFin = Convert.ToInt32(DplMesFin.SelectedValue);
        oBE_Presupuesto.iOperacion = Convert.ToInt32(DplOperacion.SelectedValue);
        oBE_Presupuesto.iIdLineaNegocio = Convert.ToInt32(DplNegocio.SelectedValue);
        oBE_Presupuesto.iLimite = Convert.ToInt32(txtTop.Text);       

        List<BE_Presupuesto> lista = oBL_Presupuesto.ListarVtasXCliente(oBE_Presupuesto);

        GrvListado.DataSource = lista;
        GrvListado.DataBind();
    }

    protected void GrvListado_RowCreated(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Header)
        {
            GridView HeaderGrid = (GridView)sender;
            GridViewRow HeaderGridRow = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Insert);

            TableCell HeaderCell = new TableCell();
            HeaderCell.Text = "Item";
            HeaderGridRow.Cells.Add(HeaderCell);

            HeaderCell = new TableCell();
            HeaderCell.Text = "Cliente Facturado";
            HeaderGridRow.Cells.Add(HeaderCell);

            HeaderCell = new TableCell();
            HeaderCell.Text = "Ejecutivo";
            HeaderGridRow.Cells.Add(HeaderCell);

            HeaderCell = new TableCell();
            HeaderCell.Text = "Ventas " + DplPeriodo.SelectedValue.ToString();
            HeaderGridRow.Cells.Add(HeaderCell);

            HeaderCell = new TableCell();
            HeaderCell.Text = "Ventas" + (Convert.ToInt32( DplPeriodo.SelectedValue.ToString())-1).ToString();
            HeaderCell.ColumnSpan = 2;
            HeaderGridRow.Cells.Add(HeaderCell);

            HeaderCell = new TableCell();
            HeaderCell.Text = "Detalle";
            HeaderCell.ColumnSpan = 2;
            HeaderGridRow.Cells.Add(HeaderCell);

            GrvListado.Controls[0].Controls.AddAt(0, HeaderGridRow);

        }
    }



    protected void ImgBtnBuscar_Click(object sender, ImageClickEventArgs e)
    {
        if (Microsoft.VisualBasic.Information.IsNumeric(txtTop.Text))
            pMuestraDetalle();
        else
        {            
            txtTop.Text = "10";

            pMuestraDetalle();
        }
    }

    protected void GrvListado_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        List<BE_Tabla> olst_TablaTipo = new List<BE_Tabla>();
        BL_Presupuesto oBL_Presupuesto = new BL_Presupuesto();
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            DataKey dataKey = this.GrvListado.DataKeys[e.Row.RowIndex];
            BE_Presupuesto oitem = (e.Row.DataItem as BE_Presupuesto);

            if (oitem.iIdCliente == 0)
                e.Row.Visible = false;

            else
            {                   
                e.Row.Cells[3].BackColor = ColorTranslator.FromHtml("#A8E7B6");

                if (oitem.dPorc < 0)
                    e.Row.Cells[5].ForeColor = System.Drawing.Color.Red;

                if (oitem.iItem == 0)  //Total
                {
                    e.Row.Cells[0].Text = "";
                    e.Row.Cells[6].Visible = false;
                    e.Row.Cells[7].Visible = false;

                    for (int c = 0; c < e.Row.Cells.Count; c++)
                    {
                        e.Row.Cells[c].Font.Size = 10;
                        e.Row.Cells[c].Font.Bold = true;

                        e.Row.Cells[c].BackColor = System.Drawing.Color.LightGray;
                    }
                }
                else
                {
                    if (oitem.iItem == Convert.ToInt32(txtTop.Text) + 1)  //Total
                    {
                        e.Row.Cells[6].Visible = false;
                        e.Row.Cells[7].Visible = false;
                    }
                    else
                    {
                        ImageButton ibtn = (ImageButton)e.Row.Cells[6].Controls[0];
                        if (Request.QueryString["Linea"] == null || Request.QueryString["Linea"] == "0")
                            ibtn.OnClientClick = String.Format("AbrirPagina('{0}','{1}');", dataKey.Values["iIdCliente"].ToString(), dataKey.Values["iIdVendedor"].ToString());
                        else
                            ibtn.Enabled = false;


                        ImageButton ibtnServicio = (ImageButton)e.Row.Cells[7].Controls[0];
                        ibtnServicio.OnClientClick = String.Format("AbrirPagServicio('{0}','{1}');", dataKey.Values["iIdCliente"].ToString(), dataKey.Values["iIdVendedor"].ToString());
                    }
                }
            }
        }
        else if (e.Row.RowType == DataControlRowType.Header)
        {
            e.Row.Cells[3].BackColor = ColorTranslator.FromHtml("#0069ae");
        }
    }

}