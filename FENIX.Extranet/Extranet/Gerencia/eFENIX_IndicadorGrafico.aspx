<%@ Page Language="C#" MasterPageFile="~/Extranet/Seguridad/eFENIX_Principal.master"
    AutoEventWireup="true" CodeFile="eFENIX_IndicadorGrafico.aspx.cs" Inherits="Extranet_Gerencia_eFENIX_IndicadorGrafico"
    Title="Indicador Gr�fico" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Charting" TagPrefix="telerik" %>
<%--<%@ Register Assembly="Infragistics35.WebUI.UltraWebChart.v11.1, Version=11.1.20111.1006, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb"
    Namespace="Infragistics.WebUI.UltraWebChart" TagPrefix="igchart" %>
<%@ Register Assembly="Infragistics35.WebUI.UltraWebChart.v11.1, Version=11.1.20111.1006, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb"
    Namespace="Infragistics.UltraChart.Resources.Appearance" TagPrefix="igchartprop" %>
<%@ Register Assembly="Infragistics35.WebUI.UltraWebChart.v11.1, Version=11.1.20111.1006, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb"
    Namespace="Infragistics.UltraChart.Data" TagPrefix="igchartdata" %>--%>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CntMaster" runat="Server">
    <table cellspacing="0" cellpadding="0" style="width: 100%;">
        <tr>
            <td class="form_titulo" style="width: 85%">
                GR�FICO DE INDICADOR
            </td>
            <td align="right" class="form_titulo_verde" style="width: 15%">
                <asp:TextBox ID="LblTotal" Visible="false" Text="" Style="background-color: #2BA143;
                    color: #FFFFFF" ReadOnly="true" BorderColor="#2BA143" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td valign="top" colspan="2">
                <table class="form_bandeja" style="margin-top: 5px; margin-bottom: 5px; width: 100%;">
                    <tr>
                        <td colspan="7">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Indicador
                        </td>
                        <td colspan="3">
                            <asp:DropDownList ID="DplNegocio" runat="server" Width="320px" AutoPostBack="True"
                                OnSelectedIndexChanged="DplNegocio_SelectedIndexChanged">
                            </asp:DropDownList>
                        </td>
                        <td colspan="2">
                            <ajax:UpdatePanel ID="UpModoGrafico" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <asp:RadioButtonList ID="RblModoGrafico" runat="server" CellPadding="0" CellSpacing="0"
                                        RepeatColumns="2">
                                        <asp:ListItem Selected="True" Value="1">Agrupada</asp:ListItem>
                                        <asp:ListItem Value="2">Detallada</asp:ListItem>
                                    </asp:RadioButtonList>
                                </ContentTemplate>
                                <Triggers>
                                    <ajax:AsyncPostBackTrigger ControlID="DplNegocio" EventName="SelectedIndexChanged" />
                                </Triggers>
                            </ajax:UpdatePanel>
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 13%;">
                            Periodo
                        </td>
                        <td style="width: 6%;">
                            <asp:DropDownList ID="DplAnioPeriodo" runat="server">
                            </asp:DropDownList>
                        </td>
                        <td colspan="2" style="width: 36%;">
                            <asp:DropDownList ID="DplSemestre" runat="server">
                            </asp:DropDownList>
                        </td>
                        <td style="width: 15%;">
                        </td>
                        <td style="width: 15%;">
                        </td>
                        <td style="width: 14%;">
                            <asp:ImageButton ID="ImgBtnBuscar" runat="server" ImageUrl="~/Imagenes/Formulario/btn_consultar_1.JPG"
                                OnClick="ImgBtnBuscar_Click" BorderColor="#FA3530" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="6">
                            &nbsp;
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr valign="top" style="height: 100%">
            <td colspan="5">
                <ajax:UpdatePanel ID="upGraficoPopUp" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div id="DvGraficoPie" style="overflow: auto; width: 100%; height: 100%" runat="server">
                            <telerik:RadChart ID="RadChart2" runat="server" DefaultType="Pie" Width="667px" Skin="Colorful"
                                OnClick="RadChart2_Click" Visible="False">
                                <ChartTitle>
                                    <Appearance Corners="Round, Round, Round, Round, 6" Dimensions-Margins="4%, 10px, 14px, 0%"
                                        Position-AlignedPosition="Top">
                                        <FillStyle MainColor="224, 224, 224" GammaCorrection="False">
                                        </FillStyle>
                                        <Border Color="DimGray" />
                                    </Appearance>
                                    <TextBlock>
                                        <Appearance TextProperties-Font="Verdana, 11.25pt">
                                        </Appearance>
                                    </TextBlock>
                                </ChartTitle>
                                <PlotArea>
                                    <XAxis>
                                        <Appearance>
                                            <MajorGridLines Color="DimGray" Width="0" />
                                        </Appearance>
                                        <AxisLabel>
                                            <TextBlock>
                                                <Appearance TextProperties-Font="Verdana, 9.75pt, style=Bold">
                                                </Appearance>
                                            </TextBlock>
                                        </AxisLabel>
                                    </XAxis>
                                    <YAxis>
                                        <Appearance>
                                            <MajorGridLines Color="DimGray" />
                                        </Appearance>
                                        <AxisLabel>
                                            <TextBlock>
                                                <Appearance TextProperties-Font="Verdana, 9.75pt, style=Bold">
                                                </Appearance>
                                            </TextBlock>
                                        </AxisLabel>
                                    </YAxis>
                                    <YAxis2>
                                        <AxisLabel>
                                            <TextBlock>
                                                <Appearance TextProperties-Font="Verdana, 9.75pt, style=Bold">
                                                </Appearance>
                                            </TextBlock>
                                        </AxisLabel>
                                    </YAxis2>
                                    <Appearance Dimensions-Margins="18%, 23%, 12%, 10%">
                                        <FillStyle MainColor="254, 255, 228" SecondColor="Transparent">
                                        </FillStyle>
                                        <Border Color="226, 218, 202" />
                                    </Appearance>
                                </PlotArea>
                                <Legend>
                                    <Appearance>
                                        <ItemTextAppearance TextProperties-Color="DimGray">
                                        </ItemTextAppearance>
                                        <Border Color="DimGray" />
                                    </Appearance>
                                </Legend>
                                <Series>
                                    <telerik:ChartSeries Name="Series 1" Type="Pie">
                                        <Appearance>
                                            <FillStyle MainColor="255, 186, 74" SecondColor="255, 244, 227">
                                            </FillStyle>
                                            <Border Color="DimGray" />
                                        </Appearance>
                                        <Items>
                                            <telerik:ChartSeriesItem Name="Item 1">
                                            </telerik:ChartSeriesItem>
                                        </Items>
                                    </telerik:ChartSeries>
                                    <telerik:ChartSeries Name="Series 2" Type="Pie">
                                        <Appearance>
                                            <FillStyle MainColor="21, 197, 22" SecondColor="218, 246, 218">
                                            </FillStyle>
                                            <Border Color="DimGray" />
                                        </Appearance>
                                    </telerik:ChartSeries>
                                </Series>
                            </telerik:RadChart>
                        </div>
                        <div id="DvGraficoBar" style="overflow: auto; width: 100%; height: 100%" runat="server">
                            <asp:GridView ID="GrvDetalle" runat="server" AutoGenerateColumns="False" DataKeyNames="sNombreIndicador"
                                SkinID="GrillaConsultaUnColor" Width="78%" Visible="false">
                                <Columns>
                                    <asp:BoundField DataField="sNombreIndicador" HeaderText="Descripcion" HeaderStyle-HorizontalAlign="Left">
                                        <ItemStyle Width="36%" HorizontalAlign="Left" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="dMes8" HeaderText="Mes 1" ItemStyle-HorizontalAlign="Right">
                                        <ItemStyle Width="9%" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="dMes9" HeaderText="Mes 2" ItemStyle-HorizontalAlign="Right">
                                        <ItemStyle Width="9%" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="dMes10" HeaderText="Mes 3" ItemStyle-HorizontalAlign="Right">
                                        <ItemStyle Width="9%" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="dMes11" HeaderText="Mes 4" ItemStyle-HorizontalAlign="Right">
                                        <ItemStyle Width="9%" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="dMes12" HeaderText="Mes 5" ItemStyle-HorizontalAlign="Right">
                                        <ItemStyle Width="9%" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="dMes13" HeaderText="Mes 6" ItemStyle-HorizontalAlign="Right">
                                        <ItemStyle Width="9%" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="dMes" HeaderText="TOTAL" ItemStyle-HorizontalAlign="Right">
                                        <ItemStyle Width="10%" />
                                    </asp:BoundField>
                                </Columns>
                            </asp:GridView>
                            <telerik:RadChart ID="RadChart1" runat="server" Height="291px" Skin="Colorful" Width="1050px"
                                Visible="False" DefaultType="Line" IntelligentLabelsEnabled="True">
                                <ChartTitle>
                                    <Appearance Corners="Round, Round, Round, Round, 6" Dimensions-Margins="4%, 10px, 14px, 0%"
                                        Position-AlignedPosition="Top">
                                        <FillStyle GammaCorrection="False" MainColor="224, 224, 224">
                                        </FillStyle>
                                        <Border Color="DimGray" />
                                    </Appearance>
                                    <TextBlock>
                                        <Appearance TextProperties-Font="Verdana, 11.25pt">
                                        </Appearance>
                                    </TextBlock>
                                </ChartTitle>
                                <PlotArea>
                                    <DataTable Visible="True">
                                        <Appearance>
                                            <FillStyle FillType="Solid" MainColor="AliceBlue">
                                            </FillStyle>
                                        </Appearance>
                                    </DataTable>
                                    <XAxis>
                                        <Appearance>
                                            <MajorGridLines Color="DimGray" Width="0" />
                                        </Appearance>
                                        <AxisLabel>
                                            <TextBlock>
                                                <Appearance TextProperties-Font="Verdana, 9.75pt, style=Bold">
                                                </Appearance>
                                            </TextBlock>
                                        </AxisLabel>
                                    </XAxis>
                                    <YAxis>
                                        <Appearance>
                                            <MajorGridLines Color="DimGray" />
                                        </Appearance>
                                        <AxisLabel>
                                            <TextBlock>
                                                <Appearance TextProperties-Font="Verdana, 9.75pt, style=Bold">
                                                </Appearance>
                                            </TextBlock>
                                        </AxisLabel>
                                    </YAxis>
                                    <YAxis2>
                                        <AxisLabel>
                                            <TextBlock>
                                                <Appearance TextProperties-Font="Verdana, 9.75pt, style=Bold">
                                                </Appearance>
                                            </TextBlock>
                                        </AxisLabel>
                                    </YAxis2>
                                    <Appearance Dimensions-Margins="18%, 23%, 50px, 10%" Position-Auto="False" Position-X="99.8"
                                        Position-Y="52.38">
                                        <FillStyle MainColor="254, 255, 228" SecondColor="Transparent">
                                        </FillStyle>
                                        <Border Color="226, 218, 202" />
                                    </Appearance>
                                </PlotArea>
                                <Legend Visible="False">
                                    <Appearance Visible="False">
                                        <ItemTextAppearance TextProperties-Color="DimGray" AutoTextWrap="True">
                                        </ItemTextAppearance>
                                        <Border Color="DimGray" />
                                    </Appearance>
                                </Legend>
                                <Appearance Corners="Round, Round, Round, Round, 3">
                                </Appearance>
                                <Series>
                                    <telerik:ChartSeries Name="Series 1" Type="Line" YAxisType="Secondary">
                                        <Appearance>
                                            <FillStyle MainColor="255, 186, 74" SecondColor="255, 244, 227">
                                            </FillStyle>
                                            <Border Color="DimGray" />
                                        </Appearance>
                                    </telerik:ChartSeries>
                                    <telerik:ChartSeries Name="Series 2" Type="Line">
                                        <Appearance>
                                            <FillStyle MainColor="21, 197, 22" SecondColor="218, 246, 218">
                                            </FillStyle>
                                            <Border Color="DimGray" />
                                        </Appearance>
                                    </telerik:ChartSeries>
                                </Series>
                            </telerik:RadChart>
                        </div>
                        <div id="DvGraficoInfragastic" style="overflow: auto; width: 100%; height: 100%"
                            runat="server">
                            <%--<igchart:UltraChart ID="UltraChart1" runat="server" BackgroundImageFileName="" 
                                ChartType="Stack3DColumnChart" 
                                EmptyChartText="Data Not Available. Please call UltraChart.Data.DataBind() after setting valid Data.DataSource" 
                                Height="403px" Version="11.1" Width="878px" 
                                onfillscenegraph="UltraChart1_FillSceneGraph" Visible="false">
                                <Legend Visible="True">
                                </Legend>
                                <ColorModel AlphaLevel="255" ColorBegin="Pink" ColorEnd="DarkRed" 
                                    ModelStyle="CustomLinear">
                                </ColorModel>
                                <Axis>
                                    <PE ElementType="None" Fill="Cornsilk" />
                                    <X LineThickness="1" TickmarkInterval="0" TickmarkStyle="Smart" Visible="True">
                                        <MajorGridLines AlphaLevel="255" Color="Gainsboro" DrawStyle="Dot" 
                                            Thickness="1" Visible="True" />
                                        <MinorGridLines AlphaLevel="255" Color="LightGray" DrawStyle="Dot" 
                                            Thickness="1" Visible="False" />
                                        <Labels Font="Verdana, 7pt" FontColor="DimGray" HorizontalAlign="Near" 
                                            ItemFormatString="" Orientation="VerticalLeftFacing" VerticalAlign="Center">
                                            <SeriesLabels Font="Verdana, 7pt" FontColor="DimGray" FormatString="" 
                                                HorizontalAlign="Near" Orientation="VerticalLeftFacing" VerticalAlign="Center">
                                                <Layout Behavior="Auto">
                                                </Layout>
                                            </SeriesLabels>
                                            <Layout Behavior="Auto">
                                            </Layout>
                                        </Labels>
                                    </X>
                                    <Y LineThickness="1" RangeMax="100000" RangeMin="-100000" TickmarkInterval="10" 
                                        TickmarkStyle="Smart" Visible="True">
                                        <MajorGridLines AlphaLevel="255" Color="Gainsboro" DrawStyle="Dot" 
                                            Thickness="1" Visible="True" />
                                        <MinorGridLines AlphaLevel="255" Color="LightGray" DrawStyle="Dot" 
                                            Thickness="1" Visible="False" />
                                        <Labels Font="Verdana, 7pt" FontColor="DimGray" HorizontalAlign="Far" 
                                            ItemFormatString="&lt;DATA_VALUE:00.##&gt;" Orientation="Horizontal" 
                                            VerticalAlign="Center">
                                            <SeriesLabels Font="Verdana, 7pt" FontColor="DimGray" FormatString="" 
                                                HorizontalAlign="Far" Orientation="Horizontal" VerticalAlign="Center">
                                                <Layout Behavior="Auto">
                                                </Layout>
                                            </SeriesLabels>
                                            <Layout Behavior="Auto">
                                            </Layout>
                                        </Labels>
                                    </Y>
                                    <Y2 LineThickness="1" TickmarkInterval="10" TickmarkStyle="Smart" 
                                        Visible="False">
                                        <MajorGridLines AlphaLevel="255" Color="Gainsboro" DrawStyle="Dot" 
                                            Thickness="1" Visible="True" />
                                        <MinorGridLines AlphaLevel="255" Color="LightGray" DrawStyle="Dot" 
                                            Thickness="1" Visible="False" />
                                        <Labels Font="Verdana, 7pt" FontColor="Gray" HorizontalAlign="Near" 
                                            ItemFormatString="&lt;DATA_VALUE:00.##&gt;" Orientation="Horizontal" 
                                            VerticalAlign="Center" Visible="False">
                                            <SeriesLabels Font="Verdana, 7pt" FontColor="Gray" FormatString="" 
                                                HorizontalAlign="Near" Orientation="Horizontal" VerticalAlign="Center">
                                                <Layout Behavior="Auto">
                                                </Layout>
                                            </SeriesLabels>
                                            <Layout Behavior="Auto">
                                            </Layout>
                                        </Labels>
                                    </Y2>
                                    <X2 LineThickness="1" TickmarkInterval="0" TickmarkStyle="Smart" 
                                        Visible="False">
                                        <MajorGridLines AlphaLevel="255" Color="Gainsboro" DrawStyle="Dot" 
                                            Thickness="1" Visible="True" />
                                        <MinorGridLines AlphaLevel="255" Color="LightGray" DrawStyle="Dot" 
                                            Thickness="1" Visible="False" />
                                        <Labels Font="Verdana, 7pt" FontColor="Gray" HorizontalAlign="Far" 
                                            ItemFormatString="&lt;ITEM_LABEL&gt;" Orientation="VerticalLeftFacing" 
                                            VerticalAlign="Center" Visible="False">
                                            <SeriesLabels Font="Verdana, 7pt" FontColor="Gray" FormatString="" 
                                                HorizontalAlign="Far" Orientation="VerticalLeftFacing" VerticalAlign="Center">
                                                <Layout Behavior="Auto">
                                                </Layout>
                                            </SeriesLabels>
                                            <Layout Behavior="Auto">
                                            </Layout>
                                        </Labels>
                                    </X2>
                                    <Z LineThickness="1" TickmarkInterval="0" TickmarkStyle="Smart" Visible="True">
                                        <MajorGridLines AlphaLevel="255" Color="Gainsboro" DrawStyle="Dot" 
                                            Thickness="1" Visible="True" />
                                        <MinorGridLines AlphaLevel="255" Color="LightGray" DrawStyle="Dot" 
                                            Thickness="1" Visible="False" />
                                        <Labels Font="Verdana, 7pt" FontColor="DimGray" HorizontalAlign="Far" 
                                            ItemFormatString="&lt;ITEM_LABEL&gt;" Orientation="Horizontal" 
                                            VerticalAlign="Center">
                                            <SeriesLabels Font="Verdana, 7pt" FontColor="DimGray" HorizontalAlign="Far" 
                                                Orientation="Horizontal" VerticalAlign="Center">
                                                <Layout Behavior="Auto">
                                                </Layout>
                                            </SeriesLabels>
                                            <Layout Behavior="Auto">
                                            </Layout>
                                        </Labels>
                                    </Z>
                                    <Z2 LineThickness="1" TickmarkInterval="0" TickmarkStyle="Smart" 
                                        Visible="False">
                                        <MajorGridLines AlphaLevel="255" Color="Gainsboro" DrawStyle="Dot" 
                                            Thickness="1" Visible="True" />
                                        <MinorGridLines AlphaLevel="255" Color="LightGray" DrawStyle="Dot" 
                                            Thickness="1" Visible="False" />
                                        <Labels Font="Verdana, 7pt" FontColor="Gray" HorizontalAlign="Near" 
                                            ItemFormatString="&lt;ITEM_LABEL&gt;" Orientation="Horizontal" 
                                            VerticalAlign="Center" Visible="False">
                                            <SeriesLabels Font="Verdana, 7pt" FontColor="Gray" HorizontalAlign="Near" 
                                                Orientation="Horizontal" VerticalAlign="Center">
                                                <Layout Behavior="Auto">
                                                </Layout>
                                            </SeriesLabels>
                                            <Layout Behavior="Auto">
                                            </Layout>
                                        </Labels>
                                    </Z2>
                                </Axis>
                                <Effects>
                                    <Effects>
                                        <igchartprop:GradientEffect />
                                    </Effects>
                                </Effects>
                                <TitleBottom Font="Microsoft Sans Serif, 7.8pt, style=Italic" 
                                    HorizontalAlign="Far" Location="Bottom">
                                </TitleBottom>
                                <Tooltips Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                    Font-Strikeout="False" Font-Underline="False" />
                            </igchart:UltraChart>--%>
                        </div>
                    </ContentTemplate>
                    <Triggers>
                        <ajax:AsyncPostBackTrigger ControlID="ImgBtnBuscar" EventName="Click" />
                    </Triggers>
                </ajax:UpdatePanel>
            </td>
        </tr>
    </table>
    <script language="javascript" type="text/javascript">

        var tiempo = 0;
        var seconds = 0;

        var myVar = setInterval(function () {
            clearInterval(myVar);
        }, 1000);

        var myTiempo = setTimeout(function () {
            clearInterval(myTiempo);
        }, 1);

        function myStopFunction() {
            clearInterval(myVar);
            seconds = '<%=GlobalEntity.Instancia.TiempoDuracionSesion%>' * 60;
            tiempo = '<%=GlobalEntity.Instancia.TiempoDuracionSesion%>' * 1000 * 60;

            CuentaTiempo(tiempo);
        }

        function CuentaTiempo(tiempo) {
            clearTimeout(myTiempo);
            myTiempo = setTimeout(function () {
                document.getElementById('<%=btn_cerrar.ClientID%>').click();
                //window.location = "../Seguridad/eFENIX_Login.aspx";
            }, tiempo);
        }



        function SessionExpireAlert(timeout) {
            seconds = timeout / 1000;
            document.getElementsByName("seconds").innerHTML = seconds;

            myvar = setInterval(function () {
                seconds--;
                document.getElementById("seconds").innerHTML = seconds;
            }, 1000);

            CuentaTiempo(timeout);
        };

        var goodexit = false;
        window.onbeforeunload = confirmRegisterExit;
        function confirmRegisterExit(evt) {
            if (!goodexit) {
                //return "Si vas abandonar el sistema haz click en Cerrar Sesion!!";
                document.getElementById('<%=btn_cerrar.ClientID%>').click();
            }
        }

    </script>

</asp:Content>
<asp:Content ID="Content3" runat="server" contentplaceholderid="ContentCab">
    <span class="texto_titulo" >Tu Sesion Expirara en &nbsp; 
        <span class="texto_titulo" id="seconds"> </span>
        <span>&nbsp;seg.</span>
     </span>
     <asp:Button ID="btn_cerrar" runat="server" Text="cerrar" 
        onclick="btn_cerrar_Click"  Style="display: none;" />
</asp:Content>

