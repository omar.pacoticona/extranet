﻿<%@ Page Language="C#" 
AutoEventWireup="true" 
CodeFile="eFENIX_Login.aspx.cs" 
Theme="SkinFile"
Inherits="FENIX_Login" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Login e-Fenix</title>
    
    <link href="../../Script/Hojas_Estilo/eFenix.css" rel="stylesheet" type="text/css" />
    
    <script language="javascript" type="text/javascript">

        
        function fc_borrar()
        {
            document.getElementById('<%=TxtPassword.ClientID %>').value = '';
            document.getElementById('<%=TxtUsuario.ClientID %>').value = '';           
            return false;
        } 

        function fc_linkCambiarContrasenha() {
            Mostrar('block');
        }

        function fc_linkOlvidoContrasenha() {
            document.getElementById("<%=this.TxtDNI.ClientID %>").value == ""
            if (document.getElementById("<%=this.TxtUsuario.ClientID %>").value == "") {
                alert("Debe ingresar el usuario");
            }
            else {
                MostrarAlerta('block');
            }
        }
        
        function fc_LimpiarCambioContrasenha() {
            document.getElementById("<%=this.txtClave.ClientID %>").value = "";
            document.getElementById("<%=this.txtNuevaClave.ClientID %>").value = "";
            document.getElementById("<%=this.txtNuevaClaveConf.ClientID %>").value = "";
            return false;
        }

        function fc_AceptarCambioContrasenha() {
            var msjError = "";

            if (document.getElementById("<%=this.txtLogin.ClientID %>").value == "") {
                msjError += "- Debe ingresar el usuario.\n";
            }
            if (document.getElementById("<%=this.txtClave.ClientID %>").value == "") {
                msjError += "- Debe ingresar su contraseña actual.\n";
            }
            if (document.getElementById("<%=this.txtNuevaClave.ClientID %>").value == "") {
                msjError += "- Debe ingresar su contraseña nueva.\n";
            }
            if (document.getElementById("<%=this.txtNuevaClave.ClientID %>").value != "") {
                if (document.getElementById("<%=this.txtNuevaClave.ClientID %>").value.length < 8) {
                    msjError += "- La contraseña nueva debe de ser de mínimo 8 carácteres.\n";
                }
            }
            if (document.getElementById("<%=this.txtNuevaClaveConf.ClientID %>").value == "") {
                msjError += "- Debe ingresar la confirmación de la contraseña.\n";
            }
            if (document.getElementById("<%=this.txtNuevaClaveConf.ClientID %>").value != "") {
                if (document.getElementById("<%=this.txtNuevaClaveConf.ClientID %>").value.length < 8) {
                    msjError += "- La confirmación de la contraseña debe de ser de mínimo 8 carácteres.\n";
                }
            }
            if (document.getElementById("<%=this.txtNuevaClave.ClientID %>").value != "" &&
                    document.getElementById("<%=this.txtNuevaClaveConf.ClientID %>").value != "" &&
                    document.getElementById("<%=this.txtNuevaClave.ClientID %>").value != document.getElementById("<%=this.txtNuevaClaveConf.ClientID %>").value) {
                msjError += "- La confirmación de la nueva contraseña no es válida.\n";
            }

            if (msjError != "") {
                alert(msjError);
                return false;
            }

            return confirm("Esta Seguro que desea cambiar su contraseña");
        }
        
        function fc_CerrarCambioContrasenha() {
            fc_LimpiarCambioContrasenha();
            Mostrar('none');
            return false;
        }
        
        function fc_CerrarRecordarContrasenha() {
            MostrarAlerta('none');
            return false;
        }

        function Mostrar(estilo) {
            document.getElementById("<%=this.txtLogin.ClientID %>").value = document.getElementById("<%=this.TxtUsuario.ClientID %>").value;
            document.getElementById('<%=pnlCambioContrasenha.ClientID%>').style.left = (screen.width - 350) / 2 + 'px';
            document.getElementById('<%=pnlCambioContrasenha.ClientID%>').style.top = (screen.height - 350) / 2 + 'px';

            document.getElementById('<%=__PopPanel__.ClientID%>').style.width = screen.width + 'px';
            document.getElementById('<%=__PopPanel__.ClientID%>').style.height = screen.height + 'px';

            document.getElementById('<%=__PopPanel__.ClientID%>').style.display = estilo;
            document.getElementById('<%=pnlCambioContrasenha.ClientID%>').style.display = estilo;
        }

        function GrabaryMostrar(estilo) {
            document.getElementById('<%=__PopPanel__.ClientID%>').style.display = estilo;
            document.getElementById('<%=pnlCambioContrasenha.ClientID%>').style.display = estilo;
            
            alert("Contraseña actualizada correctamente");
        }

        function MostrarAlerta(estilo) {
            document.getElementById('<%=pnlRecordarContrasenha.ClientID%>').style.left = (screen.width - 350) / 2 + 'px';
            document.getElementById('<%=pnlRecordarContrasenha.ClientID%>').style.top = (screen.height - 350) / 2 + 'px';

            document.getElementById('<%=__PopPanel__.ClientID%>').style.width = screen.width + 'px';
            document.getElementById('<%=__PopPanel__.ClientID%>').style.height = screen.height + 'px';

            document.getElementById('<%=this.TxtUser.ClientID%>').value = document.getElementById('<%=this.TxtUsuario.ClientID%>').value;
            
            document.getElementById('<%=__PopPanel__.ClientID%>').style.display = estilo;
            document.getElementById('<%=pnlRecordarContrasenha.ClientID%>').style.display = estilo;
        }
        
        function GrabraryMostrarAlerta(estilo) {
            document.getElementById('<%=__PopPanel__.ClientID%>').style.display = estilo;
            document.getElementById('<%=pnlRecordarContrasenha.ClientID%>').style.display = estilo;
            
            alert("Correo enviado correctamente, verifique su buzón");
        }

        
    </script>
    <style type="text/css">
        .style1
        {
            width: 100%;
        }
 
    </style>
</head>


<body  style="margin:0px;">
    <form id="form1" runat="server">
        <ajax:ScriptManager ID="scmLogin"  runat="server">
        </ajax:ScriptManager>

        <div style="height: 100%; width: 100%;padding-top:100px;">    
            <table cellpadding="0" cellspacing="0" border="0px" align="center" width="100%" >
                <tr>
                    <td style="width:28%;height:115px;">
                    
                    </td>
                    <td style="width:35%;text-align:center;" >
                        <img src="../../Imagenes/login_logo.jpg" />
                    </td>
                    <td style="width:37%;">
                    
                    </td>
                </tr>
                <tr style="height:195px;background-color:#243F90;">
                    <td>

                    </td>
                    <td align="center" valign="middle">
                        <table cellpadding="0" cellspacing="1" class="tabla_login" >
                            <tr>
                                <td align="center" valign="middle" >
                                    <img src="../../Imagenes/login_user.JPG" />
                                </td>
                                <td style="text-align:right;" >
                                    Usuario
                                </td>
                                <td style="padding-left:10px;" >
                                    <ajax:UpdatePanel ID="upUsuario" runat="server" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <asp:TextBox ID="TxtUsuario" Width="180px" runat="server" MaxLength="20" ValidationGroup="vgIngreso"></asp:TextBox>
                                        </ContentTemplate>
                                        <Triggers>
                                            <ajax:AsyncPostBackTrigger ControlID="ImbIngreso" EventName="Click" />
                                        </Triggers>
                                    </ajax:UpdatePanel>
                                </td>
                            </tr>
                            <tr>
                                <td ></td>
                                <td ></td>
                                <td style="padding-left:10px;">
                                    <asp:RequiredFieldValidator ID="rvTxtUsuario" runat="server" 
                                        ControlToValidate="TxtUsuario" ErrorMessage="Ingrese un usuario"
                                        SetFocusOnError="True" ValidationGroup="vgIngreso"  
                                        Font-Size="X-Small" Font-Names="verdana" >
                                    </asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td align="center" valign="middle" >
                                    <img src="../../Imagenes/login_pwd.JPG" />
                                </td>
                                <td style="text-align:right;" >
                                    Contraseña
                                </td>
                                <td style="padding-left:10px;">
                                    <asp:TextBox ID="TxtPassword" Width="180px" runat="server" Text="user1" TextMode="Password" MaxLength="20" ValidationGroup="vgIngreso"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td></td>
                                <td ></td>
                                <td style="padding-left:10px;">
                                    <asp:RequiredFieldValidator ID="rvTxtPassword" runat="server" 
                                        ControlToValidate="TxtPassword" ErrorMessage="Ingrese su clave" 
                                        SetFocusOnError="True" ValidationGroup="vgIngreso" 
                                         Font-Size="X-Small" Font-Names="verdana" >
                                    </asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td ></td>
                                <td style="text-align:right;">Texto de la Imagen</td>
                                <td style="padding-left:10px;" >
                                    <asp:TextBox ID="TxtCodCaptcha" runat="server" MaxLength="5" Width="180px"></asp:TextBox>
                                </td>
                            </tr>
                            
                            <tr>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td style="text-align:right;vertical-align:top;" >
                                    &nbsp;</td>
                            </tr>
                            
                            <tr>
                                <td></td>
                                <td></td>
                                <td style="text-align:right;vertical-align:top; padding-left:10px;">
                                    <asp:HyperLink ID="linkOlvidoContrasenha" runat="server" Text="¿No recuerdo mi contraseña?"
                                                Style="cursor: pointer" onClick="javascript: fc_linkOlvidoContrasenha();" Font-Size="11px"
                                                Font-Names="Arial" Font-Bold="true" ForeColor="White">
                                    </asp:HyperLink>
                                    <br>
                                    <asp:HyperLink ID="linkCambiarContrasenha" runat="server" Text="¿Deseo cambiar mi contraseña?"
                                                Style="cursor: pointer" onClick="javascript: fc_linkCambiarContrasenha();" Font-Size="11px"
                                                Font-Names="Arial" Font-Bold="true" ForeColor="White">
                                    </asp:HyperLink>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td valign="bottom">  
                    <ajax:UpdatePanel ID="UpdCaptcha" runat="server" >
                                        <ContentTemplate>
                        <table class="style1">
                            <tr>
                                <td>                                    
                                     <asp:LinkButton ID="lnkNewImagen" runat="server" ForeColor="White" Font-Size="11px"
                                                    Font-Names="Arial" Font-Bold="True" Font-Overline="False" 
                                        onclick="lnkNewImagen_Click"> Mostrar otra imagen</asp:LinkButton>       
                                                                                                                                         
                                </td>
                            </tr>
                            <tr>
                                <td>                                    
                                     <asp:Image ID="imgCaptcha"  runat="server" />
                                     <asp:Image ID="imgCaptcha2"  runat="server" />                                                                                               
                                </td>
                            </tr>
                        </table>
                        </ContentTemplate>
                                    
                                        <Triggers>
                                            <ajax:AsyncPostBackTrigger ControlID="lnkNewImagen" EventName="Click" />
                                        </Triggers>
                                    </ajax:UpdatePanel>
                    </td>
                </tr>
                <tr>
                    <td align="right">
                          <img src="../../Imagenes/login_picture.JPG" style="margin-top:2px;" />  
                    </td>
                    <td align="center" valign="middle">
                        <asp:ImageButton ID="ImbIngreso" runat="server" ImageUrl="~/Imagenes/btn_ingresar.JPG" 
                            onclick="ImbIngreso_Click" ValidationGroup="vgIngreso" />
                        <asp:ImageButton ID="ImbBorrar" runat="server" ImageUrl="~/Imagenes/btn_borrar.JPG" 
                            OnClientClick="return fc_borrar();" ValidationGroup="vgIngreso"  style="margin-left:15px;" />
                    </td>
                    <td valign="top">
                        &nbsp;</td>
                </tr>
            </table>
        </div>
        
        
        
        <%-- Popup - Cambio de Contraseña--%> 
        <asp:Panel ID="__PopPanel__" runat="server" CssClass="modalBackground" Style="left: 0px;
                top: 0px; display: none; position: fixed; z-index: 10000; display: none;" />      

    
        <asp:Panel ID="pnlCambioContrasenha" runat="server" Style=" display:none; width: 390px;
                position: fixed; z-index: 100002;border: dimgray 1px solid;background-color: White;">
            <ajax:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <table  border="0" cellpadding="0" cellspacing="0" width="100%" style="padding:5px;">
                        <tr valign="bottom" class="form_titulo" style="background-color:#005cab;height:15px;text-transform:none;">
                            <td style="padding:5px;">Cambio de Contraseña</td>
                        </tr>
                        <tr>
                            <td>
                                <table border="0" cellpadding="2" cellspacing="1" style="width:100%;margin-top: 5px; height:130px;" class="form_bandeja" >
                                    <tr>
                                        <td style="width: 170">Usuario</td>
                                        <td style="width: 180px">
                                            <asp:TextBox ID="txtLogin" runat="server" Width="150px" MaxLength="20"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Contraseña</td>
                                        <td>
                                            <asp:TextBox ID="txtClave" TextMode="Password" runat="server" Width="150px" MaxLength="20"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Nueva Contraseña</td>
                                        <td>
                                            <asp:TextBox ID="txtNuevaClave" TextMode="Password" runat="server" Width="150px" MaxLength="20"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Confirmar Contraseña</td>
                                        <td>
                                            <asp:TextBox ID="txtNuevaClaveConf" TextMode="Password" runat="server" Width="150px" MaxLength="20"></asp:TextBox>
                                        </td>
                                    </tr>                            
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td style="vertical-align:middle;text-align:right;"  class="form_bandeja" >
                              <asp:Button ID="btnAceptarCambioContrasenha" runat="server" CssClass="botonAceptar" 
                                    style="border-width:0px;" 
                                    OnClientClick="javascript: return fc_AceptarCambioContrasenha();" 
                                    OnClick="btnAceptarCambioContrasenha_Click" /> 
                              
                              <asp:Button ID="btnCancelarCambioContrasenha" runat="server" CssClass="botonCancelar" 
                                    style="border-width:0px;" 
                                    OnClientClick="javascript: return fc_CerrarCambioContrasenha();"/> 
                            </td>
                        </tr>
                    </table>
                </ContentTemplate>
            </ajax:UpdatePanel>
        </asp:Panel> 
    
        <asp:Panel ID="pnlRecordarContrasenha" runat="server" Style=" display:none;  width: 390px;
                position: fixed; z-index: 100002;border: dimgray 1px solid;background-color: White;">
            <ajax:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <table  border="0" cellpadding="0" cellspacing="0" width="100%" style="padding:5px;">
                        <tr valign="bottom" class="form_titulo" style="background-color:#005cab;height:15px;text-transform:none;">
                            <td style="padding:5px;">Recordar Contraseña</td>
                        </tr>
                        <tr>
                            <td>
                                <table border="0" cellpadding="2" cellspacing="1" style="width:100%;margin-top: 5px; height:130px;" class="form_bandeja" >
                                    <tr>
                                        <td align="justify">
                                            Usuario:</td>
                                        <td align="justify">
                                            <asp:TextBox ID="TxtUser" runat="server" Width="150px" MaxLength="20"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td align="justify">
                                            D.N.I. :</td>
                                        <td align="justify">
                                            <asp:TextBox ID="TxtDNI" runat="server"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="justify" colspan="2">
                                            Si no recuerda su contraseña pulse clic sobre el botón &quot;Aceptar&quot;. En breves 
                                            minutos el sistema le enviará un correo electrónico con un link para realizar 
                                            el cambio de su contraseña.
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        
                        <tr>
                            <td style="vertical-align:middle;text-align:right;"  class="form_bandeja" >
                              <asp:Button ID="btnAceptarRecordarContrasenha" runat="server" CssClass="botonAceptar" 
                                    style="border-width:0px;" 
                                    OnClick="btnAceptarRecordarContrasenha_Click" /> 
                              <asp:Button ID="btnCancelarRecordarContrasenha" runat="server" CssClass="botonCancelar" 
                                    style="border-width:0px;" 
                                    OnClientClick="javascript: return fc_CerrarRecordarContrasenha();"/> 
                            </td>                                    
                        </tr>
                    </table>
                </ContentTemplate>
            </ajax:UpdatePanel>
       </asp:Panel>
    </form>
</body>
</html>
