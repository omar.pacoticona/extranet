﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FENIX.Common;
using System.Web.Security;
using FENIX.BusinessEntity;
using FENIX.BusinessLogic;
using System.Configuration;

public partial class FENIX_Inicio : System.Web.UI.Page
{
    string _sFondoFenix;

    protected void Page_Load(object sender, EventArgs e)
    {
        String tiempo = (Convert.ToInt32(GlobalEntity.Instancia.TiempoDuracionSesion.ToString()) * 1000 * 60).ToString();
        Session["Reset"] = true;

        if (!Page.IsPostBack)
            ScriptManager.RegisterStartupScript(this, GetType(), "mensaje", "SessionExpireAlert(" + tiempo + ");", true);
        else
            ScriptManager.RegisterStartupScript(this, GetType(), "mensaje", "myStopFunction();", true);
    }


    public String sFondoFenix
    {
        get { return Constantes.RutaFondoFenix; ; }

    }

    protected void btn_cerrar_Click(object sender, EventArgs e)
    {
        if (GlobalEntity.Instancia.CerrarExtranet == "S")
        {
            BL_Usuario oBL_Usuario = new BL_Usuario();
            oBL_Usuario.GrabaInicioSesion(GlobalEntity.Instancia.Usuario, "0");
            Session.Abandon();
            FormsAuthentication.SignOut();
            Response.Redirect("../../eFENIX_Login.aspx");
            Response.End();
        }
    }

    public static void DescargarExeVariosNombres(System.Web.HttpResponse sResponse, String sRuta, String sNombreArchivo)
    {
        try
        {
            sResponse.ContentType = "application/octet-stream";
            sResponse.AddHeader("Content-Disposition", "attachment; filename=" + sNombreArchivo);
            sResponse.Clear();
            sResponse.WriteFile(sRuta + sNombreArchivo);
            sResponse.End();
        }
        catch (Exception ex)
        {

        }

    }

    protected void imgBtnDecargarWord_Click(object sender, EventArgs e)
    {
        string ruta = ConfigurationManager.AppSettings["RutaDescargaComunicado"];

        DescargarExeVariosNombres(Response, ruta, "Fargoline_Declaracion_Jurada.docx");
    }


}
