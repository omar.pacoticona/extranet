﻿<%@ Page Title="Inicio" Language="C#" MasterPageFile="~/Extranet/Seguridad/eFENIX_Principal.master"
    AutoEventWireup="true" CodeFile="eFENIX_Inicio.aspx.cs" Inherits="FENIX_Inicio" %>


<asp:Content ID="Content2" ContentPlaceHolderID="CntMaster" runat="Server">

    <table style="height: 100%; width: 100%; background-color: #E6E6E6;">
        <tr>
            <td style="background-repeat: no-repeat; background-position: center; text-align: center" background="../../Imagenes/fondo_eFenix.jpg">

                <asp:ImageButton ID="imgBtnComunicado" runat="server" ImageUrl="~/Imagenes/comunicado.png" Width="40%" OnClick="imgBtnDecargarWord_Click"  />
                <br />
                <br />
                <table align="center">
                    <tr>
                        <td>
                            <b>
                                <asp:Label  runat="server" Text="Descargar Carta de Responsabilidad" style="color:#27669D;" ></asp:Label>
                            </b>                           
                            
                            
                        </td>
                        <td onclick="imgBtnDecargarWord_Click">
                            <asp:ImageButton ID="imgBtnDecargarWord" runat="server" ImageUrl="~/Script/Imagenes/IconButton/word_logo.png" Width="60px" OnClick="imgBtnDecargarWord_Click" />
                        </td>
                    </tr>
                </table>

            </td>
        </tr>
    </table>

    <script language="javascript" type="text/javascript">

        var tiempo = 0;
        var seconds = 0;

        var myVar = setInterval(function () {
            clearInterval(myVar);
        }, 1000);

        var myTiempo = setTimeout(function () {
            clearInterval(myTiempo);
        }, 1);

        function myStopFunction() {
            clearInterval(myVar);
            seconds = '<%=GlobalEntity.Instancia.TiempoDuracionSesion%>' * 60;
            tiempo = '<%=GlobalEntity.Instancia.TiempoDuracionSesion%>' * 1000 * 60;

            CuentaTiempo(tiempo);
        }

        function CuentaTiempo(tiempo) {
            clearTimeout(myTiempo);
            myTiempo = setTimeout(function () {
                document.getElementById('<%=btn_cerrar.ClientID%>').click();
                //window.location = "../Seguridad/eFENIX_Login.aspx";
            }, tiempo);
        }



        function SessionExpireAlert(timeout) {
            seconds = timeout / 1000;
            document.getElementsByName("seconds").innerHTML = seconds;

            myvar = setInterval(function () {
                seconds--;
                document.getElementById("seconds").innerHTML = seconds;
            }, 1000);

            CuentaTiempo(timeout);
        };


        var goodexit = false;
        window.onbeforeunload = confirmRegisterExit;
        function confirmRegisterExit(evt) {
            if (!goodexit) {
                //return "Si vas abandonar el sistema haz click en Cerrar Sesion!!";
                document.getElementById('<%=btn_cerrar.ClientID%>').click();
            }
        }
    </script>

</asp:Content>
<asp:Content ID="Content3" runat="server" ContentPlaceHolderID="ContentCab">
    <span class="texto_titulo">Tu Sesion Expirara en &nbsp; 
        <span class="texto_titulo" id="seconds"></span>
        <span>&nbsp;seg.</span>
    </span>
    <asp:Button ID="btn_cerrar" runat="server" Text="cerrar"
        OnClick="btn_cerrar_Click" Style="display: none;" />
</asp:Content>


