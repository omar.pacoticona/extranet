﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using FENIX.BusinessEntity;
using FENIX.BusinessLogic;

public partial class Extranet_Seguridad_Usuarios : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        txtDni.Attributes.Add("onkeypress", "javascript:return SoloEnteros(event);");
        txtRucCli.Attributes.Add("onkeypress", "javascript:return SoloEnteros(event);");
        txtDNIB.Attributes.Add("onkeypress", "javascript:return SoloEnteros(event);");

        txtCliente.Attributes.Add("onkeydown", "javascript:return fc_focus(event, 'txtCliente');");
        txtUsu.Attributes.Add("onkeydown", "javascript:return fc_focus(event, 'txtUsu');");
        txtUsuario.Attributes.Add("onkeydown", "javascript:return fc_focus(event, 'txtUsuario');");
        txtNomUsuario.Attributes.Add("onkeydown", "javascript:return fc_focus(event, 'txtNomUsuario');");
        txtDni.Attributes.Add("onkeydown", "javascript:return fc_focus(event, 'txtDni');");
        txtCorreo.Attributes.Add("onkeydown", "javascript:return fc_focus(event, 'txtCorreo');");
        txtRucCli.Attributes.Add("onkeydown", "javascript:return fc_focus(event, 'txtRucCli');");
        txtDNIB.Attributes.Add("onkeydown", "javascript:return fc_focus(event, 'txtDni');");


        String tiempo = (Convert.ToInt32(GlobalEntity.Instancia.TiempoDuracionSesion.ToString()) * 1000 * 60).ToString();
        Session["Reset"] = true;

        if (!Page.IsPostBack)
        {
            //ListarDropDowList();
            HdTipo.Value = "I";
 
            List<BE_Usuario> lsBE_Usuarios = new List<BE_Usuario>();
            BE_Usuario oBE_Usuarios = new BE_Usuario();
            BL_Usuario oBL_Usuario = new BL_Usuario();
            lsBE_Usuarios.Add(oBE_Usuarios);


            GrvListado.DataSource = lsBE_Usuarios;
            GrvListado.DataBind();

            ViewState["AccesoAnularUsuario"] = oBL_Usuario.VerificaAccesoProcesoEspecial("ANULAR_USUEXTRANET", GlobalEntity.Instancia.Usuario);
                      

            ScriptManager.RegisterStartupScript(this, GetType(), "mensaje", "SessionExpireAlert(" + tiempo + ");", true);
        }
        else
            ScriptManager.RegisterStartupScript(this, GetType(), "mensaje", "myStopFunction();", true);
    }

    //#region "Método Controles"
    //protected void ListarDropDowList()
    //{
    //    try
    //    {
    //        BL_Presupuesto oBL_Presupuesto = new BL_Presupuesto();

    //        DplCliente.DataSource = oBL_Presupuesto.ListarCliente();
    //        DplCliente.DataValueField = "iIdCliente";
    //        DplCliente.DataTextField = "sCliente";
    //        DplCliente.DataBind();
    //        DplCliente.SelectedIndex = 0;

    //        DplClienteNew.DataSource = oBL_Presupuesto.ListarCliente();
    //        DplClienteNew.DataValueField = "iIdCliente";
    //        DplClienteNew.DataTextField = "sCliente";
    //        DplClienteNew.DataBind();
    //        DplClienteNew.SelectedIndex = 0;
    //    }
    //    catch (Exception e)
    //    {

    //    }
    //}
    //#endregion

    protected void btn_cerrar_Click(object sender, EventArgs e)
    {
        if (GlobalEntity.Instancia.CerrarExtranet == "S")
        {
            BL_Usuario oBL_Usuario = new BL_Usuario();
            oBL_Usuario.GrabaInicioSesion(GlobalEntity.Instancia.Usuario, "0");
            Session.Abandon();
            FormsAuthentication.SignOut();
            Response.Redirect("../../eFENIX_Login.aspx");
            Response.End();
        }
    }

    protected void pMuestraDetalle()
    {
        BE_Usuario oBE_Usuario = new BE_Usuario();
        BL_Usuario oBL_Usuario = new BL_Usuario();


        List<BE_Usuario> lista = oBL_Usuario.ListarUsuarios(txtCliente.Text, txtUsu.Text,txtDNIB.Text.Trim());

        lista.Add(oBE_Usuario);

        ViewState["oBE_DocumentoOrigenList"] = lista;

        GrvListado.DataSource = lista;
        GrvListado.DataBind();

    }

    protected void ImgBtnBuscar_Click(object sender, ImageClickEventArgs e)
    {
        pMuestraDetalle();
    }

    private void SCA_MsgInformacion(string strError)
    {
        MensajeScript(strError);
    }

    private void MensajeScript(string SMS)
    {
        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "", "<script>alert('" + SMS + "');</script>", false);
    }

    protected void GrvListado_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        
        if (e.CommandName == "Anular")
        {
            GridViewRow fila;
            fila = (GridViewRow)(((ImageButton)e.CommandSource).NamingContainer);
            int rowindex = fila.RowIndex;

            if (ViewState["AccesoAnularUsuario"].ToString() == "S")
            {
                Int32 iIdUsuario = Convert.ToInt32(GrvListado.DataKeys[fila.RowIndex].Values["iIdUsuario"].ToString());
                   // Convert.ToInt32(GrvListado.DataKeys[Convert.ToInt32(e.CommandArgument)].Values["UsuarioId"]);

                BL_Usuario oBL_Usuario = new BL_Usuario();
                BE_Usuario oBE_Usuario = new BE_Usuario();
                String[] sResultado = null;
                Int32 vl_iCodigo = 0;
                String vl_sMessage = String.Empty;

                oBE_Usuario.iIdUsuario = iIdUsuario;
                oBE_Usuario.sUsuarioActualiza = GlobalEntity.Instancia.Usuario;
                oBE_Usuario.sNombrePc = GlobalEntity.Instancia.NombrePc;

                sResultado = oBL_Usuario.AnularUsuario(oBE_Usuario).Split('|');

                for (int i = 0; i < sResultado.Length; i++)
                {
                    if (i == 0)
                    {
                        vl_iCodigo = Convert.ToInt32(sResultado[i]);
                    }
                    if (i == 1)
                    {
                        vl_sMessage += sResultado[i];
                    }
                }

                SCA_MsgInformacion(vl_sMessage);

                if (vl_iCodigo > 0)
                    pMuestraDetalle();
            }
            else
                SCA_MsgInformacion("No Tiene Permiso para Anular");
        }
        if(e.CommandName == "Habilitar")
        {
            GridViewRow fila;
            fila = (GridViewRow)(((ImageButton)e.CommandSource).NamingContainer);
            int rowindex = fila.RowIndex;
       
                Int32 iIdUsuario = Convert.ToInt32(GrvListado.DataKeys[fila.RowIndex].Values["iIdUsuario"].ToString());
                // Convert.ToInt32(GrvListado.DataKeys[Convert.ToInt32(e.CommandArgument)].Values["UsuarioId"]);

                BL_Usuario oBL_Usuario = new BL_Usuario();
                BE_Usuario oBE_Usuario = new BE_Usuario();


            oBL_Usuario.HabilitarUsuario(iIdUsuario);


            SCA_MsgInformacion("Usuario Habilitado");
            pMuestraDetalle();


        }
    }

    protected void GrvListado_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {            
            BE_Usuario oitem = (e.Row.DataItem as BE_Usuario);

            if (oitem.iIdUsuario == 0)
            {
                e.Row.Visible = false;
                return;
            }
            else
            {
                e.Row.Style["cursor"] = "pointer";
                e.Row.Attributes["onclick"] = String.Format("javascript: fc_SeleccionaFilaSimple_(this);");

                e.Row.Attributes["ondblclick"] = String.Format("javascript: fc_EditarDatos('{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}');",
                    oitem.iIdUsuario, oitem.sUsuario, oitem.sNomUsuario, oitem.sDNI, oitem.sCorreo, oitem.sRucCliente, oitem.sEsCliente, oitem.iTerCondiciones);             
            }

            if(oitem.isHabilitado == false)
            {
                e.Row.Cells[8].Visible = true;
            }
            else
            {
                e.Row.Cells[8].Text = "";
            }
        }
    }

    protected void ImgBtnNuevo_Click(object sender, ImageClickEventArgs e)
    {
        if (HdTipo.Value == "I")
        {
            LimpiaControles();
            txtUsuario.Enabled = true;
            txtUsuario.Focus();
        }
        else
        {
            txtUsuario.Enabled = false;
            if (ViewState["AccesoAnularUsuario"].ToString() == "S")
                chkEsCliente.Enabled = true;
            else
                chkEsCliente.Enabled = false;
        }
        mpeUsuario.Show();
    }

    protected void LimpiaControles()
    {
        txtIdUsuario.Text = "0";
        txtUsuario.Text = "";
        txtNomUsuario.Text = "";
        txtDni.Text = "";
        txtCorreo.Text = "";
        txtRucCli.Text = "";
        chkEsCliente.Checked = true;
        chkEsCliente.Enabled = true;
        chkTerCondiciones.Checked = true;
        chkTerCondiciones.Enabled = true;
    }

    protected void BtnSalir_Click(object sender, ImageClickEventArgs e)
    {
        mpeUsuario.Hide();
        HdTipo.Value = "I";
    }

    protected void BtnGrabar_Click(object sender, ImageClickEventArgs e)
    {
        BL_Usuario oBL_Usuario = new BL_Usuario();
        BE_Usuario oBE_Usuario = new BE_Usuario();
        String[] sResultado = null;
        Int32 vl_iCodigo = 0;
        String vl_sMessage = String.Empty;

        oBE_Usuario.iIdUsuario = Convert.ToInt32(txtIdUsuario.Text);
        oBE_Usuario.sUsuario = txtUsuario.Text;
        oBE_Usuario.sNomUsuario = txtNomUsuario.Text;
        oBE_Usuario.sDNI = txtDni.Text;
        oBE_Usuario.sCorreo = txtCorreo.Text;
        oBE_Usuario.sRucCliente = txtRucCli.Text;
        oBE_Usuario.sEsCliente = chkEsCliente.Checked ? "S" : "N";
        oBE_Usuario.iTerCondiciones = chkTerCondiciones.Checked ? 1 : 0;

        
        oBE_Usuario.sUsuarioActualiza = GlobalEntity.Instancia.Usuario;
        oBE_Usuario.sNombrePc = GlobalEntity.Instancia.NombrePc;

        sResultado = oBL_Usuario.GrabarUsuario(oBE_Usuario).Split('|');

        for (int i = 0; i < sResultado.Length; i++)
        {
            if (i == 0)
            {
                vl_iCodigo = Convert.ToInt32(sResultado[i]);
            }
            if (i == 1)
            {
                vl_sMessage += sResultado[i];
            }
        }

        SCA_MsgInformacion(vl_sMessage);
        if (vl_iCodigo > 0)
        {
            if (HdTipo.Value == "I")
            {
                LimpiaControles();
                txtUsuario.Focus();
            }
            else
            {
                pMuestraDetalle();
                HdTipo.Value = "I";
                mpeUsuario.Hide();
            }
        }
    }

    protected void GrvListado_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GrvListado.PageIndex = e.NewPageIndex;
        List<BE_Usuario> lista = (List<BE_Usuario>) ViewState["oBE_DocumentoOrigenList"];
        GrvListado.DataSource = lista;
        GrvListado.DataBind();
    }

    protected void chkHabilitado_CheckedChanged(object sender, EventArgs e)
    {
        GridViewRow fila = GrvListado.SelectedRow;
        //GridViewRow fila;
        //fila = (GridViewRow)(((CheckBox).).NamingContainer);
        int rowindex = fila.RowIndex;

        Int32 iIdUsuario = Convert.ToInt32(GrvListado.DataKeys[fila.RowIndex].Values["iIdUsuario"].ToString());
        //// Convert.ToInt32(GrvListado.DataKeys[Convert.ToInt32(e.CommandArgument)].Values["UsuarioId"]);

        //BL_Usuario oBL_Usuario = new BL_Usuario();
        //BE_Usuario oBE_Usuario = new BE_Usuario();
        //String[] sResultado = null;
        //Int32 vl_iCodigo = 0;
        //String vl_sMessage = String.Empty;


        SCA_MsgInformacion(iIdUsuario.ToString());
    }
}