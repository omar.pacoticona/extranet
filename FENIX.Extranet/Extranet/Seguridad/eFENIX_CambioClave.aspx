﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="eFENIX_CambioClave.aspx.cs" Theme="SkinFile" Inherits="Extranet_Seguridad_eFENIX_CambioClave" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Cambio Clave e-Fenix</title>

    <link href="../../Script/Hojas_Estilo/eFenix.css" rel="stylesheet" type="text/css" />

    <script language="javascript" type="text/javascript">
        function fc_AceptarCambioContrasenha() {
            var msjError = "";

            if (document.getElementById("<%=this.txtNuevaClave.ClientID %>").value == "") {
                msjError += "- Debe ingresar su contraseña nueva.\n";
            }
            if (document.getElementById("<%=this.txtNuevaClave.ClientID %>").value != "") {
                if (document.getElementById("<%=this.txtNuevaClave.ClientID %>").value.length < 8) {
                    msjError += "- La contraseña nueva debe de ser de mínimo 8 carácteres.\n";
                }
            }
            if (document.getElementById("<%=this.txtNuevaClaveConf.ClientID %>").value == "") {
                msjError += "- Debe ingresar la confirmación de la contraseña.\n";
            }
            if (document.getElementById("<%=this.txtNuevaClaveConf.ClientID %>").value != "") {
                if (document.getElementById("<%=this.txtNuevaClaveConf.ClientID %>").value.length < 8) {
                    msjError += "- La confirmación de la contraseña debe de ser de mínimo 8 carácteres.\n";
                }
            }
            if (document.getElementById("<%=this.txtNuevaClave.ClientID %>").value != "" &&
                    document.getElementById("<%=this.txtNuevaClaveConf.ClientID %>").value != "" &&
                    document.getElementById("<%=this.txtNuevaClave.ClientID %>").value != document.getElementById("<%=this.txtNuevaClaveConf.ClientID %>").value) {
                msjError += "- La confirmación de la nueva contraseña no es válida.\n";
            }

            if (msjError != "") {
                alert(msjError);
                return false;
            }

            return confirm("Esta Seguro que desea cambiar su contraseña");
        }

         function fc_CerrarCambioContrasenha() {
            fc_LimpiarCambioContrasenha();
            Mostrar('none');
            return false;
        }

        function fc_LimpiarCambioContrasenha() {
            document.getElementById("<%=this.txtNuevaClave.ClientID %>").value = "";
            document.getElementById("<%=this.txtNuevaClaveConf.ClientID %>").value = "";
            return false;
        }
        </script>

</head>
<body style="margin:0px;">
    <form id="form1" runat="server">
        <ajax:ScriptManager ID="scmLogin"  runat="server">
        </ajax:ScriptManager>

        <div style="height: 100%; width: 100%;padding-top:100px;">
            <table cellpadding="0" cellspacing="0" border="0px" align="center" width="100%" >
                <tr>
                    <td style="width:32%;height:115px;">
                    
                    </td>
                    <td style="width:36%;text-align:center;" >
                        <img src="../../Imagenes/login_logo.jpg" />
                    </td>
                    <td style="width:32%;">
                    
                    </td>
                </tr>
                <tr style="height:195px;background-color:#243F90;">
                    <td>

                    </td>
                    <td>
                        <table cellpadding="0" cellspacing="1" class="tabla_login" >
                            <tr>
                                <td align="center" valign="middle" colspan="2">
                                   <h1>Crear una Nueva Clave</h1>
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align:right;width:320px;">
                                    &nbsp;</td>
                                <td style="padding-left:10px;">
                                    &nbsp;</td>
                            </tr>
                            <tr>
                                <td style="text-align:right;width:320px;">
                                    Nueva Contraseña
                                </td>
                                <td style="padding-left:10px;">
                                    <ajax:UpdatePanel ID="upNewClave" runat="server" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <asp:TextBox ID="txtNuevaClave" Width="180px" runat="server" TextMode="Password" MaxLength="20"></asp:TextBox>
                                        </ContentTemplate>
                                        <Triggers>
                                            <ajax:AsyncPostBackTrigger ControlID="btnAceptarCambioContrasenha" EventName="Click" />
                                        </Triggers>
                                    </ajax:UpdatePanel>
                                </td>
                            </tr>
                            
                            <tr>
                                <td style="text-align:right;width:320px;">
                                    Confirmar Contraseña
                                </td>
                                <td style="padding-left:10px;">
                                    <ajax:UpdatePanel ID="upNewClave2" runat="server" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <asp:TextBox ID="txtNuevaClaveConf" Width="180px" runat="server" TextMode="Password" MaxLength="20" ></asp:TextBox>
                                        </ContentTemplate>
                                        <Triggers>
                                            <ajax:AsyncPostBackTrigger ControlID="btnAceptarCambioContrasenha" EventName="Click" />
                                        </Triggers>
                                    </ajax:UpdatePanel>
                                </td>
                            </tr>
                            <%--<tr>
                                <td></td>
                                <td></td>
                                <td style="padding-left:10px;">
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                                        ControlToValidate="txtNuevaClaveConf" ErrorMessage="Ingrese su clave" 
                                        SetFocusOnError="True" ValidationGroup="vgIngreso" 
                                         Font-Size="X-Small" Font-Names="verdana" >
                                    </asp:RequiredFieldValidator>
                                </td>
                            </tr>--%>
                       </table>
                    
                    </td>

                    <td valign="bottom">
                          <img src="../../Imagenes/login_car.JPG" style="margin: 0px 0px 10px 40px;" />
                    </td>
                </tr>
                <tr>
                    <td align="right">
                          <img src="../../Imagenes/login_picture.JPG" style="margin-top:2px;" />  
                    </td>
                            <td align="center" valign="middle">
                              <asp:Button ID="btnAceptarCambioContrasenha" runat="server" CssClass="botonAceptar" 
                                    style="border-width:0px;" 
                                    OnClientClick="javascript: return fc_AceptarCambioContrasenha();" 
                                    OnClick="btnAceptarCambioContrasenha_Click" /> 
                              
                              <asp:Button ID="btnCancelarCambioContrasenha" runat="server" CssClass="botonCancelar" 
                                    style="border-width:0px;" 
                                    OnClientClick="javascript: return fc_CerrarCambioContrasenha();"/> 
                            </td>
                    
                    <td>

                    </td>
                </tr>
            </table>                                                  
        </div>
    </form>
</body>
</html>
