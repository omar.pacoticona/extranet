﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FENIX.BusinessLogic;
using FENIX.BusinessEntity;
using System.Net.Mail;
using System.Net.Mime;
using System.Text;
using System.IO;
using System.Text.RegularExpressions;



    public partial class FENIX_Login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
                lnkNewImagen_Click(null, null);
                //SetCaptchaText();

            TxtUsuario.Focus();         
        }

        

        protected void ImbIngreso_Click(object sender, ImageClickEventArgs e)
        {
            if (string.IsNullOrEmpty(TxtUsuario.Text) == false)
            {
                BL_Usuario oBL_Usuario = new BL_Usuario();
                BE_Usuario oBE_Usuario = new BE_Usuario();

                Int32 vl_iCodigo = 0;
                String vl_sMessage = String.Empty;
                String[] sDatosUsuario = null;
                String vl_sMaquina = String.Empty;
                String vl_sIPMaquina = String.Empty;

                oBE_Usuario.sUsuario = TxtUsuario.Text.Trim().ToUpper();
                oBE_Usuario.sClave = oBE_Usuario.GetMD5(TxtPassword.Text);
                oBE_Usuario.sNuevaClave = "";
                oBE_Usuario.sSistema = "EXTRANET";
                oBE_Usuario.sSolicitarCambioClave = "S";
                oBE_Usuario.sSolicitarNewClaveOlvidada = "N";
                //vl_sMaquina = System.Net.Dns.GetHostByAddress(Request.UserHostAddress).HostName;
                vl_sMaquina = Request.UserHostName.ToString();
                vl_sIPMaquina = Request.ServerVariables["REMOTE_ADDR"];

                String[] sResultado = oBL_Usuario.ValidaUsuario(oBE_Usuario, vl_sMaquina, vl_sIPMaquina).Split('|');

                for (int i = 0; i < sResultado.Length; i++)
                {
                    if (i == 0)
                    {
                        vl_iCodigo = Convert.ToInt32(sResultado[i]);
                    }
                    else
                    {
                        vl_sMessage += sResultado[i];
                        sDatosUsuario = vl_sMessage.Split('|');
                    }
                }

                if (vl_iCodigo < 1)
                {
                    ScriptManager.RegisterClientScriptBlock(TxtUsuario, GetType(), "mensaje", String.Format(Resources.Resource.MsgAlerta, vl_sMessage), true);
                    TxtUsuario.Focus();
                }
                else
                {
                    List<BE_Usuario> Lst_User = new List<BE_Usuario>();
                    Lst_User = oBL_Usuario.ListaDatosUsuario(0, TxtUsuario.Text.Trim().ToUpper());

                    if (Lst_User.Count > 0)
                    {
                        if (Lst_User[0].sSesionIniciada.ToString() == "1")
                        {
                            ScriptManager.RegisterStartupScript(TxtUsuario, GetType(), "mensaje", String.Format(Resources.Resource.MsgAlerta, "Solo Puede Tener Una Session Activa"), true);
                            TxtUsuario.Focus();
                        }
                        else
                        {
                            if (TxtCodCaptcha.Text.ToString() == Request.Cookies["Captcha"]["value"])
                            {

                                GlobalEntity.Instancia.Nombre = sResultado[3];
                                GlobalEntity.Instancia.Usuario = sResultado[2].ToUpper();
                                GlobalEntity.Instancia.NombrePc = Request.UserHostName.ToString();
                                GlobalEntity.Instancia.DireccionIP = Request.UserHostAddress.ToString();
                                GlobalEntity.Instancia.IdCliente = Convert.ToInt32(sResultado[4]);
                                GlobalEntity.Instancia.NombreCliente = sResultado[5].ToUpper();

                                Int32 iTiempoSesion = 0;

                                List<BE_Usuario> Lst_Config = new List<BE_Usuario>();
                                Lst_Config = oBL_Usuario.ListaConfSeguridad();

                                foreach (BE_Usuario BE_Usuario in Lst_Config)
                                {
                                    iTiempoSesion = BE_Usuario.iMnCierreSesion;
                                }
                                GlobalEntity.Instancia.TiempoDuracionSesion = iTiempoSesion;

                                Session.Timeout = iTiempoSesion;   //minutos

                                oBL_Usuario.GrabaInicioSesion(TxtUsuario.Text, "1");

                                Response.Redirect("eFENIX_Inicio.aspx");
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(TxtCodCaptcha, GetType(), "mensaje", String.Format(Resources.Resource.MsgAlerta, "Codigo de Imagen Incorrecto"), true);
                                lnkNewImagen_Click(null, null);
                                TxtCodCaptcha.Text = "";
                                TxtCodCaptcha.Focus();
                            }
                        }
                    }
                }
            }
            else
            {
                ScriptManager.RegisterStartupScript(TxtDNI, GetType(), "mensaje", String.Format(Resources.Resource.MsgAlerta, "Ingresar Usuario"), true);
            }
        }

        protected Boolean ValidarPassword(String strClave)
        {

            Regex upper = new Regex("[A-Z]");
            Regex lower = new Regex("[a-z]");
            Regex number = new Regex("[0-9]");
            Regex special = new Regex("[^a-zA-Z0-9]");


            if (upper.Matches(strClave).Count == 0)
                return false;
            if (lower.Matches(strClave).Count == 0)
                return false;
            if (number.Matches(strClave).Count == 0)
                return false;
            if (special.Matches(strClave).Count == 0)
                return false;


            return true;
        }


        protected void btnAceptarCambioContrasenha_Click(object sender, EventArgs e)
        {
            BL_Usuario oBL_Usuario = new BL_Usuario();
            BE_Usuario oBE_Usuario = new BE_Usuario();

            Int32 vl_iCodigo = 0;
            String vl_sMessage = String.Empty;
            String vl_sMaquina = String.Empty;
            String vl_sIPMaquina = String.Empty;

            if (ValidarPassword(txtNuevaClave.Text.Trim()))
            {
                oBE_Usuario.sUsuario = txtLogin.Text.Trim().ToUpper();
                oBE_Usuario.sClave = oBE_Usuario.GetMD5(txtClave.Text.Trim());
                oBE_Usuario.sNuevaClave = oBE_Usuario.GetMD5(txtNuevaClave.Text.Trim());
                oBE_Usuario.sSistema = "EXTRANET";
                oBE_Usuario.sSolicitarCambioClave = "N";
                oBE_Usuario.sSolicitarNewClaveOlvidada = "N";
                vl_sMaquina = Request.UserHostName.ToString();
                vl_sIPMaquina = Request.ServerVariables["REMOTE_ADDR"];

                String[] sResultado = oBL_Usuario.ValidaUsuario(oBE_Usuario, vl_sMaquina, vl_sIPMaquina).Split('|');

                for (int i = 0; i < sResultado.Length; i++)
                {
                    if (i == 0)
                    {
                        vl_iCodigo = Convert.ToInt32(sResultado[i]);
                    }
                    else
                    {
                        vl_sMessage += sResultado[i];
                    }
                }

                if (vl_iCodigo < 1)
                {
                    ScriptManager.RegisterStartupScript(txtLogin, GetType(), "mensaje", String.Format(Resources.Resource.MsgAlerta, vl_sMessage), true);
                    txtLogin.Focus();
                }
                else
                    ScriptManager.RegisterStartupScript(txtLogin, GetType(), "mensaje", String.Format("javascript: GrabaryMostrar('none');"), true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(txtLogin, GetType(), "mensaje", String.Format(Resources.Resource.MsgAlerta, "Clave Debe Contener Mayusculas,Minusculas,Numeros,Letras y caracteres Especiales"), true);
                txtLogin.Focus();
            }
        }

        protected void btnAceptarRecordarContrasenha_Click(object sender, EventArgs e)
        {
            try
            {
                BL_Usuario oBL_Usuario = new BL_Usuario();
                BE_Usuario oBE_Usuario = new BE_Usuario();

                //oBE_Usuario.sUsuario = TxtUsuario.Text.Trim().ToUpper();
                //oBE_Usuario.sClave = oBE_Usuario.GetMD5("123456");
                //oBE_Usuario.sSistema = "EXTRANET";

                //String[] sResultado = oBL_Usuario.ReiniciaClave(oBE_Usuario).Split('|');
                if (TxtDNI.Text.Trim().Length == 0)
                    ScriptManager.RegisterStartupScript(TxtDNI, GetType(), "mensaje", String.Format(Resources.Resource.MsgAlerta, "Ingresar DNI"), true);
                else
                {
                    List<BE_Usuario> Lst_User = new List<BE_Usuario>();
                    Lst_User = oBL_Usuario.ListaDatosUsuario(0, TxtUser.Text.Trim().ToUpper());

                    if (Lst_User.Count > 0)
                    {
                        if (TxtDNI.Text != Lst_User[0].sDNI.ToString())
                            ScriptManager.RegisterStartupScript(TxtDNI, GetType(), "mensaje", String.Format(Resources.Resource.MsgAlerta, "Numero de Documento Incorrecto"), true);
                        else
                        {
                            //StreamReader reader = new StreamReader(Server.MapPath("../Avisos/Aviso_RecordarContrasenha2.htm"));
                            //String readFile = reader.ReadToEnd();
                            //String sBody = "";

                            //sBody = readFile;
                            //sBody = sBody.Replace("[NombreUsuario]", Lst_User[0].sDescripcion.ToString());
                            ////sBody += sBody.Replace("[Id]", Lst_User[0].iIdUsuario.ToString());

                            ////AlternateView htmlView = AlternateView.CreateAlternateViewFromString(sBody, Encoding.UTF8, MediaTypeNames.Text.Html);
                            ////LinkedResource img_Logo = new LinkedResource(@"../../FENIX.Extranet.root/FENIX.Extranet/FENIX.Extranet/Imagenes/login_logo.jpg", MediaTypeNames.Image.Jpeg); img_Logo.ContentId = "login_logo";
                            ////htmlView.LinkedResources.Add(img_Logo);


                            //MailMessage correo = new MailMessage();
                            //correo.From = new System.Net.Mail.MailAddress("webmaster@fargoline.com.pe");
                            //correo.To.Add(Lst_User[0].sCorreo.ToString());
                            //correo.Bcc.Add("carla.echenique@fargoline.com.pe,webmaster@fargoline.com.pe");
                            //correo.Subject = "Recuperacion de clave de acceso a la Extranet Fargoline";
                            //correo.Body = sBody.ToString();
                            //correo.IsBodyHtml = true;
                            //correo.Priority = System.Net.Mail.MailPriority.Normal;
                            ////correo.AlternateViews.Add(htmlView);


                            //SmtpClient smtp = new SmtpClient();
                            //smtp.UseDefaultCredentials = false;
                            //smtp.Host = "smtp.gmail.com";
                            //smtp.Port = 587;
                            //smtp.Credentials = new System.Net.NetworkCredential("webmaster@fargoline.com.pe", "Fargo2013");
                            //smtp.EnableSsl = true;
                            //smtp.Send(correo);


                            oBE_Usuario.iIdUsuario = Convert.ToInt32(Lst_User[0].iIdUsuario.ToString());
                            oBE_Usuario.sDescripcion = Lst_User[0].sDescripcion.ToString();
                            oBE_Usuario.sCorreo = Lst_User[0].sCorreo.ToString();

                            oBL_Usuario.EnviaCorreoNewClave(oBE_Usuario);

                            Response.Redirect("eFENIX_CambioClave.aspx?Id=" + oBE_Usuario.iIdUsuario);

                            ScriptManager.RegisterStartupScript(btnAceptarRecordarContrasenha, GetType(), "mensaje", String.Format("javascript: GrabraryMostrarAlerta('none');"), true);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                String sError = ex.Message;
            }
        }




        protected void lnkNewImagen_Click(object sender, EventArgs e)
        {           
            string strTexto = (Guid.NewGuid().ToString()).Substring(0, 5);

            Response.Cookies["Captcha"]["value"] = strTexto;
            imgCaptcha.ImageUrl = string.Format("Captcha.aspx?refresh={0}", strTexto);

            Session["Captcha"] = strTexto;
            imgCaptcha2.ImageUrl = string.Format("Captcha.ashx?refresh={0}", strTexto);
            
        }
        
}
    

