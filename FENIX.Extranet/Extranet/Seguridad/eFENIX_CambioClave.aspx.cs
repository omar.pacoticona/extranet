﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FENIX.BusinessLogic;
using FENIX.BusinessEntity;
using System.Text.RegularExpressions;

public partial class Extranet_Seguridad_eFENIX_CambioClave : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        ViewState["IdUsuario"] = Request.QueryString["Id"];
        txtNuevaClave.Focus();
    }

    protected Boolean ValidarPassword(String strClave)
    {
        Regex upper = new Regex("[A-Z]");
        Regex lower = new Regex("[a-z]");
        Regex number = new Regex("[0-9]");
        Regex special = new Regex("[^a-zA-Z0-9]");


        if (upper.Matches(strClave).Count == 0)
            return false;
        if (lower.Matches(strClave).Count == 0)
            return false;
        if (number.Matches(strClave).Count == 0)
            return false;
        if (special.Matches(strClave).Count == 0)
            return false;


        return true;
    }


    protected void btnAceptarCambioContrasenha_Click(object sender, EventArgs e)
    {
        BL_Usuario oBL_Usuario = new BL_Usuario();
        BE_Usuario oBE_Usuario = new BE_Usuario();

        Int32 vl_iCodigo = 0;
        String vl_sMessage = String.Empty;
        String vl_sMaquina = String.Empty;
        String vl_sIPMaquina = String.Empty;

        if (ValidarPassword(txtNuevaClave.Text.Trim()))
        {
            List<BE_Usuario> Lst_User = new List<BE_Usuario>();
            Lst_User  =  oBL_Usuario.ListaDatosUsuario(Convert.ToInt32(ViewState["IdUsuario"]),"");

            oBE_Usuario.sUsuario = Lst_User[0].sUsuario.ToString().Trim().ToUpper();
            oBE_Usuario.sClave = Lst_User[0].sClave.ToString();
            oBE_Usuario.sNuevaClave = oBE_Usuario.GetMD5(txtNuevaClave.Text.Trim());
            oBE_Usuario.sNuevaClaveConf = txtNuevaClave.Text.Trim();
            oBE_Usuario.sSistema = "EXTRANET";
            oBE_Usuario.sSolicitarCambioClave = "N";
            oBE_Usuario.sSolicitarNewClaveOlvidada = "S";
            vl_sMaquina = Request.UserHostName.ToString();
            vl_sIPMaquina = Request.ServerVariables["REMOTE_ADDR"];

            String[] sResultado = oBL_Usuario.ValidaUsuario(oBE_Usuario, vl_sMaquina, vl_sIPMaquina).Split('|');

            for (int i = 0; i < sResultado.Length; i++)
            {
                if (i == 0)
                {
                    vl_iCodigo = Convert.ToInt32(sResultado[i]);
                }
                else
                {
                    vl_sMessage += sResultado[i];
                }
            }

            ScriptManager.RegisterStartupScript(txtNuevaClave, GetType(), "mensaje", String.Format(Resources.Resource.MsgAlerta, vl_sMessage), true);
            if (vl_iCodigo < 1)
                txtNuevaClave.Focus();
            else
                Response.Redirect("~/eFENIX_Login.aspx");
        }
        else
        {
            ScriptManager.RegisterStartupScript(txtNuevaClave, GetType(), "mensaje", String.Format(Resources.Resource.MsgAlerta, "Clave Debe Contener Mayusculas,Minusculas,Numeros,Letras y caracteres Especiales"), true);
            txtNuevaClave.Focus();
        }
    }
}