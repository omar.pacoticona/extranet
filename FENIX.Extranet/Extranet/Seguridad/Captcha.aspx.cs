﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;


    public partial class Captcha : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            int iHeight = 80;
            int iWidth = 190;
            Random oRandom = new Random();

            int[] aBackgroundNoiseColor = new int[] { 150, 150, 150 };
            int[] aTextColor = new int[] { 0, 0, 0 };
            int[] aFontEmSizes = new int[] { 15, 20, 25, 30, 35 };

            string[] aFontNames = new string[]
        {
         "Comic Sans MS",
         "Arial",
         "Times New Roman",
         "Georgia",
         "Verdana",
         "Geneva"
        };

            FontStyle[] aFontStyles = new FontStyle[]
        {  
         FontStyle.Bold,
         FontStyle.Italic,
         FontStyle.Regular,
         FontStyle.Strikeout,
         FontStyle.Underline
        };

            HatchStyle[] aHatchStyles = new HatchStyle[]
        {
         HatchStyle.BackwardDiagonal, HatchStyle.Cross, 
            HatchStyle.DashedDownwardDiagonal, HatchStyle.DashedHorizontal,
         HatchStyle.DashedUpwardDiagonal, HatchStyle.DashedVertical, 
            HatchStyle.DiagonalBrick, HatchStyle.DiagonalCross,
         HatchStyle.Divot, HatchStyle.DottedDiamond, HatchStyle.DottedGrid, 
            HatchStyle.ForwardDiagonal, HatchStyle.Horizontal,
         HatchStyle.HorizontalBrick, HatchStyle.LargeCheckerBoard, 
            HatchStyle.LargeConfetti, HatchStyle.LargeGrid,
         HatchStyle.LightDownwardDiagonal, HatchStyle.LightHorizontal, 
            HatchStyle.LightUpwardDiagonal, HatchStyle.LightVertical,
         HatchStyle.Max, HatchStyle.Min, HatchStyle.NarrowHorizontal, 
            HatchStyle.NarrowVertical, HatchStyle.OutlinedDiamond,
         HatchStyle.Plaid, HatchStyle.Shingle, HatchStyle.SmallCheckerBoard, 
            HatchStyle.SmallConfetti, HatchStyle.SmallGrid,
         HatchStyle.SolidDiamond, HatchStyle.Sphere, HatchStyle.Trellis, 
            HatchStyle.Vertical, HatchStyle.Wave, HatchStyle.Weave,
         HatchStyle.WideDownwardDiagonal, HatchStyle.WideUpwardDiagonal, HatchStyle.ZigZag
        };
            //Adicionado Hasta Aqui





            Bitmap bmp = new Bitmap(Server.MapPath("~/Imagenes/carrito1.jpg"));
            MemoryStream mem = new MemoryStream();
            int witdh = bmp.Width;
            int height = bmp.Height;
            //string fontfamily = "Arial";
            string sCaptchaText = Request.Cookies["Captcha"]["value"];

            Bitmap bitmap = new Bitmap(bmp, new Size(witdh, height));
            Graphics g = Graphics.FromImage(bitmap);

            g.TextRenderingHint = System.Drawing.Text.TextRenderingHint.AntiAlias;  //Adicionado

            //g.SmoothingMode = SmoothingMode.AntiAlias;
            //int xCopyright = witdh - 138; //170;
            //int yCopyright = height - 65;

            //Rectangle rect;
            //Font font;
            //int newfontsize = 30;

            //font = new Font(fontfamily, newfontsize, FontStyle.Italic);
            //rect = new Rectangle(xCopyright, yCopyright, 0, 0);

            //StringFormat format = new StringFormat();
            //format.Alignment = StringAlignment.Near;
            //format.LineAlignment = StringAlignment.Near;
            //GraphicsPath path = new GraphicsPath();
            //path.AddString(sCaptchaText, font.FontFamily, (int)font.Style, font.Size, rect, format);

            //HatchBrush hatchBrush = new HatchBrush(HatchStyle.LargeConfetti, Color.FromName("Black"),
            //    Color.FromName("Green"));
            //g.FillPath(hatchBrush, path);


            System.Drawing.Drawing2D.Matrix oMatrix = new System.Drawing.Drawing2D.Matrix();
            int i = 0;
            for (i = 0; i <= sCaptchaText.Length - 1; i++)
            {
                oMatrix.Reset();
                int iChars = sCaptchaText.Length;

                int x = iWidth / (iChars + 1) * i;
                int y = iHeight / 2;

                //Rotate text Random
                oMatrix.RotateAt(oRandom.Next(-40, 40), new PointF(x, y));
                g.Transform = oMatrix;


                //Draw the letters with Random Font Type, Size and Color
                g.DrawString
                (
                    //Text
                    sCaptchaText.Substring(i, 1),
                    //Random Font Name and Style
                    new Font(aFontNames[oRandom.Next(aFontNames.Length - 1)],
                       aFontEmSizes[oRandom.Next(aFontEmSizes.Length - 1)],
                       aFontStyles[oRandom.Next(aFontStyles.Length - 1)]),
                    //Random Color (Darker colors RGB 0 to 100)
                    new SolidBrush(Color.FromArgb(oRandom.Next(0, 100),
                       oRandom.Next(0, 100), oRandom.Next(0, 100))),
                    x,
                    oRandom.Next(10, 40)
                );
                g.ResetTransform();
            }



            HttpContext.Current.Response.Clear();
            HttpContext.Current.Response.ContentType = "imagen/jpeg";
            bitmap.Save(mem, ImageFormat.Jpeg);
            bmp.Dispose();

            //font.Dispose();
            //hatchBrush.Dispose();
            //g.Dispose();
            mem.WriteTo(HttpContext.Current.Response.OutputStream);
            bitmap.Dispose();
        }
    }
