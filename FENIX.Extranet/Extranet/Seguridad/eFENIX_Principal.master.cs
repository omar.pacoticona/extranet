﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using FENIX.BusinessEntity;
using FENIX.BusinessLogic;


public partial class eFENIX_Principal : System.Web.UI.MasterPage
{
    //private string strState = null;
    //private int intStatePosition = -1;
     

    protected void Page_Load(object sender, EventArgs e)
    {        
            
        if (GlobalEntity.Instancia.Usuario == null)
        {
            Response.Redirect("~/eFENIX_Login.aspx");
            return;
        }
            //if (GlobalEntity.Instancia.NombreCliente.Length >= 20)
            //{
            //    LblUsuario.Text = GlobalEntity.Instancia.Usuario + " | " + GlobalEntity.Instancia.NombreCliente.Substring(0, 20);
            //}
            //else
            //{
            //    LblUsuario.Text = GlobalEntity.Instancia.Usuario + " | " + GlobalEntity.Instancia.NombreCliente;
            //}


        
        // Disable ExpandDepth if the TreeView's expand/collapse
        // state is stored in session.        
        if (Session["TreeViewState"] != null)
            tvw.ExpandDepth = 0;

          
        if (!Page.IsPostBack)
        {
            ////Estado del árbol almacenado en cookies
            //if (Request.Cookies.Get("TreeViewState") != null)
            //    strState = Request.Cookies.Get("TreeViewState").Value;
            

            CargarMenu();

            if (Session["TreeViewState"] != null)
            {
                List<string> list = (List<string>)Session["TreeViewState"];
                RestoreTreeViewState(tvw.Nodes, list);
            }

            GlobalEntity.Instancia.CerrarExtranet = "S";

              
            if (GlobalEntity.Instancia.NombreCliente.Length >= 20)               
               LblUsuario.Text = GlobalEntity.Instancia.Usuario + " | " + GlobalEntity.Instancia.NombreCliente.Substring(0, 20);               
            else               
               LblUsuario.Text = GlobalEntity.Instancia.Usuario + " | " + GlobalEntity.Instancia.NombreCliente;                           
        }                 
        else
            GlobalEntity.Instancia.CerrarExtranet = "N";



        ////Guardo en una variable de JavaScript el ID de cliente del campo oculto
        ////donde .NET guarda el estado del mismo
        //Page.ClientScript.RegisterClientScriptBlock(
        //    typeof(eFENIX_Principal),
        //    "TreeViewState",
        //    "var idTreeViewState='" + tvw.ClientID + "_ExpandState';",
        //    true);
    }

    private void CargarMenu()
    {        
        Int32 iMenuPadre = 0;

        BL_Usuario oBL_Usuario = new BL_Usuario();
        BE_Usuario oBE_Usuario = new BE_Usuario();

        TreeNode nodoPadre = new TreeNode();
        TreeNode nodoHijo = new TreeNode();

        bool agregarH = false;

        try
        {
            oBE_Usuario.sUsuario = GlobalEntity.Instancia.Usuario.Trim();
            oBE_Usuario.sSistema = "EXTRANET";

            List<BE_Usuario> Lst_Menu = new List<BE_Usuario>();
            Lst_Menu = oBL_Usuario.AccesoMenu(oBE_Usuario);

            int iIdNodo = 0;
            foreach (BE_Usuario oMenu in Lst_Menu)
            {
                iIdNodo += 1;
                if (iMenuPadre != oMenu.iPadreMenuId)
                {
                    if (agregarH)
                    {
                        tvw.Nodes.Add(nodoPadre);
                        //LoadNodeState(nodoPadre);
                    }

                    iMenuPadre = oMenu.iPadreMenuId;
                    nodoPadre = new TreeNode(oMenu.sDescripcionPadre);
                    nodoPadre.SelectAction = TreeNodeSelectAction.None;
                    nodoPadre.NavigateUrl = "";
                    nodoPadre.Value = iIdNodo.ToString();

                    iIdNodo += 1;
                    nodoHijo = new TreeNode();
                    nodoHijo.Text = oMenu.sDescripcion;
                    nodoHijo.NavigateUrl = oMenu.sUrl;
                    nodoHijo.Value = iIdNodo.ToString();
                    
                    nodoPadre.ChildNodes.Add(nodoHijo);
                   
                    agregarH = true;                   
                }
                else
                {
                    agregarH = true;

                    iIdNodo += 1;
                    nodoHijo = new TreeNode();
                    nodoHijo.Text = oMenu.sDescripcion;
                    nodoHijo.NavigateUrl = oMenu.sUrl;
                    nodoHijo.Value = iIdNodo.ToString();

                    nodoPadre.ChildNodes.Add(nodoHijo);
                    
                }
            }

            if (agregarH)
            {
                tvw.Nodes.Add(nodoPadre);
                //LoadNodeState(nodoPadre);
            }
  
        }
        catch (Exception ex)
        {
            //MensajeScript(ex.Message);
        }
    }






    #region "Menu 2"
    
    private void CargarMenu2()
    {
        Int32 iMenuPadre = 0;

        BL_Usuario oBL_Usuario = new BL_Usuario();
        BE_Usuario oBE_Usuario = new BE_Usuario();

        try
        {
            oBE_Usuario.sUsuario = GlobalEntity.Instancia.Usuario.Trim();
            oBE_Usuario.sSistema = "EXTRANET";

            List<BE_Usuario> Lst_Menu = new List<BE_Usuario>();
            Lst_Menu = oBL_Usuario.AccesoMenu(oBE_Usuario);
            
            foreach (BE_Usuario oMenu in Lst_Menu)
            {

                if (iMenuPadre != oMenu.iPadreMenuId)
                {
                    iMenuPadre = oMenu.iPadreMenuId;

                    agregarMenu(oMenu.sDescripcionPadre, "", 1);
                    agregarMenu(oMenu.sDescripcion, oMenu.sUrl, 2);
                }
                else
                {
                    agregarMenu(oMenu.sDescripcion, oMenu.sUrl, 2);
                }
            }
        }
        catch (Exception ex)
        {
            //MensajeScript(ex.Message);
        }
    }
    private void agregarMenu(String sDescripcion, String sUrl, Int32 iTipo)
    {
        Panel oMenuItem;
        Int32 intTipoCss = 3;
  

        oMenuItem = new Panel();
        HyperLink link = new HyperLink();

        // Add Viñeta
        //-------------------------------------
        Image imgVineta = new Image();
        imgVineta.ImageUrl = iTipo.Equals(1) ? "../../Imagenes/mboff.gif" : "../../Imagenes/mbon.gif";
        //imgVineta.Attributes["onmouseover"] = String.Format("javascript: this.src='../../Imagenes/mbon.gif';");
        //imgVineta.Attributes["onmouseout"] = String.Format("javascript: this.src='../../Imagenes/mboff.gif';");

        link.Controls.Add(imgVineta);
        if (iTipo.Equals (2)) link.NavigateUrl = sUrl;

        // Add Texto
        //-------------------------------------
        Label txtOpcion = new Label();
        txtOpcion.Text = sDescripcion;
        txtOpcion.Font.Bold = true;
        link.Controls.Add(txtOpcion);

        // Add Estilos
        //-------------------------------------
        oMenuItem.CssClass = String.Format("clsMenuNivel{0}Ver", intTipoCss);
        oMenuItem.Attributes.Add("onmouseover", String.Format("javascript:this.className='clsMenuNivel{0}VerHover'", intTipoCss));
        oMenuItem.Attributes.Add("onmouseout", String.Format("javascript:this.className='clsMenuNivel{0}Ver'", intTipoCss));        
        //oMenuItem.ID = "menu" + Convert.ToString(i + 1);
        oMenuItem.Controls.Add(link);
        oMenuItem.Style["padding-top"] = "10px";

        if (iTipo.Equals(2))//Menu Hijo
        {
            //oMenuItem.Attributes.Add("onclick", String.Format("javascript:location.href='{0}'", sUrl));
            oMenuItem.Style["padding-left"] = "15px";
        }
        else
        {
            oMenuItem.Style["text-transform"] = "uppercase";
            oMenuItem.Style["text-decoration"] = "underline";
        }

        this.divOpciones.Controls.Add(oMenuItem);
    }
    
    #endregion

    /*
    private void AddMenuItem(MenuItem oMenuItem, List<BE_Menu> oLis_BE_Menu)
    {
        foreach (BE_Menu oBE_Menu in oLis_BE_Menu)
        {
            if ((oBE_Menu.iPadreId.ToString() == oMenuItem.Value) && !(oBE_Menu.iMenuId.ToString() == oBE_Menu.iPadreId.ToString()))
            {
                MenuItem oMenuItem_New = new MenuItem();
                oMenuItem_New.Value = oBE_Menu.iMenuId.ToString();

                oMenuItem_New.Text = oBE_Menu.sDescripcion;
                oMenuItem_New.ImageUrl = oBE_Menu.sIcono;
                oMenuItem_New.NavigateUrl = oBE_Menu.sUrl;
                oMenuItem.ChildItems.Add(oMenuItem_New);
                AddMenuItem(oMenuItem_New, oLis_BE_Menu);
            }
        }

    }
    */

    protected void btnOrdenServicio_Click(object sender, EventArgs e)
    {        
        Response.Redirect("../Operaciones/eFENIX_Orden_Servicio_Listado.aspx");
        return;
    }


    protected void Button2_Click(object sender, EventArgs e)
    {
        Response.Redirect("../Operaciones/FENIX_Preliquidacion.aspx");
    }



    protected void btn_cerrar_Click(object sender, ImageClickEventArgs e)
    {
        BL_Usuario oBL_Usuario = new BL_Usuario();
        oBL_Usuario.GrabaInicioSesion(GlobalEntity.Instancia.Usuario, "0");
        Session.Abandon();
        FormsAuthentication.SignOut();
        Response.Redirect("~/eFENIX_Login.aspx");
        Response.End();
    }

    protected void tvw_TreeNodeCollapsed(object sender, TreeNodeEventArgs e)
    {
        if (e.Node.Parent == null)       
            GlobalEntity.Instancia.CerrarExtranet = "S";

        if (IsPostBack)
        {
            List<string> list = new List<string>();
            SaveTreeViewState(tvw.Nodes, list);
            Session["TreeViewState"] = list;
        }
             
    }


    ///// Establece el estado de "expandido" o no al nodo
    ///// dependiendo del valor de la cookie
    ///// </summary>
    ///// <param name="Node">Nodo</param>
    //private void LoadNodeState(TreeNode Node)
    //{
    //    if (strState != null && strState.Length > ++intStatePosition)        
    //        Node.Expanded = strState[intStatePosition] == 'e';
        
    //}
    protected void tvw_DataBound(object sender, EventArgs e)
    {
        if (Session["TreeViewState"] == null)
        {
            //
            // Record the TreeView's current expand/collapse state.
            //
            List<string> list = new List<string>();
            SaveTreeViewState(tvw.Nodes, list);
            Session["TreeViewState"] = list;
        }
        else
        {
            //
            // Apply the recorded expand/collapse state to the TreeView.
            //
            List<string> list = (List<string>)Session["TreeViewState"];
            RestoreTreeViewState(tvw.Nodes, list);
        }
    }
    protected void tvw_TreeNodeExpanded(object sender, TreeNodeEventArgs e)
    {
        if (IsPostBack)
        {
            List<string> list = new List<string>();
            SaveTreeViewState(tvw.Nodes, list);
            Session["TreeViewState"] = list;
        }
    }

    private void SaveTreeViewState(TreeNodeCollection nodes, List<string> list)
    {
        //
        // Recursivley record all expanded nodes in the List.
        //
        foreach (TreeNode node in nodes)
        {
            if (node.ChildNodes != null && node.ChildNodes.Count != 0)
            {
                if (node.Expanded.HasValue && node.Expanded == true && !String.IsNullOrEmpty(node.Text))
                    list.Add(node.Text);
                SaveTreeViewState(node.ChildNodes, list);
            }
        }
    }

    private void RestoreTreeViewState(TreeNodeCollection nodes, List<string> list)
    {
        foreach (TreeNode node in nodes)
        {
            //
            // Restore the state of one node.
            //
            if (list.Contains(node.Text)) //True -> Esta Expandido
            {
                if (node.ChildNodes != null && node.ChildNodes.Count != 0)  // && node.Expanded.HasValue && node.Expanded == false)
                    node.Expand();
            }
            //else
            //{
            //    if (node.ChildNodes != null && node.ChildNodes.Count != 0 && node.Expanded.HasValue && node.Expanded == true)
            //        node.Collapse();
            //}

            //
            // If the node has child nodes, restore their state, too.
            //
            if (node.ChildNodes != null && node.ChildNodes.Count != 0)
                RestoreTreeViewState(node.ChildNodes, list);
        }
    }
}
