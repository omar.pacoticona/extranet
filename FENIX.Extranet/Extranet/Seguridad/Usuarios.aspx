﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Extranet/Seguridad/eFENIX_Principal.master" AutoEventWireup="true" CodeFile="Usuarios.aspx.cs" Inherits="Extranet_Seguridad_Usuarios" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentCab" Runat="Server">
    <span class="texto_titulo" >Tu Sesion Expirara en &nbsp; 
        <span class="texto_titulo" id="seconds"> </span>
        <span>&nbsp;seg.</span>
     </span>
     <asp:Button ID="btn_cerrar" runat="server" Text="cerrar" 
        onclick="btn_cerrar_Click"  Style="display: none;" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="CntMaster" Runat="Server">
    <style type="text/css">
         .modalPopup
        {
            background-color: White;
            border-width: 3px;           
            border:dashed 1px gray;            
            width: 680px;
        }
       .modalBackground
        {
           background-color: Black;
           filter: alpha(opacity=50);
           opacity: 0.5; 
           -moz-opacity: 0.5;
        }
        .auto-style1 {
            border: 1px solid #C9C7C8;
            background-color: #0069ae;/* #005cab;*/;
            padding: 4px;
            color: #E6E6E6;
            text-transform: uppercase;
            font-family: "Calibri";
            font-weight: bold;
            height: 27px;
        }
    </style>
    <script language="javascript" type="text/javascript">
        var tiempo = 0;
        var seconds = 0;

        var myVar = setInterval(function () {
            clearInterval(myVar);
        }, 1000);

        var myTiempo = setTimeout(function () {
            clearInterval(myTiempo);
        }, 1);

        function myStopFunction() {
            clearInterval(myVar);
            seconds = '<%=GlobalEntity.Instancia.TiempoDuracionSesion%>' * 60;
            tiempo = '<%=GlobalEntity.Instancia.TiempoDuracionSesion%>' * 1000 * 60;

            CuentaTiempo(tiempo);
        }

        function CuentaTiempo(tiempo) {
            clearTimeout(myTiempo);
            myTiempo = setTimeout(function () {
                document.getElementById('<%=btn_cerrar.ClientID%>').click();
                //window.location = "../Seguridad/eFENIX_Login.aspx";
            }, tiempo);
        }



        function SessionExpireAlert(timeout) {
            seconds = timeout / 1000;
            document.getElementsByName("seconds").innerHTML = seconds;

            myvar = setInterval(function () {
                seconds--;
                document.getElementById("seconds").innerHTML = seconds;
            }, 1000);

            CuentaTiempo(timeout);
        };


        var goodexit = false;
        window.onbeforeunload = confirmRegisterExit;
        function confirmRegisterExit(evt) {
            if (!goodexit) {
                //return "Si vas abandonar el sistema haz click en Cerrar Sesion!!";
                document.getElementById('<%=btn_cerrar.ClientID%>').click();
            }
        }       
        
    </script>


    <asp:UpdatePanel ID="UpdatePanel1" runat="server">   
        <ContentTemplate>
            <table cellspacing="0" cellpadding="0" style="width: 100%;">
                <tr>
                    <td class="auto-style1" >
                        <asp:TextBox ID="LblTitulo" Text="Creacion de Usuarios" ReadOnly="true" 
                            style="background-color: #014C8F; border: #014C8F; color: #FFFFFF; font-family: Calibri; font-size: 13px; font-weight: bold; text-transform: uppercase;" 
                            runat="server" Width="231px"></asp:TextBox>
                    </td>
           
                </tr>
        
                <tr>
                    <td valign="top" colspan="2">
                        <table class="form_bandeja" style="margin-top: 5px; margin-bottom: 5px; width: 100%;">
                            <tr>
                                <td >
                                    cliente
                                </td>
                                <td >
                                    <asp:TextBox ID="txtCliente" runat="server" Width="300px"></asp:TextBox>
                                </td>
                                <td>
                                    USUARIO</td>
                                <td>
                                    <asp:TextBox ID="txtUsu" runat="server" Width="300px"></asp:TextBox>
                                </td>
                                <td>
                                    DNI
                                </td>
                                <td>
                                     <asp:TextBox ID="txtDNIB" runat="server" Width="100px"></asp:TextBox>
                                </td>
                                <td >
                                     <asp:ImageButton ID="ImgBtnBuscar" runat="server" ImageUrl="~/Imagenes/Formulario/btn_consultar_1.JPG" OnClick="ImgBtnBuscar_Click" />
                                  
                                </td>
                                <td >
                                     <asp:ImageButton ID="ImgBtnNuevo" runat="server" ImageUrl="~/Imagenes/Formulario/btn_nuevo.JPG" OnClick="ImgBtnNuevo_Click" />
                                </td>
                            </tr>
                            </table>
                    </td>
                </tr>
        
                <tr valign="top" style="height: 100%">
                    <td colspan="5">
                        <ajax:UpdatePanel ID="upUsuarios" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <div style="overflow: auto; width: 100%; height: 100%">
                                    <asp:GridView ID="GrvListado" runat="server" AutoGenerateColumns="False" DataKeyNames="iIdUsuario,sRucCliente,iTerCondiciones"
                                        SkinID="GrillaConsultaUnColor" Width="100%" 
                                        OnRowCommand="GrvListado_RowCommand"
                                        OnRowDataBound="GrvListado_RowDataBound" 
                                        AllowPaging="True" onpageindexchanging="GrvListado_PageIndexChanging" >
                                        <Columns>                                    
                                            <asp:BoundField DataField="sUsuario" HeaderText="Usuario" ItemStyle-HorizontalAlign="Left">
                                                <ItemStyle Width="10%" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="sNomUsuario" HeaderText="Nombres" ItemStyle-HorizontalAlign="Left" >
                                                <ItemStyle Width="20%" />
                                            </asp:BoundField>                                    
                                            <asp:BoundField DataField="sDNI" HeaderText="DNI"  >
                                                <ItemStyle Width="10%" />
                                            </asp:BoundField>                                    
                                            <asp:BoundField DataField="sCliente" HeaderText="Cliente" >
                                                <ItemStyle Width="20%" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="sCorreo" HeaderText="Correo" >
                                                <ItemStyle Width="15%" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="sFecha" HeaderText="F.Creacion">
                                                <ItemStyle Width="10%" />
                                            </asp:BoundField>
                                          <%--  <asp:BoundField DataField="sHabilitado" HeaderText="Habilitado">
                                                <ItemStyle Width="5%" />
                                            </asp:BoundField>--%>
                                            <asp:TemplateField HeaderText="Habilitado">
                                                <ItemTemplate>
                                                    <center>
                                                          <asp:CheckBox id="chkHabilitado"  enabled="false" runat="server" Checked='<%#Convert.ToBoolean(Eval("isHabilitado"))%>'/>
                                                    </center>
                                                  
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="sEsCliente" HeaderText="Es Cliente">
                                                <ItemStyle Width="5%" />
                                            </asp:BoundField>
                                             <asp:TemplateField ShowHeader="False">
                                                <ItemTemplate><center>
                                                   <asp:ImageButton ID="ImgHabilitar" runat="server" CausesValidation="false"  OnClientClick="return confirm('Esta Seguro de Habilitar al Usuario?');"
                                                       CommandName="Habilitar" ImageUrl="../../Script/Imagenes/IconButton/Checked_icon.png" ToolTip ="Habilitar Usuario" />                                                                                
                                                </center></ItemTemplate>
                                                <ItemStyle Width="5%" />
                                            </asp:TemplateField>         
                                            <asp:TemplateField ShowHeader="False">
                                                <ItemTemplate><center>
                                                   <asp:ImageButton ID="ImgAnular" runat="server" CausesValidation="false"  OnClientClick="return confirm('Esta Seguro de Anular el Usuario?');"
                                                       CommandName="Anular" ImageUrl="../../Script/Imagenes/IconButton/Delete_Icon.PNG" ToolTip ="Anular Usuario" />                                                                                
                                                </center></ItemTemplate>
                                                <ItemStyle Width="5%" />
                                            </asp:TemplateField>                                                                             
                                        </Columns>
                                    </asp:GridView>
                                </div>
                            </ContentTemplate>
                            <Triggers>
                                <ajax:AsyncPostBackTrigger ControlID="GrvListado" EventName="SelectedIndexChanged" />                        
                                <ajax:AsyncPostBackTrigger ControlID="ImgBtnBuscar" EventName="Click" />
                                <asp:AsyncPostBackTrigger ControlID="GrvListado" EventName="SelectedIndexChanged" />
                                <asp:AsyncPostBackTrigger ControlID="ImgBtnBuscar" EventName="Click" />
                                <asp:AsyncPostBackTrigger ControlID="GrvListado" EventName="SelectedIndexChanged" />
                                <asp:AsyncPostBackTrigger ControlID="ImgBtnBuscar" EventName="Click" />
                                <asp:AsyncPostBackTrigger ControlID="GrvListado" EventName="SelectedIndexChanged"></asp:AsyncPostBackTrigger>
                                <asp:AsyncPostBackTrigger ControlID="ImgBtnBuscar" EventName="Click"></asp:AsyncPostBackTrigger>
                            </Triggers>
                        </ajax:UpdatePanel>
                    </td>
                </tr>                
            </table>
        </ContentTemplate>
        <Triggers>                
                <asp:AsyncPostBackTrigger ControlID="ImgBtnNuevo" EventName="Click" />                
        </Triggers>
     </asp:UpdatePanel>


    <asp:Panel ID="pnlUsuarioNew" CssClass="modalPopup" runat="server" Style="display: none;">
        <ajax:UpdatePanel ID="UpUsuarioNew" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <table cellspacing="0" cellpadding="0" border="0" style="height: 60px; width: 100%;"
                    class="form_bandeja">
                    <tr>
                        <td style="padding-left: 15px; padding-top: 25px;" valign="middle">
                            <h2>
                                <span class="tituloComision">Datos del Usuario</span></h2>
                        </td>
                        <td align="right" style="padding-bottom: 5px; padding-top: 10px; padding-right: 15px;">
                            <asp:ImageButton ID="BtnGrabar" runat="server" ImageUrl="~/Imagenes/formulario/btn_grabar.jpg" OnClick="BtnGrabar_Click" />                            
                        </td>
                        <td>
                            <asp:ImageButton ID="BtnSalir" runat="server" ImageUrl="~/Imagenes/formulario/btn_cancelar.jpg" OnClick="BtnSalir_Click" />                            
                        </td>
                    </tr>
                </table>
                <table cellspacing="0" cellpadding="0" width="100%" border="0" class="cuerpoFondo">
                    <tr>
                        <td>
                            <table style="width: 100%; background-color: #ffffff;" class="cuerpoComision">                                
                                <tr>
                                    <td style="height: 21px" >
                                        Id
                                    </td>
                                    <td style="height: 21px">
                                        <asp:TextBox ID="txtIdUsuario" runat="server" Enabled="False"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 150px">
                                        Usuario</td>
                                    <td >
                                        <asp:TextBox ID="txtUsuario" runat="server" Width="240px" MaxLength="20"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 150px">
                                        Apellidos-Nombres</td>
                                    <td >
                                        <asp:TextBox ID="txtNomUsuario" runat="server" Width="482px" MaxLength="50"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 150px">
                                        DNI</td>
                                    <td>
                                        <asp:TextBox ID="txtDni" runat="server" MaxLength="8"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 150px">
                                        Correo Electronico</td>
                                    <td>
                                        <asp:TextBox ID="txtCorreo" runat="server" Width="240px" MaxLength="50"></asp:TextBox>
                                    </td>
                                </tr>
                                
                                <tr>
                                    <td style="width: 150px; height: 26px;">
                                        RUC Cliente
                                    </td>
                                    <td style="height: 26px" >
                                        <asp:TextBox ID="txtRucCli" runat="server" MaxLength="11"></asp:TextBox>                                                                             
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Es Cliente?
                                    </td>
                                    <td>
                                        <asp:CheckBox ID="chkEsCliente" runat="server" />
                                    </td>
                                </tr>
                                 <tr>
                                    <td>
                                       Es Agente Autorizador por SUNAT?
                                    </td>
                                    <td>
                                        <asp:CheckBox ID="chkTerCondiciones" runat="server" />
                                    </td>
                                </tr>
                            </table>
                            <asp:HiddenField ID="HdTipo" runat="server" />
                        </td>
                    </tr>
                </table>
            </ContentTemplate>
            <Triggers>
                <ajax:AsyncPostBackTrigger ControlID="GrvListado" EventName="RowCommand" />                
                <asp:AsyncPostBackTrigger ControlID="GrvListado" EventName="RowCommand" />
                <asp:AsyncPostBackTrigger ControlID="ImgBtnNuevo" EventName="Click" />
            </Triggers>
        </ajax:UpdatePanel>
    </asp:Panel>

    <ajaxToolkit:ModalPopupExtender ID="mpeUsuario" runat="server" BackgroundCssClass="modalBackground"
        CancelControlID="BtnClose" PopupControlID="pnlUsuarioNew" TargetControlID="BtnOpen">
    </ajaxToolkit:ModalPopupExtender>
    <asp:Button ID="BtnOpen" Style="display: none" runat="server" />
    <asp:Button ID="BtnClose" Style="display: none" runat="server" />


     <script language="javascript" type="text/javascript">

         var objFilaAnt = null;
         var backgroundColorFilaAnt = "";
         function fc_SeleccionaFilaSimple_(objFila) 
         {
             try {
                 if (objFilaAnt != null) {
                     objFilaAnt.style.backgroundColor = backgroundColorFilaAnt;
                 }

                 objFilaAnt = objFila;
                 backgroundColorFilaAnt = objFila.style.backgroundColor;
                 objFila.style.backgroundColor = "#C7C7C7";                 
             }

             catch (e) {
                 error = e.message;
             }
         }


         function fc_EditarDatos(iIdUsuario, sUsuario, snomUsuario, sDNI, sCorreo, sRucCliente, sEsCliente,iTermCondiciones) 
         {
            try {
                document.getElementById('<%=txtIdUsuario.ClientID%>').value = iIdUsuario;
                document.getElementById('<%=txtUsuario.ClientID%>').value = sUsuario;
                document.getElementById('<%=txtNomUsuario.ClientID%>').value = snomUsuario;
                document.getElementById('<%=txtDni.ClientID%>').value = sDNI;
                document.getElementById('<%=txtCorreo.ClientID%>').value = sCorreo;
                document.getElementById('<%=txtRucCli.ClientID%>').value = sRucCliente;

                if (sEsCliente == 'S')
                    document.getElementById('<%=chkEsCliente.ClientID%>').checked = true;
                else
                    document.getElementById('<%=chkEsCliente.ClientID%>').checked = false;


                if (iTermCondiciones == 1)
                    document.getElementById('<%=chkTerCondiciones.ClientID%>').checked = true;
                else
                    document.getElementById('<%=chkTerCondiciones.ClientID%>').checked = false;


                 document.getElementById('<%=HdTipo.ClientID%>').value = 'E';
                 document.getElementById("<%=ImgBtnNuevo.ClientID%>").click();
                 }
             catch (e) {
                 error = e.message;
             }
         }


         function SoloEnteros(e) {
             var tecla = document.all ? tecla = e.keyCode : tecla = e.which;
             return ((tecla > 47 && tecla < 58) || tecla == 44 || tecla == 46);
         }

         function fc_focus(event, sObject) {
             if (event.keyCode == 13) 
             {
                 if (sObject == 'txtUsuario') {
                     document.getElementById('<%=txtNomUsuario.ClientID %>').focus();
                 }
                 else if (sObject == 'txtNomUsuario') {
                     document.getElementById('<%=txtDni.ClientID %>').focus();
                 }
                 else if (sObject == 'txtDni') {
                     document.getElementById('<%=txtCorreo.ClientID %>').focus();
                 }
                 else if (sObject == 'txtCorreo') {
                     document.getElementById('<%=txtRucCli.ClientID %>').focus();
                 }
                 else if (sObject == 'txtRucCli') {
                     document.getElementById('<%=chkEsCliente.ClientID %>').focus();
                 }
                 else if (sObject == 'txtCliente' || sObject == 'txtUsu') {
                     document.getElementById('<%=ImgBtnBuscar.ClientID %>').click();
                 }
                 
                 return false;
             }             
         }

     </script>
</asp:Content>

