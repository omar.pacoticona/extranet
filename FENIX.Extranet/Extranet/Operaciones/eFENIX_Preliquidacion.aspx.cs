﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using FENIX.Common;
using FENIX.BusinessEntity;
using FENIX.BusinessLogic;
using System.Collections.Generic;
using QNET.Web.UI.Controls;

public partial class Fargonet_Facturacion_FENIX_Preliquidacion : System.Web.UI.Page
{

    #region "Eventos Pagina"   
    protected void Page_Load(object sender, EventArgs e)
    {       
        TxtVolante.Attributes.Add("onkeypress", "javascript:return SoloEnteros(event);");

        String tiempo = (Convert.ToInt32(GlobalEntity.Instancia.TiempoDuracionSesion.ToString()) * 1000 * 60).ToString();
        Session["Reset"] = true;

        if (!Page.IsPostBack)
        {
            ViewState["ordenLista"] = SortDirection.Descending;
            ListarDropDowList();           
            TxtDocumentoOrigen.BackColor = System.Drawing.Color.Silver;            
            BtnDocOri.Enabled = false;
            TxtFechaRetiro.RetornarFechas = Convert.ToString(DateTime.Now.ToString("dd/MM/yyyy"));
            TxtTotal.Text = "0.00";
            TxtIgv.Text =  "0.00";
            TxtSubTotal.Text =  "0.00";
            TxtDescuento.Text = "0.00";

            ScriptManager.RegisterStartupScript(this, GetType(), "mensaje", "SessionExpireAlert(" + tiempo + ");", true);
        }
        else
            ScriptManager.RegisterStartupScript(this, GetType(), "mensaje", "myStopFunction();", true);

        GrvListadoAlamcenaje.EmptyDataText = "No Existen Servicios a Liquidar";
        GrvListadoServicio.EmptyDataText = "No Existen Servicios a Liquidar";
    }   
    protected Boolean Validar()
    {
        Boolean bResultado = true;
        string sMensaje = string.Empty;
        if (RbtExportacion.Checked)
        {           
            if ((TxtDocumentoOrigen.Text.Trim().Length == 0))
                 
            {
                sMensaje = Resources.Resource.MsgNoDocOri;
                bResultado = false;
            }


        }
        else if (RbtImportacion.Checked)
        {
            if (TxtVolante.Text.Trim().Length == 0)
            {
                sMensaje = Resources.Resource.MsgNoVolante;
                bResultado = false;
            }
                    
        }

        if (sMensaje.Trim().Length > 0)
        {

            ScriptManager.RegisterStartupScript(TxtDocumentoOrigen, GetType(), Resources.Resource.MsgTitulo, string.Format(Resources.Resource.MsgJavaAlerta, sMensaje), true);

        }
        return bResultado;

    
    }    
    protected void chkSeleccionar_CheckedChanged(object sender, EventArgs e)
    {
        SeleccionarItemsCheck(sender);
    }
    protected void chkSeleccionarAlma_CheckedChanged(object sender, EventArgs e)
    {
        SeleccionarItemsCheck(sender);
    }
    protected void RbtImportacion_CheckedChanged(object sender, EventArgs e)
    {
        if (RbtImportacion.Checked)
        {
            InicializarControles();
            RbtExportacion.Checked = false;           
            BtnDocOri.Enabled = false;            
            TxtVolante.Enabled = true;
            MostrarColorActivos(System.Drawing.Color.Silver);
            TxtVolante.BackColor = System.Drawing.Color.White;
            TxtVolante.Focus();
            TxtFechaRetiro.RetornarFechas = DateTime.Now.ToString("dd/MM/yyyy");
            UpListadoServicio.Update();

        }
    }
    protected void RbtExportacion_CheckedChanged(object sender, EventArgs e)
    {
        if (RbtExportacion.Checked)
        {
            InicializarControles();
            RbtImportacion.Checked = false;
            BtnDocOri.Enabled = true;          
            TxtVolante.Enabled = false;
            MostrarColorActivos(System.Drawing.Color.White);
            TxtVolante.BackColor = System.Drawing.Color.Silver;
            BtnDocOri.Focus();
            TxtFechaRetiro.RetornarFechas = DateTime.Now.ToString("dd/MM/yyyy");
            UpListadoAlmacenaje.Update();

        }
    }
    protected void GrvListadoServicio_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            DataKey dataKey;
            dataKey = this.GrvListadoServicio.DataKeys[e.Row.RowIndex];
            BE_Liquidacion oitem = (e.Row.DataItem as BE_Liquidacion);
            
            e.Row.Style["cursor"] = "pointer";
            e.Row.Attributes["onclick"] = String.Format("javascript: fc_SeleccionaFilaSimple(this)");

            e.Row.Cells[10].ToolTip = oitem.sTarifa;

            if ((oitem.dDetraccion) > 0)
            {
                e.Row.Cells[15].Text = "<a href='' onclick='return fun_retorno();' class='tooltip' >" + oitem.dDetraccion.ToString() + "<span> Servicio Afecto a Detraccion con el [ " + oitem.sPorcentDetraccion  + " ]</span></a>";
            }

            for (int c = 1; c < e.Row.Cells.Count; c++)
            {
                if (oitem.sSituacion == Situacion_PreLiquidacion.Peligrosa.ToString())
                {
                    e.Row.Cells[c].BackColor = System.Drawing.Color.LightCoral;
                }
                else if (oitem.sSituacion == Situacion_PreLiquidacion.Pendiente.ToString())
                {
                    e.Row.Cells[c].BackColor = System.Drawing.Color.Lime;
                }
                else if (oitem.sSituacion == Situacion_PreLiquidacion.Sin_Tarifa.ToString())
                {
                    e.Row.Cells[c].BackColor = System.Drawing.Color.Yellow;
                }
            }
        }
    }
    protected void GrvListadoAlamcenaje_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            DataKey dataKey;
            dataKey = this.GrvListadoAlamcenaje.DataKeys[e.Row.RowIndex];
            BE_Liquidacion oitem = (e.Row.DataItem as BE_Liquidacion);

            CheckBox chkSeleccion = (CheckBox)e.Row.FindControl("chkSeleccionarAlma");

            e.Row.Style["cursor"] = "pointer";
            e.Row.Attributes["onclick"] = String.Format("javascript: fc_SeleccionaFilaSimple(this)");


            //if (e.Row.Cells[21].Controls.Count > 0)
            //{
            //    ImageButton ibtn = (ImageButton)e.Row.Cells[21].Controls[0];
            //    ibtn.OnClientClick = string.Format("javascript:return OpenDescuento('{0}','{1}');", chkSeleccion.ClientID, oitem.sDescripcionServicio);
            //    ibtn.Visible = false;
            //}

            e.Row.Cells[10].ToolTip = oitem.sTarifa;

            for (int c = 1; c < e.Row.Cells.Count; c++)
            {

                if (oitem.sSituacion == Situacion_PreLiquidacion.Peligrosa.ToString())
                {
                    e.Row.Cells[c].BackColor = System.Drawing.Color.LightCoral;
                }
                else if (oitem.sSituacion == Situacion_PreLiquidacion.Pendiente.ToString())
                {
                    e.Row.Cells[c].BackColor = System.Drawing.Color.Lime;
                }
                else if (oitem.sSituacion == Situacion_PreLiquidacion.Sin_Tarifa.ToString())
                {
                    e.Row.Cells[c].BackColor = System.Drawing.Color.Yellow;
                }
            }
        }

    }     
    protected void GrvListado_Sorting(object sender, GridViewSortEventArgs e)
    {
        BE_LiquidacionList oBE_LiquidacionServicio = (BE_LiquidacionList)ViewState["oBE_LiquidacionServicio"];
        SortDirection indOrden = (SortDirection)ViewState["ordenLista"];

        foreach (GridViewRow GrvRow in GrvListadoServicio.Rows)
        {
            if ((GrvRow.FindControl("chkSeleccionar") as CheckBox).Checked)
            {
                ScriptManager.RegisterStartupScript(UpListadoServicio, GetType(), "mensaje", string.Format(Resources.Resource.MsgAlerta, "Debe Desmarcar todos los registros"), true);
                return;
            }
        }


        if (oBE_LiquidacionServicio != null)
        {
            if (indOrden == SortDirection.Ascending)
            {
                oBE_LiquidacionServicio.Ordenar(e.SortExpression, direccionOrden.Descending);
                ViewState["ordenLista"] = SortDirection.Descending;
            }
            else
            {
                oBE_LiquidacionServicio.Ordenar(e.SortExpression, direccionOrden.Ascending);
                ViewState["ordenLista"] = SortDirection.Ascending;
            }
        }
        this.GrvListadoServicio.DataSource = oBE_LiquidacionServicio;
        this.GrvListadoServicio.DataBind();

        ViewState["GrvListadoServicio"] = oBE_LiquidacionServicio;
    }
    protected void GrvListadoAlamcenaje_Sorting(object sender, GridViewSortEventArgs e)
    {
        BE_LiquidacionList oBE_LiquidacionAlmacenaje = (BE_LiquidacionList)ViewState["oBE_LiquidacionAlmacenaje"];
        SortDirection indOrden = (SortDirection)ViewState["ordenLista"];

        foreach (GridViewRow GrvRow in GrvListadoAlamcenaje.Rows)
        {
            if ((GrvRow.FindControl("chkSeleccionarAlma") as CheckBox).Checked)
            {
                ScriptManager.RegisterStartupScript(UpListadoAlmacenaje, GetType(), "mensaje", string.Format(Resources.Resource.MsgAlerta, "Debe Desmarcar todos los registros"), true);
                return;
            }
        }

        if (oBE_LiquidacionAlmacenaje != null)
        {
            if (indOrden == SortDirection.Ascending)
            {
                oBE_LiquidacionAlmacenaje.Ordenar(e.SortExpression, direccionOrden.Descending);
                ViewState["ordenLista"] = SortDirection.Descending;
            }
            else
            {
                oBE_LiquidacionAlmacenaje.Ordenar(e.SortExpression, direccionOrden.Ascending);
                ViewState["ordenLista"] = SortDirection.Ascending;
            }
        }
        this.GrvListadoAlamcenaje.DataSource = oBE_LiquidacionAlmacenaje;
        this.GrvListadoAlamcenaje.DataBind();

        ViewState["oBE_LiquidacionAlmacenaje"] = oBE_LiquidacionAlmacenaje;
    }

    protected void btn_cerrar_Click(object sender, EventArgs e)
    {
        if (GlobalEntity.Instancia.CerrarExtranet == "S")
        {
            BL_Usuario oBL_Usuario = new BL_Usuario();
            oBL_Usuario.GrabaInicioSesion(GlobalEntity.Instancia.Usuario, "0");
            Session.Abandon();
            FormsAuthentication.SignOut();
            Response.Redirect("../../eFENIX_Login.aspx");
            Response.End();
        }
    }


    #endregion
    
    #region "Metodos"
    public void InicializarControles()
    {
        GrvListadoAlamcenaje.DataSource = null;
        GrvListadoServicio.DataSource = null;
        GrvListadoAlamcenaje.DataBind();
        GrvListadoServicio.DataBind();
        TxtVolante.Text = string.Empty;

        TxtVolante.Text = string.Empty;
        Txtcliente.Text = string.Empty;
        TxtClienteContrato.Text = string.Empty;
        TxtFechaRetiro.RetornarFechas = string.Empty;
        TxtContrato.Text = string.Empty;
        DplMoneda.SelectedIndex=0;
        TxtObservacion.Text = string.Empty;

        TxtSubTotal.Text = "0";
        TxtTotal.Text = "0";
        TxtIgv.Text = "0";
        TxtDescuento.Text = "0";
        TxtDocumentoOrigen.Text = string.Empty;        
        hdIdOrdSerDet.Value = string.Empty;
        TxtIdDocOri.Text = string.Empty;
        LblMessage.Text = String.Empty;
        TxtFechaRetiroInfo.Text = String.Empty;
        

    }
    protected void MostrarColorActivos(System.Drawing.Color oColor)
    {
       
        TxtVolante.BackColor = oColor;
        TxtDocumentoOrigen.BackColor = oColor;

    }
    protected void SeleccionarItemsCheck(object sender)
    {
        Decimal dDescuento = 0;
        Decimal dAfecto = 0;
        Decimal dTotal = 0;
        Decimal dIgv = 0;

        CheckBox oChkSeleccionar = (CheckBox)sender;
        GridViewRow oGridViewRow = (GridViewRow)oChkSeleccionar.NamingContainer;
        GridView oGridView = (GridView)oGridViewRow.NamingContainer;

        dDescuento = dDescuento + Convert.ToDecimal(oGridView.DataKeys[oGridViewRow.RowIndex].Values["dDescuento"]);
        dAfecto = dAfecto + Convert.ToDecimal(oGridView.DataKeys[oGridViewRow.RowIndex].Values["dAfecto"]);
        dTotal = dTotal + Convert.ToDecimal(oGridView.DataKeys[oGridViewRow.RowIndex].Values["dTotal"]);
        dIgv = dIgv + Convert.ToDecimal(oGridView.DataKeys[oGridViewRow.RowIndex].Values["dIgv"]);

        if (oChkSeleccionar.Checked)
        {
            TxtTotal.Text = Convert.ToString(Convert.ToDecimal(TxtTotal.Text) + dTotal);
            TxtIgv.Text = Convert.ToString(Convert.ToDecimal(TxtIgv.Text) + dIgv);
            TxtSubTotal.Text = Convert.ToString(Convert.ToDecimal(TxtSubTotal.Text) + dAfecto);
            TxtDescuento.Text = Convert.ToString(Convert.ToDecimal(TxtDescuento.Text) + dDescuento);
        }
        else
        {
            TxtTotal.Text = Convert.ToString(Convert.ToDecimal(TxtTotal.Text) - dTotal);
            TxtIgv.Text = Convert.ToString(Convert.ToDecimal(TxtIgv.Text) - dIgv);
            TxtSubTotal.Text = Convert.ToString(Convert.ToDecimal(TxtSubTotal.Text) - dAfecto);
            TxtDescuento.Text = Convert.ToString(Convert.ToDecimal(TxtDescuento.Text) - dDescuento);
        }
    }
    protected Int32 AsignaIdModalidad(String sModalidad)
    {
        Int32 iModalidad = 0;
        if (sModalidad.ToUpper() == "POR UNO")
        {
            iModalidad = 80;
        }
        else if (sModalidad.ToUpper() == "POR HORA")
        {
            iModalidad = 81;
        }
        else if (sModalidad.ToUpper() == "POR CANTIDAD")
        {
            iModalidad = 82;
        }
        else if (sModalidad.ToUpper() == "POR PESO")
        {
            iModalidad = 83;
        }
        else if (sModalidad.ToUpper() == "POR VOLUMEN")
        {
            iModalidad = 84;
        }

        return iModalidad;
    }

    #endregion

    #region "Transacciones"
    protected void ConsultarServicios(int? iIdDocOri)
    {
        BE_Liquidacion oBE_Liquidacion = new BE_Liquidacion();
        BL_Liquidacion oBL_Liquidacion = new BL_Liquidacion();
        BE_LiquidacionList oBE_LiquidacionServicio = new BE_LiquidacionList();
        BE_LiquidacionList oBE_LiquidacionAlmacenaje = new BE_LiquidacionList();
        int iRetorno = 0;
        string[] Resultado = null;
       
        oBE_Liquidacion.iIdDocOri = iIdDocOri;

        if (TxtFechaRetiro.RetornarFechas.Trim().Length > 0)
            oBE_Liquidacion.dtFecha_Liquidacion = Convert.ToDateTime(TxtFechaRetiro.RetornarFechas + " " + DateTime.Now.Hour.ToString() + ":" + DateTime.Now.Minute.ToString() + ":" + DateTime.Now.Second.ToString() );

        oBE_Liquidacion.iIdMoneda = Convert.ToInt32(DplMoneda.SelectedValue);

        if (RbtImportacion.Checked)
        {
            oBE_Liquidacion.iIdOperacion = Convert.ToInt32(TipoOperacion.Importacion);
            oBE_Liquidacion.sNroVolante = TxtVolante.Text;
        }

        if (RbtExportacion.Checked)
            oBE_Liquidacion.iIdOperacion = Convert.ToInt32(TipoOperacion.Exportacion);



        oBE_Liquidacion.iIdLineaNegocio = 89;


        if (RbtImportacion.Checked)
            Resultado = oBL_Liquidacion.Listar_Contrato(Convert.ToInt32(TxtVolante.Text), null, Convert.ToInt32(TipoOperacion.Importacion)).Split('|');
        else if (RbtExportacion.Checked)
            Resultado = oBL_Liquidacion.Listar_Contrato(null, iIdDocOri, Convert.ToInt32(TipoOperacion.Exportacion)).Split('|');

        if (Resultado.Length > 0 && Resultado[0].Trim().Length > 0)
        {
            oBE_Liquidacion.iIdContrato = Convert.ToInt32(Resultado[3]);
            
            ViewState["IdContrato"] = Resultado[3];
            oBE_Liquidacion.sClienteContrato = Resultado[1];
            oBE_Liquidacion.sObservacion = Resultado[2];
            oBE_Liquidacion.sNroContrato = Resultado[0];
            TxtIdContrato.Text = ViewState["IdContrato"].ToString();
            TxtContrato.Text = Resultado[0];
            TxtClienteContrato.Text = Resultado[1];
            TxtObservacion.Text = Resultado[2];

        } else

        {
           
            TxtContrato.Text = String.Empty;
            TxtClienteContrato.Text = String.Empty;
            TxtObservacion.Text = String.Empty;
            TxtIdContrato.Text = string.Empty;
            ViewState["IdContrato"] = null;

        }

        if (RbtImportacion.Checked)
            iRetorno = oBL_Liquidacion.RegistrarServicios_Mandatorios(oBE_Liquidacion.iIdContrato, Convert.ToInt32(TxtVolante.Text),null, Convert.ToInt32(TipoOperacion.Importacion));
        else if (RbtExportacion.Checked)
            iRetorno = oBL_Liquidacion.RegistrarServicios_Mandatorios(oBE_Liquidacion.iIdContrato, null,iIdDocOri, Convert.ToInt32(TipoOperacion.Exportacion));

        if (iRetorno == 1)
        {
            LblMessage.Text = "El documento ya cuenta con Servicios Liquidados!!!";
        }


        BE_LiquidacionList lista = oBL_Liquidacion.Listar_PreLinquidacion(oBE_Liquidacion);

        if ((lista != null) && (lista.Count>0))
        {
            for (int i = 0; i < lista.Count; i++)
            {

                if ((lista[i].sDescripcionServicio.Trim().Length >= 10) && (lista[i].sDescripcionServicio.Trim().Substring(0, 10).ToUpper() == "ALMACENAJE"))
                {
                    BE_Liquidacion oBE_Servicio = new BE_Liquidacion();
                    oBE_Servicio.iIdOrdSer = lista[i].iIdOrdSer;
                    oBE_Servicio.iIdOrdSerDet = lista[i].iIdOrdSerDet;
                    oBE_Servicio.sDescripcionServicio = lista[i].sDescripcionServicio;
                    oBE_Servicio.sContenedor = lista[i].sContenedor;
                    oBE_Servicio.sTipoCarga = lista[i].sTipoCarga;
                    oBE_Servicio.sTipoContenedor = lista[i].sTipoContenedor;
                    oBE_Servicio.iIdTamanoContenedor = lista[i].iIdTamanoContenedor;
                    oBE_Servicio.sEmbalaje = lista[i].sEmbalaje;
                    oBE_Servicio.iIdTarifa = lista[i].iIdTarifa;
                    oBE_Servicio.dMontoUnitario = lista[i].dMontoUnitario;
                    oBE_Servicio.dCantidad = lista[i].dCantidad;
                    oBE_Servicio.dAfecto = lista[i].dAfecto;
                    oBE_Servicio.dInafecto = lista[i].dInafecto;
                    oBE_Servicio.dDescuento = lista[i].dDescuento;
                    oBE_Servicio.dIgv = lista[i].dIgv;
                    oBE_Servicio.dTotal = lista[i].dTotal;
                    oBE_Servicio.sModalidad = lista[i].sModalidad;
                    oBE_Servicio.sOrigen = lista[i].sOrigen;
                    oBE_Servicio.sSituacion = lista[i].sSituacion;
                    oBE_Servicio.dtFecha_Ingreso = lista[i].dtFecha_Ingreso;
                    oBE_Servicio.iItem = lista[i].iItem;
                    oBE_Servicio.sTarifa = lista[i].sTarifa;
                    oBE_Servicio.sTipoMoneda = lista[i].sTipoMoneda;
                    oBE_Servicio.sRetroactivo = lista[i].sRetroactivo;
                    oBE_Servicio.dtFechaRetiro = lista[i].dtFechaRetiro;
                    oBE_Servicio.iDiasLibres = lista[i].iDiasLibres;
                    TxtDiasLibres.Text = lista[i].iDiasLibres.ToString();
                    TxtRetroactivo.Text = lista[i].sRetroactivo.ToString();
                    oBE_LiquidacionAlmacenaje.Add(oBE_Servicio);


                }

                else
                {

                    BE_Liquidacion oBE_Servicio = new BE_Liquidacion();
                    oBE_Servicio.iIdOrdSer = lista[i].iIdOrdSer;
                    oBE_Servicio.iIdOrdSerDet = lista[i].iIdOrdSerDet;
                    oBE_Servicio.sDescripcionServicio = lista[i].sDescripcionServicio;
                    oBE_Servicio.sContenedor = lista[i].sContenedor;
                    oBE_Servicio.sTipoCarga = lista[i].sTipoCarga;
                    oBE_Servicio.sTipoContenedor = lista[i].sTipoContenedor;
                    oBE_Servicio.iIdTamanoContenedor = lista[i].iIdTamanoContenedor;
                    oBE_Servicio.sEmbalaje = lista[i].sEmbalaje;
                    oBE_Servicio.iIdTarifa = lista[i].iIdTarifa;
                    oBE_Servicio.dMontoUnitario = lista[i].dMontoUnitario;
                    oBE_Servicio.dCantidad = lista[i].dCantidad;
                    oBE_Servicio.dAfecto = lista[i].dAfecto;
                    oBE_Servicio.dInafecto = lista[i].dInafecto;
                    oBE_Servicio.dDescuento = lista[i].dDescuento;
                    oBE_Servicio.dIgv = lista[i].dIgv;
                    oBE_Servicio.dTotal = lista[i].dTotal;
                    oBE_Servicio.sModalidad = lista[i].sModalidad;
                    oBE_Servicio.sOrigen = lista[i].sOrigen;
                    oBE_Servicio.sSituacion = lista[i].sSituacion;
                    oBE_Servicio.dtFecha_Ingreso = lista[i].dtFecha_Ingreso;
                    oBE_Servicio.iItem = lista[i].iItem;
                    oBE_Servicio.sTarifa = lista[i].sTarifa;
                    oBE_Servicio.sTipoMoneda = lista[i].sTipoMoneda;
                    oBE_Servicio.dDetraccion = lista[i].dDetraccion;
                    oBE_Servicio.sPorcentDetraccion = lista[i].sPorcentDetraccion;
                    oBE_LiquidacionServicio.Add(oBE_Servicio);
                }

            }


            if (oBE_LiquidacionServicio[0].dtFechaRetiro != null)
                TxtFechaRetiroInfo.Text = oBE_LiquidacionServicio[0].dtFechaRetiro.ToString();

            if (TxtFechaRetiroInfo.Text.Trim().Length == 0)
                TxtFechaRetiroInfo.Text = "Sin Retiro";
        }
        upInfoAlmacenaje.Update();
        ViewState["oBE_LiquidacionServicio"] = oBE_LiquidacionServicio;
        GrvListadoServicio.DataSource = oBE_LiquidacionServicio;
        GrvListadoServicio.DataBind();

        ViewState["oBE_LiquidacionAlmacenaje"] = oBE_LiquidacionAlmacenaje;
        GrvListadoAlamcenaje.DataSource = oBE_LiquidacionAlmacenaje;
        GrvListadoAlamcenaje.DataBind();
       


    }
    protected void ListarDropDowList()
    {
        try
        {
            BL_Liquidacion oBL_Liquidacion = new BL_Liquidacion();
            List<BE_Tabla> olst_TablaTipo = new List<BE_Tabla>();

            int i = 0;
            DplMoneda.Items.Clear();

            olst_TablaTipo = oBL_Liquidacion.ListarMoneda();
            for (i = 0; i < olst_TablaTipo.Count; ++i)
            {
                DplMoneda.Items.Add(new ListItem(olst_TablaTipo[i].sDescripcion.ToString(), olst_TablaTipo[i].iIdValor.ToString()));
            }


            i = 0;

            DplMoneda.SelectedIndex = 0;

            DplTipo_Desc.Items.Add(new ListItem("Por Porcentaje", "1"));
            DplTipo_Desc.Items.Add(new ListItem("Por Valor", "2"));

            DplTipo_Desc.SelectedIndex = 1;

        }
        catch (Exception e)
        {
            new ResultadoInterfase(ResultadoInterfase.TipoResultado.Error, e);
        }


    }
    protected void GrabarLiquidacion()
    {
        BE_Liquidacion oBE_Liquidacion = new BE_Liquidacion();
        BL_Liquidacion oBL_Liquidacion = new BL_Liquidacion();
        List<BE_Liquidacion> Lis_BE_Liquidacion = new List<BE_Liquidacion>();
        String[] sResultado = null;
        Int32 vl_iCodigo = 0;
        String vl_sMessage = String.Empty;

        oBE_Liquidacion.iIdMoneda = Convert.ToInt32(DplMoneda.SelectedValue);
        //oBE_Liquidacion.iIdVolante = hdid

        if (RbtExportacion.Checked)
            oBE_Liquidacion.iIdOperacion = 65;
        else if (RbtImportacion.Checked)
            oBE_Liquidacion.iIdOperacion = 66;

        oBE_Liquidacion.iIdContrato = Convert.ToInt32(TxtIdContrato.Text);
        oBE_Liquidacion.sNroVolante = TxtVolante.Text;
        oBE_Liquidacion.dtFechaRetiro = Convert.ToDateTime(TxtFechaRetiro.RetornarFechas);
        oBE_Liquidacion.sNroContrato = TxtContrato.Text;
        oBE_Liquidacion.sUsuario = GlobalEntity.Instancia.Usuario;
        oBE_Liquidacion.sNombrePc = GlobalEntity.Instancia.NombrePc;


        foreach (GridViewRow GrvRow in GrvListadoServicio.Rows)
        {
            BE_Liquidacion oBE_Liquidacion_Lis = new BE_Liquidacion();
            oBE_Liquidacion_Lis.iIdOrdSer = Convert.ToInt32(GrvListadoServicio.DataKeys[GrvRow.RowIndex].Values["iIdOrdSer"].ToString());
            oBE_Liquidacion_Lis.iIdOrdSerDet = Convert.ToInt32(GrvListadoServicio.DataKeys[GrvRow.RowIndex].Values["iIdOrdSerDet"].ToString());            
            oBE_Liquidacion_Lis.iIdTarifa = Convert.ToInt32(GrvListadoServicio.DataKeys[GrvRow.RowIndex].Values["iIdTarifa"].ToString());
            oBE_Liquidacion_Lis.dMontoBruto = Convert.ToDecimal(GrvListadoServicio.DataKeys[GrvRow.RowIndex].Values["dMontoUnitario"].ToString());
            oBE_Liquidacion_Lis.dCantidad = Convert.ToDecimal(GrvListadoServicio.DataKeys[GrvRow.RowIndex].Values["dCantidad"].ToString());
            oBE_Liquidacion_Lis.dAfecto = Convert.ToDecimal(GrvListadoServicio.DataKeys[GrvRow.RowIndex].Values["dAfecto"].ToString());
            oBE_Liquidacion_Lis.dDetraccion = Convert.ToDecimal(GrvListadoServicio.DataKeys[GrvRow.RowIndex].Values["dDetraccion"].ToString());
            oBE_Liquidacion_Lis.dIgv = Convert.ToDecimal(GrvListadoServicio.DataKeys[GrvRow.RowIndex].Values["dIgv"].ToString());
            oBE_Liquidacion_Lis.dTotal = Convert.ToDecimal(GrvListadoServicio.DataKeys[GrvRow.RowIndex].Values["dTotal"].ToString());



            oBE_Liquidacion_Lis.sModalidad = Convert.ToString(AsignaIdModalidad(GrvListadoServicio.DataKeys[GrvRow.RowIndex].Values["sModalidad"].ToString()));
            oBE_Liquidacion_Lis.sOrigen = GrvListadoServicio.DataKeys[GrvRow.RowIndex].Values["sOrigen"].ToString(); 
            
            oBE_Liquidacion_Lis.sUsuario = GlobalEntity.Instancia.Usuario;
            oBE_Liquidacion_Lis.sNombrePc = GlobalEntity.Instancia.NombrePc;


            Lis_BE_Liquidacion.Add(oBE_Liquidacion_Lis);
            oBE_Liquidacion_Lis = null;
        }



        sResultado = oBL_Liquidacion.Insertar_PreLiquidacion(oBE_Liquidacion, Lis_BE_Liquidacion).Split('|');

        for (int i = 0; i < sResultado.Length; i++)
        {
            if (i == 0)
            {
                vl_iCodigo = Convert.ToInt32(sResultado[i]);
            }
            else
            {
                vl_sMessage += sResultado[i];
            }
        }


        ScriptManager.RegisterStartupScript(TxtDocumentoOrigen, GetType(), "mensaje", String.Format(Resources.Resource.MsgAlerta, vl_sMessage), true);
        return;


    }
    #endregion

    #region "Botones"
    protected void BtnImprimir_Click(object sender, ImageClickEventArgs e)
    {

        String sIdOrdSerDet = String.Empty;
        foreach (GridViewRow GrvRow in GrvListadoAlamcenaje.Rows)
        {
            if ((GrvRow.FindControl("chkSeleccionarAlma") as CheckBox).Checked)
            {

                sIdOrdSerDet = sIdOrdSerDet + GrvListadoAlamcenaje.DataKeys[GrvRow.RowIndex].Values["iIdOrdSerDet"] + "|";

            }
        }

        foreach (GridViewRow GrvRow in GrvListadoServicio.Rows)
        {
            if ((GrvRow.FindControl("chkSeleccionar") as CheckBox).Checked)
            {
                sIdOrdSerDet = sIdOrdSerDet + GrvListadoServicio.DataKeys[GrvRow.RowIndex].Values["iIdOrdSerDet"] + "|";

            }
        }

        ScriptManager.RegisterStartupScript(upImprimir, GetType(), "xx_Script_xx", "javascript: fc_Imprimir('" + DplMoneda.SelectedValue + "','" + TxtIdContrato.Text + "','" + sIdOrdSerDet + "','" + TxtContrato.Text + "');", true);

    }   
    protected void BtnNuevo_Click(object sender, EventArgs e)
    {
        InicializarControles();
        ViewState["IdNaveViaje"] = null;
        ViewState["iIdDocOri"] = null;
        ViewState["IdContrato"] = null;
        hdCodigoDO.Value = "";
        TxtDocumentoOrigen.BackColor = System.Drawing.Color.Silver;
        TxtVolante.Enabled = true;
        TxtVolante.BackColor = System.Drawing.Color.White;
        TxtFechaRetiro.RetornarFechas = DateTime.Now.ToString("dd/MM/yyyy");

        RbtImportacion.Checked = true;
        RbtExportacion.Checked = false;

        BtnLiquidar.Enabled = true;
        TxtIdContrato.Text = String.Empty;
        TxtVolante.Focus();


    }
    protected void btnGrabar_Click(object sender, EventArgs e)
    {
        BL_Liquidacion oBL_Liquidacion = new BL_Liquidacion();

        decimal dMonto = 0;
        Int32 iIdOrdSer = 0;
        Int32 iIdOrdSerDet = 0;
        foreach (GridViewRow GrvRow in GrvListadoAlamcenaje.Rows)
        {
            if ((GrvRow.FindControl("chkSeleccionarAlma") as CheckBox).Checked)
            {
                dMonto = dMonto + Convert.ToInt32(GrvListadoAlamcenaje.DataKeys[GrvRow.RowIndex].Values["dTotal"]);
                iIdOrdSer = Convert.ToInt32(GrvListadoAlamcenaje.DataKeys[GrvRow.RowIndex].Values["iIdOrdSer"]);
                iIdOrdSerDet = Convert.ToInt32(GrvListadoAlamcenaje.DataKeys[GrvRow.RowIndex].Values["iIdOrdSerDet"]);

                oBL_Liquidacion.RegistrarDescuento(Convert.ToInt32(DplTipo_Desc.SelectedValue), Convert.ToDecimal(TxtValor_Des.Text), iIdOrdSer, iIdOrdSerDet, dMonto);
                ConsultarServicios(Convert.ToInt32((ViewState["iIdDocOri"].ToString())));
                mpeDescuento.Hide();

            }
        }

        TxtTotal.Text = "0";
        TxtSubTotal.Text = "0";
        TxtIgv.Text = "0";
        TxtDescuento.Text = "0";
        UpListadoAlmacenaje.Update();

    }
    protected void ucBusqueda_Load(object sender, EventArgs e)
    {

        try
        {
            UserControl oUserControl = (UserControl)sender;
            HiddenField hdBuscar = oUserControl.FindControl("hdBuscar") as HiddenField;
            HiddenField hdConsultar = oUserControl.FindControl("hdConsultar") as HiddenField;

            HiddenField hdCodigo = oUserControl.FindControl("hdCodigo") as HiddenField;
            HiddenField hdDescripcion = oUserControl.FindControl("hdDescripcion") as HiddenField;
            String sTipoResultadoUserControl = hdBuscar.Value;
            GridView oGridView = oUserControl.FindControl("GrvListado") as GridView;

            HiddenField hdDescripcion1 = oUserControl.FindControl("hdDescripcion1") as HiddenField;
            HiddenField hdDescripcion2 = oUserControl.FindControl("hdDescripcion2") as HiddenField;
            HiddenField hdDescripcion3 = oUserControl.FindControl("hdDescripcion3") as HiddenField;
            HiddenField hdDescripcion4 = oUserControl.FindControl("hdDescripcion4") as HiddenField;
            HiddenField hdDescripcion5 = oUserControl.FindControl("hdDescripcion5") as HiddenField;

            oGridView.DataSource = null;
            oGridView.DataBind();

            if (hdIdDocOri.Value == Constantes.sAcepto)
            {
                ucBusqueda.tipoBusqueda = Comun.Busqueda.DocumentoOrigen_Liquidacion;
                if (ViewState["IdNaveViaje"] != null)
                    ucBusqueda.CodigoSecundario = Convert.ToInt32(ViewState["IdNaveViaje"].ToString());
                ucBusqueda.Col_DescripcionText = "Descripción";
                ucBusqueda.ColumnaVisible = 4;

                ucBusqueda.Titulo = Constantes.Titulo_Servicio;
                hdConsultar.Value = "0";
                hdIdDocOri.Value = "0";
            }

            if (sTipoResultadoUserControl == Constantes.sAcepto) // Acepto
            {
                String[] sNavVia = null;
                ViewState["iIdDocOri"] = hdDescripcion2.Value;
                TxtIdDocOri.Text = hdDescripcion2.Value;
                TxtDocumentoOrigen.Text = hdDescripcion.Value;

                sNavVia = hdDescripcion4.Value.Split('|');
                TextBox TxtManifiestoUc = oUserControl.FindControl("TxtManifiesto") as TextBox;
                TextBox TxtDescripcionUc = oUserControl.FindControl("TxtDescripcion") as TextBox;
                TextBox TxtCodigoUc = oUserControl.FindControl("TxtCodigo") as TextBox;

                TxtManifiestoUc.Text = String.Empty;
                TxtDescripcionUc.Text = String.Empty;
                TxtCodigoUc.Text = String.Empty;
                upCabecera2.Update();

            }
            else if (sTipoResultadoUserControl == Constantes.sCancelo)  // cancelo
            {
                mpBusqueda.Hide();

                //Limpiamos los TextBox
                TextBox TxtManifiestoUc = oUserControl.FindControl("TxtManifiesto") as TextBox;
                TextBox TxtDescripcionUc = oUserControl.FindControl("TxtDescripcion") as TextBox;
                TextBox TxtCodigoUc = oUserControl.FindControl("TxtCodigo") as TextBox;

                TxtManifiestoUc.Text = String.Empty;
                TxtDescripcionUc.Text = String.Empty;
                TxtCodigoUc.Text = String.Empty;
                TxtFechaRetiro.Focus();
            }

            hdBuscar.Value = "";

        }
        catch (Exception ex)
        {
            new ResultadoInterfase(ResultadoInterfase.TipoResultado.Error, ex);
        }
    }
    protected void BtnLiquidar_Click(object sender, EventArgs e)
    {

        BE_Liquidacion oBE_Liquidacion = new BE_Liquidacion();
        BL_Liquidacion oBL_Liquidacion = new BL_Liquidacion();
        String[] sResultado = null;
        Int32 vl_iCodigo = 0;
        String vl_sMessage = String.Empty;
        sResultado = oBL_Liquidacion.Validar_TipoCambio().Split('|');

        for (int i = 0; i < sResultado.Length; i++)
        {
            if (i == 0)
            {
                vl_iCodigo = Convert.ToInt32(sResultado[i]);
            }
            else
            {
                vl_sMessage += sResultado[i];
            }
        }

        if (vl_iCodigo < 1)
        {
            ScriptManager.RegisterStartupScript(TxtDocumentoOrigen, GetType(), "mensaje", String.Format(Resources.Resource.MsgAlerta, vl_sMessage), true);
            return;
        }

        TxtSubTotal.Text = "0";
        TxtTotal.Text = "0";
        TxtIgv.Text = "0";
        TxtDescuento.Text = "0";

        if (!Validar())
        {
            return;
        }

        try
        {

            DateTime resp = Convert.ToDateTime(TxtFechaRetiro.RetornarFechas);
        }
        catch
        {
            ScriptManager.RegisterStartupScript(TxtDocumentoOrigen, GetType(), Resources.Resource.MsgTitulo, string.Format(Resources.Resource.MsgJavaAlerta, Resources.Resource.MsgFechaInvalidaJava), true);
            return;
        }


        if (RbtImportacion.Checked)
            oBE_Liquidacion = oBL_Liquidacion.Listar_PreLiquidacion_Info_Cabecera(TxtVolante.Text, Convert.ToInt32(TipoOperacion.Importacion), null, null);
        else if (RbtExportacion.Checked)
            oBE_Liquidacion = oBL_Liquidacion.Listar_PreLiquidacion_Info_Cabecera(null, Convert.ToInt32(TipoOperacion.Exportacion), Convert.ToInt32(ViewState["iIdDocOri"].ToString()), Convert.ToInt32(ViewState["IdNaveViaje"].ToString()));

        if (oBE_Liquidacion == null)
        {
            InicializarControles();
            ScriptManager.RegisterStartupScript(TxtDocumentoOrigen, GetType(), Resources.Resource.MsgTitulo, string.Format(Resources.Resource.MsgJavaAlerta, Resources.Resource.MsgNoExistePreLiq), true);

        }
        else
        {
            Txtcliente.Text = oBE_Liquidacion.sCliente;
            TxtClienteContrato.Text = oBE_Liquidacion.sClienteContrato;
            TxtDocumentoOrigen.Text = oBE_Liquidacion.sNumeroDo;
            ConsultarServicios(oBE_Liquidacion.iIdDocOri);
            hdIdOrdSerDet.Value = string.Empty;
            TxtIdDocOri.Text = Convert.ToString(oBE_Liquidacion.iIdDocOri);
            ViewState["iIdDocOri"] = oBE_Liquidacion.iIdDocOri;
            BtnLiquidar.Enabled = false;
        }
    }
    protected void BtnGrabarLiq_Click(object sender, EventArgs e)
    {
        GrabarLiquidacion();
    }
    #endregion
}
