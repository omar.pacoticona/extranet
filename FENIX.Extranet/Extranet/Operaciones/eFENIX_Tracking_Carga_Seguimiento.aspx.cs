﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using QNET.Web.UI.Controls;
using FENIX.BusinessEntity;
using FENIX.BusinessLogic;
using System.Text;
using FENIX.Common;
using System.Collections.Generic;
using System.Drawing;


public partial class Extranet_Operaciones_eFENIX_Tracking_Carga_Seguimiento : System.Web.UI.Page
{
    #region "Evento Pagina"
        protected void Page_Load(object sender, EventArgs e)
        {
            dnvListado.BindGridView += new QNET.Web.UI.Controls.DataNavigator.BindGridViewDelegate(BindGridLista);

            TxtAnoManifiesto.Attributes.Add("onkeydown", "javascript:return fc_PressKeyDO();");
            TxtNumManifiesto.Attributes.Add("onkeydown", "javascript:return fc_PressKeyDO();");
            TxtDocumento.Attributes.Add("onkeydown", "javascript:return fc_PressKeyDO();");
            TxtContenedor.Attributes.Add("onkeydown", "javascript:return fc_PressKeyDO();");

            TxtAnoManifiesto.Attributes.Add("onkeypress", "javascript:return SoloEnteros(event);");
            TxtNumManifiesto.Attributes.Add("onkeypress", "javascript:return SoloEnteros(event);");

            TxtCliente.Text = GlobalEntity.Instancia.NombreCliente;
            

            String tiempo = (Convert.ToInt32(GlobalEntity.Instancia.TiempoDuracionSesion.ToString()) * 1000 * 60).ToString();
            Session["Reset"] = true;

            if (!Page.IsPostBack)
            {
                TxtAnoManifiesto.Text = Convert.ToString(DateTime.Now.Year);
                ListarDropDowList();

                if (Request.QueryString["sCliente"] != null)
                   TxtCliente.Text = Request.QueryString["sCliente"];
                if (Request.QueryString["sPeriodoManif"] != null)
                    TxtAnoManifiesto.Text = Request.QueryString["sPeriodoManif"];

                TxtNumManifiesto.Text = Request.QueryString["sNroManif"];
                DplSituacion.SelectedValue  = Request.QueryString["sSitu"];
                TxtContenedor.Text = Request.QueryString["sContenedor"];
                TxtDocumento.Text = Request.QueryString["sDoc"];

                if (TxtNumManifiesto.Text != "" || TxtContenedor.Text != "" || TxtDocumento.Text != "")
                    btnConsulta_Click(sender, e);
                else
                {
                    if (GrvListado.Rows.Count == 0)
                    {
                        List<BE_Tracking> lsBE_Tracking = new List<BE_Tracking>();
                        BE_Tracking oBE_Tracking = new BE_Tracking();
                        lsBE_Tracking.Add(oBE_Tracking);
                        GrvListado.DataSource = lsBE_Tracking;
                        GrvListado.DataBind();
                    }
                }
                ScriptManager.RegisterStartupScript(this, GetType(), "mensaje", "SessionExpireAlert(" + tiempo + ");", true);
            }
            else
                ScriptManager.RegisterStartupScript(this, GetType(), "mensaje", "myStopFunction();", true);
        }

        protected void ImgBtnBuscar_Click(object sender, ImageClickEventArgs e)
        {
            dnvListado.InvokeBind();

            if (GrvListado.Rows.Count == 0)
            {
                List<BE_Tracking> lsBE_Tracking = new List<BE_Tracking>();
                BE_Tracking oBE_Tracking = new BE_Tracking();
                lsBE_Tracking.Add(oBE_Tracking);
                GrvListado.DataSource = lsBE_Tracking;
                GrvListado.DataBind();
            }
        }

        protected void GrvListado_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataKey dataKey;
                dataKey = this.GrvListado.DataKeys[e.Row.RowIndex];
                BE_Tracking oitem = (e.Row.DataItem as BE_Tracking);

                if (dataKey.Values["iIdDocOriDet"] != null)
                {
                    CheckBox chkSeleccion = (CheckBox)e.Row.FindControl("chkSeleccionar");
                    chkSeleccion.Attributes.Add("onclick", "HabilitarUno(this);");

                    e.Row.Attributes["onclick"] = String.Format("javascript:return fc_SeleccionaFilaSimple(this);document.getElementById('{0}').value = '{1}';",HdImbContenedor.ClientID, dataKey.Values["iIdDocOriDet"].ToString());
                    e.Row.Attributes["ondblclick"] = String.Format("AbrirPagina('{0}','{1}','{2}');", dataKey.Values["sDocumentoOrigen"].ToString(), dataKey.Values["iIdDocOriDet"].ToString(), dataKey.Values["iIdDocOri"].ToString());

                    if (e.Row.Cells[12].Controls.Count > 0)
                    {
                        ImageButton ibtn = (ImageButton)e.Row.Cells[12].Controls[0];
                        ibtn.OnClientClick = String.Format("AbrirPagina('{0}','{1}','{2}');", dataKey.Values["sDocumentoOrigen"].ToString(), dataKey.Values["iIdDocOriDet"].ToString(), dataKey.Values["iIdDocOri"].ToString());
                    }

                    for (int c = 1; c < e.Row.Cells.Count; c++)
                    {
                        e.Row.Cells[c].Attributes.Add("onclick", string.Format("HabilitarUno(document.getElementById('{0}'));", chkSeleccion.ClientID));
                    }

                    e.Row.Style["cursor"] = "pointer";
                }
                else
                {
                    e.Row.Visible = false;
                }
            }
        }

        protected void btnConsulta_Click(object sender, EventArgs e)
        {
            dnvListado.InvokeBind();

            if (GrvListado.Rows.Count == 0)
            {
                List<BE_Tracking> lsBE_Tracking = new List<BE_Tracking>();
                BE_Tracking oBE_Tracking = new BE_Tracking();
                lsBE_Tracking.Add(oBE_Tracking);
                GrvListado.DataSource = lsBE_Tracking;
                GrvListado.DataBind();
            }
        }

        protected void ImgBtnBuscar_Click(object sender, EventArgs e)
        {
            dnvListado.InvokeBind();

            if (GrvListado.Rows.Count == 0)
            {
                List<BE_Tracking> lsBE_Tracking = new List<BE_Tracking>();
                BE_Tracking oBE_Tracking = new BE_Tracking();
                lsBE_Tracking.Add(oBE_Tracking);
                GrvListado.DataSource = lsBE_Tracking;
                GrvListado.DataBind();
            }
        }

        protected void btn_cerrar_Click(object sender, EventArgs e)
        {
            if (GlobalEntity.Instancia.CerrarExtranet == "S")
            {
                BL_Usuario oBL_Usuario = new BL_Usuario();
                oBL_Usuario.GrabaInicioSesion(GlobalEntity.Instancia.Usuario, "0");
                Session.Abandon();
                FormsAuthentication.SignOut();
                Response.Redirect("../../eFENIX_Login.aspx");
                Response.End();
            }
        }
    #endregion



    #region Metodo Transacciones
        DataNavigatorParams BindGridLista(object sender, EventArgs e)
        {
            BL_Tracking oBL_Tracking = new BL_Tracking();
            BE_Tracking oBE_Tracking = new BE_Tracking();

            oBE_Tracking.iIdCliente = GlobalEntity.Instancia.IdCliente;
            oBE_Tracking.sAnoManifiesto = TxtAnoManifiesto.Text;
            oBE_Tracking.sNumManifiesto = TxtNumManifiesto.Text;
            oBE_Tracking.sDocumentoOrigen = TxtDocumento.Text;
            oBE_Tracking.sContenedor = TxtContenedor.Text;
            oBE_Tracking.sSituacion = DplSituacion.SelectedValue;

            GrvListado.PageSize = 12;
            oBE_Tracking.NPagina = dnvListado.CurrentPage;
            oBE_Tracking.NRegistros = GrvListado.PageSize;

            IList<BE_Tracking> lista = oBL_Tracking.ListarSeguimiento(oBE_Tracking);
            dnvListado.Visible = (oBE_Tracking.NTotalRegistros > oBE_Tracking.NRegistros);
            LblTotal.Text = "Total de Registros: " + Convert.ToString(oBE_Tracking.NTotalRegistros);
            UdpTotales.Update();

            return new DataNavigatorParams(lista, oBE_Tracking.NTotalRegistros);
        }
    #endregion



    #region "Metodo Controles"
        protected void ListarDropDowList()
        {
            try
            {
                DplSituacion.Items.Clear();
                DplSituacion.Items.Add(new ListItem("TODOS", "TODOS"));
                DplSituacion.Items.Add(new ListItem("DECLARADO", "DECLARADO"));
                DplSituacion.Items.Add(new ListItem("RECIBIDO", "RECIBIDO"));
                DplSituacion.Items.Add(new ListItem("RETIRADO", "RETIRADO"));
                DplSituacion.SelectedIndex = 0;
            }
            catch (Exception e)
            {
                new ResultadoInterfase(ResultadoInterfase.TipoResultado.Error, e);
            }
        }
    #endregion
}