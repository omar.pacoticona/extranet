﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using QNET.Web.UI.Controls;
using FENIX.BusinessEntity;
using FENIX.BusinessLogic;
using System.Text;
using FENIX.Common;
using System.Collections.Generic;
using System.Drawing;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.IO;

public partial class Extranet_Operaciones_eFENIX_Factura_Consulta : System.Web.UI.Page
{
    public int IdDocPago = 0;
    public Int32 IdAgente = 0;

    wsExtranet.eProxyFL.ServicioFLClient cliente = new wsExtranet.eProxyFL.ServicioFLClient("eFL");
    wsExtranet.ePaperlessFL.ServicioFLClient clienteXML = new wsExtranet.ePaperlessFL.ServicioFLClient("FL");
    #region "Evento Pagina"
    protected void Page_Load(object sender, EventArgs e)
    {

        ScriptManager scriptManager = ScriptManager.GetCurrent(this.Page);
        dnvListado.BindGridView += new QNET.Web.UI.Controls.DataNavigator.BindGridViewDelegate(BindGridLista);
        //scriptManager.RegisterPostBackControl(this.GrvListado);

        //txtVolante.Attributes.Add("onkeydown", "javascript:return fc_PressKeyDO();");
        txtDocumentoOrigen.Attributes.Add("onkeydown", "javascript:return fc_PressKeyDO();");      
        IdAgente = GlobalEntity.Instancia.IdCliente;

        String tiempo = (Convert.ToInt32(GlobalEntity.Instancia.TiempoDuracionSesion.ToString()) * 1000 * 60).ToString();
        Session["Reset"] = true;

        if (!Page.IsPostBack)
        {
            LimpiarTextBox();
            ListarDropDowList();
          
            //    DplAnioPeriodo.SelectedValue = DateTime.Now.Year.ToString();
            //DplMesPeriodo.SelectedValue = DateTime.Now.Month.ToString();

            ScriptManager.RegisterStartupScript(this, GetType(), "mensaje", "SessionExpireAlert(" + tiempo + ");", true);
        }
        else        
            ScriptManager.RegisterStartupScript(this, GetType(), "mensaje", "myStopFunction();", true);

    }
    protected void ImgBtnBuscar_Click(object sender, EventArgs e)
    {
        dnvListado.InvokeBind();

        if (GrvListado.Rows.Count == 0)
        {
            List<BE_DocumentoPago> oBE_DocumentoPagoList = new List<BE_DocumentoPago>();
            BE_DocumentoPago oBE_DocumentoPago = new BE_DocumentoPago();
            //oBE_DocumentoPagoList.Add(oBE_DocumentoPago);
            // GrvListado.DataSource = oBE_DocumentoPagoList;
            //GrvListado.DataBind();
        }
    }

    void LlenarGrilla()
    {
        BE_DocumentoPago oBE_DocumentoPago = new BE_DocumentoPago();
        BE_DocumentoPagoList oBE_DocumentoPagoList = new BE_DocumentoPagoList();

        oBE_DocumentoPago.idAgenciaAduana = IdAgente;
        oBE_DocumentoPago.sSerie = String.Empty;
        oBE_DocumentoPago.sNumero = String.Empty;
        oBE_DocumentoPago.sNroDocumentoOrigen = txtDocumentoOrigen.Text;
        oBE_DocumentoPago.sNroVolante = String.Empty;
        oBE_DocumentoPago.sNroContenedor = String.Empty;

        oBE_DocumentoPago.sSerie = txtSerie.Text;
        oBE_DocumentoPago.sNumero = txtNumero.Text;
        oBE_DocumentoPago.sNegocio = "102";//dplLineaNegocio.SelectedValue;

        //oBE_DocumentoPago.sFechaIni = DateTime.Parse(txtFechaIni.Text);
        //oBE_DocumentoPago.sFechaIni = Convert.ToDateTime("01/11/2018"); //Fecha desde la cual se pueden descargar las fact. desde fenix
        //oBE_DocumentoPago.sFechaFin = DateTime.Now;

      
    //    oBE_DocumentoPago.iIdTipoDocumentoPago = Convert.ToInt32(dplTipoDocumento.SelectedValue);
        BL_DocumentoPago oBL_DocumentoPago = new BL_DocumentoPago();

        oBE_DocumentoPagoList = oBL_DocumentoPago.ListarDocumentoPago(oBE_DocumentoPago);

        if (oBE_DocumentoPagoList.Count == 0 || oBE_DocumentoPagoList == null)
        {
            oBE_DocumentoPagoList = new BE_DocumentoPagoList();
           // oBE_DocumentoPagoList.Add(new BE_DocumentoPago());
            GrvListado.DataSource = oBE_DocumentoPagoList;
            GrvListado.DataBind();

            Mensaje("No hay registros encontrados");
        }

        ViewState["oBE_DocumentoPagoList"] = oBE_DocumentoPagoList;
        GrvListado.DataSource = oBE_DocumentoPagoList;
        GrvListado.DataBind();

        dnvListado.Visible = true;
        LblTotal.Text = "Total de Registros: " + Convert.ToString(GrvListado.Rows.Count);
        UdpTotales.Update();

        new DataNavigatorParams(oBE_DocumentoPagoList, GrvListado.Rows.Count);
    }


    DataNavigatorParams BindGridLista(object sender, EventArgs e)
    {//
        BE_DocumentoPago oBE_DocumentoPago = new BE_DocumentoPago();
        BE_DocumentoPagoList oBE_DocumentoPagoList = new BE_DocumentoPagoList();

        oBE_DocumentoPago.idAgenciaAduana = IdAgente;
        oBE_DocumentoPago.sSerie = String.Empty;
        oBE_DocumentoPago.sNumero = String.Empty;
        oBE_DocumentoPago.sNroDocumentoOrigen = txtDocumentoOrigen.Text;
        oBE_DocumentoPago.sNroVolante = String.Empty;
        oBE_DocumentoPago.sNroContenedor = String.Empty;

        oBE_DocumentoPago.sSerie = txtSerie.Text;
        oBE_DocumentoPago.sNumero = txtNumero.Text;
        oBE_DocumentoPago.sNegocio = "102";//dplLineaNegocio.SelectedValue;

        //oBE_DocumentoPago.sFechaIni = DateTime.Parse(txtFechaIni.Text);
        //oBE_DocumentoPago.sFechaIni = Convert.ToDateTime("01/11/2018"); //Fecha desde la cual se pueden descargar las fact. desde fenix
        //oBE_DocumentoPago.sFechaFin = DateTime.Now;

        oBE_DocumentoPago.sFechaIni = Request.Form["DatePickername"];
        oBE_DocumentoPago.sFechaFin = Request.Form["DatePickerFinname"];
        var Inicio                  = Request.Form["DatePickername"];
        var Fin                     = Request.Form["DatePickerFinname"];

        if (Inicio == "")
            Inicio                  = Convert.ToString(DateTime.Today);

        if (Fin == "")
            Fin                     = Convert.ToString(DateTime.Today);


        if (Convert.ToDateTime(Inicio) > Convert.ToDateTime(Fin))
        {
            SCA_MsgInformacion("Fecha Fin no puede ser inferior a Fecha Inicio.");
        }

        else
        {

            GrvListado.PageSize = 150;
            oBE_DocumentoPago.NPagina = dnvListado.CurrentPage;
            oBE_DocumentoPago.NRegistros = GrvListado.PageSize;
            //    oBE_DocumentoPago.iIdTipoDocumentoPago = Convert.ToInt32(dplTipoDocumento.SelectedValue);
            BL_DocumentoPago oBL_DocumentoPago = new BL_DocumentoPago();

            oBE_DocumentoPagoList = oBL_DocumentoPago.ListarDocumentoPago(oBE_DocumentoPago);

            if (oBE_DocumentoPagoList.Count == 0 || oBE_DocumentoPagoList == null)
            {
                oBE_DocumentoPagoList          = new BE_DocumentoPagoList();
                // oBE_DocumentoPagoList.Add(new BE_DocumentoPago());
              //  GrvListado.DataSource = oBE_DocumentoPagoList;
                //GrvListado.DataBind();

                Mensaje("No hay registros encontrados");
            }

            ViewState["oBE_DocumentoPagoList"] = oBE_DocumentoPagoList;
            GrvListado.DataSource              = oBE_DocumentoPagoList;
            GrvListado.DataBind();

            dnvListado.Visible                 = true;
            LblTotal.Text                      = "Total de Registros: " + Convert.ToString(GrvListado.Rows.Count);
            UdpTotales.Update();
        }

        

       return new DataNavigatorParams(oBE_DocumentoPagoList, GrvListado.Rows.Count);
    }

    protected void ListarDropDowList()
    {
        try
        {

            //LLENAR LINEA DE NEGOCIO
            List<BE_LineaNegocio> listLineaNegocio = new List<BE_LineaNegocio>();
            BL_DocumentoPago oBL_DocumentoPago = new BL_DocumentoPago();
/*
            List<BE_TipoDocumentoPago> listTipoDocumentoPago = new List<BE_TipoDocumentoPago>();

            listTipoDocumentoPago = oBL_DocumentoPago.ListarTipoDocumentoPago();

            dplTipoDocumento.Items.Clear();
            dplTipoDocumento.DataSource = listTipoDocumentoPago;
            dplTipoDocumento.DataValueField = "iIdTipoDocumentoPago";
            dplTipoDocumento.DataTextField = "sTipoDocumentoPago";
            dplTipoDocumento.DataBind();
*/

            //dplLineaNegocio.Items.Clear();  
            //listLineaNegocio = oBL_DocumentoPago.ListarNegocio();          
            //dplLineaNegocio.DataSource = listLineaNegocio;

            //dplLineaNegocio.DataValueField = "idLinea";
            //dplLineaNegocio.DataTextField = "LineaNegocio";
            //dplLineaNegocio.DataBind();

            //dplLineaNegocio.SelectedValue = "1";
            //dplLineaNegocio.Text = "SELECCIONE";

               


        }
        catch (Exception e)
        {
        }
    }

    protected void btn_cerrar_Click(object sender, EventArgs e)
    {
        if (GlobalEntity.Instancia.CerrarExtranet == "S")
        {
            BL_Usuario oBL_Usuario = new BL_Usuario();
            oBL_Usuario.GrabaInicioSesion(GlobalEntity.Instancia.Usuario, "0");
            Session.Abandon();
            FormsAuthentication.SignOut();
            Response.Redirect("../../eFENIX_Login.aspx");
            Response.End();
        }
    }
    #endregion

    #region Metodos Comunes
    void LimpiarTextBox()
    {

        //lblAnio.Visible = false;
        //DplAnioPeriodo.Visible = false;

        //lblMes.Visible = false;
        //DplMesPeriodo.Visible = false;

        //lblSerie.Visible = false;
        //txtSerie.Visible = false;
        txtSerie.Text = String.Empty;

        //lblNumero.Visible = false;
        //txtNumero.Visible = false;
        txtNumero.Text = String.Empty;

        //lblVolante.Visible = false;
        //txtVolante.Visible = false;
        //txtVolante.Text = String.Empty;

        //lblDocumento.Visible = false;
        //txtDocumentoOrigen.Visible = false;
        txtDocumentoOrigen.Text = String.Empty;
       


       
    }
    #endregion


    protected void GrvListado_RowDataBound(object sender, GridViewRowEventArgs e)
    {

        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            e.Row.Cells[3].Text = DateTime.Parse(e.Row.Cells[3].Text).ToShortDateString();

            decimal dSubTotal = 0;
            decimal.TryParse(e.Row.Cells[7].Text, out dSubTotal);
            e.Row.Cells[7].Text = dSubTotal.ToString("N2"); //SubTotal

            decimal dIGV = 0;
            decimal.TryParse(e.Row.Cells[8].Text, out dIGV);
            e.Row.Cells[8].Text = dIGV.ToString("N2"); //IGV

            decimal dTotal = 0;
            decimal.TryParse(e.Row.Cells[9].Text, out dTotal);
            e.Row.Cells[9].Text = dTotal.ToString("N2"); //Total

            e.Row.Cells[6].ToolTip =
                                          //  "DEP. TEMPORAL" + Environment.NewLine +
                                          //"Importación: Bill Of Lading (BL)" + Environment.NewLine +
                                          //"Exportación: Booking (BK)" + Environment.NewLine +
                                          //"--------------------------------------" + Environment.NewLine +
                                          "DEP. ADUANERO Y SIMPLE" + Environment.NewLine +
                                          "Número de Orden" + Environment.NewLine 
                                          //+
                                          //"--------------------------------------"
                                           ;
        }



    }

    protected void GrvListado_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        Int32 iIdDocPago = 0;       
        Int32 iIdAgencia = 0;

        iIdDocPago = Convert.ToInt32(GrvListado.DataKeys[Convert.ToInt32(e.CommandArgument)].Values["iIdDocPago"].ToString());
        List<Int32> lstComprobantes = new List<Int32>();

        if (e.CommandName == "DescargaFactura")
        {
            //wsExtraneteProxyFL.ServicioFLClient cliente = new wsExtraneteProxyFL.ServicioFLClient("eFL");

            lstComprobantes.Add(iIdDocPago);
            Int32[] Comprobantes = lstComprobantes.ToArray();
            //Descarga de Facturas Aqui
            wsExtranet.ePaperlessFL.RespuestaServicio4 respuesta;

            Credencial();

            respuesta = clienteXML.Transmision_GenerarPDFOficial(Comprobantes);
            string strMensaje = string.Empty;
            wsExtranet.ePaperlessFL.Evento[] Mensajes;
            Mensajes = respuesta.Mensajes;

            wsExtranet.ePaperlessFL.Evento[] LogErrores;
            LogErrores = respuesta.LogErrores;

            //List<eProxyFL.Evento> Mensajes = respuesta.Mensajes;
            //List<eProxyFL.Evento> Errores = respuesta.LogErrores;
            Byte[] bytesZip = respuesta.Bytes;
            String NombreZip = respuesta.DataCadena;

            if (NombreZip != "")
            {
                Response.ClearHeaders();
                Response.Clear();
                Response.ContentType = "application/x-zip-compressed";
                Response.AddHeader("content-disposition", String.Format("attachment;filename={0}", NombreZip));
                Response.BinaryWrite(bytesZip);

                Response.Flush();
                
                HttpContext.Current.ApplicationInstance.CompleteRequest();
                Response.End();

            }
            else
            {
                if (LogErrores.Count() > 0)
                {
                    strMensaje = respuesta.LogErrores[0].Observacion1 + "    " + respuesta.LogErrores[0].Observacion3;
                    SCA_MsgInformacion(strMensaje);
                }
            }

        }

        if (e.CommandName == "DescargaFacturaXML")
        {
            //wsExtraneteProxyFL.ServicioFLClient cliente = new wsExtraneteProxyFL.ServicioFLClient("eFL");

            lstComprobantes.Add(iIdDocPago);
            Int32[] Comprobantes = lstComprobantes.ToArray();
            //Descarga de Facturas Aqui
            wsExtranet.ePaperlessFL.RespuestaServicio4 respuesta;

            Credencial();

            respuesta = clienteXML.Transmision_GenerarXMLOficial(Comprobantes);
            string strMensaje = string.Empty;
            wsExtranet.ePaperlessFL.Evento[] Mensajes;
            Mensajes = respuesta.Mensajes;

            wsExtranet.ePaperlessFL.Evento[] LogErrores;
            LogErrores = respuesta.LogErrores;

            //List<eProxyFL.Evento> Mensajes = respuesta.Mensajes;
            //List<eProxyFL.Evento> Errores = respuesta.LogErrores;
            Byte[] bytesZip = respuesta.Bytes;
            String NombreZip = respuesta.DataCadena;

            if (NombreZip != "")
            {
                Response.ClearHeaders();
                Response.Clear();
                Response.ContentType = "application/x-zip-compressed";
                Response.AddHeader("content-disposition", String.Format("attachment;filename={0}", NombreZip));
                Response.BinaryWrite(bytesZip);

                Response.Flush();

                HttpContext.Current.ApplicationInstance.CompleteRequest();
                Response.End();

            }
            else
            {
                if (LogErrores.Count() > 0)
                {
                    strMensaje = respuesta.LogErrores[0].Observacion1 + "    " + respuesta.LogErrores[0].Observacion3;
                    SCA_MsgInformacion(strMensaje);
                }
            }

        }



    }

    void Mensaje(String SMS)
    {
        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "", "<script>alert('" + SMS + "');</script>", false);
    }
    public void Credencial()
    {
        string usuarioWindows = string.Empty;
        string passwordWindows = string.Empty;

        if (ConfigurationManager.AppSettings.AllKeys.Contains("WSERVICESFL_LOGIN"))
            usuarioWindows = ConfigurationManager.AppSettings.Get("WSERVICESFL_LOGIN").ToString();

        if (ConfigurationManager.AppSettings.AllKeys.Contains("WSERVICESFL_CLAVE"))
            passwordWindows = ConfigurationManager.AppSettings.Get("WSERVICESFL_CLAVE").ToString();


        cliente.ClientCredentials.Windows.ClientCredential.UserName = usuarioWindows;
        cliente.ClientCredentials.Windows.ClientCredential.Password = passwordWindows;

        clienteXML.ClientCredentials.Windows.ClientCredential.UserName = usuarioWindows;
        clienteXML.ClientCredentials.Windows.ClientCredential.Password = passwordWindows;
    }

    private void SCA_MsgInformacion(string strError)
    {
        MensajeScript(strError);
    }

    private void MensajeScript(string SMS)
    {
        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "", "<script>alert('" + SMS + "');</script>", false);
    }
   
}


