﻿<%@ Page Language="C#" 
    MasterPageFile="~/Extranet/Seguridad/eFENIX_Principal.master" 
    AutoEventWireup="true" CodeFile="eFENIX_PreLiquidacion_Consultar.aspx.cs" 
    Inherits="Extranet_Operaciones_eFENIX_PreLiquidacion_Consultar" %>


 <asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
      

      <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"/>
     <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
     <%-- <script src="https://code.jquery.com/jquery-1.12.4.js" type="text/javascript"></script>--%>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js" type="text/javascript"></script>
    <script src="../../Script/Java_Script/jsAcuerdoComision.js"></script>
    



    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

    </asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="CntMaster" Runat="Server">

     <table cellspacing="0" cellpadding="0" style="width: 100%;">
        <tr>
            <td class="form_titulo" style="width: 85%">
                <asp:TextBox ID="LblTitulo" Text="Listado de PreLiquidaciones" ReadOnly="true" 
                    style="background-color: #0069ae; border: #0069ae; color: #FFFFFF; font-family: Calibri; font-size: 13px; font-weight: bold; text-transform: uppercase;" 
                    runat="server" Width="231px" ></asp:TextBox>
            </td>           
        </tr>
        </table>
        
       <table class="form_bandeja" style=" width: 100%; margin-top:5px;">
        <tr>
            <td  style="width: 5%;">
              &nbsp;&nbsp; Proveedor   :
            </td> 

            <td style="width: 60%;">
              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;    <asp:Label ID="lblProveedor" runat="server" Text="Proveedor"
                                 Visible="true" CssClass="lblComisionista"></asp:Label>
            </td> 
        </tr>
    </table>

     <table cellspacing="0" cellpadding="0" style="width: 100%;">
        <%--<tr>
            <td class="form_titulo" style="width: 85%">
                <asp:TextBox ID="LblTitulo" Text="Lista de Liquidaciones" ReadOnly="true" 
                    style="background-color: #0069ae; border: #0069ae; color: #FFFFFF; font-family: Calibri; font-size: 13px; font-weight: bold; text-transform: uppercase;" 
                    runat="server" Width="231px" ></asp:TextBox>
            </td>          
        </tr>--%>
        
        <tr style="height: 55px;">
            <td valign="top" colspan="2">
                <table class="form_bandeja" style=" width: 100%;">                
                   
                    <tr>
                        <td style="width: 10%;">
                              &nbsp;&nbsp; Liquidacion
                        </td>
                        <td style="width: 15%;">
                            <asp:TextBox ID="txtLiquidacion" runat="server" MaxLength="7" Style="text-transform: uppercase"
                                Width="135px"></asp:TextBox></td>
                        <td style="width: 10%;">
                           Documento
                        </td>
                        <td style="width: 15%;">
                            <asp:TextBox ID="TxtBL" runat="server" MaxLength="25" Style="text-transform: uppercase" placeholder="BL / Booking"
                                Width="135px"></asp:TextBox>
                        </td>
                        <td style="width: 10%;">
                            Volante
                        </td>
                        <td style="width: 25%;">
                            <asp:TextBox ID="TxtVolante" runat="server" MaxLength="15" Style="text-transform: uppercase"
                                Width="135px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;&nbsp;&nbsp;Fecha Inicio
                        </td>
                        <td>
                              <input id="txtIni_datep" type="text" readonly size="20px" maxlength="10" name="DatePickername" class="inputText_extranet"  />
                        </td>
                        <td>
                            Fecha Fin
                        </td>
                        <td>
                             <input id="txtFin_datep" type="text"  readonly size="350px"  maxlength="10" name="DatePickerFinname" class="inputText_extranet" />
                        </td>
                        <td>
                           <div style="margin-left:1%;height:45px;">
                              <asp:Button ID="ImgBtnBuscar"  runat="server" Text="Consultar" OnClick="ImgBtnBuscar_Click" class="btn btn-primary" BackColor="#0069ae"/>
                                </div>
                        </td>
                        <td>
                           
                        </td>
                    </tr>
                    </table>
            </td>
        </tr>
      
        <tr valign="top" style="height: 70%">
            <td colspan="5">
                <ajax:UpdatePanel ID="upDocumentoOrigen" runat="server" UpdateMode="Conditional" >
                    <ContentTemplate>                         
                        <div style="overflow: auto; width: 100%; height: 350px;">
                            <asp:GridView ID="GrvListado" runat="server" AutoGenerateColumns="False"
                                SkinID="GrillaConsulta" Width="100%" DataKeyNames="iIdLiq" OnRowCommand="GrvListado_RowCommand" OnRowDataBound="GrvListado_RowDataBound" >
                                <Columns>
                                     <asp:TemplateField>  
                                           <ItemTemplate>
                                                      <asp:CheckBox ID="chkSeleccionar" runat="server" Visible="false" />
                                           </ItemTemplate>
                                        <HeaderStyle BackColor="#0069ae" Width="20px" />
                                        <ItemStyle Width="20px"></ItemStyle>
                                    </asp:TemplateField>


                                  <asp:BoundField DataField="iIdLiq" HeaderText="NroLiquidacion" ItemStyle-HorizontalAlign="Center" >
                                    </asp:BoundField>
                                    <asp:BoundField DataField="sFechaCalculo" HeaderText="Fecha" ItemStyle-HorizontalAlign="Center" >
                                    </asp:BoundField>
                                    <asp:BoundField DataField="sClienteFacturar" HeaderText="Cliente" ItemStyle-HorizontalAlign="Center"  >
                                    </asp:BoundField>
                                    <asp:BoundField DataField="sNumeroDo" HeaderText="Documento" ItemStyle-HorizontalAlign="Center" >
                                    </asp:BoundField>
                                      <asp:BoundField DataField="sOperacion" HeaderText="Operacion" ItemStyle-HorizontalAlign="Center" >
                                    </asp:BoundField>
                                    <asp:BoundField DataField="sNroVolante" HeaderText="Volante" ItemStyle-HorizontalAlign="Center" >
                                    </asp:BoundField>
                                      <asp:BoundField DataField="sTipoMoneda" HeaderText="Moneda" ItemStyle-HorizontalAlign="Center" >
                                    </asp:BoundField>
                                    <asp:BoundField DataField="dAfecto" HeaderText="Afecto" ItemStyle-HorizontalAlign="Center" >
                                    </asp:BoundField>                                   
                                    <asp:BoundField DataField="dIgv" HeaderText="IGV" ItemStyle-HorizontalAlign="Center" >
                                    </asp:BoundField>
                                  
                                    <asp:BoundField DataField="dMontoBruto" HeaderText="Total" ItemStyle-HorizontalAlign="Center" >
                                    </asp:BoundField>
                                    <%--<asp:BoundField DataField="dIgv" HeaderText="NroLiquidacion" ItemStyle-HorizontalAlign="Center" >
                                    </asp:BoundField>--%>

                                   
                                    <asp:ButtonField ButtonType="Image" CommandName="OpenPreLiquidacion" ItemStyle-HorizontalAlign="left" HeaderText="Liquidacion" HeaderStyle-Width="10px" ImageUrl="~/Script/Imagenes/IconButton/filePDF.png"
                                            Text="Descargar Liquidacion">
                                    
                                    </asp:ButtonField> 
                                </Columns>
                            </asp:GridView>
                        </div>
                    </ContentTemplate>
                    <Triggers>      
                      
                        <ajax:AsyncPostBackTrigger ControlID="ImgBtnBuscar" EventName="Click" />

                     
                  
                        <ajax:AsyncPostBackTrigger ControlID="dnvListado" EventName="PageChanged" />
                        <ajax:AsyncPostBackTrigger ControlID="dnvListado" EventName="NextPage" />
                        <ajax:AsyncPostBackTrigger ControlID="dnvListado" EventName="PreviousPage" />
                        <ajax:AsyncPostBackTrigger ControlID="dnvListado" EventName="FirstPage" />  
                        <ajax:AsyncPostBackTrigger ControlID="dnvListado" EventName="LastPage" />  

<asp:AsyncPostBackTrigger ControlID="ImgBtnBuscar" EventName="Click"></asp:AsyncPostBackTrigger>
<asp:AsyncPostBackTrigger ControlID="dnvListado" EventName="PageChanged"></asp:AsyncPostBackTrigger>
<asp:AsyncPostBackTrigger ControlID="dnvListado" EventName="NextPage"></asp:AsyncPostBackTrigger>
<asp:AsyncPostBackTrigger ControlID="dnvListado" EventName="PreviousPage"></asp:AsyncPostBackTrigger>
<asp:AsyncPostBackTrigger ControlID="dnvListado" EventName="FirstPage"></asp:AsyncPostBackTrigger>
<asp:AsyncPostBackTrigger ControlID="dnvListado" EventName="LastPage"></asp:AsyncPostBackTrigger>

<asp:AsyncPostBackTrigger ControlID="ImgBtnBuscar" EventName="Click"></asp:AsyncPostBackTrigger>
<asp:AsyncPostBackTrigger ControlID="dnvListado" EventName="PageChanged"></asp:AsyncPostBackTrigger>
<asp:AsyncPostBackTrigger ControlID="dnvListado" EventName="NextPage"></asp:AsyncPostBackTrigger>
<asp:AsyncPostBackTrigger ControlID="dnvListado" EventName="PreviousPage"></asp:AsyncPostBackTrigger>
<asp:AsyncPostBackTrigger ControlID="dnvListado" EventName="FirstPage"></asp:AsyncPostBackTrigger>
<asp:AsyncPostBackTrigger ControlID="dnvListado" EventName="LastPage"></asp:AsyncPostBackTrigger>

<asp:AsyncPostBackTrigger ControlID="ImgBtnBuscar" EventName="Click"></asp:AsyncPostBackTrigger>
<asp:AsyncPostBackTrigger ControlID="dnvListado" EventName="PageChanged"></asp:AsyncPostBackTrigger>
<asp:AsyncPostBackTrigger ControlID="dnvListado" EventName="NextPage"></asp:AsyncPostBackTrigger>
<asp:AsyncPostBackTrigger ControlID="dnvListado" EventName="PreviousPage"></asp:AsyncPostBackTrigger>
<asp:AsyncPostBackTrigger ControlID="dnvListado" EventName="FirstPage"></asp:AsyncPostBackTrigger>
<asp:AsyncPostBackTrigger ControlID="dnvListado" EventName="LastPage"></asp:AsyncPostBackTrigger>

<asp:AsyncPostBackTrigger ControlID="ImgBtnBuscar" EventName="Click"></asp:AsyncPostBackTrigger>
<asp:AsyncPostBackTrigger ControlID="dnvListado" EventName="PageChanged"></asp:AsyncPostBackTrigger>
<asp:AsyncPostBackTrigger ControlID="dnvListado" EventName="NextPage"></asp:AsyncPostBackTrigger>
<asp:AsyncPostBackTrigger ControlID="dnvListado" EventName="PreviousPage"></asp:AsyncPostBackTrigger>
<asp:AsyncPostBackTrigger ControlID="dnvListado" EventName="FirstPage"></asp:AsyncPostBackTrigger>
<asp:AsyncPostBackTrigger ControlID="dnvListado" EventName="LastPage"></asp:AsyncPostBackTrigger>

                    </Triggers>
                </ajax:UpdatePanel>
            </td>
         </tr>

          <tr valign="top" style="height: 6%;">
            <td colspan="5">
               <ajax:UpdatePanel ID="upDnvListado" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <FENIX:DataNavigator ID="dnvListado" runat="server" AllowCustomPaging="True" ButtonText="Ir" 
                            ForeColor="White" Font-Size="11px" BackColor="#0069ae" 
                            CurrentPage="1" CustomClientFunction="" EnableAskingAfterPage="False" EnableCustomScript="False"
                            GridViewId="GrvListado" ImagesPath="~/Imagenes/DN/" IsParentShowWindow="False"
                            MessageAskingAfterPage="" PageIndicatorFormat="Página {0} / {1}" Visible="False"
                            Width="100%" WidthButtonIr="" WidthTextBoxNumPagina="25px" />
                    </ContentTemplate>
                    <Triggers>
                        <ajax:AsyncPostBackTrigger ControlID="dnvListado" EventName="PageChanged" />
                        <ajax:AsyncPostBackTrigger ControlID="ImgBtnBuscar" EventName="Click" />
                    </Triggers>
                </ajax:UpdatePanel>
            </td>
        </tr>
        
        
    </table>
    <script language="javascript" type="text/javascript">

         var tiempo = 0;
        var seconds = 0;

        var myVar = setInterval(function () {
            clearInterval(myVar);
        }, 1000);

        var myTiempo = setTimeout(function () {
            clearInterval(myTiempo);
        }, 1);

        function myStopFunction() {
            clearInterval(myVar);
            seconds = '<%=GlobalEntity.Instancia.TiempoDuracionSesion%>' * 60;
            tiempo = '<%=GlobalEntity.Instancia.TiempoDuracionSesion%>' * 1000 * 60;

            CuentaTiempo(tiempo);
        }

        function CuentaTiempo(tiempo) {
            clearTimeout(myTiempo);
            myTiempo = setTimeout(function () {
                document.getElementById('<%=btn_cerrar.ClientID%>').click();
                //window.location = "../Seguridad/eFENIX_Login.aspx";
            }, tiempo);
        }

         function SessionExpireAlert(timeout) {
            seconds = timeout / 1000;
            document.getElementsByName("seconds").innerHTML = seconds;

            myvar = setInterval(function () {
                seconds--;
                document.getElementById("seconds").innerHTML = seconds;
            }, 1000);

            CuentaTiempo(timeout);
        };

        function SelectGroupChecks(ParentID) {
            j = 0;
            k = 1;
            idCheck = ParentID.replace('All', '');
            objCheck = null;
            frm = document.forms[0];

            for (i = 0; i < frm.elements.length; i++) {
                obj = frm.elements[i];

                if ((obj.type == 'checkbox')) {
                    j = j + 1;
                    if (obj.disabled != 1) {
                        obj.checked = document.getElementById(ParentID).checked;
                    } else {
                    }
                }
            }
        }

           function fc_Aprobar()
        {           
            return confirm('¿Seguro de generar la liquidación?');
        }


    </script>



    </asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentCab" Runat="Server">
        
        <span class="texto_titulo" >Tu Sesion Expirara en &nbsp; 
            <span class="texto_titulo" id="seconds"> </span>
            <span>&nbsp;seg.</span>
         </span>
         <asp:Button ID="btn_cerrar" runat="server" Text="cerrar" 
            onclick="btn_cerrar_Click"  Style="display: none;" />
    </asp:Content>
