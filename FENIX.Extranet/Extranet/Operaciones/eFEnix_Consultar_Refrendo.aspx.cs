﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using FENIX.BusinessEntity;
using FENIX.BusinessLogic;
using QNET.Web.UI.Controls;
using System.Configuration;
//using System.Net.Http;

public partial class Extranet_Operaciones_eFEnix_Consultar_Refrendo : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        dnvListado.BindGridView += new QNET.Web.UI.Controls.DataNavigator.BindGridViewDelegate(BindGridLista);

        ScriptManager scriptManager = ScriptManager.GetCurrent(this.Page);
        scriptManager.RegisterPostBackControl(this.GrvListado);
        String tiempo = (Convert.ToInt32(GlobalEntity.Instancia.TiempoDuracionSesion.ToString()) * 1000 * 60).ToString();
        Session["Reset"] = true;

        if (!Page.IsPostBack)
        {
            ListarEstadosRefrendo();
            ScriptManager.RegisterStartupScript(this, GetType(), "mensaje", "SessionExpireAlert(" + tiempo + ");", true);
        }
        else
            ScriptManager.RegisterStartupScript(this, GetType(), "mensaje", "SessionExpireAlert(" + tiempo + ");", true);
    }


    protected void btn_cerrar_Click(object sender, EventArgs e)
    {
        if (GlobalEntity.Instancia.CerrarExtranet == "S")
        {
            BL_Usuario oBL_Usuario = new BL_Usuario();
            oBL_Usuario.GrabaInicioSesion(GlobalEntity.Instancia.Usuario, "0");
            Session.Abandon();
            FormsAuthentication.SignOut();
            Response.Redirect("../../eFENIX_Login.aspx");
            Response.End();
        }
    }
    private void SCA_MsgInformacion(string strError)
    {
        MensajeScript(strError);
    }

    private void MensajeScript(string SMS)
    {
        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "", "<script>alert('" + SMS + "');</script>", false);
    }

    protected void ImgBtnBuscar_Click(object sender, EventArgs e)
    {
        dnvListado.InvokeBind();

        if (GrvListado.Rows.Count == 0)
        {
            List<BE_AcuerdoComision> oLista_Acuerdo = new List<BE_AcuerdoComision>();
            BE_AcuerdoComision oBE_AcuerdoComision = new BE_AcuerdoComision();
            // oLista_Acuerdo.Add(oBE_AcuerdoComision);
            //GrvListado.DataSource = oLista_Acuerdo;
            // /GrvListado.DataBind();
        }
        ScriptManager.RegisterStartupScript(this, GetType(), "mensaje2", "HabilitaIsIsPostack();", true);
    }

    DataNavigatorParams BindGridLista(object sender, EventArgs e)
    {//
        BE_Refrendo oBE_Refrendo;
        BL_Refrendo oBL_Refrendo;
        List<BE_Refrendo> oLista_Refrendo = new List<BE_Refrendo>();
        Int32 IdCliente = GlobalEntity.Instancia.IdCliente;

        oBL_Refrendo = new BL_Refrendo();
        oBE_Refrendo = new BE_Refrendo();
        oBE_Refrendo.sBooking = txtBooking.Text;
        oBE_Refrendo.sSolicitudRefr = txtSolicitud.Text;
        oBE_Refrendo.sMesIni = Request.Form["DatePickername"];
        oBE_Refrendo.sMesFin = Request.Form["DatePickerFinname"];
        oBE_Refrendo.sSituacion = DplEstado.SelectedValue;
        oBE_Refrendo.IdCliente = IdCliente;

        var Inicio = Request.Form["DatePickername"];
        var Fin = Request.Form["DatePickerFinname"];

        if (Inicio == "")
            Inicio = Convert.ToString(DateTime.Today);

        if (Fin == "")
            Fin = Convert.ToString(DateTime.Today);


        if (Convert.ToDateTime(Inicio) > Convert.ToDateTime(Fin))
        {
            SCA_MsgInformacion("Fecha Fin no puede ser inferior a Fecha Inicio.");
        }
        else
        {
            GrvListado.PageSize = 50;
            oBE_Refrendo.NPagina = dnvListado.CurrentPage;
            oBE_Refrendo.NRegistros = GrvListado.PageSize;
            oLista_Refrendo = oBL_Refrendo.ListarSolicitudRefrendo(oBE_Refrendo);

            if (oLista_Refrendo.Count == 0 || oLista_Refrendo == null)
            {
                SCA_MsgInformacion("No hay registros encontrados");
            }

            ViewState["oBE_AcuerdoComisionList"] = oLista_Refrendo;

            //GrvListado.Width                             = 1055;       
            dnvListado.Visible = (oLista_Refrendo.Count() > 0);
            //upDocumentoOrigen.Update();
        }

        return new DataNavigatorParams(oLista_Refrendo, oBE_Refrendo.NtotalRegistros);
    }

    protected void GrvListado_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "OpenDetalle")
        {
            Int32 IdSolicitud;
            IdSolicitud = Convert.ToInt32(GrvListado.DataKeys[Convert.ToInt32(e.CommandArgument)].Values["sSolicitudRefr"].ToString());
            BE_Refrendo oBE_Refrendo = new BE_Refrendo();
            BL_Refrendo oBL_Refrendo = new BL_Refrendo();
            List<BE_Refrendo> oLista_Refrendo = new List<BE_Refrendo>();
            oBE_Refrendo.iIdSolicitudRefr = IdSolicitud;
            oLista_Refrendo = oBL_Refrendo.ListarDetalleSolicitudRefrendo(oBE_Refrendo);

            TxtDua.Text = oLista_Refrendo[0].sNroDua;
            txtHdBoking.Text = oLista_Refrendo[0].sBooking;
            txtCutCSeca.Text = oLista_Refrendo[0].dtCargaSeca;
            txtCutCRefrigerada.Text = oLista_Refrendo[0].dtCargaRefrigerada;
            txtObservacionesCliente.Text = oLista_Refrendo[0].sComentarios;
            txtComentariosRevision.Text = oLista_Refrendo[0].sComentariosUsuario;
            txtRucFacturar.Text = oLista_Refrendo[0].sRuc;
            txtClienteFacturar.Text = oLista_Refrendo[0].sClienteFacturar;
            txtDespachador.Text= oLista_Refrendo[0].sNombreDesp;
            txtDNIDesp.Text = oLista_Refrendo[0].sDniDesp;

            lblNombreArchivoDua.Text = "";
            lblEstadoDua.Text = "";
            lblNombreArchivoBook.Text = "";
            lblEstadoBook.Text = "";
            lblNombreArchivoTicket.Text = "";
            lblEstadoTicket.Text = "";
            lblNombreArchivoPackList.Text = "";
            lblEstadoPasList.Text = "";
            lblNombreArchivoGuia.Text = "";
            lblEstadoGuia.Text = "";
            lblNombreArchivoDocPago.Text = "";
            lblEstadoDocPago.Text = "";
            btnVerDAM.Visible = false;
            btnVerBook.Visible = false;
            btnverTicetk.Visible = false;
            btnverPackList.Visible = false;
            btnVerGuia.Visible = false;
            btnVerDocPago.Visible = false;

            for (int i = 0; i < oLista_Refrendo.Count; i++)
            {
                if (oLista_Refrendo[i].iIdTipoDocumento == 1)
                {
                    lblNombreArchivoDua.Text = oLista_Refrendo[i].sRutaArchivo;
                    lblEstadoDua.Text = oLista_Refrendo[i].sNombreSituacion;
                    lblIdDAM.Text = oLista_Refrendo[i].iIdSolicitudRefrDet.ToString();
                    btnVerDAM.Visible = true;
                    if (oLista_Refrendo[i].sSituacion == "3")
                        lblEstadoDua.ForeColor = Color.Red;
                    if (oLista_Refrendo[i].sSituacion == "1")
                        lblEstadoDua.ForeColor = Color.Black;
                    if (oLista_Refrendo[i].sSituacion == "4")
                        lblEstadoDua.ForeColor = Color.Black;
                    if (oLista_Refrendo[i].sSituacion == "2")
                        lblEstadoDua.ForeColor = Color.Green;


                    //colorsLabel(lblBlEstadoArchivoAdj, oBE_AutorizacionRetiroList[i].idSituacion);
                }
                if (oLista_Refrendo[i].iIdTipoDocumento == 2)
                {
                    lblNombreArchivoBook.Text = oLista_Refrendo[i].sRutaArchivo;
                    lblEstadoBook.Text = oLista_Refrendo[i].sNombreSituacion;
                    lblIdBokoking.Text = oLista_Refrendo[i].iIdSolicitudRefrDet.ToString();
                    btnVerBook.Visible = true;
                    if (oLista_Refrendo[i].sSituacion == "3")
                        lblEstadoBook.ForeColor = Color.Red;
                    if (oLista_Refrendo[i].sSituacion == "1")
                        lblEstadoBook.ForeColor = Color.Black;
                    if (oLista_Refrendo[i].sSituacion == "4")
                        lblEstadoBook.ForeColor = Color.Black;
                    if (oLista_Refrendo[i].sSituacion == "2")
                        lblEstadoBook.ForeColor = Color.Green;
                    //colorsLabel(lblEndoseEstadoArchivoAdj, oBE_AutorizacionRetiroList[i].idSituacion);
                }
                if (oLista_Refrendo[i].iIdTipoDocumento == 3)
                {
                    lblNombreArchivoTicket.Text = oLista_Refrendo[i].sRutaArchivo;
                    lblEstadoTicket.Text = oLista_Refrendo[i].sNombreSituacion;
                    LblIdTicket.Text = oLista_Refrendo[i].iIdSolicitudRefrDet.ToString();
                    btnverTicetk.Visible = true;
                    if (oLista_Refrendo[i].sSituacion == "3")
                        lblEstadoTicket.ForeColor = Color.Red;
                    if (oLista_Refrendo[i].sSituacion == "1")
                        lblEstadoTicket.ForeColor = Color.Black;
                    if (oLista_Refrendo[i].sSituacion == "4")
                        lblEstadoTicket.ForeColor = Color.Black;
                    if (oLista_Refrendo[i].sSituacion == "2")
                        lblEstadoTicket.ForeColor = Color.Green;
                    // colorsLabel(lblSenasaEstadoArchivoAdj, oBE_AutorizacionRetiroList[i].idSituacion);
                }

                if (oLista_Refrendo[i].iIdTipoDocumento == 4)
                {
                    lblNombreArchivoPackList.Text = oLista_Refrendo[i].sRutaArchivo;
                    lblEstadoPasList.Text = oLista_Refrendo[i].sNombreSituacion;
                    btnverPackList.Visible = true;
                    lblIdPackList.Text = oLista_Refrendo[i].iIdSolicitudRefrDet.ToString();
                    if (oLista_Refrendo[i].sSituacion == "3")
                        lblEstadoPasList.ForeColor = Color.Red;
                    if (oLista_Refrendo[i].sSituacion == "1")
                        lblEstadoPasList.ForeColor = Color.Black;
                    if (oLista_Refrendo[i].sSituacion == "4")
                        lblEstadoPasList.ForeColor = Color.Black;
                    if (oLista_Refrendo[i].sSituacion == "2")
                        lblEstadoPasList.ForeColor = Color.Green;

                }

                if (oLista_Refrendo[i].iIdTipoDocumento == 5)
                {
                    lblNombreArchivoGuia.Text = oLista_Refrendo[i].sRutaArchivo;
                    lblEstadoGuia.Text = oLista_Refrendo[i].sNombreSituacion;
                    btnVerGuia.Visible = true;
                    lblIdGuiaREM.Text = oLista_Refrendo[i].iIdSolicitudRefrDet.ToString();
                    if (oLista_Refrendo[i].sSituacion == "3")
                        lblEstadoGuia.ForeColor = Color.Red;
                    if (oLista_Refrendo[i].sSituacion == "1")
                        lblEstadoGuia.ForeColor = Color.Black;
                    if (oLista_Refrendo[i].sSituacion == "4")
                        lblEstadoGuia.ForeColor = Color.Black;
                    if (oLista_Refrendo[i].sSituacion == "2")
                        lblEstadoGuia.ForeColor = Color.Green;
                }

                if (oLista_Refrendo[i].iIdTipoDocumento == 6) //DOCUMENTO PAGO
                {
                    lblNombreArchivoDocPago.Text = oLista_Refrendo[i].sRutaArchivo;
                    lblEstadoDocPago.Text = oLista_Refrendo[i].sNombreSituacion;
                    btnVerDocPago.Visible = true;
                    ldlIdDocPago.Text = oLista_Refrendo[i].iIdSolicitudRefrDet.ToString();
                    if (oLista_Refrendo[i].sSituacion == "3")
                        lblEstadoDocPago.ForeColor = Color.Red;
                    if (oLista_Refrendo[i].sSituacion == "1")
                        lblEstadoDocPago.ForeColor = Color.Black;
                    if (oLista_Refrendo[i].sSituacion == "4")
                        lblEstadoDocPago.ForeColor = Color.Black;
                    if (oLista_Refrendo[i].sSituacion == "2")
                        lblEstadoDocPago.ForeColor = Color.Green;
                }


            }

            oBL_Refrendo = new BL_Refrendo();
            oBE_Refrendo = new BE_Refrendo();
            oLista_Refrendo = new List<BE_Refrendo>();

            oBE_Refrendo.iIdSolicitudRefr = Convert.ToInt32(GrvListado.DataKeys[Convert.ToInt32(e.CommandArgument)].Values["sSolicitudRefr"].ToString());
            oLista_Refrendo = oBL_Refrendo.ListarDetalleBultosPrecintosIngresadosCliente(oBE_Refrendo);
            ScriptManager.RegisterStartupScript(this, GetType(), "mensaje2", "HabilitaPostPostack();", true);
            if (oLista_Refrendo.Count > 0)
            {
                GrvListadoDetalle.DataSource = oLista_Refrendo;
                GrvListadoDetalle.DataBind();
                ScriptManager.RegisterStartupScript(this, GetType(), "mensaje2", "MostrarDiv();", true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "mensaje3", "OcultarDivDet();", true);
            }

        }
    }

    void ListarEstadosRefrendo()
    {
        BE_Refrendo oBE_Refrendo = new BE_Refrendo();
        BL_Refrendo oBL_Refrendo = new BL_Refrendo();
        List<BE_Refrendo> ListEstados = new List<BE_Refrendo>();
        ListEstados = oBL_Refrendo.ListarEstadosRefrendo(oBE_Refrendo);

        DplEstado.DataSource = ListEstados;
        DplEstado.DataValueField = "iIdValor";
        DplEstado.DataTextField = "sValor";
        DplEstado.DataBind();
        DplEstado.Items.Add(new ListItem("[Todos]", "-1"));
        DplEstado.SelectedIndex = DplEstado.Items.IndexOf(DplEstado.Items.FindByValue("-1"));

    }

    protected void GrvListado_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            DataKey dataKey;
            dataKey = this.GrvListado.DataKeys[e.Row.RowIndex];
            BE_Refrendo oitem = (e.Row.DataItem as BE_Refrendo);
            ImageButton imgAsignarse = (ImageButton)e.Row.FindControl("imgAsignarse");


            CheckBox chkSeleccion = (CheckBox)e.Row.FindControl("ChkSeleccionar");
            e.Row.Style["cursor"] = "pointer";


            if (dataKey.Values["sCanal"].ToString() == "NARANJA")
            {
                e.Row.Cells[10].ForeColor = Color.Orange;
            }
            if (dataKey.Values["sCanal"].ToString() == "ROJO")
            {
                e.Row.Cells[10].ForeColor = Color.Red;
            }
            if (dataKey.Values["sSituacion"].ToString() == "3") //Rechazado
            {
                e.Row.Cells[9].ForeColor = Color.Red;
                // e.Row.Cells[8].ForeColor = Color.White;
            }

            if (dataKey.Values["sSituacion"].ToString() == "2") //Aprobado
            {
                e.Row.Cells[9].ForeColor = Color.Green;
                // e.Row.Cells[8].ForeColor = Color.White;
            }

            if (dataKey.Values["sSituacion"].ToString() == "1") //Enviado
            {
                e.Row.Cells[9].ForeColor = Color.Gray;
            }
            if (dataKey.Values["sSituacion"].ToString() == "4") //En Proceso
            {
                e.Row.Cells[9].ForeColor = Color.Black;
            }


            //ImageButton imgAsignarse = e.Row.FindControl("imgAsignarse") as ImageButton;


            //e.Row.Cells[12].Visible = false;


            //if (oitem.idSituacion == 1)
            //{
            //    e.Row.Cells[9].BackColor = Color.Yellow;
            //    e.Row.Cells[9].ForeColor = Color.DarkBlue;
            //}
            //else if (oitem.idSituacion == 2)
            //{
            //    e.Row.Cells[9].BackColor = Color.Green;
            //    e.Row.Cells[9].ForeColor = Color.White;
            //}
            //else if (oitem.idSituacion == 3)
            //{
            //    e.Row.Cells[9].BackColor = Color.Red;
            //    e.Row.Cells[9].ForeColor = Color.White;
            //}
            //else if (oitem.idSituacion == 4)
            //{
            //    e.Row.Cells[9].BackColor = Color.Olive;
            //    e.Row.Cells[9].ForeColor = Color.White;
            //    e.Row.Cells[12].Visible = true;
            //}

            //if (permiso.Equals("CJ"))
            //{
            //    e.Row.Cells[12].Visible = false;
            //}
        }
    }

    protected void btnVerDAM_Click(object sender, EventArgs e)
    {
        //try
        //{
        String IdAgente = GlobalEntity.Instancia.IdCliente.ToString();
        if (lblNombreArchivoDua.Text.Length > 1)
        {
            string ruta = ConfigurationManager.AppSettings["Refrendo_Solicitud"] + IdAgente + "/";
            Descargar(Response, ruta, lblNombreArchivoDua.Text);
            Page_Load(null, null);

            //Response.ContentType = "application/octet-stream";
            //Response.ContentType = "application/pdf";
            //Response.AddHeader("Content-Disposition", "attachment; filename=" + lblNombreArchivoDua.Text);
            //Response.Clear();
            //Response.WriteFile(ruta + lblNombreArchivoDua.Text);
            //Response.End();
        }

        //}
        //catch (Exception ex) {

        //}
    }

    public static void Descargar(HttpResponse sResponse, String sRuta, String sNombreArchivo)
    {
        try
        {
            //   sResponse.ContentType = "application/octet-stream";
            sResponse.ContentType = "application/pdf";
            sResponse.AddHeader("Content-Disposition", "attachment; filename=" + sNombreArchivo);
            sResponse.Clear();
            sResponse.WriteFile(sRuta + sNombreArchivo);
            sResponse.End();
        }
        catch (Exception ex)
        {

        }
    }

    //private void Descargar(HttpResponse response, string ruta, string text)
    //{
    //   // throw new NotImplementedException();
    //}

    protected void btnVerBook_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            String IdAgente = GlobalEntity.Instancia.IdCliente.ToString();
            if (lblNombreArchivoBook.Text.Length > 1)
            {
                string ruta = ConfigurationManager.AppSettings["Refrendo_Solicitud"] + IdAgente + "/";
                Descargar(Response, ruta, lblNombreArchivoBook.Text);
                // Page_Load(null, null);
            }

        }
        catch (Exception ex)
        {

        }
    }

    protected void btnverTicetk_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            String IdAgente = GlobalEntity.Instancia.IdCliente.ToString();
            if (lblNombreArchivoTicket.Text.Length > 1)
            {
                string ruta = ConfigurationManager.AppSettings["Refrendo_Solicitud"] + IdAgente + "/";
                Descargar(Response, ruta, lblNombreArchivoTicket.Text);
                // Page_Load(null, null);
            }

        }
        catch (Exception ex)
        {

        }
    }

    protected void btnverPackList_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            String IdAgente = GlobalEntity.Instancia.IdCliente.ToString();
            if (lblNombreArchivoPackList.Text.Length > 1)
            {
                string ruta = ConfigurationManager.AppSettings["Refrendo_Solicitud"] + IdAgente + "/";
                Descargar(Response, ruta, lblNombreArchivoPackList.Text);
                // Page_Load(null, null);
            }

        }
        catch (Exception ex)
        {

        }
    }

    protected void btnVerGuia_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            String IdAgente = GlobalEntity.Instancia.IdCliente.ToString();
            if (lblNombreArchivoGuia.Text.Length > 1)
            {
                string ruta = ConfigurationManager.AppSettings["Refrendo_Solicitud"] + IdAgente + "/";
                Descargar(Response, ruta, lblNombreArchivoGuia.Text);
                // Page_Load(null, null);
            }

        }
        catch (Exception ex)
        {

        }
    }

    protected void btnVerDocPago_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            String IdAgente = GlobalEntity.Instancia.IdCliente.ToString();
            if (lblNombreArchivoDocPago.Text.Length > 1)
            {
                string ruta = ConfigurationManager.AppSettings["Refrendo_Solicitud"] + IdAgente + "/";
                Descargar(Response, ruta, lblNombreArchivoDocPago.Text);
                // Page_Load(null, null);
            }

        }
        catch (Exception ex)
        {

        }
    }
}