<%@ Page Language="C#" MasterPageFile="~/Extranet/Seguridad/eFENIX_Principal.master"
    AutoEventWireup="true" CodeFile="FENIX_Preliquidacion.aspx.cs" Inherits="Fargonet_Facturacion_FENIX_Preliquidacion"
    Title="Pre - Liquidaci�n" %>

<%@ Register Src="../../Script/Controles/ucBusqueda.ascx" TagName="ucBusqueda" TagPrefix="uc1" %>
<%@ Register Src="../../Script/Controles/ucBusquedaNaveViaje.ascx" TagName="ucBusquedaNaveViaje"
    TagPrefix="uc3" %>
<%@ Register Src="../../Script/Controles/ctrlCalendar.ascx" TagName="ctrlCalendar"
    TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CntMaster" runat="Server">
    <table cellpadding="0" cellspacing="0" style="width: 100%;">
        <div id="content">
            <tr>
                <td>
                    <table class="TabMantenimiento" cellpadding="0" cellspacing="0" style="width: 99.5%;">
                        <tr>
                            <td colspan="3" class="BarraControl">
                                FACTURACI�N<strong> - PRE-LIQUIDACI�N </strong>
                            </td>
                            <td align="right" style="background-color: #003399; color: #FFFFFF; font-weight: bold"
                                colspan="2">
                                <ajax:UpdatePanel ID="upImprimir" runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <asp:ImageButton ID="BtnExportar" runat="server" ImageUrl="~/Script/Imagenes/excel.gif"
                                            ToolTip="Exportar" Height="22px" Width="23px" />
                                        <asp:ImageButton ID="BtnImprimir" runat="server" ImageUrl="~/Script/Imagenes/b-imprimir.gif"
                                            ToolTip="Imprimir Tarja Eir" OnClick="BtnImprimir_Click" />
                                        <asp:ImageButton ID="imgRetornar" runat="server" ImageUrl="~/Script/Imagenes/b-regresar.gif"
                                            ToolTip="Retornar al Listado" />
                                    </ContentTemplate>
                                    <Triggers>
                                        <ajax:AsyncPostBackTrigger ControlID="BtnImprimir" EventName="Click" />
                                    </Triggers>
                                </ajax:UpdatePanel>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="5">
                                <ajax:UpdatePanel ID="upCabecera2" runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <table cellpadding="0" cellspacing="0" class="TabMantenimiento" style="width: 100%;
                                            height: 100px">
                                            <tr>
                                                <td>
                                                    Tipo Calculo
                                                </td>
                                                <td>
                                                    <asp:RadioButton ID="RbtImportacion" Checked="true" Text="Importaci�n" runat="server"
                                                        AutoPostBack="True" OnCheckedChanged="RbtImportacion_CheckedChanged" />
                                                    <asp:RadioButton ID="RbtExportacion" Text="Exportaci�n" runat="server" AutoPostBack="True"
                                                        OnCheckedChanged="RbtExportacion_CheckedChanged" />
                                                </td>
                                                <td>
                                                    Nro.Volante
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="TxtVolante" runat="server" Width="87px"></asp:TextBox>
                                                </td>
                                                <td>
                                                    F.Almacenaje
                                                </td>
                                                <td colspan="2">
                                                    <uc2:ctrlCalendar ID="TxtFechaRetiro" runat="server" />
                                                </td>
                                                <td>
                                                    <asp:Button ID="BtnLiquidar" runat="server" OnClick="BtnLiquidar_Click" OnClientClick="return fun_Liquidar()"
                                                        Text="Liquidar" Width="80px" />
                                                    <br />
                                                    <asp:Button ID="BtnNuevo" runat="server" OnClick="BtnNuevo_Click" Text="Nuevo" 
                                                        Width="80px" />
                                                    <br />
                                                    <asp:Button ID="BtnGrabarLiq" runat="server" Text="Grabar" 
                                                        Width="80px" onclick="BtnGrabarLiq_Click" />
                                                    <br />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Booking</td>
                                                <td>
                                                    <asp:TextBox ID="TxtDocumentoOrigen" runat="server" Enabled="false" 
                                                        Width="177px"></asp:TextBox>
                                                    <asp:ImageButton ID="BtnDocOri" runat="server" Height="26px" 
                                                        ImageUrl="~/Script/Imagenes/IconButton/Buscar.png" 
                                                        OnClientClick="return fun_OpenDocOri();" Width="32px" />
                                                </td>
                                                <td>
                                                    Moneda
                                                </td>
                                                <td>
                                                    <asp:DropDownList ID="DplMoneda" runat="server">
                                                    </asp:DropDownList>
                                                </td>
                                                <td>
                                                    Cliente
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="Txtcliente" runat="server" Enabled="false" Width="283px"></asp:TextBox>
                                                </td>
                                                <td colspan="2" rowspan="2">
                                                    <asp:Label ID="LblMessage" runat="server" Font-Bold="True" ForeColor="Red"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    &nbsp;</td>
                                                <td>
                                                    <asp:TextBox ID="TxtIdDocOri" runat="server" Enabled="false" Width="30px" Style="display: none"></asp:TextBox>
                                                </td>
                                                <td>
                                                    Contrato
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="TxtIdContrato" Enabled="false" runat="server" Width="30px"></asp:TextBox>
                                                    <asp:TextBox ID="TxtContrato" Enabled="false" runat="server" Width="87px"></asp:TextBox>
                                                </td>
                                                <td>
                                                    Tarifas
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="TxtClienteContrato" runat="server" Enabled="false" Width="283px"></asp:TextBox>
                                                </td>
                                            </tr>
                                        </table>
                                    </ContentTemplate>
                                    <Triggers>
                                        <ajax:AsyncPostBackTrigger ControlID="BtnLiquidar" EventName="Click" />
                                        <ajax:AsyncPostBackTrigger ControlID="RbtExportacion" EventName="CheckedChanged" />
                                        <ajax:AsyncPostBackTrigger ControlID="RbtImportacion" EventName="CheckedChanged" />
                                        <ajax:AsyncPostBackTrigger ControlID="BtnNuevo" EventName="Click" />
                                        <ajax:AsyncPostBackTrigger ControlID="BtnDocOri" EventName="Click" />
                                        
                                    </Triggers>
                                </ajax:UpdatePanel>
                            </td>
                        </tr>
                        <tr style="width: 100%">
                            <%-- <asp:BoundField DataField="dInafecto" HeaderText="Inafecto" SortExpression="dInafecto"
                                                        ItemStyle-Width="2%"></asp:BoundField>
                                                    <asp:TemplateField HeaderText="Descuento" SortExpression="dDescuento">
                                                        <ItemTemplate>
                                                            <asp:TextBox ID="TxtDescuentoGrvAlm" Width="100%" BorderStyle="None" Style="font-size: 10px;
                                                                text-align: center; background-color: Transparent" runat="server" Text='<%# Bind("dDescuento") %>'></asp:TextBox>
                                                        </ItemTemplate>
                                                        <ItemStyle Width="2%" />
                                                    </asp:TemplateField>--%>
                            <td colspan="5" valign="top" style="width: 100%">
                                <ajax:UpdatePanel ID="UpListadoServicio" runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <asp:Panel BackColor="White" runat="server" ScrollBars="Vertical" Height="150px"
                                            Width="100%" ID="pListadoServicio">
                                            <asp:GridView SkinID="GrvPreLiquidacion" ID="GrvListadoServicio" runat="server" AutoGenerateColumns="False"
                                                Caption="LISTADO DE SERVICIOS" DataKeyNames="iIdOrdSer,iIdOrdSerDet,dMontoUnitario,dTotal,dIgv,dAfecto,dDescuento,sTipoMoneda,iIdDocOri,iIdDocOriDet,iIdTarifa,dMontoBruto,dCantidad,dDetraccion,sModalidad,sOrigen"
                                                OnSorting="GrvListado_Sorting" AllowSorting="True" Width="100%" OnRowDataBound="GrvListadoServicio_RowDataBound">
                                                <Columns>
                                                    <asp:TemplateField>
                                                        <HeaderTemplate>
                                                            <asp:CheckBox runat="server" ID="chkAllQuitarGrp" onclick="SelectGroupChecks(this.id)"
                                                                AutoPostBack="True" />
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:CheckBox ID="chkSeleccionar" runat="server" OnCheckedChanged="chkSeleccionar_CheckedChanged"
                                                                AutoPostBack="True" />
                                                        </ItemTemplate>
                                                        <ItemStyle Width="1%" />
                                                    </asp:TemplateField>
                                                    <asp:BoundField DataField="iIdOrdSer" HeaderText="Nro.OS" ItemStyle-Width="1%" SortExpression="iIdOrdSer">
                                                      
                                                    </asp:BoundField>
                                                    <asp:TemplateField HeaderText="O.S.Det" SortExpression="iIdOrdSerDet">
                                                        <ItemTemplate>
                                                            <asp:TextBox ID="TxtIdOrdSerDet" Width="100%" BorderStyle="None" Style="text-align: center;
                                                                background-color: Transparent" runat="server" Text='<%# Bind("iIdOrdSerDet") %>'></asp:TextBox>
                                                        </ItemTemplate>
                                                        <ItemStyle Width="1%" />
                                                    </asp:TemplateField>
                                                    <asp:BoundField DataField="iItem" HeaderText="Item" SortExpression="iItem" >
                                                        <ItemStyle Width="1%" HorizontalAlign="Center" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="sDescripcionServicio" SortExpression="sDescripcionServicio"
                                                        HeaderText="Servicio" ItemStyle-Width="20%">
                                                        <ItemStyle Width="20%" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="sContenedor" SortExpression="sContenedor" HeaderText="Cont."
                                                        ItemStyle-Width="5%">
                                                        
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="sTipoCarga" SortExpression="sTipoCarga" HeaderText="Carga"
                                                        ItemStyle-Width="5%">
                                                        
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="sTipoContenedor" SortExpression="sTipoContenedor" HeaderText="T.Cnt"
                                                        ItemStyle-Width="5%">
                                                        
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="iIdTamanoContenedor" SortExpression="iIdTamanoContenedor"
                                                        HeaderText="Tm�o" >
                                                        <ItemStyle Width="3%" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="sEmbalaje" SortExpression="sEmbalaje" HeaderText="Emb."
                                                        ItemStyle-Width="5%">
                                                        
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="iIdTarifa" SortExpression="iIdTarifa" HeaderText="Tarf"
                                                        ItemStyle-Width="2%">
                                                        
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="dMontoUnitario" SortExpression="dMontoUnitario" HeaderText="Monto"
                                                        ItemStyle-Width="2%">
                                                        <ItemStyle HorizontalAlign="Center" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="dCantidad" SortExpression="dCantidad" HeaderText="Cantidad"
                                                        ItemStyle-Width="2%">
                                                        <ItemStyle HorizontalAlign="Center" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="sTipoMoneda" HeaderText="" SortExpression="sTipoMoneda"
                                                        ItemStyle-Width="1%">
                                                        
                                                    </asp:BoundField> 
                                                    <asp:TemplateField HeaderText="Afecto" SortExpression="dAfecto">
                                                        <ItemTemplate>
                                                            <asp:TextBox ID="TxtAfectoGrvSer" Width="100%" BorderStyle="None" Style="font-size: 10px;
                                                                text-align: center; background-color: Transparent" runat="server" Text='<%# Bind("dAfecto") %>'></asp:TextBox>
                                                        </ItemTemplate>
                                                        <ItemStyle Width="1%" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="DET" SortExpression="dDetraccion">
                                                        <ItemStyle Width="2%" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Igv" SortExpression="dIgv">
                                                        <ItemTemplate>
                                                            <asp:TextBox ID="TxtIgvGrvSer" Width="100%" BorderStyle="None" Style="font-size: 10px;
                                                                text-align: center; background-color: Transparent" runat="server" Text='<%# Bind("dIgv") %>'></asp:TextBox>
                                                        </ItemTemplate>
                                                        <ItemStyle Width="2%" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Total" SortExpression="dTotal">
                                                        <ItemTemplate>
                                                            <asp:TextBox ID="TxtTotalGrvSer" Width="100%" BorderStyle="None" Style="font-size: 10px;
                                                                text-align: center; background-color: Transparent" runat="server" Text='<%# Bind("dTotal") %>'></asp:TextBox>
                                                        </ItemTemplate>
                                                        <ItemStyle Width="2%" />
                                                    </asp:TemplateField>
                                                    
                                                    
                                                    <asp:BoundField DataField="sModalidad" SortExpression="sModalidad" HeaderText="Modalidad"
                                                        ItemStyle-Width="5%">
                                                        
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="sOrigen" SortExpression="sOrigen" HeaderText="Origen"
                                                        ItemStyle-Width="5%">
                                                        
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="dtFecha_Ingreso" SortExpression="dtFecha_Ingreso" HeaderText="F.Ingreso"
                                                        DataFormatString="{0:dd/MM/yyyy}">
                                                        <ItemStyle Width="5%" />
                                                    </asp:BoundField>
                                                </Columns>
                                            </asp:GridView>
                                        </asp:Panel>
                                    </ContentTemplate>
                                    <Triggers>
                                        <ajax:AsyncPostBackTrigger ControlID="BtnLiquidar" EventName="Click" />
                                        <ajax:AsyncPostBackTrigger ControlID="BtnNuevo" EventName="Click" />
                                    </Triggers>
                                </ajax:UpdatePanel>
                                <FENIX:DataNavigator ID="dnvListadoServicio" runat="server" AllowCustomPaging="True"
                                    ButtonText="" CurrentPage="1" CustomClientFunction="" EnableAskingAfterPage="False"
                                    EnableCustomScript="False" GridViewId="GrvListado" ImagesPath="~/Imagenes/DN/"
                                    IsParentShowWindow="False" MessageAskingAfterPage="" PageIndicatorFormat="P�gina {0} / {1}"
                                    Visible="False" Width="100%" WidthButtonIr="" WidthTextBoxNumPagina="25px" />
                            </td>
                            <%-- <asp:BoundField DataField="dInafecto" HeaderText="Inafecto" SortExpression="dInafecto"
                                                        ItemStyle-Width="2%"></asp:BoundField>
                                                    <asp:TemplateField HeaderText="Descuento" SortExpression="dDescuento">
                                                        <ItemTemplate>
                                                            <asp:TextBox ID="TxtDescuentoGrvAlm" Width="100%" BorderStyle="None" Style="font-size: 10px;
                                                                text-align: center; background-color: Transparent" runat="server" Text='<%# Bind("dDescuento") %>'></asp:TextBox>
                                                        </ItemTemplate>
                                                        <ItemStyle Width="2%" />
                                                    </asp:TemplateField>--%>
                        </tr>
                        <tr>
                            <td colspan="5" valign="top" style="width: 100%">
                                <ajax:UpdatePanel ID="UpListadoAlmacenaje" runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <asp:Panel BackColor="lightgoldenrodyellow" runat="server" ScrollBars="Vertical"
                                            Height="110px" Width="100%" ID="plScrol">
                                            <asp:GridView ID="GrvListadoAlamcenaje" SkinID="GrvPreLiquidacion" runat="server" AutoGenerateColumns="False"
                                                Caption="LISTADO DE ALMACENAJE" DataKeyNames="iIdOrdSer,iIdOrdSerDet,dTotal,dIgv,dAfecto,dDescuento,sTipoMoneda"
                                                Width="100%" OnSorting="GrvListadoAlamcenaje_Sorting" AllowSorting="True" OnRowDataBound="GrvListadoAlamcenaje_RowDataBound">
                                                <Columns>
                                                    <asp:TemplateField>
                                                        <ItemTemplate>
                                                            <asp:CheckBox ID="chkSeleccionarAlma" runat="server" AutoPostBack="True" OnCheckedChanged="chkSeleccionarAlma_CheckedChanged" />
                                                        </ItemTemplate>
                                                        <ItemStyle Width="1%" />
                                                    </asp:TemplateField>
                                                    <asp:BoundField DataField="iIdOrdSer" HeaderText="Nro.OS" SortExpression="iIdOrdSer">
                                                        <ItemStyle Width="1%" />
                                                    </asp:BoundField>
                                                    <asp:TemplateField HeaderText="O.S.Det" SortExpression="iIdOrdSerDet">
                                                        <ItemTemplate>
                                                            <asp:TextBox ID="TxtIdOrdSerDetAlm" Width="100%" BorderStyle="None" Style="text-align: center;
                                                                background-color: Transparent" runat="server" Text='<%# Bind("iIdOrdSerDet") %>'></asp:TextBox>
                                                        </ItemTemplate>
                                                        <ItemStyle Width="1%" />
                                                    </asp:TemplateField>
                                                    <asp:BoundField DataField="iItem" HeaderText="Item" SortExpression="iItem" ItemStyle-Width="1%">
                                                        <ItemStyle Width="1%" HorizontalAlign="Center" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="sDescripcionServicio" SortExpression="sDescripcionServicio"
                                                        HeaderText="Servicio" ItemStyle-Width="20%">
                                                        <ItemStyle Width="20%" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="sContenedor" HeaderText="Cont." SortExpression="sContenedor"
                                                        ItemStyle-Width="5%"></asp:BoundField>
                                                    <asp:BoundField DataField="sTipoCarga" HeaderText="Carga" SortExpression="sTipoCarga"
                                                        ItemStyle-Width="5%"></asp:BoundField>
                                                    <asp:BoundField DataField="sTipoContenedor" HeaderText="T.Cnt" SortExpression="sTipoContenedor"
                                                        ItemStyle-Width="5%"></asp:BoundField>
                                                    <asp:BoundField DataField="iIdTamanoContenedor" HeaderText="Tm�o" SortExpression="iIdTamanoContenedor"
                                                        ItemStyle-Width="3%"></asp:BoundField>
                                                    <asp:BoundField DataField="sEmbalaje" HeaderText="Emb." SortExpression="sEmbalaje"
                                                        ItemStyle-Width="5%"></asp:BoundField>
                                                    <asp:BoundField DataField="iIdTarifa" HeaderText="Tarf" SortExpression="iIdTarifa"
                                                        ItemStyle-Width="2%"></asp:BoundField>
                                                    <asp:BoundField DataField="dMontoUnitario" HeaderText="Monto" SortExpression="dMontoUnitario"
                                                        ItemStyle-Width="2%"></asp:BoundField>
                                                    <asp:BoundField DataField="dCantidad" HeaderText="Cantidad" SortExpression="dCantidad"
                                                        ItemStyle-Width="2%"></asp:BoundField>
                                                    <asp:BoundField DataField="sTipoMoneda" HeaderText="" SortExpression="sTipoMoneda"
                                                        ItemStyle-Width="1%"></asp:BoundField>
                                                    <asp:TemplateField HeaderText="Afecto" SortExpression="dAfecto">
                                                        <ItemTemplate>
                                                            <asp:TextBox ID="TxtAfectoGrvAlm" Width="100%" BorderStyle="None" Style="font-size: 10px;
                                                                text-align: center; background-color: Transparent" runat="server" Text='<%# Bind("dAfecto") %>'></asp:TextBox>
                                                        </ItemTemplate>
                                                        <ItemStyle Width="1%" />
                                                    </asp:TemplateField>
                                                     <asp:TemplateField HeaderText="DET" SortExpression="dDetraccion">
                                                        <ItemStyle Width="2%" />
                                                    </asp:TemplateField>
                                                   <%-- <asp:BoundField DataField="dInafecto" HeaderText="Inafecto" SortExpression="dInafecto"
                                                        ItemStyle-Width="2%"></asp:BoundField>
                                                    <asp:TemplateField HeaderText="Descuento" SortExpression="dDescuento">
                                                        <ItemTemplate>
                                                            <asp:TextBox ID="TxtDescuentoGrvAlm" Width="100%" BorderStyle="None" Style="font-size: 10px;
                                                                text-align: center; background-color: Transparent" runat="server" Text='<%# Bind("dDescuento") %>'></asp:TextBox>
                                                        </ItemTemplate>
                                                        <ItemStyle Width="2%" />
                                                    </asp:TemplateField>--%>
                                                    <asp:TemplateField HeaderText="Igv" SortExpression="dIgv">
                                                        <ItemTemplate>
                                                            <asp:TextBox ID="TxtIgvGrvAlm" Width="100%" BorderStyle="None" Style="font-size: 10px;
                                                                text-align: center; background-color: Transparent" runat="server" Text='<%# Bind("dIgv") %>'></asp:TextBox>
                                                        </ItemTemplate>
                                                        <ItemStyle Width="2%" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Total" SortExpression="dTotal">
                                                        <ItemTemplate>
                                                            <asp:TextBox ID="TxtTotalGrvAlm" Width="100%" BorderStyle="None" Style="font-size: 10px;
                                                                text-align: center; background-color: Transparent" runat="server" Text='<%# Bind("dTotal") %>'></asp:TextBox>
                                                        </ItemTemplate>
                                                        <ItemStyle Width="2%" />
                                                    </asp:TemplateField>
                                                    <asp:BoundField DataField="sModalidad" HeaderText="Modalidad" SortExpression="sModalidad"
                                                        ItemStyle-Width="5%"></asp:BoundField>
                                                    <asp:BoundField DataField="sOrigen" HeaderText="Origen" SortExpression="sOrigen"
                                                        ItemStyle-Width="5%"></asp:BoundField>
                                                   <asp:BoundField DataField="dtFecha_Ingreso" HeaderText="F.Ingreso" SortExpression="dtFecha_Ingreso"
                                                        DataFormatString="{0:dd/MM/yyyy}" ItemStyle-Width="5%"></asp:BoundField>
                                                </Columns>
                                            </asp:GridView>
                                        </asp:Panel>
                                    </ContentTemplate>
                                    <Triggers>
                                        <ajax:AsyncPostBackTrigger ControlID="BtnLiquidar" EventName="Click" />
                                        <ajax:AsyncPostBackTrigger ControlID="BtnNuevo" EventName="Click" />
                                    </Triggers>
                                </ajax:UpdatePanel>
                                <FENIX:DataNavigator ID="dnvListadoAlmacenaje" runat="server" AllowCustomPaging="True"
                                    ButtonText="" CurrentPage="1" CustomClientFunction="" EnableAskingAfterPage="False"
                                    EnableCustomScript="False" GridViewId="GrvListado" ImagesPath="~/Imagenes/DN/"
                                    IsParentShowWindow="False" MessageAskingAfterPage="" PageIndicatorFormat="P�gina {0} / {1}"
                                    Visible="False" Width="100%" WidthButtonIr="" WidthTextBoxNumPagina="25px" />
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 101px">
                                &nbsp;<table cellpadding="0" cellspacing="0" style="width: 105%; height: 60px;">
                                    <tr>
                                        <td bgcolor="Yellow" style="font-size: small; color: #000000; text-decoration: underline;
                                            border-style: none; font-family: 'Times New Roman', Times, serif;">
                                            Sin Tarifa
                                           </td>
                                    </tr>
                                    <tr>
                                        <td style="font-size: small; background-color: #FF0000; color: #000000; text-decoration: underline;
                                            border-style: none; font-family: 'Times New Roman', 
                                                        Times, serif; background-color: #FF0000;">
                                            Peligrosa
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="font-size: small; color: #000000; text-decoration: underline; border-style: none;
                                            font-family: 'Times New Roman', Times, serif; background-color: #00FF00;" bgcolor="#009900">
                                            O.S Pendiente
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td style="width: 213px">
                                &nbsp;
                                <ajax:UpdatePanel ID="upObservacion" runat="server">
                                    <ContentTemplate>
                                        <asp:TextBox ID="TxtObservacion" Enabled="false" runat="server" Height="80px" TextMode="MultiLine"
                                            Width="635px"></asp:TextBox>
                                    </ContentTemplate>
                                </ajax:UpdatePanel>
                            </td>
                            <td colspan="2">
                                <ajax:UpdatePanel ID="upInfoAlmacenaje" UpdateMode="Conditional" runat="server">
                                    <ContentTemplate>
                                        <table class="TabMantenimiento" cellspacing="0" cellpadding="0" style="width: 80%;">
                                            <tr>
                                                <td colspan="2">
                                                    <strong>Informaci�n Almacenaje </strong>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Dias Libres
                                                </td>
                                                <td align="right">
                                                    <asp:TextBox ID="TxtDiasLibres" runat="server" ReadOnly="true" Style="text-align: right"
                                                        Width="40px" ForeColor="#990000"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Retroactivo?
                                                </td>
                                                <td align="right">
                                                    <asp:TextBox ID="TxtRetroactivo" ReadOnly="true" Style="text-align: right" runat="server"
                                                        Width="40px" ForeColor="#990000"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Fech.Retiro
                                                </td>
                                                <td align="right">
                                                    <asp:TextBox ID="TxtFechaRetiroInfo" ReadOnly="true" Style="text-align: right" runat="server"
                                                        Width="40px" ForeColor="#990000"></asp:TextBox>
                                                </td>
                                            </tr>
                                        </table>
                                    </ContentTemplate>
                                </ajax:UpdatePanel>
                            </td>
                            <td>
                                <ajax:UpdatePanel ID="upTotal" runat="server">
                                    <ContentTemplate>
                                        <table class="TabMantenimiento" cellspacing="0" cellpadding="0" style="width: 94%;">
                                            <tr>
                                                <td>
                                                    Sub Total
                                                </td>
                                                <td align="right">
                                                    <asp:TextBox ReadOnly="true" Style="text-align: right" ID="TxtSubTotal" runat="server"
                                                        Width="40px" Font-Bold="True"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Descuento
                                                </td>
                                                <td align="right">
                                                    <asp:TextBox ID="TxtDescuento" ReadOnly="true" Style="text-align: right" runat="server"
                                                        Width="40px" Font-Bold="True"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    IGV
                                                </td>
                                                <td align="right">
                                                    <asp:TextBox ID="TxtIgv" ReadOnly="true" Style="text-align: right" runat="server"
                                                        Width="40px" Font-Bold="True"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Total
                                                </td>
                                                <td align="right">
                                                    <asp:TextBox ID="TxtTotal" ReadOnly="true" Style="text-align: right" runat="server"
                                                        Width="40px" Font-Bold="True"></asp:TextBox>
                                                </td>
                                            </tr>
                                        </table>
                                    </ContentTemplate>
                                    <Triggers>
                                        <ajax:AsyncPostBackTrigger ControlID="BtnLiquidar" EventName="Click" />
                                    </Triggers>
                                </ajax:UpdatePanel>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </div>
    </table>
    <asp:Panel ID="pnlDescuento" runat="server" CssClass="PanelPopup" Width="455px">
        <ajax:UpdatePanel ID="UpDescuento" runat="server">
            <ContentTemplate>
                <table cellpadding="0" cellspacing="0" class="TabMantenimiento" style="width: 100%;">
                    <tr>
                        <td>
                            Servicio
                        </td>
                        <td>
                            <asp:TextBox ID="TxtServicio_Desc" runat="server" Width="357px" BackColor="#CCCCCC"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Tipo
                        </td>
                        <td>
                            <asp:DropDownList ID="DplTipo_Desc" runat="server" Height="16px" Width="219px">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Valorvalor
                        </td>
                        <td>
                            <asp:TextBox ID="TxtValor_Des" runat="server" Width="79px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="2">
                            <br />
                        </td>
                    </tr>
                </table>
            </ContentTemplate>
            <Triggers>
                <ajax:AsyncPostBackTrigger ControlID="btnGrabar" EventName="Click" />
                <ajax:AsyncPostBackTrigger ControlID="BtnLiquidar" EventName="Click" />
            </Triggers>
        </ajax:UpdatePanel>
        <table style="width: 100%" align="center">
            <tr>
                <td align="center">
                    <asp:Button ID="btnGrabar" runat="server" Text="Aceptar" OnClick="btnGrabar_Click" />
                    <asp:Button ID="btnCancelar" runat="server" Text="Cancelar" />
                </td>
            </tr>
        </table>
    </asp:Panel>
    <ajaxToolkit:ModalPopupExtender ID="mpeDescuento" runat="server" DynamicServicePath=""
        Enabled="True" PopupControlID="pnlDescuento" TargetControlID="BtnOpenDescuento"
        CancelControlID="btnCancelar" BackgroundCssClass="modalBackground">
    </ajaxToolkit:ModalPopupExtender>
    <asp:Button ID="BtnOpenDescuento" Style="display: none;" runat="server" Text="[Open Descuento]" />
    <asp:Panel ID="pBusqueda" runat="server" CssClass="PanelPopup" Width="405px">
        <ajax:UpdatePanel ID="upBusqueda" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <uc1:ucBusqueda ID="ucBusqueda" runat="server" OnLoad="ucBusqueda_Load" />
            </ContentTemplate>
            <Triggers>
                
                <ajax:AsyncPostBackTrigger ControlID="BtnDocOri" EventName="Click" />
            </Triggers>
        </ajax:UpdatePanel>
    </asp:Panel>
    <ajaxToolkit:ModalPopupExtender ID="mpBusqueda" runat="server" DynamicServicePath=""
        Enabled="True" PopupControlID="pBusqueda" TargetControlID="BtnOpenBusqueda" CancelControlID="BtnCerrarBusqueda"
        BackgroundCssClass="modalBackground">
    </ajaxToolkit:ModalPopupExtender>   
    <asp:Button ID="btnOpenNavePopub" Style="display: none;" runat="server" Text="[Open NavVia]" />
    <asp:Button ID="BtnCerrarNavePop" Style="display: none;" runat="server" Text="[Close NavVia]" />
    <asp:Button ID="BtnCerrarBusqueda" Style="display: none;" runat="server" Text="[Close Busqueda]" />
    <asp:Button ID="BtnOpenBusqueda" Style="display: none;" runat="server" Text="[Open Busqueda]" />
    <asp:HiddenField ID="hdCodigoDO" runat="server" />
    <asp:HiddenField ID="hdIdDocOri" runat="server" />
    <asp:HiddenField ID="hdIdNavVia" runat="server" />
    <asp:HiddenField ID="hdIdOrdSerDet" runat="server" />
    <asp:HiddenField ID="hdIdContrato" runat="server" />
    <script language="javascript" type="text/javascript">

        var tiempo = 0;
        var seconds = 0;

        var myVar = setInterval(function () {
            clearInterval(myVar);
        }, 1000);

        var myTiempo = setTimeout(function () {
            clearInterval(myTiempo);
        }, 1);

        function myStopFunction() {
            clearInterval(myVar);
            seconds = '<%=GlobalEntity.Instancia.TiempoDuracionSesion%>' * 60;
            tiempo = '<%=GlobalEntity.Instancia.TiempoDuracionSesion%>' * 1000 * 60;

            CuentaTiempo(tiempo);
        }

        function CuentaTiempo(tiempo) {
            clearTimeout(myTiempo);
            myTiempo = setTimeout(function () {
                document.getElementById('<%=btn_cerrar.ClientID%>').click();
                //window.location = "../Seguridad/eFENIX_Login.aspx";
            }, tiempo);
        }



        function SessionExpireAlert(timeout) {
            seconds = timeout / 1000;
            document.getElementsByName("seconds").innerHTML = seconds;

            myvar = setInterval(function () {
                seconds--;
                document.getElementById("seconds").innerHTML = seconds;
            }, 1000);

            CuentaTiempo(timeout);
        };

        var goodexit = false;
        window.onbeforeunload = confirmRegisterExit;
        function confirmRegisterExit(evt) {
            if (!goodexit) {
                //return "Si vas abandonar el sistema haz click en Cerrar Sesion!!";
                document.getElementById('<%=btn_cerrar.ClientID%>').click();
            }
        }


        function fun_Liquidar() {

            document.getElementById('<%=hdIdOrdSerDet.ClientID%>').value = '';
            return true;
        }
        function ValidarLiquidacion() {
            Message = ""
            if ((document.getElementById("<%=RbtImportacion.ClientID%>").checked) == '0' ||
                (document.getElementById("<%=RbtExportacion.ClientID%>").checked) == '0') {
                Message = "Seleccione un tipo de facturaci�n \n";
            }

            if (document.getElementById("<%=RbtImportacion.ClientID%>").checked == '0') {
                if (fc_Trim(document.getElementById("<%=TxtFechaRetiro.TextBoxClientID%>").value) == '') {
                    Message = Message + "Ingrese una fecha de liquidaci�n \n";

                }
                if (fc_Trim(document.getElementById("<%=TxtVolante.ClientID%>").value) == '') {
                    Message = Message + "Ingrese un n�merovolante";

                }

            } else if (document.getElementById("<%=RbtExportacion.ClientID%>").checked == '0') {                
                if (fc_Trim(document.getElementById("<%=TxtDocumentoOrigen.ClientID%>").value) == '') {
                    Message = Message + "Ingrese un Documento Origen";

                }

            }


            if (fc_Trim(Message) == '') {
                return true;
            } else {
                alert(Message);
                return false;
            }

        }



        function OpenDescuento(chk, sServicio) {

            if (document.getElementById(chk).checked == false) {
                alert('Debe Seleccionar un registro');

            }
            else {
                document.getElementById('<%= BtnOpenDescuento.ClientID %>').click();
                document.getElementById('<%= TxtServicio_Desc.ClientID %>').value = sServicio;
            }
            return false;
        }

        function fun_OpenDocOri() {
          
            document.getElementById('<%= hdIdDocOri.ClientID%>').value = '1';
            document.getElementById('<%= hdIdNavVia.ClientID%>').value = '0';
            document.getElementById('<%= BtnOpenBusqueda.ClientID %>').click();
            return true;
        }

        function fun_OpenNaveViaje() {

            document.getElementById('<%= hdIdDocOri.ClientID%>').value = '0';
            document.getElementById('<%= hdIdNavVia.ClientID%>').value = '1';
            document.getElementById('<%= btnOpenNavePopub.ClientID %>').click();
            return true;
        }

        function SelectGroupChecks(ParentID) {
            j = 0;
            k = 1;
            idCheck = ParentID.replace('All', '');
            Total = 0;
            Afecto = 0;
            Igv = 0;
            Descuento = 0;
            objCheck = null;
            frm = document.forms[0];
            sIdOrdSerDetTodos = "";
            for (i = 0; i < frm.elements.length; i++) {
                obj = frm.elements[i];

                if ((obj.type == 'checkbox')) {
                    j = j + 1;
                    if (obj.disabled != 1) {
                        obj.checked = document.getElementById(ParentID).checked;
                    } else {

                    }
                }

                if ((obj.type == 'text')) {
                    if (document.getElementById(ParentID).checked == '1') {
                        if (obj.name.substr(41, 56) == 'TxtIdOrdSerDet') {
                            sIdOrdSerDetTodos = sIdOrdSerDetTodos + obj.value + '|';

                        }
                        if (obj.name.substr(43, 60) == 'TxtIdOrdSerDetAlm') {
                            sIdOrdSerDetTodos = sIdOrdSerDetTodos + obj.value + '|';
                        }
                    }


                }

            }

            if (document.getElementById(ParentID).checked == '1') {
                document.getElementById("<%=hdIdOrdSerDet.ClientID%>").value = sIdOrdSerDetTodos;
            }
            else {
                document.getElementById("<%=hdIdOrdSerDet.ClientID%>").value = "";
            }

        }

        function redondea(sVal, nDec) {
            var n = parseFloat(sVal);
            var s = "0.00";
            if (!isNaN(n)) {
                n = Math.round(n * Math.pow(10, nDec)) / Math.pow(10, nDec);
                s = String(n);
                s += (s.indexOf(".") == -1 ? "." : "") + String(Math.pow(10, nDec)).substr(1);
                s = s.substr(0, s.indexOf(".") + nDec + 1);
            }

            return s;

        }
        function SoloEnteros(e) {
            var tecla = document.all ? tecla = e.keyCode : tecla = e.which;
            return ((tecla > 47 && tecla < 58) || tecla == 44 || tecla == 46);
        }

        function fc_Imprimir(iTipoMoneda, iContrato, iIdOrdSerDet, sNroContrato) {

            var iVolante = null;
            var iIdDocOri = null;
            var sOnservacion = "";

            var sFechaLiquidacion = document.getElementById("<%= TxtFechaRetiro.TextBoxClientID %>").value;
            var iIdLineaNegocio = '<%=GlobalEntity.Instancia.IdLineaNegocio %>';
            var sUsuario = '<%=GlobalEntity.Instancia.Usuario %>';
            var sOnservacion = document.getElementById("<%=TxtObservacion.ClientID %>").value;

            var iIdDocOri = document.getElementById('<%= TxtIdDocOri.ClientID %>').value;
            var sDia = sFechaLiquidacion.substring(0, 2);
            var sMes = sFechaLiquidacion.substring(3, 5);
            var sAnno = sFechaLiquidacion.substring(6, 11);


            if (iTipoMoneda == '0') {
                iTipoMoneda = 23
            } else if (iTipoMoneda == '1') {
                iTipoMoneda = 24
            }
            if (document.getElementById("<%= RbtImportacion.ClientID %>").checked == '1') {
                iVolante = document.getElementById("<%= TxtVolante.ClientID %>").value;
                iTipoOperacion = 66;
            } else if (document.getElementById("<%= RbtExportacion.ClientID %>").checked == '1') {
                iTipoOperacion = 65;
                iVolante = 0;
            }
            if (iIdOrdSerDet == "") {
                alert("Debe seleccionar almenos algun servicio a facturar");
                return false;
            } else {
                strUrl = "&vi_Usuario=" + sUsuario;
                strUrl = strUrl + "&vin_IdLineaNegocio=" + iIdLineaNegocio;
                strUrl = strUrl + "&Vin_IdVolante=" + iVolante;
                strUrl = strUrl + "&Vin_IdDocOri=" + iIdDocOri;
                // Formato SQL
                strUrl = strUrl + "&vin_Fecha_Liquidacion=" + sDia + "/" + sMes + "/" + sAnno;
                strUrl = strUrl + "&vin_IdTipoMoneda=" + iTipoMoneda;
                strUrl = strUrl + "&vin_IdOperacion=" + iTipoOperacion;
                strUrl = strUrl + "&vin_IdOrdSerDet=" + iIdOrdSerDet;

                if (iContrato != "") {
                    strUrl = strUrl + "&vin_IdContrato=" + iContrato;
                } else {

                    strUrl = strUrl + "&vin_IdContrato=0";
                }
                strUrl = strUrl + "&vin_sObservacion=''";
                strUrl = strUrl + "&vin_NroContrato=" + sNroContrato;

                url = '<%=Convert.ToString(ConfigurationManager.AppSettings["ServerReporting"])%>/pages/ReportViewer.aspx?%2fFENIX.Reporte%2fFENIX_Reporte_PreLiquidacion&rs:Command=Render&rc:Parameters=False' + strUrl;
                window.open(url, '', 'toolbar=no,left=0,top=0,,width= 800,height=700, directories=no, status=no, scrollbars=yes, resizable=no, menubar=no');

                return false;


            }

        }
        function fun_LimpiarVariables() {
            alert('entro');
            document.getElementById("<%= hdIdOrdSerDet.ClientID %>").value = '';
            document.getElementById("<%= TxtIdDocOri.ClientID %>").value = '';

        }
        var objFilaAnt = null;
        var backgroundColorFilaAnt = "";

        function fc_SeleccionaFilaSimple(objFila) {
            try {
                if (objFilaAnt != null) {
                    objFilaAnt.style.backgroundColor = backgroundColorFilaAnt;
                }
                objFilaAnt = objFila;
                backgroundColorFilaAnt = objFila.style.backgroundColor;
                objFila.style.backgroundColor = "#c4e4ff";
            }

            catch (e) {
                error = e.message;
            }
        }
        function HabilitarUno(chk) {
            f = document.forms[0].elements;
            for (x = 0; x < f.length; x++) {
                obj = f[x];
                if (obj.type == 'checkbox') {
                    obj.checked = false;
                }
            }
            chk.enabled;
            chk.checked = true;
        }
        function fun_retorno() {
            return false;
        }
    </script>

</asp:Content>
<asp:Content ID="Content3" runat="server" contentplaceholderid="ContentCab">
    <span class="texto_titulo" >Tu Sesion Expirara en &nbsp; 
        <span class="texto_titulo" id="seconds"> </span>
        <span>&nbsp;seg.</span>
     </span>
     <asp:Button ID="btn_cerrar" runat="server" Text="cerrar" 
        onclick="btn_cerrar_Click"  Style="display: none;" />
</asp:Content>

