﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FENIX.Common;
using FENIX.BusinessEntity;
using FENIX.BusinessLogic;
using System.Web.Security;

public partial class Extranet_Operaciones_eFENIX_PreLiquidacion_Generar : System.Web.UI.Page
{


    private void SCA_MsgInformacion(string strError)
    {
        MensajeScript(strError);
    }

    private void MensajeScript(string SMS)
    {
        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "", "<script>alert('" + SMS + "');</script>", false);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        String tiempo = (Convert.ToInt32(GlobalEntity.Instancia.TiempoDuracionSesion.ToString()) * 1000 * 60).ToString();
        Session["Reset"] = true;
        if (!Page.IsPostBack)
        {
            llenarCombos();
            

            ScriptManager.RegisterStartupScript(this, GetType(), "mensaje", "SessionExpireAlert(" + tiempo + ");", true);
        }
        else
            ScriptManager.RegisterStartupScript(this, GetType(), "mensaje", "myStopFunction();", true);
    }

    protected void btn_cerrar_Click(object sender, EventArgs e)
    {
        if (GlobalEntity.Instancia.CerrarExtranet == "S")
        {
            BL_Usuario oBL_Usuario = new BL_Usuario();
            oBL_Usuario.GrabaInicioSesion(GlobalEntity.Instancia.Usuario, "0");
            Session.Abandon();
            FormsAuthentication.SignOut();
            Response.Redirect("../../eFENIX_Login.aspx");
            Response.End();
        }
    }

    protected void ImgBtnBuscar_Click(object sender, EventArgs e)
    {
        BE_Liquidacion oBE_Liquidacion = new BE_Liquidacion();
        BL_Liquidacion oBL_Liquidacion = new BL_Liquidacion();
        BE_Liquidacion oBE_LiquidacionDocOrigen = new BE_Liquidacion();
       

        Int32 Negocio = 0;
        Int32 Operacion = 0;
        Int32 IdOficina = 0;
        Negocio = Convert.ToInt32(dplLineaNegocio.SelectedValue);
        Int32 IdCliente = GlobalEntity.Instancia.IdCliente;
        if(RbtImportacion.Checked == false && RbtExportacion.Checked == false)
        {
            SCA_MsgInformacion("Seleccine el tipo de operacion");
            return;
        }
        if (RbtImportacion.Checked) { Operacion = 66; }
        if (RbtExportacion.Checked) { Operacion = 65; }
        oBE_Liquidacion.sNumeroDo = txtDocumento.Text;
        if(ChkMasterLCL.Checked) { oBE_Liquidacion.sMasterLCL = "1"; }
        if (!ChkMasterLCL.Checked) { oBE_Liquidacion.sMasterLCL = "0"; }
        
        oBE_LiquidacionDocOrigen = oBL_Liquidacion.Listar_PreLiquidacion_Info_Cabecera_2(Negocio,Operacion,oBE_Liquidacion.sNumeroDo, oBE_Liquidacion.sMasterLCL, IdOficina, IdCliente);

        if(IdCliente != oBE_LiquidacionDocOrigen.iIdCliente)
        {
            SCA_MsgInformacion("El documento ingresado no pertece al cliente.");
            return;
        }
        
        if(oBE_LiquidacionDocOrigen.iIdContrato == 0)
        {
            SCA_MsgInformacion("El documento no tiene contrato asociado. Comuniquese con ATC.");
        }
        

        if (oBE_LiquidacionDocOrigen == null)
        {
            //InicializarControles();
            SCA_MsgInformacion("El documento no es valido");
            return;
        }
        
        else
        {
            txtCliente.Text = oBE_LiquidacionDocOrigen.sCliente;
           TxtIdAcuerdo.Text = Convert.ToString(oBE_LiquidacionDocOrigen.iIdContrato);
            TxtNroAcuerdo.Text = Convert.ToString(oBE_LiquidacionDocOrigen.sNroAcuerdo);
            txtsDocOrigen.Text = oBE_LiquidacionDocOrigen.sNumeroDo;
            txtClienteAcuerdo.Text = oBE_LiquidacionDocOrigen.sClienteContrato;
            TxtIdMoneda.Text = oBE_LiquidacionDocOrigen.sTipoMoneda.ToString();
            TxtIdCliente.Text = oBE_LiquidacionDocOrigen.iIdCliente.ToString();
            Int32 IdContrato = Convert.ToInt32(oBE_LiquidacionDocOrigen.iIdContrato);
            //upCabecera.Update();
            if (oBE_LiquidacionDocOrigen.sNroAcuerdo == "" || IdContrato == 0)
            {
                SCA_MsgInformacion("El BL no tiene un numero de acuerdo asociado.");
                return;
            }
            //TxtObservacion.Text = oBE_Liquidacion.sObservacion;
            
            //HfIdCliente.Value = oBE_Liquidacion.iIdCliente.ToString();
            //ViewState["iIdDocOri"] = oBE_Liquidacion.iIdDocOri;

            ConsultarServicios(oBE_LiquidacionDocOrigen.iIdDocOri);
        }

    }

    protected void ConsultarServicios(int? iIdDocOri)
    {
        BE_Liquidacion oBE_Liquidacion = new BE_Liquidacion();
        BL_Liquidacion oBL_Liquidacion = new BL_Liquidacion();
        BE_LiquidacionList oBE_LiquidacionServicio = new BE_LiquidacionList();
        BE_LiquidacionList oBE_LiquidacionAlmacenaje = new BE_LiquidacionList();

        //if (RbtImportacion.Checked) { Operacion = 66; }
        //if (RbtExportacion.Checked) { Operacion = 65; }

        oBE_Liquidacion.iIdDocOri = iIdDocOri;
        oBE_Liquidacion.dtFecha_Liquidacion = Convert.ToDateTime(Request.Form["DatePickerFinname"]);

        String[] Resultado = null;
        int iRetorno = 2;
        Int32 iIdAcuerdo = 0;
        if (TxtNroAcuerdo.Text.Trim() != String.Empty)
            iIdAcuerdo = Convert.ToInt32(TxtIdAcuerdo.Text.Trim());

        oBE_Liquidacion.iIdAcuerdo = iIdAcuerdo;
        oBE_Liquidacion.iIdMoneda = Convert.ToInt32(TxtIdMoneda.Text);
        oBE_Liquidacion.iIdSuperNegocio_2 = Convert.ToInt32(dplLineaNegocio.SelectedValue);
        oBE_Liquidacion.iIdLineaNegocio = Convert.ToInt32(dplLineaNegocio.SelectedValue);

        if (RbtImportacion.Checked)
        {
            oBE_Liquidacion.iIdOperacion = Convert.ToInt32(TipoOperacion.Importacion);
            if (!ChkMasterLCL.Checked)
                oBE_Liquidacion.sNroVolante = txtDocumento.Text;
            else
                oBE_Liquidacion.sNroVolante = null;
        }
        else if (RbtExportacion.Checked)
            oBE_Liquidacion.iIdOperacion = Convert.ToInt32(TipoOperacion.Exportacion);


        if (RbtImportacion.Checked)
            if (ChkMasterLCL.Checked)
                Resultado = oBL_Liquidacion.Listar_Recuperar_Contrato_2(Convert.ToInt32(dplLineaNegocio.SelectedValue), null, iIdDocOri, Convert.ToInt32(TipoOperacion.Importacion), "1").Split('|');
            else
                Resultado = oBL_Liquidacion.Listar_Recuperar_Contrato_2(Convert.ToInt32(dplLineaNegocio.SelectedValue), Convert.ToInt32(txtDocumento.Text), null, Convert.ToInt32(TipoOperacion.Importacion), "0").Split('|');
        else if (RbtExportacion.Checked)
            Resultado = oBL_Liquidacion.Listar_Recuperar_Contrato_2(Convert.ToInt32(dplLineaNegocio.SelectedValue), null, iIdDocOri, Convert.ToInt32(TipoOperacion.Exportacion), "0").Split('|');


        if (RbtImportacion.Checked)
        {
            if (ChkMasterLCL.Checked)
                iRetorno = oBL_Liquidacion.RegistrarServicios_Mandatorios_2(89, Convert.ToInt32(TipoOperacion.Importacion), iIdDocOri, iIdAcuerdo, null, "2");
            else
                iRetorno = oBL_Liquidacion.RegistrarServicios_Mandatorios_2(89, Convert.ToInt32(TipoOperacion.Importacion), iIdDocOri, iIdAcuerdo, Convert.ToInt32(txtDocumento.Text), "0");
        }
        else if (RbtExportacion.Checked)
            iRetorno = oBL_Liquidacion.RegistrarServicios_Mandatorios_2(89, Convert.ToInt32(TipoOperacion.Exportacion), iIdDocOri, iIdAcuerdo, null, "0");


        oBE_Liquidacion.sMasterLCL = ChkMasterLCL.Checked ? "1" : "0";

        oBE_Liquidacion.iIdLiq = 0;
        BE_LiquidacionList lista = oBL_Liquidacion.Listar_PreLiquidacion_2(oBE_Liquidacion);

        Int32 iIdAsumeEnergia = 0;
        Decimal dAfecto = 0;
        Decimal dInafecto = 0;
        Decimal dDescuento = 0;
        Decimal dTotal = 0;
        Decimal dIgv = 0;

        if ((lista != null) && (lista.Count > 0))
        {
            for (int i = 0; i < lista.Count; i++)
            {
                if ((lista[i].sDescripcionServicio.Trim().Length >= 10) && (lista[i].iIdTipoServicio_2 == 3)) //ALMACENAJE //lista[i].sDescripcionServicio.Trim().Substring(0, 10).ToUpper() == "ALMACENAJE"
                {
                    BE_Liquidacion oBE_Servicio = new BE_Liquidacion();
                    oBE_Servicio.iIdOrdSer = lista[i].iIdOrdSer;
                    oBE_Servicio.iIdOrdSerDet = lista[i].iIdOrdSerDet;
                    oBE_Servicio.iIdDocOriDet = lista[i].iIdDocOriDet;
                    oBE_Servicio.sDescripcionServicio = lista[i].sDescripcionServicio;
                    oBE_Servicio.sContenedor = lista[i].sContenedor;
                    oBE_Servicio.sTipoCarga = lista[i].sTipoCarga;
                    oBE_Servicio.sTipoContenedor = lista[i].sTipoContenedor;
                    oBE_Servicio.iIdTamanoContenedor = lista[i].iIdTamanoContenedor;
                    oBE_Servicio.sEmbalaje = lista[i].sEmbalaje;
                    oBE_Servicio.iIdTarifa = lista[i].iIdTarifa;
                    oBE_Servicio.dMontoUnitario = lista[i].dMontoUnitario;
                    oBE_Servicio.dCantidad = lista[i].dCantidad;
                    oBE_Servicio.dAfecto = lista[i].dAfecto;
                    oBE_Servicio.dInafecto = lista[i].dInafecto;
                    oBE_Servicio.dDescuento = lista[i].dDescuento;
                    oBE_Servicio.dIgv = lista[i].dIgv;
                    oBE_Servicio.dTotal = lista[i].dTotal;
                    oBE_Servicio.sModalidad = lista[i].sModalidad;
                    oBE_Servicio.sOrigen = lista[i].sOrigen;
                    oBE_Servicio.sSituacion = lista[i].sSituacion;
                    oBE_Servicio.dtFecha_Ingreso = lista[i].dtFecha_Ingreso;
                    oBE_Servicio.iItem = lista[i].iItem;
                    oBE_Servicio.sTarifa = lista[i].sTarifa;
                    oBE_Servicio.dPeso = lista[i].dPeso;
                    oBE_Servicio.sTipoMoneda = lista[i].sTipoMoneda;
                    oBE_Servicio.sRetroactivo = lista[i].sRetroactivo;
                    oBE_Servicio.dtFechaRetiro = lista[i].dtFechaRetiro;
                    oBE_Servicio.iDiasLibres = lista[i].iDiasLibres;
                    oBE_Servicio.iIdAsumeEnergia_2 = lista[i].iIdAsumeEnergia_2;
                    oBE_Servicio.sOrigenPqteSLI = lista[i].sOrigenPqteSLI;
                    oBE_Servicio.sMarcaSLI = lista[i].sMarcaSLI;
                    oBE_Servicio.sCondCarga = lista[i].sCondCarga;
                    //TxtDiasLibres.Text = lista[i].iDiasLibres.ToString();
                    //TxtRetroactivo.Text = lista[i].sRetroactivo.ToString();
                    oBE_Servicio.dMontoNeto = lista[i].dAfecto;
                    oBE_Servicio.dMontoBruto = lista[i].dTotal;

                    oBE_LiquidacionAlmacenaje.Add(oBE_Servicio);
                }
                else
                {
                    BE_Liquidacion oBE_Servicio = new BE_Liquidacion();
                    oBE_Servicio.iIdOrdSer = lista[i].iIdOrdSer;
                    oBE_Servicio.iIdOrdSerDet = lista[i].iIdOrdSerDet;
                    oBE_Servicio.iIdDocOriDet = lista[i].iIdDocOriDet;
                    oBE_Servicio.sDescripcionServicio = lista[i].sDescripcionServicio;
                    oBE_Servicio.sContenedor = lista[i].sContenedor;
                    oBE_Servicio.sTipoCarga = lista[i].sTipoCarga;
                    oBE_Servicio.sTipoContenedor = lista[i].sTipoContenedor;
                    oBE_Servicio.iIdTamanoContenedor = lista[i].iIdTamanoContenedor;
                    oBE_Servicio.sEmbalaje = lista[i].sEmbalaje;
                    oBE_Servicio.iIdTarifa = lista[i].iIdTarifa;
                    oBE_Servicio.dMontoUnitario = lista[i].dMontoUnitario;
                    oBE_Servicio.dCantidad = lista[i].dCantidad;
                    oBE_Servicio.dAfecto = lista[i].dAfecto;
                    oBE_Servicio.dInafecto = lista[i].dInafecto;
                    oBE_Servicio.dDescuento = lista[i].dDescuento;
                    oBE_Servicio.dIgv = lista[i].dIgv;
                    oBE_Servicio.dTotal = lista[i].dTotal;
                    oBE_Servicio.sModalidad = lista[i].sModalidad;
                    oBE_Servicio.sOrigen = lista[i].sOrigen;
                    oBE_Servicio.sSituacion = lista[i].sSituacion;
                    oBE_Servicio.dtFecha_Ingreso = lista[i].dtFecha_Ingreso;
                    oBE_Servicio.iItem = lista[i].iItem;
                    oBE_Servicio.sTarifa = lista[i].sTarifa;
                    oBE_Servicio.dPeso = lista[i].dPeso;
                    oBE_Servicio.sTipoMoneda = lista[i].sTipoMoneda;
                    oBE_Servicio.dDetraccion = lista[i].dDetraccion;
                    oBE_Servicio.sPorcentDetraccion = lista[i].sPorcentDetraccion;
                    oBE_Servicio.iIdAsumeEnergia_2 = lista[i].iIdAsumeEnergia_2;
                    oBE_Servicio.sOrigenPqteSLI = lista[i].sOrigenPqteSLI;
                    oBE_Servicio.sMarcaSLI = lista[i].sMarcaSLI;
                    oBE_Servicio.sCondCarga = lista[i].sCondCarga;
                    oBE_Servicio.dMontoNeto = lista[i].dAfecto;
                    oBE_Servicio.dMontoBruto = lista[i].dTotal;
                    oBE_LiquidacionServicio.Add(oBE_Servicio);

                    if (lista[i].iIdAsumeEnergia_2 == 1)
                    {
                        iIdAsumeEnergia = 1;
                    }
                }
                dAfecto += lista[i].dAfecto;
                dInafecto += lista[i].dInafecto;
                //dSubTotal = dSubTotal + lista[i].dAfecto;
                dDescuento +=  lista[i].dDescuento;
                dIgv += lista[i].dIgv;
                dTotal = dTotal + lista[i].dTotal;
            }

            //TxtSubTotal.Text = dSubTotal.ToString();
            //TxtDescuento.Text = dDescuento.ToString();
            //TxtIgv.Text = dIGV.ToString();
            //TxtTotal.Text = dTotal.ToString();

            //if (iIdAsumeEnergia == 1)
            //{
            //    TxtCliente.Visible = false;
            //    DdlCliente.Visible = true;
            //}
            //else
            //{
            //    TxtCliente.Visible = true;
            //    DdlCliente.Visible = false;
            //}

            //if ((oBE_LiquidacionServicio.Count > 0) || (oBE_LiquidacionAlmacenaje.Count > 0))
            //{
            //    btnGrabar.Enabled = true;
            //}
        }
        GvListaServicios.DataSource = oBE_LiquidacionServicio;
        GvListaServicios.DataBind();

        GrvListadoAlmacenaje.DataSource = oBE_LiquidacionAlmacenaje;
        GrvListadoAlmacenaje.DataBind();

        lblAfecto.Text = dAfecto.ToString();
        lblDescuento.Text = dDescuento.ToString();
        lblIgv.Text = dIgv.ToString();
        lblTotal.Text = dTotal.ToString();
        UpdatePanel2.Update();

        //if (TxtFechaAlma.Text.Trim().Length > 0)
        //    oBE_Liquidacion.dtFecha_Liquidacion = Convert.ToDateTime(TxtFechaAlma.Text + " " + DateTime.Now.Hour.ToString() + ":" + DateTime.Now.Minute.ToString() + ":" + DateTime.Now.Second.ToString());
    }

    public void llenarCombos()
    {
        ListItem i;
        i = new ListItem("Dep. Temporal", "89");
        dplLineaNegocio.Items.Add(i);
        dplLineaNegocio.SelectedValue = "89";
    }






    protected void ImgBtnGrabar_Click(object sender, EventArgs e)
    {
        //Graba Pre Liquidacion
        try
        {
            BE_Liquidacion oBE_Liquidacion = new BE_Liquidacion();
            BE_LiquidacionList oBE_LiquidacionList = new BE_LiquidacionList();
            BL_Liquidacion oBL_Liquidacion = new BL_Liquidacion();
            String sIdDua = string.Empty;
            String sTotal = string.Empty;
            String sSubTotal = string.Empty;
            String sIgv = string.Empty;
            String sDetraccion = string.Empty;
            String sDescuento = string.Empty;
            Int32 iRetorno = 0;
            Int32 Operacion = 0;
            if (RbtImportacion.Checked) { Operacion = 66; }
            if (RbtExportacion.Checked) { Operacion = 65; }

            //oBE_Liquidacion.iIdCliente = Convert.ToInt32(HfIdCliente.Value);
            oBE_Liquidacion.sObservacion = String.Empty;

            if (TxtIdAcuerdo.Text.Trim().Length > 0)
            {
                oBE_Liquidacion.iIdAcuerdo = Convert.ToInt32(TxtIdAcuerdo.Text);
                iRetorno = oBL_Liquidacion.ConsultarEstadoAcuerdo_2(oBE_Liquidacion.iIdAcuerdo);

                if (iRetorno != 6)
                {
                    SCA_MsgInformacion("No puede registrar una Pre Liquidacion si el contrato está DESAPROBADO");
                    return;
                }
            }

            oBE_Liquidacion.iIdSuperNegocio_2 = Convert.ToInt32(dplLineaNegocio.SelectedValue);
            oBE_Liquidacion.iIdOperacion = Convert.ToInt32(Operacion);
            oBE_Liquidacion.sNumeroDo = txtsDocOrigen.Text;
            oBE_Liquidacion.iIdClienteFacturar = Convert.ToInt32(TxtIdCliente.Text);
            oBE_Liquidacion.iIdAgAduana = null;

            //if (HfIdAcuerdo.Value.Trim().Length > 0)
            //{
            //    oBE_Liquidacion.iIdAcuerdo_2 = Convert.ToInt32(TxtIdAcuerdo.Text);
            //}

            oBE_Liquidacion.iIdMoneda = Convert.ToInt32(TxtIdMoneda.Text);
            oBE_Liquidacion.dAfecto = Convert.ToDecimal(lblAfecto.Text);
            oBE_Liquidacion.dInafecto = 0;
            oBE_Liquidacion.dIgv = Convert.ToDecimal(lblIgv.Text);
            oBE_Liquidacion.dMontoBruto = Convert.ToDecimal(lblTotal.Text);
            oBE_Liquidacion.dMontoNeto = Convert.ToDecimal(lblAfecto.Text);
            oBE_Liquidacion.dDescuento = Convert.ToDecimal(lblDescuento.Text);
           // oBE_Liquidacion.sObservacion = TxtObservacion.Text;
            oBE_Liquidacion.sUsuario = GlobalEntity.Instancia.Usuario;
            oBE_Liquidacion.sNombrePc = GlobalEntity.Instancia.NombrePc;
            oBE_Liquidacion.iIdTipoLiquidacion_2 = 0; //Pre-Liquidacion
            oBE_Liquidacion.sFechaCalculo = Request.Form["DatePickerFinname"]; 


            Decimal dAfecto = 0, dInafecto = 0, dIgv = 0, dTotal = 0;
            BL_Liquidacion oBL_DocumentoPago = new BL_Liquidacion();
            Decimal IGV = oBL_DocumentoPago.IGV();

            foreach (GridViewRow GrvRow in GvListaServicios.Rows)
            {
                if ((GrvRow.FindControl("chkSeleccionarServicio") as CheckBox).Checked)
                {
                    BE_Liquidacion oBE_LiquidacionDet = new BE_Liquidacion();

                    oBE_LiquidacionDet.iIdOrdSerDet = Convert.ToInt32(GvListaServicios.DataKeys[GrvRow.RowIndex].Values["iIdOrdSerDet"].ToString());
                    oBE_LiquidacionDet.iIdTarifa = Convert.ToInt32(GvListaServicios.DataKeys[GrvRow.RowIndex].Values["iIdTarifa"].ToString());
                    oBE_LiquidacionDet.dCantidad = Convert.ToInt32(GvListaServicios.DataKeys[GrvRow.RowIndex].Values["dCantidad"]);
                    oBE_LiquidacionDet.dPeso = Convert.ToDecimal(GvListaServicios.DataKeys[GrvRow.RowIndex].Values["dPeso"].ToString());
                    oBE_LiquidacionDet.dVolumen = 0;
                    oBE_LiquidacionDet.dMontoUnitario = Convert.ToDecimal(GvListaServicios.DataKeys[GrvRow.RowIndex].Values["dMontoUnitario"].ToString());

                    dAfecto = Math.Round(Convert.ToDecimal(GvListaServicios.DataKeys[GrvRow.RowIndex].Values["dAfecto"].ToString()), 2);
                    if(dAfecto <= 0)
                    {
                        SCA_MsgInformacion("Hay un servicio que no tiene monto Afecto. Comuniquese con ATC");                       
                        return;
                    }
                    dInafecto = Math.Round(Convert.ToDecimal(GvListaServicios.DataKeys[GrvRow.RowIndex].Values["dInafecto"].ToString()), 2);
                    dIgv = Math.Round(dAfecto * IGV, 2);
                    dTotal = dAfecto + dIgv + dInafecto;

                    oBE_LiquidacionDet.dAfecto = dAfecto;
                    oBE_LiquidacionDet.dInafecto = dInafecto;
                    oBE_LiquidacionDet.dIgv = dIgv;


                    oBE_LiquidacionDet.dMontoNeto = dAfecto + dInafecto;
                    oBE_LiquidacionDet.dMontoBruto = dTotal;



                    oBE_LiquidacionDet.dDescuento = Convert.ToDecimal(GvListaServicios.DataKeys[GrvRow.RowIndex].Values["dDescuento"].ToString());
                    oBE_LiquidacionDet.dDetraccion = Convert.ToDecimal(GvListaServicios.DataKeys[GrvRow.RowIndex].Values["dDetraccion"].ToString());

                    oBE_LiquidacionDet.sOrigenPqteSLI = GvListaServicios.DataKeys[GrvRow.RowIndex].Values["sOrigenPqteSLI"].ToString();
                    oBE_LiquidacionDet.sMarcaSLI = GvListaServicios.DataKeys[GrvRow.RowIndex].Values["sMarcaSLI"].ToString();

                    oBE_LiquidacionDet.sUsuario = GlobalEntity.Instancia.Usuario;
                    oBE_LiquidacionDet.sNombrePc = GlobalEntity.Instancia.NombrePc;
                    oBE_LiquidacionList.Add(oBE_LiquidacionDet);
                }else
                {

                }
            }


            foreach (GridViewRow GrvRow in GrvListadoAlmacenaje.Rows)
            {
                if ((GrvRow.FindControl("chkSeleccionarAlmacenaje") as CheckBox).Checked)
                {
                    BE_Liquidacion oBE_LiquidacionDet = new BE_Liquidacion();

                    oBE_LiquidacionDet.iIdOrdSerDet = Convert.ToInt32(GrvListadoAlmacenaje.DataKeys[GrvRow.RowIndex].Values["iIdOrdSerDet"].ToString());
                    oBE_LiquidacionDet.iIdTarifa = Convert.ToInt32(GrvListadoAlmacenaje.DataKeys[GrvRow.RowIndex].Values["iIdTarifa"].ToString());
                    oBE_LiquidacionDet.dCantidad = Convert.ToInt32(GrvListadoAlmacenaje.DataKeys[GrvRow.RowIndex].Values["dCantidad"]);
                    oBE_LiquidacionDet.dPeso = Convert.ToDecimal(GrvListadoAlmacenaje.DataKeys[GrvRow.RowIndex].Values["dPeso"].ToString());
                    oBE_LiquidacionDet.dVolumen = 0;
                    oBE_LiquidacionDet.dMontoUnitario = Convert.ToDecimal(GrvListadoAlmacenaje.DataKeys[GrvRow.RowIndex].Values["dMontoUnitario"].ToString());


                    dAfecto = Math.Round(Convert.ToDecimal(GrvListadoAlmacenaje.DataKeys[GrvRow.RowIndex].Values["dAfecto"].ToString()), 2);
                    if (dAfecto <= 0)
                    {
                        SCA_MsgInformacion("Hay un servicio que no tiene monto Afecto. Comuniquese con ATC");
                        return;
                    }
                    dInafecto = Math.Round(Convert.ToDecimal(GrvListadoAlmacenaje.DataKeys[GrvRow.RowIndex].Values["dInafecto"].ToString()), 2);
                    dIgv = Math.Round(Convert.ToDecimal(GrvListadoAlmacenaje.DataKeys[GrvRow.RowIndex].Values["dIgv"].ToString()), 2);
                    dTotal = Math.Round(Convert.ToDecimal(GrvListadoAlmacenaje.DataKeys[GrvRow.RowIndex].Values["dTotal"].ToString()), 2);

                    oBE_LiquidacionDet.dAfecto = dAfecto;
                    oBE_LiquidacionDet.dInafecto = dInafecto;
                    oBE_LiquidacionDet.dIgv = dIgv;

                    oBE_LiquidacionDet.dMontoNeto = dAfecto + dInafecto;
                    oBE_LiquidacionDet.dMontoBruto = dTotal;



                    oBE_LiquidacionDet.dDescuento = Convert.ToDecimal(GrvListadoAlmacenaje.DataKeys[GrvRow.RowIndex].Values["dDescuento"].ToString());
                    oBE_LiquidacionDet.dDetraccion = Convert.ToDecimal(GrvListadoAlmacenaje.DataKeys[GrvRow.RowIndex].Values["dDetraccion"].ToString());

                    oBE_LiquidacionDet.sOrigenPqteSLI = GrvListadoAlmacenaje.DataKeys[GrvRow.RowIndex].Values["sOrigenPqteSLI"].ToString();
                    oBE_LiquidacionDet.sMarcaSLI = GrvListadoAlmacenaje.DataKeys[GrvRow.RowIndex].Values["sMarcaSLI"].ToString();

                    oBE_LiquidacionDet.sUsuario = GlobalEntity.Instancia.Usuario;
                    oBE_LiquidacionDet.sNombrePc = GlobalEntity.Instancia.NombrePc;
                    oBE_LiquidacionDet.sIdDua = sIdDua;

                    if (oBE_LiquidacionDet.iIdOrdSerDet > 0)
                        oBE_LiquidacionList.Add(oBE_LiquidacionDet);
                }
            }


            int vl_iCodigo = 0;
            String vl_sMessage = string.Empty;
            String[] oResultadoTransaccionTx = oBL_Liquidacion.Insertar_PreLiquidacion_2(oBE_Liquidacion, oBE_LiquidacionList).Split('|');

            for (int i = 0; i < oResultadoTransaccionTx.Length; i++)
            {
                if (i == 0)
                {
                    vl_iCodigo = Convert.ToInt32(oResultadoTransaccionTx[i]);

                }
                else
                {
                    vl_sMessage += oResultadoTransaccionTx[i];
                }
            }

            if (vl_iCodigo < 1)
            {
                SCA_MsgInformacion(vl_sMessage);
                return;
            }
            else
            {
                SCA_MsgInformacion(vl_sMessage);
               // TxtIdPreLiquidacion.Text = vl_iCodigo.ToString();

               // ScriptManager.RegisterStartupScript(TxtIdPreLiquidacion, GetType(), "__mensaje__", String.Format("javascript: Imprimir('{0}');", TxtIdPreLiquidacion.Text), true);
            }

           // btnGrabar.Enabled = false;
        }
        catch (Exception ex)
        {
            //
        }
    }
}