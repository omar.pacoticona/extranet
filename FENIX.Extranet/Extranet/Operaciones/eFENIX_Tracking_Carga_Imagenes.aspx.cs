﻿using FENIX.BusinessEntity;
using FENIX.BusinessLogic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using Ionic.Zip;


public partial class Extranet_Documentacion_AutorizacionRetiroPDF : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string a = "";

        if (!Page.IsPostBack)
        {
            try
            {
                //verImagen("614051");
                ViewState["id"] = Request.QueryString["id"];

                //int IdCodigo = Convert.ToInt32(ViewState["IdCodigo"]);
                verImagen(ViewState["id"].ToString());
                //a = Convert.ToString(IdCodigo);
            }
            catch
            {
                //Page.ClientScript.RegisterStartupScript(Page.GetType(), "Message Box", "<script language = 'javascript'>alert('--');window.close();</script>");
            }

        }
    }

    void verImagen(string a)
    {

        Data data = new Data();
        ArrayList Lista = new ArrayList();

        List<Data> lstdata = new List<Data>();


        // string ruta = @"\\FSAFARGOLINE110\Image_AppTarja\";
        string ruta = ConfigurationManager.AppSettings["Ruta_ImgTarja"];
        string ruta0 = ruta + a + "/0.png";
        string ruta1 = ruta + a + "/1.png";
        string ruta2 = ruta + a + "/2.png";
        string ruta3 = ruta + a + "/3.png";
        string ruta4 = ruta + a + "/4.png";
        string ruta5 = ruta + a + "/5.png";
        string ruta6 = ruta + a + "/6.png";
        string ruta7 = ruta + a + "/7.png";
        string ruta8 = ruta + a + "/8.png";
        string ruta9 = ruta + a + "/9.png";
        string ruta10 = ruta + a + "/10.png";

        //DETALLE 


        string PrecintoAduana = ruta + a + "/Precintos/Aduana.png";
        string PrecintoCliente = ruta + a + "/Precintos/Cliente.png";
        string PrecintoConsolidador = ruta + a + "/Precintos/Consolidador.png";
        string PrecintoLinea = ruta + a + "/Precintos/Linea.png";
        string PrecintoOperador = ruta + a + "/Precintos/Operador.png";
        string PrecintoOtros = ruta + a + "/Precintos/Otros.png";





        byte[] imagen0 = GetImage(ruta0);
        byte[] imagen1 = GetImage(ruta1);       //comprobar que exsiste
        byte[] imagen2 = GetImage(ruta2);
        byte[] imagen3 = GetImage(ruta3);
        byte[] imagen4 = GetImage(ruta4);
        byte[] imagen5 = GetImage(ruta5);
        byte[] imagen6 = GetImage(ruta6);
        byte[] imagen7 = GetImage(ruta7);
        byte[] imagen8 = GetImage(ruta8);
        byte[] imagen9 = GetImage(ruta9);
        byte[] imagen10 = GetImage(ruta10);

        byte[] PrecintoA = GetImage(PrecintoAduana);
        byte[] PrecintoC = GetImage(PrecintoCliente);
        byte[] PrecintoCon = GetImage(PrecintoConsolidador);
        byte[] PrecintoL = GetImage(PrecintoLinea);
        byte[] PrecintoO = GetImage(PrecintoOperador);
        byte[] PrecintoOt = GetImage(PrecintoOtros);


        if (imagen0 != null)
        {
            data.Ruta = ruta + a + "/0.png";
            data.Titulo = "01";
            lstdata.Add(data);
            data = new Data();

        }

        if (imagen1 != null)
        {

            data.Ruta = ruta + a + "/1.png";
            data.Titulo = "02";
            lstdata.Add(data);
            data = new Data();

        }

        if (imagen2 != null)
        {
            data.Ruta = ruta + a + "/2.png";
            data.Titulo = "03";
            lstdata.Add(data);
            data = new Data();
        }

        if (imagen3 != null)
        {
            data.Ruta = ruta + a + "/3.png";
            data.Titulo = "04";
            lstdata.Add(data);
            data = new Data();
        }

        if (imagen4 != null)
        {
            data.Ruta = ruta + a + "/4.png";
            data.Titulo = "05";
            lstdata.Add(data);
            data = new Data();
        }

        if (imagen5 != null)
        {
            data.Ruta = ruta + a + "/5.png";
            data.Titulo = "06";
            lstdata.Add(data);
            data = new Data();
        }

        if (imagen6 != null)
        {
            data.Ruta = ruta + a + "/6.png";
            data.Titulo = "07";
            lstdata.Add(data);
            data = new Data();
        }

        if (imagen7 != null)
        {
            data.Ruta = ruta + a + "/7.png";
            data.Titulo = "08";
            lstdata.Add(data);
            data = new Data();
        }

        if (imagen8 != null)
        {
            data.Ruta = ruta + a + "/8.png";
            data.Titulo = "09";
            lstdata.Add(data);
            data = new Data();
        }

        if (imagen9 != null)
        {
            data.Ruta = ruta + a + "/9.png";
            data.Titulo = "10";
            lstdata.Add(data);
            data = new Data();
        }

        if (imagen10 != null)
        {
            data.Ruta = ruta + a + "/10.png";
            data.Titulo = "11";
            lstdata.Add(data);
            data = new Data();
        }

        if (PrecintoA != null)
        {
            data.Ruta = ruta + a + "/Precintos/Aduana.png";
            data.Titulo = "PRECINTO ADUANA";
            lstdata.Add(data);
            data = new Data();
        }

        if (PrecintoC != null)
        {
            data.Ruta = ruta + a + "/Precintos/Cliente.png";
            data.Titulo = "PRECINTO CLIENTE";
            lstdata.Add(data);
            data = new Data();
        }

        if (PrecintoCon != null)
        {
            data.Ruta = ruta + a + "/Precintos/Consolidador.png";
            data.Titulo = "PRECINTO CONSOLIDADOR";
            lstdata.Add(data);
            data = new Data();
        }

        if (PrecintoL != null)
        {
            data.Ruta = ruta + a + "/Precintos/Linea.png";
            data.Titulo = "PRECINTO LINEA";
            lstdata.Add(data);
            data = new Data();
        }

        if (PrecintoO != null)
        {
            data.Ruta = ruta + a + "/Precintos/Operador.png";
            data.Titulo = "PRECINTO OPERADOR";
            lstdata.Add(data);
            data = new Data();
        }

        if (PrecintoOt != null)
        {
            data.Ruta = ruta + a + "/Precintos/Otros.png";
            data.Titulo = "PRECINTO OTROS";
            lstdata.Add(data);
            data = new Data();
        }

        if (lstdata.Count() == 0)
        {
            Page.ClientScript.RegisterStartupScript(Page.GetType(), "Message Box", "<script language = 'javascript'>alert('No existen imagenes');window.close();</script>");
        }

        Repeater1.DataSource = lstdata;

        Repeater1.DataBind();


    }

    private byte[] GetImage(string url)
    {
        try
        {
            System.Net.WebClient _WebClient = new System.Net.WebClient();
            byte[] _image = _WebClient.DownloadData(url);
            if (_image.Length > 0)
                return _image;
            else
                return null;
        }
        catch (Exception)
        {
            return null;
        }
    }

    void descargarImgen(string a)
    {


    }

    protected void btn_cerrar_Click(object sender, EventArgs e)
    {
        DescargarZip(Response);
    }

    public void DescargarZip(System.Web.HttpResponse sResponse)
    {
        //string a = "22222";// ViewState["id"].ToString();
        //string ruta = @"C:\Users\Lenovo\Downloads\";

        //Response.Clear();
        //using (ZipFile zip = new ZipFile())
        //{
        //    zip.AddDirectory(ruta, a);
        //    zip.Comment = "Archivo comprimido el " + System.DateTime.Now.ToString("G");
        //    zip.Save(Response.OutputStream);
        //}
        //Response.AddHeader("Content-Disposition", "attachment; filename=MisDocumentos.zip");
        //Response.ContentType = "application/octet-stream";
        //Response.End();

    }
    public class Data
    {
        public string Ruta { get; set; }
        public string Titulo { get; set; }

    }

}