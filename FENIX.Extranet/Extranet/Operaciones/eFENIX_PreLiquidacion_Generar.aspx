﻿<%@ Page Language="C#" MasterPageFile="~/Extranet/Seguridad/eFENIX_Principal.master" AutoEventWireup="true" CodeFile="eFENIX_PreLiquidacion_Generar.aspx.cs" Inherits="Extranet_Operaciones_eFENIX_PreLiquidacion_Generar" %>



 <asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
      

      <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"/>
     <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
     <%-- <script src="https://code.jquery.com/jquery-1.12.4.js" type="text/javascript"></script>--%>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js" type="text/javascript"></script>
    <script src="../../Script/Java_Script/jsPreLiquidacion.js"></script>
    



    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

    </asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="CntMaster" runat="Server">

    <table cellspacing="0" cellpadding="0" style="width: 100%;">
        <tr>
            <td class="form_titulo" style="width: 85%">
                <asp:TextBox ID="LblTitulo" Text="Generare Pre - Liquidacion" ReadOnly="true" 
                    style="background-color: #0069ae; border: #0069ae; color: #FFFFFF; font-family: Calibri; font-size: 13px; font-weight: bold; text-transform: uppercase;" 
                    runat="server" Width="231px" ></asp:TextBox>
            </td> 
        </tr>

    </table>
   
    <table class="form_bandeja" style=" width: 100%; margin-top:5px;">
        <tr>
                        <td >&nbsp;&nbsp;&nbsp;&nbsp;  BL / Volante 
                        </td>
                        <td >
                            <asp:TextBox ID="txtDocumento" runat="server" Width="220px" Style="text-transform: uppercase;" autocomplete="off" Placeholder="VOLANTE O BOOKING"></asp:TextBox>
                        </td>

                        <td>
                            <asp:RadioButton ID="RbtImportacion" Text="Descarga" runat="server" />
                            <br />
                            <asp:RadioButton ID="RbtExportacion" Text="Embarque" runat="server" />
                        </td>

                        <td >Fecha Almacenaje
                        </td>
                        <td >
                            <input id="txtFin_datep" type="text" readonly maxlength="10" name="DatePickerFinname" class="inputText_extranet" />
                        </td>
                        <td>
                            <asp:CheckBox ID="ChkMasterLCL" runat="server" Text="Master LCL"
                                AutoPostBack="True" />
                        </td>


                        <td >
                            <asp:Button ID="ImgBtnBuscar" runat="server" Text="Consultar" class="btn btn-primary" BackColor="#0069ae" OnClick="ImgBtnBuscar_Click" />
                            &nbsp; 
                            <asp:Button ID="ImgBtnGrabar" runat="server" Text="Grabar" class="btn btn-primary" BackColor="#0069ae" OnClick="ImgBtnGrabar_Click" />
                        </td>

                    </tr>
                  
    </table>

     <table cellspacing="0" cellpadding="0" style="width: 100%; height:50px;">
         <tr style="height: 55px;">
            <td valign="top" colspan="2">
                <ajax:UpdatePanel ID="upCabecera" runat="server" UpdateMode="Conditional" >
                    <ContentTemplate>
                 <table class="form_bandeja" style=" width: 100%; height:50px;">
                   <tr>
                        
                        <td style="width: 12%">&nbsp; Linea Negocio
                        </td>
                        <td style="width: 3%;">
                            <asp:DropDownList ID="dplLineaNegocio" runat="server" Width="220px" Enabled="false">
                            </asp:DropDownList>
                        </td>
                        <td style="width: 12%;">

                        </td>
                        <td style="width: 12%;">&nbsp;&nbsp;&nbsp;&nbsp; Documento 
                        </td>
                        <td style="width: 13%;">
                            <asp:TextBox ID="txtsDocOrigen" runat="server" Width="220px" Enabled="false" Style="text-transform: uppercase; margin-left: 27;" autocomplete="off"></asp:TextBox>
                        </td>
                        <td style="width: 9%;">&nbsp;  Cliente 
                        </td>
                        <td style="width: 9%;">
                            <asp:TextBox ID="txtCliente" runat="server" Width="220px" Enabled="false" Style="text-transform: uppercase; margin-left: 27;" autocomplete="off"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 2%">&nbsp;&nbsp;  Cliente Acuerdo
                        </td>
                        <td style="width: 2%;">
                            <asp:TextBox ID="txtClienteAcuerdo" runat="server" Width="220px" Enabled="false" Style="text-transform: uppercase; margin-left: 27;" autocomplete="off"></asp:TextBox>
                        </td>
                        <td></td>
                        <td style="width: 2%">&nbsp;&nbsp;&nbsp;&nbsp; Nro Acuerdo
                        </td>
                        <td style="width: 2%;">
                            <asp:TextBox ID="TxtNroAcuerdo" runat="server" Width="220px" Enabled="false" Style="text-transform: uppercase; margin-left: 27;" autocomplete="off"></asp:TextBox>
                            <asp:TextBox ID="TxtIdAcuerdo" runat="server" Width="20px" Visible="false" Style="text-transform: uppercase; margin-left: 27;" autocomplete="off"></asp:TextBox>
                            <asp:TextBox ID="TxtIdMoneda" runat="server" Width="20px" Visible="false" Style="text-transform: uppercase; margin-left: 27;" autocomplete="off"></asp:TextBox>
                            <asp:TextBox ID="TxtIdCliente" runat="server" Width="20px" Visible="false" Style="text-transform: uppercase; margin-left: 27;" autocomplete="off"></asp:TextBox>
                        </td>
                        
                    </tr>
                    
                    </table>
                        </ContentTemplate>
                    </ajax:UpdatePanel>


                </td>

         </tr>

     </table>

       <table class="form_bandeja" style=" width: 100%; height: 20px; margin-top:5px;">
           <tr>
            <td class="form_titulo" style="width: 85%">
                <asp:TextBox ID="TextBox10" Text="Detalle de servicios" ReadOnly="true" 
                    style="background-color: #0069ae; border: #0069ae; color: #FFFFFF; font-family: Calibri; font-size: 13px; font-weight: bold; text-transform: uppercase;" 
                    runat="server" Width="231px" ></asp:TextBox>
            </td> 
           <%-- <td  style="width: 85%">
                 <asp:Button ID="btnCakcular"  runat="server" Text="Ver detalles de servicios" class="form-control" BackColor="#0069ae" ForeColor="White" OnClick="btnCakcular_Click"/>
            </td> --%>
        </tr>
         
         <tr>
              <td colspan="5">
                <ajax:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional" >
                    <ContentTemplate>                         
                        <div style="overflow: auto; width: 100%; height: 150px;">
                            <asp:GridView ID="GvListaServicios" runat="server" AutoGenerateColumns="False"
                                SkinID="GrillaConsulta" Width="100%"  DataKeyNames="iIdOrdSer,iIdOrdSerDet,iIdTarifa,dCantidad,dPeso,dIgv,dAfecto,dInafecto,dMontoBruto,dMontoNeto,dDescuento,dDetraccion,sSituacion,dMontoUnitario,dTotal,iIdDocOriDet,sOrigenPqteSLI,sMarcaSLI" >
                                <Columns>
                                    <asp:TemplateField>
                                                       <%-- <HeaderTemplate>
                                                            <asp:CheckBox runat="server" ID="chkAllQuitarGrp" onclick="SelectGroupChecks(this.id)"
                                                                AutoPostBack="True" />
                                                        </HeaderTemplate>--%>
                                                        <ItemTemplate>
                                                            <asp:CheckBox ID="chkSeleccionarServicio" runat="server"/>
                                                        </ItemTemplate>
                                                        <ItemStyle Width="1%" />
                                    </asp:TemplateField>
                                     <asp:BoundField DataField="iIdOrdSer" HeaderText="Nro OrdSer" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5px">                                       
                                    </asp:BoundField>
                                     <asp:BoundField DataField="iItem" HeaderText="Item" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5px">                                       
                                    </asp:BoundField>
                                     <asp:BoundField DataField="sDescripcionServicio" HeaderText="Servicio" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5px">                                       
                                    </asp:BoundField>
                                    <asp:BoundField DataField="sContenedor" HeaderText="Cont." ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5px">                                       
                                    </asp:BoundField>
                                     <asp:BoundField DataField="sCondCarga" HeaderText="IMO" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5px">                                       
                                    </asp:BoundField>
                                     <asp:BoundField DataField="sTipoCarga" HeaderText="Carga" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5px">                                       
                                    </asp:BoundField>
                                    <asp:BoundField DataField="sTipoContenedor" HeaderText="T. Cont." ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5px">                                       
                                    </asp:BoundField>
                                     <asp:BoundField DataField="iIdTamanoContenedor" HeaderText="Tamaño" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5px">                                       
                                    </asp:BoundField>
                                     <asp:BoundField DataField="sEmbalaje" HeaderText="Emb." ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5px">                                       
                                    </asp:BoundField>
                                    <asp:BoundField DataField="dMontoUnitario" HeaderText="P. Unit." ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5px">                                       
                                    </asp:BoundField>
                                     <asp:BoundField DataField="dCantidad" HeaderText="Cantidad" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5px">                                       
                                    </asp:BoundField>
                                     <asp:BoundField DataField="sTipoMoneda" HeaderText="Moneda" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5px">                                       
                                    </asp:BoundField>
                                    <asp:BoundField DataField="dAfecto" HeaderText="Afecto" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5px">                                       
                                    </asp:BoundField>
                                     <asp:BoundField DataField="dInafecto" HeaderText="Inafecto" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5px">                                       
                                    </asp:BoundField>
                                     <asp:BoundField DataField="dDescuento" HeaderText="Dscto." ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5px">                                       
                                    </asp:BoundField>
                                    <asp:BoundField DataField="dIgv" HeaderText="IGV" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5px">                                       
                                    </asp:BoundField>
                                     <asp:BoundField DataField="dTotal" HeaderText="Total" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5px">                                       
                                    </asp:BoundField>
                                     <asp:BoundField DataField="dDetraccion" HeaderText="Det." ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5px">                                       
                                    </asp:BoundField>
                                    <asp:BoundField DataField="sModalidad" HeaderText="Modalidad" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5px">                                       
                                    </asp:BoundField>
                                     <asp:BoundField DataField="sOrigen" HeaderText="Origen" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5px">                                       
                                    </asp:BoundField>
                                     <asp:BoundField DataField="dtFecha_Ingreso" HeaderText="F. Ingreso" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5px">                                       
                                    </asp:BoundField>                                 

                                </Columns>
                            </asp:GridView>
                        </div>
                    </ContentTemplate>    
                     <Triggers>                        
                     <ajax:PostBackTrigger ControlID="GvListaServicios" />            
                        <ajax:AsyncPostBackTrigger ControlID="ImgBtnBuscar" EventName="Click" />                      
                    </Triggers>
                </ajax:UpdatePanel>
            </td>                        
                     </tr>
          </table>
          <%--   </td>
             </tr>

          </table>--%>

     <table class="form_bandeja" style=" width: 100%;">
           <tr>
            <td class="form_titulo" style="width: 85%">
                <asp:TextBox ID="TextBox11" Text="Detalle de servicios de almacenaje" ReadOnly="true" 
                    style="background-color: #0069ae; border: #0069ae; color: #FFFFFF; font-family: Calibri; font-size: 13px; font-weight: bold; text-transform: uppercase;" 
                    runat="server" Width="231px" ></asp:TextBox>
            </td> 
        </tr>
         
          <tr valign="top" style="height: 70%; margin-top:10px;">
           <td colspan="5">
                <ajax:UpdatePanel ID="upDocumentoOrigen" runat="server" UpdateMode="Conditional" >
                    <ContentTemplate>                         
                        <div style="overflow: auto; width: 100%; height: 150px;">
                            <asp:GridView ID="GrvListadoAlmacenaje" runat="server" AutoGenerateColumns="False"
                                SkinID="GrillaConsulta" Width="50%"  DataKeyNames="iIdOrdSer,iIdOrdSerDet,iIdTarifa,dCantidad,dPeso,dIgv,dAfecto,dInafecto,dMontoBruto,dMontoNeto,dDescuento,dDetraccion,sSituacion,dMontoUnitario,dTotal,iIdDocOriDet,sOrigenPqteSLI,sMarcaSLI" >
                                <Columns>
                                     <asp:TemplateField>
                                                       <%-- <HeaderTemplate>
                                                            <asp:CheckBox runat="server" ID="chkAllQuitarGrp" onclick="SelectGroupChecks(this.id)"
                                                                AutoPostBack="True" />
                                                        </HeaderTemplate>--%>
                                                        <ItemTemplate>
                                                            <asp:CheckBox ID="chkSeleccionarAlmacenaje" runat="server"/>
                                                        </ItemTemplate>
                                                        <ItemStyle Width="1%" />
                                    </asp:TemplateField>
                                     <asp:BoundField DataField="iIdOrdSer" HeaderText="Nro OrdSer" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5px">                                       
                                    </asp:BoundField>
                                     <asp:BoundField DataField="iItem" HeaderText="Item" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5px">                                       
                                    </asp:BoundField>
                                     <asp:BoundField DataField="sDescripcionServicio" HeaderText="Servicio" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5px">                                       
                                    </asp:BoundField>
                                    <asp:BoundField DataField="sContenedor" HeaderText="Cont." ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5px">                                       
                                    </asp:BoundField>
                                     <asp:BoundField DataField="sCondCarga" HeaderText="IMO" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5px">                                       
                                    </asp:BoundField>
                                     <asp:BoundField DataField="sTipoCarga" HeaderText="Carga" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5px">                                       
                                    </asp:BoundField>
                                    <asp:BoundField DataField="sTipoContenedor" HeaderText="T. Cont." ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5px">                                       
                                    </asp:BoundField>
                                     <asp:BoundField DataField="iIdTamanoContenedor" HeaderText="Tamaño" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5px">                                       
                                    </asp:BoundField>
                                     <asp:BoundField DataField="sEmbalaje" HeaderText="Emb." ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5px">                                       
                                    </asp:BoundField>
                                    <asp:BoundField DataField="dMontoUnitario" HeaderText="P. Unit." ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5px">                                       
                                    </asp:BoundField>
                                     <asp:BoundField DataField="dCantidad" HeaderText="Cantidad" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5px">                                       
                                    </asp:BoundField>
                                     <asp:BoundField DataField="sTipoMoneda" HeaderText="Moneda" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5px">                                       
                                    </asp:BoundField>
                                    <asp:BoundField DataField="dAfecto" HeaderText="Afecto" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5px">                                       
                                    </asp:BoundField>
                                     <asp:BoundField DataField="dInafecto" HeaderText="Inafecto" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5px">                                       
                                    </asp:BoundField>
                                     <asp:BoundField DataField="dDescuento" HeaderText="Dscto." ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5px">                                       
                                    </asp:BoundField>
                                    <asp:BoundField DataField="dIgv" HeaderText="IGV" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5px">                                       
                                    </asp:BoundField>
                                     <asp:BoundField DataField="dTotal" HeaderText="Total" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5px">                                       
                                    </asp:BoundField>
                                     <asp:BoundField DataField="dDetraccion" HeaderText="Det." ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5px">                                       
                                    </asp:BoundField>
                                    <asp:BoundField DataField="sModalidad" HeaderText="Modalidad" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5px">                                       
                                    </asp:BoundField>
                                     <asp:BoundField DataField="sOrigen" HeaderText="Origen" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5px">                                       
                                    </asp:BoundField>
                                     <asp:BoundField DataField="dtFecha_Ingreso" HeaderText="F. Ingreso" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5px">                                       
                                    </asp:BoundField>                                 

                                </Columns>
                            </asp:GridView>
                        </div>
                    </ContentTemplate> 
                    <Triggers>
                     <ajax:PostBackTrigger ControlID="GrvListadoAlmacenaje" />            
                        <ajax:AsyncPostBackTrigger ControlID="ImgBtnBuscar" EventName="Click" />                      
                    </Triggers>
                </ajax:UpdatePanel>
            </td>
         </tr>
         </table>
    
     <table class="form_bandeja" style="width: 100%; margin-top: 3px;">
         <tr> 
             <td colspan="5">
                 <ajax:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional" >
                    <ContentTemplate> 
                    <table class="form_bandeja" style="width: 90%; margin-top: 1px; margin-left:10px;">
                      
                      <tr>
                         <%-- <ajax:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional" >
                    <ContentTemplate> --%>
                         <td style="width: 10%">Afecto  
                         </td>
                         <td style="width: 20px;">
                            <asp:Label ID="lblAfecto" Width="90px" runat="server" Text=" - "
                                 Visible="true" CssClass="lblComisionista"></asp:Label>
                         </td>
                     </tr>
                     <tr>
                         <td style="width: 10%">Descuento 
                         </td>
                         <td style="width: 13px;">
                              <asp:Label ID="lblDescuento"  Width="90px" runat="server" Text=" - "
                                 Visible="true" CssClass="lblComisionista"></asp:Label>
                         </td>
                     </tr>
                     <tr>
                         <td style="width: 10%">IGV 
                         </td>
                         <td style="width: 13px;">
                             <asp:Label ID="lblIgv"  Width="90px" runat="server" Text=" - "
                                 Visible="true" CssClass="lblComisionista"></asp:Label>
                         </td>
                     </tr>
                      <tr>
                         <td style="width: 9%">Total   
                         </td>
                         <td style="width: 20px;">
                            <asp:Label ID="lblTotal"  Width="90px" runat="server" Text=" - "
                                 Visible="true" CssClass="lblComisionista"></asp:Label>
                         </td>
                     </tr>
                     <tr>
                         <td style="width: 10%">Tipo de Cambio  
                         </td>
                         <td style="width: 80px;">
                             <asp:Label ID="Label6"   Width="90px" runat="server" Text=" -  "
                                 Visible="true" CssClass="lblComisionista"></asp:Label>
                         </td>
                     </tr>
                          
                      
                 </table>
                        </ContentTemplate>
                     </ajax:UpdatePanel>

              </td>

         </tr>
         </table>
        
        

     

    
    <script language="javascript" type="text/javascript">

        var tiempo = 0;
        var seconds = 0;

        var myVar = setInterval(function () {
            clearInterval(myVar);
        }, 1000);

        var myTiempo = setTimeout(function () {
            clearInterval(myTiempo);
        }, 1);

        function myStopFunction() {
            clearInterval(myVar);
            seconds = '<%=GlobalEntity.Instancia.TiempoDuracionSesion%>' * 60;
            tiempo = '<%=GlobalEntity.Instancia.TiempoDuracionSesion%>' * 1000 * 60;

            CuentaTiempo(tiempo);
        }

        function CuentaTiempo(tiempo) {
            clearTimeout(myTiempo);
            myTiempo = setTimeout(function () {
                document.getElementById('<%=btn_cerrar.ClientID%>').click();
                //window.location = "../Seguridad/eFENIX_Login.aspx";
            }, tiempo);
        }



        function SessionExpireAlert(timeout) {
            seconds = timeout / 1000;
            document.getElementsByName("seconds").innerHTML = seconds;

            myvar = setInterval(function () {
                seconds--;
                document.getElementById("seconds").innerHTML = seconds;
            }, 1000);

            CuentaTiempo(timeout);
        };


        var goodexit = false;
        window.onbeforeunload = confirmRegisterExit;
        function confirmRegisterExit(evt) {
            if (!goodexit) {
                //return "Si vas abandonar el sistema haz click en Cerrar Sesion!!";
                document.getElementById('<%=btn_cerrar.ClientID%>').click();
            }
        }

  
    </script>



    </asp:Content>


<asp:Content ID="Content3" ContentPlaceHolderID="ContentCab" Runat="Server">
        
        <span class="texto_titulo" >Tu Sesion Expirara en &nbsp; 
            <span class="texto_titulo" id="seconds"> </span>
            <span>&nbsp;seg.</span>
         </span>
         <asp:Button ID="btn_cerrar" runat="server" Text="cerrar" 
            onclick="btn_cerrar_Click"  Style="display: none;" />
    </asp:Content>