﻿<%@ Page Language="C#" 
MasterPageFile="~/Extranet/Seguridad/eFENIX_Principal.master"
AutoEventWireup="true" 
CodeFile="eFENIX_Volante_Solicitud.aspx.cs" 
Inherits="Extranet_Operaciones_eFENIX_Volante_Solicitud"
Title="Consulta de Volantes" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="CntMaster" runat="Server">

    <table cellspacing="0" cellpadding="0" style="width: 100%;">
        
        <tr>
            <td colspan="2" class="form_titulo" style="width: 85%">
                SOLICITUD DE VOLANTES
            </td>
            <td align="right" class="form_titulo_verde" style="width: 15%" >
                <ajax:UpdatePanel ID="UdpTotales" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <asp:TextBox ID="LblTotal" Text="Total de Registros:" style="background-color: #0069ae; color: #FFFFFF" ReadOnly="true" BorderColor="#0069ae" runat="server"></asp:TextBox>
                    </ContentTemplate>
                    <Triggers>
                        <ajax:AsyncPostBackTrigger ControlID="ImgBtnAgregar" EventName="Click" />
                        <ajax:AsyncPostBackTrigger ControlID="ImgBtnLimpiar" EventName="Click" />
                    </Triggers>
                </ajax:UpdatePanel>
            </td>               
        </tr>
        
        <tr>
            <td valign="top" colspan="3">
                <table class="form_bandeja" style="margin-top: 5px; margin-bottom: 5px; width: 100%;">
                    <tr>
                        <td style="width: 10%;">
                            Cliente
                        </td>
                        <td colspan="3" style="width: 40%;">
                            <asp:TextBox ID="TxtCliente" runat="server" ReadOnly="true" BackColor="#ECE9D8" Style="text-transform: uppercase"
                                Width="414px"></asp:TextBox>
                        </td>
                        <td colspan="2" style="width: 15%;">
                            &nbsp;Ag.Aduana
                        </td>
                        <td style="width: 35%;">
                            <asp:DropDownList ID="DdlAgencia" runat="server" Width="300px" CssClass="texto_extranet" AppendDataBoundItems="true">
                            <asp:ListItem Value="0">--Seleccione Item--</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 10%;">
                            DOC.ORIGEN</td>
                        <td colspan="3" style="width: 40%;">
                            <ajax:UpdatePanel ID="UpdTextoDocumento" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <asp:TextBox ID="TxtDocumentoM" runat="server" MaxLength="25" Style="text-transform: uppercase"
                                        Width="150px"></asp:TextBox>
                                    &nbsp;&nbsp;
                                    <asp:ImageButton ID="ImgBtnAgregar" runat="server" 
                                        ImageUrl="~/Script/Imagenes/IconButton/Agregar.png" 
                                        OnClick="ImgBtnAgregar_Click" OnClientClick="return ValidarFiltro();" 
                                        ToolTip="Agregar a Lista" />
                                    &nbsp;&nbsp;
                                    <asp:ImageButton ID="ImgBtnLimpiar" runat="server" Height="16px" 
                                        ImageUrl="~/Script/Imagenes/IconButton/LimpiarLista.png" 
                                        OnClick="ImgBtnLimpiar_Click" ToolTip="Limpiar Lista" />
                                </ContentTemplate>
                                <Triggers>
                                    <ajax:AsyncPostBackTrigger ControlID="ImgBtnAgregar" EventName="Click" />
                                    <asp:AsyncPostBackTrigger ControlID="ImgBtnAgregar" EventName="Click"></asp:AsyncPostBackTrigger>
                                </Triggers>
                            </ajax:UpdatePanel>
                        </td>
                        <td colspan="2" style="width: 15%;">
                            &nbsp;
                            <asp:ImageButton ID="ImgBtnBuscar" runat="server" ImageUrl="~/Imagenes/Formulario/btn_grabar.JPG"
                                OnClick="ImgBtnBuscar_Click"/>
                            <asp:ImageButton ID="ImgBtnPdf" runat="server" ImageUrl="~/Imagenes/Formulario/btn_grabar.JPG" Style="display: none;" 
                                OnClick="ImgBtnPdf_Click"/>
                            </td>
                        <td style="width: 35%;">
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td style="width: 10%;">
                            &nbsp;
                        </td>
                        <td style="width: 10%;">
                            
                        </td>
                        <td style="width: 5%;" align="left">
                            &nbsp;</td>
                        <td colspan="2" style="width: 15%;">                            
                            &nbsp;
                        </td>
                        <td style="width: 50%;" colspan="2">
                            &nbsp;
                        </td>
                    </tr>
                </table>
             </td>   
        </tr>
    

        <tr valign="top" style="height: 100%; width: 100%;">
            <td style="width: 30%;">
                <table class="form_alterno" style="margin-top: 5px; margin-bottom: 5px; width: 100%;">
                    <tr>
                        <td colspan="2">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td valign="top" style="width: 30%;">
                            Listado de BLS
                        </td>
                        <td style="width: 70%;">
                            <ajax:UpdatePanel ID="UpdLista" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <asp:ListBox id="LstDocumentos" runat="server" SelectionMode="Single" Height="250px" Width="220px" CssClass="texto_extranet" AutoPostBack="false">

                                    </asp:ListBox>
                                </ContentTemplate>
                                <Triggers>
                                    <ajax:AsyncPostBackTrigger ControlID="ImgBtnAgregar" EventName="Click" />
                                    <ajax:AsyncPostBackTrigger ControlID="ImgBtnLimpiar" EventName="Click" />
                                </Triggers>
                            </ajax:UpdatePanel>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                        <td>
                            <asp:Label ID="LblAviso" runat="server" Width="220px" ></asp:Label>
                        </td>
                    </tr>
                </table>
            </td>
            <td colspan="2" style="width: 70%;">
                <table class="form_alterno" style="margin-top: 5px; margin-bottom: 5px; width: 100%;">
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <ajax:UpdatePanel ID="upDocumentoOrigen" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <div style="overflow: auto; width: 700px; height: 350px">
                                        <asp:GridView ID="GrvListado" runat="server" AutoGenerateColumns="False" DataKeyNames="iIdDocOri,iIdVolante,sVolante,sDocumentoHijo,sComentario"
                                            SkinID="GrillaNormal" Width="100%" OnRowCommand="GrvListado_RowCommand">
                                            <Columns>
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="chkSeleccionar" runat="server"/>
                                                    </ItemTemplate>
                                                    <HeaderStyle BackColor="#0069ae" Width="3%" />
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="sDocumentoHijo" HeaderText="Doc. Origen" ItemStyle-HorizontalAlign="Left"></asp:BoundField>
                                                <asp:BoundField DataField="sComentario" HeaderText="Comentario" ItemStyle-HorizontalAlign="Left"></asp:BoundField>
                                                <asp:BoundField DataField="sVolante" HeaderText="Nro. Volante" ItemStyle-HorizontalAlign="Center"></asp:BoundField>
                                                <asp:ButtonField ButtonType="Image" HeaderText="Ver" CommandName="OpenVerVolante" ImageUrl="~/Script/Imagenes/IconButton/Visualizar.png"
                                                        Text="Ver Volante" ItemStyle-HorizontalAlign="Left"></asp:ButtonField>
                                            </Columns>
                                        </asp:GridView>
                                    </div>
                                </ContentTemplate>
                                <Triggers>
                                    <%--<ajax:AsyncPostBackTrigger ControlID="GrvListado" EventName="SelectedIndexChanged" />--%>
                                    <ajax:AsyncPostBackTrigger ControlID="ImgBtnBuscar" EventName="Click" />
                                    <ajax:AsyncPostBackTrigger ControlID="dnvListado" EventName="PageChanged" />
                                    <ajax:AsyncPostBackTrigger ControlID="dnvListado" EventName="NextPage" />
                                    <ajax:AsyncPostBackTrigger ControlID="dnvListado" EventName="PreviousPage" />
                                    <ajax:AsyncPostBackTrigger ControlID="dnvListado" EventName="FirstPage" />  
                                    <ajax:AsyncPostBackTrigger ControlID="dnvListado" EventName="LastPage" />     
                                </Triggers>
                            </ajax:UpdatePanel>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr valign="top" style="height: 6%;">
            <td colspan="2">
               <ajax:UpdatePanel ID="upDnvListado" runat="server">
                    <ContentTemplate>
                        <FENIX:DataNavigator ID="dnvListado" runat="server" AllowCustomPaging="True" ButtonText="Ir" 
                            ForeColor="White" Font-Size="11px" BackColor="#2BA143" 
                            CurrentPage="1" CustomClientFunction="" EnableAskingAfterPage="False" EnableCustomScript="False"
                            GridViewId="GrvListado" ImagesPath="~/Imagenes/DN/" IsParentShowWindow="False"
                            MessageAskingAfterPage="" PageIndicatorFormat="Página {0} / {1}" Visible="False"
                            Width="100%" WidthButtonIr="" WidthTextBoxNumPagina="25px" />
                    </ContentTemplate>
                    <Triggers>
                        <ajax:AsyncPostBackTrigger ControlID="dnvListado" EventName="PageChanged" />
                        <ajax:AsyncPostBackTrigger ControlID="ImgBtnBuscar" EventName="Click" />
                    </Triggers>
                </ajax:UpdatePanel>
            </td>
        </tr>
    </table>

    
    <asp:HiddenField ID="HdIdDocOri" runat="server" />
    <asp:HiddenField ID="HdIdVolante" runat="server" />


    <script language="javascript" type="text/javascript">

        var tiempo = 0;
        var seconds = 0;

        var myVar = setInterval(function () {
            clearInterval(myVar);
        }, 1000);

        var myTiempo = setTimeout(function () {
            clearInterval(myTiempo);
        }, 1);

        function myStopFunction() {
            clearInterval(myVar);
            seconds = '<%=GlobalEntity.Instancia.TiempoDuracionSesion%>' * 60;
            tiempo = '<%=GlobalEntity.Instancia.TiempoDuracionSesion%>' * 1000 * 60;

            CuentaTiempo(tiempo);
        }

        function CuentaTiempo(tiempo) {
            clearTimeout(myTiempo);
            myTiempo = setTimeout(function () {
                document.getElementById('<%=btn_cerrar.ClientID%>').click();
                //window.location = "../Seguridad/eFENIX_Login.aspx";
            }, tiempo);
        }



        function SessionExpireAlert(timeout) {
            seconds = timeout / 1000;
            document.getElementsByName("seconds").innerHTML = seconds;

            myvar = setInterval(function () {
                seconds--;
                document.getElementById("seconds").innerHTML = seconds;
            }, 1000);

            CuentaTiempo(timeout);
        };


        var goodexit = false;
        window.onbeforeunload = confirmRegisterExit;
<%--        function confirmRegisterExit(evt) {
            if (!goodexit) {
                //return "Si vas abandonar el sistema haz click en Cerrar Sesion!!";
                document.getElementById('<%=btn_cerrar.ClientID%>').click();
            }
        }--%>

        var objFilaAnt = null;
        var backgroundColorFilaAnt = "";

        function fc_SeleccionaFilaSimple(objFila, objrowIndex, chkID) {
            try {

                if (objFilaAnt != null) {
                    objFilaAnt.style.backgroundColor = backgroundColorFilaAnt;
                }
                objFilaAnt = objFila;
                backgroundColorFilaAnt = objFila.style.backgroundColor;
                objFila.style.backgroundColor = "#c4e4ff";
            }

            catch (e) {
                error = e.message;
            }
        }

        function HabilitarUno(chk) {
            f = document.forms[0].elements;
            for (x = 0; x < f.length; x++) {
                obj = f[x];
                if (obj.type == 'checkbox') {
                    obj.checked = false;
                }
            }
            chk.enabled;
            chk.checked = true;
        }
        
        function fc_PressKeyDO() {
            if (event.keyCode == 13) {
                document.getElementById("<%=ImgBtnAgregar.ClientID%>").click();
                return false;
            }
            else {
                return true;
            }
        }

        function ValidarFiltro() {
            var sCriterio = document.getElementById('<%=TxtDocumentoM.ClientID %>').value;

            if ((fc_Trim(sCriterio) == "")) {
                alert('Debe ingresar el Documento de Origen (BL)');
                return false;
            }
            else {
                return true;
            }
        }

        function VerVolante(sIdDocOri, sIdVolante) {
            document.getElementById('<%=HdIdDocOri.ClientID %>').value = sIdDocOri;
            document.getElementById('<%=HdIdVolante.ClientID %>').value = sIdVolante;

            document.getElementById("<%=ImgBtnPdf.ClientID%>").click();
        }
    </script>
</asp:Content>

<asp:Content ID="Content3" runat="server" contentplaceholderid="ContentCab">
    <span class="texto_titulo" >Tu Sesion Expirara en &nbsp; 
        <span class="texto_titulo" id="seconds"> </span>
        <span>&nbsp;seg.</span>
     </span>
     <asp:Button ID="btn_cerrar" runat="server" Text="cerrar" 
        onclick="btn_cerrar_Click"  Style="display: none;" />
</asp:Content>
