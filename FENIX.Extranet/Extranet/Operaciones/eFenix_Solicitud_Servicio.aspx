﻿<%@ Page Language="C#"
    MasterPageFile="~/Extranet/Seguridad/eFENIX_Principal.master"
    AutoEventWireup="true"
    CodeFile="eFenix_Solicitud_Servicio.aspx.cs" 
    Inherits="Operaciones.eFenix_Solicitud_Servicio" 
    Title="Solicitud de Orden de Servicio"
    CodeBehind="eFenix_Solicitud_Servicio.aspx.cs"
    EnableEventValidation="true"
    %>



<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
     <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
    <title></title>

   <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"/>
     <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
     <%-- <script src="https://code.jquery.com/jquery-1.12.4.js" type="text/javascript"></script>--%>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js" type="text/javascript"></script>
    <<script src="../../Script/Java_Script/jsSolicitudServicios.js"></script>
    <%--<script src="https://www.jquery-steps.com/Scripts/jquery.steps.min.js"></script>--%>



  <%-- <script src="../../Script/Java_Script/jquery.steps.js" type="text/javascript"></script>--%>

  

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="CntMaster" runat="Server">
    <!DOCTYPE html>
    
<html>


<body>

    


    <div class="form_titulo" style="width: 100%; height:23px;vertical-align: inherit">
        <p style="margin-top: 1.5px;font-size: 14px;">Solicitud de Servicios</p>
    </div>
    <div id="tabs-container">
    <form action="submit" id="frmServicios">
  <ul class="tabs">
    <li><a href="#tabs-1">1. Seleccione Carga</a></li>
    <li><a href="#tabs-2">2. Seleccione Servicios</a></li>
    <li><a href="#tabs-3">3. Confirmar Solicitud</a></li>
  </ul>
  <div id="tabs-1">
    
              <fieldset  style="width:100%;background-color:white;">
      <legend style="background-color:#0069AE;font-weight:700;">Datos de la Solicitud</legend>
                  <div id="divBusqueda">
                     <table class="form_bandeja" style="margin-top: 5px; margin-bottom: 5px; width: 100%;">
                         <tr> <td><asp:Label runat="server">Operacion</asp:Label></td>
                             <td><asp:DropDownList ID="dplTipoOperacion" runat="server"></asp:DropDownList></td>
                         </tr>
                         <tr>
                             <td><asp:Label runat="server">BL / BK</asp:Label></td>
                             <td> <asp:TextBox  ID="TxtDocumentoM" runat="server" Style="text-transform: uppercase"
                                Width="135px"></asp:TextBox></td>
                             <td><asp:Label runat="server">Volante</asp:Label></td>
                            <td><asp:TextBox ID="TxtVolante" runat="server" MaxLength="15" onKeyPress="return isNumberKey(event)" Style="text-transform: uppercase"
                                Width="125px"></asp:TextBox></td>
                             <td style="width: 9px">
                            &nbsp;
                            </td>
                        <td>
                            <asp:ImageButton ID="ImgBtnBuscar" runat="server" ImageUrl="~/Imagenes/Formulario/btn_consultar.JPG"
                                OnClick="ImgBtnBuscar_Click" OnClientClick="return ValidarFiltro();"/>
                            &nbsp;&nbsp;
                            <asp:ImageButton ID="ImgBtnNuevo" runat="server" ImageUrl="~/Imagenes/Formulario/btn_nuevo.JPG"
                                OnClick="ImgBtnNuevo_Click"/>
                        </td>
                         </tr>
                    <%--  --%>

            
         

                     </table>   

                  </div>
    
     </fieldset>
          <br />
     <fieldset  style="width:100%;background-color:white;">
     <legend style="background-color:#0069AE;font-weight:700;">Detalle de Carga</legend>
         <table>
         <tr valign="top" style="height: 50%;width:99%;">
            <td colspan="4">
                <ajax:UpdatePanel ID="upDocumentoOrigen"  runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <%--<div style="overflow: scroll; overflow-x:auto; width:99%; height:50%;">--%>
                            <asp:Panel ID="Panel1" runat="server" BackColor="White" Height="220px" 
                                                                 ScrollBars="Vertical" Width="99%">

                            
                            <asp:GridView ID="GrvListado" runat="server" AutoGenerateColumns="False" DataKeyNames="iIdDocOriDet,sNumeroDO,iVolante"
                                SkinID="GrillaConsulta" Width="100%" >
                                <Columns>
                                    <asp:TemplateField>                                        
                                        <ItemTemplate>
                                           

                                            <input id="chkSeleccionar" value="<%# Eval("iIdDocOriDet") %>" type="checkbox"/>


                                        </ItemTemplate>
                                        <HeaderStyle BackColor="#0069ae" Width="20px" />
                                        <ItemStyle Width="20px"></ItemStyle>
                                    </asp:TemplateField>
                                                                
                                    <asp:BoundField DataField="sNumeroDO" HeaderText="Documento"/>
                                    <asp:BoundField DataField="iVolante" HeaderText="Volante"/>
                                    <asp:BoundField DataField="iItem" HeaderText="Item"/>
                                     <asp:BoundField DataField="sCarga" HeaderText="CTN/CHS/CS"/>
                                     <asp:BoundField DataField="sTamano" HeaderText="Tamaño"/> 
                                    <asp:BoundField DataField="sTipo" HeaderText="Tipo"/>
                                    <asp:BoundField DataField="sCondicFacturar" HeaderText="Condicion"/>
                                    <asp:BoundField DataField="dBultos" HeaderText="Bultos"/>
                                    <asp:BoundField DataField="dPeso" HeaderText="Peso"/>                                                                 
                                </Columns>
                            </asp:GridView>    
                                </asp:Panel>
                        <%--</div>--%>
                 
                    </ContentTemplate>
                    <Triggers>
                        <%--<ajax:PostBackTrigger ControlID="GrvListado"/>--%>
                        <ajax:AsyncPostBackTrigger ControlID="ImgBtnBuscar" EventName="Click" />
                        <ajax:AsyncPostBackTrigger ControlID="dnvListado" EventName="PageChanged" />
                        <ajax:AsyncPostBackTrigger ControlID="dnvListado" EventName="NextPage" />
                        <ajax:AsyncPostBackTrigger ControlID="dnvListado" EventName="PreviousPage" />
                        <ajax:AsyncPostBackTrigger ControlID="dnvListado" EventName="FirstPage" />  
                        <ajax:AsyncPostBackTrigger ControlID="dnvListado" EventName="LastPage" />     
                    </Triggers>
                </ajax:UpdatePanel>

            </td>
         

              <%-- <td colspan="3"><asp:Label runat="server">Fecha Programacion</asp:Label></td>
               <td colspan="3"> <asp:TextBox  ID="TextBox1" readonly runat="server" MaxLength="10" Style="text-transform: uppercase"
                Width="135px"></asp:TextBox></td>

               <td><asp:Label runat="server">Operacion</asp:Label></td>
               <td><asp:DropDownList ID="dplServicio" runat="server"></asp:DropDownList></td>--%>
                         
           

        </tr>           
        <tr valign="top" style="height: 6%;">
            <td colspan="4">
               <ajax:UpdatePanel ID="upDnvListado" runat="server">
                    <ContentTemplate>
                        <FENIX:DataNavigator ID="dnvListado" runat="server" AllowCustomPaging="True" ButtonText="Ir" 
                            ForeColor="White" Font-Size="11px" BackColor="#0069ae" 
                            CurrentPage="1" CustomClientFunction="" EnableAskingAfterPage="False" EnableCustomScript="False"
                            GridViewId="GrvListado" ImagesPath="~/Imagenes/DN/" IsParentShowWindow="False"
                            MessageAskingAfterPage="" PageIndicatorFormat="Página {0} / {1}" Visible="False"
                            Width="99%" WidthButtonIr="" WidthTextBoxNumPagina="25px" />
                    </ContentTemplate>
                    <Triggers>
                        <ajax:AsyncPostBackTrigger ControlID="dnvListado" EventName="PageChanged" />
                        <ajax:AsyncPostBackTrigger ControlID="ImgBtnBuscar" EventName="Click" />
                    </Triggers>
                </ajax:UpdatePanel>
            </td>
        </tr>
         </table>

     </fieldset>
          <br />
     <fieldset style="width:100%;background-color:white;">
         <table>
             <tr>
                 <td>
                     <asp:Label runat="server">Servicio</asp:Label></td>
                 <td>
                     <asp:DropDownList ID="dplServicio" runat="server"></asp:DropDownList></td>
                 <td></td>
                 <td colspan="3">
                     <asp:Label runat="server">Fecha Programacion</asp:Label></td>
                 <td colspan="3">
                     <asp:TextBox ID="txtP_datepicker" ReadOnly runat="server" MaxLength="10" Style="text-transform: uppercase"
                         Width="135px"></asp:TextBox></td>
                 <td>
                     <asp:Image runat="server" ImageUrl="~/Imagenes/calendario.gif" /></td>
                 <td></td>
                 <td>
                     <asp:Label ID="lblDAM" runat="server"> DAM</asp:Label>
                 </td>
                 <td>
                     <asp:TextBox ID="txtDetalleServicio" runat="server" Style="text-transform: uppercase"
                         MaxLength="6" onKeyPress="return isNumberKey(event)"
                         Width="150px">
                     </asp:TextBox>
                 </td>
             </tr>
             <tr>
                 <td>&nbsp;</td>
             </tr>

         </table>
         <table>
              <tr>
                 
              <td>
                  
                  <fieldset id="fsMotivo" style="background-color:white;font-weight:normal;font-size:9px;">
                      <legend style="background-color:white;color:#00549a;font-weight:600;text-align:left; ">Motivo</legend>
                      <div>
                           <asp:CheckBox id="chkInspeccion" runat="server" Text="Inspeccion" style="width:100px;vertical-align: middle"/> 
                      </div>
                     <div>
                          <asp:CheckBox id="chkVentilacion" runat="server" Text="Ventilacion" onClick="ckChange()" style="vertical-align: middle"/> 
                     </div>
                     <div>
                         <asp:CheckBox id="chkFumigacion"  runat="server" Text="Fumigacion" onClick="ckChange()" style="vertical-align: middle"/>
                     </div>
                      <br />
                 
                      </fieldset>

                  
                  
                  <fieldset id="fsTipoCarga" style="background-color:white;font-weight:normal;font-size:9px;">
                      <legend style="background-color:white;color:#00549a;font-weight:600;text-align:left; ">Tipo Carga</legend>
                      <asp:RadioButtonList ID="rbtnTipoCargaList" runat="server"> 
                          <asp:ListItem>Estandar</asp:ListItem>
                          <asp:ListItem>Especial</asp:ListItem>

                      </asp:RadioButtonList>
                    <%--  <div>
                          <asp:RadioButton ID="rbEstandar" Visible="false" runat="server" Text="Estandar"/>
                      </div>
                      <div>
                          <asp:RadioButton ID="rbEspecial" Visible="false" runat="server" Text="Especial"/>
                      </div>--%>
                      <br />
                  </fieldset>
                  
              </td>
              <td>
                  <fieldset id="fsDev" style="background-color:white;font-weight:normal;font-size:9px;">
                      <legend style="background-color:white;color:#00549a;font-weight:600;text-align:left; ">Devuelve Contenedor</legend>
                      <div>
                          <asp:RadioButtonList ID="rbtnListDevContenedor" runat="server">
                              <asp:ListItem>Cliente</asp:ListItem>
                              <asp:ListItem>Fargoline</asp:ListItem>
                          </asp:RadioButtonList>
                        <%--  <asp:RadioButton ID="rbCliente"  Visible="false" runat="server" Text="Cliente"/>
                          <asp:RadioButton ID="rbFargoline" Visible="false" runat="server" Text="Fargoline"/>--%>
                      </div>
                      <br />
                      <div >
                          <asp:Label runat="server" Visible="false" Text="Fecha Devol."></asp:Label>
                        <asp:TextBox  ID="txtD_datepicker" Visible="false" readonly runat="server" MaxLength="10" Style="text-transform: uppercase"
                          Width="75px"></asp:TextBox>&nbsp;&nbsp;<asp:Image id="imgCalendarDev" Visible="false" runat="server" style="vertical-align:middle;" ImageUrl="~/Imagenes/calendario.gif" />
                      </div>
                      <br />
                  </fieldset>
              </td>
              <td>
                  
              </td>
          </tr>     

         </table>
         <table  width="100%">
             <tr>
                 <td>
                  <asp:ImageButton ID="btnTab1" runat="server" autopostback="true" ImageUrl="~/Imagenes//btnNext.png"
                                 CssClass="btnNext" ToolTip="Siguiente" OnClientClick="return ListarServicioAdicional();"
                       ImageAlign="Right"/>                    
                 </td>
                  
              </tr>
         </table>

      </fieldset>     
    
  </div>
  <div id="tabs-2">
      <table width="100%">
          <tr>
              <td>
                         <fieldset  style="width:100%;background-color:white;">
      <legend style="background-color:#0069AE;font-weight:700;">Datos de la Solicitud</legend>
                  <div id="divDetalles">
                     <table class="form_bandeja" style="margin-top: 5px; margin-bottom: 5px; width: 100%;">                       
                         <tr>
                             <td><asp:Label runat="server">BL / BK</asp:Label></td>
                             <td> <asp:TextBox  ID="txtDOSeleccionado" readonly runat="server" Style="text-transform: uppercase"
                                Width="175px"></asp:TextBox></td><td></td>                            
                             <td><asp:Label runat="server">Volante</asp:Label></td>
                            <td><asp:TextBox ID="txtVOSeleccionado" readonly runat="server" MaxLength="15" onKeyPress="return isNumberKey(event)" Style="text-transform: uppercase"
                                Width="125px"></asp:TextBox></td>
                             <td style="width: 9px">
                            &nbsp;
                            </td>                       
                         </tr>
                           <tr>
                   <td><asp:Label runat="server">Servicio</asp:Label></td>
               <td><asp:DropDownList ID="dplServicioSeleccionado" readonly runat="server"></asp:DropDownList></td>
                   <td></td>
              <td><asp:Label runat="server">Fecha</asp:Label></td>
               <td colspan="3"> <asp:TextBox  ID="datepickerSeleccionado" readonly runat="server" MaxLength="10" Style="text-transform: uppercase"
                Width="125px"></asp:TextBox></td>
                  
                  <td></td>                 
              
         </tr>
                     </table>   

                  </div>
                             <div>
                                 <div id="dvCargaSeleccionada" style="height:220px;width:99%;">
                                     <fieldset>
                                         <legend style="background-color:#0069AE;font-weight:700;">Carga seleccionada</legend>
                                         <table id="tblCargaSeleccionada" cellspacing="1" cellpadding="1" border="0"
                                             style="float:left; background-color:white;border-width:0px;width:100%;">
                                             <tbody id="tbdCargaSeleccionada">
                                                 <tr class="CabeceraGrilla" style="color:white;border-style:none;">   
                                                     <th scope="col" style="background-color:#0069AE;width:20px;">&nbsp;</th>
                                                     <th scope="col">CNT / CHS</th>
                                                     <th scope="col">Tamaño</th>
                                                     <th scope="col">Tipo</th>
                                                     <th scope="col">Condicion</th>
                                                     <th scope="col">Bultos</th>
                                                     <th scope="col">Peso</th>
                                                 </tr>
                                             </tbody>

                                         </table>
                                     </fieldset>
                                 </div>
                                 <div>
                                     <br />
                                 </div>
                                 
                             
                                 <div id="dvServiciosAdicionales">
                                      <fieldset>
                                     <legend style="background-color:#0069AE;font-weight:700;">Seleccione servicios operativos a utilizar</legend>
                                     <table id="tblServiciosAdicionales">
                                         <tr id="TrServiciosAdicionales">
                                           <%--  <td>
                                                 <div>
                                                     <asp:CheckBox id="chk122" runat="server" Text="Cuadrilla" style="width:100px;vertical-align: middle"/>

                                                 </div>
                                                 <div>
                                                     <asp:CheckBox id="chk123" runat="server" Text="Etiquetados" style="width:100px;vertical-align: middle"/>

                                                 </div>     
                                                 <div>
                                                     <asp:CheckBox id="chk124" runat="server" Text="Forrado con Absorbentes" style="width:100px;vertical-align: middle"/>

                                                 </div>
                                                 <div>
                                                     <asp:CheckBox id="chk125" runat="server" Text="Forrado con Papel" style="width:100px;vertical-align: middle"/>

                                                 </div>
                                                 <div>
                                                     <asp:CheckBox id="chk126" runat="server" Text="Montacargas 2.5 TM" style="width:100px;vertical-align: middle"/>

                                                 </div>  

                                             </td>
                                             <td>
                                                 <div>
                                                     <asp:CheckBox id="chk127" runat="server" Text="Montacargas 15.0 TM" style="width:100px;vertical-align: middle"/>

                                                 </div>
                                                 <div>
                                                     <asp:CheckBox id="chk128" runat="server" Text="Montacargas 5.0 TM" style="width:100px;vertical-align: middle"/>

                                                 </div>
                                                 <div>
                                                     <asp:CheckBox id="chk129" runat="server" Text="Paletizado Y/O Enzunchado" style="width:100px;vertical-align: middle"/>

                                                 </div>
                                                  <div>
                                                     <asp:CheckBox id="CheckBox1" runat="server" Text="Precintado" style="width:100px;vertical-align: middle"/>

                                                 </div>
                                                  <div>
                                                     <asp:CheckBox id="CheckBox2" runat="server" Text="Trincado Y/O Destrincado" style="width:100px;vertical-align: middle"/>

                                                 </div>

                                             </td>--%>
                                    
                                    
                                         </tr>
                                     </table>
                                       
                                 </fieldset>
                                 </div>
                                
                                 <ul id="reservationsList" class="stripedList"></ul>

                             </div>
    
     </fieldset>    
              </td>
          </tr>
      </table>
      <table width="100%">
          <tr>
              <td>
                  <asp:ImageButton ID="btnTab2P" runat="server" ImageUrl="~/Imagenes//btnNext.png"
                                CssClass="btnNext1" ToolTip="Siguiente" OnClientClick="return ListarCargaConfirmada();"
                       ImageAlign="Right"/>
                
                  <asp:ImageButton ID="btnTab2N" runat="server" ImageUrl="~/Imagenes//btnPrevious.png"
                                 CssClass="btnPrev" OnClientClick="return GoBack();"
                       ImageAlign="Left" ToolTip="Previo"/>                  
              </td>
             
          </tr>
      </table>
      
  </div>

  <div id="tabs-3">
       <fieldset  style="width:100%;background-color:white;">
      <legend style="background-color:#0069AE;font-weight:700;">Datos de la Solicitud</legend>
                  <div id="divDetallesConfirm">
                     <table class="form_bandeja" style="margin-top: 5px; margin-bottom: 5px; width: 100%;">                       
                         <tr>
                             <td><asp:Label runat="server">BL / BK</asp:Label></td>
                             <td> <asp:TextBox  ID="txtDOConfirmado" readonly runat="server" Style="text-transform: uppercase"
                                Width="175px"></asp:TextBox></td><td></td>                            
                             <td><asp:Label runat="server">Volante</asp:Label></td>
                            <td><asp:TextBox ID="txtVolanteConfirmado" readonly runat="server" MaxLength="15" onKeyPress="return isNumberKey(event)" Style="text-transform: uppercase"
                                Width="125px"></asp:TextBox></td>
                             <td style="width: 9px">
                            &nbsp;
                            </td>                       
                         </tr>
                           <tr>
                   <td><asp:Label runat="server">Servicio</asp:Label></td>
               <td><asp:DropDownList ID="dplServicioConfirmado" readonly runat="server"></asp:DropDownList></td>
                   <td></td>
              <td><asp:Label runat="server">Fecha</asp:Label></td>
               <td colspan="3"> <asp:TextBox  ID="datepickerConfirmado" readonly runat="server" MaxLength="10" Style="text-transform: uppercase"
                Width="125px"></asp:TextBox></td>
                  
                  <td></td>                 
              
         </tr>
                     </table>   

                  </div>
         </fieldset>
      <fieldset id="confirmCarga" style="width: 400px; height:200px;overflow:auto;">
          <legend style="background-color:#0069AE;font-weight:700;padding:0;width: 100%;">Carga</legend>
         
          <div id="dvCargaConfirmada">
             
           <%--   <div id="dv1">
                    <input type="checkbox" value="112" id="chkCargaConfirmada11" style="vertical-align:middle" />
                  <label for="chkCargaConfirmada11"> TYKU2313189, 20, DC, FCL, 1, 2530</label>
              <fieldset style="width:95%;float:right;">
                   <legend style="background-color:#0069AE;font-weight:700;padding:0;width: 100%;">Servicios</legend>
                  <div>
                        <input type="checkbox" value="11123" id="chkDet1" style="vertical-align:middle"/>
                      <label for="chkDet1">Cuadrilla</label>
                  </div>
                  <div>
                      <input type="checkbox" value="11124" id="chkDet2" style="vertical-align:middle"/>
                      <label for="chkDet2">Montacargas 2.5TM</label>
                  </div>
                  

                   
              </fieldset>

              </div>
               

            <div style="width:100%;">

                <br /><hr />
            </div>
             
              <div id="dv2">
                    <input type="checkbox" value="112" id="chkCargaConfirmada22" style="vertical-align:middle" />
                  <label for="chkCargaConfirmada22">WSHU313189, 20, DC, FCL, 1, 2530</label>
              <fieldset style="width:95%;float:right;">
                   <legend style="background-color:#0069AE;font-weight:700;padding:0;width: 100%;">Servicios</legend>
                  <div>
                       <input type="checkbox" value="11123" id="chkDet3" style="vertical-align:middle;"/>
                      <label for="chkDet3">Cuadrilla</label>

                  </div>
                  <div>
                        <input type="checkbox" value="11125" id="chkDet4" style="vertical-align:middle"/>
                      <label for="chkDet4">Montacargas 2.5TM</label>

                  </div>
                   
              </fieldset>

              </div>--%>
             
              
          </div>

      </fieldset>
         
     
          
      <table width="100%">
          <tr>
              <td>
                  <asp:ImageButton ID="btnTab3C" runat="server" ImageUrl="~/Imagenes//btnConfirm.png"
                               CssClass="btnConfirm" ToolTip="Finalizar Solicitud" OnClientClick=""
                       ImageAlign="Right"/>
                  <asp:ImageButton ID="btnTab3P" runat="server" ImageUrl="~/Imagenes//btnPrevious.png"                              
                      CssClass="btnPrev1" ToolTip="Previo"  OnClientClick="return GoBack();"
                       ImageAlign="Left"/>     
                  
              </td>             
          </tr>
      </table>


     
  </div>

    </form>
</div>
   
</body>


</html>


   <script language="javascript" type="text/javascript">
          var tiempo = 0;
        var seconds = 0;

        var myVar = setInterval(function () {
            clearInterval(myVar);
        }, 1000);

        var myTiempo = setTimeout(function () {
            clearInterval(myTiempo);
        }, 1);

        function myStopFunction() {
            clearInterval(myVar);
            seconds = '<%=GlobalEntity.Instancia.TiempoDuracionSesion%>' * 60;
            tiempo = '<%=GlobalEntity.Instancia.TiempoDuracionSesion%>' * 1000 * 60;

            CuentaTiempo(tiempo);
        }

        function CuentaTiempo(tiempo) {
            clearTimeout(myTiempo);
            myTiempo = setTimeout(function () {
               
                //window.location = "../Seguridad/eFENIX_Login.aspx";
            }, tiempo);
        }



        function SessionExpireAlert(timeout) {
            seconds = timeout / 1000;
            document.getElementsByName("seconds").innerHTML = seconds;

            myvar = setInterval(function () {
                seconds--;
                document.getElementById("seconds").innerHTML = seconds;
            }, 1000);

            CuentaTiempo(timeout);
        };


  </script>

       <%--   </table>--%>

    <style type="text/css">
#reservationsList {width:500px; max-height:600px; background:#fff; overflow:auto;}    
 
/*STRIPE LIST*/    
ul.stripedList {
    margin:0;
    padding:0;
    list-style:none;
}
.stripedList li {
    display:block;
    text-decoration:none;
    color:#333;
    font-family:Arial,Helvetica,sans-serif;
    font-size:12px;
    line-height:20px;
    height:20px;
}
.stripedList li span {
    display:inline-block;
    border-right:1px solid #ccc;
    overflow:hidden;
    text-overflow:ellipsis;
    padding:0 10px;
    height:20px;
}
.stripedList .evenRow {background:#f2f2f2;}
 
.c1 {width:55px;}
.c2 {width:70px;}
.c3 {width:130px;}
.c4 {width:15px;}
.cLast {border:0 !important;}
 
        .auto-style1 {
            margin-bottom: 4px;
        }
 
    </style>

</asp:Content>