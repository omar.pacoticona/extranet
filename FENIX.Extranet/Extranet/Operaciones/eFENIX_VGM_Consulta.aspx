﻿<%@ Page Language="C#" 
MasterPageFile="~/Extranet/Seguridad/eFENIX_Principal.master"
AutoEventWireup="true" 
CodeFile="eFENIX_VGM_Consulta.aspx.cs" 
Inherits="Extranet_Operaciones_eFENIX_VGM_Consulta"
Title="Consulta de VGM" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="CntMaster" runat="Server">

    <table cellspacing="0" cellpadding="0" style="width: 100%;">
        
        <tr>
            <td class="form_titulo" style="width: 85%; height: 2px;">
                CONSULTA DE VGM
            </td>
            <td align="right" class="form_titulo_verde" style="width: 15%; height: 2px;" >
                <ajax:UpdatePanel ID="UdpTotales" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <asp:TextBox ID="LblTotal" Text="Total de Registros:" style="background-color: #0069ae; color: #FFFFFF" ReadOnly="true" BorderColor="#0069ae" runat="server"></asp:TextBox>
                    </ContentTemplate>
                </ajax:UpdatePanel>
            </td>               
        </tr>
        
        <tr>
            <td valign="top" colspan="2">
                <table class="form_bandeja" style="margin-top: 5px; margin-bottom: 5px; width: 100%;">
                     <tr>
                        <td style="width: 10%;">
                            Manifiesto
                        </td>
                        <td style="width: 25%;">
                            <asp:TextBox ID="TxtAnoManifiesto" runat="server" MaxLength="4" Style="text-transform: uppercase"
                                Width="40px"></asp:TextBox>&nbsp;<asp:TextBox ID="TxtNumManifiesto" runat="server"
                                    MaxLength="6" Style="text-transform: uppercase" Width="80px"></asp:TextBox></td>
                        <td style="width: 10%;">
                            Booking
                        </td>
                        <td style="width: 20%;">
                            <asp:TextBox ID="TxtDocumentoM" runat="server" MaxLength="25" Style="text-transform: uppercase"
                                Width="120px"></asp:TextBox>
                        </td>  
                           <td>
                            &nbsp;
                        </td>
                        <td>
                            <asp:ImageButton ID="ImgBtnBuscar" runat="server" ImageUrl="~/Imagenes/Formulario/btn_consultar.JPG"
                                OnClick="ImgBtnBuscar_Click" OnClientClick="return ValidarFiltro();"/>                            
                        </td>
                    </tr>                    
                </table>
             </td>   
        </tr>
    

        <tr valign="top" style="height: 100%">
            <td colspan="5">
                <ajax:UpdatePanel ID="upDocumentoOrigen" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div style="overflow: auto; width: 100%; height: 100%">
                            <asp:GridView ID="GrvListado" runat="server" AutoGenerateColumns="False" DataKeyNames="iIdDocOri,sNumeroDO,sConsignatario"
                                SkinID="GrillaConsulta" Width="100%"  OnRowCommand="GrvListado_RowCommand" OnRowDataBound="GrvListado_RowDataBound">
                                <Columns>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkSeleccionar" runat="server"/>
                                        </ItemTemplate>
                                        <HeaderStyle BackColor="#0069ae" Width="3%" />
                                    </asp:TemplateField>
                                                                
                                    <asp:BoundField DataField="sNumeroDO" HeaderText="Booking" />
                                     <asp:BoundField DataField="sConsignatario" HeaderText="Cliente" />
                                     <asp:BoundField DataField="sNumManifiesto" HeaderText="Manifiesto" />      
                                    <asp:ButtonField ButtonType="Image" CommandName="OpenVerVGM" HeaderText="VGM" ImageUrl="~/Script/Imagenes/IconButton/filePDF.png"
                                            Text="Descargar VGM"></asp:ButtonField>                                    
                                </Columns>
                            </asp:GridView>
                        </div>
                    </ContentTemplate>
                    <Triggers>
                        <%--<ajax:PostBackTrigger ControlID="GrvListado"/>--%>
                        <ajax:AsyncPostBackTrigger ControlID="ImgBtnBuscar" EventName="Click" />
                        <ajax:AsyncPostBackTrigger ControlID="dnvListado" EventName="PageChanged" />
                        <ajax:AsyncPostBackTrigger ControlID="dnvListado" EventName="NextPage" />
                        <ajax:AsyncPostBackTrigger ControlID="dnvListado" EventName="PreviousPage" />
                        <ajax:AsyncPostBackTrigger ControlID="dnvListado" EventName="FirstPage" />  
                        <ajax:AsyncPostBackTrigger ControlID="dnvListado" EventName="LastPage" />     
                    </Triggers>
                </ajax:UpdatePanel>
            </td>
        </tr>
        <tr valign="top" style="height: 6%;">
            <td colspan="5">
               <ajax:UpdatePanel ID="upDnvListado" runat="server">
                    <ContentTemplate>
                        <FENIX:DataNavigator ID="dnvListado" runat="server" AllowCustomPaging="True" ButtonText="Ir" 
                            ForeColor="White" Font-Size="11px" BackColor="#0069ae" 
                            CurrentPage="1" CustomClientFunction="" EnableAskingAfterPage="False" EnableCustomScript="False"
                            GridViewId="GrvListado" ImagesPath="~/Imagenes/DN/" IsParentShowWindow="False"
                            MessageAskingAfterPage="" PageIndicatorFormat="Página {0} / {1}" Visible="False"
                            Width="100%" WidthButtonIr="" WidthTextBoxNumPagina="25px" />
                    </ContentTemplate>
                    <Triggers>
                        <ajax:AsyncPostBackTrigger ControlID="dnvListado" EventName="PageChanged" />
                        <ajax:AsyncPostBackTrigger ControlID="ImgBtnBuscar" EventName="Click" />
                    </Triggers>
                </ajax:UpdatePanel>
            </td>
        </tr>
    </table>
    <asp:HiddenField ID="HdIdDocOri" runat="server" />
    <script language="javascript" type="text/javascript">

        var tiempo = 0;
        var seconds = 0;

             $(document).ready(function(){
    //     $('#rbtnContenedorSi').change(function(){
    //    if(this.checked)
    //        $('#divContenedores').fadeIn('slow');
    //    else
    //        $('#divContenedores').fadeOut('slow');

    //});
        });

        var myVar = setInterval(function () {
            clearInterval(myVar);
        }, 1000);

        var myTiempo = setTimeout(function () {
            clearInterval(myTiempo);
        }, 1);

        function myStopFunction() {
            clearInterval(myVar);
            seconds = '<%=GlobalEntity.Instancia.TiempoDuracionSesion%>' * 60;
            tiempo = '<%=GlobalEntity.Instancia.TiempoDuracionSesion%>' * 1000 * 60;

            CuentaTiempo(tiempo);
        }

        function CuentaTiempo(tiempo) {
            clearTimeout(myTiempo);
            myTiempo = setTimeout(function () {
                document.getElementById('<%=btn_cerrar.ClientID%>').click();
                //window.location = "../Seguridad/eFENIX_Login.aspx";
            }, tiempo);
        }



        function SessionExpireAlert(timeout) {
            seconds = timeout / 1000;
            document.getElementsByName("seconds").innerHTML = seconds;

            myvar = setInterval(function () {
                seconds--;
                document.getElementById("seconds").innerHTML = seconds;
            }, 1000);

            CuentaTiempo(timeout);
        };


        var goodexit = false;
        window.onbeforeunload = confirmRegisterExit;
        function confirmRegisterExit(evt) {
            if (!goodexit) {
                //return "Si vas abandonar el sistema haz click en Cerrar Sesion!!";
                document.getElementById('<%=btn_cerrar.ClientID%>').click();
            }
        }

        var objFilaAnt = null;
        var backgroundColorFilaAnt = "";

        //function fc_SeleccionaFilaSimple(objFila, objrowIndex, chkID) {
        //    try {

        //        if (objFilaAnt != null) {
        //            objFilaAnt.style.backgroundColor = backgroundColorFilaAnt;
        //        }
        //        objFilaAnt = objFila;
        //        backgroundColorFilaAnt = objFila.style.backgroundColor;
        //        objFila.style.backgroundColor = "#c4e4ff";
        //    }

        //    catch (e) {
        //        error = e.message;
        //    }
        //}

        function HabilitarUno(chk) {
            f = document.forms[0].elements;
            for (x = 0; x < f.length; x++) {
                obj = f[x];
                if (obj.type == 'checkbox') {
                    obj.checked = false;
                }
            }
            chk.enabled;
            chk.checked = true;
        }

         function AbrirPagina(sDoc, iIdDocOri, sCliente) {           
            sPeriodo = document.getElementById('<%=TxtAnoManifiesto.ClientID%>').value;
            sNroManif = document.getElementById('<%=TxtNumManifiesto.ClientID%>').value;           

            window.location.href = "eFENIX_VGM_Consulta_Detalle.aspx?sNumeroDO=" + sDoc + "&sIdDocOri="+ iIdDocOri + "&sCliente=" + sCliente + "&sPeriodoManif=" + sPeriodo + "&sNroManif=" + sNroManif;
            //window.open("eFENIX_Tracking_Carga_Detalle.aspx?sDocumentoOrigen=" + sDoc + "&sDetalle=" + iDetalle);
        }

        function fc_PressKeyDO() {
            if (event.keyCode == 13) {
                document.getElementById("<%=ImgBtnBuscar.ClientID%>").click();
                return false;
            }
            else {
                return true;
            }
        }

       <%-- function VerVolante(sIdDocOri, sIdVolante) {
            document.getElementById('<%=HdIdDocOri.ClientID %>').value = sIdDocOri;
            document.getElementById('<%=HdIdVolante.ClientID %>').value = sIdVolante;

            document.getElementById("<%=ImgBtnPdf.ClientID%>").click();
        }--%>
        
        function ValidarFiltro() {
            var sManifiesto = document.getElementById('<%=TxtNumManifiesto.ClientID %>').value;
            var sDocOrigenM = document.getElementById('<%=TxtDocumentoM.ClientID %>').value;  
            var sCriterio = sManifiesto + sDocOrigenM;

            if ((fc_Trim(sCriterio) == "")) {
                alert('Debe indicar al menos un criterio de búsqueda');
                return false;
            }
            else {
                return true;
            }
        }
    </script>

</asp:Content>
<asp:Content ID="Content3" runat="server" contentplaceholderid="ContentCab">
    <span class="texto_titulo" >Tu Sesion Expirara en &nbsp; 
        <span class="texto_titulo" id="seconds"> </span>
        <span>&nbsp;seg.</span>
     </span>
     <asp:Button ID="btn_cerrar" runat="server" Text="cerrar" 
        onclick="btn_cerrar_Click"  Style="display: none;" />
</asp:Content>
