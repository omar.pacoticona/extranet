﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FENIX.Common;
using FENIX.BusinessEntity;
using FENIX.BusinessLogic;
using System.Web.Security;
using System.Drawing;

public partial class Extranet_Operaciones_eFENIX_PreLiquidacion_Generarr : System.Web.UI.Page
{
    private void SCA_MsgInformacion(string strError)
    {
        MensajeScript(strError);
    }

    private void MensajeScript(string SMS)
    {
        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "", "<script>alert('" + SMS + "');</script>", false);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        String tiempo = (Convert.ToInt32(GlobalEntity.Instancia.TiempoDuracionSesion.ToString()) * 1000 * 60).ToString();
        Session["Reset"] = true;
        if (!Page.IsPostBack)
        {
            BL_Liquidacion oBL_DocumentoPago = new BL_Liquidacion();
            Decimal TipoCambio = oBL_DocumentoPago.TipoCambio();
            lblTipoCambio.Text = "Tipo de cambio : " + TipoCambio.ToString();
            llenarCombos();
            lblDocOrigen.Visible = false;
            txtsDocOrigen.Visible = false;
            //SCA_MsgInformacion("Estimado cliente: la pre liquidación es un documento referencial que considera los servicios incurridos hasta el momento de su generación. "+
            //                        "Podrán generarse y cargarse servicios con posterioridad, los mismos que deberán ser reconocidos y pagados.");
            //SCA_MsgInformacion("La pre-liquidación es un documento referencial que considera los servicios a la carga registrados hasta el momento de su generación. " +
            //"En ciertas circunstancias, es posible que se generen servicios a la carga después de la emisión de esta pre-liquidación," +
            //" los mismos que deberán ser reconocidos y pagados.");

            //lblMoneda.Visible = false;
            //DplMonedaGrabar.Visible = false;
            //ImgBtnGrabar.Visible = false;

            //  mCondiciones.Show();
            ViewState["ListaServicios"] = "0";
            //Button1.Enabled = false;
            //ImgBtnGrabar.Enabled = false;                  
            ScriptManager.RegisterStartupScript(this, GetType(), "mensaje", "SessionExpireAlert(" + tiempo + ");", true);
        }
        else
            ScriptManager.RegisterStartupScript(this, GetType(), "mensaje", "myStopFunction();", true);
    }

    protected void btn_cerrar_Click(object sender, EventArgs e)
    {
        if (GlobalEntity.Instancia.CerrarExtranet == "S")
        {
            BL_Usuario oBL_Usuario = new BL_Usuario();
            oBL_Usuario.GrabaInicioSesion(GlobalEntity.Instancia.Usuario, "0");
            Session.Abandon();
            FormsAuthentication.SignOut();
            Response.Redirect("../../eFENIX_Login.aspx");
            Response.End();
        }
    }

    protected void ImgBtnBuscar_Click(object sender, EventArgs e)
    {
        BE_Liquidacion oBE_Liquidacion = new BE_Liquidacion();
        BL_Liquidacion oBL_Liquidacion = new BL_Liquidacion();
        BE_Liquidacion oBE_LiquidacionDocOrigen = new BE_Liquidacion();
        

        Int32 Negocio = 0;
        Int32 Operacion = 0;
        Int32 IdOficina = 0;       
        Negocio = 89; //DEP. Temporal
        Int32 IdCliente = GlobalEntity.Instancia.IdCliente;
        lblNomFecha.Text = "Fecha Retiro";
        lblDocOrigen.Text = "BL";
        lblOperacion.Text = "--";
        

        if (txtDocumento.Text == "")
        {
            SCA_MsgInformacion("Ingrese el Volante o Booking a consultar");
            return;
        }
        var Fin = Request.Form["DatePickerFinname"];
        var Inicio = Convert.ToString(DateTime.Today);     

        String OperacionValidar = oBL_Liquidacion.Operacion(txtDocumento.Text);

        if(OperacionValidar == "65")
        {
            if (Convert.ToDateTime(Fin) < Convert.ToDateTime(Inicio))
            {
                SCA_MsgInformacion("Fecha embarque no debe ser menor a la fecha actual");
                return;
            }
            //DplOperacion.SelectedValue = "65";
            txtVolBook.Text = "Booking";
            lblDocOrigen.Text = "BL";
            lblDocOrigen.Visible = false;
            txtsDocOrigen.Visible = false;
            lblNomFecha.Text = "Fecha Embarque";
            Operacion = 65;
            vi_Operacion.Value = "65";
            lblOperacion.Text = "EMBARQUE";
        }

        if (OperacionValidar == "66")
        {
            if (Convert.ToDateTime(Fin) < Convert.ToDateTime(Inicio))
            {
                SCA_MsgInformacion("Fecha retiro no debe ser menor a la fecha actual");
                return;
            }
            //DplOperacion.SelectedValue = "66";
            txtVolBook.Text = "Volante";
            lblDocOrigen.Text = "BL";
            lblDocOrigen.Visible = true;
            txtsDocOrigen.Visible = true;
            lblNomFecha.Text = "Fecha Retiro";
            Operacion = 66;
            vi_Operacion.Value = "66";
            lblOperacion.Text = "DESCARGA";
        }

        //if (DplOperacion.SelectedValue == "66") { Operacion = 66; }
        //if (DplOperacion.SelectedValue == "65") { Operacion = 65; }
        oBE_Liquidacion.sNumeroDo = txtDocumento.Text;       
        oBE_Liquidacion.sMasterLCL = "0";

        oBE_LiquidacionDocOrigen = oBL_Liquidacion.Listar_PreLiquidacion_Info_Cabecera_2(Negocio, Operacion, oBE_Liquidacion.sNumeroDo, oBE_Liquidacion.sMasterLCL, IdOficina, IdCliente);

        if (oBE_LiquidacionDocOrigen.iIdDocOri == null)
        {
            //InicializarControles();
            SCA_MsgInformacion("El documento ingresado pertenece otro cliente.");

            lblAfecto.Text = "-";
            lblDescuento.Text = "-";
            lblIgv.Text = "-";
           // lblTotal.Text = "-";
            upSubTotalDolares.Update();
            txtsDocOrigen.Text = "";
            txtCliente.Text = "";
            txtClienteAcuerdo.Text = "";
            TxtNroAcuerdo.Text = "";
            txtVolBook.Text = "Documento";
            //DplOperacion.SelectedValue = "-1";
            lblDocOrigen.Text = "BL";
            lblDocOrigen.Visible = false;
            txtsDocOrigen.Visible = false;
            txtsDocOrigen.Text = "";
            lblNomFecha.Text = "Fecha Retiro";
            lblOperacion.Text = "--";
            lblTotal1.Text = "-";
            lblTotal2.Text = "-";
            upCabecera.Update();
            upSubTotalDolares.Update();

            List<BE_Liquidacion> oLista_Liquidacion = new List<BE_Liquidacion>();
            BE_Liquidacion oBE_Liquidacionn = new BE_Liquidacion();
            //oLista_Acuerdo.Add(oBE_AcuerdoComision);
            GrvListadoAlmacenaje.DataSource = oLista_Liquidacion;
            GrvListadoAlmacenaje.DataBind();

            List<BE_Liquidacion> oLista_LiquidacionV2 = new List<BE_Liquidacion>();
            BE_Liquidacion oBE_LiquidacionV2 = new BE_Liquidacion();
            //oLista_Acuerdo.Add(oBE_AcuerdoComision);
            GvListaServicios.DataSource = oLista_LiquidacionV2;
            GvListaServicios.DataBind();

            return;
        }
      
        else
        {
            
            txtIdDocOri.Text = oBE_LiquidacionDocOrigen.iIdDocOri.ToString();
            txtCliente.Text = oBE_LiquidacionDocOrigen.sCliente;
            txtCliente.ToolTip = oBE_LiquidacionDocOrigen.sCliente;
            TxtIdAcuerdo.Text = Convert.ToString(oBE_LiquidacionDocOrigen.iIdContrato);
            TxtNroAcuerdo.Text = Convert.ToString(oBE_LiquidacionDocOrigen.sNroAcuerdo);
            txtsDocOrigen.Text = oBE_LiquidacionDocOrigen.sNumeroDo;
            txtClienteAcuerdo.Text = oBE_LiquidacionDocOrigen.sClienteContrato;
            TxtIdMoneda.Text = oBE_LiquidacionDocOrigen.sTipoMoneda.ToString();
            if (TxtIdMoneda.Text == "999"  )
            {
                TxtIdMoneda.Text = "0"; //Por defecto que coja el valor de dolares.
                vi_MonedaGrabar.Value = (DplMonedaGrabar.SelectedValue);
                //vi_MonedaContrato.Value = "0";
                //lblOpcion.Text = "Opcion : Dolares";
            }
            if (TxtIdMoneda.Text == "24")
            {
                TxtIdMoneda.Text = "0";
                vi_MonedaGrabar.Value = (DplMonedaGrabar.SelectedValue);
                //vi_MonedaContrato.Value = "24";
                //lblOpcion.Text = "Opcion : Dolares";
            }
            if (TxtIdMoneda.Text == "23")
            {
                TxtIdMoneda.Text = "0";
                vi_MonedaGrabar.Value = (DplMonedaGrabar.SelectedValue);
                //vi_MonedaContrato.Value = "23";
               // lblOpcion.Text = "Opcion : Soles";
            }
            //// TxtIdMoneda.Text = "0";
            UpdatePanel2.Update();
            TxtIdCliente.Text = oBE_LiquidacionDocOrigen.iIdCliente.ToString();
            TxtIdAgntAduanas.Text = oBE_LiquidacionDocOrigen.iIdAgAduana.ToString();
            txtNroVolante.Text = txtDocumento.Text;
            //HdTxtIdOperacion.Text = DplOperacion.SelectedValue; //vi_Operacion
            var Operacionn = vi_Operacion.Value;
            HdTxtIdOperacion.Text = Operacionn; //vi_Operacion
            Int32 IdContrato = Convert.ToInt32(oBE_LiquidacionDocOrigen.iIdContrato);
           // Button1.Enabled = true;
          //  ImgBtnGrabar.Enabled = true;
            upCabecera.Update();           

            //if (oBE_LiquidacionDocOrigen.iIdContrato == 0)
            //{
            //    //SCA_MsgInformacion("Su pedido de liquidación no puede ser atendido porque nuestra área Comercial debe asignar un contrato");
            //    BE_Liquidacion oBE_Alerta = new BE_Liquidacion();
            //    BL_Liquidacion BL_oAletras = new BL_Liquidacion();
            //    oBE_Alerta.sNroVolante = txtDocumento.Text;
            //    oBE_Alerta.sTipoMensaje = "1";
            //    oBE_Alerta.sOperacion = DplOperacion.SelectedValue;
            //    oBE_Alerta.iIdOperacion = Convert.ToInt32(DplOperacion.SelectedValue);
            //    String[] mensaje;
            //    mensaje = BL_oAletras.PreLiquidacion_EnviarAlertas(oBE_Alerta).Split('|');
            //    // return;
            //}
            ConsultarServicios(oBE_LiquidacionDocOrigen.iIdDocOri);
        }

    }

    protected void ConsultarServicios(Int32? iIdDocOri)
    {
        BE_Liquidacion oBE_Liquidacion = new BE_Liquidacion();
        BL_Liquidacion oBL_Liquidacion = new BL_Liquidacion();
        BE_LiquidacionList oBE_LiquidacionServicio = new BE_LiquidacionList();
        BE_LiquidacionList oBE_LiquidacionAlmacenaje = new BE_LiquidacionList();
        var iIdMonedaGrabar = vi_MonedaGrabar.Value;
        //if (RbtImportacion.Checked) { Operacion = 66; }
        //if (RbtExportacion.Checked) { Operacion = 65; }
        var OperacionnValue = vi_Operacion.Value;

        oBE_Liquidacion.iIdDocOri = iIdDocOri;
        oBE_Liquidacion.dtFecha_Liquidacion = Convert.ToDateTime(Request.Form["DatePickerFinname"]);

        String[] Resultado = null;
        int iRetorno = 2;
        Int32 iIdAcuerdo = 0;
        if (TxtNroAcuerdo.Text.Trim() != String.Empty)
            iIdAcuerdo = Convert.ToInt32(TxtIdAcuerdo.Text.Trim());

        oBE_Liquidacion.iIdAcuerdo = iIdAcuerdo;
        oBE_Liquidacion.iIdMoneda = Convert.ToInt32(TxtIdMoneda.Text);
        oBE_Liquidacion.idMonedaGrabar = Convert.ToInt32(iIdMonedaGrabar);
        if (oBE_Liquidacion.idMonedaGrabar == 23) oBE_Liquidacion.idMonedaGrabar2 = 24;
        if (oBE_Liquidacion.idMonedaGrabar == 24) oBE_Liquidacion.idMonedaGrabar2 = 23;
        //oBE_Liquidacion.iIdSuperNegocio_2 = Convert.ToInt32(dplLineaNegocio.SelectedValue);
        //oBE_Liquidacion.iIdLineaNegocio = Convert.ToInt32(dplLineaNegocio.SelectedValue);
        oBE_Liquidacion.iIdSuperNegocio_2 = 89;
        oBE_Liquidacion.iIdLineaNegocio = 89;
        //ChkMasterLCL.Checked = false;


        if (OperacionnValue == "66")
        {
            oBE_Liquidacion.iIdOperacion = Convert.ToInt32(TipoOperacion.Importacion);
            //if (!ChkMasterLCL.Checked)
             oBE_Liquidacion.sNroVolante = txtDocumento.Text;
            //else
               // oBE_Liquidacion.sNroVolante = null;
        }
        else if (OperacionnValue == "65")
            oBE_Liquidacion.iIdOperacion = Convert.ToInt32(TipoOperacion.Exportacion);


        if (OperacionnValue == "66")
            //if (ChkMasterLCL.Checked)
            //    Resultado = oBL_Liquidacion.Listar_Recuperar_Contrato_2(Convert.ToInt32(dplLineaNegocio.SelectedValue), null, iIdDocOri, Convert.ToInt32(TipoOperacion.Importacion), "1").Split('|');
            //else
                Resultado = oBL_Liquidacion.Listar_Recuperar_Contrato_2(89, Convert.ToInt32(txtDocumento.Text), null, Convert.ToInt32(TipoOperacion.Importacion), "0").Split('|');
        else if (OperacionnValue == "65")
            Resultado = oBL_Liquidacion.Listar_Recuperar_Contrato_2(89, null, iIdDocOri, Convert.ToInt32(TipoOperacion.Exportacion), "0").Split('|');


        if (OperacionnValue == "66")
        {
            //if (ChkMasterLCL.Checked)
            //    iRetorno = oBL_Liquidacion.RegistrarServicios_Mandatorios_2(89, Convert.ToInt32(TipoOperacion.Importacion), iIdDocOri, iIdAcuerdo, null, "2");
            //else
                iRetorno = oBL_Liquidacion.RegistrarServicios_Mandatorios_2(89, Convert.ToInt32(TipoOperacion.Importacion), iIdDocOri, iIdAcuerdo, Convert.ToInt32(txtDocumento.Text), "0");
        }
        else if (OperacionnValue == "65")
            iRetorno = oBL_Liquidacion.RegistrarServicios_Mandatorios_2(89, Convert.ToInt32(TipoOperacion.Exportacion), iIdDocOri, iIdAcuerdo, null, "0");


        //oBE_Liquidacion.sMasterLCL = ChkMasterLCL.Checked ? "1" : "0";
        oBE_Liquidacion.sMasterLCL =  "0";

        oBE_Liquidacion.iIdLiq = 0;
        BE_LiquidacionList lista = oBL_Liquidacion.Listar_PreLiquidacion_2(oBE_Liquidacion);
        ViewState["ListaServicios"] = lista.Count();
        BL_Liquidacion oBL_DocumentoPago = new BL_Liquidacion();
        Decimal TipoCambio = oBL_DocumentoPago.TipoCambio();
        var MonedaGrabar = vi_MonedaGrabar.Value;
        //var MonedAContrato = vi_MonedaContrato.Value;
        //if(MonedAContrato == "0")
        //{

        //}
        Int32 iIdAsumeEnergia = 0;
        Decimal dAfecto = 0;
        Decimal dInafecto = 0;
        Decimal dDescuento = 0;
        Decimal dTotal = 0;
        Decimal dIgv = 0;

        Decimal dAfecto2= 0;
        Decimal dInafecto2 = 0;
        Decimal dDescuento2 = 0;
        Decimal dTotal2 = 0;
        Decimal dIgv2 = 0;


        if ((lista != null) && (lista.Count > 0))
        {
            for (int i = 0; i < lista.Count; i++)
            {
                if ((lista[i].sDescripcionServicio.Trim().Length >= 10) && (lista[i].iIdTipoServicio_2 == 3)) //ALMACENAJE //lista[i].sDescripcionServicio.Trim().Substring(0, 10).ToUpper() == "ALMACENAJE"
                {
                    BE_Liquidacion oBE_Servicio = new BE_Liquidacion();
                    oBE_Servicio.iIdOrdSer = lista[i].iIdOrdSer;
                    oBE_Servicio.iIdOrdSerDet = lista[i].iIdOrdSerDet;
                    oBE_Servicio.iIdDocOriDet = lista[i].iIdDocOriDet;
                    oBE_Servicio.sDescripcionServicio = lista[i].sDescripcionServicio;
                    oBE_Servicio.sContenedor = lista[i].sContenedor;
                    oBE_Servicio.sTipoCarga = lista[i].sTipoCarga;
                    oBE_Servicio.sTipoContenedor = lista[i].sTipoContenedor;
                    oBE_Servicio.iIdTamanoContenedor = lista[i].iIdTamanoContenedor;
                    oBE_Servicio.sEmbalaje = lista[i].sEmbalaje;
                    oBE_Servicio.iIdTarifa = lista[i].iIdTarifa;
                    oBE_Servicio.dMontoUnitario = lista[i].dMontoUnitario;
                    oBE_Servicio.dCantidad = lista[i].dCantidad;
                    oBE_Servicio.dAfecto = lista[i].dAfecto;
                    oBE_Servicio.dInafecto = lista[i].dInafecto;
                    oBE_Servicio.dDescuento = lista[i].dDescuento;
                    oBE_Servicio.dIgv = lista[i].dIgv;
                    oBE_Servicio.dTotal = lista[i].dTotal;
                    oBE_Servicio.sModalidad = lista[i].sModalidad;
                    oBE_Servicio.sOrigen = lista[i].sOrigen;
                    oBE_Servicio.sSituacion = lista[i].sSituacion;
                    oBE_Servicio.sFechaIngreso = lista[i].sFechaIngreso;
                    oBE_Servicio.iItem = lista[i].iItem;
                    oBE_Servicio.sTarifa = lista[i].sTarifa;
                    oBE_Servicio.dPeso = lista[i].dPeso;
                    oBE_Servicio.sTipoMoneda = lista[i].sTipoMoneda;
                    oBE_Servicio.sRetroactivo = lista[i].sRetroactivo;
                    oBE_Servicio.dtFechaRetiro = lista[i].dtFechaRetiro;
                    oBE_Servicio.iDiasLibres = lista[i].iDiasLibres;
                    oBE_Servicio.iIdAsumeEnergia_2 = lista[i].iIdAsumeEnergia_2;
                    oBE_Servicio.sOrigenPqteSLI = lista[i].sOrigenPqteSLI;
                    oBE_Servicio.sMarcaSLI = lista[i].sMarcaSLI;
                    oBE_Servicio.sCondCarga = lista[i].sCondCarga;
                    //TxtDiasLibres.Text = lista[i].iDiasLibres.ToString();
                    //TxtRetroactivo.Text = lista[i].sRetroactivo.ToString();
                    oBE_Servicio.dMontoNeto = lista[i].dAfecto;
                    oBE_Servicio.dMontoBruto = lista[i].dTotal;
                    //
                    oBE_Servicio.dAfectoGrabar = lista[i].dAfectoGrabar;
                    oBE_Servicio.dInafectoGrabar = lista[i].dInafectoGrabar;
                    oBE_Servicio.dDescuentoGrabar = lista[i].dDescuentoGrabar;
                    oBE_Servicio.ddetraccionGrabar = lista[i].ddetraccionGrabar;
                    oBE_Servicio.dIgvGrabar = lista[i].dIgvGrabar;
                    oBE_Servicio.dMontoTotalGrabar = lista[i].dMontoTotalGrabar;
                    oBE_Servicio.dUnitarioGrabar = lista[i].dUnitarioGrabar;

                    oBE_Servicio.dAfectoGrabar2 = lista[i].dAfectoGrabar2;
                    oBE_Servicio.dInafectoGrabar2 = lista[i].dInafectoGrabar2;
                    oBE_Servicio.dDescuentoGrabar2 = lista[i].dDescuentoGrabar2;
                    oBE_Servicio.ddetraccionGrabar2 = lista[i].ddetraccionGrabar2;
                    oBE_Servicio.dIgvGrabar2 = lista[i].dIgvGrabar2;
                    oBE_Servicio.dMontoTotalGrabar2 = lista[i].dMontoTotalGrabar2;
                    oBE_Servicio.dUnitarioGrabar2 = lista[i].dUnitarioGrabar2;

                    oBE_LiquidacionAlmacenaje.Add(oBE_Servicio);
                }
                else
                {
                    BE_Liquidacion oBE_Servicio = new BE_Liquidacion();
                    oBE_Servicio.iIdOrdSer = lista[i].iIdOrdSer;
                    oBE_Servicio.iIdOrdSerDet = lista[i].iIdOrdSerDet;
                    oBE_Servicio.iIdDocOriDet = lista[i].iIdDocOriDet;
                    oBE_Servicio.sDescripcionServicio = lista[i].sDescripcionServicio;
                    oBE_Servicio.sContenedor = lista[i].sContenedor;
                    oBE_Servicio.sTipoCarga = lista[i].sTipoCarga;
                    oBE_Servicio.sTipoContenedor = lista[i].sTipoContenedor;
                    oBE_Servicio.iIdTamanoContenedor = lista[i].iIdTamanoContenedor;
                    oBE_Servicio.sEmbalaje = lista[i].sEmbalaje;
                    oBE_Servicio.iIdTarifa = lista[i].iIdTarifa;
                    oBE_Servicio.dMontoUnitario = lista[i].dMontoUnitario;
                    oBE_Servicio.dCantidad = lista[i].dCantidad;
                    oBE_Servicio.dAfecto = lista[i].dAfecto;
                    oBE_Servicio.dInafecto = lista[i].dInafecto;
                    oBE_Servicio.dDescuento = lista[i].dDescuento;
                    oBE_Servicio.dIgv = lista[i].dIgv;
                    oBE_Servicio.dTotal = lista[i].dTotal;
                    oBE_Servicio.sModalidad = lista[i].sModalidad;
                    oBE_Servicio.sOrigen = lista[i].sOrigen;
                    oBE_Servicio.sSituacion = lista[i].sSituacion;
                    oBE_Servicio.sFechaIngreso = lista[i].sFechaIngreso;
                    oBE_Servicio.iItem = lista[i].iItem;
                    oBE_Servicio.sTarifa = lista[i].sTarifa;
                    oBE_Servicio.dPeso = lista[i].dPeso;
                    oBE_Servicio.sTipoMoneda = lista[i].sTipoMoneda;
                    oBE_Servicio.dDetraccion = lista[i].dDetraccion;
                    oBE_Servicio.sPorcentDetraccion = lista[i].sPorcentDetraccion;
                    oBE_Servicio.iIdAsumeEnergia_2 = lista[i].iIdAsumeEnergia_2;
                    oBE_Servicio.sOrigenPqteSLI = lista[i].sOrigenPqteSLI;
                    oBE_Servicio.sMarcaSLI = lista[i].sMarcaSLI;
                    oBE_Servicio.sCondCarga = lista[i].sCondCarga;
                    oBE_Servicio.dMontoNeto = lista[i].dAfecto;
                    oBE_Servicio.dMontoBruto = lista[i].dTotal;
                    //
                    oBE_Servicio.dAfectoGrabar = lista[i].dAfectoGrabar;
                    oBE_Servicio.dInafectoGrabar = lista[i].dInafectoGrabar;
                    oBE_Servicio.dDescuentoGrabar = lista[i].dDescuentoGrabar;
                    oBE_Servicio.ddetraccionGrabar = lista[i].ddetraccionGrabar;
                    oBE_Servicio.dIgvGrabar = lista[i].dIgvGrabar;
                    oBE_Servicio.dMontoTotalGrabar = lista[i].dMontoTotalGrabar;
                    oBE_Servicio.dUnitarioGrabar = lista[i].dUnitarioGrabar;

                    oBE_Servicio.dAfectoGrabar2 = lista[i].dAfectoGrabar2;
                    oBE_Servicio.dInafectoGrabar2 = lista[i].dInafectoGrabar2;
                    oBE_Servicio.dDescuentoGrabar2 = lista[i].dDescuentoGrabar2;
                    oBE_Servicio.ddetraccionGrabar2 = lista[i].ddetraccionGrabar2;
                    oBE_Servicio.dIgvGrabar2 = lista[i].dIgvGrabar2;
                    oBE_Servicio.dMontoTotalGrabar2 = lista[i].dMontoTotalGrabar2;
                    oBE_Servicio.dUnitarioGrabar2 = lista[i].dUnitarioGrabar2;

                    oBE_LiquidacionServicio.Add(oBE_Servicio);

                    if (lista[i].iIdAsumeEnergia_2 == 1)
                    {
                        iIdAsumeEnergia = 1;
                    }
                }

                //if(lista[i].sTipoMoneda == "$")
                //{
                    dAfecto += lista[i].dAfectoGrabar;
                    dInafecto += lista[i].dInafectoGrabar;                
                    dDescuento += lista[i].dDescuentoGrabar;
                    dIgv += lista[i].dIgvGrabar;
                    dTotal = dTotal + lista[i].dMontoTotalGrabar;

                    dAfecto2 += lista[i].dAfectoGrabar2;
                    dInafecto2 += lista[i].dInafectoGrabar2;
                    dDescuento2 += lista[i].dDescuentoGrabar2;
                    dIgv2 += lista[i].dIgvGrabar2;
                    dTotal2 = dTotal2 + lista[i].dMontoTotalGrabar2;

                //}

                //if (lista[i].sTipoMoneda == "S/.")
                //{
                //    dAfectoSoles += lista[i].dAfecto;
                //    dInafectoSoles += lista[i].dInafecto;
                //    //dSubTotal = dSubTotal + lista[i].dAfecto;
                //    dDescuentoSoles += lista[i].dDescuento;
                //    dIgvSoles += lista[i].dIgv;
                //    dTotalSoles = dTotalSoles + lista[i].dTotal;
                //   // ScriptManager.RegisterStartupScript(this, GetType(), "mensaje3", "upTablaTotalSoles();", true);

                //}


                //// ========= MONTO TOTAL NO VISIBLE  EN DOLARES =======

                //dAfectoDolares += lista[i].dTotSubTotalDolares;
                //dInafectoDolares += lista[i].dInafectoDolares;
                ////dSubTotal = dSubTotal + lista[i].dAfecto;
                //dDescuentoDolares += lista[i].dTotDescuentoDolares;
                //dIgvDolares += lista[i].dTotIgvDolares;
                //dTotalDolares = dTotalDolares + lista[i].dTotTotalDolares;



                //// ========= MONTO TOTAL NO VISIBLE  EN DOLARES =======
                //dAfectoSolesTotal += lista[i].dTotSubTotalSoles;
                //dInafectoSolesTotal += 0;
                ////dSubTotal = dSubTotal + lista[i].dAfecto;
                //dDescuentoSolesTotal += 0;
                //dIgvSolesTotal += lista[i].dTotIgvSoles;
                //dTotalSolesTotal = dTotalSolesTotal + lista[i].dTotTotalSoles;


            }

            //TxtSubTotal.Text = dSubTotal.ToString();
            //TxtDescuento.Text = dDescuento.ToString();
            //TxtIgv.Text = dIGV.ToString();
            //TxtTotal.Text = dTotal.ToString();

            //if (iIdAsumeEnergia == 1)
            //{
            //    TxtCliente.Visible = false;
            //    DdlCliente.Visible = true;
            //}
            //else
            //{
            //    TxtCliente.Visible = true;
            //    DdlCliente.Visible = false;
            //}

            //if ((oBE_LiquidacionServicio.Count > 0) || (oBE_LiquidacionAlmacenaje.Count > 0))
            //{
            //    btnGrabar.Enabled = true;
            //}
        }
        GvListaServicios.DataSource = oBE_LiquidacionServicio;
        GvListaServicios.DataBind();

        GrvListadoAlmacenaje.DataSource = oBE_LiquidacionAlmacenaje;
        GrvListadoAlmacenaje.DataBind();

        //OBTIENDO SOLES CON EL TIPO DE CAMBIO.
        //dAfectoSolesTotal = dAfectoDolares * TipoCambio;
        //dIgvSolesTotal = dIgvDolares*TipoCambio;
        //dTotalSolesTotal = dAfectoSolesTotal + dIgvSolesTotal;

        //SUB TOTAL SOLES.
        decimal dSubAfecto = 0;
        String sAfecto = "";
        decimal.TryParse(dAfecto.ToString(), out dSubAfecto);
        sAfecto = dSubAfecto.ToString("N2");

        decimal dSubDescuento = 0;
        String sDescuento = "";
        decimal.TryParse(dDescuento.ToString(), out dSubDescuento);
        sDescuento = dSubDescuento.ToString("N2");

        decimal dSubIgv = 0;
        String sIgv = "";
        decimal.TryParse(dIgv.ToString(), out dSubIgv);
        sIgv = dSubIgv.ToString("N2");

        decimal ddTotal2 = 0;
        String sTotal2 = "";
        decimal.TryParse(dTotal2.ToString(), out ddTotal2);
        sTotal2 = ddTotal2.ToString("N2");

        decimal Total = Convert.ToDecimal(sAfecto) + Convert.ToDecimal(sIgv);
       
        //=======================================================
        dTotal = Total;

       
        Hd_Afecto.Value = sAfecto.ToString();
        Hd_Inafecto.Value = 0.ToString();
        Hd_Descuento.Value = sDescuento.ToString();
        Hd_Igv.Value = sIgv.ToString();
        Hd_Total.Value = Total.ToString();

        if(MonedaGrabar == "23")
        {
            lblAfecto.Text = sAfecto;
            HdtxtAfecto.Text = sAfecto;
            lblDescuento.Text = sDescuento;
            HdtxtDescuento.Text = sDescuento;
            lblIgv.Text = sIgv;
            HdtxtIGV.Text = sIgv;
            //lblTotal.Text = sTotal;
            txtTotalHd.Text = Total.ToString();
            lblTotal1.Text = Total.ToString();
            lblTotalNombre.Text = "Total S/";
            lblTotalNombre2.Text = "Total $";
            //dTotal = Math.Round((dTotal / TipoCambio),3);
            //Total2 = (dTotal / TipoCambio);
           // Total2 = 
            lblTotal2.Text = sTotal2.ToString();
        }

        if(MonedaGrabar == "24")
        {
            lblAfecto.Text = sAfecto;
            HdtxtAfecto.Text = sAfecto;
            lblDescuento.Text = sDescuento;
            HdtxtDescuento.Text = sDescuento;
            lblIgv.Text = sIgv;
            HdtxtIGV.Text = sIgv;
            lblTotal1.Text = Total.ToString();
            //lblTotal.Text = sTotal;
            txtTotalHd.Text = Total.ToString();
            lblTotalNombre.Text = "Total $";
            lblTotalNombre2.Text = "Total S/";
            //dTotal = Math.Round((dTotal *  TipoCambio),3); //0233516
            // Total2 = (dTotal * TipoCambio);
            //lblTotal2.Text = dTotal.ToString();
            lblTotal2.Text = sTotal2.ToString();

        }

        decimal dSubTotal = 0;
        String sTotal = "";
        decimal.TryParse(dTotal.ToString(), out dSubTotal);
        sTotal = dSubTotal.ToString("N2");
        lblTotal1.Text = sTotal;

       
        upSubTotalDolares.Update();
        UpdatePanel3.Update();
        
    }

    public void llenarCombos()
    {
        ListItem i;
        //i = new ListItem("Dep. Temporal", "89");
        //dplLineaNegocio.Items.Add(i);
        //dplLineaNegocio.SelectedValue = "89";

        //i = new ListItem("Dep. Temporal", "89");
        //DplMonedaGrabar.Items.Add(i);
        //dplLineaNegocio.SelectedValue = "89";

        ListItem Descarga, Embarque,Seleccione;
        //Seleccione = new ListItem("         ", "-1");
        //Descarga = new ListItem("DESCARGA", "66");
        //Embarque = new ListItem("EMBARQUE", "65");
        //DplOperacion.Items.Add(Seleccione);
        //DplOperacion.Items.Add(Descarga);        
        //DplOperacion.Items.Add(Embarque);
        //DplOperacion.SelectedValue = "-1";

        ListItem Dolares, Soles, Seleccionar;       
        Dolares = new ListItem("DOLARES", "24");
        Soles = new ListItem("SOLES", "23");       
        DplMonedaGrabar.Items.Add(Dolares);
        DplMonedaGrabar.Items.Add(Soles);
        DplMonedaGrabar.SelectedValue = "24";
        
    }






    protected void ImgBtnGrabar_Click(object sender, EventArgs e)
    {
        //var a = LblTitulo.Text;
        var AceptaMensaje = vi_Validacion.Value;
        //var AceptaMensaje = confirm();
        if (AceptaMensaje.Equals("Si"))
        {
            //try
            //{
                vi_Validacion.Value = "";
                BE_Liquidacion oBE_Liquidacion = new BE_Liquidacion();
                BE_LiquidacionList oBE_LiquidacionList = new BE_LiquidacionList();
                BL_Liquidacion oBL_Liquidacion = new BL_Liquidacion();
            BL_Liquidacion oBL_DocumentoPag = new BL_Liquidacion();
                String sIdDua = string.Empty;
                String sTotal = string.Empty;
                String sSubTotal = string.Empty;
                String sIgv = string.Empty;
                String sDetraccion = string.Empty;
                String sDescuento = string.Empty;
                Int32 iRetorno = 0;
                Int32 Operacion = 0;
                Int32 IdSolicitudPreLiq = 0;
                Int32 IdMoneda = 0;
                String totalServicios = ViewState["ListaServicios"].ToString();
                Decimal TipoCambio = oBL_DocumentoPag.TipoCambio();

            if(TipoCambio == 0)
            {
                SCA_MsgInformacion("No existe Tipo de Cambio del dia registrado. Intente mas tarde.");
                return;
            }

            var Fin = Request.Form["DatePickerFinname"];
            var Inicio = Convert.ToString(DateTime.Today);

           

            //===========
            if (GlobalEntity.Instancia.Usuario == null || GlobalEntity.Instancia.Usuario == "")
            {
                SCA_MsgInformacion("Inicie session nuevamente.");
                return;
            }
            if (totalServicios == "0")
            {
                SCA_MsgInformacion("No se visualizan servicios a grabar");
                return;
            }


            var HdIdMoneda = vi_MonedaGrabar.Value;
            var OperacionnValue = vi_Operacion.Value;
            if (OperacionnValue == "66")
            {
                if (Convert.ToDateTime(Fin) < Convert.ToDateTime(Inicio))
                {
                    SCA_MsgInformacion("Fecha retiro no debe ser menor a la fecha actual");
                    return;
                }
            }

            if (OperacionnValue == "65")
            {
                if (Convert.ToDateTime(Fin) < Convert.ToDateTime(Inicio))
                {
                    SCA_MsgInformacion("Fecha embarque no debe ser menor a la fecha actual");
                    return;
                }
            }

            var HdIdMoneda2 = "";
            if (Convert.ToInt32(HdIdMoneda) == 23) HdIdMoneda2 = "24";
            if (Convert.ToInt32(HdIdMoneda) == 24) HdIdMoneda2 = "23";
            var AfectoTotal = "";
                var DescuentoTotal = "";
                var IgvTotal = "";
                 var TotalTotal = "";
            
                            AfectoTotal = Hd_Afecto.Value.ToString();
                            DescuentoTotal = Hd_Descuento.Value.ToString();
                            IgvTotal = Hd_Igv.Value.ToString();
                            TotalTotal = Hd_Total.Value.ToString();

            //if (DplOperacion.SelectedValue == "66") { Operacion = 66; }
            //    if (DplOperacion.SelectedValue == "65") { Operacion = 65; }
            if (OperacionnValue == "66") { Operacion = 66; }
            if (OperacionnValue == "65") { Operacion = 65; }




            String[] Respuesta;
                oBE_Liquidacion.iIdDocOri = Convert.ToInt32(txtIdDocOri.Text);
                oBE_Liquidacion.sOrigenSolicitud = "Online";
            //oBE_Liquidacion.iIdSuperNegocio_2 = Convert.ToInt32(dplLineaNegocio.SelectedValue);
            oBE_Liquidacion.iIdSuperNegocio_2 = 89;
            oBE_Liquidacion.sUsuario = GlobalEntity.Instancia.Usuario;
                oBE_Liquidacion.iIdLiq = null;
                oBE_Liquidacion.iIdCliente = GlobalEntity.Instancia.IdCliente;
                oBE_Liquidacion.iIdAcuerdo = Convert.ToInt32(TxtIdAcuerdo.Text);
                oBE_Liquidacion.sFechaCalculo = Request.Form["DatePickerFinname"];
                oBE_Liquidacion.sSituacion = "1"; //CORRECTO

                Respuesta = oBL_Liquidacion.PreLiquidacion_Ins_SolicitudPreLiquidacion(oBE_Liquidacion).Split('|');
                IdSolicitudPreLiq = Convert.ToInt32(Respuesta[0]);
                oBE_Liquidacion.iIdSolitudPreLiq = IdSolicitudPreLiq;
                if (Respuesta[0] == "-1")
                {

                }

                if (Convert.ToInt32(Respuesta[0]) > 0)
                {
                try
                {
                    String[] RespuestaAcuerdo;
                    if (Convert.ToInt32(TxtIdAcuerdo.Text) >= 1)
                    {
                        oBE_Liquidacion.iIdAcuerdo = Convert.ToInt32(TxtIdAcuerdo.Text);
                        iRetorno = oBL_Liquidacion.ConsultarEstadoAcuerdo_2(oBE_Liquidacion.iIdAcuerdo);

                        if (iRetorno != 6)
                        {
                            oBE_Liquidacion.iIdSolitudPreLiq = Convert.ToInt32(IdSolicitudPreLiq);
                            oBE_Liquidacion.iIdValidacion = 0; //No Cumple la validacion
                            oBE_Liquidacion.iTipoProceso = 1;
                            RespuestaAcuerdo = oBL_Liquidacion.PreLiquidacion_Ins_Validaciones(oBE_Liquidacion).Split('|');

                            //ENVIA MENSAJE
                            BE_Liquidacion oBE_Alerta = new BE_Liquidacion();
                            BL_Liquidacion BL_oAletras = new BL_Liquidacion();
                            oBE_Alerta.sNroVolante = txtDocumento.Text;
                            oBE_Alerta.sTipoMensaje = "4";
                            oBE_Alerta.sOperacion = HdTxtIdOperacion.Text;
                            //oBE_Alerta.sOperacion = DplOperacion.SelectedValue;
                            //oBE_Alerta.iIdOperacion = Convert.ToInt32(DplOperacion.SelectedValue);
                            String[] mensaje;
                            mensaje = BL_oAletras.PreLiquidacion_EnviarAlertas(oBE_Alerta).Split('|');
                        }
                        else
                        {   //CUMPLE LA VALIDACION.
                            oBE_Liquidacion.iIdSolitudPreLiq = Convert.ToInt32(IdSolicitudPreLiq);
                            oBE_Liquidacion.iIdValidacion = 1; //Cumple la validacion
                            oBE_Liquidacion.iTipoProceso = 1;
                            RespuestaAcuerdo = oBL_Liquidacion.PreLiquidacion_Ins_Validaciones(oBE_Liquidacion).Split('|');
                        }
                    }
                    else
                    {
                        oBE_Liquidacion.iIdSolitudPreLiq = Convert.ToInt32(IdSolicitudPreLiq);
                        oBE_Liquidacion.iIdValidacion = 0; //No Cumple la validacion
                        oBE_Liquidacion.iTipoProceso = 1;
                        RespuestaAcuerdo = oBL_Liquidacion.PreLiquidacion_Ins_Validaciones(oBE_Liquidacion).Split('|');


                        BE_Liquidacion oBE_Alerta = new BE_Liquidacion();
                        BL_Liquidacion BL_oAletras = new BL_Liquidacion();
                        oBE_Alerta.sNroVolante = txtDocumento.Text;
                        oBE_Alerta.sTipoMensaje = "1";
                        oBE_Alerta.sOperacion = HdTxtIdOperacion.Text;
                        //oBE_Alerta.sOperacion = DplOperacion.SelectedValue;
                        //oBE_Alerta.iIdOperacion = Convert.ToInt32(DplOperacion.SelectedValue);
                        String[] mensaje;
                        mensaje = BL_oAletras.PreLiquidacion_EnviarAlertas(oBE_Alerta).Split('|');
                    }

                    //oBE_Liquidacion.iIdCliente = Convert.ToInt32(HfIdCliente.Value);
                    oBE_Liquidacion.sObservacion = String.Empty;
                    //oBE_Liquidacion.iIdSuperNegocio_2 = Convert.ToInt32(dplLineaNegocio.SelectedValue);
                    oBE_Liquidacion.iIdSuperNegocio_2 = 89;
                    oBE_Liquidacion.iIdOperacion = Convert.ToInt32(HdTxtIdOperacion.Text);
                    oBE_Liquidacion.sNumeroDo = txtsDocOrigen.Text;
                    oBE_Liquidacion.iIdClienteFacturar = Convert.ToInt32(TxtIdCliente.Text);
                    oBE_Liquidacion.iIdAgAduana = null;

                    //if (HfIdAcuerdo.Value.Trim().Length > 0)
                    //{
                    //    oBE_Liquidacion.iIdAcuerdo_2 = Convert.ToInt32(TxtIdAcuerdo.Text);
                    //}

                    //oBE_Liquidacion.iIdMoneda = Convert.ToInt32(TxtIdMoneda.Text);
                    //  oBE_Liquidacion.iIdMoneda = 24;
                    oBE_Liquidacion.iIdMoneda = Convert.ToInt32(HdIdMoneda);
                    //oBE_Liquidacion.dAfecto = Convert.ToDecimal(lblAfecto.Text);
                    //oBE_Liquidacion.dInafecto = 0;
                    //oBE_Liquidacion.dIgv = Convert.ToDecimal(lblIgv.Text);
                    //oBE_Liquidacion.dMontoBruto = Convert.ToDecimal(lblTotal.Text);
                    //oBE_Liquidacion.dMontoNeto = Convert.ToDecimal(lblAfecto.Text);
                    //oBE_Liquidacion.dDescuento = Convert.ToDecimal(lblDescuento.Text);     
                    oBE_Liquidacion.dAfecto = Convert.ToDecimal(AfectoTotal);
                    oBE_Liquidacion.dInafecto = 0;
                    oBE_Liquidacion.dIgv = Convert.ToDecimal(IgvTotal);
                    oBE_Liquidacion.dMontoBruto = Convert.ToDecimal(TotalTotal);
                    oBE_Liquidacion.dMontoNeto = Convert.ToDecimal(AfectoTotal);
                    oBE_Liquidacion.dDescuento = Convert.ToDecimal(DescuentoTotal);

                    //Hd_Afecto.Value = dAfectoDolares.ToString();
                    //Hd_Inafecto.Value = dInafectoDolares.ToString();
                    //Hd_Descuento.Value = dDescuentoDolares.ToString();
                    //Hd_Igv.Value = dIgvDolares.ToString();
                    //Hd_Total.Value = dTotalDolares.ToString();              

                    oBE_Liquidacion.sUsuario = GlobalEntity.Instancia.Usuario;
                    oBE_Liquidacion.sNombrePc = GlobalEntity.Instancia.NombrePc;
                    oBE_Liquidacion.iIdTipoLiquidacion_2 = 0; //Pre-Liquidacion
                    oBE_Liquidacion.sFechaCalculo = Request.Form["DatePickerFinname"];


                    Decimal dAfecto = 0, dInafecto = 0, dIgv = 0, dTotal = 0;
                    Decimal dAfecto2 = 0, dInafecto2 = 0, dIgv2 = 0, dTotal2 = 0;
                    BL_Liquidacion oBL_DocumentoPago = new BL_Liquidacion();
                    Decimal IGV = oBL_DocumentoPago.IGV();

                    foreach (GridViewRow GrvRow in GvListaServicios.Rows)
                    {
                        if (GrvRow.RowType == DataControlRowType.DataRow)
                        {

                            if (Convert.ToInt32(GvListaServicios.DataKeys[GrvRow.RowIndex].Values["dAfecto"]) <= 0)
                            {
                                GrvRow.Cells[13].ForeColor = Color.FromName("#CA3634");
                            }
                        }
                        if ((GrvRow.FindControl("chkSeleccionarServicio") as CheckBox).Checked)
                        {
                            BE_Liquidacion oBE_LiquidacionDet = new BE_Liquidacion();
                            BL_Liquidacion BL_Liquidacion = new BL_Liquidacion();
                            String[] RespuestaServicios;
                            oBE_LiquidacionDet.iIdOrdSerDet = Convert.ToInt32(GvListaServicios.DataKeys[GrvRow.RowIndex].Values["iIdOrdSerDet"].ToString());
                            oBE_LiquidacionDet.iIdTarifa = Convert.ToInt32(GvListaServicios.DataKeys[GrvRow.RowIndex].Values["iIdTarifa"].ToString());
                            oBE_LiquidacionDet.dCantidad = Convert.ToInt32(GvListaServicios.DataKeys[GrvRow.RowIndex].Values["dCantidad"]);
                            oBE_LiquidacionDet.dPeso = Convert.ToDecimal(GvListaServicios.DataKeys[GrvRow.RowIndex].Values["dPeso"].ToString());
                            oBE_LiquidacionDet.dVolumen = 0;
                            // oBE_LiquidacionDet.dMontoUnitario = Convert.ToDecimal(GvListaServicios.DataKeys[GrvRow.RowIndex].Values["dMontoUnitario"].ToString()); dUnitarioDolares

                            //if(Convert.ToInt32(HdIdMoneda) == 24)
                            //{
                            oBE_LiquidacionDet.dMontoUnitario = Convert.ToDecimal(GvListaServicios.DataKeys[GrvRow.RowIndex].Values["dUnitarioGrabar"].ToString());
                            //dAfecto = Math.Round(Convert.ToDecimal(GvListaServicios.DataKeys[GrvRow.RowIndex].Values["dAfecto"].ToString()), 2);
                            //dAfecto = Math.Round(Convert.ToDecimal(GvListaServicios.DataKeys[GrvRow.RowIndex].Values["dTotSubTotalDolares"].ToString()), 2);
                            dAfecto = Convert.ToDecimal(GvListaServicios.DataKeys[GrvRow.RowIndex].Values["dAfectoGrabar"].ToString());
                            dInafecto = Math.Round(Convert.ToDecimal(GvListaServicios.DataKeys[GrvRow.RowIndex].Values["dInafectoGrabar"].ToString()), 2);
                            // dIgv = Math.Round(dAfecto * IGV, 3);
                            dIgv = Convert.ToDecimal(GvListaServicios.DataKeys[GrvRow.RowIndex].Values["dIgvGrabar"].ToString());
                            dTotal = dAfecto + dIgv + dInafecto;
                            oBE_LiquidacionDet.dDescuento = Convert.ToDecimal(GvListaServicios.DataKeys[GrvRow.RowIndex].Values["dDescuentoGrabar"].ToString());
                            oBE_LiquidacionDet.dDetraccion = Convert.ToDecimal(GvListaServicios.DataKeys[GrvRow.RowIndex].Values["ddetraccionGrabar"].ToString());
                            oBE_LiquidacionDet.iIdMoneda = Convert.ToInt32(HdIdMoneda);


                            //==== TIPO DE CAMBIO === //
                            oBE_LiquidacionDet.dUnitarioGrabar2 = Convert.ToDecimal(GvListaServicios.DataKeys[GrvRow.RowIndex].Values["dUnitarioGrabar2"].ToString());
                            dAfecto2 = Convert.ToDecimal(GvListaServicios.DataKeys[GrvRow.RowIndex].Values["dAfectoGrabar2"].ToString());
                            dInafecto2 = Math.Round(Convert.ToDecimal(GvListaServicios.DataKeys[GrvRow.RowIndex].Values["dInafectoGrabar2"].ToString()), 2);
                            // dIgv = Math.Round(dAfecto * IGV, 3);
                            dIgv2 = Convert.ToDecimal(GvListaServicios.DataKeys[GrvRow.RowIndex].Values["dIgvGrabar2"].ToString());
                            dTotal2 = dAfecto2 + dIgv2 + dInafecto2;
                            oBE_LiquidacionDet.dDescuentoGrabar2 = Convert.ToDecimal(GvListaServicios.DataKeys[GrvRow.RowIndex].Values["dDescuentoGrabar2"].ToString());
                            oBE_LiquidacionDet.ddetraccionGrabar2 = Convert.ToDecimal(GvListaServicios.DataKeys[GrvRow.RowIndex].Values["ddetraccionGrabar2"].ToString());
                            oBE_LiquidacionDet.idMonedaGrabar2 = Convert.ToInt32(HdIdMoneda2);

                            if (GvListaServicios.DataKeys[GrvRow.RowIndex].Values["sTipoMoneda"].ToString() == "$")
                            {
                                oBE_LiquidacionDet.idMonedaVisual = 24;
                            }
                            else
                                oBE_LiquidacionDet.idMonedaVisual = 23;


                            oBE_LiquidacionDet.dUnitarioVisual = Convert.ToDecimal(GvListaServicios.DataKeys[GrvRow.RowIndex].Values["dMontoUnitario"].ToString());
                            oBE_LiquidacionDet.dMontoTotalVisual = Convert.ToDecimal(GvListaServicios.DataKeys[GrvRow.RowIndex].Values["dAfecto"].ToString());

                            if (dAfecto <= 0)
                            {

                                //GRABAR LAS VALIDACIONES
                                String[] RespuestaTarifa;
                                BE_Liquidacion oBE_LiquidacionVal = new BE_Liquidacion();
                                BL_Liquidacion oBL_LiquidacionVal = new BL_Liquidacion();

                                oBE_LiquidacionVal.iIdSolitudPreLiq = Convert.ToInt32(IdSolicitudPreLiq);
                                oBE_LiquidacionVal.iIdValidacion = 0; //No Cumple la validacion
                                oBE_LiquidacionVal.iTipoProceso = 2; //Para Tarifa 0
                                RespuestaTarifa = oBL_LiquidacionVal.PreLiquidacion_Ins_Validaciones(oBE_LiquidacionVal).Split('|');

                            }

                            BL_Liquidacion oBL_LiquidacionServicio = new BL_Liquidacion();
                            String[] RespuestaServicio;
                            RespuestaServicio = oBL_LiquidacionServicio.PreLiquidacion_Lis_ValidacionesServicioPendiente(oBE_LiquidacionDet).Split('|');

                            if (RespuestaServicio[0] == "0") //Servicios Pendientes.
                            {

                                String[] RespuestaServicio2;
                                BE_Liquidacion oBE_LiquidacionVal = new BE_Liquidacion();
                                BL_Liquidacion oBL_LiquidacionVal = new BL_Liquidacion();

                                oBE_LiquidacionVal.iIdSolitudPreLiq = Convert.ToInt32(IdSolicitudPreLiq);
                                oBE_LiquidacionVal.iIdValidacion = 0; //No Cumple la validacion
                                oBE_LiquidacionVal.iTipoProceso = 3; //Para SERVICIOS PENDIENTES
                                RespuestaServicio2 = oBL_LiquidacionVal.PreLiquidacion_Ins_Validaciones(oBE_LiquidacionVal).Split('|');
                            }

                            //dInafecto = Math.Round(Convert.ToDecimal(GvListaServicios.DataKeys[GrvRow.RowIndex].Values["dInafecto"].ToString()), 2);


                            oBE_LiquidacionDet.dAfecto = dAfecto;
                            oBE_LiquidacionDet.dInafecto = dInafecto;
                            oBE_LiquidacionDet.dIgv = dIgv;
                            oBE_LiquidacionDet.dMontoNeto = dAfecto + dInafecto;
                            oBE_LiquidacionDet.dMontoBruto = dTotal;

                            // === TIPO DE CAMBIO === //
                            oBE_LiquidacionDet.dAfectoGrabar2 = dAfecto2;
                            oBE_LiquidacionDet.dInafectoGrabar2 = dInafecto2;
                            oBE_LiquidacionDet.dIgvGrabar2 = dIgv2;
                            oBE_LiquidacionDet.dMontoNetoGrabar2 = dAfecto2 + dInafecto2;
                            oBE_LiquidacionDet.dMontoBrutoGrabar2 = dTotal2;


                            oBE_LiquidacionDet.sOrigenPqteSLI = GvListaServicios.DataKeys[GrvRow.RowIndex].Values["sOrigenPqteSLI"].ToString();
                            oBE_LiquidacionDet.sMarcaSLI = GvListaServicios.DataKeys[GrvRow.RowIndex].Values["sMarcaSLI"].ToString();

                            oBE_LiquidacionDet.sUsuario = GlobalEntity.Instancia.Usuario;
                            oBE_LiquidacionDet.sNombrePc = GlobalEntity.Instancia.NombrePc;
                            oBE_LiquidacionList.Add(oBE_LiquidacionDet);

                            //INSERTAMOS EN LA TABLA DE SERVICIOS VALIDACION.
                            oBE_LiquidacionDet.iIdSolitudPreLiq = IdSolicitudPreLiq;
                            oBE_LiquidacionDet.iIdOrdSer = Convert.ToInt32(GvListaServicios.DataKeys[GrvRow.RowIndex].Values["iIdOrdSer"].ToString());
                            oBE_LiquidacionDet.iIdDocOriDet = Convert.ToInt32(GvListaServicios.DataKeys[GrvRow.RowIndex].Values["iIdDocOriDet"].ToString());
                            oBE_LiquidacionDet.sDescripcionServicio = GvListaServicios.DataKeys[GrvRow.RowIndex].Values["sDescripcionServicio"].ToString();
                            //oBE_LiquidacionDet.iIdMoneda = Convert.ToInt32(TxtIdMoneda.Text);
                            // oBE_LiquidacionDet.iIdMoneda = 24;


                            RespuestaServicios = oBL_Liquidacion.PreLiquidacion_Ins_ServiciosPreValidacion(oBE_LiquidacionDet).ToString().Split('|');
                        }

                    }

                    foreach (GridViewRow GrvRow in GrvListadoAlmacenaje.Rows)
                    {
                        if (GrvRow.RowType == DataControlRowType.DataRow)
                        {

                            if (Convert.ToInt32(GrvListadoAlmacenaje.DataKeys[GrvRow.RowIndex].Values["dAfecto"]) <= 0)
                            {
                                GrvRow.Cells[13].ForeColor = Color.FromName("#CA3634");
                            }
                        }

                        if ((GrvRow.FindControl("chkSeleccionarAlmacenaje") as CheckBox).Checked)
                        {
                            BE_Liquidacion oBE_LiquidacionDet = new BE_Liquidacion();

                            oBE_LiquidacionDet.iIdOrdSerDet = Convert.ToInt32(GrvListadoAlmacenaje.DataKeys[GrvRow.RowIndex].Values["iIdOrdSerDet"].ToString());
                            oBE_LiquidacionDet.iIdTarifa = Convert.ToInt32(GrvListadoAlmacenaje.DataKeys[GrvRow.RowIndex].Values["iIdTarifa"].ToString());
                            oBE_LiquidacionDet.dCantidad = Convert.ToInt32(GrvListadoAlmacenaje.DataKeys[GrvRow.RowIndex].Values["dCantidad"]);
                            oBE_LiquidacionDet.dPeso = Convert.ToDecimal(GrvListadoAlmacenaje.DataKeys[GrvRow.RowIndex].Values["dPeso"].ToString());
                            oBE_LiquidacionDet.dVolumen = 0;

                            //if (Convert.ToInt32(HdIdMoneda) == 24)
                            //{
                            oBE_LiquidacionDet.dMontoUnitario = Convert.ToDecimal(GrvListadoAlmacenaje.DataKeys[GrvRow.RowIndex].Values["dUnitarioGrabar"].ToString());
                            //dAfecto = Math.Round(Convert.ToDecimal(GvListaServicios.DataKeys[GrvRow.RowIndex].Values["dAfecto"].ToString()), 2);
                            //dAfecto = Math.Round(Convert.ToDecimal(GvListaServicios.DataKeys[GrvRow.RowIndex].Values["dTotSubTotalDolares"].ToString()), 2);
                            dAfecto = Convert.ToDecimal(GrvListadoAlmacenaje.DataKeys[GrvRow.RowIndex].Values["dAfectoGrabar"].ToString());
                            dInafecto = Math.Round(Convert.ToDecimal(GrvListadoAlmacenaje.DataKeys[GrvRow.RowIndex].Values["dInafectoGrabar"].ToString()), 2);
                            dIgv = Math.Round(dAfecto * IGV, 3);
                            dTotal = dAfecto + dIgv + dInafecto;
                            oBE_LiquidacionDet.dDescuento = Convert.ToDecimal(GrvListadoAlmacenaje.DataKeys[GrvRow.RowIndex].Values["dDescuentoGrabar"].ToString());
                            oBE_LiquidacionDet.dDetraccion = Convert.ToDecimal(GrvListadoAlmacenaje.DataKeys[GrvRow.RowIndex].Values["ddetraccionGrabar"].ToString());
                            oBE_LiquidacionDet.iIdMoneda = Convert.ToInt32(HdIdMoneda);

                            //==== TIPO DE CAMBIO === //
                            oBE_LiquidacionDet.dUnitarioGrabar2 = Convert.ToDecimal(GrvListadoAlmacenaje.DataKeys[GrvRow.RowIndex].Values["dUnitarioGrabar2"].ToString());
                            dAfecto2 = Convert.ToDecimal(GrvListadoAlmacenaje.DataKeys[GrvRow.RowIndex].Values["dAfectoGrabar2"].ToString());
                            dInafecto2 = Math.Round(Convert.ToDecimal(GrvListadoAlmacenaje.DataKeys[GrvRow.RowIndex].Values["dInafectoGrabar2"].ToString()), 2);
                            // dIgv = Math.Round(dAfecto * IGV, 3);
                            dIgv2 = Convert.ToDecimal(GrvListadoAlmacenaje.DataKeys[GrvRow.RowIndex].Values["dIgvGrabar2"].ToString());
                            dTotal2 = dAfecto2 + dIgv2 + dInafecto2;
                            oBE_LiquidacionDet.dDescuentoGrabar2 = Convert.ToDecimal(GrvListadoAlmacenaje.DataKeys[GrvRow.RowIndex].Values["dDescuentoGrabar2"].ToString());
                            oBE_LiquidacionDet.ddetraccionGrabar2 = Convert.ToDecimal(GrvListadoAlmacenaje.DataKeys[GrvRow.RowIndex].Values["ddetraccionGrabar2"].ToString());
                            oBE_LiquidacionDet.idMonedaGrabar2 = Convert.ToInt32(HdIdMoneda2);

                            if (GrvListadoAlmacenaje.DataKeys[GrvRow.RowIndex].Values["sTipoMoneda"].ToString() == "$")
                            {
                                oBE_LiquidacionDet.idMonedaVisual = 24;
                            }
                            else
                                oBE_LiquidacionDet.idMonedaVisual = 23;


                            oBE_LiquidacionDet.dUnitarioVisual = Convert.ToDecimal(GrvListadoAlmacenaje.DataKeys[GrvRow.RowIndex].Values["dMontoUnitario"].ToString());
                            oBE_LiquidacionDet.dMontoTotalVisual = Convert.ToDecimal(GrvListadoAlmacenaje.DataKeys[GrvRow.RowIndex].Values["dAfecto"].ToString());
                            if (dAfecto <= 0)
                            {

                                //GRABAR LAS VALIDACIONES
                                String[] RespuestaTarifa;
                                BE_Liquidacion oBE_LiquidacionVal = new BE_Liquidacion();
                                BL_Liquidacion oBL_LiquidacionVal = new BL_Liquidacion();

                                oBE_LiquidacionVal.iIdSolitudPreLiq = Convert.ToInt32(IdSolicitudPreLiq);
                                oBE_LiquidacionVal.iIdValidacion = 0; //No Cumple la validacion
                                oBE_LiquidacionVal.iTipoProceso = 2; //Para Tarifa 0
                                RespuestaTarifa = oBL_LiquidacionVal.PreLiquidacion_Ins_Validaciones(oBE_LiquidacionVal).Split('|');
                            }

                            BL_Liquidacion oBL_LiquidacionServicio = new BL_Liquidacion();
                            String[] RespuestaServicio;
                            RespuestaServicio = oBL_LiquidacionServicio.PreLiquidacion_Lis_ValidacionesServicioPendiente(oBE_LiquidacionDet).Split('|');

                            if (RespuestaServicio[0] == "0") //Servicios Pendientes.
                            {


                                String[] RespuestaServicio2;
                                BE_Liquidacion oBE_LiquidacionVal = new BE_Liquidacion();
                                BL_Liquidacion oBL_LiquidacionVal = new BL_Liquidacion();

                                oBE_LiquidacionVal.iIdSolitudPreLiq = Convert.ToInt32(IdSolicitudPreLiq);
                                oBE_LiquidacionVal.iIdValidacion = 0; //No Cumple la validacion
                                oBE_LiquidacionVal.iTipoProceso = 3; //Para SERVICIOS PENDIENTES
                                RespuestaServicio2 = oBL_LiquidacionVal.PreLiquidacion_Ins_Validaciones(oBE_LiquidacionVal).Split('|');
                            }

                            //dInafecto = Math.Round(Convert.ToDecimal(GrvListadoAlmacenaje.DataKeys[GrvRow.RowIndex].Values["dInafecto"].ToString()), 2);
                            //  dInafecto = Math.Round(Convert.ToDecimal(GrvListadoAlmacenaje.DataKeys[GrvRow.RowIndex].Values["dInafectoDolares"].ToString()), 2);
                            // dIgv = Math.Round(Convert.ToDecimal(GrvListadoAlmacenaje.DataKeys[GrvRow.RowIndex].Values["dIgv"].ToString()), 2);
                            // dIgv = Math.Round(Convert.ToDecimal(GrvListadoAlmacenaje.DataKeys[GrvRow.RowIndex].Values["dTotIgvDolares"].ToString()), 3);
                            // dIgv = Convert.ToDecimal(GrvListadoAlmacenaje.DataKeys[GrvRow.RowIndex].Values["dTotIgvDolares"].ToString());
                            // dTotal = Math.Round(Convert.ToDecimal(GrvListadoAlmacenaje.DataKeys[GrvRow.RowIndex].Values["dTotal"].ToString()), 2);
                            //dTotal = Math.Round(Convert.ToDecimal(GrvListadoAlmacenaje.DataKeys[GrvRow.RowIndex].Values["dTotTotalDolares"].ToString()), 3);
                            // dTotal = Convert.ToDecimal(GrvListadoAlmacenaje.DataKeys[GrvRow.RowIndex].Values["dTotTotalDolares"].ToString());

                            oBE_LiquidacionDet.dAfecto = dAfecto;
                            oBE_LiquidacionDet.dInafecto = dInafecto;
                            oBE_LiquidacionDet.dIgv = dIgv;

                            oBE_LiquidacionDet.dMontoNeto = dAfecto + dInafecto;
                            oBE_LiquidacionDet.dMontoBruto = dTotal;

                            //TIPÓ CAMBIO
                            oBE_LiquidacionDet.dAfectoGrabar2 = dAfecto2;
                            oBE_LiquidacionDet.dInafectoGrabar2 = dInafecto2;
                            oBE_LiquidacionDet.dIgvGrabar2 = dIgv2;

                            oBE_LiquidacionDet.dMontoNetoGrabar2 = dAfecto2 + dInafecto2;
                            oBE_LiquidacionDet.dMontoBrutoGrabar2 = dTotal2;



                            //  oBE_LiquidacionDet.dDescuento = Convert.ToDecimal(GrvListadoAlmacenaje.DataKeys[GrvRow.RowIndex].Values["dTotDescuentoDolares"].ToString());
                            //  oBE_LiquidacionDet.dDetraccion = Convert.ToDecimal(GrvListadoAlmacenaje.DataKeys[GrvRow.RowIndex].Values["dDetraccionDolares"].ToString());

                            oBE_LiquidacionDet.sOrigenPqteSLI = GrvListadoAlmacenaje.DataKeys[GrvRow.RowIndex].Values["sOrigenPqteSLI"].ToString();
                            oBE_LiquidacionDet.sMarcaSLI = GrvListadoAlmacenaje.DataKeys[GrvRow.RowIndex].Values["sMarcaSLI"].ToString();

                            oBE_LiquidacionDet.sUsuario = GlobalEntity.Instancia.Usuario;
                            oBE_LiquidacionDet.sNombrePc = GlobalEntity.Instancia.NombrePc;
                            oBE_LiquidacionDet.sIdDua = sIdDua;

                            if (oBE_LiquidacionDet.iIdOrdSerDet > 0)
                                oBE_LiquidacionList.Add(oBE_LiquidacionDet);

                            //INSERTAMOS EN LA TABLA DE SERVICIOS VALIDACION.
                            String[] RespuestaServicios;
                            oBE_LiquidacionDet.iIdSolitudPreLiq = IdSolicitudPreLiq;
                            oBE_LiquidacionDet.iIdOrdSer = Convert.ToInt32(GrvListadoAlmacenaje.DataKeys[GrvRow.RowIndex].Values["iIdOrdSer"].ToString());
                            oBE_LiquidacionDet.iIdDocOriDet = Convert.ToInt32(GrvListadoAlmacenaje.DataKeys[GrvRow.RowIndex].Values["iIdDocOriDet"].ToString());
                            oBE_LiquidacionDet.sDescripcionServicio = GrvListadoAlmacenaje.DataKeys[GrvRow.RowIndex].Values["sDescripcionServicio"].ToString();
                            //oBE_LiquidacionDet.iIdMoneda = Convert.ToInt32(TxtIdMoneda.Text);
                            //oBE_LiquidacionDet.iIdMoneda = 24;
                            //oBE_LiquidacionDet.iIdMoneda = Convert.ToInt32(HdIdMoneda);

                            RespuestaServicios = oBL_Liquidacion.PreLiquidacion_Ins_ServiciosPreValidacion(oBE_LiquidacionDet).ToString().Split('|');
                        }
                    }

                    //ENVIAR ALERTAS.

                    BE_Liquidacion oBE_AlertaS = new BE_Liquidacion();
                    BL_Liquidacion BL_oAlertas = new BL_Liquidacion();
                    List<BE_Liquidacion> oListValidaciones = new List<BE_Liquidacion>();
                    oBE_AlertaS.iIdSolitudPreLiq = Convert.ToInt32(IdSolicitudPreLiq);
                    oListValidaciones = BL_oAlertas.Listar_Estados_ParaAlertas(oBE_AlertaS);

                    if (oListValidaciones[0].sValiacionServicio == "0")
                    {
                        //ENVIAR CORREO AL AREA CORRESPONDIENTE
                        BE_Liquidacion oBE_Alerta = new BE_Liquidacion();
                        BL_Liquidacion BL_oAletras = new BL_Liquidacion();
                        oBE_Alerta.sNroVolante = txtNroVolante.Text;
                        oBE_Alerta.sTipoMensaje = "3";
                        oBE_Alerta.sOperacion = HdTxtIdOperacion.Text;
                        String[] mensaje;

                        mensaje = BL_oAletras.PreLiquidacion_EnviarAlertas(oBE_Alerta).Split('|');
                    }

                    if (oListValidaciones[0].sValidacionTarifa == "0")
                    {
                        //ENVIAR MENSAJES CORREO ELECTRONICO.
                        BE_Liquidacion oBE_Alerta = new BE_Liquidacion();
                        BL_Liquidacion BL_oAletras = new BL_Liquidacion();
                        oBE_Alerta.sNroVolante = txtNroVolante.Text;
                        oBE_Alerta.sTipoMensaje = "2";
                        oBE_Alerta.sOperacion = HdTxtIdOperacion.Text;
                        String[] mensaje;
                        mensaje = BL_oAletras.PreLiquidacion_EnviarAlertas(oBE_Alerta).Split('|');
                    }
                }
                catch (Exception ex)
                {
                    SCA_MsgInformacion("Error Catch | 1");
                    return;
                }

                    try
                    {
                        int vl_iCodigo = 0;
                        String vl_sMessage = string.Empty;
                        String[] oResultadoTransaccionTx = oBL_Liquidacion.Insertar_PreLiquidacion_2(oBE_Liquidacion, oBE_LiquidacionList).Split('|');
                  
                        for (int i = 0; i < oResultadoTransaccionTx.Length; i++)
                        {
                            if (i == 0)
                            {
                                vl_iCodigo = Convert.ToInt32(oResultadoTransaccionTx[i]);

                            }
                            else
                            {
                                vl_sMessage += oResultadoTransaccionTx[i];
                            }
                        }

                        if (vl_iCodigo < 1)
                        {
                            SCA_MsgInformacion(vl_sMessage);
                            //SCA_MsgInformacion(vl_sMessage + " " + vl_iCodigo);
                            lblAfecto.Text = "-";
                            lblDescuento.Text = "-";
                            lblIgv.Text = "-";
                        lblTotal2.Text = "-";
                        lblTotal1.Text = "-";
                        txtsDocOrigen.Visible = false;
                        txtsDocOrigen.Text = "";
                        lblDocOrigen.Text = "BL";
                        lblDocOrigen.Visible = false;
                        upCabecera.Update();

                        upSubTotalDolares.Update();
                            txtsDocOrigen.Text = "";
                            txtCliente.Text = "";
                            txtClienteAcuerdo.Text = "";
                            TxtNroAcuerdo.Text = "";
                        txtVolBook.Text = "DOCUMENTO";
                        // DplOperacion.SelectedValue = "-1";
                        lblOperacion.Text = "--";
                        lblNomFecha.Text = "Fecha Retiro";
                        upCabecera.Update();
                         // ScriptManager.RegisterStartupScript(this, GetType(), "mensaje4", "OcultarupTablasRefrescar();", true);


                        List<BE_Liquidacion> oLista_Liquidacion = new List<BE_Liquidacion>();
                            BE_Liquidacion oBE_Liquidacionn = new BE_Liquidacion();
                            //oLista_Acuerdo.Add(oBE_AcuerdoComision);
                            GrvListadoAlmacenaje.DataSource = oLista_Liquidacion;
                            GrvListadoAlmacenaje.DataBind();

                            List<BE_Liquidacion> oLista_LiquidacionV2 = new List<BE_Liquidacion>();
                            BE_Liquidacion oBE_LiquidacionV2 = new BE_Liquidacion();
                            //oLista_Acuerdo.Add(oBE_AcuerdoComision);
                            GvListaServicios.DataSource = oLista_LiquidacionV2;
                            GvListaServicios.DataBind();
                             // TxtIdPreLiquidacion.Text = vl_iCodigo.ToString();
                            ViewState["ListaServicios"] = "0";
                            return;
                        }
                        else
                        {
                            SCA_MsgInformacion(vl_sMessage);
                            lblAfecto.Text = "-";
                            lblDescuento.Text = "-";
                            lblIgv.Text = "-";
                        lblTotal2.Text = "-";
                        lblTotal1.Text = "-";
                        txtsDocOrigen.Visible = false;
                        txtsDocOrigen.Text = "";
                        lblDocOrigen.Text = "BL";
                        lblDocOrigen.Visible = false;
                        lblNomFecha.Text = "Fecha Retiro";

                        upSubTotalDolares.Update();
                            txtsDocOrigen.Text = "";
                            txtCliente.Text = "";
                            txtClienteAcuerdo.Text = "";
                            TxtNroAcuerdo.Text = "";
                            txtDocumento.Text = "";
                            UpdatePanel4.Update();
                        lblOperacion.Text = "--";
                        // DplOperacion.SelectedValue = "-1";
                        // UpdatePanel5.Update();
                        upCabecera.Update();
                        txtVolBook.Text = "DOCUMENTO";
                      //  DplOperacion.SelectedValue = "-1";
                        ScriptManager.RegisterStartupScript(this, GetType(), "mensaje4", "OcultarupTablasRefrescar();", true);
                        
                        List<BE_Liquidacion> oLista_Liquidacion = new List<BE_Liquidacion>();
                            BE_Liquidacion oBE_Liquidacionn = new BE_Liquidacion();
                            //oLista_Acuerdo.Add(oBE_AcuerdoComision);
                            GrvListadoAlmacenaje.DataSource = oLista_Liquidacion;
                            GrvListadoAlmacenaje.DataBind();

                            List<BE_Liquidacion> oLista_LiquidacionV2 = new List<BE_Liquidacion>();
                            BE_Liquidacion oBE_LiquidacionV2 = new BE_Liquidacion();
                            //oLista_Acuerdo.Add(oBE_AcuerdoComision);
                            GvListaServicios.DataSource = oLista_LiquidacionV2;
                            GvListaServicios.DataBind();
                        // upDocumentoOrigen.Update();
                        // TxtIdPreLiquidacion.Text = vl_iCodigo.ToString();
                            ViewState["ListaServicios"] = "0";
                            // ScriptManager.RegisterStartupScript(TxtIdPreLiquidacion, GetType(), "__mensaje__", String.Format("javascript: Imprimir('{0}');", TxtIdPreLiquidacion.Text), true);
                        }
                    }
                    catch (Exception ex)
                    {
                    SCA_MsgInformacion("Error en el catch | 2");
                    }
                   


                }



                // btnGrabar.Enabled = false;
            //}
            //catch (Exception ex)
            //{
            //    //
            //}
        }
        //if (AceptaMensaje.Equals("No"))
        //{
        //    try
        //    {
        //        vi_Validacion.Value = "";
        //        BE_Liquidacion oBE_Liquidacion = new BE_Liquidacion();
        //        BE_LiquidacionList oBE_LiquidacionList = new BE_LiquidacionList();
        //        BL_Liquidacion oBL_Liquidacion = new BL_Liquidacion();
        //        String sIdDua = string.Empty;
        //        String sTotal = string.Empty;
        //        String sSubTotal = string.Empty;
        //        String sIgv = string.Empty;
        //        String sDetraccion = string.Empty;
        //        String sDescuento = string.Empty;
        //        Int32 iRetorno = 0;
        //        Int32 Operacion = 0;
        //        Int32 IdSolicitudPreLiq = 0;
        //        Int32 IdMoneda = 0;
        //        if (DplOperacion.SelectedValue == "66") { Operacion = 66; }
        //        if (DplOperacion.SelectedValue == "65") { Operacion = 65; }
        //        String totalServicios = ViewState["ListaServicios"].ToString();
        //        //===========              

        //        //if (GvListaServicios.Rows.Count != 0 || GrvListadoAlmacenaje.Rows.Count != 0)
        //        //{       
        //        if ( Convert.ToInt32(totalServicios) > 0)
        //        {

                
        //            String[] Respuesta;
        //            String[] RespuestaValTarifa;
        //            oBE_Liquidacion.iIdDocOri = Convert.ToInt32(txtIdDocOri.Text);
        //            oBE_Liquidacion.sOrigenSolicitud = "Online";
        //            oBE_Liquidacion.iIdSuperNegocio_2 = Convert.ToInt32(dplLineaNegocio.SelectedValue);
        //            oBE_Liquidacion.sUsuario = GlobalEntity.Instancia.Usuario;
        //            oBE_Liquidacion.iIdLiq = null;
        //            oBE_Liquidacion.iIdCliente = GlobalEntity.Instancia.IdCliente;
        //            oBE_Liquidacion.iIdAcuerdo = Convert.ToInt32(TxtIdAcuerdo.Text);
        //            oBE_Liquidacion.sFechaCalculo = Request.Form["DatePickerFinname"];
        //            oBE_Liquidacion.sSituacion = "0"; //INCORRECTO

        //            Respuesta = oBL_Liquidacion.PreLiquidacion_Ins_SolicitudPreLiquidacion(oBE_Liquidacion).Split('|');
        //            IdSolicitudPreLiq = Convert.ToInt32(Respuesta[0]);
        //            oBE_Liquidacion.iIdSolitudPreLiq = IdSolicitudPreLiq;

        //            //if (Respuesta[0] == "-1")
        //            //{

        //            //}
                    
        //            if (Convert.ToInt32(Respuesta[0]) > 0)
        //            {
        //                String[] RespuestaAcuerdo;
        //                if (Convert.ToInt32(TxtIdAcuerdo.Text) >= 1)
        //                {        
        //                    oBE_Liquidacion.iIdAcuerdo = Convert.ToInt32(TxtIdAcuerdo.Text);
        //                    iRetorno = oBL_Liquidacion.ConsultarEstadoAcuerdo_2(oBE_Liquidacion.iIdAcuerdo);

        //                    if (iRetorno != 6)
        //                    {
        //                        oBE_Liquidacion.iIdSolitudPreLiq = Convert.ToInt32(IdSolicitudPreLiq);
        //                        oBE_Liquidacion.iIdValidacion = 0; //No Cumple la validacion
        //                        oBE_Liquidacion.iTipoProceso = 1;
        //                        RespuestaAcuerdo = oBL_Liquidacion.PreLiquidacion_Ins_Validaciones(oBE_Liquidacion).Split('|');

        //                        //ENVIA MENSAJE
        //                        BE_Liquidacion oBE_Alerta = new BE_Liquidacion();
        //                        BL_Liquidacion BL_oAletras = new BL_Liquidacion();
        //                        oBE_Alerta.sNroVolante = txtDocumento.Text;
        //                        oBE_Alerta.sTipoMensaje = "4";
        //                        oBE_Alerta.sOperacion = HdTxtIdOperacion.Text;
        //                        //oBE_Alerta.sOperacion = DplOperacion.SelectedValue;
        //                        //oBE_Alerta.iIdOperacion = Convert.ToInt32(DplOperacion.SelectedValue);
        //                        String[] mensaje;
        //                        mensaje = BL_oAletras.PreLiquidacion_EnviarAlertas(oBE_Alerta).Split('|');
        //                    }
        //                    else
        //                    {   //CUMPLE LA VALIDACION.
        //                        oBE_Liquidacion.iIdSolitudPreLiq = Convert.ToInt32(IdSolicitudPreLiq);
        //                        oBE_Liquidacion.iIdValidacion = 1; //Cumple la validacion
        //                        oBE_Liquidacion.iTipoProceso = 1;
        //                        RespuestaAcuerdo = oBL_Liquidacion.PreLiquidacion_Ins_Validaciones(oBE_Liquidacion).Split('|');
        //                    }
        //                }
        //                else
        //                {
        //                    oBE_Liquidacion.iIdSolitudPreLiq = Convert.ToInt32(IdSolicitudPreLiq);
        //                    oBE_Liquidacion.iIdValidacion = 0; //No Cumple la validacion
        //                    oBE_Liquidacion.iTipoProceso = 1;
        //                    RespuestaAcuerdo = oBL_Liquidacion.PreLiquidacion_Ins_Validaciones(oBE_Liquidacion).Split('|');


        //                    BE_Liquidacion oBE_Alerta = new BE_Liquidacion();
        //                    BL_Liquidacion BL_oAletras = new BL_Liquidacion();
        //                    oBE_Alerta.sNroVolante = txtDocumento.Text;
        //                    oBE_Alerta.sTipoMensaje = "1";
        //                    oBE_Alerta.sOperacion = HdTxtIdOperacion.Text;
        //                    //oBE_Alerta.sOperacion = DplOperacion.SelectedValue;
        //                    //oBE_Alerta.iIdOperacion = Convert.ToInt32(DplOperacion.SelectedValue);
        //                    String[] mensaje;
        //                    mensaje = BL_oAletras.PreLiquidacion_EnviarAlertas(oBE_Alerta).Split('|');
        //                }

        //                //NO CUMPLE LA VALIDACION DE ACEPTAR LAS TARIFAS MOSTRADAS.
        //                oBE_Liquidacion.iIdSolitudPreLiq = Convert.ToInt32(IdSolicitudPreLiq);
        //                oBE_Liquidacion.iIdValidacion = 0; //No Cumple la validacion
        //                oBE_Liquidacion.iTipoProceso = 4;
        //                RespuestaValTarifa = oBL_Liquidacion.PreLiquidacion_Ins_Validaciones(oBE_Liquidacion).Split('|');


        //                oBE_Liquidacion.sObservacion = String.Empty;                       
        //                oBE_Liquidacion.iIdSuperNegocio_2 = Convert.ToInt32(dplLineaNegocio.SelectedValue);
        //                oBE_Liquidacion.iIdOperacion = Convert.ToInt32(Operacion);
        //                oBE_Liquidacion.sNumeroDo = txtsDocOrigen.Text;
        //                oBE_Liquidacion.iIdClienteFacturar = Convert.ToInt32(TxtIdCliente.Text);
        //                oBE_Liquidacion.iIdAgAduana = null;                        

        //                oBE_Liquidacion.iIdMoneda = Convert.ToInt32(TxtIdMoneda.Text);                           
        //                oBE_Liquidacion.dAfecto = Convert.ToDecimal(HdtxtAfecto.Text);
        //                oBE_Liquidacion.dInafecto = 0;
        //                oBE_Liquidacion.dIgv = Convert.ToDecimal(HdtxtIGV.Text);
        //                oBE_Liquidacion.dMontoBruto = Convert.ToDecimal(txtTotalHd.Text);
        //                oBE_Liquidacion.dMontoNeto = Convert.ToDecimal(HdtxtAfecto.Text);
        //                oBE_Liquidacion.dDescuento = Convert.ToDecimal(HdtxtDescuento.Text);

        //                oBE_Liquidacion.sUsuario = GlobalEntity.Instancia.Usuario;
        //                oBE_Liquidacion.sNombrePc = GlobalEntity.Instancia.NombrePc;
        //                oBE_Liquidacion.iIdTipoLiquidacion_2 = 0; //Pre-Liquidacion
        //                oBE_Liquidacion.sFechaCalculo = Request.Form["DatePickerFinname"];


        //                Decimal dAfecto = 0, dInafecto = 0, dIgv = 0, dTotal = 0;
        //                BL_Liquidacion oBL_DocumentoPago = new BL_Liquidacion();
        //                Decimal IGV = oBL_DocumentoPago.IGV();

        //                foreach (GridViewRow GrvRow in GvListaServicios.Rows)
        //                {
        //                    if (GrvRow.RowType == DataControlRowType.DataRow)
        //                    {

        //                        if (Convert.ToInt32(GvListaServicios.DataKeys[GrvRow.RowIndex].Values["dAfecto"]) <= 0)
        //                        {
        //                            GrvRow.Cells[13].ForeColor = Color.FromName("#CA3634");
        //                        }
        //                    }
        //                    if ((GrvRow.FindControl("chkSeleccionarServicio") as CheckBox).Checked)
        //                    {
        //                        BE_Liquidacion oBE_LiquidacionDet = new BE_Liquidacion();
        //                        BL_Liquidacion BL_Liquidacion = new BL_Liquidacion();
        //                        String[] RespuestaServicios;
        //                        oBE_LiquidacionDet.iIdOrdSerDet = Convert.ToInt32(GvListaServicios.DataKeys[GrvRow.RowIndex].Values["iIdOrdSerDet"].ToString());
        //                        oBE_LiquidacionDet.iIdTarifa = Convert.ToInt32(GvListaServicios.DataKeys[GrvRow.RowIndex].Values["iIdTarifa"].ToString());
        //                        oBE_LiquidacionDet.dCantidad = Convert.ToInt32(GvListaServicios.DataKeys[GrvRow.RowIndex].Values["dCantidad"]);
        //                        oBE_LiquidacionDet.dPeso = Convert.ToDecimal(GvListaServicios.DataKeys[GrvRow.RowIndex].Values["dPeso"].ToString());
        //                        oBE_LiquidacionDet.dVolumen = 0;
        //                        oBE_LiquidacionDet.dMontoUnitario = Convert.ToDecimal(GvListaServicios.DataKeys[GrvRow.RowIndex].Values["dMontoUnitario"].ToString());

        //                        dAfecto = Math.Round(Convert.ToDecimal(GvListaServicios.DataKeys[GrvRow.RowIndex].Values["dAfecto"].ToString()), 2);
        //                        if (dAfecto <= 0)
        //                        {                                    

        //                            //GRABAR LAS VALIDACIONES
        //                            String[] RespuestaTarifa;
        //                            BE_Liquidacion oBE_LiquidacionVal = new BE_Liquidacion();
        //                            BL_Liquidacion oBL_LiquidacionVal = new BL_Liquidacion();

        //                            oBE_LiquidacionVal.iIdSolitudPreLiq = Convert.ToInt32(IdSolicitudPreLiq);
        //                            oBE_LiquidacionVal.iIdValidacion = 0; //No Cumple la validacion
        //                            oBE_LiquidacionVal.iTipoProceso = 2; //Para Tarifa 0
        //                            RespuestaTarifa = oBL_LiquidacionVal.PreLiquidacion_Ins_Validaciones(oBE_LiquidacionVal).Split('|');

        //                        }

        //                        BL_Liquidacion oBL_LiquidacionServicio = new BL_Liquidacion();
        //                        String[] RespuestaServicio;
        //                        RespuestaServicio = oBL_LiquidacionServicio.PreLiquidacion_Lis_ValidacionesServicioPendiente(oBE_LiquidacionDet).Split('|');

        //                        if (RespuestaServicio[0] == "0") //Servicios Pendientes.
        //                        {
                                    
        //                            String[] RespuestaServicio2;
        //                            BE_Liquidacion oBE_LiquidacionVal = new BE_Liquidacion();
        //                            BL_Liquidacion oBL_LiquidacionVal = new BL_Liquidacion();

        //                            oBE_LiquidacionVal.iIdSolitudPreLiq = Convert.ToInt32(IdSolicitudPreLiq);
        //                            oBE_LiquidacionVal.iIdValidacion = 0; //No Cumple la validacion
        //                            oBE_LiquidacionVal.iTipoProceso = 3; //Para SERVICIOS PENDIENTES
        //                            RespuestaServicio2 = oBL_LiquidacionVal.PreLiquidacion_Ins_Validaciones(oBE_LiquidacionVal).Split('|');
        //                        }
                                

        //                        dInafecto = Math.Round(Convert.ToDecimal(GvListaServicios.DataKeys[GrvRow.RowIndex].Values["dInafecto"].ToString()), 2);
        //                        dIgv = Math.Round(dAfecto * IGV, 2);
        //                        dTotal = dAfecto + dIgv + dInafecto;

        //                        oBE_LiquidacionDet.dAfecto = dAfecto;
        //                        oBE_LiquidacionDet.dInafecto = dInafecto;
        //                        oBE_LiquidacionDet.dIgv = dIgv;


        //                        oBE_LiquidacionDet.dMontoNeto = dAfecto + dInafecto;
        //                        oBE_LiquidacionDet.dMontoBruto = dTotal;


        //                        oBE_LiquidacionDet.dDescuento = Convert.ToDecimal(GvListaServicios.DataKeys[GrvRow.RowIndex].Values["dDescuento"].ToString());
        //                        oBE_LiquidacionDet.dDetraccion = Convert.ToDecimal(GvListaServicios.DataKeys[GrvRow.RowIndex].Values["dDetraccion"].ToString());

        //                        oBE_LiquidacionDet.sOrigenPqteSLI = GvListaServicios.DataKeys[GrvRow.RowIndex].Values["sOrigenPqteSLI"].ToString();
        //                        oBE_LiquidacionDet.sMarcaSLI = GvListaServicios.DataKeys[GrvRow.RowIndex].Values["sMarcaSLI"].ToString();

        //                        oBE_LiquidacionDet.sUsuario = GlobalEntity.Instancia.Usuario;
        //                        oBE_LiquidacionDet.sNombrePc = GlobalEntity.Instancia.NombrePc;
        //                        oBE_LiquidacionList.Add(oBE_LiquidacionDet);

        //                        //INSERTAMOS EN LA TABLA DE SERVICIOS VALIDACION.
        //                        oBE_LiquidacionDet.iIdSolitudPreLiq = IdSolicitudPreLiq;
        //                        oBE_LiquidacionDet.iIdOrdSer = Convert.ToInt32(GvListaServicios.DataKeys[GrvRow.RowIndex].Values["iIdOrdSer"].ToString());
        //                        oBE_LiquidacionDet.iIdDocOriDet = Convert.ToInt32(GvListaServicios.DataKeys[GrvRow.RowIndex].Values["iIdDocOriDet"].ToString());
        //                        oBE_LiquidacionDet.sDescripcionServicio = GvListaServicios.DataKeys[GrvRow.RowIndex].Values["sDescripcionServicio"].ToString();
        //                        oBE_LiquidacionDet.iIdMoneda = Convert.ToInt32(TxtIdMoneda.Text);

        //                        RespuestaServicios = oBL_Liquidacion.PreLiquidacion_Ins_ServiciosPreValidacion(oBE_LiquidacionDet).ToString().Split('|');
        //                    }

        //                }

        //                foreach (GridViewRow GrvRow in GrvListadoAlmacenaje.Rows)
        //                {
        //                    if (GrvRow.RowType == DataControlRowType.DataRow)
        //                    {

        //                        if (Convert.ToInt32(GrvListadoAlmacenaje.DataKeys[GrvRow.RowIndex].Values["dAfecto"]) <= 0)
        //                        {
        //                            GrvRow.Cells[13].ForeColor = Color.FromName("#CA3634");
        //                        }
        //                    }

        //                    if ((GrvRow.FindControl("chkSeleccionarAlmacenaje") as CheckBox).Checked)
        //                    {
        //                        BE_Liquidacion oBE_LiquidacionDet = new BE_Liquidacion();

        //                        oBE_LiquidacionDet.iIdOrdSerDet = Convert.ToInt32(GrvListadoAlmacenaje.DataKeys[GrvRow.RowIndex].Values["iIdOrdSerDet"].ToString());
        //                        oBE_LiquidacionDet.iIdTarifa = Convert.ToInt32(GrvListadoAlmacenaje.DataKeys[GrvRow.RowIndex].Values["iIdTarifa"].ToString());
        //                        oBE_LiquidacionDet.dCantidad = Convert.ToInt32(GrvListadoAlmacenaje.DataKeys[GrvRow.RowIndex].Values["dCantidad"]);
        //                        oBE_LiquidacionDet.dPeso = Convert.ToDecimal(GrvListadoAlmacenaje.DataKeys[GrvRow.RowIndex].Values["dPeso"].ToString());
        //                        oBE_LiquidacionDet.dVolumen = 0;
        //                        oBE_LiquidacionDet.dMontoUnitario = Convert.ToDecimal(GrvListadoAlmacenaje.DataKeys[GrvRow.RowIndex].Values["dMontoUnitario"].ToString());

        //                        dAfecto = Math.Round(Convert.ToDecimal(GrvListadoAlmacenaje.DataKeys[GrvRow.RowIndex].Values["dAfecto"].ToString()), 2);
        //                        if (dAfecto <= 0)
        //                        {
                                   

        //                            //GRABAR LAS VALIDACIONES
        //                            String[] RespuestaTarifa;
        //                            BE_Liquidacion oBE_LiquidacionVal = new BE_Liquidacion();
        //                            BL_Liquidacion oBL_LiquidacionVal = new BL_Liquidacion();

        //                            oBE_LiquidacionVal.iIdSolitudPreLiq = Convert.ToInt32(IdSolicitudPreLiq);
        //                            oBE_LiquidacionVal.iIdValidacion = 0; //No Cumple la validacion
        //                            oBE_LiquidacionVal.iTipoProceso = 2; //Para Tarifa 0
        //                            RespuestaTarifa = oBL_LiquidacionVal.PreLiquidacion_Ins_Validaciones(oBE_LiquidacionVal).Split('|');
        //                        }

        //                        BL_Liquidacion oBL_LiquidacionServicio = new BL_Liquidacion();
        //                        String[] RespuestaServicio;
        //                        RespuestaServicio = oBL_LiquidacionServicio.PreLiquidacion_Lis_ValidacionesServicioPendiente(oBE_LiquidacionDet).Split('|');

        //                        if (RespuestaServicio[0] == "0") //Servicios Pendientes.
        //                        {
                                    
        //                            String[] RespuestaServicio2;
        //                            BE_Liquidacion oBE_LiquidacionVal = new BE_Liquidacion();
        //                            BL_Liquidacion oBL_LiquidacionVal = new BL_Liquidacion();

        //                            oBE_LiquidacionVal.iIdSolitudPreLiq = Convert.ToInt32(IdSolicitudPreLiq);
        //                            oBE_LiquidacionVal.iIdValidacion = 0; //No Cumple la validacion
        //                            oBE_LiquidacionVal.iTipoProceso = 3; //Para SERVICIOS PENDIENTES
        //                            RespuestaServicio2 = oBL_LiquidacionVal.PreLiquidacion_Ins_Validaciones(oBE_LiquidacionVal).Split('|');
        //                        }                               

        //                        dInafecto = Math.Round(Convert.ToDecimal(GrvListadoAlmacenaje.DataKeys[GrvRow.RowIndex].Values["dInafecto"].ToString()), 2);
        //                        dIgv = Math.Round(Convert.ToDecimal(GrvListadoAlmacenaje.DataKeys[GrvRow.RowIndex].Values["dIgv"].ToString()), 2);
        //                        dTotal = Math.Round(Convert.ToDecimal(GrvListadoAlmacenaje.DataKeys[GrvRow.RowIndex].Values["dTotal"].ToString()), 2);

        //                        oBE_LiquidacionDet.dAfecto = dAfecto;
        //                        oBE_LiquidacionDet.dInafecto = dInafecto;
        //                        oBE_LiquidacionDet.dIgv = dIgv;

        //                        oBE_LiquidacionDet.dMontoNeto = dAfecto + dInafecto;
        //                        oBE_LiquidacionDet.dMontoBruto = dTotal;



        //                        oBE_LiquidacionDet.dDescuento = Convert.ToDecimal(GrvListadoAlmacenaje.DataKeys[GrvRow.RowIndex].Values["dDescuento"].ToString());
        //                        oBE_LiquidacionDet.dDetraccion = Convert.ToDecimal(GrvListadoAlmacenaje.DataKeys[GrvRow.RowIndex].Values["dDetraccion"].ToString());

        //                        oBE_LiquidacionDet.sOrigenPqteSLI = GrvListadoAlmacenaje.DataKeys[GrvRow.RowIndex].Values["sOrigenPqteSLI"].ToString();
        //                        oBE_LiquidacionDet.sMarcaSLI = GrvListadoAlmacenaje.DataKeys[GrvRow.RowIndex].Values["sMarcaSLI"].ToString();

        //                        oBE_LiquidacionDet.sUsuario = GlobalEntity.Instancia.Usuario;
        //                        oBE_LiquidacionDet.sNombrePc = GlobalEntity.Instancia.NombrePc;
        //                        oBE_LiquidacionDet.sIdDua = sIdDua;

        //                        if (oBE_LiquidacionDet.iIdOrdSerDet > 0)
        //                            oBE_LiquidacionList.Add(oBE_LiquidacionDet);

        //                        //INSERTAMOS EN LA TABLA DE SERVICIOS VALIDACION.
        //                        String[] RespuestaServicios;
        //                        oBE_LiquidacionDet.iIdSolitudPreLiq = IdSolicitudPreLiq;
        //                        oBE_LiquidacionDet.iIdOrdSer = Convert.ToInt32(GrvListadoAlmacenaje.DataKeys[GrvRow.RowIndex].Values["iIdOrdSer"].ToString());
        //                        oBE_LiquidacionDet.iIdDocOriDet = Convert.ToInt32(GrvListadoAlmacenaje.DataKeys[GrvRow.RowIndex].Values["iIdDocOriDet"].ToString());
        //                        oBE_LiquidacionDet.sDescripcionServicio = GrvListadoAlmacenaje.DataKeys[GrvRow.RowIndex].Values["sDescripcionServicio"].ToString();
        //                        oBE_LiquidacionDet.iIdMoneda = Convert.ToInt32(TxtIdMoneda.Text);

        //                        RespuestaServicios = oBL_Liquidacion.PreLiquidacion_Ins_ServiciosPreValidacion(oBE_LiquidacionDet).ToString().Split('|');
        //                    }
        //                }
                        
        //            }

        //            //ENVIAR ALERTAS.

        //            BE_Liquidacion oBE_AlertaS = new BE_Liquidacion();
        //            BL_Liquidacion BL_oAlertas = new BL_Liquidacion();
        //            List<BE_Liquidacion> oListValidaciones = new List<BE_Liquidacion>();
        //            oBE_AlertaS.iIdSolitudPreLiq = Convert.ToInt32(IdSolicitudPreLiq);
        //            oListValidaciones = BL_oAlertas.Listar_Estados_ParaAlertas(oBE_AlertaS);

        //            if (oListValidaciones[0].sValiacionServicio == "0")
        //            {
        //                //ENVIAR CORREO AL AREA CORRESPONDIENTE
        //                BE_Liquidacion oBE_Alerta = new BE_Liquidacion();
        //                BL_Liquidacion BL_oAletras = new BL_Liquidacion();
        //                oBE_Alerta.sNroVolante = txtNroVolante.Text;
        //                oBE_Alerta.sTipoMensaje = "3";
        //                oBE_Alerta.sOperacion = HdTxtIdOperacion.Text;
        //                String[] mensaje;

        //                mensaje = BL_oAletras.PreLiquidacion_EnviarAlertas(oBE_Alerta).Split('|');
        //            }

        //            if (oListValidaciones[0].sValidacionTarifa == "0")
        //            {
        //                //ENVIAR MENSAJES CORREO ELECTRONICO.
        //                BE_Liquidacion oBE_Alerta = new BE_Liquidacion();
        //                BL_Liquidacion BL_oAletras = new BL_Liquidacion();
        //                oBE_Alerta.sNroVolante = txtNroVolante.Text;
        //                oBE_Alerta.sTipoMensaje = "2";
        //                oBE_Alerta.sOperacion = HdTxtIdOperacion.Text;
        //                String[] mensaje;
        //                mensaje = BL_oAletras.PreLiquidacion_EnviarAlertas(oBE_Alerta).Split('|');
        //            }

        //        }

        //        lblAfecto.Text = "-";
        //        lblDescuento.Text = "-";
        //        lblIgv.Text = "-";
        //        lblTotal.Text = "-";
        //        UpdatePanel2.Update();
        //        txtsDocOrigen.Text = "";
        //        txtCliente.Text = "";
        //        txtClienteAcuerdo.Text = "";
        //        TxtNroAcuerdo.Text = "";
        //        txtDocumento.Text = "";
        //        UpdatePanel4.Update();
        //        DplOperacion.SelectedValue = "-1";
        //        UpdatePanel5.Update();
        //        upCabecera.Update();

        //        List<BE_Liquidacion> oLista_Liquidacion = new List<BE_Liquidacion>();
        //        BE_Liquidacion oBE_Liquidacionn = new BE_Liquidacion();
        //        //oLista_Acuerdo.Add(oBE_AcuerdoComision);
        //        GrvListadoAlmacenaje.DataSource = oLista_Liquidacion;
        //        GrvListadoAlmacenaje.DataBind();

        //        List<BE_Liquidacion> oLista_LiquidacionV2 = new List<BE_Liquidacion>();
        //        BE_Liquidacion oBE_LiquidacionV2 = new BE_Liquidacion();
        //        //oLista_Acuerdo.Add(oBE_AcuerdoComision);
        //        GvListaServicios.DataSource = oLista_LiquidacionV2;
        //        GvListaServicios.DataBind();
        //        // upDocumentoOrigen.Update();
        //        // TxtIdPreLiquidacion.Text = vl_iCodigo.ToString();
        //        ViewState["ListaServicios"] = "0";



        //        // btnGrabar.Enabled = false;
        //    }
        //    catch (Exception ex)
        //    {
        //        //
        //    }
        //}

        
        
    }

    protected void GrvListadoAlmacenaje_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {


            DataKey dataKey;
            dataKey = this.GrvListadoAlmacenaje.DataKeys[e.Row.RowIndex];
            BE_AcuerdoComision oitem = (e.Row.DataItem as BE_AcuerdoComision);
            System.Web.UI.WebControls.CheckBox chkSeleccionAlmacenaje = (System.Web.UI.WebControls.CheckBox)e.Row.FindControl("chkSeleccionarAlmacenaje");
            //System.Web.UI.WebControls.CheckBox chkSeleccionGroup = (System.Web.UI.WebControls.CheckBox)e.Row.FindControl("chkAllQuitarGrp");
            chkSeleccionAlmacenaje.Enabled = false;
            chkSeleccionAlmacenaje.Checked = true;

            decimal dAfectoo = 0;
            decimal.TryParse(e.Row.Cells[14].Text, out dAfectoo);
            e.Row.Cells[14].Text = dAfectoo.ToString("N2");


            decimal dAfecto = 0;
            decimal.TryParse(e.Row.Cells[28].Text, out dAfecto);
            e.Row.Cells[28].Text = dAfecto.ToString("N2");

            decimal dUnitario = 0;
            decimal.TryParse(e.Row.Cells[12].Text, out dUnitario);
            e.Row.Cells[12].Text = dUnitario.ToString("N2");

            decimal dIgv = 0;
            decimal.TryParse(e.Row.Cells[16].Text, out dIgv);
            e.Row.Cells[16].Text = dIgv.ToString("N2");

            decimal dTotal = 0;
            decimal.TryParse(e.Row.Cells[18].Text, out dTotal);
            e.Row.Cells[18].Text = dTotal.ToString("N2");



            if (Convert.ToInt32(dataKey.Values["dAfecto"]) <= 0)
            {
                e.Row.Cells[13].ForeColor = Color.FromName("#CA3634");
                e.Row.Cells[14].ForeColor = Color.FromName("#CA3634");
            }

           

        }

        if(e.Row.RowType == DataControlRowType.Header)
        {
            if (DplMonedaGrabar.SelectedValue == "23")
            {
                //GvListaServicios.Columns[28].HeaderText = "Total S/.";
                e.Row.Cells[28].Text = "Total SOLES";
            }
            else
            {
                //GvListaServicios.Columns[28].HeaderText = "Total $";
                e.Row.Cells[28].Text = "Total DOLARES";
            }
        }
    }

    protected void GvListaServicios_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {


            DataKey dataKey;
            dataKey = this.GvListaServicios.DataKeys[e.Row.RowIndex];
           BE_Liquidacion oitem = (e.Row.DataItem as BE_Liquidacion);
            System.Web.UI.WebControls.CheckBox chkSeleccionServicio = (System.Web.UI.WebControls.CheckBox)e.Row.FindControl("chkSeleccionarServicio");
            
            //System.Web.UI.WebControls.CheckBox chkSeleccionGroup = (System.Web.UI.WebControls.CheckBox)e.Row.FindControl("chkAllQuitarGrp");
            chkSeleccionServicio.Enabled = false;           
            chkSeleccionServicio.Checked = true;


            decimal dAfectoo = 0;
            decimal.TryParse(e.Row.Cells[14].Text, out dAfectoo);
            e.Row.Cells[14].Text = dAfectoo.ToString("N2");


            decimal dAfecto = 0;
            decimal.TryParse(e.Row.Cells[28].Text, out dAfecto);
            e.Row.Cells[28].Text = dAfecto.ToString("N2");

            decimal dUnitario = 0;
            decimal.TryParse(e.Row.Cells[12].Text, out dUnitario);
            e.Row.Cells[12].Text = dUnitario.ToString("N2");

            //decimal dIgv = 0;
            //decimal.TryParse(e.Row.Cells[16].Text, out dIgv);
            //e.Row.Cells[16].Text = dIgv.ToString("N2");

            decimal dTotal = 0;
            decimal.TryParse(e.Row.Cells[18].Text, out dTotal);
            e.Row.Cells[18].Text = dTotal.ToString("N2");

            //decimal dTotalDolares = 0;
            //decimal.TryParse(e.Row.Cells[28].Text, out dTotalDolares);
            //e.Row.Cells[28].Text = dTotalDolares.ToString("N2");

            //decimal dTotalSoles = 0;
            //decimal.TryParse(e.Row.Cells[31].Text, out dTotalSoles);
            //e.Row.Cells[31].Text = dTotalSoles.ToString("N2");

            //decimal dTotalDolares = 0;
            //decimal.TryParse(e.Row.Cells[28].Text, out dTotalDolares);
            //e.Row.Cells[28].Text = dTotalDolares.ToString("N2");

            if (oitem.sMarcaSLI != "1")
            {
                if (Convert.ToInt32(dataKey.Values["dAfecto"]) <= 0)
                {
                    e.Row.Cells[13].ForeColor = Color.FromName("#CA3634");
                    e.Row.Cells[14].ForeColor = Color.FromName("#CA3634");
                }
            }
            
           
            for (int c = 1; c < e.Row.Cells.Count; c++)
            {
                if (oitem.sMarcaSLI == "1")
                {
                    //e.Row.Cells[c].BackColor = System.Drawing.Color.LightBlue;
                    //e.Row.Cells[c].ToolTip = "Servicio dentro del paquete SLI";
                    e.Row.Cells[c].Visible = false;
                    chkSeleccionServicio.Checked = false;
                }
            }

            //if (DplMonedaGrabar.SelectedValue == "23")
            //{
            //    //GvListaServicios.Columns[28].HeaderText = "Total S/.";
            //    e.Row.Cells[28].Text = "Total S/";
            //}
            //else
            //{
            //    GvListaServicios.Columns[28].HeaderText = "Total $";
                
            //}

        }

        if (e.Row.RowType == DataControlRowType.Header)
        {
            
            if (DplMonedaGrabar.SelectedValue == "23")
            {
                //GvListaServicios.Columns[28].HeaderText = "Total S/.";
                e.Row.Cells[28].Text = "Total SOLES";
            }
            else
            {
                //GvListaServicios.Columns[28].HeaderText = "Total $";
                e.Row.Cells[28].Text = "Total DOLARES";
            }
        }
    }

    //protected void btnAceptarCondiciones_Click(object sender, EventArgs e)
    //{
    //    mCondiciones.Hide();
    //}

    
    
}