﻿<%@ Page Language="C#" 
MasterPageFile="~/Extranet/Seguridad/eFENIX_Principal.master"
AutoEventWireup="true" 
CodeFile="eFENIX_Factura_ConsultaOTROS.aspx.cs" 
Inherits="Extranet_Operaciones_eFENIX_Factura_Consulta"
Title="Consulta de Facturas" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

       <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"/>
     <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
     <%-- <script src="https://code.jquery.com/jquery-1.12.4.js" type="text/javascript"></script>--%>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js" type="text/javascript"></script>
    <script src="../../Script/Java_Script/jsAcuerdoComision.js"></script>
    <%-- <script src="../../Script/Java_Script/jsSolicitudServicios.js"></script>--%>
    <%--<script src="https://www.jquery-steps.com/Scripts/jquery.steps.min.js"></script>--%>



    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
 

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="CntMaster" runat="Server">

    <table cellspacing="0" cellpadding="0" style="width: 100%;">
        
        <tr>
            <td class="form_titulo" style="width: 85%">
                DOCUMENTOS DE PAGO OTROS
            </td>
            <td align="right" class="form_titulo_verde" style="width: 15%" >
                <ajax:UpdatePanel ID="UdpTotales" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <asp:TextBox ID="LblTotal" Text="Total de Registros:" style="background-color: #0069ae; color: #FFFFFF" ReadOnly="true" BorderColor="#0069ae" runat="server"></asp:TextBox>
                    </ContentTemplate>
                </ajax:UpdatePanel>
            </td>               
        </tr>
        
        <tr>
            <td valign="top" colspan="2">
                <table class="form_bandeja" style="margin-top: 5px; margin-bottom: 5px; width: 100%;">   
                    <%--    <tr>
                            <td style="width:6%;" >
                             TIPO

                         </td>
                          <td style="width: 6%;">
                            <asp:DropDownList ID="dplTipoDocumento" runat="server" Width="145px" >
                            </asp:DropDownList>
                        </td>  
                        </tr>          --%>   
                    <tr>
                        <td style="width: 6%;">
                            FECHA INICIO
                        </td>
                        <td style="width:14%;">
                            <input id="txtIni_datep" type="text" readonly size="20px" maxlength="10" name="DatePickername" class="inputText_extranet"  />
                        </td>
                         <td style="width: 6%;">
                            FECHA FIN
                        </td>
                        <td style="width: 6%;">
                            <input id="txtFin_datep" type="text"  readonly  maxlength="10" name="DatePickerFinname" class="inputText_extranet" />
                        </td>  
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                       

                    </tr>
                     <tr>
                        <td style="width: 6%;">
                            SERIE
                        </td>
                        <td style="width:14%;">
                           <asp:TextBox ID="txtSerie" runat="server" MaxLength="25" Style="text-transform: uppercase"
                                Width="135px"></asp:TextBox>
                        </td>
                         <td style="width: 6%;">
                            NUMERO
                        </td>
                        <td style="width: 6%;">
                          <asp:TextBox ID="txtNumero" runat="server" MaxLength="25" Style="text-transform: uppercase"
                                Width="135px"></asp:TextBox>
                        </td>   
                          
                        <td>
                            &nbsp;
                        </td>
                        <td>
                            <asp:ImageButton ID="ImgBtnBuscar" runat="server" ImageUrl="~/Imagenes/Formulario/btn_consultar.JPG"
                                OnClick="ImgBtnBuscar_Click"/>
                        </td>

                    </tr>     
                                <tr>
                                    <td> &nbsp;</td>
                                </tr>
                    </table>
                    </td>
            </tr>
                     
                    
                    
               
            
     
    

        <tr valign="top" style="height: 70%">
            <td colspan="5">
                <ajax:UpdatePanel ID="upDocumentoOrigen" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div style="overflow: auto; width: 100%; height: 350px;">
                            <asp:GridView ID="GrvListado" runat="server" AutoGenerateColumns="False" DataKeyNames="iIdDocPago,sFechaEmision,dSubtotal,dIGV,dTotal"
                                SkinID="GrillaConsulta" Width="100%" OnRowCommand="GrvListado_RowCommand"
                                OnRowDataBound="GrvListado_RowDataBound">
                                <Columns>
                                   <asp:BoundField DataField="iIdDocPago" HeaderText="ID" Visible="false" />
                                    <asp:BoundField DataField="sSerie" HeaderText="Serie" />
                                    <asp:BoundField DataField="sNumero" HeaderText="Numero" />
                                    <%--<asp:BoundField DataField="sTipoDocumentoPago" HeaderText="Tipo Documento Pago" />--%>
                                    <asp:BoundField DataField="sFechaEmision" HeaderText="Fecha Emision" />                                   
                                    <asp:BoundField DataField="sCliente" HeaderText="Cliente" />
                                    <%--<asp:BoundField DataField="sNroDocumentoOrigen" HeaderText="Referencia"/>--%>
                                    <%--<asp:BoundField DataField="sNroVolante" HeaderText="Volante" />--%>                              
                                    <asp:BoundField DataField="sMoneda" HeaderText="Moneda" />
                                    <asp:BoundField DataField="dSubtotal" HeaderText="Subtotal" />
                                    <asp:BoundField DataField="dIGV" HeaderText="IGV" />
                                    <asp:BoundField DataField="dTotal" HeaderText="Total " />                                       
                                     <asp:ButtonField ButtonType="Image" CommandName="DescargaFactura" HeaderText="PDF" ImageUrl="~/Script/Imagenes/IconButton/filePDF.png"
                                            Text="Descargar Factura"></asp:ButtonField>
                                     <asp:ButtonField ButtonType="Image" CommandName="DescargaFacturaXML" HeaderText="XML" ImageUrl="~/Script/Imagenes/IconButton/fileXML.png"
                                            Text="Descargar Factura"></asp:ButtonField>

                                </Columns>
                            </asp:GridView>
                        </div>
                    </ContentTemplate>
                    <Triggers>                        
                      <ajax:PostBackTrigger ControlID="GrvListado" />                        
                        <ajax:AsyncPostBackTrigger ControlID="ImgBtnBuscar" EventName="Click" />
                        <ajax:AsyncPostBackTrigger ControlID="dnvListado" EventName="PageChanged" />
                        <ajax:AsyncPostBackTrigger ControlID="dnvListado" EventName="NextPage" />
                        <ajax:AsyncPostBackTrigger ControlID="dnvListado" EventName="PreviousPage" />
                        <ajax:AsyncPostBackTrigger ControlID="dnvListado" EventName="FirstPage" />  
                        <ajax:AsyncPostBackTrigger ControlID="dnvListado" EventName="LastPage" />     
                    </Triggers>
                </ajax:UpdatePanel>
            </td>
        </tr>
        <tr valign="top" style="height: 6%;">
            <td colspan="5">
               <ajax:UpdatePanel ID="upDnvListado" runat="server">
                    <ContentTemplate>
                        <FENIX:DataNavigator ID="dnvListado" runat="server" AllowCustomPaging="True" ButtonText="Ir" 
                            ForeColor="White" Font-Size="11px" BackColor="#0069ae" 
                            CurrentPage="1" CustomClientFunction="" EnableAskingAfterPage="False" EnableCustomScript="False"
                            GridViewId="GrvListado" ImagesPath="~/Imagenes/DN/" IsParentShowWindow="False"
                            MessageAskingAfterPage="" PageIndicatorFormat="Página {0} / {1}" Visible="False"
                            Width="100%" WidthButtonIr="" WidthTextBoxNumPagina="25px" />
                    </ContentTemplate>
                    <Triggers>                        
                        <ajax:AsyncPostBackTrigger ControlID="dnvListado" EventName="PageChanged" />                       
                        <ajax:AsyncPostBackTrigger ControlID="ImgBtnBuscar" EventName="Click" />
                    </Triggers>
                </ajax:UpdatePanel>
            </td>
        </tr>
    </table>

   
    <asp:Button ID="btnOpenPopub" Style="display: none;" runat="server" Text="[Open Proxy]" />
    <asp:Button ID="btnClosePopub" Style="display: none;" runat="server" Text="[Close Proxy]" />


    <asp:Button ID="btnOpenProxyInser" Style="display: none;" runat="server" Text="[Open Proxy]" />
    <asp:Button ID="btnCloseProxyInser" Style="display: none;" runat="server" Text="[Close Proxy]" />

    <!--    ADDED NEW PANEL TO CONTAINERS ENABLED AND DISABLED-->



    <script language="javascript" type="text/javascript">

        var tiempo = 0;
        var seconds = 0;

        $(document).ready(function(){
    //     $('#rbtnContenedorSi').change(function(){
    //    if(this.checked)
    //        $('#divContenedores').fadeIn('slow');
    //    else
    //        $('#divContenedores').fadeOut('slow');

    //});
        });
           

        var myVar = setInterval(function () {
            clearInterval(myVar);
        }, 1000);

        var myTiempo = setTimeout(function () {
            clearInterval(myTiempo);
        }, 1);

        function myStopFunction() {
            clearInterval(myVar);
            seconds = '<%=GlobalEntity.Instancia.TiempoDuracionSesion%>' * 60;
            tiempo = '<%=GlobalEntity.Instancia.TiempoDuracionSesion%>' * 1000 * 60;

            CuentaTiempo(tiempo);
        }

    

        function CuentaTiempo(tiempo) {
            clearTimeout(myTiempo);
            myTiempo = setTimeout(function () {
                document.getElementById('<%=btn_cerrar.ClientID%>').click();
                //window.location = "../Seguridad/eFENIX_Login.aspx";
            }, tiempo);
        }

        function SessionExpireAlert(timeout) {
            seconds = timeout / 1000;
            document.getElementsByName("seconds").innerHTML = seconds;

            myvar = setInterval(function () {
                seconds--;
                document.getElementById("seconds").innerHTML = seconds;
            }, 1000);

            CuentaTiempo(timeout);
        };


        var goodexit = false;
        window.onbeforeunload = confirmRegisterExit;
        function confirmRegisterExit(evt) {
            if (!goodexit) {
                //return "Si vas abandonar el sistema haz click en Cerrar Sesion!!";
                document.getElementById('<%=btn_cerrar.ClientID%>').click();
            }
        }

        var objFilaAnt = null;
        var backgroundColorFilaAnt = "";
        
        function fc_PressKeyDO() {
            if (event.keyCode == 13) {
                document.getElementById("<%=ImgBtnBuscar.ClientID%>").click();
                return false;
            }
            else {
                return true;
            }
        }

       function HabilitarUno(chk) {
            f = document.forms[0].elements;
            for (x = 0; x < f.length; x++) {
                obj = f[x];
                if (obj.type == 'checkbox') {
                    obj.checked = false;
                }
            }
            chk.enabled;
            chk.checked = true;
        }
     
    </script>

</asp:Content>
<asp:Content ID="Content3" runat="server" contentplaceholderid="ContentCab">
    <span class="texto_titulo" >Tu Sesion Expirara en &nbsp; 
        <span class="texto_titulo" id="seconds"> </span>
        <span>&nbsp;seg.</span>
     </span>
     <asp:Button ID="btn_cerrar" runat="server" Text="cerrar" 
        onclick="btn_cerrar_Click"  Style="display: none;" />
</asp:Content>
