﻿<%@ Page Language="C#" 
MasterPageFile="~/Extranet/Seguridad/eFENIX_Principal.master"
AutoEventWireup="true" 
CodeFile="eFENIX_PlacasChoferes_Consulta.aspx.cs" 
Inherits="Extranet_Operaciones_eFENIX_PlacasChoferes_Consulta"
Title="Consulta de Placas y Choferes" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="CntMaster" runat="Server">

    <table cellspacing="0" cellpadding="0" style="width: 100%;">
        
        <tr>
            <td class="form_titulo" style="width: 85%">
                CONSULTA DE PLACAS Y CHOFERES
            </td>
            <td align="right" class="form_titulo_verde" style="width: 15%" >
                <ajax:UpdatePanel ID="UdpTotales" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <asp:TextBox ID="LblTotal" Text="Total de Registros:" style="background-color: #0069ae; color: #FFFFFF" ReadOnly="true" BorderColor="#0069ae" runat="server"></asp:TextBox>
                    </ContentTemplate>
                </ajax:UpdatePanel>
            </td>               
        </tr>
        
        <tr>
            <td valign="top" colspan="2">
                <table class="form_bandeja" style="margin-top: 5px; margin-bottom: 5px; width: 100%; height: 62px;">
                    <tr>
                        <td style="width: 11%;">
                            Num° Aut. Ret.</td>
                        <td style="width: 19%">
                            <asp:TextBox ID="txtNumAut" runat="server" MaxLength="15" Style="text-transform: uppercase"
                                Width="150px"></asp:TextBox>
                        </td>
                        <td style="width: 12%">

                            Volante</td>
                        <td style="width: 21%">

                            <asp:TextBox ID="TxtVolante" runat="server" MaxLength="15" Style="text-transform: uppercase"
                                Width="139px" Height="16px"></asp:TextBox>

                        </td>
                        <td style="width: 67px">

                            &nbsp;</td>
                        <td style="width: 162px">

                            <asp:ImageButton ID="ImgBtnBuscar" runat="server" ImageUrl="~/Imagenes/Formulario/btn_consultar.JPG"
                                OnClick="ImgBtnBuscar_Click" OnClientClick="return ValidarFiltro();"/>
                        </td>
                    </tr>
                    <tr>
                        <td>bl</td>
                        <td style="width: 19%">

                            <asp:TextBox ID="TxtDocumentoH" runat="server" MaxLength="25" Style="text-transform: uppercase"
                                Width="160px" Height="16px"></asp:TextBox>
                        </td>
                        <td style="width: 12%">Contenedor/Chasis
                        </td>
                        <td style="width: 21%">

                            <asp:TextBox ID="TxtContenedor" runat="server" Style="text-transform: uppercase"
                                MaxLength="30" Width="150px"></asp:TextBox>

                        </td>
                        <td>Cliente
                        </td>
                        <td>

                            <asp:TextBox ID="TxtCliente" runat="server" Style="text-transform: uppercase"
                                Width="400px"></asp:TextBox>

                        </td>
                    </tr>
                    </table>
             </td>   
        </tr>
    

        <tr valign="top" style="height: 100%">
            <td colspan="5">
                <ajax:UpdatePanel ID="upDocumentoOrigen" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div style="overflow: auto; width: 100%; height: 100%">
                            <asp:GridView ID="GrvListado" runat="server" AutoGenerateColumns="False" DataKeyNames="IIdAutRet,SNumAutRet"
                                SkinID="GrillaConsulta" Width="100%" OnRowCommand="GrvListado_RowCommand">
                                <Columns>
                                    <asp:BoundField DataField="IIdAutRet" HeaderText="IdAutRet" Visible="false" />
                                    <asp:BoundField DataField="SNumAutRet" HeaderText="NUM. AUT. RET." />
                                    <asp:BoundField DataField="SFechaVigencia" HeaderText="Fecha Vigencia" />
                                    <asp:BoundField DataField="iIdCliente" HeaderText="IdCliente" Visible="false" />
                                    <asp:BoundField DataField="SCliente" HeaderText="Cliente" />
                                    <asp:BoundField DataField="SDespachador" HeaderText="Despachador" />
                                    <asp:BoundField DataField="SDocumentoO" HeaderText="Doc. Origen" />
                                    <asp:BoundField DataField="SVolante" HeaderText="Volante" />                              
                                    <asp:BoundField DataField="SSituacion" HeaderText="Situacion" />
                                    <asp:BoundField DataField="SEstado" HeaderText="Estado" />
                                    <asp:ButtonField ButtonType="Image" CommandName="OpenModificar" HeaderText="Mod." ImageUrl="~/Script/Imagenes/IconButton/Edit_Pequeno.png"
                                            Text="Ver Volante"></asp:ButtonField>
                                </Columns>
                            </asp:GridView>
                        </div>
                    </ContentTemplate>
                    <Triggers>
                        <ajax:AsyncPostBackTrigger ControlID="GrvListado" EventName="SelectedIndexChanged" />
                        <ajax:AsyncPostBackTrigger ControlID="ImgBtnBuscar" EventName="Click" />
                        <ajax:AsyncPostBackTrigger ControlID="dnvListado" EventName="PageChanged" />
                        <ajax:AsyncPostBackTrigger ControlID="dnvListado" EventName="NextPage" />
                        <ajax:AsyncPostBackTrigger ControlID="dnvListado" EventName="PreviousPage" />
                        <ajax:AsyncPostBackTrigger ControlID="dnvListado" EventName="FirstPage" />  
                        <ajax:AsyncPostBackTrigger ControlID="dnvListado" EventName="LastPage" />     
                    </Triggers>
                </ajax:UpdatePanel>
            </td>
        </tr>
        <tr valign="top" style="height: 6%;">
            <td colspan="5">
               <ajax:UpdatePanel ID="upDnvListado" runat="server">
                    <ContentTemplate>
                        <FENIX:DataNavigator ID="dnvListado" runat="server" AllowCustomPaging="True" ButtonText="Ir" 
                            ForeColor="White" Font-Size="11px" BackColor="#0069ae" 
                            CurrentPage="1" CustomClientFunction="" EnableAskingAfterPage="False" EnableCustomScript="False"
                            GridViewId="GrvListado" ImagesPath="~/Imagenes/DN/" IsParentShowWindow="False"
                            MessageAskingAfterPage="" PageIndicatorFormat="Página {0} / {1}" Visible="False"
                            Width="100%" WidthButtonIr="" WidthTextBoxNumPagina="25px" />
                    </ContentTemplate>
                    <Triggers>
                        <ajax:AsyncPostBackTrigger ControlID="dnvListado" EventName="PageChanged" />
                        <ajax:AsyncPostBackTrigger ControlID="ImgBtnBuscar" EventName="Click" />
                    </Triggers>
                </ajax:UpdatePanel>
            </td>
        </tr>
    </table>

    <asp:Panel ID="PnlAgregarChoferPlaca"  runat="server" CssClass="PanelPopup" Width="760px" BorderWidth="1" Height="750px"
        BorderColor="#004b8e">
        <div style="overflow: auto; width: 100%; height: 90%; align-content:center">
        <asp:UpdatePanel ID="UpdatePanel1" runat="server"  ScrollBars="Vertical">
            <ContentTemplate>
                <table cellspacing="0" cellpadding="0" border="0" style="height: 60px; width: 750px;
                    background: url( '../../Imagenes/menu/menu_titulo6.png');">
                    <tr>
                        <td align="right" style="padding-bottom: 5px; padding-top: 10px; padding-right: 15px;">
                             <br />
                             <br />
                            <asp:Button ID="btnCerrar2" runat="server" CssClass="btnAcccionComision" OnClientClick="return ClosePopUpAgrePlacaChofer();" Text="Cerrar" Width="70px" />
                        </td>
                    </tr>
                </table>
                <table border="0" cellpadding="2" cellspacing="1" style="width: 700px; height: 35px;
                    margin-top: 5px; margin-left: 5px; padding: 5px; background-color: #EFEFF1"
                    class="cuerpoComision">
                    <tr>
                        <td >
                            N° Aut. Ret.
                        </td>
                        <td>
                            <asp:TextBox ID="txtAutRetAgre" runat="server" Width="120px" MaxLength="25" SkinID="txtNormal" Style="text-transform: uppercase" Enabled="False"></asp:TextBox>
                        </td>
                        <td colspan="2">
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="lblFecha" runat="server">Fecha Vigencia</asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="txtFecha" runat="server" MaxLength="25" SkinID="txtNormal" Style="text-transform: uppercase" Width="120px"
                                 Enabled="false"></asp:TextBox>
                        </td>
                        <td><asp:Label ID="lblSituacion" runat="server">Situacion</asp:Label></td>
                        <td>
                            <asp:TextBox ID="txtSituacion" runat="server" MaxLength="25" SkinID="txtNormal" Style="text-transform: uppercase" Width="160px"
                                 Enabled="false"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="lblEstado" runat="server">Estado</asp:Label></td>
                        <td>
                            <asp:TextBox ID="txtEstado" runat="server" MaxLength="25" SkinID="txtNormal" Style="text-transform: uppercase" Width="120px" 
                                Enabled="false"></asp:TextBox>
                        </td>
                        <td>
                            <asp:Label ID="lblDespachador" runat="server">Despachador</asp:Label>&nbsp;Tramito</td>
                        <td>
                            <asp:TextBox ID="txtDespachador" runat="server" MaxLength="50" SkinID="txtNormal" Style="text-transform: uppercase" Width="200px" Height="16px" 
                                Enabled="false"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>Placa</td>
                        <td>
                            <asp:TextBox ID="txtPlacaAgre" runat="server" MaxLength="6" SkinID="txtNormal" Style="text-transform: uppercase" Width="120px"></asp:TextBox>
                        </td>
                        <td>DNI Chofer</td>
                        <td>
                            <asp:TextBox ID="txtDNIAgre" runat="server" Height="16px" MaxLength="15" SkinID="txtNormal" Style="text-transform: uppercase" Width="120px"></asp:TextBox>
                            <asp:ImageButton ID="btnImgChoferDNI" OnClick="btnImgChoferDNI_Click" runat="server"  ImageUrl="~/Imagenes/buscar.png" />
                        </td>
                    </tr>
                    <tr>
                        <td>Brevete Chofer</td>
                        <td>
                            <asp:TextBox ID="txtBrevete" runat="server" MaxLength="15" SkinID="txtNormal" Style="text-transform: uppercase" Width="110px"></asp:TextBox>
                             <asp:ImageButton ID="btnImgBrevete" OnClick="btnImgBrevete_Click" runat="server" ImageUrl="~/Imagenes/buscar.png" />
                        </td>
                        <td>Nombre Chofer</td>
                        <td>
                            <asp:TextBox ID="txtNombreChofer" runat="server" MaxLength="80" SkinID="txtNormal" Style="text-transform: uppercase" Width="250px" Height="16px" Enabled="false"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" align="right">
                            <asp:Button ID="btnAgregar" runat="server" CssClass="btnAcccionComision" Text="Agregar" Width="70px" OnClick="btnAgregar_Click" />
                        </td>
                        <td align="center">
                            <asp:Button ID="btnQuitar" runat="server" CssClass="btnAcccionComision" Text="Quitar" Width="70px" OnClick="btnQuitar_Click" />
                        </td>
                           <td align="center">Empadronar Chofer
                             <asp:CheckBox ID="ChkEmpadronar" runat="server" AutoPostBack="true" OnCheckedChanged="ChkEmpadronar_OnCheckedChanged" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                <ContentTemplate>
                                    <asp:GridView ID="gvListadoAgre" runat="server" AutoGenerateColumns="False" DataKeyNames="IIdRegistro,SPlaca,SDoc,sChofer,sTicket,sFSalida"
                                        SkinID="GrillaConsulta" OnRowDataBound="gvListadoAgre_OnRowDataBound" Width="100%">
                                        <Columns>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="chkSeleccionar" runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="IIdRegistro" HeaderText="IIdRegistro" Visible="false"/>
                                            <asp:BoundField DataField="SPlaca" HeaderText="PLACA" />
                                            <asp:BoundField DataField="SDoc" HeaderText="DOCUMENTO" />
                                            <asp:BoundField DataField="sChofer" HeaderText="Chofer" />
                                            <asp:BoundField DataField="sTicket" HeaderText="Ticket Balanza" />
                                            <asp:BoundField DataField="sFSalida" HeaderText="Fec. Sal. Balanza" />
                                        </Columns>
                                    </asp:GridView>
                                </ContentTemplate>
                                <Triggers>
                                </Triggers>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                                            <tr>
                  
                            <td align="center" colspan="4">
                             
                  
                        <br />
                                Historial de Registros de VALIDACION- INGRESO A FARGOLINE
                                <asp:UpdatePanel ID="UpdatePanel12" runat="server">
                                    <ContentTemplate>
                                        <div id="divChoferesR" style="overflow: auto; width: 550px;  padding-left: 2px;">
                                            <asp:GridView ID="gvListadoR" runat="server" Font-Bold="false" SkinID="GrillaConsulta" Width="500px">
                                                <Columns>
                                                    <asp:BoundField DataField="dtFechaRegistro" HeaderText="Fecha" />
                                                    <asp:BoundField DataField="SDoc" HeaderText="DNI" />
                                                    <asp:BoundField DataField="sBrevete" HeaderText="Brevete" />
                                                    <asp:BoundField DataField="SPlaca" HeaderText="Placa" />
                                                    <asp:BoundField DataField="sSituacion" HeaderText="Situacion" />
                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                        </tr>
                    <tr>
                        <td colspan="4">
                            Despachadores que pueden retirar:
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4"></td>
                    </tr>
                    <tr>
                        <asp:UpdatePanel ID="UpdatePanel7" runat="server">
                            <ContentTemplate>
                        <td>DNI</td>
                        <td>
                            <asp:TextBox ID="txtDniDesp" runat="server" Enabled="false" MaxLength="25" SkinID="txtNormal" Style="text-transform: uppercase" Width="120px">
                            </asp:TextBox>
                        </td>
                        <td>Nombre</td>
                        <td>
                            <asp:TextBox ID="txtDespNom" runat="server" Enabled="false" SkinID="txtNormal" Style="text-transform: uppercase" Width="120px">
                            </asp:TextBox>
                            <asp:ImageButton ID="imbDespachador" runat="server" ImageUrl="~/Script/Imagenes/IconButton/Buscar.png"
                                        OnClientClick="return OpenPopupDespachador();"/>
                        </td>
                                </ContentTemplate>
                        </asp:UpdatePanel>
                    </tr>
                    <tr>
                        <td colspan="2" align="right">
                            <asp:Button ID="btnAgregarDesp" runat="server" CssClass="btnAcccionComision" Text="Agregar" Width="70px" OnClick="btnAgregarDesp_Click"/>
                        </td>
                        <td align="center">
                            <asp:Button ID="btnQuitarDesp" runat="server" CssClass="btnAcccionComision" Text="Quitar" Width="70px" OnClick="btnQuitarDesp_Click"/>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                                <ContentTemplate>
                                    <asp:GridView ID="gvDespachadores" runat="server" AutoGenerateColumns="False" DataKeyNames="IIdRegistro,IIdDespachador,SDniDespachador,SDespachador"
                                        SkinID="GrillaConsulta" OnRowDataBound="gvDespachadores_OnRowDataBound" Width="100%">
                                        <Columns>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="chkSeleccionarDesp" runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="IIdRegistro" HeaderText="IIdRegistro" Visible="false"/>
                                            <asp:BoundField DataField="IIdDespachador" HeaderText="IdDespachador" Visible="false" />
                                            <asp:BoundField DataField="SDniDespachador" HeaderText="DNI" />
                                            <asp:BoundField DataField="SDespachador" HeaderText="Despachador" />
                                        </Columns>
                                    </asp:GridView>
                                </ContentTemplate>
                                <Triggers>
                                </Triggers>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                       <tr>
                        <td align="center">
                            <table>
                                <tr>
                                    <td>
                                         <asp:Button ID="Button1" runat="server" CssClass="btnAcccionComision" OnClientClick="return ClosePopUpAgrePlacaChofer();" Text="Cerrar Formulario" Width="140px" ForeColor="Black"/>
                                    </td>
                                </tr>
                            </table>

                        </td>
                    </tr>
                </table>
            </ContentTemplate>
            <Triggers>

            </Triggers>
        </asp:UpdatePanel>
            </div>
    </asp:Panel>

    <ajaxToolkit:ModalPopupExtender ID="mpAgrePlacaChofer" runat="server"
        Enabled="True" PopupControlID="PnlAgregarChoferPlaca" BackgroundCssClass="modalBackground"
        TargetControlID="btnOpen" CancelControlID="btnClose">
    </ajaxToolkit:ModalPopupExtender>
     <asp:Button ID="btnOpen" Style="display: none;" runat="server" Text="[Open Proxy]" />
    <asp:Button ID="btnClose" Style="display: none;" runat="server" Text="[Close Proxy]" />

        <asp:Panel ID="pnlDespachador"  runat="server" CssClass="PanelPopup" Width="530px" BorderWidth="1"
        BorderColor="#004b8e">
        <asp:UpdatePanel ID="UpdatePanel4" runat="server">
            <ContentTemplate>
                <table cellspacing="0" cellpadding="0" border="0" style="height: 60px; width: 520px;
                    background: url( '../../Imagenes/menu/menu_titulo6.png');">
                    <tr>
                        <td align="right" style="padding-bottom: 5px; padding-top: 10px; padding-right: 15px;">
                             <asp:Button ID="btnAceptar" runat="server" Text="Aceptar" CssClass="btnAcccionComision"
                                            Width="70px" OnClick="btnAceptar_Click"/>
                            <asp:Button ID="btnCerrarPopUp" runat="server" Text="Cerrar" CssClass="btnAcccionComision"
                                 Width="70px" OnClick="btnCerrarPopUp_Click" />
                        </td>
                    </tr>
                </table>
                <table border="0" cellpadding="2" cellspacing="1" style="width: 500px; height: 35px;
                    margin-top: 5px; margin-left: 5px; padding: 5px; background-color: #EFEFF1"
                    class="cuerpoComision">
                    <tr>
                        <td >
                            DNI
                            &nbsp;
                            <asp:TextBox ID="txtIdDespachadorPopUp" runat="server" Width="100px" MaxLength="10" SkinID="txtNormal" Style="text-transform: uppercase"></asp:TextBox>
                            &nbsp;
                            Nombre
                            &nbsp;
                            <asp:TextBox ID="txtDespachadorPopUp" runat="server" Width="200px" MaxLength="50" SkinID="txtNormal" Style="text-transform: uppercase"></asp:TextBox>
                            <asp:ImageButton ID="imgbBuscar" runat="server" ImageUrl="~/Script/Imagenes/IconButton/Buscar.png" OnClick="imgbBuscar_Click"/>
                        </td>
                    </tr>
                </table>
                <table border="0" cellpadding="2" cellspacing="1" style="width: 480px; height: 290px; margin-top: 5px; margin-left: 5px; margin-right: 5px; margin-bottom: 5px; padding: 5px; background-color: #FFFFFF"
                    class="form_bandeja">
                    <tr style="vertical-align: top;">
                        <td colspan="3">
                            <div style="overflow: auto; width: 480px; height: 290px; padding-left: 1px; position: absolute;">
                                <ajax:UpdatePanel ID="UpdatePanel5" runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <asp:GridView ID="grvListadoDespachador" Width="480px" runat="server" 
                                            AllowPaging="true" AllowSorting="true" SkinID="GrillaConsulta"
                                            AutoGenerateColumns="False" Font-Bold="false" DataKeyNames="IIdDespachador,SDniDespachador,SDespachador"
                                             OnRowDataBound="grvListadoDespachador_RowDataBound">
                 <%--  DataKeyNames="iIdValor,sDescripcion,sDescripcion1,sDescripcion2,sDescripcion3,sDescripcion4,sDescripcion5,sDescripcion6" 
                            " 
                            OnSorting="GrvListado_Sorting" --%>
                                            <Columns>
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="ChkSeleccionarDespachador" runat="server" />
                                                    </ItemTemplate> 
                                                    <ItemStyle Width="5%" />
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="IIdDespachador" HeaderText="Código">
                                                    <ItemStyle Width="10px" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="SDniDespachador" HeaderText="DNI">
                                                </asp:BoundField>
                                                 <asp:BoundField DataField="SDespachador" HeaderText="Despachador">
                                                </asp:BoundField>
                                            </Columns>
                                            <PagerSettings Visible="False" />
                                        </asp:GridView>
                                    </ContentTemplate>
                                    <Triggers>
                       <%-- <ajax:AsyncPostBackTrigger ControlID="GrvListado" EventName="Sorting"></ajax:AsyncPostBackTrigger>
                        <ajax:AsyncPostBackTrigger ControlID="GrvListado" EventName="PageIndexChanging"></ajax:AsyncPostBackTrigger>
                        <ajax:AsyncPostBackTrigger ControlID="GrvListado" EventName="RowDataBound"></ajax:AsyncPostBackTrigger>--%>

                                    </Triggers>
                                </ajax:UpdatePanel>
                            </div>
                        </td>
                    </tr>
                </table>
            </ContentTemplate>
            <Triggers>

            </Triggers>
        </asp:UpdatePanel>
    </asp:Panel>
        <ajaxToolkit:ModalPopupExtender ID="mpDespachador" runat="server"
        Enabled="True" PopupControlID="pnlDespachador" BackgroundCssClass="modalBackground"
        TargetControlID="btnOpenProxyInser" CancelControlID="btnCloseProxyInser">
    </ajaxToolkit:ModalPopupExtender>
    <asp:Button ID="btnOpenProxyInser" Style="display: none;" runat="server" Text="[Open Proxy]" />
    <asp:Button ID="btnCloseProxyInser" Style="display: none;" runat="server" Text="[Close Proxy]" />

    <script language="javascript" type="text/javascript">

        var tiempo = 0;
        var seconds = 0;

        var myVar = setInterval(function () {
            clearInterval(myVar);
        }, 1000);

        var myTiempo = setTimeout(function () {
            clearInterval(myTiempo);
        }, 1);

        function myStopFunction() {
            clearInterval(myVar);
            seconds = '<%=GlobalEntity.Instancia.TiempoDuracionSesion%>' * 60;
            tiempo = '<%=GlobalEntity.Instancia.TiempoDuracionSesion%>' * 1000 * 60;

            CuentaTiempo(tiempo);
        }

        function CuentaTiempo(tiempo) {
            clearTimeout(myTiempo);
            myTiempo = setTimeout(function () {
                document.getElementById('<%=btn_cerrar.ClientID%>').click();
                //window.location = "../Seguridad/eFENIX_Login.aspx";
            }, tiempo);
        }



        function SessionExpireAlert(timeout) {
            seconds = timeout / 1000;
            document.getElementsByName("seconds").innerHTML = seconds;

            myvar = setInterval(function () {
                seconds--;
                document.getElementById("seconds").innerHTML = seconds;
            }, 1000);

            CuentaTiempo(timeout);
        };


        var goodexit = false;
        window.onbeforeunload = confirmRegisterExit;
        function confirmRegisterExit(evt) {
            if (!goodexit) {
                //return "Si vas abandonar el sistema haz click en Cerrar Sesion!!";
                document.getElementById('<%=btn_cerrar.ClientID%>').click();
            }
        }

        var objFilaAnt = null;
        var backgroundColorFilaAnt = "";

        function fc_SeleccionaFilaSimple(objFila, objrowIndex, chkID) {
            try {

                if (objFilaAnt != null) {
                    objFilaAnt.style.backgroundColor = backgroundColorFilaAnt;
                }
                objFilaAnt = objFila;
                backgroundColorFilaAnt = objFila.style.backgroundColor;
                objFila.style.backgroundColor = "#c4e4ff";
            }

            catch (e) {
                error = e.message;
            }
        }

        function HabilitarUno(chk) {
            f = document.forms[0].elements;
            for (x = 0; x < f.length; x++) {
                obj = f[x];
                if (obj.type == 'checkbox') {
                    obj.checked = false;
                }
            }
            chk.enabled;
            chk.checked = true;
        }
        
        function fc_PressKeyDO() {
            if (event.keyCode == 13) {
                document.getElementById("<%=ImgBtnBuscar.ClientID%>").click();
                return false;
            }
            else {
                return true;
            }
        }

        function fc_PressKeyDesp() {
            if (event.keyCode == 13) {
                document.getElementById("<%=imgbBuscar.ClientID%>").click();
                return false;
            }
            else {
                return true;
            }
        }

        function ClosePopUpAgrePlacaChofer() {
            document.getElementById('<%=btnClose.ClientID %>').click();
            return false;
        }
        function OpenPopUpAgrePlacaChofer(e, sObjeto) {
                document.getElementById('<%=btnOpen.ClientID %>').click();
            return false;
        }

        function HabilitarUno(chk) {
            f = document.forms[0].elements;
            for (x = 0; x < f.length; x++) {
                obj = f[x];
                if (obj.type == 'checkbox') {
                    obj.checked = false;
                }
            }
            chk.enabled;
            chk.checked = true;
        }

        function ClosePopubDespachador() {
            document.getElementById('<%=btnCloseProxyInser.ClientID %>').click();
            return false;
        }
        function OpenPopupDespachador(e, sObjeto) {
                document.getElementById('<%=btnOpenProxyInser.ClientID %>').click();
            return false;
        }

       function fc_PressKeyDNI() {
            if (event.keyCode == 13) {
                document.getElementById("<%=btnImgChoferDNI.ClientID%>").click();
                return false;
            }
            else {
                return true;
            }
        }
       function fc_PressKeyBrevete() {
            if (event.keyCode == 13) {
                document.getElementById("<%=btnImgBrevete.ClientID%>").click();
                return false;
            }
            else {
                return true;
            }
        }
    </script>

</asp:Content>
<asp:Content ID="Content3" runat="server" contentplaceholderid="ContentCab">
    <span class="texto_titulo" >Tu Sesion Expirara en &nbsp; 
        <span class="texto_titulo" id="seconds"> </span>
        <span>&nbsp;seg.</span>
     </span>
     <asp:Button ID="btn_cerrar" runat="server" Text="cerrar" 
        onclick="btn_cerrar_Click"  Style="display: none;" />
</asp:Content>
