﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using QNET.Web.UI.Controls;
using FENIX.BusinessEntity;
using FENIX.BusinessLogic;
using System.Text;
using FENIX.Common;
using System.Collections.Generic;
using System.Drawing;


public partial class Extranet_Operaciones_eFENIX_Orden_Servicio_Aduana : System.Web.UI.Page
{
    #region "Evento Pagina"
        private void SCA_MsgInformacion(string strError)
        {
            MensajeScript(strError);
        }

        private void MensajeScript(string SMS)
        {
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "", "<script>alert('" + SMS + "');</script>", false);
        }
        
        protected void Page_Load(object sender, EventArgs e)
        {
            dnvListado.BindGridView += new QNET.Web.UI.Controls.DataNavigator.BindGridViewDelegate(BindGridLista);

            //TxtAnoManifiesto.Attributes.Add("onkeydown", "javascript:return fc_PressKeyDO();");
            //TxtNumManifiesto.Attributes.Add("onkeydown", "javascript:return fc_PressKeyDO();");
            //TxtDocumentoM.Attributes.Add("onkeydown", "javascript:return fc_PressKeyDO();");
            //TxtVolante.Attributes.Add("onkeydown", "javascript:return fc_PressKeyDO();");
            //TxtOrden.Attributes.Add("onkeydown", "javascript:return fc_PressKeyDO();");

            //TxtAnoManifiesto.Attributes.Add("onkeypress", "javascript:return SoloEnteros(event);");
            //TxtNumManifiesto.Attributes.Add("onkeypress", "javascript:return SoloEnteros(event);");
            //TxtVolante.Attributes.Add("onkeypress", "javascript:return SoloEnteros(event);");
            //TxtOrden.Attributes.Add("onkeypress", "javascript:return SoloEnteros(event);");

            //TxtCliente.Text = GlobalEntity.Instancia.NombreCliente;
            //TxtAnoManifiesto.Text = Convert.ToString(DateTime.Now.Year);

            String tiempo = (Convert.ToInt32(GlobalEntity.Instancia.TiempoDuracionSesion.ToString()) * 1000 * 60).ToString();
            Session["Reset"] = true;

            if (!Page.IsPostBack)
            {
                LlenaCombos();

                if (GrvListado.Rows.Count == 0)
                {
                    List<BE_DocumentoOrigen> lsBE_DocumentoOrigen = new List<BE_DocumentoOrigen>();
                    BE_DocumentoOrigen oBE_DocumentoOrigen = new BE_DocumentoOrigen();
                    lsBE_DocumentoOrigen.Add(oBE_DocumentoOrigen);
                    GrvListado.DataSource = lsBE_DocumentoOrigen;
                    GrvListado.DataBind();
                }
                ScriptManager.RegisterStartupScript(this, GetType(), "mensaje", "SessionExpireAlert(" + tiempo + ");", true);
            }
            else
                ScriptManager.RegisterStartupScript(this, GetType(), "mensaje", "myStopFunction();", true);
        }

        protected void ImgBtnBuscar_Click(object sender, ImageClickEventArgs e)
        {
            dnvListado.InvokeBind();

            if (GrvListado.Rows.Count == 0)
            {
                List<BE_DocumentoOrigen> lsBE_DocumentoOrigen = new List<BE_DocumentoOrigen>();
                BE_DocumentoOrigen oBE_DocumentoOrigen = new BE_DocumentoOrigen();
                lsBE_DocumentoOrigen.Add(oBE_DocumentoOrigen);
                GrvListado.DataSource = lsBE_DocumentoOrigen;
                GrvListado.DataBind();
            }
        }

        protected void ImgBtnNuevo_Click(object sender, ImageClickEventArgs e)
        {
            ScriptManager.RegisterStartupScript(GrvListado, GetType(), "AbrirPopup", String.Format("javascript: OpenPopub();"), true);
        }

        protected void btnGrabarOrdenServicio_Click(object sender, EventArgs e)
        {
            try
            {
                BL_OrdenServicio oBL_OrdenServicio = new BL_OrdenServicio();
                BE_OrdenServicio oBE_OrdenServicio = new BE_OrdenServicio();

                int vl_iCodigo = 0;
                int vl_iIdServicio = 0;
                String vl_sMessage = string.Empty;
                String[] oResultadoTransaccionTx;

                vl_iIdServicio = Convert.ToInt32(DplServicio.SelectedValue);
                if (vl_iIdServicio == -1)
                {
                    SCA_MsgInformacion("Debe seleccionar el Servicio");
                    return;
                }

                oBE_OrdenServicio.iIdDocOri = 0;
                oBE_OrdenServicio.iIdCliente = 0;
                oBE_OrdenServicio.iIdSocio = GlobalEntity.Instancia.IdCliente;

                oBE_OrdenServicio.iIdLineaNegocio = 89;
                oBE_OrdenServicio.sNumeroDO = TxtDocumentoNuevo.Text.Trim();
                oBE_OrdenServicio.iIdOrigen = 2;
                oBE_OrdenServicio.iIdSituacion = 0;
                oBE_OrdenServicio.sFechaProg = "";
                oBE_OrdenServicio.sObservacion = "GENERADO VIA WEB";
                oBE_OrdenServicio.sNombrePc = GlobalEntity.Instancia.NombrePc;
                oBE_OrdenServicio.sUsuario = GlobalEntity.Instancia.Usuario;

                oResultadoTransaccionTx = oBL_OrdenServicio.Insertar_OrdenServicio(oBE_OrdenServicio, vl_iIdServicio).ToString().Split('|');

                for (int i = 0; i < oResultadoTransaccionTx.Length; i++)
                {
                    if (i == 0)
                    {
                        vl_iCodigo = Convert.ToInt32(oResultadoTransaccionTx[i]);
                    }
                    else
                    {
                        vl_sMessage += oResultadoTransaccionTx[i];
                    }
                }

                SCA_MsgInformacion(vl_sMessage);
                if (vl_iCodigo > 1)
                {

                }
            }
            catch (Exception ex)
            {
                new ResultadoInterfase(ResultadoInterfase.TipoResultado.Error, ex);
            }
        }

        protected void GrvListado_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataKey dataKey;
                dataKey = this.GrvListado.DataKeys[e.Row.RowIndex];
                BE_Tracking oitem = (e.Row.DataItem as BE_Tracking);

                if (dataKey.Values["iIdOrdSer"] != null)
                {
                    CheckBox chkSeleccion = (CheckBox)e.Row.FindControl("chkSeleccionar");
                    chkSeleccion.Attributes.Add("onclick", "HabilitarUno(this);");

                    e.Row.Attributes["onclick"] = String.Format("javascript: fc_SeleccionaFilaSimple(this);");

                    for (int c = 1; c < e.Row.Cells.Count; c++)
                    {
                        e.Row.Cells[c].Attributes.Add("onclick", string.Format("HabilitarUno(document.getElementById('{0}'));", chkSeleccion.ClientID));
                    }

                    e.Row.Style["cursor"] = "pointer";
                }
                else
                {
                    e.Row.Visible = false;
                }
            }
        }

        protected void btnConsulta_Click(object sender, EventArgs e)
        {
            dnvListado.InvokeBind();

            if (GrvListado.Rows.Count == 0)
            {
                List<BE_DocumentoOrigen> lsBE_DocumentoOrigen = new List<BE_DocumentoOrigen>();
                BE_DocumentoOrigen oBE_DocumentoOrigen = new BE_DocumentoOrigen();
                lsBE_DocumentoOrigen.Add(oBE_DocumentoOrigen);
                GrvListado.DataSource = lsBE_DocumentoOrigen;
                GrvListado.DataBind();
            }
        }

        protected void ImgBtnBuscar_Click(object sender, EventArgs e)
        {
            dnvListado.InvokeBind();

            if (GrvListado.Rows.Count == 0)
            {
                List<BE_DocumentoOrigen> lsBE_DocumentoOrigen = new List<BE_DocumentoOrigen>();
                BE_DocumentoOrigen oBE_DocumentoOrigen = new BE_DocumentoOrigen();
                lsBE_DocumentoOrigen.Add(oBE_DocumentoOrigen);
                GrvListado.DataSource = lsBE_DocumentoOrigen;
                GrvListado.DataBind();
            }
        }

        protected void GrvListado_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            int vl_iCodigo = 0;
            String vl_sMessage = string.Empty;
            String[] oResultadoTransaccionTx = null;

            if (e.CommandName == "Anular")
            {
                Int32 iIdOrdSer = Convert.ToInt32(GrvListado.DataKeys[Convert.ToInt32(e.CommandArgument)].Values["iIdOrdSer"].ToString());

                BL_OrdenServicio oBL_OrdenServicio = new BL_OrdenServicio();
                BE_OrdenServicio oBE_OrdenServicio = new BE_OrdenServicio();

                oBE_OrdenServicio.iIdOrdSer = iIdOrdSer;
                oBE_OrdenServicio.sUsuario = GlobalEntity.Instancia.Usuario;
                oBE_OrdenServicio.sNombrePc = GlobalEntity.Instancia.NombrePc;

                oResultadoTransaccionTx = oBL_OrdenServicio.EliminarCabeceraOS(oBE_OrdenServicio).Split('|');

                for (int i = 0; i < oResultadoTransaccionTx.Length; i++)
                {
                    if (i == 0)
                    {
                        vl_iCodigo = Convert.ToInt32(oResultadoTransaccionTx[i]);
                    }
                    else
                    {
                        vl_sMessage += oResultadoTransaccionTx[i];
                    }
                }

                SCA_MsgInformacion(vl_sMessage);
                if (vl_iCodigo > 1)
                {
                    dnvListado.InvokeBind();
                }
            }
        }

        protected void btn_cerrar_Click(object sender, EventArgs e)
        {
            if (GlobalEntity.Instancia.CerrarExtranet == "S")
            {
                BL_Usuario oBL_Usuario = new BL_Usuario();
                oBL_Usuario.GrabaInicioSesion(GlobalEntity.Instancia.Usuario, "0");
                Session.Abandon();
                FormsAuthentication.SignOut();
                Response.Redirect("../../eFENIX_Login.aspx");
                Response.End();
            }
        }
    #endregion



    #region Metodo Transacciones
        DataNavigatorParams BindGridLista(object sender, EventArgs e)
        {
            BL_DocumentoOrigen oBL_DocumentoOrigen = new BL_DocumentoOrigen();
            BE_DocumentoOrigen oBE_DocumentoOrigen = new BE_DocumentoOrigen();

            oBE_DocumentoOrigen.iIdCliente = GlobalEntity.Instancia.IdCliente;
            //oBE_DocumentoOrigen.sAnoManifiesto = TxtAnoManifiesto.Text;
            //oBE_DocumentoOrigen.sNumManifiesto = TxtNumManifiesto.Text.Trim();
            //oBE_DocumentoOrigen.sDocumentoMaster = TxtDocumentoM.Text.Trim();
            //oBE_DocumentoOrigen.sVolante = TxtVolante.Text.Trim();
            //oBE_DocumentoOrigen.sOrden = TxtOrden.Text.Trim();
            
            GrvListado.PageSize = 12;
            oBE_DocumentoOrigen.NPagina = dnvListado.CurrentPage;
            oBE_DocumentoOrigen.NRegistros = GrvListado.PageSize;

            IList<BE_DocumentoOrigen> lista = oBL_DocumentoOrigen.ListarVolantesParaServicio(oBE_DocumentoOrigen);
            dnvListado.Visible = (oBE_DocumentoOrigen.NTotalRegistros > oBE_DocumentoOrigen.NRegistros);
            LblTotal.Text = "Total de Registros: " + Convert.ToString(oBE_DocumentoOrigen.NTotalRegistros);
            UdpTotales.Update();

            return new DataNavigatorParams(lista, oBE_DocumentoOrigen.NTotalRegistros);
        }

        protected void LlenaCombos()
        {
            BL_OrdenServicio oBL_OrdenServicio = new BL_OrdenServicio();
            List<BE_OrdenServicioDetalle> olst_BE_OrdenServicioDetalle = new List<BE_OrdenServicioDetalle>();

            int i = 0;

            DplServicio.Items.Clear();

            olst_BE_OrdenServicioDetalle = oBL_OrdenServicio.ListarServicioAduanero();
            DplServicio.Items.Add(new ListItem("[Seleccionar]", "-1"));

            if (olst_BE_OrdenServicioDetalle != null) 
                for (i = 0; i < olst_BE_OrdenServicioDetalle.Count; ++i)
                {
                    DplServicio.Items.Add(new ListItem(olst_BE_OrdenServicioDetalle[i].sDescripcion.ToString(), olst_BE_OrdenServicioDetalle[i].iCodigo.ToString()));
                }

            DplServicio.SelectedIndex = -1;
        }
    #endregion



    #region "Metodo Controles"
    
    
    #endregion
}