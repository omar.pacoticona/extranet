﻿<%@ Page Language="C#"
    AutoEventWireup="true" 
    CodeFile="eFEnix_Consultar_Refrendo.aspx.cs" 
     MasterPageFile="~/Extranet/Seguridad/eFENIX_Principal.master"
    Inherits="Extranet_Operaciones_eFEnix_Consultar_Refrendo"
    EnableEventValidation="true"%>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
     <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
    <title></title>
    <%----Agregando ultimo 290.6.2020--%>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
<%--    -- ====--%>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"/>
     <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>    
 <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js" type="text/javascript"></script>
   
      <script src="../../Script/Java_Script/jsSolicitudRefrendo.js"></script> 
     
    

</asp:Content>
  <asp:Content ID="Content3" ContentPlaceHolderID="ContentCab" Runat="Server">
        
        <span class="texto_titulo" >Tu Sesion Expirara en &nbsp; 
            <span class="texto_titulo" id="seconds"> </span>
            <span>&nbsp;seg.</span>
         </span>
         <asp:Button ID="btn_cerrar" runat="server" Text="cerrar" 
            onclick="btn_cerrar_Click"  Style="display: none;" />
    </asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="CntMaster" runat="Server">
    <!DOCTYPE html>
    
<html>


<body>

    <div class="form_titulo" style="width: 100%; height:23px;vertical-align: inherit">
        <p style="margin-top: 1.5px;font-size: 14px;">Listado de solicitudes</p>
    </div>
     <table class="form_bandeja" style=" width: 100%; margin-top:5px;">
        <tr>
              <td style="width:90px;">  
                  <asp:Label runat="server">N° Solicitud </asp:Label>
               </td>
                <td  style="width:220px;" >
                           <asp:TextBox ID="txtSolicitud" runat="server" Width="210px" Style="text-transform: uppercase;" autocomplete="off"></asp:TextBox>
                 </td>                        
            <td style="width:70px;">                          
                             <asp:Label runat="server">Booking </asp:Label>
                        </td>
                 <td style="width:200px;">
                <asp:TextBox ID="txtBooking" runat="server" Width="210px" Style="text-transform: uppercase;" autocomplete="off"></asp:TextBox>
              </td>
            <td style="width:70px;">                          
                             <asp:Label runat="server">Estado </asp:Label>
                        </td>
                 <td style="width:200px;">
               <asp:DropDownList ID="DplEstado" runat="server" Width="200px"></asp:DropDownList>
              </td>

               
                    </tr>
        <tr>
             <td style="width:84px;"> 
                 <asp:Label runat="server">FECHA INICIO </asp:Label>
                        </td>
                        <td style="width:160px;">
                              <input id="txtIni_datep" type="text" readonly size="25px" maxlength="10" name="DatePickername" class="inputText_extranet"  />
                        </td>
            <td style="width:84px;"> 
                 <asp:Label runat="server">FECHA FIN </asp:Label>
                        </td>
                        <td style="width:160px;">
                             <input id="txtFin_datep" type="text"  readonly  maxlength="10" size="25px"  name="DatePickerFinname" class="inputText_extranet" />
               </td>
            <td>

            </td>
            <td>
                <asp:Button ID="ImgBtnBuscar"  runat="server" Text="Consultar"  class="btn btn-primary" BackColor="#0069ae" ForeColor="#ffffff" OnClick="ImgBtnBuscar_Click"/>
            </td>
        </tr>
                  
    </table>
<div id="tabs-container">
    <form action="submit" id="frmServicios">
  <ul class="tabs">-
    <li><a href="#tabs-1">Listado</a></li>
    <li><a href="#tabs-2">Detalles de solicitud</a></li>    
  </ul>
  <div id="tabs-1" style="width:100%;">
    
    <%--         
     <fieldset  style="width:100%;background-color:white;">
     <legend style="background-color:#0069AE;font-weight:700;">Resultado de busqueda</legend>--%>
         <table  class="form_bandeja" style=" width: 98%; margin-top:1px;">
         <tr valign="top" style="height: 50%;width:99%;">
            <td>
           <ajax:UpdatePanel ID="UpdatePanel1" runat="server">
                     <ContentTemplate>
             <div style="overflow: auto; width: 100%; height: 250px;">
                            <asp:GridView ID="GrvListado" runat="server" AutoGenerateColumns="False"
                                SkinID="GrillaConsulta" Width="100%" OnRowCommand="GrvListado_RowCommand" DataKeyNames="sSolicitudRefr,iIdDocori,sCanal,sSituacion" OnRowDataBound="GrvListado_RowDataBound">
                                <Columns>
                                     <asp:TemplateField>  
                                           <ItemTemplate>
                                                      <asp:CheckBox ID="chkSeleccionar" runat="server" Visible="false" />
                                           </ItemTemplate>
                                        <HeaderStyle BackColor="#0069ae" Width="20px" />
                                        <ItemStyle Width="20px"></ItemStyle>
                                    </asp:TemplateField>
                                  <asp:BoundField DataField="sSolicitudRefr" HeaderText="Nro Solicitud" ItemStyle-HorizontalAlign="Center" >
                                    </asp:BoundField>
                                    <asp:BoundField DataField="sMesIni" HeaderText="FechaRegistro" ItemStyle-HorizontalAlign="Center" >
                                    </asp:BoundField>
                                    <asp:BoundField DataField="sNroDua" HeaderText="Nro DAM" ItemStyle-HorizontalAlign="Center"  >
                                    </asp:BoundField>
                                    <asp:BoundField DataField="sExportador" HeaderText="Exportador" ItemStyle-HorizontalAlign="Center" >
                                    </asp:BoundField>
                                      <asp:BoundField DataField="iIdDocori" HeaderText="IdDocOri" ItemStyle-HorizontalAlign="Center" visible="false">
                                    </asp:BoundField>
                                    <asp:BoundField DataField="sBooking" HeaderText="Booking" ItemStyle-HorizontalAlign="Center" >
                                    </asp:BoundField>
                                    <asp:BoundField DataField="sNave" HeaderText="Nave" ItemStyle-HorizontalAlign="Center" >
                                    </asp:BoundField>  
                                    <asp:BoundField DataField="sSituacion" HeaderText="IdSituacion" ItemStyle-HorizontalAlign="Center" visible="false">
                                    </asp:BoundField>
                                      <asp:BoundField DataField="sNombreSituacion" HeaderText="Estado" ItemStyle-HorizontalAlign="Center" >
                                    </asp:BoundField>  
                                    <asp:BoundField DataField="sCanal" HeaderText="Canal" ItemStyle-HorizontalAlign="Center" >
                                    </asp:BoundField>  
                                    
                                   
                                    <asp:ButtonField ButtonType="Image" CommandName="OpenDetalle" ItemStyle-HorizontalAlign="left" HeaderText="Ver Detalle" HeaderStyle-Width="10px" ImageUrl="~/Script/Imagenes/Detalle.png"
                                            Text="Ver Detalle">
                                    <ItemStyle  HorizontalAlign="Center" Width="5px" />
                                    </asp:ButtonField> 
                                </Columns>
                            </asp:GridView>
                        </div>
                     </ContentTemplate>
              </ajax:UpdatePanel>
                 </td>
        </tr> 
             <tr valign="top" style="height: 6%;">
            <td colspan="5">
               <ajax:UpdatePanel ID="upDnvListado" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <FENIX:DataNavigator ID="dnvListado" runat="server" AllowCustomPaging="True" ButtonText="Ir" 
                            ForeColor="White" Font-Size="11px" BackColor="#0069ae" 
                            CurrentPage="1" CustomClientFunction="" EnableAskingAfterPage="False" EnableCustomScript="False"
                            GridViewId="GrvListado" ImagesPath="~/Imagenes/DN/" IsParentShowWindow="False"
                            MessageAskingAfterPage="" PageIndicatorFormat="Página {0} / {1}" Visible="False"
                            Width="100%" WidthButtonIr="" WidthTextBoxNumPagina="25px" />
                    </ContentTemplate>
                    <Triggers>
                        <ajax:AsyncPostBackTrigger ControlID="dnvListado" EventName="PageChanged" />
                        <ajax:AsyncPostBackTrigger ControlID="ImgBtnBuscar" EventName="Click" />
                    </Triggers>
                </ajax:UpdatePanel>
            </td>
        </tr>
         </table>

     <%--</fieldset>--%>
    
  </div>
  <div id="tabs-2" style="width:100%;">
      <table width="100%">
          <tr>
              <td> <ajax:UpdatePanel ID="UpdatePanel2" runat="server">
                     <ContentTemplate>
                         
                  <div id="divDetalles">
                     <table class="form_bandeja" style="margin-top: 5px; margin-bottom: 5px; width: 98%;">                       
                         <tr>
                             <td style="width: 190px;"><asp:Label runat="server">DAM </asp:Label></td>
                             <td style="width: 220px"> <asp:TextBox  ID="TxtDua" readonly runat="server" Style="text-transform: uppercase" Enabled="false"
                                Width="175px"></asp:TextBox></td>                            
                             <td style="width: 120px"><asp:Label runat="server">BOOKING</asp:Label></td>
                            <td style="width: 229px"><asp:TextBox ID="txtHdBoking" readonly runat="server" MaxLength="15"  Style="text-transform: uppercase" Enabled="false"
                                Width="200px"></asp:TextBox></td>
                             <td style="width: 120px">
                            <asp:Label runat="server">FECHA LIMITE / C. SECA </asp:Label>
                            </td> 
                             <td style="width: 180px">
                            <asp:TextBox ID="txtCutCSeca" runat="server" MaxLength="15"  Style="text-transform: uppercase" Enabled="false"
                                Width="125px"></asp:TextBox>
                            </td> 
                              <td style="width: 120px">
                            <asp:Label runat="server">FECHA LIMITE / C. REFRIGERADA </asp:Label>
                            </td> 
                             <td style="width: 180px">
                            <asp:TextBox ID="txtCutCRefrigerada"  runat="server" MaxLength="15"  Style="text-transform: uppercase" Enabled="false"
                                Width="125px"></asp:TextBox>
                            </td> 
                             <%--  <td style="width: 120px">
                            <asp:Label runat="server">RUC A FACTURAR</asp:Label>
                            </td> 
                             <td style="width: 180px">
                            <asp:TextBox ID="txtRucFacturar"  runat="server" MaxLength="15"  Style="text-transform: uppercase" Enabled="false"
                                Width="125px"></asp:TextBox>
                            </td> --%>
                         </tr>
                           
                     </table>   
                       <table class="form_bandeja" style="margin-top: 5px; margin-bottom: 5px; width: 98%;">                       
                         <tr>
                             <td style="width: 110px"><asp:Label runat="server">CLIENTE A FACTURAR </asp:Label></td>
                             <td style="width: 200px">
                                  <asp:TextBox ID="txtClienteFacturar"  runat="server" Style="text-transform: uppercase" Enabled="false"
                                Width="530px"></asp:TextBox>
                             </td>
                             <td style="width: 100px"><asp:Label runat="server">RUC A FACTURAR </asp:Label></td>
                             <td style="width: 220px">                                 
                                  <asp:TextBox ID="txtRucFacturar"  runat="server" MaxLength="15"  Style="text-transform: uppercase" Enabled="false"
                                Width="125px"></asp:TextBox>
                             </td>
                             <td style="width: 30px">

                             </td>
                         </tr>
                           <tr>
                               <td style="width: 110px"><asp:Label runat="server">DESPACHADOR </asp:Label></td>
                             <td style="width: 200px">
                                  <asp:TextBox ID="txtDespachador"  runat="server" Style="text-transform: uppercase" Enabled="false"
                                Width="530px"></asp:TextBox>
                             </td>
                             <td style="width: 100px"><asp:Label runat="server"> DNI </asp:Label></td>
                             <td style="width: 220px">                                 
                                  <asp:TextBox ID="txtDNIDesp"  runat="server" MaxLength="15"  Style="text-transform: uppercase" Enabled="false"
                                Width="125px"></asp:TextBox>
                             </td>
                             <td style="width: 30px">

                             </td>
                           </tr>
                           </table>
               </div>
                         </ContentTemplate>
                  </ajax:UpdatePanel>
                        
              </td>
            </tr>
          <tr>
              <td>
                 <table>
                                    <tr valign="top" style="height: 90%; width: 700px;">
                                        <td colspan="4">
                                            <%--<asp:UpdatePanel runat="server" UpdateMode="Conditional">
                                                <ContentTemplate>--%>
                                                    <asp:Panel ID="Panel5" runat="server" BackColor="White" Height="180px" ScrollBars="Vertical" Width="1000px">
                                                        <table  border="2" style="border-radius:20px;">
                                                            <tr style="color: ivory; background-color: #0069AE">
                                                                <%--<th>id</th>--%>
                                                                <th style="width:150px;">Documentos</th>
                                                                <th style="width:5px;"></th>
                                                                <th style="width:800px;">Nombre del archivo</th>
                                                                <th style="width:100px;">Estado</th>                                                                
                                                            </tr>
                                                            <tr>
                                                                <asp:Label runat="server" ID="lblIdDAM" Text="0" Visible="False"></asp:Label>
                                                                <td> DAM
                                                                </td>
                                                                <td>
                                                                    <asp:ImageButton runat="server" ID="btnVerDAM" ImageUrl="~/Script/Imagenes/IconButton/descargar.png" Width="15px" OnClick="btnVerDAM_Click" />
                                                                </td>
                                                                <td>
                                                                    <asp:Label runat="server" ID="lblNombreArchivoDua" Text="" ></asp:Label>
                                                                </td>
                                                                <td>
                                                                    <asp:Label runat="server" ID="lblEstadoDua" Text=" "></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <asp:Label runat="server" ID="lblIdBokoking" Text="0" Visible="False"></asp:Label>
                                                                <td> Booking
                                                                </td>   
                                                                <td>
                                                                    <asp:ImageButton runat="server" ID="btnVerBook" ImageUrl="~/Script/Imagenes/IconButton/descargar.png" Width="15px" OnClick="btnVerBook_Click" />
                                                                </td>
                                                                <td>
                                                                    <asp:Label runat="server" ID="lblNombreArchivoBook" Text=""></asp:Label>
                                                                </td>
                                                                <td>
                                                                    <asp:Label runat="server" ID="lblEstadoBook" Text=" "></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <asp:Label runat="server" ID="LblIdTicket" Text="0" Visible="False"></asp:Label>
                                                                <td> TicketPeso
                                                                </td>  
                                                                <td>
                                                                    <asp:ImageButton runat="server" ID="btnverTicetk" ImageUrl="~/Script/Imagenes/IconButton/descargar.png" Width="15px" OnClick="btnverTicetk_Click" />
                                                                </td>
                                                                
                                                                <td>
                                                                    <asp:Label runat="server" ID="lblNombreArchivoTicket" Text=" "></asp:Label>
                                                                </td>
                                                                <td>
                                                                    <asp:Label runat="server" ID="lblEstadoTicket" Text=" "></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <asp:Label runat="server" ID="lblIdPackList" Text="0" Visible="False"></asp:Label>
                                                                <td> Packing List
                                                                </td>              
                                                                <td>
                                                                    <asp:ImageButton runat="server" ID="btnverPackList" ImageUrl="~/Script/Imagenes/IconButton/descargar.png" Width="15px" OnClick="btnverPackList_Click" />
                                                                </td>
                                                                
                                                                <td>
                                                                    <asp:Label runat="server" ID="lblNombreArchivoPackList" Text=" "></asp:Label>
                                                                </td>
                                                                <td>
                                                                    <asp:Label runat="server" ID="lblEstadoPasList" Text=" "></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <asp:Label runat="server" ID="lblIdGuiaREM" Text="0" Visible="False"></asp:Label>
                                                                <td> GuiaRemision
                                                                </td>                                                               
                                                                <td>
                                                                    <asp:ImageButton runat="server" ID="btnVerGuia" ImageUrl="~/Script/Imagenes/IconButton/descargar.png" Width="15px" OnClick="btnVerGuia_Click" />
                                                                </td>
                                                                <td>
                                                                    <asp:Label runat="server" ID="lblNombreArchivoGuia" Text=" "></asp:Label>
                                                                </td>
                                                                <td>
                                                                    <asp:Label runat="server" ID="lblEstadoGuia" Text=" "></asp:Label>
                                                                </td>
                                                            </tr>
                                                             <tr>
                                                                 <asp:Label runat="server" ID="ldlIdDocPago" Text="0" Visible="False"></asp:Label>
                                                                <td> Documento de Pago
                                                                </td>                                                               
                                                                <td>
                                                                    <asp:ImageButton runat="server" ID="btnVerDocPago" ImageUrl="~/Script/Imagenes/IconButton/descargar.png" Width="15px" OnClick="btnVerDocPago_Click" />
                                                                </td>
                                                                <td>
                                                                    <asp:Label runat="server" ID="lblNombreArchivoDocPago" Text=" "></asp:Label>
                                                                </td>
                                                                <td>
                                                                    <asp:Label runat="server" ID="lblEstadoDocPago" Text=""></asp:Label>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </asp:Panel>
                                                <%--</ContentTemplate>   
                                                
                                           </asp:UpdatePanel>--%>
                                        </td>
                                    </tr>
                                    
                                </table>
              </td>
          </tr>          
      </table>
       <div id="divDetalles2" style="width:98%;">
                     <table class="form_bandeja" style="margin-top: 5px; margin-bottom: 5px; width: 100%;">                       
                         <tr  valign="top" style="height: 50%; width: 59%;">
                             <td style="width: 500px">
                                 <asp:Label runat="server">Comentarios del Cliente </asp:Label>
                             </td>
                             <td style="width: 500px"> 
                                 <asp:Label runat="server">Comentarios de la revision </asp:Label>

                             </td>                            
                           
                         </tr>
                         <tr>
                             <td>
                                 <asp:TextBox id="txtObservacionesCliente" runat="server" TextMode="MultiLine"  Width="520px" cols="70" rows="2" Enabled="false" placeholder=""></asp:TextBox>
                             </td>
                              <td>
                                 <asp:TextBox id="txtComentariosRevision" runat="server" TextMode="MultiLine"  Width="520px" cols="70" rows="2" Enabled="false" placeholder=""></asp:TextBox>
                             </td>
                         </tr>
                           
                     </table>   
            </div>
        <div id="divDetalles3">
            <table  class="form_bandeja" style="margin-top: 5px; margin-bottom: 5px; width: 98%;">
            <tr valign="top" style="height: 50%; width: 99%;">
                <td>
                    <ajax:UpdatePanel ID="UpdatePanel3" runat="server">
                        <ContentTemplate>
                            <div style="overflow: auto; width: 100%; height: 100px;">
                                <asp:GridView ID="GrvListadoDetalle" runat="server" AutoGenerateColumns="False" DataKeyNames="iIdDocOridet"
                                    SkinID="GrillaConsulta" Width="100%">
                                    <Columns>
                                        <%--<asp:TemplateField>                                                       
                                                        <ItemTemplate>
                                                            <asp:CheckBox ID="chkSeleccionarDetalle" runat="server"/>
                                                        </ItemTemplate>
                                                        <ItemStyle Width="1%" />
                                    </asp:TemplateField>--%>
                                        <asp:BoundField DataField="iIdDocOridet" HeaderText="Contenedor" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5px" Visible="false"></asp:BoundField>
                                        <asp:BoundField DataField="sContenedor" HeaderText="Contenedor" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5px"></asp:BoundField>
                                        <asp:BoundField DataField="sTipoContenedor" HeaderText="TipoContenedor" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5px"></asp:BoundField>
                                        <asp:BoundField DataField="sDimension" HeaderText="Dimension" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5px"></asp:BoundField>                                       
                                        <asp:BoundField DataField="sSituacion" HeaderText="Situacion" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5px" Visible="false"></asp:BoundField>
                                        <asp:BoundField DataField="sBultos" HeaderText="Bultos" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5px"></asp:BoundField>
                                        <asp:BoundField DataField="sPrecintoAduanas" HeaderText="Precinto Aduana" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5px"></asp:BoundField>
                                        <asp:BoundField DataField="sPrecintoLinea" HeaderText="Precinto Linea" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5px"></asp:BoundField>

                                    </Columns>
                                </asp:GridView>
                            </div>
                        </ContentTemplate>
                    </ajax:UpdatePanel>
                </td>
            </tr>
                </table>
        </div>
  </div>

    </form>
</div>
   
</body>
</html>


   <script language="javascript" type="text/javascript">
       var tiempo = 0;
       var seconds = 0;

       var myVar = setInterval(function () {
           clearInterval(myVar);
       }, 1000);

       var myTiempo = setTimeout(function () {
           clearInterval(myTiempo);
       }, 1);

       function myStopFunction() {
           clearInterval(myVar);
            //seconds = '<%=GlobalEntity.Instancia.TiempoDuracionSesion%>' * 60;
            tiempo = '<%=GlobalEntity.Instancia.TiempoDuracionSesion%>' * 1000 * 60;

           CuentaTiempo(tiempo);
       }

       function CuentaTiempo(tiempo) {
           clearTimeout(myTiempo);
           myTiempo = setTimeout(function () {

               //window.location = "../Seguridad/eFENIX_Login.aspx";
           }, tiempo);
       }



       function SessionExpireAlert(timeout) {
           seconds = timeout / 1000;
           document.getElementsByName("seconds").innerHTML = seconds;

           myvar = setInterval(function () {
               seconds--;
               document.getElementById("seconds").innerHTML = seconds;
           }, 1000);

           CuentaTiempo(timeout);
       };


   </script>

       <%--   </table>--%>

    <style type="text/css">
#reservationsList {width:500px; max-height:600px; background:#fff; overflow:auto;}    
 
/*STRIPE LIST*/    
ul.stripedList {
    margin:0;
    padding:0;
    list-style:none;
}
.stripedList li {
    display:block;
    text-decoration:none;
    color:#333;
    font-family:Arial,Helvetica,sans-serif;
    font-size:12px;
    line-height:20px;
    height:20px;
}



.stripedList li span {
    display:inline-block;
    border-right:1px solid #ccc;
    overflow:hidden;
    text-overflow:ellipsis;
    padding:0 10px;
    height:20px;
}
.stripedList .evenRow {background:#f2f2f2;}
 
.c1 {width:55px;}
.c2 {width:70px;}
.c3 {width:130px;}
.c4 {width:15px;}
.cLast {border:0 !important;}
 
        .auto-style1 {
            margin-bottom: 4px;
        }
 
    </style>

</asp:Content>


