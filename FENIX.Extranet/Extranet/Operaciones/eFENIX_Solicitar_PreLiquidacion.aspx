﻿<%@ Page Language="C#"  MasterPageFile="~/Extranet/Seguridad/eFENIX_Principal.master" AutoEventWireup="true" CodeFile="eFENIX_Solicitar_PreLiquidacion.aspx.cs" Inherits="Extranet_Operaciones_eFENIX_Solicitar_PreLiquidacion" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"/>
     <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
   
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js" type="text/javascript"></script>
     <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js" type="text/javascript"></script>
    <script src="../../Script/Java_Script/jsAcuerdoComision.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CntMaster" runat="Server">


    <table cellspacing="0" cellpadding="0" style="width: 100%;">
        <tr>
            <td class="form_titulo" style="width: 85%">
                <asp:TextBox ID="LblTitulo" Text="Generare Pre - Liquidacion" ReadOnly="true" 
                    style="background-color: #0069ae; border: #0069ae; color: #FFFFFF; font-family: Calibri; font-size: 13px; font-weight: bold; text-transform: uppercase;" 
                    runat="server" Width="231px" ></asp:TextBox>
            </td> 
        </tr>
    </table>

  

     <table class="form_bandeja" style=" width: 100%; margin-top:5px; height: 50px;">
         <tr>
        <td style="width: 12%">
                         &nbsp;&nbsp;&nbsp;&nbsp;  BL / Volante 
                        </td>
                        <td style="width:53px;">
                            <asp:TextBox ID="txtDocumento" runat="server" Width="220px"  Style="text-transform: uppercase; margin-left: 27;" autocomplete="off" ></asp:TextBox>
                            <asp:TextBox ID="txtSuperNegocio" runat="server" Width="220px"  Style="text-transform: uppercase; margin-left: 27;" autocomplete="off" Visible="false" ></asp:TextBox>
                            <asp:TextBox ID="HdtxtIddocOri" runat="server" Width="220px"  Style="text-transform: uppercase; margin-left: 27;" autocomplete="off" Visible="false" ></asp:TextBox>
                            <asp:TextBox ID="HdtxtIdVolante" runat="server" Width="220px"  Style="text-transform: uppercase; margin-left: 27;" autocomplete="off" Visible="false"></asp:TextBox>
                            <asp:TextBox ID="txtIdTipoMoneda" runat="server" Width="220px"  Style="text-transform: uppercase; margin-left: 27;" autocomplete="off" Visible="false" ></asp:TextBox>
                            <asp:TextBox ID="txtOperacion" runat="server" Width="220px"  Style="text-transform: uppercase; margin-left: 27;" autocomplete="off" Visible="false" ></asp:TextBox>
                            <asp:TextBox ID="HdtxtIdAcuerdo" runat="server" Width="220px"  Style="text-transform: uppercase; margin-left: 27;" autocomplete="off" Visible="false" ></asp:TextBox>

                             <%--<input id="txtIni_datep" type="text" readonly size="20px" maxlength="10" name="DatePickername" class="inputText_extranet"  />--%>
                        </td>
             <td style="width: 55px;">  
                 <asp:Button ID="ImgBtnBuscar"  runat="server" Text="Consultar" class="btn btn-primary" BackColor="#0069ae" OnClick="ImgBtnBuscar_Click"/></td>
             </tr>
     </table>

     <table class="form_bandeja" style=" width: 100%; margin-top:5px;">
          <tr >
               <td >
                    <ajax:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional" >
                    <ContentTemplate>  
                 <table class="form_bandeja" style="width: 90%; margin-top: 2px; margin-left:110px;">
                     <tr>

                         <td style="width: 25%">Cliente  
                         </td>
                         <td style="width: 100px;">
                             
                             <asp:Label ID="lblCliente" runat="server" Text=" - "
                                 Visible="true" CssClass="lblComisionista"></asp:Label>
                         </td>
                     </tr>
                     <tr>
                         <td style="width: 10%">Agencia de Aduana  
                         </td>
                         <td style="width: 13px;">
                            <asp:Label ID="lblAgntAduana" runat="server" Text=" - "
                                 Visible="true" CssClass="lblComisionista"></asp:Label>
                         </td>
                     </tr>
                     <tr>
                         <td style="width: 10%">Fecha Ingreso Almacen  
                         </td>
                         <td style="width: 13px;">
                             <asp:Label ID="lblFechaIngreso" runat="server" Text=" - "
                                 Visible="true" CssClass="lblComisionista"></asp:Label>
                         </td>
                     </tr>
                 </table>
                       
                    </ContentTemplate>
                         </ajax:UpdatePanel>
                   </td>
              </tr>

     </table>



      <table class="form_bandeja" style=" width: 100%; margin-top:5px;">
           <tr>
            <td class="form_titulo" style="width: 85%">
                <asp:TextBox ID="TextBox10" Text="Detallar fecha de retiro de la carga" ReadOnly="true" 
                    style="background-color: #0069ae; border: #0069ae; color: #FFFFFF; font-family: Calibri; font-size: 13px; font-weight: bold; text-transform: uppercase;" 
                    runat="server" Width="231px" ></asp:TextBox>
            </td> 
            <td  style="width: 85%">
                 <asp:Button ID="btnCakcular"  runat="server" Text="Ver detalles de servicios" class="form-control" BackColor="#0069ae" ForeColor="White" OnClick="btnCakcular_Click"/>
            </td> 
        </tr>
         
         <tr>
             <td style="width:100%;">
                 <table class="form_bandeja" style="width: 90%; margin-bottom: 80px;">
                     <tr>
                         <td style="width: 10%">
                              <ajax:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional" >
                    <ContentTemplate>                         
                        <div style="overflow: auto; width: 100%; height: 150px;">
                            <asp:GridView ID="GrvListaContenedores" runat="server" AutoGenerateColumns="False"
                                SkinID="GrillaConsulta" Width="100%" DataKeyNames="iIdDocOriDet,dtFechaRetiro" OnRowDataBound="GrvListaContenedores_RowDataBound">
                                <Columns>
                                     <asp:TemplateField>      
                                           <ItemTemplate>
                                                      <asp:CheckBox ID="chkSeleccionarContenedor" runat="server" />
                                           </ItemTemplate>
                                        <HeaderStyle BackColor="#0069ae" Width="20px" />
                                        <ItemStyle Width="20px"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="iIdDocOriDet" HeaderText="" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5px">                                       
                                    </asp:BoundField>
                                     <asp:BoundField DataField="sNumeroDo" HeaderText="Documento" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5px">                                       
                                    </asp:BoundField>
                                     <asp:BoundField DataField="iItem" HeaderText="Item" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5px">                                       
                                    </asp:BoundField>
                                     <asp:BoundField DataField="sContenedor" HeaderText="Contenedor/Chasis" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5px">                                       
                                    </asp:BoundField>
                                     <asp:BoundField DataField="iIdTamanoContenedor" HeaderText="Tamaño" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5px">                                       
                                    </asp:BoundField>
                                    <asp:BoundField DataField="sTipoContenedor" HeaderText="Tipo" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5px">                                       
                                    </asp:BoundField>
                                    <asp:BoundField DataField="sCondCarga" HeaderText="Condicion" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5px">                                       
                                    </asp:BoundField>    
                                     <asp:BoundField DataField="sFechaIngreso" HeaderText="Fecha Ingreso" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5px">                                       
                                    </asp:BoundField>
                                     <asp:TemplateField HeaderText="Fecha Retiro"  HeaderStyle-Width="100px" ItemStyle-HorizontalAlign="Center">
                                                         <ItemTemplate>
                                                                 <%--<asp:Label ID="lblSerie" runat ="server" Text='<%# Eval("sDocNroSerie") %>' />  --%>
                                                             <asp:TextBox ID="TxtSerie" CssClass="TxtSerie" runat="server" Width="90px" Placeholder="DD/MM/YYYY" Text='<%# Bind("dtFechaRetiro") %>'   Style="text-transform: uppercase"></asp:TextBox>
                                                            <%--<input id="txtFin_datep" type="text"  readonly  maxlength="10" name="DatePickerFinname" class="inputText_extranet"/>--%>
                                                           </ItemTemplate>
                                                        <%--<EditItemTemplate>
                                                          <asp:TextBox ID="TxtSerie" runat="server" Width="50px"  Text='<%# Bind("sDocNroSerie") %>' Placeholder="INGRESE NRO SERIE"   MaxLength="4"></asp:TextBox>  
                                                        </EditItemTemplate>   --%>                                                        
                                                        <HeaderStyle ForeColor="White" />                                                                                    
                                    </asp:TemplateField>
                                    
                                </Columns>
                            </asp:GridView>
                        </div>
                    </ContentTemplate>  
                                   <Triggers>       
                         <ajax:AsyncPostBackTrigger ControlID="GrvListaContenedores" EventName="SelectedIndexChanged" />  
<asp:AsyncPostBackTrigger ControlID="GrvListaContenedores" EventName="SelectedIndexChanged"></asp:AsyncPostBackTrigger>
                        <asp:AsyncPostBackTrigger ControlID="GrvListaContenedores" EventName="SelectedIndexChanged"></asp:AsyncPostBackTrigger>
                        <asp:AsyncPostBackTrigger ControlID="GrvListaContenedores" EventName="SelectedIndexChanged"></asp:AsyncPostBackTrigger>                    
                        
                             <asp:AsyncPostBackTrigger ControlID="ImgBtnBuscar" EventName="Click"></asp:AsyncPostBackTrigger>
                        
                    </Triggers>
                </ajax:UpdatePanel>
                         </td>                        
                     </tr>
                 </table>
             </td>

             <td style="width:500px;">
                 
                  <table class="form_bandeja" style="width: 100%; margin-top: 1px; margin-left:10px;">
                      
                      <tr>
                         <td style="width: 100%">Valor de venta  
                         </td>
                         <td style="width: 20px;">
                            <asp:Label ID="Label1" Width="90px" runat="server" Text=" - "
                                 Visible="true" CssClass="lblComisionista"></asp:Label>
                         </td>
                     </tr>
                     <tr>
                         <td style="width: 10%">Descuento 
                         </td>
                         <td style="width: 13px;">
                              <asp:Label ID="Label2"  Width="90px" runat="server" Text=" - "
                                 Visible="true" CssClass="lblComisionista"></asp:Label>
                         </td>
                     </tr>
                     <tr>
                         <td style="width: 10%">IGV 
                         </td>
                         <td style="width: 13px;">
                             <asp:Label ID="Label3"  Width="90px" runat="server" Text=" - "
                                 Visible="true" CssClass="lblComisionista"></asp:Label>
                         </td>
                     </tr>
                      <tr>
                         <td style="width: 9%">Total $  
                         </td>
                         <td style="width: 20px;">
                            <asp:Label ID="Label4"  Width="90px" runat="server" Text=" - "
                                 Visible="true" CssClass="lblComisionista"></asp:Label>
                         </td>
                     </tr>
                     <tr>
                         <td style="width: 10%">Total S/. 
                         </td>
                         <td style="width: 13px;">
                             <asp:Label ID="Label5"  Width="90px" runat="server" Text=" - "
                                 Visible="true" CssClass="lblComisionista"></asp:Label>
                         </td>
                     </tr>
                     <tr>
                         <td style="width: 10%">Tipo de Cambio  
                         </td>
                         <td style="width: 80px;">
                             <asp:Label ID="Label6"   Width="90px" runat="server" Text=" -  "
                                 Visible="true" CssClass="lblComisionista"></asp:Label>
                         </td>
                     </tr>
                 </table>
                     
                 </td>
             </tr>

<%--          <tr>
              <td style="width: 1006px">
                    &nbsp;
              </td>
            
          </tr>--%>

        <%--  <tr valign="top" style="height: 70%; margin-top:10px;">
           <td colspan="5">
                <ajax:UpdatePanel ID="upDocumentoOrigen" runat="server" UpdateMode="Conditional" >
                    <ContentTemplate>                         
                        <div style="overflow: auto; width: 100%; height: 150px;">
                            <asp:GridView ID="GrvListado" runat="server" AutoGenerateColumns="False"
                                SkinID="GrillaConsulta" Width="100%">
                                <Columns>
                                     <asp:BoundField DataField="dImporte" HeaderText="Importe" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5px">                                       
                                    </asp:BoundField>
                                     <asp:BoundField DataField="dImporte" HeaderText="Importe" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5px">                                       
                                    </asp:BoundField>
                                     <asp:BoundField DataField="dImporte" HeaderText="Importe" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5px">                                       
                                    </asp:BoundField>

                                </Columns>
                            </asp:GridView>
                        </div>
                    </ContentTemplate>                   
                </ajax:UpdatePanel>
            </td>
         </tr>--%>
          </table>
     <table class="form_bandeja" style=" width: 100%; margin-top:5px;">
           <tr>
            <td class="form_titulo" style="width: 85%">
                <asp:TextBox ID="TextBox11" Text="Detalle de servicios generados" ReadOnly="true" 
                    style="background-color: #0069ae; border: #0069ae; color: #FFFFFF; font-family: Calibri; font-size: 13px; font-weight: bold; text-transform: uppercase;" 
                    runat="server" Width="231px" ></asp:TextBox>
            </td> 
        </tr>
         
          <tr valign="top" style="height: 70%; margin-top:10px;">
           <td colspan="5">
                <ajax:UpdatePanel ID="upDocumentoOrigen" runat="server" UpdateMode="Conditional" >
                    <ContentTemplate>                         
                        <div style="overflow: auto; width: 100%; height: 150px;">
                            <asp:GridView ID="GrvListado" runat="server" AutoGenerateColumns="False"
                                SkinID="GrillaConsulta" Width="100%" DataKeyNames="iIdDocOriDet">
                                <Columns>
                                     <asp:BoundField DataField="iIdOrdSer" HeaderText="Nro OrdSer" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5px">                                       
                                    </asp:BoundField>
                                     <asp:BoundField DataField="iItem" HeaderText="Item" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5px">                                       
                                    </asp:BoundField>
                                     <asp:BoundField DataField="sDescripcionServicio" HeaderText="Servicio" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5px">                                       
                                    </asp:BoundField>
                                    <asp:BoundField DataField="sContenedor" HeaderText="Cont." ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5px">                                       
                                    </asp:BoundField>
                                     <asp:BoundField DataField="sCondCarga" HeaderText="IMO" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5px">                                       
                                    </asp:BoundField>
                                     <asp:BoundField DataField="sTipoCarga" HeaderText="Carga" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5px">                                       
                                    </asp:BoundField>
                                    <asp:BoundField DataField="sTipoContenedor" HeaderText="T. Cont." ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5px">                                       
                                    </asp:BoundField>
                                     <asp:BoundField DataField="iIdTamanoContenedor" HeaderText="Tamaño" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5px">                                       
                                    </asp:BoundField>
                                     <asp:BoundField DataField="sEmbalaje" HeaderText="Emb." ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5px">                                       
                                    </asp:BoundField>
                                    <asp:BoundField DataField="dMontoUnitario" HeaderText="P. Unit." ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5px">                                       
                                    </asp:BoundField>
                                     <asp:BoundField DataField="dCantidad" HeaderText="Cantidad" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5px">                                       
                                    </asp:BoundField>
                                     <asp:BoundField DataField="sTipoMoneda" HeaderText="Moneda" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5px">                                       
                                    </asp:BoundField>
                                    <asp:BoundField DataField="dAfecto" HeaderText="Afecto" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5px">                                       
                                    </asp:BoundField>
                                     <asp:BoundField DataField="dInafecto" HeaderText="Inafecto" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5px">                                       
                                    </asp:BoundField>
                                     <asp:BoundField DataField="dDescuento" HeaderText="Dscto." ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5px">                                       
                                    </asp:BoundField>
                                    <asp:BoundField DataField="dIgv" HeaderText="IGV" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5px">                                       
                                    </asp:BoundField>
                                     <asp:BoundField DataField="dTotal" HeaderText="Total" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5px">                                       
                                    </asp:BoundField>
                                     <asp:BoundField DataField="dDetraccion" HeaderText="Det." ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5px">                                       
                                    </asp:BoundField>
                                    <asp:BoundField DataField="sModalidad" HeaderText="Modalidad" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5px">                                       
                                    </asp:BoundField>
                                     <asp:BoundField DataField="sOrigen" HeaderText="Origen" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5px">                                       
                                    </asp:BoundField>
                                     <asp:BoundField DataField="dtFecha_Ingreso" HeaderText="F. Ingreso" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5px">                                       
                                    </asp:BoundField>                                 

                                </Columns>
                            </asp:GridView>
                        </div>
                    </ContentTemplate>                   
                </ajax:UpdatePanel>
            </td>
         </tr>
         </table>

     

    
    <script language="javascript" type="text/javascript">

        var tiempo = 0;
        var seconds = 0;

        var myVar = setInterval(function () {
            clearInterval(myVar);
        }, 1000);

        var myTiempo = setTimeout(function () {
            clearInterval(myTiempo);
        }, 1);

        function myStopFunction() {
            clearInterval(myVar);
            seconds = '<%=GlobalEntity.Instancia.TiempoDuracionSesion%>' * 60;
            tiempo = '<%=GlobalEntity.Instancia.TiempoDuracionSesion%>' * 1000 * 60;

            CuentaTiempo(tiempo);
        }

        function CuentaTiempo(tiempo) {
            clearTimeout(myTiempo);
            myTiempo = setTimeout(function () {
                document.getElementById('<%=btn_cerrar.ClientID%>').click();
                //window.location = "../Seguridad/eFENIX_Login.aspx";
            }, tiempo);
        }



        function SessionExpireAlert(timeout) {
            seconds = timeout / 1000;
            document.getElementsByName("seconds").innerHTML = seconds;

            myvar = setInterval(function () {
                seconds--;
                document.getElementById("seconds").innerHTML = seconds;
            }, 1000);

            CuentaTiempo(timeout);
        };


        var goodexit = false;
        window.onbeforeunload = confirmRegisterExit;
        function confirmRegisterExit(evt) {
            if (!goodexit) {
                //return "Si vas abandonar el sistema haz click en Cerrar Sesion!!";
                document.getElementById('<%=btn_cerrar.ClientID%>').click();
            }
        }

  
    </script>

</asp:Content>
<asp:Content ID="Content3" runat="server" contentplaceholderid="ContentCab">
     <span class="texto_titulo" >Tu Sesion Expirara en &nbsp; 
        <span class="texto_titulo" id="seconds"> </span>
        <span>&nbsp;seg.</span>
     </span>
     <asp:Button ID="btn_cerrar" runat="server" Text="cerrar" 
        onclick="btn_cerrar_Click"  Style="display: none;" />
</asp:Content>

