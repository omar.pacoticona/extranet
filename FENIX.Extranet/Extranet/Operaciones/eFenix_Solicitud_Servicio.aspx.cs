﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using QNET.Web.UI.Controls;
using System.Data;
using FENIX.BusinessLogic;
using FENIX.BusinessEntity;
using System.Web.Services;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;

namespace Operaciones
{
    public partial class eFenix_Solicitud_Servicio : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
            ScriptManager scriptManager = ScriptManager.GetCurrent(this.Page);
            scriptManager.RegisterPostBackControl(this.GrvListado);
            //  dnvListado.BindGridView += new QNET.Web.UI.Controls.DataNavigator.BindGridViewDelegate(BindGridLista);
            if (!Page.IsPostBack)
        {
            ListarItems();
        }
    }

    private void SCA_MsgInformacion(string strError)
    {
        MensajeScript(strError);
    }

    private void MensajeScript(string SMS)
    {
        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "", "<script>alert('" + SMS + "');</script>", false);

        
    }

    protected void ImgBtnBuscar_Click(object sender, ImageClickEventArgs e)
    {

        ListarDetalles();
       // dnvListado.InvokeBind();

        //if (GrvListado.Rows.Count == 0)
        //{
        //    List<BE_Detalle> lsBE_Detalle = new List<BE_Detalle>();
        //    BE_Detalle oBE_Detalle = new BE_Detalle();
        //    lsBE_Detalle.Add(oBE_Detalle);
        //    GrvListado.DataSource = lsBE_Detalle;
        //    GrvListado.DataBind();
        //    GrvListado.Visible = false;
        //}     

    }

    protected void ImgBtnNuevo_Click(object sender, ImageClickEventArgs e)
    {

    }

    protected void ListarItems()
    {
        /*LISTAR OPERACIONES*/
        BL_Servicio oBL_Servicio = new BL_Servicio();
        List<BE_Operacion> oLst_BE_Operacion = new List<BE_Operacion>();
        Int32 i = 0;
        dplTipoOperacion.Items.Clear();
        oLst_BE_Operacion = oBL_Servicio.ListarItems();
        dplTipoOperacion.Items.Add(new System.Web.UI.WebControls.ListItem("[Seleccionar]", "-1"));
        for (i = 0; i < oLst_BE_Operacion.Count; ++i)
        {
            dplTipoOperacion.Items.Add(new System.Web.UI.WebControls.ListItem(oLst_BE_Operacion[i].sTipoOperacion.ToString(), oLst_BE_Operacion[i].iIdTipoOperacion.ToString()));

        }
        dplTipoOperacion.SelectedIndex = -1;


        /*LISTAR SERVICIOS*/
        List<BE_Servicio> oLst_BE_Servicio = new List<BE_Servicio>();
        Int32 e = 0;
        dplServicio.Items.Clear();
        oLst_BE_Servicio = oBL_Servicio.ListarServicios();
        dplServicio.Items.Add(new System.Web.UI.WebControls.ListItem("[Seleccionar]", "-1"));
        for (e = 0; e < oLst_BE_Servicio.Count; ++e)
        {
            dplServicio.Items.Add(new System.Web.UI.WebControls.ListItem(oLst_BE_Servicio[e].sServicio.ToString(), oLst_BE_Servicio[e].iIdServicio.ToString()));

        }
        dplServicio.SelectedIndex = -1;

    }

    DataNavigatorParams BindGridLista(object sender, EventArgs e)
    {
        BL_Servicio oBL_Servicio = new BL_Servicio();
        BE_eOrdenServicio oBE_Servicio = new BE_eOrdenServicio();
        List<BE_Detalle> oBE_DetalleLis;
   
        oBE_Servicio.iIdTipoOperacion = Convert.ToInt32(dplTipoOperacion.SelectedValue);
        oBE_Servicio.iIdClienteAduana = GlobalEntity.Instancia.IdCliente;
        oBE_Servicio.sNumeroDO = TxtDocumentoM.Text.Trim();
        oBE_Servicio.sNumeroVO = TxtVolante.Text.Trim();
  
        oBE_DetalleLis = oBL_Servicio.listarDetalleServicio(oBE_Servicio);
        GrvListado.Width = 1055;
        dnvListado.Visible = (oBE_DetalleLis.Count() > 0);

        ViewState["oBE_DetalleLis"] = oBE_DetalleLis;
       // LblTotal.Text = "Total de Registros: " + Convert.ToString(oBE_DocumentoOrigenListaList.Count());
       // UdpTotales.Update();


        return new DataNavigatorParams(oBE_DetalleLis, oBE_DetalleLis.Count());
    }


    protected void ListarDetalles()
    {
        BL_Servicio oBL_Servicio = new BL_Servicio();
        BE_eOrdenServicio oBE_Servicio = new BE_eOrdenServicio();
        List<BE_Detalle> oBE_DetalleLis;

        oBE_Servicio.iIdTipoOperacion = Convert.ToInt32(dplTipoOperacion.SelectedValue);
        oBE_Servicio.iIdClienteAduana = GlobalEntity.Instancia.IdCliente;
        oBE_Servicio.sNumeroDO = TxtDocumentoM.Text.Trim();
        oBE_Servicio.sNumeroVO = TxtVolante.Text.Trim();

        oBE_DetalleLis = oBL_Servicio.listarDetalleServicio(oBE_Servicio);
        ViewState["oBE_DetalleLis"] = oBE_DetalleLis;
        GrvListado.Width = 1055;
        //  dnvListado.Visible = (oBE_DetalleLis.Count() > 0);
        dnvListado.Visible = false;
        GrvListado.DataSource = oBE_DetalleLis;
        GrvListado.DataBind();
        GrvListado.Visible = true;

        if (GrvListado.Rows.Count == 0)
        {
            List<BE_Detalle> lsBE_Detalle = new List<BE_Detalle>();
            BE_Detalle oBE_Detalle = new BE_Detalle();
            lsBE_Detalle.Add(oBE_Detalle);
            GrvListado.DataSource = lsBE_Detalle;
            GrvListado.DataBind();
            GrvListado.Visible = false;

            SCA_MsgInformacion("No se encontraron resultados");

        }
    }

    protected void btnTab2P_Click(object sender, ImageClickEventArgs e)
    {

    }

    protected void btnTab1_Click1(object sender, ImageClickEventArgs e)
    {
        String sDocumento = String.Empty;
       // Int32 iSeleccionados = 0;
        List<String> lstValidateSDoc = new List<String>();


        foreach (GridViewRow GrvRow in GrvListado.Rows)
        {
            if ((GrvRow.FindControl("chkSeleccionar") as CheckBox).Checked)
            {
                sDocumento = GrvListado.DataKeys[GrvRow.RowIndex].Values["sNumeroDO"].ToString();
                lstValidateSDoc.Add(sDocumento);
              //  iSeleccionados = iSeleccionados + 1;
            }
        }

        //var allAreSame = lstValidateSDoc.All(x => x == lstValidateSDoc.First());

        //if (!allAreSame)
        //{
        //    SCA_MsgInformacion("Debe seleccionar carga de un solo documento");
        //}
        //if(iSeleccionados <= 0)
        //{
        //    SCA_MsgInformacion("Debe seleccionar elementos del listado");
        //}
    }

    protected void btnTab1_Click1(object sender, EventArgs e)
    {
        String sDocumento = String.Empty;
        // Int32 iSeleccionados = 0;
        List<String> lstValidateSDoc = new List<String>();


        foreach (GridViewRow GrvRow in GrvListado.Rows)
        {
            if ((GrvRow.FindControl("chkSeleccionar") as CheckBox).Checked)
            {
                sDocumento = GrvListado.DataKeys[GrvRow.RowIndex].Values["sNumeroDO"].ToString();
                lstValidateSDoc.Add(sDocumento);
                //  iSeleccionados = iSeleccionados + 1;
            }
        }
    }

    [WebMethod]
    public static List<BE_ServicioAdicional> ListarServicioAdicional(Int32 iIdTipoOperacion,Int32 iIdOficina,Int32 iIdSuperNegocio,Int32 iIdLineaNegocio)
    {
        BL_Servicio oBL_Servicio = new BL_Servicio();
        List<BE_ServicioAdicional> oLst_BE_ServicioAdicional = new List<BE_ServicioAdicional>();
        Int32 IdTipoOperacion = iIdTipoOperacion;
        Int32 IdOficina = iIdOficina;
        Int32 IdSuperNegocio = iIdSuperNegocio;
        Int32 IdLineaNegocio = iIdLineaNegocio;

         oLst_BE_ServicioAdicional = oBL_Servicio.ListarServiciosAdicionales(IdTipoOperacion,IdOficina,IdSuperNegocio,IdLineaNegocio);
            
        return oLst_BE_ServicioAdicional;
    }

    [WebMethod]
    public static String RegistrarSolicitudServicio()
        {
            return "";
        }

}

}