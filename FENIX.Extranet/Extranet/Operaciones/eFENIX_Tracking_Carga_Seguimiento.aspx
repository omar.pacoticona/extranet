﻿<%@ Page Language="C#" 
MasterPageFile="~/Extranet/Seguridad/eFENIX_Principal.master"
AutoEventWireup="true" 
CodeFile="eFENIX_Tracking_Carga_Seguimiento.aspx.cs" 
Inherits="Extranet_Operaciones_eFENIX_Tracking_Carga_Seguimiento"
Title="Tracking a la Carga" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="CntMaster" runat="Server">

    <table cellspacing="0" cellpadding="0" style="width: 100%;">
        
        <tr>
            <td class="form_titulo" style="width: 85%">
                TRACKING A LA CARGA
            </td> 
            <td align="right" class="form_titulo_verde" style="width: 15%" >
                <ajax:UpdatePanel ID="UdpTotales" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <asp:TextBox ID="LblTotal" Text="Total de Registros:" style="background-color: #0069ae; color: #FFFFFF" ReadOnly="true" BorderColor="#0069ae" runat="server"></asp:TextBox>
                    </ContentTemplate>
                </ajax:UpdatePanel>
            </td>           
        </tr>
        
        <tr>
            <td valign="top" colspan="2">
                <table class="form_bandeja" style="margin-top: 5px; margin-bottom: 5px; width: 100%;">
                    <tr>
                        <td style="width: 10%;">
                            Cliente
                        </td>
                        <td colspan="5">
                            <asp:TextBox ID="TxtCliente" runat="server" ReadOnly="true" BackColor="#ECE9D8" Style="text-transform: uppercase"
                                Width="515px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 10%;">
                            Manifiesto
                        </td>
                        <td style="width: 25%;">
                            <asp:TextBox ID="TxtAnoManifiesto" runat="server" MaxLength="4" Style="text-transform: uppercase"
                                Width="40px"></asp:TextBox>&nbsp;<asp:TextBox ID="TxtNumManifiesto" runat="server"
                                    MaxLength="6" Style="text-transform: uppercase" Width="80px"></asp:TextBox></td>
                        <td>
                            Situacion
                        </td>
                        <td style="width: 20%;">
                            <asp:DropDownList ID="DplSituacion" runat="server" Width="132px">
                            </asp:DropDownList>
                        </td>
                        <td style="width: 35%;" colspan="2">
                            &nbsp;
                        </td>

                    </tr>
                    <tr>
                        <td>
                            Doc. Origen
                        </td>
                        <td>
                            <asp:TextBox ID="TxtDocumento" runat="server" MaxLength="25" Style="text-transform: uppercase"
                                Width="135px"></asp:TextBox>
                        </td>
                        <td>
                            Contenedor/Chasis
                        </td>
                        <td>
                            <asp:TextBox ID="TxtContenedor" runat="server" Style="text-transform: uppercase"
                                MaxLength="30" Width="120px"></asp:TextBox>
                        </td>
                        <td>
                            <asp:ImageButton ID="ImgBtnBuscar" runat="server" ImageUrl="~/Imagenes/Formulario/btn_consultar.JPG"
                                OnClick="ImgBtnBuscar_Click" OnClientClick="return ValidarFiltro();" />
                            
                        </td>
                        
                    </tr>
                </table>
             </td>   
        </tr>
    
    

        <tr valign="top" style="height: 100%">
            <td colspan="5">
                <ajax:UpdatePanel ID="upDocumentoOrigen" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div style="overflow: auto; width: 100%; height: 100%">
                            <asp:GridView ID="GrvListado" runat="server" AutoGenerateColumns="False" DataKeyNames="iIdDocOriDet,sDocumentoOrigen,iIdDocOri"
                                SkinID="GrillaConsulta" Width="100%" OnRowDataBound="GrvListado_RowDataBound">
                                <Columns>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkSeleccionar" runat="server"/>
                                        </ItemTemplate>
                                        <HeaderStyle BackColor="#0069ae" Width="3%" />
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="sDocumentoOrigen" HeaderText="Doc. Origen" />
                                    <asp:BoundField DataField="sContenedor" HeaderText="Cnt / Chasis / Item" />
                                    <asp:BoundField DataField="sCondicion" HeaderText="Condicion" />
                                    <asp:BoundField DataField="sTipoCtn" HeaderText="Tipo Ctn" />
                                    <asp:BoundField DataField="sClienteFinal" HeaderText="Cliente" Visible="false" />
                                    <asp:BoundField DataField="dBultoManif" HeaderText="Q.Mnf" />
                                    <asp:BoundField DataField="dPesoManif" HeaderText="Peso Mnf" />
                                    <asp:BoundField DataField="dBultoRecib" HeaderText="Q.Rec" />
                                    <asp:BoundField DataField="dPesoRecib" HeaderText="Peso Rec" />
                                    <asp:BoundField DataField="sPrecintos" HeaderText="Precintos" Visible="false" />
                                    <asp:BoundField DataField="sSituacion" HeaderText="Situacion" />
                                    <asp:ButtonField ButtonType="Image" CommandName="OpenVerTracking" HeaderText="" ImageUrl="~/Script/Imagenes/IconButton/Calendar_Upd.png"
                                        Text="Ver Tracking"></asp:ButtonField>
                                </Columns>
                            </asp:GridView>
                        </div>
                    </ContentTemplate>
                    <Triggers>
                        <ajax:AsyncPostBackTrigger ControlID="GrvListado" EventName="SelectedIndexChanged" />
                        <ajax:AsyncPostBackTrigger ControlID="ImgBtnBuscar" EventName="Click" />
                        <ajax:AsyncPostBackTrigger ControlID="dnvListado" EventName="PageChanged" />
                        <ajax:AsyncPostBackTrigger ControlID="dnvListado" EventName="NextPage" />
                        <ajax:AsyncPostBackTrigger ControlID="dnvListado" EventName="PreviousPage" />
                        <ajax:AsyncPostBackTrigger ControlID="dnvListado" EventName="FirstPage" />  
                        <ajax:AsyncPostBackTrigger ControlID="dnvListado" EventName="LastPage" />     
                    </Triggers>
                </ajax:UpdatePanel>
            </td>
        </tr>
      
        <tr valign="top" style="height: 2%;">
            <td colspan="5">
               <ajax:UpdatePanel ID="upDnvListado" runat="server">
                    <ContentTemplate>
                        <FENIX:DataNavigator ID="dnvListado" runat="server" AllowCustomPaging="True" ButtonText="Ir" 
                            ForeColor="White" Font-Size="11px" BackColor="#0069ae" 
                            CurrentPage="1" CustomClientFunction="" EnableAskingAfterPage="False" EnableCustomScript="False"
                            GridViewId="GrvListado" ImagesPath="~/Imagenes/DN/" IsParentShowWindow="False"
                            MessageAskingAfterPage="" PageIndicatorFormat="Página {0} / {1}" Visible="False"
                            Width="100%" WidthButtonIr="" WidthTextBoxNumPagina="25px" />
                    </ContentTemplate>
                    <Triggers>
                        <ajax:AsyncPostBackTrigger ControlID="dnvListado" EventName="PageChanged" />
                        <ajax:AsyncPostBackTrigger ControlID="ImgBtnBuscar" EventName="Click" />
                    </Triggers>
                </ajax:UpdatePanel>
            </td>
        </tr> 
    </table>

    
    <asp:HiddenField ID="HdImbContenedor" runat="server" />

    <script language="javascript" type="text/javascript">

        var tiempo = 0;
        var seconds = 0;

        var myVar = setInterval(function () {
            clearInterval(myVar);
        }, 1000);

        var myTiempo = setTimeout(function () {
            clearInterval(myTiempo);
        }, 1);

        function myStopFunction() {
            clearInterval(myVar);
            seconds = '<%=GlobalEntity.Instancia.TiempoDuracionSesion%>' * 60;
            tiempo = '<%=GlobalEntity.Instancia.TiempoDuracionSesion%>' * 1000 * 60;

            CuentaTiempo(tiempo);
        }

        function CuentaTiempo(tiempo) {
            clearTimeout(myTiempo);
            myTiempo = setTimeout(function () {
                document.getElementById('<%=btn_cerrar.ClientID%>').click();
                //window.location = "../Seguridad/eFENIX_Login.aspx";
            }, tiempo);
        }



        function SessionExpireAlert(timeout) {
            seconds = timeout / 1000;
            document.getElementsByName("seconds").innerHTML = seconds;

            myvar = setInterval(function () {
                seconds--;
                document.getElementById("seconds").innerHTML = seconds;
            }, 1000);

            CuentaTiempo(timeout);
        };


        var goodexit = false;
        window.onbeforeunload = confirmRegisterExit;
        function confirmRegisterExit(evt) {
            if (!goodexit) {
                //return "Si vas abandonar el sistema haz click en Cerrar Sesion!!";
                document.getElementById('<%=btn_cerrar.ClientID%>').click();
            }
        }

        function ValidarFiltro() {
            var sManifiesto = document.getElementById('<%=TxtNumManifiesto.ClientID %>').value;
            var sDocOrigen = document.getElementById('<%=TxtDocumento.ClientID %>').value;
            var sContenedor = document.getElementById('<%=TxtContenedor.ClientID %>').value;

            var sCriterio = sManifiesto + sDocOrigen + sContenedor;

            if ((fc_Trim(sCriterio) == "")) {
                alert('Debe indicar al menos un criterio de búsqueda');
                return false;
            }
            else {
                return true;
            }
        }

        var objFilaAnt = null;
        var backgroundColorFilaAnt = "";

        function fc_SeleccionaFilaSimple(objFila, objrowIndex, chkID) {
            try {

                if (objFilaAnt != null) {
                    objFilaAnt.style.backgroundColor = backgroundColorFilaAnt;
                }
                objFilaAnt = objFila;
                backgroundColorFilaAnt = objFila.style.backgroundColor;
                objFila.style.backgroundColor = "#c4e4ff";
            }

            catch (e) {
                error = e.message;
            }
        }

        function HabilitarUno(chk) {
            f = document.forms[0].elements;
            for (x = 0; x < f.length; x++) {
                obj = f[x];
                if (obj.type == 'checkbox') {
                    obj.checked = false;
                }
            }
            chk.enabled;
            chk.checked = true;
        }
        
        function fc_PressKeyDO() {
            if (event.keyCode == 13) {
                document.getElementById("<%=ImgBtnBuscar.ClientID%>").click();
                return false;
            }
            else {
                return true;
            }
        }

        function AbrirPagina(sDoc, iIdDocOriDet, iIdDocOri) {
            sDocOrigen = document.getElementById('<%=TxtDocumento.ClientID%>').value;
            sCliente = document.getElementById('<%=TxtCliente.ClientID%>').value;
            sPeriodo = document.getElementById('<%=TxtAnoManifiesto.ClientID%>').value;
            sNroManif = document.getElementById('<%=TxtNumManifiesto.ClientID%>').value;
            sSitua = document.getElementById('<%=DplSituacion.ClientID%>').value;
            sContenedor = document.getElementById('<%=TxtContenedor.ClientID%>').value;

            window.location.href = "eFENIX_Tracking_Carga_Detalle.aspx?sDocumentoOrigen=" + sDoc + "&sDetalle=" + iIdDocOriDet + "&sDoc=" + sDocOrigen + "&sIdDocOri="+ iIdDocOri + "&sCliente=" + sCliente + "&sPeriodoManif=" + sPeriodo + "&sNroManif=" + sNroManif + "&sSitu=" + sSitua + "&sContenedor=" + sContenedor;
            //window.open("eFENIX_Tracking_Carga_Detalle.aspx?sDocumentoOrigen=" + sDoc + "&sDetalle=" + iDetalle);
        }

        
    </script>

</asp:Content>
<asp:Content ID="Content3" runat="server" contentplaceholderid="ContentCab">
    <span class="texto_titulo" >Tu Sesion Expirara en &nbsp; 
        <span class="texto_titulo" id="seconds"> </span>
        <span>&nbsp;seg.</span>
     </span>
     <asp:Button ID="btn_cerrar" runat="server" Text="cerrar" 
        onclick="btn_cerrar_Click"  Style="display: none;" />
</asp:Content>
