<%@ Page Language="C#" 
MasterPageFile="~/Extranet/Seguridad/eFENIX_Principal.master"
AutoEventWireup="true" 
CodeFile="eFENIX_Tracking_Carga_Detalle.aspx.cs" 
Inherits="Extranet_Operaciones_eFENIX_Tracking_Carga_Detalle"
Title="Detalle del Tracking a la Carga" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="CntMaster" runat="Server">

    <table cellspacing="0" cellpadding="0" style="width: 100%;">
        <tr>
            <td class="form_titulo">
                DETALLE DEL TRACKING A LA CARGA
            </td>
            <td align="right" class="form_titulo_verde" style="width: 15%" >
                <ajax:UpdatePanel ID="UdpTotales" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <asp:TextBox ID="LblTotal" Text="" style="background-color: #2BA143; color: #FFFFFF" ReadOnly="true" BorderColor="#2BA143" runat="server"></asp:TextBox>
                    </ContentTemplate>
                </ajax:UpdatePanel>
            </td> 
        </tr>
        <tr>
            <td colspan="2">
                <table  class="form_bandeja" style="margin-top:5px;margin-bottom:5px; width: 100%;" >
                    <tr>
                        <td style="width: 10%;">
                            Cliente
                        </td>
                        <td colspan="5" style="width: 90%;">
                            <asp:TextBox ID="TxtCliente" runat="server" ReadOnly="true" BackColor="#ECE9D8" Style="text-transform: uppercase" Width="515px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 10%;">
                            Doc. Origen
                        </td>
                        <td style="width: 25%;">
                            <asp:TextBox ID="TxtDocumento" runat="server" Style="text-transform: uppercase" Width="135px"></asp:TextBox>
                            <asp:ImageButton ID="ImgBtnItems" runat="server"  ImageAlign="AbsMiddle" ImageUrl="~/Imagenes/buscar.png" OnClick="ImgBtnItems_Click"/>
                        </td>
                        <td style="width:10%;">
                            Contenedor/Chasis
                        </td>
                        <td style="width: 20%;">
                            <ajax:UpdatePanel ID="UpdatePanel4" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <asp:DropDownList ID="DplItems" runat="server"  Width="133px" 
                                        AutoPostBack="True" OnSelectedIndexChanged="DplItems_SelectedIndexChanged"></asp:DropDownList>
                                </ContentTemplate>
                                <Triggers>
                                    <ajax:AsyncPostBackTrigger ControlID="ImgBtnItems" EventName="Click" />
                                </Triggers>
                            </ajax:UpdatePanel>
                        </td>
                        <td style="width: 25%;">
                            <asp:ImageButton ID="ImgBtnBuscar" runat="server" ImageUrl="~/Imagenes/Formulario/btn_consultar.JPG"
                                OnClick="ImgBtnBuscar_Click"/>
                         <%--   <asp:ImageButton ID="ImgBtnPdf" runat="server" ImageUrl="~/Imagenes/Formulario/btn_grabar.JPG" Style="display: none;" 
                                OnClick="ImgBtnPdf_Click"/>--%>
                        </td>                
                        <td style="width: 10%;">
                                <asp:ImageButton ID="ImgBtnBack" runat="server" ImageUrl="~/Imagenes/botones_accion/btn_salir.JPG"
                                 OnClientClick="return Retornar();" />
                        </td>                
                    </tr>
                </table>
                
                <ajax:UpdatePanel ID="UpdatePanel13" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>

                        <table class="form_alterno" id="tbTracking" style="width: 100%;">
                            <tr>
                                <td colspan="4" style="width: 400px; height: 55px; font-size: 13px">
                                    Consignatario : &nbsp; &nbsp;
                                    <asp:Label ID="LblConsignatario" runat="server" Font-Bold="True"></asp:Label>
                                </td>
                                <td colspan="5" style="width: 350px; height: 55px; font-size: 13px">
                                    Contenedor / Chasis : &nbsp; &nbsp;
                                    <asp:Label ID="LblContenedor" runat="server" Font-Bold="True"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="9">
                                    &nbsp; 
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 50px;">
                                    <table align="center" cellspacing="1" style="width: 150px; height: 250px; border: 1px double #000080;">
                                        <tr width="">
                                            <td class="fondo_cabtracking" height="40px;">
                                                <span>DIRECCIONAMIENTO</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="text-align: center">
                                                <asp:Label ID="LblManifiesto" runat="server" Font-Bold="True"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="text-align: center; height: 40px">
                                                <asp:Image ID="ImgDireccionamiento" runat="server" Visible="false" 
                                                    ImageUrl="~/Imagenes/NavVia_Btn.png" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="text-align: center">
                                                <asp:Label ID="LblTermDescarga" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="text-align: center">
                                                <asp:Label ID="LblNave" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="text-align: center">
                                                <asp:Label ID="LblViaje" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr><td></td></tr>
                                        <tr><td></td></tr>
                                        <tr><td></td></tr>
                                        <tr><td></td></tr>
                                        <tr><td></td></tr>
                                    </table>
                                </td>
                                
                                <td style="text-align: center; width: 50px;">
                                    <asp:Image ID="ImgFlecha01" runat="server" Visible="false" 
                                        ImageUrl="~/Script/Imagenes/IconButton/flecha.png" />
                                </td>
                                
                                <td style="width: 50px;">
                                    <table align="center" cellspacing="1" style="width: 150px; height: 250px; border: 1px double #000080;">
                                        <tr>
                                            <td class="fondo_cabtracking" height="40px;">
                                                <b><span>INGRESO AL ALMACEN</span></b>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="text-align: center">
                                                <asp:Label ID="LblTicketDescarga" runat="server" Font-Bold="True"></asp:Label>
                                                 <asp:TextBox ID="txtHdTicketIngreso" runat="server" Style="display: none;" Width="16px"></asp:TextBox>
                                                <asp:TextBox ID="txtHdIdMovBalIngreso" runat="server" Style="display: none;" Width="16px"></asp:TextBox>
                                                <asp:TextBox ID="txtHdIdEirIngreso" runat="server" Style="display: none;" Width="16px"></asp:TextBox>
                                                <asp:TextBox ID="TextBox1" runat="server" Style="display: none;" Width="16px"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="text-align: center; height: 40px">
                                                <asp:ImageButton ID="ImgIngresoAlmacen" runat="server" Visible="false" 
                                                    ImageUrl="~/Imagenes/Contenedor_Btn.png" ToolTip="Click para descargar Ticket de Ingreso" 
                                                    OnClick="ImgBtnBalanzaIngresoPdf_Click"/>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="text-align: center">
                                                <asp:Label ID="LblIngresoDescarga" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="text-align: center">
                                                <asp:Label ID="LblSalidaDescarga" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                         <tr>
                                            <td style="text-align: center">
                                               
                                                <asp:Label ID="LblPesoDescarga" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr><td></td></tr>
                                        <tr><td style="text-align:center"><asp:Label ID="lblEirIngreso" Visible="false" runat="server" Font-Bold="True"></asp:Label></td></tr>
                                        <tr>
                                            <td style="text-align: center">                                                 
                                                    <asp:ImageButton ID="ImgEirIngreso" runat="server" Visible="false" 
                                                    ImageUrl="~/Imagenes/contenedorazul.jpg" ToolTip="Click para descargar EIR Ingreso" 
                                                    OnClick="ImgBtnEirIngresoPdf_Click"/>
                                                                                      
                                                    <asp:ImageButton ID="btnEirIngresoImg" runat="server" Visible="true" 
                                                    ImageUrl="~/Imagenes/camara.png" ToolTip="Click para VER IMAGEN EIR Ingreso" 
                                                    OnClick="btnEirIngresoImg_Click"/>
                                            </td>
                                        </tr>
                                       
                                    </table>
                                </td>
                                
                                <td style="text-align: center; width: 50px;">
                                    <asp:Image ID="ImgFlecha02" runat="server" Visible="false" 
                                        ImageUrl="~/Script/Imagenes/IconButton/flecha.png" />
                                </td>
                                
                                <td style="width: 50px;">
                                    <table align="center" cellspacing="1" style="width: 150px; height: 250px; border: 1px double #000080;">
                                        <tr>
                                            <td class="fondo_cabtracking" height="40px;">
                                                <b><span>EMISION DE VOLANTE</span></b>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="text-align: center">
                                                <asp:Label ID="LblVolante" runat="server" Font-Bold="True"></asp:Label>
                                                <asp:TextBox ID="TxtHdIdVolante" runat="server" Style="display: none;" Width="16px"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="text-align: center; height: 40px">
                                                <asp:ImageButton ID="ImgEmisionVolante" runat="server" Visible="false" 
                                                    ImageUrl="~/Imagenes/Edit_Mediano.png" ToolTip="Click para descargar Volante" 
                                                    OnClick="ImgBtnPdf_Click"/>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="text-align: center">
                                                <asp:Label ID="LblFechaVolante" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="text-align: center">
                                                <asp:Label ID="LblAgenciaVacio" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="text-align: center">
                                                <asp:Label ID="LblAgenciaVolante" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr><td></td></tr>
                                        <tr><td></td></tr>
                                        <tr><td></td></tr>
                                        <tr><td></td></tr>
                                        <tr><td></td></tr>

                                    </table>
                                </td>
                                
                                <td style="text-align: center; width: 50px;">
                                    <asp:Image ID="ImgFlecha03" runat="server" Visible="false" ImageUrl="~/Script/Imagenes/IconButton/flecha.png" />
                                </td>
                                
                                <td style="width: 50px;">
                                    <table align="center" cellspacing="1" style="width: 150px; height: 250px; border: 1px double #000080;">
                                        <tr>
                                            <td class="fondo_cabtracking" height="40px;">
                                                <b><span>AUT. DE RETIRO</span></b>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="text-align: center">
                                                <asp:Label ID="LblRetiro" runat="server" Font-Bold="True"></asp:Label>
                                                
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="text-align: center; height: 40px">
                                                <asp:Image ID="ImgAutorizacionRetiro" runat="server" Visible="false" 
                                                    ImageUrl="~/Imagenes/ToolButton_Edit.png" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="text-align: center">
                                                <asp:Label ID="LblFechaRetiro" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="text-align: center">
                                                <asp:Label ID="LblDespachadorVacio" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="text-align: center">
                                                <asp:Label ID="LblDespachador" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr><td></td></tr>
                                        <tr><td></td></tr>
                                        <tr><td></td></tr>
                                        <tr><td></td></tr>
                                        <tr><td></td></tr>
                                    </table>
                                </td>
                                
                                <td style="text-align: center; width: 50px;">
                                    <asp:Image ID="ImgFlecha04" runat="server" Visible="false" ImageUrl="~/Script/Imagenes/IconButton/flecha.png" />
                                </td>
                                
                                <td style="width: 50px;">
                                    <table align="center" cellspacing="1" style="width: 150px; height: 250px; border: 1px double #000080;">
                                        <tr>
                                            <td class="fondo_cabtracking" height="40px;">
                                                <b><span>SALIDA DEL ALMACEN</span></b>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="text-align: center">
                                                <asp:Label ID="LblTicketDespacho" runat="server" Font-Bold="True"></asp:Label>
                                                 <asp:TextBox ID="txtHdTicketSalida" runat="server" Style="display: none;" Width="16px"></asp:TextBox>
                                                <asp:TextBox ID="txtHdIdMovBalSalida" runat="server" Style="display: none;" Width="16px"></asp:TextBox>
                                                <asp:TextBox ID="txtHdIdEirSalida" runat="server" Style="display: none;" Width="16px"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="text-align: center; height: 40px">
                                                <asp:ImageButton ID="ImgSalidaAlmacen" runat="server" Visible="false" 
                                                    ImageUrl="~/Imagenes/Auto_Btn.png" ToolTip="Click para descargar Ticket de Salida" 
                                                    OnClick="ImgBtnBalanzaSalidaPdf_Click"/>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="text-align: center">
                                                <asp:Label ID="LblIngresoDespacho" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="text-align: center">
                                                <asp:Label ID="LblSalidaDespacho" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="text-align: center">
                                                
                                                <asp:Label ID="LblPesoDespacho" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr><td></td></tr>
                                        <tr>
                                            <td style="text-align: center">
                                                <asp:Label ID="lblEirDespacho" Visible="false" runat="server" Font-Bold="True"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="text-align:center">
                                                <asp:ImageButton ID="ImgEirSalida" runat="server" Visible="false"                                                    
                                                    ImageUrl="~/Imagenes/contenedorazul.jpg" ToolTip="Click para descargar EIR Salida" 
                                                    OnClick="ImgBtnEirSalidaPdf_Click"/>
                                                       <asp:ImageButton ID="btnEirSalidaImg" runat="server" Visible="true" 
                                                    ImageUrl="~/Imagenes/camara.png" ToolTip="Click para VER IMAGEN EIR Salida" 
                                                    OnClick="btnEirSalidaImg_Click"/>
                                            </td>
                                        </tr>
                                        
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </ContentTemplate>
                    <Triggers>
                        <ajax:AsyncPostBackTrigger ControlID="ImgBtnBuscar" EventName="Click" />
                        <ajax:AsyncPostBackTrigger ControlID="DplItems" EventName="SelectedIndexChanged" />
                        <ajax:AsyncPostBackTrigger ControlID="ImgBtnItems" EventName="Click" />
                    </Triggers>
                </ajax:UpdatePanel>
            </td>
        </tr>
    </table>
   <%-- <div>
        <table style="float:left;">
               <tr><td>&nbsp;</td></tr>
              <tr><td>&nbsp;</td></tr>             
            <tr>
                <td>
                    <table align="center" cellspacing="1" style="width: 280px; height: 160px; border: 1px double #000080;" >
                        <tr style="border-bottom:1pt solid black;"><td align="center"><label style="font-weight:bold;">LEYENDA</label></td></tr>
                        <tr>
                            <td>
                                <asp:ImageButton ID="ImageButton1" runat="server" Visible="true" 
                                                    ImageUrl="~/Imagenes/Contenedor_Btn.png" disabled="true"
                                     style="vertical-align:middle"
                                                    />
                                <label>&nbsp;Descarga Ticket de ingreso</label>

                            </td>

                        </tr>
                        <tr>
                            <td>
                                <asp:ImageButton ID="ImageButton2" runat="server" Visible="true" disabled="true"
                                                    ImageUrl="~/Imagenes/Edit_Mediano.png" 
                                     style="vertical-align:middle"
                                                    />
                                <label>&nbsp;Descarga volante</label>

                            </td>

                        </tr>
                        <tr>
                            <td>
                                <asp:ImageButton ID="ImageButton3" runat="server" Visible="true" disabled="true"
                                                    ImageUrl="~/Imagenes/Auto_Btn.png" 
                                     style="vertical-align:middle"
                                                    />
                                <label>&nbsp;Descarga Ticket de Despacho</label>

                            </td>

                        </tr>
                        <tr>
                            <td>
                                <asp:ImageButton ID="ImageButton4" runat="server" Visible="true" disabled="true"
                                                    ImageUrl="~/Imagenes/contenedorazul.jpg"
                                     style="vertical-align:middle"
                                                    />
                                <label>&nbsp;Descarga EIR</label>

                            </td>

                        </tr>
                    </table>

                </td>
            </tr>
        </table>
    </div>--%>

    <script language="javascript" type="text/javascript">

        var tiempo = 0;
        var seconds = 0;

        var myVar = setInterval(function () {
            clearInterval(myVar);
        }, 1000);

        var myTiempo = setTimeout(function () {
            clearInterval(myTiempo);
        }, 1);

        function myStopFunction() {
            clearInterval(myVar);
            seconds = '<%=GlobalEntity.Instancia.TiempoDuracionSesion%>' * 60;
            tiempo = '<%=GlobalEntity.Instancia.TiempoDuracionSesion%>' * 1000 * 60;

            CuentaTiempo(tiempo);
        }

        function CuentaTiempo(tiempo) {
            clearTimeout(myTiempo);
            myTiempo = setTimeout(function () {
                document.getElementById('<%=btn_cerrar.ClientID%>').click();
                //window.location = "../Seguridad/eFENIX_Login.aspx";
            }, tiempo);
        }



        function SessionExpireAlert(timeout) {
            seconds = timeout / 1000;
            document.getElementsByName("seconds").innerHTML = seconds;

            myvar = setInterval(function () {
                seconds--;
                document.getElementById("seconds").innerHTML = seconds;
            }, 1000);

            CuentaTiempo(timeout);
        };


     //   var goodexit = false;
      //  window.onbeforeunload = confirmRegisterExit;
<%--        function confirmRegisterExit(evt) {
            if (!goodexit) {
                //return "Si vas abandonar el sistema haz click en Cerrar Sesion!!";
                document.getElementById('<%=btn_cerrar.ClientID%>').click();
            }
        }--%>

        function ValidarFiltro() {
            var sDoc = document.getElementById('<%=TxtDocumento.ClientID %>').value;

            if ((fc_Trim(sDoc) == "")) {
                alert(msgDatosObligatorios);
                return false;
            }
            else {
                return true;
            }
        }

        function fc_PressKeyDO() {
            if (event.keyCode == 13) {
                document.getElementById("<%=ImgBtnItems.ClientID%>").click();
                return false;
            }
            else {
                return true;
            }
        }
        
        function GenerarPDF() {
//            sCodigo = document.getElementById("<%=TxtHdIdVolante.ClientID%>").value;
//            sUsuario = '<%=GlobalEntity.Instancia.Usuario%>';

//            if (sCodigo == "") {
//                alert("No existe ningun Volante Despacho")
//                return false;
//            } else {
//                strUrl = "&vi_IdVolante=" + sCodigo + "&vi_Usuario=" + sUsuario;
//                url = '<%=Convert.ToString(ConfigurationManager.AppSettings["ServerReporting"])%>/pages/ReportViewer.aspx?%2fFENIX.Reporte%2fFENIX_VolanteDespacho&rs:format=pdf&rs:Command=Render&rc:Parameters=False' + strUrl;
//                window.open(url, '', 'toolbar=no,left=0,top=0,width= 800,height=700, directories=no, status=no, scrollbars=yes, resizable=yes, menubar=no');
//                return false;
            //            }
            
           
        }

        function Retornar() {
            sDoc = '<%=ViewState["Doc"]%>'
            sCliente = '<%=ViewState["Cliente"]%>';
            sPeriodo = '<%=ViewState["AnnoManif"]%>';
            sNroManif = '<%=ViewState["NroManif"]%>';
            sSitua = '<%=ViewState["Situacion"]%>';
            sContenedor = '<%=ViewState["Contenedor"]%>';

            //location.href = "eFENIX_Tracking_Carga_Seguimiento.aspx?sDoc=" + sDoc + "&sCliente=" + sCliente + "&sPeriodoManif=" + sPeriodo + "&sNroManif=" + sNroManif + "&sSitu=" + sSitua + "&sContenedor=" + sContenedor;

            window.open("eFENIX_Tracking_Carga_Seguimiento.aspx?sDoc=" + sDoc + "&sCliente=" + sCliente + "&sPeriodoManif=" + sPeriodo + "&sNroManif=" + sNroManif + "&sSitu=" + sSitua + "&sContenedor=" + sContenedor, "_self");
            return false;
        }
        function fc_VerImg(sCodigo) {
            iwidth = 870;
            iHeight = 550;
            url = "eFENIX_Tracking_Carga_Imagenes.aspx?id=" + sCodigo;
            window.open(url, 'AR', 'toolbar=no,left=0,top=0,width=' + iwidth + 'px,' + 'height=' + iHeight + 'px, directories=no, status=no, scrollbars=no, resizable=no, menubar=no');
            window.blur();
            return false;
        }
                  
    </script>

</asp:Content>
<asp:Content ID="Content3" runat="server" contentplaceholderid="ContentCab">
     <span class="texto_titulo" >Tu Sesion Expirara en &nbsp; 
        <span class="texto_titulo" id="seconds"> </span>
        <span>&nbsp;seg.</span>
     </span>
     <asp:Button ID="btn_cerrar" runat="server" Text="cerrar" 
        onclick="btn_cerrar_Click"  Style="display: none;" />
</asp:Content>

