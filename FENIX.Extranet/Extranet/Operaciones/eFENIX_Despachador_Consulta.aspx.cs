﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using QNET.Web.UI.Controls;
using FENIX.BusinessEntity;
using FENIX.BusinessLogic;
using System.Text;
using FENIX.Common;
using System.Collections.Generic;
using System.Drawing;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.IO;



public partial class Extranet_Operaciones_eFENIX_Volante_Consulta : System.Web.UI.Page
{
    #region "Evento Pagina"
    protected void Page_Load(object sender, EventArgs e)
        {
            
            txtCodigo.Attributes.Add("onkeydown", "javascript:return fc_PressKeyDO();");
            txtDNI.Attributes.Add("onkeydown", "javascript:return fc_PressKeyDO();");
            txtNombre.Attributes.Add("onkeydown", "javascript:return fc_PressKeyDO();");
            txtApellido.Attributes.Add("onkeydown", "javascript:return fc_PressKeyDO();");

            txtCodigo.Attributes.Add("onkeypress", "javascript:return SoloEnteros(event);");

            TxtCliente.Text = GlobalEntity.Instancia.NombreCliente;
            

            String tiempo = (Convert.ToInt32(GlobalEntity.Instancia.TiempoDuracionSesion.ToString()) * 1000 * 60).ToString();
            Session["Reset"] = true;

            if (!Page.IsPostBack)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "mensaje", "SessionExpireAlert(" + tiempo + ");", true);
            }
            else
                ScriptManager.RegisterStartupScript(this, GetType(), "mensaje", "myStopFunction();", true);
        }

    protected void ImgBtnBuscar_Click(object sender, ImageClickEventArgs e)
    {
        LlenarGrilla();
    }

    void LlenarGrilla()
    {
        try
        {
            String Cliente = GlobalEntity.Instancia.IdCliente.ToString();
            String Codigo = txtCodigo.Text;
            String Nombres = txtNombre.Text;
            String Apellidos = txtApellido.Text;
            String DNI = txtDNI.Text;

            BE_DespachadorList lista = new BE_DespachadorList();
            BL_DocumentoOrigen objNegocio = new BL_DocumentoOrigen();

            lista = objNegocio.ListarDespachador2(Cliente, Codigo, Nombres, Apellidos, DNI);

            GrvListado.DataSource = lista;
            GrvListado.DataBind();
            GrvListado.EmptyDataText = Resources.Resource.MsgRolNull;
            if (GrvListado.Rows.Count == 0 || GrvListado.DataSource == null)
            {
                Mensaje("No tiene Despachadores Registrados");
                GrvListado.Visible = false;
            }
            else
            {
                GrvListado.Visible = true;
            }

            dnvListado.Visible = true;
            LblTotal.Text = "Total de Registros: " + Convert.ToString(GrvListado.Rows.Count);
            UdpTotales.Update();

            new DataNavigatorParams(lista, GrvListado.Rows.Count);
        }
        catch (Exception e)
        {
            String mensaje = e.ToString();
            Mensaje(mensaje);
        }
    }

    void Mensaje(String SMS)
    {
        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "", "<script>alert('" + SMS + "');</script>", false);
    }

    protected void btn_cerrar_Click(object sender, EventArgs e)
        {
            if (GlobalEntity.Instancia.CerrarExtranet == "S")
            {
                BL_Usuario oBL_Usuario = new BL_Usuario();
                oBL_Usuario.GrabaInicioSesion(GlobalEntity.Instancia.Usuario, "0");
                Session.Abandon();
                FormsAuthentication.SignOut();
                Response.Redirect("../../eFENIX_Login.aspx");
                Response.End();
            }
        }

    protected void ImgBtnNuevo_Click(object sender, ImageClickEventArgs e)
    {
        LimpiarPopUp();
        lblRespuesta.Text = "";
        mpRegistrarDespachador.Show();
        GlobalEntity.Instancia.sAccion = "Nuevo";
    }

    void LimpiarPopUp()
    {
        txtNombrePopUp.Text = String.Empty;
        txtApellidoPopUp.Text = String.Empty;
        txtDNIPopUp.Text = String.Empty;
    }
    #endregion

    protected void btnAceptar_Click(object sender, EventArgs e)
    {
        String Msj = String.Empty;
        if (GlobalEntity.Instancia.sAccion == "Nuevo")
        {
            Msj = Grabar();
            lblRespuesta.Text = Msj;
            lblRespuesta.ForeColor = Color.Red;
            Mensaje(Msj);
        }
        else
        {
            Msj = Actualizar();
            lblRespuesta.Text = Msj;
            lblRespuesta.ForeColor = Color.Red;
            Mensaje(Msj);
        }
    }

    protected void GrvListado_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        String DNI = String.Empty;
        String Nombres = String.Empty;
        String Apellidos = String.Empty;
        String Carnet = String.Empty;
        String FechaVenFin = String.Empty;
        String FechaVenIni = String.Empty;

        if (e.CommandName == "OpenDespachador")
        {
            GlobalEntity.Instancia.sIdDespachador = GrvListado.DataKeys[Convert.ToInt32(e.CommandArgument)].Values["Codigo"].ToString();
            DNI = GrvListado.DataKeys[Convert.ToInt32(e.CommandArgument)].Values["DNI"].ToString();
            Nombres = GrvListado.DataKeys[Convert.ToInt32(e.CommandArgument)].Values["Nombres"].ToString();
            Apellidos = GrvListado.DataKeys[Convert.ToInt32(e.CommandArgument)].Values["Apellidos"].ToString();

            txtDNIPopUp.Text = DNI;
            txtNombrePopUp.Text = Nombres;
            txtApellidoPopUp.Text = Apellidos;
            lblRespuesta.Text = "";
            mpRegistrarDespachador.Show();
            GlobalEntity.Instancia.sAccion = "Actualizar";
        }
        if (e.CommandName == "EliminarDespachador")
        {
            String IdDespachador = GrvListado.DataKeys[Convert.ToInt32(e.CommandArgument)].Values["Codigo"].ToString();

            BL_DocumentoOrigen objNegocio = new BL_DocumentoOrigen();

            String Respuesta = String.Empty;

            Respuesta = objNegocio.EliminarDespachador(IdDespachador);
            Mensaje(Respuesta);
            LlenarGrilla();
        }
    }

    String Grabar()
    {
        String Nombre = txtNombrePopUp.Text;
        String Apellido = txtApellidoPopUp.Text;
        String DNI = txtDNIPopUp.Text;
        String Respuesta = String.Empty;

        if (Nombre == String.Empty)
        {
            Respuesta = "Ingrese Nombre del Despachador";
            return Respuesta;
        }
        if (Apellido == String.Empty)
        {
            Respuesta = "Ingrese Apellido del Despachador";
            return Respuesta;
        }
        if (DNI == String.Empty)
        {
            Respuesta = "Ingrese DNI del Despachador";
            return Respuesta;
        }
        BL_DocumentoOrigen objNegocio = new BL_DocumentoOrigen();

        Respuesta = objNegocio.GrabarDespachador(Nombre, Apellido, DNI, GlobalEntity.Instancia.IdCliente, GlobalEntity.Instancia.Usuario);
        GlobalEntity.Instancia.sAccion = "";
        mpRegistrarDespachador.Hide();
        LlenarGrilla();
        return Respuesta;
    }

    String Actualizar()
    {
        String Nombre = txtNombrePopUp.Text;
        String Apellido = txtApellidoPopUp.Text;
        String DNI = txtDNIPopUp.Text;
        String Respuesta = String.Empty;

        if (Nombre == String.Empty)
        {
            Respuesta = "Ingrese Nombre del Despachador";
            return Respuesta;
        }
        if (Apellido == String.Empty)
        {
            Respuesta = "Ingrese Apellido del Despachador";
            return Respuesta;
        }

        if (DNI == String.Empty)
        {
            Respuesta = "Ingrese DNI del Despachador";
            return Respuesta;
        }

        BL_DocumentoOrigen objNegocio = new BL_DocumentoOrigen();

        Respuesta = objNegocio.ACtualizarDespachador(GlobalEntity.Instancia.sIdDespachador, Nombre, Apellido, DNI, GlobalEntity.Instancia.IdCliente.ToString(), GlobalEntity.Instancia.Usuario);
        GlobalEntity.Instancia.sAccion = "";
        mpRegistrarDespachador.Hide();
        LlenarGrilla();
        return Respuesta;
    }
}