﻿<%@ Page Language="C#" 
    MasterPageFile="~/Extranet/Seguridad/eFENIX_Principal.master"
    AutoEventWireup="true" 
    CodeFile="eFenix_Solicitud_Refrendo.aspx.cs" 
    Inherits="Extranet_Operaciones_eFenix_Solicitud_Refrendo"
    EnableEventValidation="true" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
     <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
    <title></title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
   <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"/>
     <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>    
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js" type="text/javascript"></script>
   <%--<script src="../../Script/Java_Script/jsSolicitudServicios.js"></script>--%>
    <script src="../../Script/Java_Script/jsSolicitudRefrendo.js"></script>

</asp:Content>

  <asp:Content ID="Content3" ContentPlaceHolderID="ContentCab" Runat="Server">
        
        <span class="texto_titulo" >Tu Sesion Expirara en &nbsp; 
            <span class="texto_titulo" id="seconds"> </span>
            <span>&nbsp;seg.</span>
         </span>
         <asp:Button ID="btn_cerrar" runat="server" Text="cerrar" 
            onclick="btn_cerrar_Click"  Style="display: none;" />
    </asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="CntMaster" runat="Server">
    <!DOCTYPE html>
    
<html>


<body>
    <div class="form_titulo" style="width: 100%; height:23px;vertical-align: inherit">
        <p style="margin-top: 1.5px;font-size: 14px;">Solicitud de Refrendo</p>
    </div>
    <div id="tabs-container">
    <form action="submit" id="frmServicios">
  <ul class="tabs">
    <li><a href="#tabs-1">1. Registro de Solicitud</a></li>
    <li><a href="#tabs-2">2. Detalle de la Solicitud</a></li>
   <%--<li><a href="#tabs-3">3. Confirmar Solicitud</a></li>--%>
  </ul>
  <div id="tabs-1">
    
              <fieldset  style="width:100%;background-color:white;">
      <legend style="background-color:#0069AE;font-weight:700;">Datos de la Solicitud</legend>
                  <div id="divBusqueda">
                      <table class="form_bandeja" style="margin-top: 5px; margin-bottom: 5px; width: 100%;">
                          <tr>
                              <td style="width: 180px;">
                                  <asp:Label runat="server">Booking </asp:Label></td>
                              <td style="width: 250px;">
                                   <ajax:UpdatePanel ID="UpdatePanel3" runat="server">
                                <ContentTemplate>
                                  <asp:TextBox ID="txtBooking" Enabled="true" runat="server" MaxLength="20" Style="text-transform: uppercase"  autocomplete="off" 
                              Width="185px"></asp:TextBox>
                               </ContentTemplate>
                                   </ajax:UpdatePanel>

                              </td>
                              <td style="width: 250px;">
                                  <asp:Button ID="ImgBtnBuscar" runat="server" Text="Consultar" class="btn btn-primary" BackColor="#0069ae" ForeColor="#ffffff" OnClick="ImgBtnBuscar_Click" />
                              </td>
                              <td></td>
                          </tr>
                      </table>

                  </div>
    
     </fieldset>
          <br />
     <fieldset  style="width:100%;background-color:white;">
     <legend style="background-color:#0069AE;font-weight:700;">Resultado de busqueda</legend>
         <table  class="form_bandeja" style=" width: 100%; margin-top:5px;">
         <tr valign="top" style="height: 50%;width:99%;">
            <td>
           <ajax:UpdatePanel ID="UpdatePanel1" runat="server">
                     <ContentTemplate>
             <table  class="form_bandeja" style=" width: 100%; margin-top:5px;">
                  <tr>
                <td style="width:250px;">
                    <asp:Label runat="server">Exportador </asp:Label>
                </td>
                <td style="width:600px;">
                     <asp:TextBox ID="txtExportador" ReadOnly runat="server"  Style="text-transform: uppercase" Enabled="false"
                         Width="450px"></asp:TextBox>
                     <asp:TextBox ID="txtIdExportador" ReadOnly runat="server"  Style="text-transform: uppercase" Visible="false"
                         Width="50px"></asp:TextBox>
                    <asp:TextBox ID="txtIdDocOri" ReadOnly runat="server"  Style="text-transform: uppercase" Visible="false"
                         Width="50px"></asp:TextBox>
                    <asp:TextBox ID="txtIdBooking" ReadOnly runat="server"  Style="text-transform: uppercase" Visible="false"
                         Width="50px"></asp:TextBox>
                </td>
                </tr>

                <tr>
                <td style="width:250px;">
                    <asp:Label runat="server">Agencia de Aduanas </asp:Label>
                </td>
                <td style="width:550px;">
                     <asp:TextBox ID="txtAgeniaAduanas" ReadOnly runat="server"  Style="text-transform: uppercase" Enabled="false"
                         Width="450px"></asp:TextBox>
                    <asp:TextBox ID="txtIdAgenciaAduanas" ReadOnly runat="server"  Style="text-transform: uppercase" Visible="false"
                         Width="50px"></asp:TextBox>
                    <asp:TextBox ID="txtIdCondicionTransmitir" ReadOnly runat="server"  Style="text-transform: uppercase" Visible="false"
                         Width="30px"></asp:TextBox>
                </td>
                </tr>

                 <tr>
                <td style="width:250px;">
                    <asp:Label runat="server"> Nave / Viaje / Rumbo </asp:Label>
                </td>
                <td style="width:550px;">
                     <asp:TextBox ID="txtNave" ReadOnly runat="server"  Style="text-transform: uppercase" Enabled="false"
                         Width="212px"></asp:TextBox> &nbsp; &nbsp;
                    <asp:TextBox ID="txtViaje" ReadOnly runat="server"  Style="text-transform: uppercase" Enabled="false"
                         Width="100px"></asp:TextBox> &nbsp; &nbsp;
                    <asp:TextBox ID="txtRumbo" ReadOnly runat="server" Style="text-transform: uppercase" Enabled="false"
                         Width="100px"></asp:TextBox>
                    <asp:TextBox ID="txtIdNave" ReadOnly runat="server"  Style="text-transform: uppercase" Visible="false"
                         Width="50px"></asp:TextBox>
                </td>
                </tr>

                 <tr>
                <td style="width:250px;">
                    <asp:Label runat="server">Fecha Limite Solicitud/ C. Seca</asp:Label>
                </td>
                <td style="width:500px;">
                     <asp:TextBox ID="txtDtCargaSeca" ReadOnly runat="server" Style="text-transform: uppercase" Enabled="false"
                         Width="450px"></asp:TextBox>
                </td>
                </tr>

                  <tr>
                <td style="width:250px;">
                    <asp:Label runat="server">Fecha Limite Solicitud/ C. Refrigerada </asp:Label>
                </td>
                <td style="width:500px;">
                     <asp:TextBox ID="txtRefrigerante" ReadOnly runat="server"  Style="text-transform: uppercase" Enabled="false"
                         Width="450px"></asp:TextBox>
                </td>
                </tr>
                  
               </table>
                     </ContentTemplate>

                <Triggers>  
                          <asp:AsyncPostBackTrigger ControlID="ImgBtnBuscar" EventName="Click"></asp:AsyncPostBackTrigger>        
                 </Triggers>
               
              </ajax:UpdatePanel>
                 </td>

        </tr> 
         </table>

     </fieldset>
          <br />
      <div id="IdDetalle">
       <fieldset  style="width:100%;background-color:white;">
     <%--<legend style="background-color:#0069AE;font-weight:700;">Detalle del Booking</legend>--%>
           <table class="form_bandeja" style=" width: 100%;">
           <tr>
            <td class="form_titulo" style="width: 85%">
                <asp:TextBox ID="TextBox11" Text="Detalle del Booking" ReadOnly="true" 
                    style="background-color: #0069ae; border: #0069ae; color: #FFFFFF; font-family: Calibri; font-size: 13px; font-weight: bold; text-transform: uppercase;" 
                    runat="server" Width="231px" ></asp:TextBox>
            </td> 
        </tr>
         
          <tr>
           <td colspan="5">
                <ajax:UpdatePanel ID="upDetalleDocOriDet" runat="server" UpdateMode="Conditional" >
                    <ContentTemplate>                         
                        <div style="overflow: auto; width: 100%; height: 100px;">
                            <asp:GridView ID="GrvListadoDetalle" runat="server" AutoGenerateColumns="False" DataKeyNames="iIConexionCarga,iIdDocOridet"
                                SkinID="GrillaConsulta" Width="100%" OnRowDataBound="GrvListadoDetalle_RowDataBound"  >
                                <Columns>
                                     <asp:TemplateField>                                                       
                                                        <ItemTemplate>
                                                            <asp:CheckBox ID="chkSeleccionarDetalle" runat="server"/>
                                                        </ItemTemplate>
                                                        <ItemStyle Width="1%" />
                                    </asp:TemplateField>
                                     <asp:BoundField DataField="iIdDocOridet" HeaderText="Contenedor" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5px" Visible="false">                                       
                                    </asp:BoundField>
                                     <asp:BoundField DataField="sContenedor" HeaderText="Contenedor" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5px">                                       
                                    </asp:BoundField>
                                     <asp:BoundField DataField="sTipoContenedor" HeaderText="TipoContenedor" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5px">                                       
                                    </asp:BoundField>
                                     <asp:BoundField DataField="sDimension" HeaderText="Dimension" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5px">                                       
                                    </asp:BoundField>
                                     <asp:BoundField DataField="dTara" HeaderText="Tara" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5px">                                       
                                    </asp:BoundField>
                                     <asp:BoundField DataField="dPesoNeto" HeaderText="Peso" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5px">                                       
                                    </asp:BoundField>
                                     <asp:BoundField DataField="sSituacion" HeaderText="Situacion" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5px" Visible="false">                                       
                                    </asp:BoundField>                                    <asp:BoundField DataField="iIConexionCarga" HeaderText="Conexion" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5px" Visible="false">                                       
                                    </asp:BoundField>
                                    <asp:TemplateField HeaderText="Bultos (*)"  ShowHeader="False" HeaderStyle-Width="50px" ItemStyle-HorizontalAlign="Center">
                                                            <ItemTemplate>
                                                              
                                                                 <asp:TextBox ID="txtBultos" runat="server" Width="80px" Placeholder="0"   autocomplete="off"  MaxLength="8"  Text='<%# Eval("sBultos") %>' Style="text-transform: uppercase"></asp:TextBox>  
                                                           </ItemTemplate>
                                                                                                            
                                                        <HeaderStyle ForeColor="White" />                                                                                    
                                      </asp:TemplateField> 
                                     <asp:BoundField DataField="iIConexionCarga" HeaderText="Conexion" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5px" Visible="false">                                       
                                    </asp:BoundField>
                                    <asp:TemplateField HeaderText="PrecintoAduana"  ShowHeader="False" HeaderStyle-Width="50px" ItemStyle-HorizontalAlign="Center">
                                                            <ItemTemplate>
                                                              
                                                                 <asp:TextBox ID="txtPrecintoAduanas" runat="server" Width="120px"  autocomplete="off"   MaxLength="15"  Text='<%# Eval("sPrecintoAduanas") %>' Style="text-transform: uppercase"></asp:TextBox>  
                                                           </ItemTemplate>
                                                                                                            
                                                        <HeaderStyle ForeColor="White" />                                                                                    
                                      </asp:TemplateField> 
                                    <asp:TemplateField HeaderText="PrecintoLinea"  ShowHeader="False" HeaderStyle-Width="50px" ItemStyle-HorizontalAlign="Center">
                                                            <ItemTemplate>
                                                              
                                                                 <asp:TextBox ID="txtPrecintoLinea" runat="server" Width="110px"  autocomplete="off"    MaxLength="15"  Text='<%# Eval("sPrecintoLinea") %>' Style="text-transform: uppercase"></asp:TextBox>  
                                                           </ItemTemplate>
                                                                                                            
                                                        <HeaderStyle ForeColor="White" />                                                                                    
                                      </asp:TemplateField> 
                                </Columns>
                            </asp:GridView>
                        </div>
                    </ContentTemplate>                    
                </ajax:UpdatePanel>
            </td>
         </tr>
         </table>
           </fieldset>
     </div>
      <br />
     <fieldset style="width:100%;background-color:white;">         
         <table  width="100%">
             <tr>
                 <td>
                     <div style="float: right;">
                  <%--<asp:ImageButton ID="btnTab1" runat="server" autopostback="true" ImageUrl="~/Imagenes//btnNext.png"
                                 CssClass="btnNext" ToolTip="Siguiente"
                       ImageAlign="Right" OnClick="btnTab1_Click"/>--%>  
                     <asp:Button ID="btnTab1" runat="server" Text="Siguiente >" class="btn btn-primary" BackColor="#0069ae" ForeColor="#ffffff" OnClick="btnTab1_Click"  />
                         </div>
                 </td>
                  
              </tr>
         </table>

      </fieldset>     
    
  </div>
  <div id="tabs-2">
      <table width="100%">
          <tr>
              <td>
                               <div id="dvCargaSeleccionada" style="height:200px;width:99%;">
                                     <fieldset style="width: 99%; background-color: white;">
                                <legend style="background-color: #0069AE; font-weight: 700;">Adjuntar documentos</legend>
                                <table>
                                    <tr valign="top" style="height: 60%; width: 700px;">
                                        <td colspan="4">
                                            <asp:UpdatePanel ID="upTablaArchivo" runat="server" UpdateMode="Conditional">
                                                <ContentTemplate>
                                                    <asp:Panel ID="Panel5" runat="server" BackColor="White" Height="200px" ScrollBars="Vertical" Width="1150px">
                                                        <table class="table table-striped table-bordered table-hover">
                                                            <tr style="color: ivory; background-color: #0069AE">
                                                                <%--<th>id</th>--%>
                                                                <th>Documento</th>
                                                                <th>Adjuntar</th>
                                                                <th>Accion</th>
                                                                <th>Nombre Archivo</th>
                                                            </tr>
                                                            <tr>
                                                                 <asp:Label runat="server" ID="lblUplBLID" Text="0" Visible="false"></asp:Label>
                                                                
                                                                <td> DAM  (<asp:Label runat="server" ForeColor="Red" ID="lbl" Text="*"></asp:Label>)
                                                                </td>
                                                                <td>                                                                  
                                                                    <asp:FileUpload ID="FileUploadDua" runat="server" Width="320px" Enabled="true"  accept="application/pdf"/>
                                                          <%-- onchange="saveFileUpl('FileUploadDua');"--%>
                                                                </td>
                                                                <td align="center">
                                                                    <asp:ImageButton runat="server" ID="imgBtnUplDua" ImageUrl="~/Imagenes/save.png" OnClick="imgBtnUplDua_Click"  OnClientClick="return saveFileUpl('imgBtnUplDua');" />
                                                                 
                                                                    <asp:ImageButton runat="server" ID="imgBtnUplDuaupd" ImageUrl="~/Imagenes/cambiar.png" Visible="false" />
                                                                </td>
                                                                <td>
                                                                    <asp:Label runat="server" ID="lblUplDua" Text=""></asp:Label>
                                                                </td>                                                                
                                                            </tr>
                                                            <tr>
                                                             <%--   <td>  </td>--%>
                                                                    <asp:Label runat="server" ID="lblUplBookingId" Text="0" Visible="false"></asp:Label>
                                                              
                                                                <td>BOOKING</td>
                                                                <td>
                                                                    <asp:FileUpload ID="FileUploadBooking" runat="server" Width="320px" Enabled="true" accept="application/pdf"  onchange="saveFileUpl('FileUploadBooking');" />
                                                                </td>
                                                                <td align="center">
                                                                    <asp:ImageButton runat="server" ID="imgBtnUplBoking" ImageUrl="~/Imagenes/save.png"  Enabled="true" OnClick="imgBtnUplBoking_Click"  OnClientClick="return saveFileUpl('imgBtnUplBoking');" />
                                                                    <asp:ImageButton runat="server" ID="imgBtnUplBokingUpd" ImageUrl="~/Imagenes/cambiar.png"  Visible="false" />
                                                                </td>
                                                                <td>
                                                                    <asp:Label runat="server" ID="lblUplBooking" Text=" "></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                               <%-- <td> </td>--%>
                                                                    <asp:Label runat="server" ID="lblUplTicketID" Text="0" Visible="false"></asp:Label>
                                                                
                                                                <td>TICKET DE PESO</td>
                                                                <td>
                                                                    <asp:FileUpload ID="FileUploaTicket" runat="server" Width="320px" Enabled="true" accept="application/pdf"   />
                                                                </td>
                                                                <td align="center">
                                                                    <asp:ImageButton runat="server" ID="imgBtnUplTicekt" ImageUrl="~/Imagenes/save.png"  Enabled="true" OnClick="imgBtnUplTicekt_Click"  OnClientClick="return saveFileUpl('imgBtnUplTicekt');"/>
                                                                    <asp:ImageButton runat="server" ID="imgBtnUplTicektUpd" ImageUrl="~/Imagenes/cambiar.png" Visible="false" />
                                                                </td>
                                                                <td>
                                                                    <asp:Label runat="server" ID="lblUplTicket" Text=" "></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                               <%-- <td> </td>--%>
                                                                    <asp:Label runat="server" ID="lblUplPackListID" Text="0" Visible="false"></asp:Label>
                                                               
                                                                <td>PACKING LIST</td>
                                                                <td>
                                                                    <asp:FileUpload ID="FileUploadPackList" runat="server" Width="320px" Enabled="true" accept="application/pdf"  onchange="saveFileUpl('FileUploadPackList');"/>
                                                                </td>
                                                                <td align="center">
                                                                    <asp:ImageButton runat="server" ID="imgBtnUplPackList" ImageUrl="~/Imagenes/save.png" Enabled="true" OnClick="imgBtnUplPackList_Click"  OnClientClick="return saveFileUpl('imgBtnUplPackList');" />
                                                                    <asp:ImageButton runat="server" ID="imgBtnUplPackListUpd" ImageUrl="~/Imagenes/cambiar.png"  Visible="false" />
                                                                </td>
                                                                <td>
                                                                    <asp:Label runat="server" ID="lblUplPackList" Text=" "></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                               <%-- <td> </td>--%>
                                                                    <asp:Label runat="server" ID="lblUplGuiaRemID" Text="0" Visible="false"></asp:Label>
                                                               
                                                                <td>GUIA DE REMISION</td>
                                                                <td>
                                                                    <asp:FileUpload ID="FileUploadsGuiaRem" runat="server" Width="320px" Enabled="true" accept="application/pdf"  onchange="saveFileUpl('FileUploadsGuiaRem');"/>
                                                                </td>
                                                                <td align="center">
                                                                    <asp:ImageButton runat="server" ID="imgBtnUplGuiaRem" ImageUrl="~/Imagenes/save.png" Enabled="true" OnClick="imgBtnUplGuiaRem_Click"  OnClientClick="return saveFileUpl('imgBtnUplGuiaRem');"/>
                                                                    <asp:ImageButton runat="server" ID="imgBtnUplGuiaRemUpd" ImageUrl="~/Imagenes/cambiar.png"  Visible="false" />
                                                                </td>
                                                                <td>
                                                                    <asp:Label runat="server" ID="lblUplGuiaRem" Text=" "></asp:Label>
                                                                </td>
                                                            </tr>
                                                             <tr>
                                                               <%-- <td> </td>--%>
                                                                    <asp:Label runat="server" ID="lblIdTipoPago" Text="0" Visible="false"></asp:Label>
                                                               
                                                                <td>DOCUMENTO DE PAGO</td>
                                                                <td>
                                                                    <asp:FileUpload ID="FileUploadTipoPago" runat="server" Width="320px" Enabled="false" accept="application/pdf"  onchange="saveFileUpl('FileUploadTipoPago');"/>
                                                                </td>
                                                                <td align="center">
                                                                    <asp:ImageButton runat="server" ID="imgBtnUplTipoPago" ImageUrl="~/Imagenes/save.png" Enabled="true" Visible="false" OnClick="imgBtnUplTipoPago_Click"  OnClientClick="return saveFileUpl('imgBtnUplTipoPago');"/>
                                                                    <asp:ImageButton runat="server" ID="imgBtnUplTipoPagoUpd" ImageUrl="~/Imagenes/cambiar.png"  Visible="false" />
                                                                </td>
                                                                <td>
                                                                    <asp:Label runat="server" ID="lblupPago" Text=""></asp:Label>
                                                                </td>
                                                            </tr>                                                           
                                                        </table>
                                                    </asp:Panel>
                                                </ContentTemplate>    
                                                 <Triggers>  
                                                          <asp:AsyncPostBackTrigger ControlID="BtnGrabar" EventName="Click"></asp:AsyncPostBackTrigger>  
                                                      <asp:AsyncPostBackTrigger ControlID="btnTab1" EventName="Click"></asp:AsyncPostBackTrigger> 
                                                     <asp:AsyncPostBackTrigger ControlID="btnRucFac" EventName="Click"></asp:AsyncPostBackTrigger>  
                                                     
                                                  </Triggers>
                                           </asp:UpdatePanel>
                                        </td>
                                    </tr>
                                    
                                </table>
                            </fieldset>
                                 </div>                                
    
              </td>
            </tr>
          <tr>
              <td>
                  <ajax:UpdatePanel ID="UpdatePanel4" runat="server">
                     <ContentTemplate>
                  <table  class="form_bandeja" style=" width: 98%; margin-top:30px;">
                      <tr>
                          <td style="width:130px;">
                              <asp:Label runat="server">&nbsp;&nbsp;RUC a FACTURAR</asp:Label></td>
                          <th style="width:110px;">
                              <asp:TextBox ID="txtRucFac" runat="server" Style="text-transform: uppercase" Width="100px" MaxLength="11" type="number"></asp:TextBox></th>
                          <th style="width:50px;">&nbsp;
                               <asp:ImageButton ID="btnRucFac" runat="server" ImageUrl="~/Imagenes/buscar.png" OnClick="btnRucFac_Click" /></th>
                          <th style="width:350px;">
                              <asp:Label runat="server" Text="" ID="lblRucFacturar"></asp:Label></th>
                          <th></th>
                          <asp:TextBox ID="txtRucFacNoVisible" runat="server" Style="text-transform: uppercase" Width="100px" MaxLength="11" Visible="false" type="number"></asp:TextBox>
                              
                          <td>
                              <asp:Label runat="server">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;DESPACHADOR</asp:Label></td>
                          </td>
                          <th style="width:210px;">&nbsp;
                              <asp:DropDownList ID="DplDespachador" runat="server" Width="200px"  ></asp:DropDownList></th>
                          <th></th>
                      </tr>
                  </table>
                         </ContentTemplate>
                      </ajax:UpdatePanel>
              </td>
          </tr>
          <tr>
              <td>
                  <fieldset  style="width:100%;background-color:white; margin-top:10px;">
     <legend style="background-color:#0069AE;font-weight:700;">Datos de pago</legend>
         <table  class="form_bandeja" style=" width: 100%; margin-top:5px;">
         <tr valign="top" style="height: 50%;width:99%;">
            <td>
           <ajax:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
                     <ContentTemplate>
             <table  class="form_bandeja" style=" width: 100%; margin-top:5px;">
                  <tr>
                <td style="width:150px;">
                    <asp:Label ID="lblTipoPago" runat="server">Tipo de Pago </asp:Label>
                </td>
                <td style="width:200px;">
                     <asp:DropDownList ID="DplTipoPago" runat="server" Width="200px" OnSelectedIndexChanged="DplTipoPago_SelectedIndexChanged" AutoPostBack="true" ></asp:DropDownList>
                </td>
                 <td style="width:150px;">
                    <asp:Label id="lblMonto" runat="server">Monto </asp:Label>
                </td>
                <td style="width:200px;">
                    <asp:TextBox  ID="txtMonto" runat="server" Style="text-transform: uppercase"  autocomplete="off" MaxLength="10"
                                Width="100px"></asp:TextBox>
                </td>
                </tr>

                   <tr>
                <td style="width:150px;">
                    <asp:Label ID="lblFormaPago" runat="server">Forma de Pago </asp:Label>
                </td>
                <td style="width:200px;">
                     <asp:DropDownList ID="DplFormaPago" runat="server" Width="200px"></asp:DropDownList>
                </td>
                 <td style="width:150px;">
                    <asp:Label ID="lblNroOperacion" runat="server">N° Operacion </asp:Label>
                </td>
                <td style="width:200px;">
                    <asp:TextBox  ID="txtOperacion" runat="server" Style="text-transform: uppercase"  autocomplete="off" 
                                Width="100px"></asp:TextBox>
                </td>
                </tr>

                    <tr>
                <td style="width:150px;">
                    <asp:Label ID="lblBanco" runat="server">Banco Origen </asp:Label>
                </td>
                <td style="width:200px;">
                     <asp:DropDownList ID="DplBancoOrigen" runat="server" Width="200px"></asp:DropDownList>
                </td>
                 <td style="width:150px;">
                    <asp:Label id="lblMoneda" runat="server">Moneda </asp:Label>
                </td>
                <td style="width:200px;">
                     <asp:DropDownList ID="DplMoneda" runat="server" Width="110px">
                         <asp:ListItem Value="-1">[Seleccionar]</asp:ListItem>
                           <asp:ListItem Value="23">SOLES</asp:ListItem>
                             <asp:ListItem Value="24">US Dolar</asp:ListItem>
                     </asp:DropDownList>
                </td>                        
                </tr>   
                 <tr>
                     <td>
                          <asp:Label id="lblBancoDestino" runat="server">Banco Destino </asp:Label>
                     </td>
                     <td>
                         <asp:DropDownList ID="DplBancoDestino" runat="server" Width="200px"></asp:DropDownList>
                    </td>
                     
                     <td>
                         <asp:Label id="lblLiquidacion" runat="server">Nro Liquidacion</asp:Label>
                     </td>
                    <td>
                        <asp:TextBox  ID="txtIdLiquidacion" runat="server" Style="text-transform: uppercase" Enabled="false"  autocomplete="off" 
                                Width="100px"  type="number"></asp:TextBox>
                    </td>
                 </tr>
               </table>
                 <table  class="form_bandeja" style=" width: 100%; margin-top:1px;">
                  <tr>
                     <td style="width:100%;">
                         <asp:TextBox id="txtObservaciones" runat="server" TextMode="MultiLine"  Width="520px" cols="70" rows="2" placeholder="Ingrese Obervaciones"></asp:TextBox>
                    </td>
                      </tr>
                             </table>
                     </ContentTemplate>
              </ajax:UpdatePanel>
                 </td>

        </tr> 
         </table>

     </fieldset>
              </td>
          </tr>
      </table>
      <table width="100%">
          <tr>
              <td>
                  <%--<asp:ImageButton ID="btnTab2P" runat="server" ImageUrl="~/Imagenes//btnNext.png"
                                CssClass="btnNext1" ToolTip="Siguiente"
                       ImageAlign="Right"/>--%>
               <div style="float:right;">
                  <asp:Button ID="BtnGrabar"  runat="server" Text="Grabar" class="btn btn-primary" BackColor="#0069ae" ForeColor="#ffffff" OnClick="BtnGrabar_Click" />
                   
                  </div>
              </td>
             
          </tr>
      </table>
      
  </div>

    </form>
</div>

    <asp:HiddenField ID="vi_ValidacionRUC" runat="server" />
   
</body>


</html>


   <script language="javascript" type="text/javascript">
          var tiempo = 0;
        var seconds = 0;

        var myVar = setInterval(function () {
            clearInterval(myVar);
        }, 1000);

        var myTiempo = setTimeout(function () {
            clearInterval(myTiempo);
        }, 1);

        function myStopFunction() {
            clearInterval(myVar);
            seconds = '<%=GlobalEntity.Instancia.TiempoDuracionSesion%>' * 60;
            tiempo = '<%=GlobalEntity.Instancia.TiempoDuracionSesion%>' * 1000 * 60;

           CuentaTiempo(tiempo);
       }

       function CuentaTiempo(tiempo) {
           clearTimeout(myTiempo);
           myTiempo = setTimeout(function () {

               //window.location = "../Seguridad/eFENIX_Login.aspx";
           }, tiempo);
       }



       function SessionExpireAlert(timeout) {
           seconds = timeout / 1000;
           document.getElementsByName("seconds").innerHTML = seconds;

           myvar = setInterval(function () {
               seconds--;
               document.getElementById("seconds").innerHTML = seconds;
           }, 1000);

           CuentaTiempo(timeout);
       };

       function HabilitarUno(chk) {
           f = document.forms[0].elements;
           for (x = 0; x < f.length; x++) {
               obj = f[x];
               if (obj.type == 'checkbox') {
                   obj.checked = false;
               }
           }
           chk.enabled;
           chk.checked = true;
       }


   </script>

       <%--   </table>--%>

    <style type="text/css">
#reservationsList {width:500px; max-height:600px; background:#fff; overflow:auto;}    
 
/*STRIPE LIST*/    
ul.stripedList {
    margin:0;
    padding:0;
    list-style:none;
}
.stripedList li {
    display:block;
    text-decoration:none;
    color:#333;
    font-family:Arial,Helvetica,sans-serif;
    font-size:12px;
    line-height:20px;
    height:20px;
}
.stripedList li span {
    display:inline-block;
    border-right:1px solid #ccc;
    overflow:hidden;
    text-overflow:ellipsis;
    padding:0 10px;
    height:20px;
}
.stripedList .evenRow {background:#f2f2f2;}
 
.c1 {width:55px;}
.c2 {width:70px;}
.c3 {width:130px;}
.c4 {width:15px;}
.cLast {border:0 !important;}
 
        .auto-style1 {
            margin-bottom: 4px;
        }
 
    </style>

       

</asp:Content>
