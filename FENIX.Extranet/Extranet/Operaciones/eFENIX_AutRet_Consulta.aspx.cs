﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using QNET.Web.UI.Controls;
using FENIX.BusinessEntity;
using FENIX.BusinessLogic;
using System.Text;
using FENIX.Common;
using System.Collections.Generic;
using System.Drawing;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.IO;



public partial class Extranet_Operaciones_eFENIX_AutRet_Consulta : System.Web.UI.Page
{
    public int IdAutRet = 0;
    public Int32 IdAgente = 0;
    #region "Evento Pagina"
    protected void Page_Load(object sender, EventArgs e)
    {
        TxtNumAut.Attributes.Add("onkeydown", "javascript:return fc_PressKeyDO();");
        TxtDocumentoH.Attributes.Add("onkeydown", "javascript:return fc_PressKeyDO();");
        TxtVolante.Attributes.Add("onkeydown", "javascript:return fc_PressKeyDO();");
        TxtContenedor.Attributes.Add("onkeydown", "javascript:return fc_PressKeyDO();");

        TxtNumAut.Attributes.Add("onkeypress", "javascript:return SoloEnteros(event);");
        TxtVolante.Attributes.Add("onkeypress", "javascript:return SoloEnteros(event);");

        txtPlacaPop.Attributes.Add("onkeypress", "javascript:return AlphaNumericOnly(event);");
        txtDocPop.Attributes.Add("onkeypress", "javascript:return SoloEnteros(event);");
        txtIdDespachadorPopUp.Attributes.Add("onkeydown", "javascript:return fc_PressKeyDesp();");
        txtDespachadorPopUp.Attributes.Add("onkeydown", "javascript:return fc_PressKeyDesp();");

        txtDocPop.Attributes.Add("onkeydown", "javascript:return fc_PressKeyDNI();");
        txtBreChoPop.Attributes.Add("onkeydown", "javascript:return fc_PressKeyBrevete();");

        TxtCliente.Text = GlobalEntity.Instancia.NombreCliente;
        IdAgente = GlobalEntity.Instancia.IdCliente;

        String tiempo = (Convert.ToInt32(GlobalEntity.Instancia.TiempoDuracionSesion.ToString()) * 1000 * 60).ToString();
        Session["Reset"] = true;

        if (!Page.IsPostBack)
        {
            LimpiarTextBox();
            //LimpiarGrilla();
            ScriptManager.RegisterStartupScript(this, GetType(), "mensaje", "SessionExpireAlert(" + tiempo + ");", true);
        }
        else
            ScriptManager.RegisterStartupScript(this, GetType(), "mensaje", "myStopFunction();", true);
    }
    protected void ImgBtnBuscar_Click(object sender, EventArgs e)
    {
        LlenarGrilla();
    }

    void LlenarGrilla()
    {
        BE_AutorizacionRetiro oBE_AutorizacionRetiro = new BE_AutorizacionRetiro();
        BE_AutorizacionRetiroList oBE_AutorizacionRetiroList = new BE_AutorizacionRetiroList();

        oBE_AutorizacionRetiro.SCliente = "";
        oBE_AutorizacionRetiro.iIdAgencia = IdAgente;
        oBE_AutorizacionRetiro.SVolante = TxtVolante.Text;
        oBE_AutorizacionRetiro.SNumAutRet = TxtNumAut.Text;
        oBE_AutorizacionRetiro.SDocumentoO = TxtDocumentoH.Text;
        oBE_AutorizacionRetiro.SAgenciaAdu = TxtCliente.Text;
        oBE_AutorizacionRetiro.SContenedor = TxtContenedor.Text;
        oBE_AutorizacionRetiro.dtFechaDesde = Convert.ToDateTime("01/01/1999");
        oBE_AutorizacionRetiro.dtFechaHasta = DateTime.Now;

        BL_DocumentoOrigen objNegocio = new BL_DocumentoOrigen();

        oBE_AutorizacionRetiroList = objNegocio.ListarAutRetiro(oBE_AutorizacionRetiro);

        if (oBE_AutorizacionRetiroList.Count == 0 || oBE_AutorizacionRetiroList == null)
        {
            oBE_AutorizacionRetiroList = new BE_AutorizacionRetiroList();
            oBE_AutorizacionRetiroList.Add(new BE_AutorizacionRetiro());
            Mensaje("No hay registros encontrados");
        }

        ViewState["oBE_ServicioList"] = oBE_AutorizacionRetiroList;
        GrvListado.DataSource = oBE_AutorizacionRetiroList;
        GrvListado.DataBind();

        dnvListado.Visible = true;
        LblTotal.Text = "Total de Registros: " + Convert.ToString(GrvListado.Rows.Count);
        UdpTotales.Update();

        new DataNavigatorParams(oBE_AutorizacionRetiroList, GrvListado.Rows.Count);
    }

    protected void btn_cerrar_Click(object sender, EventArgs e)
    {
        if (GlobalEntity.Instancia.CerrarExtranet == "S")
        {
            BL_Usuario oBL_Usuario = new BL_Usuario();
            oBL_Usuario.GrabaInicioSesion(GlobalEntity.Instancia.Usuario, "0");
            Session.Abandon();
            FormsAuthentication.SignOut();
            Response.Redirect("../../eFENIX_Login.aspx");
            Response.End();
        }
    }
    #endregion

    #region Metodos Comunes
    void LimpiarTextBox()
    {
        //TxtAgencia.Text = String.Empty;
        TxtContenedor.Text = String.Empty;
        TxtDocumentoH.Text = String.Empty;
        TxtNumAut.Text = String.Empty;
        TxtVolante.Text = String.Empty;
    }
    #endregion

    protected void GrvListado_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        Int32 iIdAutRet = 0;
        Int32 iIdCliente = 0;
        Int32 iIdAgencia = 0;
        String DespachadorTra = String.Empty;
        GlobalEntity.Instancia.SPresencia = "1";
        iIdAutRet = Convert.ToInt32(GrvListado.DataKeys[Convert.ToInt32(e.CommandArgument)].Values["IIdAutRet"].ToString());

        if (e.CommandName == "OpenHabilitar")
        {
            BL_DocumentoOrigen objNegocio = new BL_DocumentoOrigen();
            String Permiso = objNegocio.ValidarPermisos(GlobalEntity.Instancia.IdCliente.ToString());
            if (Permiso == "1")
            {

                DespachadorTra = GrvListado.DataKeys[Convert.ToInt32(e.CommandArgument)].Values["SDespachador"].ToString();
                DateTime fecha = DateTime.Now;
                int dia = Convert.ToInt32(fecha.DayOfWeek);
                int hora = Convert.ToInt32(DateTime.Now.Hour);
                int min = Convert.ToInt32(DateTime.Now.Minute);
                String SMS = ConfigurationManager.AppSettings["MensajeAut"].ToString();
                //validar si es domingo
                if (dia == Convert.ToInt32(ConfigurationManager.AppSettings["AutDomingo"].ToString()))
                {
                    Mensaje(SMS);
                    return;
                }
                //validar si es de lunes a sabado
                else if (dia >= Convert.ToInt32(ConfigurationManager.AppSettings["AutLunes"].ToString()) &&
                    dia <= Convert.ToInt32(ConfigurationManager.AppSettings["AutSabados"].ToString()))
                {
                    //validar hora es mas de las 10 P.M 
                    if (hora > Convert.ToInt32(ConfigurationManager.AppSettings["AutHoraCierre"].ToString()))
                    {
                        Mensaje(SMS);
                        return;
                    }
                    //validar si son mas de las 10 y 1
                    else if (hora == Convert.ToInt32(ConfigurationManager.AppSettings["AutHoraCierre"].ToString()) &&
                        min > Convert.ToInt32(ConfigurationManager.AppSettings["AutMinCierre"].ToString()))
                    {
                        Mensaje(SMS);
                        return;
                    }
                }
                ConsultarAutId(iIdAutRet, DespachadorTra);
                LlenarDespachadores();
                txtIdDesPop.Text = "";
                txtIdDesPop.Text = "";
                imbDespachador.Enabled = false;
                rbtnPresenciaSi.Checked = false;
                rbtnPresenciaNo.Checked = true;
                rbtnContenedorSi.Checked = false;
                rbtnContenedorNo.Checked = true;


                mpHabilitar.Show();
                listarRegistrosApp(iIdAutRet);
            }
            else
            {
                Mensaje("Solamente pueden Habilitar Autorizaciones de Retiro las Agencias de Aduanas");
            }
        }
        BE_AutorizacionRetiro oAutorizacionRetiro = new BE_AutorizacionRetiro();

        BL_DocumentoOrigen oDocumentoOrigen = new BL_DocumentoOrigen();
        oAutorizacionRetiro = oDocumentoOrigen.ConsultarAutRetId(iIdAutRet);

        if (oAutorizacionRetiro.SFecha < DateTime.Now)
        {
            if (e.CommandName == "OpenActualizar")
            {
                Mensaje("Opción disponible solo si la Autorización de Retiro esta vigente");
            }

        }
        else
        {
            if (e.CommandName == "OpenActualizar")
            {
                BL_DocumentoOrigen objNegocio = new BL_DocumentoOrigen();
                String Permiso = objNegocio.ValidarPermisos(GlobalEntity.Instancia.IdCliente.ToString());
                GlobalEntity.Instancia.IIdAutRet = iIdAutRet;
                if (Permiso == "1")
                {
                    iIdAutRet = Convert.ToInt32(GrvListado.DataKeys[Convert.ToInt32(e.CommandArgument)].Values["IIdAutRet"].ToString());
                    DespachadorTra = GrvListado.DataKeys[Convert.ToInt32(e.CommandArgument)].Values["SDespachador"].ToString();
                    DateTime fecha = DateTime.Now;
                    int dia = Convert.ToInt32(fecha.DayOfWeek);
                    int hora = Convert.ToInt32(DateTime.Now.Hour);
                    int min = Convert.ToInt32(DateTime.Now.Minute);
                    String SMS = ConfigurationManager.AppSettings["MensajeAut"].ToString();
                    //validar si es domingo
                    if (dia == Convert.ToInt32(ConfigurationManager.AppSettings["AutDomingo"].ToString()))
                    {
                        Mensaje(SMS);
                        return;
                    }
                    //validar si es de lunes a sabado
                    else if (dia >= Convert.ToInt32(ConfigurationManager.AppSettings["AutLunes"].ToString()) &&
                        dia <= Convert.ToInt32(ConfigurationManager.AppSettings["AutSabados"].ToString()))
                    {
                        //validar hora es mas de las 10 P.M 
                        if (hora > Convert.ToInt32(ConfigurationManager.AppSettings["AutHoraCierre"].ToString()))
                        {
                            Mensaje(SMS);
                            return;
                        }
                        //validar si son mas de las 10 y 1
                        else if (hora == Convert.ToInt32(ConfigurationManager.AppSettings["AutHoraCierre"].ToString()) &&
                            min > Convert.ToInt32(ConfigurationManager.AppSettings["AutMinCierre"].ToString()))
                        {
                            Mensaje(SMS);
                            return;
                        }
                    }
                    LlenarGrillaContenedoresStatus(iIdAutRet);
                    rbtnContStatusSI.Checked = false;
                    rbtnContStatusNO.Checked = true;
                    gridContenedoresStatus.Visible = false;
                    mpActualizar.Show();
                }
                else
                {
                    Mensaje("Solamente pueden Habilitar Autorizaciones de Retiro las Agencias de Aduanas");
                }
            }
        }

        if (e.CommandName == "OpenRenovacion")
        {

            TxtIdAutRet.Text = GrvListado.DataKeys[Convert.ToInt32(e.CommandArgument)].Values["IIdAutRet"].ToString();
            TxtNumeroPO.Text = GrvListado.DataKeys[Convert.ToInt32(e.CommandArgument)].Values["SNumAutRet"].ToString();           
            TxtEstadoVigenciaPO.Text = GrvListado.DataKeys[Convert.ToInt32(e.CommandArgument)].Values["SEstado"].ToString();
            TxtSituacionPO.Text = GrvListado.DataKeys[Convert.ToInt32(e.CommandArgument)].Values["SSituacion"].ToString();

            txtFechaCreacion.Text = GrvListado.DataKeys[Convert.ToInt32(e.CommandArgument)].Values["dFechaCreacion"].ToString();
            txtFechaHabilitado.Text = GrvListado.DataKeys[Convert.ToInt32(e.CommandArgument)].Values["dFechaHabilitado"].ToString();
            TxtFechaVigenciaMaximaPop.Text = GrvListado.DataKeys[Convert.ToInt32(e.CommandArgument)].Values["dFechaMaxVigencia"].ToString();


            lblUplMedioPago.Text = "";
            FileUploadMedioPago.Enabled = true;
            btnSolicitarRenovacion.Enabled = true;

            mpRenovacion.Show();
           // UpdatePanel11.Update();
        }
            
    }

    void Mensaje(String SMS)
    {
        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "", "<script>alert('" + SMS + "');</script>", false);
    }

    void ConsultarAutId(Int32 iIdAutRet, String DespachadorTra)
    {
        BE_AutorizacionRetiro objEntidad = new BE_AutorizacionRetiro();

        BL_DocumentoOrigen objNegocio = new BL_DocumentoOrigen();
        objEntidad = objNegocio.ConsultarAutRetId(iIdAutRet);

        txtAutPop.Text = objEntidad.SNumAutRet;
        txtVigenciaPop.Text = objEntidad.SFecha.ToString().Substring(0, 10);
        txtEstadoPop.Text = objEntidad.SEstado;
        txtSituacionPop.Text = objEntidad.SSituacion;
        txtDesTraPop.Text = DespachadorTra;
        GlobalEntity.Instancia.IIdAutRet = iIdAutRet;
        IdAutRet = iIdAutRet;
        //validar si retira sin presencia
        //ValidarPresencia();
        //if (GlobalEntity.Instancia.SPresencia == "0")
        //{
        //    txtIdDesPop.Enabled = false;
        //    txtDespPop.Enabled = false;
        //    imbDespachador.Enabled = false;
        //}
        //else
        //{
        //    txtIdDesPop.Enabled = false;
        //    txtDespPop.Enabled = false;
        //    imbDespachador.Enabled = true;
        //}
        //consultar si tiene placa y chofer
        ListarPlacasChofer(iIdAutRet);
        LlenarGrillaContenedores();
    }

    protected void btnAgregar_Click(object sender, EventArgs e)
    {
        int accion = 0;// 1 busqueda por Brevete || 2 busqueda por DNI|| 0 NINGUNO
        /*Validar si el DNI existe  en la BD*/
        if (ChkEmpadronar.Checked == false)
        {
            buscarChofer();
            accion = 0;
        }
        else
        {
            accion = 1;
        }

        /*---*/
        BE_PlacaChofer objEntidad = new BE_PlacaChofer();
        String Respuesta = String.Empty;
        String DNI = String.Empty;
        String Brevete = String.Empty;
        String NomChofer = String.Empty;
        if (String.IsNullOrEmpty(txtDocPop.Text))
        {
            Respuesta = "Ingrese DNI";
            Mensaje(Respuesta);
            return;
        }

        objEntidad.SPlaca = txtPlacaPop.Text.Trim();
        objEntidad.SChofer = txtNomChoPop.Text.Trim();
        objEntidad.SDoc = txtDocPop.Text.Trim();
        objEntidad.sBrevete = txtBreChoPop.Text.Trim();
        objEntidad.SUsuario = GlobalEntity.Instancia.Usuario;
        objEntidad.SPC = "Extranet";
        objEntidad.IIdAutRet = GlobalEntity.Instancia.IIdAutRet;

        Int32 iCodigo = 0;
        String vl_sMessage = String.Empty;

        int tipo = 1;//busqueda x DNI Y BREVETE ||1 InsertarPlacaChofer
        BL_DocumentoOrigen objNegocio = new BL_DocumentoOrigen();

        String[] sResultado = objNegocio.InsertarPlacaChofer(objEntidad, tipo, accion).Split('|');

        for (int i = 0; i < sResultado.Length; i++)
        {
            if (i == 0)
            {
                iCodigo = Convert.ToInt32(sResultado[i]);
            }
            else if (i == 1)
            {
                vl_sMessage += sResultado[i];
            }

        }

        if (iCodigo < 1)
        {

            Mensaje(vl_sMessage);
        }
        else
        {
            txtPlacaPop.Text = String.Empty;
            txtNomChoPop.Text = String.Empty;
            txtDocPop.Text = String.Empty;
            txtBreChoPop.Text = String.Empty;
            ListarPlacasChofer(GlobalEntity.Instancia.IIdAutRet);
            gvListado.Visible = true;
        }
    }

    void ListarPlacasChofer(Int32 IdAutRet)
    {
        try
        {
            BL_DocumentoOrigen oBLDua = new BL_DocumentoOrigen();
            BE_PlacaChoferList objEntidadLista = new BE_PlacaChoferList();
            objEntidadLista = oBLDua.ListarPlacaChofer(IdAutRet);
            gvListado.DataSource = objEntidadLista;
            gvListado.DataBind();
            //gvListado.EmptyDataText = Resources.Resource.MsgRolNull;
            if (gvListado.DataSource == null || gvListado.Rows.Count == 0)
            {
                //Mensaje("No tiene Placas ni Choferes Registrados");
                gvListado.Visible = false;
            }
        }
        catch (Exception e)
        {
            new ResultadoInterfase(ResultadoInterfase.TipoResultado.Error, e);
        }
    }

    protected void btnQuitar_Click(object sender, EventArgs e)
    {
        Int32 vl_sCodigo = 0;
        String Usuario = String.Empty;
        String NombrePc = String.Empty;
        Usuario = GlobalEntity.Instancia.Usuario;
        NombrePc = "Extranet";
        String Ticket = String.Empty;
        int cont = 0;

        foreach (GridViewRow GrvRow in gvListado.Rows)
        {
            if ((GrvRow.FindControl("chkSeleccionar") as CheckBox).Checked)
            {
                //vl_sCodigo = vl_sCodigo + gvListado.DataKeys[GrvRow.RowIndex].Values["iIdRegistro"].ToString() + ",";
                vl_sCodigo = Convert.ToInt32(gvListado.DataKeys[GrvRow.RowIndex].Values["IIdRegistro"].ToString());
                Ticket = gvListado.DataKeys[GrvRow.RowIndex].Values["sTicket"].ToString();
                cont = cont + 1;
            }
        }
        if (cont == 0)
        {
            Mensaje("Debe Seleccionar un Registro");
            return;
        }

        if (!String.IsNullOrEmpty(Ticket))
        {
            Mensaje("No puede Quitar una Placa con Movimiento de Balanza");
            return;
        }

        if (vl_sCodigo.ToString().Trim().Length != 0 || vl_sCodigo != 0)
        {
            BL_DocumentoOrigen oBL_Dua = new BL_DocumentoOrigen();
            oBL_Dua.EliminarPlacaChofer(vl_sCodigo, Usuario, NombrePc);
            ListarPlacasChofer(GlobalEntity.Instancia.IIdAutRet);
        }
        else
        {
            Mensaje("Seleccione registro(s) a quitar");
            return;
        }
    }
    protected void rbtnContenedorNo_CheckedChanged(object sender, EventArgs e)
    {
        gridContenedores.Visible = false;
        //LimpiarGrillaCont();
    }

    protected void LimpiarGrillaCont()
    {
        BE_AutorizacionRetiroList objEntidadLista = new BE_AutorizacionRetiroList();
        BE_AutorizacionRetiro objEntidad = new BE_AutorizacionRetiro();
        objEntidadLista.Add(objEntidad);
        gridContenedores.DataSource = objEntidadLista;
        gridContenedores.DataBind();
    }

    protected void rbtnContenedorSi_CheckedChanged(object sender, EventArgs e)
    {
        //Llenar Detalle de Contenedores
        //LlenarGrillaContenedores();
        gridContenedores.Visible = true;
    }

    void LlenarGrillaContenedores()
    {
        BE_AutorizacionRetiro oBE_AutorizacionRetiro = new BE_AutorizacionRetiro();
        BE_AutorizacionRetiroList oBE_AutorizacionRetiroList = new BE_AutorizacionRetiroList();

        oBE_AutorizacionRetiro.IIdAutRet = GlobalEntity.Instancia.IIdAutRet;

        BL_DocumentoOrigen objNegocio = new BL_DocumentoOrigen();

        oBE_AutorizacionRetiroList = objNegocio.ListarAutorizacionRetiroDetPorID(oBE_AutorizacionRetiro);

        if (oBE_AutorizacionRetiroList.Count == 0 || oBE_AutorizacionRetiroList == null)
        {
            oBE_AutorizacionRetiroList = new BE_AutorizacionRetiroList();
            oBE_AutorizacionRetiroList.Add(new BE_AutorizacionRetiro());
        }

        ViewState["oBE_ServicioList"] = oBE_AutorizacionRetiroList;
        gridContenedores.DataSource = oBE_AutorizacionRetiroList;
        gridContenedores.DataBind();
    }

    void LlenarGrillaContenedoresStatus(Int32 iIdAutRet)
    {

        BL_DocumentoOrigen objNegocio = new BL_DocumentoOrigen();
        BE_AutorizacionRetiro oBE_AutorizacionRetiro = new BE_AutorizacionRetiro();
        BE_AutorizacionRetiroList oBE_AutorizacionRetiroList = new BE_AutorizacionRetiroList();

        oBE_AutorizacionRetiro.IIdAutRet = iIdAutRet;



        oBE_AutorizacionRetiroList = objNegocio.ListarAutorizacionRetiroDetStatusPorID(oBE_AutorizacionRetiro);

        if (oBE_AutorizacionRetiroList.Count == 0 || oBE_AutorizacionRetiroList == null)
        {
            oBE_AutorizacionRetiroList = new BE_AutorizacionRetiroList();
            oBE_AutorizacionRetiroList.Add(new BE_AutorizacionRetiro());
        }

        ViewState["oBE_ServicioList"] = oBE_AutorizacionRetiroList;
        gridContenedoresStatus.DataSource = oBE_AutorizacionRetiroList;
        gridContenedoresStatus.DataBind();
        //gridContenedoresStatus.Visible = true;

        foreach (GridViewRow GrvRow in gridContenedoresStatus.Rows)
        {
            if (gridContenedoresStatus.DataKeys[GrvRow.RowIndex].Values["iEstado"].ToString() == "0")
            {
                CheckBox chkSeleccion = (CheckBox)GrvRow.FindControl("chkSelContActualizar");
                chkSeleccion.Enabled = true;
                chkSeleccion.Checked = false;
            }
            else if (gridContenedoresStatus.DataKeys[GrvRow.RowIndex].Values["iEstado"].ToString() == "1")
            {
                CheckBox chkSeleccion = (CheckBox)GrvRow.FindControl("chkSelContActualizar");
                chkSeleccion.Enabled = true;
                chkSeleccion.Checked = true;
            }
            else if (gridContenedoresStatus.DataKeys[GrvRow.RowIndex].Values["iEstado"].ToString() == "2")
            {
                CheckBox chkSeleccion = (CheckBox)GrvRow.FindControl("chkSelContActualizar");
                chkSeleccion.Enabled = false;
                chkSeleccion.Checked = false;
                GrvRow.BackColor = System.Drawing.Color.Gray;
            }
        }

    }
    protected void imgbBuscar_Click(object sender, ImageClickEventArgs e)
    {
        BE_AutorizacionRetiroList objEntidadList = new BE_AutorizacionRetiroList();
        String Resp = "Seleccione un criterio de busqueda";

        String Id = txtIdDespachadorPopUp.Text;
        String desp = txtDespachadorPopUp.Text;

        //if (Id == String.Empty)
        //{
        //    if (desp == String.Empty)
        //    {
        //        Mensaje(Resp);
        //        return;
        //    }
        //}
        //else if (desp == String.Empty)
        //{
        //    if (Id == String.Empty)
        //    {
        //        Mensaje(Resp);
        //        return;
        //    }
        //}

        BL_DocumentoOrigen objNegocio = new BL_DocumentoOrigen();

        objEntidadList = objNegocio.ListarDespachador(Id, desp, GlobalEntity.Instancia.IdCliente);

        if (objEntidadList.Count == 0 || objEntidadList == null)
        {
            objEntidadList = new BE_AutorizacionRetiroList();
            objEntidadList.Add(new BE_AutorizacionRetiro());
        }

        ViewState["oBE_ServicioList"] = objEntidadList;
        grvListadoDespachador.DataSource = objEntidadList;
        grvListadoDespachador.DataBind();
    }

    void LimpiarDespachadores()
    {
        BE_AutorizacionRetiroList objEntidadLista = new BE_AutorizacionRetiroList();
        BE_AutorizacionRetiro objEntidad = new BE_AutorizacionRetiro();
        objEntidadLista.Add(objEntidad);
        grvListadoDespachador.DataSource = objEntidadLista;
        grvListadoDespachador.DataBind();
    }

    protected void btnAceptar_Click(object sender, EventArgs e)
    {
        Int32 vl_sCodigo = 0;
        String Despachador = String.Empty;
        String DniDespachador = String.Empty;

        foreach (GridViewRow GrvRow in grvListadoDespachador.Rows)
        {
            if ((GrvRow.FindControl("ChkSeleccionarDespachador") as CheckBox).Checked)
            {
                //vl_sCodigo = vl_sCodigo + gvListado.DataKeys[GrvRow.RowIndex].Values["iIdRegistro"].ToString() + ",";
                vl_sCodigo = Convert.ToInt32(grvListadoDespachador.DataKeys[GrvRow.RowIndex].Values["IIdDespachador"].ToString());
                Despachador = grvListadoDespachador.DataKeys[GrvRow.RowIndex].Values["SDespachador"].ToString();
                DniDespachador = grvListadoDespachador.DataKeys[GrvRow.RowIndex].Values["SDniDespachador"].ToString();
            }
        }
        if (vl_sCodigo != 0)
        {
            GlobalEntity.Instancia.sIdDespachador = vl_sCodigo.ToString();
            txtIdDesPop.Text = DniDespachador;
            txtDespPop.Text = Despachador;
            txtIdDespachadorPopUp.Text = "";
            txtDespachadorPopUp.Text = "";
            LimpiarDespachadores();
            mpDespachador.Hide();
        }

        else
        {
            Mensaje("Seleccione registro(s) a quitar");
            return;
        }
    }
    protected void btnHabilitar_Click(object sender, EventArgs e)
    {
        BL_DocumentoOrigen objNegocio = new BL_DocumentoOrigen();
        String Detalle = String.Empty;
        Int32 vl_sCodigo = 0;
        //if (txtEstadoPop.Text == "No Vigente")
        //{
        //    Mensaje("Autorizacion debe estar Vigente");
        //    return;
        //}
        if (rbtnContenedorNo.Checked == false && rbtnContenedorSi.Checked == false)
        {
            Mensaje("Debe seleccionar si retira un contenedor en especifico o no");
            return;
        }
        DateTime vigencia = Convert.ToDateTime(txtVigenciaPop.Text);
        int dia = 0;
        if (Convert.ToInt32(vigencia.DayOfWeek) == 6)
        {
            dia = 2;
        }
        else
        {
            dia = 1;
        }

        String Respuesta = String.Empty;
        String selCont = String.Empty;
        Respuesta = objNegocio.HabilitarAutRet(GlobalEntity.Instancia.IIdAutRet, vigencia, dia, GlobalEntity.Instancia.Usuario, "Extranet");
        String[] result = Respuesta.Split('|');
        String AutRetResp = result[0].ToString();

        if (AutRetResp == txtAutPop.Text)
        {
            if (rbtnContenedorNo.Checked)
            {
                selCont = "0";
            }
            //if (rbtnContenedorSi.Checked)
            //{
            //    selCont = "1";
            //    int cont = 0;
            //    foreach (GridViewRow GrvRow in gridContenedores.Rows)
            //    {
            //        if ((GrvRow.FindControl("chkSeleccionarContenedor") as CheckBox).Checked)
            //        {
            //            vl_sCodigo = Convert.ToInt32(gridContenedores.DataKeys[GrvRow.RowIndex].Values["IdAutRetDet"].ToString());
            //            //cont = cont + 1;
            //            if (vl_sCodigo != 0)
            //            {
            //                Detalle = objNegocio.InsContSelAutRet(GlobalEntity.Instancia.IIdAutRet, vigencia, dia, GlobalEntity.Instancia.SPresencia,
            //                    selCont, vl_sCodigo, GlobalEntity.Instancia.Usuario, "Extranet");
            //            }
            //        }
            //    }
            //    if (vl_sCodigo == 0)
            //    {
            //        Mensaje("Seleccione un registro");
            //        return;
            //    }

            //}

            List<String> Containers = new List<String>();
            List<BE_Contenedor> lstContainers = new List<BE_Contenedor>();
            foreach (GridViewRow row in gridContenedores.Rows)
            {
                if (row.RowType == DataControlRowType.DataRow)
                {
                    CheckBox chkRow = (row.Cells[0].FindControl("chkSeleccionarContenedor") as CheckBox);
                    if (chkRow.Checked)
                    {
                        //String container = (row.Cells[2].FindControl("SContenedor") as Label).Text;
                        String container = gridContenedores.Rows[row.RowIndex].Cells[3].Text;
                        Containers.Add(container);
                    }
                }
            }

            if (rbtnContenedorSi.Checked)
            {
                selCont = "1";

                foreach (GridViewRow GrvRow in gridContenedores.Rows)
                {
                    //ENVIAR SOLO CONTENEDORES SELECCIONADOS
                    if ((GrvRow.FindControl("chkSeleccionarContenedor") as CheckBox).Checked)
                    {
                        vl_sCodigo = Convert.ToInt32(gridContenedores.DataKeys[GrvRow.RowIndex].Values["IdAutRetDet"].ToString());
                        if (vl_sCodigo != 0)
                        {
                            BE_Contenedor contenedor = new BE_Contenedor();
                            contenedor.IdAutRet = GlobalEntity.Instancia.IIdAutRet;
                            contenedor.Vigencia = vigencia;
                            contenedor.Dia = dia;
                            contenedor.sPresencia = GlobalEntity.Instancia.SPresencia;
                            contenedor.selCont = Int32.Parse(selCont);
                            contenedor.IdAutRetDet = vl_sCodigo;
                            contenedor.sUsuario = GlobalEntity.Instancia.Usuario;
                            contenedor.sNombrePC = "Extranet";

                            lstContainers.Add(contenedor);
                        }
                    }

                }
                if (vl_sCodigo == 0)
                {
                    Mensaje("Seleccione un registro");
                    return;

                }
            }
            else
            {
                ////ENVIAR LISTADO CON TODOS LOS CONTENEDORES EN CASO NO SE SELECCIONE NINGUNO
                //foreach (GridViewRow GrvRow in gridContenedores.Rows)
                //{
                //    vl_sCodigo = Convert.ToInt32(gridContenedores.DataKeys[GrvRow.RowIndex].Values["IdAutRetDet"].ToString());
                //    if (vl_sCodigo != 0)
                //    {
                //        BE_Contenedor contenedor = new BE_Contenedor();
                //        contenedor.IdAutRet = GlobalEntity.Instancia.IIdAutRet;
                //        contenedor.Vigencia = vigencia;
                //        contenedor.Dia = dia;
                //        contenedor.sPresencia = GlobalEntity.Instancia.SPresencia;
                //        contenedor.selCont = Int32.Parse(selCont);
                //        contenedor.IdAutRetDet = vl_sCodigo;
                //        contenedor.sUsuario = GlobalEntity.Instancia.Usuario;
                //        contenedor.sNombrePC = "Extranet";
                //        lstContainers.Add(contenedor);
                //    }
                //}
            }
            Int32 AutRetiroId = GlobalEntity.Instancia.IIdAutRet;
            if (Containers.Count() > 0)
            {
                /*  CORREO  */
                objNegocio.InsLstContSelAutRet(lstContainers, 0);
                //  objNegocio.SendContainers(Containers, AutRetiroId);
            }
            else
            {
                objNegocio.InsLstContSelAutRet(lstContainers, AutRetiroId);
            }
        }
        Mensaje(Respuesta);
        mpHabilitar.Hide();
        LlenarGrilla();
    }

    protected void rbtnPresenciaSi_CheckedChanged(object sender, EventArgs e)
    {
        GlobalEntity.Instancia.SPresencia = "0";
        //txtIdDesPop.Enabled = true;
        //txtDespPop.Enabled = true;
        imbDespachador.Enabled = true;
        gvDespachadores.Visible = true;
        btnAgregarDesp.Enabled = true;
        btnQuitarDesp.Enabled = true;
    }

    protected void rbtnPresenciaNo_CheckedChanged(object sender, EventArgs e)
    {
        GlobalEntity.Instancia.SPresencia = "1";
        //txtIdDesPop.Enabled = false;
        //txtDespPop.Enabled = false;
        imbDespachador.Enabled = false;
        gvDespachadores.Visible = false;
        btnAgregarDesp.Enabled = false;
        btnQuitarDesp.Enabled = false;
    }

    protected void gvListado_OnRowDataBound(object sender, GridViewRowEventArgs e)
    {

        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            DataKey dataKey = this.gvListado.DataKeys[e.Row.RowIndex];
            BE_PlacaChofer oitem = (e.Row.DataItem as BE_PlacaChofer);

            if (oitem.SPlaca == null)
            {
                e.Row.Visible = false;

                return;
            }
            else
            {
                CheckBox chkSeleccion = (CheckBox)e.Row.FindControl("chkSeleccionar");
                chkSeleccion.Attributes.Add("onclick", "HabilitarUno(this);");
                e.Row.Style["cursor"] = "pointer";
                //e.Row.Attributes["onclick"] = String.Format("javascript: fc_SeleccionaFilaSimple(this)");
            }
        }

    }


    protected void grvListadoDespachador_RowDataBound(object sender, GridViewRowEventArgs e)
    {

        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            DataKey dataKey = this.grvListadoDespachador.DataKeys[e.Row.RowIndex];
            BE_AutorizacionRetiro oitem = (e.Row.DataItem as BE_AutorizacionRetiro);

            if (String.IsNullOrEmpty(oitem.SDespachador))
            {
                e.Row.Visible = false;

                return;
            }
            else
            {
                CheckBox chkSeleccion = (CheckBox)e.Row.FindControl("ChkSeleccionarDespachador");
                chkSeleccion.Attributes.Add("onclick", "HabilitarUno(this);");
                e.Row.Style["cursor"] = "pointer";
                //e.Row.Attributes["onclick"] = String.Format("javascript: fc_SeleccionaFilaSimple(this)");
            }
        }
    }

    protected void gvDespachadores_OnRowDataBound(object sender, GridViewRowEventArgs e)
    {

        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            DataKey dataKey = this.gvDespachadores.DataKeys[e.Row.RowIndex];
            BE_AutorizacionRetiro oitem = (e.Row.DataItem as BE_AutorizacionRetiro);

            if (String.IsNullOrEmpty(oitem.SDespachador))
            {
                e.Row.Visible = false;

                return;
            }
            else
            {
                CheckBox chkSeleccion = (CheckBox)e.Row.FindControl("chkSeleccionarDesp");
                chkSeleccion.Attributes.Add("onclick", "HabilitarUno(this);");
                e.Row.Style["cursor"] = "pointer";
                //e.Row.Attributes["onclick"] = String.Format("javascript: fc_SeleccionaFilaSimple(this)");
            }
        }
    }

    void LlenarDespachadores()
    {
        try
        {
            BL_DocumentoOrigen oBLDua = new BL_DocumentoOrigen();
            BE_AutorizacionRetiroList objEntidadLista = new BE_AutorizacionRetiroList();
            objEntidadLista = oBLDua.ListarAutDespachadores(GlobalEntity.Instancia.IIdAutRet);
            gvDespachadores.DataSource = objEntidadLista;
            gvDespachadores.DataBind();
            //gvDespachadores.EmptyDataText = Resources.Resource.MsgRolNull;
            if (gvDespachadores.DataSource == null || gvDespachadores.Rows.Count == 0)
            {
                //Mensaje("No tiene Placas ni Choferes Registrados");
                gvDespachadores.Visible = false;
            }
            else
            {
                gvDespachadores.Visible = true;
            }
        }
        catch (Exception e)
        {
            new ResultadoInterfase(ResultadoInterfase.TipoResultado.Error, e);
        }
    }

    protected void btnAgregarDesp_Click(object sender, EventArgs e)
    {
        BE_AutorizacionRetiro objEntidad = new BE_AutorizacionRetiro();
        String Respuesta = String.Empty;
        String DNI = String.Empty;
        String NomDesp = String.Empty;
        if (String.IsNullOrEmpty(txtIdDesPop.Text))
        {
            Respuesta = "Ingrese DNI";
            Mensaje(Respuesta);
            return;
        }

        if (String.IsNullOrEmpty(txtDespPop.Text))
        {
            Respuesta = "Ingrese Nombre del Despachador";
            Mensaje(Respuesta);
            return;
        }

        objEntidad.SDespachador = txtDespPop.Text;
        objEntidad.IIdDespachador = Convert.ToInt32(GlobalEntity.Instancia.sIdDespachador);
        objEntidad.SDniDespachador = txtIdDesPop.Text;
        objEntidad.SUsuario = GlobalEntity.Instancia.Usuario;
        objEntidad.SPC = "Extranet";
        objEntidad.IIdAutRet = GlobalEntity.Instancia.IIdAutRet;

        Int32 iCodigo = 0;
        String vl_sMessage = String.Empty;

        BL_DocumentoOrigen objNegocio = new BL_DocumentoOrigen();

        String[] sResultado = objNegocio.InsertarDespachadorAut(objEntidad).Split('|');

        for (int i = 0; i < sResultado.Length; i++)
        {
            if (i == 0)
            {
                iCodigo = Convert.ToInt32(sResultado[i]);
            }
            else if (i == 1)
            {
                vl_sMessage += sResultado[i];
            }

        }

        if (iCodigo < 1)
        {

            Mensaje(vl_sMessage);
        }
        else
        {
            txtIdDesPop.Text = String.Empty;
            txtDespPop.Text = String.Empty;
            LlenarDespachadores();
            grvListadoDespachador.Visible = true;
        }
    }

    protected void btnQuitarDesp_Click(object sender, EventArgs e)
    {
        Int32 vl_sCodigo = 0;
        String Usuario = String.Empty;
        String NombrePc = String.Empty;
        Usuario = GlobalEntity.Instancia.Usuario;
        NombrePc = "Extranet";
        int cont = 0;

        foreach (GridViewRow GrvRow in gvDespachadores.Rows)
        {
            if ((GrvRow.FindControl("chkSeleccionarDesp") as CheckBox).Checked)
            {
                //vl_sCodigo = vl_sCodigo + gvListado.DataKeys[GrvRow.RowIndex].Values["iIdRegistro"].ToString() + ",";
                vl_sCodigo = Convert.ToInt32(gvDespachadores.DataKeys[GrvRow.RowIndex].Values["IIdRegistro"].ToString());
                cont = cont + 1;
            }
        }
        if (cont == 0)
        {
            Mensaje("Debe Seleccionar un Registro");
            return;
        }
        //if (cont > 1)
        //{
        //    Mensaje("Debe Seleccionar solo un registro a la vez");
        //    return;
        //}
        if (vl_sCodigo.ToString().Trim().Length != 0 || vl_sCodigo != 0)
        {
            BL_DocumentoOrigen oBL_Despachador = new BL_DocumentoOrigen();
            oBL_Despachador.EliminarDespachador2(vl_sCodigo, Usuario, NombrePc);
            LlenarDespachadores();
        }
        else
        {
            Mensaje("Seleccione registro(s) a quitar");
            return;
        }
    }

    protected void btnCerrarPopUp_Click(object sender, EventArgs e)
    {
        LimpiarDespachadores();
        mpDespachador.Hide();
    }

    protected void btnActualizar_Click(object sender, EventArgs e)
    {
        BL_DocumentoOrigen objNegocio = new BL_DocumentoOrigen();
        String Detalle = String.Empty;
        Int32 vl_sCodigo = 0;
        String Respuesta = String.Empty;
        String selCont = String.Empty;
        List<String> Containers = new List<String>();
        List<BE_Contenedor> lstContainers = new List<BE_Contenedor>();

        if (rbtnContStatusSI.Checked)
        {
            foreach (GridViewRow row in gridContenedoresStatus.Rows)
            {
                if (row.RowType == DataControlRowType.DataRow)
                {
                    CheckBox chkRow = (row.Cells[0].FindControl("chkSelContActualizar") as CheckBox);
                    if (chkRow.Checked)
                    {
                        //String container = (row.Cells[2].FindControl("SContenedor") as Label).Text;
                        String container = gridContenedoresStatus.Rows[row.RowIndex].Cells[4].Text;
                        Containers.Add(container);
                    }
                }
            }


            selCont = "1";

            foreach (GridViewRow GrvRow in gridContenedoresStatus.Rows)
            {
                //ENVIAR SOLO CONTENEDORES SELECCIONADOS
                if ((GrvRow.FindControl("chkSelContActualizar") as CheckBox).Checked)
                {
                    vl_sCodigo = Convert.ToInt32(gridContenedoresStatus.DataKeys[GrvRow.RowIndex].Values["IdAutRetDet"].ToString());
                    if (vl_sCodigo != 0)
                    {
                        BE_Contenedor contenedor = new BE_Contenedor();
                        contenedor.IdAutRet = GlobalEntity.Instancia.IIdAutRet;
                        contenedor.Vigencia = System.DateTime.Now;
                        contenedor.Dia = 0;
                        contenedor.sPresencia = GlobalEntity.Instancia.SPresencia;
                        contenedor.selCont = Int32.Parse(selCont);
                        contenedor.IdAutRetDet = vl_sCodigo;
                        contenedor.sUsuario = GlobalEntity.Instancia.Usuario;
                        contenedor.sNombrePC = "Extranet";
                        lstContainers.Add(contenedor);
                    }
                }
            }

            if (vl_sCodigo == 0)
            {
                Mensaje("Seleccione un registro");
                return;
            }
        }
        //else
        //{
        //    //ENVIAR LISTADO CON TODOS LOS CONTENEDORES EN CASO NO SE SELECCIONE NINGUNO
        //    foreach (GridViewRow GrvRow in gridContenedoresStatus.Rows)
        //    {
        //        vl_sCodigo = Convert.ToInt32(gridContenedoresStatus.DataKeys[GrvRow.RowIndex].Values["IdAutRetDet"].ToString());
        //        //if (vl_sCodigo != 0)
        //        //{
        //        //    BE_Contenedor contenedor = new BE_Contenedor();
        //        //    contenedor.IdAutRet = GlobalEntity.Instancia.IIdAutRet;                  
        //        //    contenedor.sPresencia = GlobalEntity.Instancia.SPresencia;
        //        //    contenedor.selCont = Int32.Parse(selCont);
        //        //    contenedor.IdAutRetDet = vl_sCodigo;
        //        //    contenedor.sUsuario = GlobalEntity.Instancia.Usuario;
        //        //    contenedor.sNombrePC = "Extranet";
        //        //    lstContainers.Add(contenedor);
        //        //}
        //    }
        //}
        Int32 AutRetiroId = GlobalEntity.Instancia.IIdAutRet;
        if (Containers.Count() > 0)
        {
            /*  CORREO  */
            objNegocio.InsLstContSelAutRet(lstContainers, 0);

            Respuesta = "Registro actualizado.";
        }
        else
        {
            objNegocio.InsLstContSelAutRet(lstContainers, AutRetiroId);
            Respuesta = "Registro actualizado.";
        }
        objNegocio.UpdateSentContainers(Containers, AutRetiroId);


        Mensaje(Respuesta);
        mpActualizar.Hide();
        LlenarGrilla();
    }


    protected void rbtnContStatusSI_CheckedChanged(object sender, EventArgs e)
    {
        gridContenedoresStatus.Visible = true;
    }
    protected void rbtnContStatusNO_CheckedChanged(object sender, EventArgs e)
    {
        gridContenedoresStatus.Visible = false;
    }

    protected void btnImgBrevete_Click(object sender, EventArgs e)
    {
        String Respuesta = String.Empty;
        Int32 iCodigo = 0;
        String vl_sNombre = String.Empty;
        String vl_svalueDNI = String.Empty;
        BE_PlacaChofer objEntidad = new BE_PlacaChofer();


        if (String.IsNullOrEmpty(txtBreChoPop.Text))
        {
            Respuesta = "Ingrese Brevete";
            Mensaje(Respuesta);
            return;
        }

        objEntidad.sBrevete = txtBreChoPop.Text;
        objEntidad.SChofer = "";
        int tipo = 2;//busqueda x DNI Y BREVETE
        int accion = 1;// 1 busqueda por Brevete || 2 busqueda por DNI

        BL_DocumentoOrigen objNegocio = new BL_DocumentoOrigen();

        String[] sResultado = objNegocio.InsertarPlacaChofer(objEntidad, tipo, accion).Split('|');
        for (int i = 0; i < sResultado.Length; i++)
        {
            if (i == 0)
            {
                iCodigo = Convert.ToInt32(sResultado[i]);
            }
            else if (i == 1)
            {
                vl_sNombre = sResultado[i];
            }
            else if (i == 2)
            {
                vl_svalueDNI = sResultado[i];
            }

        }

        if (iCodigo == 0)
        {
            txtDocPop.Text = "";
            txtBreChoPop.Text = "";
            txtNomChoPop.Text = "";
            string msj = vl_sNombre;
            Mensaje(vl_sNombre);
            return;
        }

        txtDocPop.Text = vl_svalueDNI;
        txtNomChoPop.Text = vl_sNombre;

    }

    protected void btnImgChoferDNI_Click(object sender, EventArgs e)
    {
        buscarChofer();
    }
    protected void ChkEmpadronar_OnCheckedChanged(object sender, EventArgs e)
    {
        if (ChkEmpadronar.Checked == true)
        {
            txtDocPop.Text = "";
            txtBreChoPop.Text = "";
            txtNomChoPop.Text = "";
            txtNomChoPop.Enabled = true;
        }
        else
        {
            txtDocPop.Text = "";
            txtBreChoPop.Text = "";
            txtNomChoPop.Text = "";
            txtNomChoPop.Enabled = false;
        }
    }
    void buscarChofer()
    {
        String Respuesta = String.Empty;
        Int32 iCodigo = 0;
        String vl_sNombre = String.Empty;
        String vl_svalueBrebete = String.Empty;

        BE_PlacaChofer objEntidad = new BE_PlacaChofer();

        if (String.IsNullOrEmpty(txtDocPop.Text))
        {
            Respuesta = "Ingrese DNI";
            Mensaje(Respuesta);
            return;
        }

        objEntidad.SDoc = txtDocPop.Text;
        objEntidad.SChofer = "";
        int _tipo = 2;//busqueda x DNI Y BREVETE
        int _accion = 2;// 1 busqueda por Brevete || 2 busqueda por DNI

        BL_DocumentoOrigen objNegocio = new BL_DocumentoOrigen();

        String[] sResultado = objNegocio.InsertarPlacaChofer(objEntidad, _tipo, _accion).Split('|');


        for (int i = 0; i < sResultado.Length; i++)
        {
            if (i == 0)
            {
                iCodigo = Convert.ToInt32(sResultado[i]);
            }
            else if (i == 1)
            {
                vl_sNombre = sResultado[i];
            }
            else if (i == 2)
            {
                vl_svalueBrebete = sResultado[i];
            }

        }


        if (iCodigo == 0)
        {
            txtDocPop.Text = "";
            txtBreChoPop.Text = "";
            txtNomChoPop.Text = "";
            Mensaje(vl_sNombre);
            return;
        }


        txtBreChoPop.Text = vl_svalueBrebete;
        txtNomChoPop.Text = vl_sNombre;
    }

    void listarRegistrosApp(int id)
    {
        BL_DocumentoOrigen objNegocio = new BL_DocumentoOrigen();
        BE_AutorizacionRetiro oBE_AutorizacionRetiro = new BE_AutorizacionRetiro();
        BE_PlacaChoferList oBE_PlacaChoferList = new BE_PlacaChoferList();

        oBE_PlacaChoferList = objNegocio.ListarPlacaChoferApp(id, 2);


        gvListadoR.DataSource = oBE_PlacaChoferList;
        gvListadoR.DataBind();
    }

    protected void imgCerrarPopRenovac_Click(object sender, ImageClickEventArgs e)
    {
        mpRenovacion.Hide();
    }

    protected void btnSolicitarRenovacion_Click(object sender, EventArgs e)
    {

    }

    Tuple<string, int> cargarArchivo(FileUpload fileUpload, string agencia)
    {
        string ruta = ConfigurationManager.AppSettings["Are_Solicitud"] + agencia + "/";

        string filename = "";
        int estado = 0;
        string fecha = DateTime.Now.ToString("ddMMyyHmmss");
        try
        {
            if (fileUpload.HasFile)
            {
                if (fileUpload.PostedFile.ContentType == "application/pdf")
                {
                    //COMPROBAR SI EXISTE RUTA 
                    if (!File.Exists(ruta))
                    {
                        //SI NO EXISTE CREAR LA RUTA
                        DirectoryInfo directory = Directory.CreateDirectory(ruta);
                    }

                    //GUARDA INFORMACION
                    filename = Path.GetFileName(fecha + "_" + fileUpload.FileName);
                    fileUpload.SaveAs(ruta + filename);
                    estado = 1;
                }
                else
                {
                    estado = 2;
                }
            }


        }
        catch (Exception ex)
        {
            //MensajeScript("eror de upload");
            estado = 0;
        }

        return Tuple.Create(filename, estado);
    }



}


