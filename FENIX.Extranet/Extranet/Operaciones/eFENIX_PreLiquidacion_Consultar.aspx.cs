﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FENIX.Common;
using FENIX.BusinessEntity;
using FENIX.BusinessLogic;
using System.Web.Security;
using QNET.Web.UI.Controls;
using System.Configuration;
using wsExtranet.SSRSws;

public partial class Extranet_Operaciones_eFENIX_PreLiquidacion_Consultar : System.Web.UI.Page
{
    String urlPreLiquidacion = ConfigurationManager.AppSettings["urlPreLiquidacion"].ToString();
    String userSSRS = ConfigurationManager.AppSettings["userSSRS"].ToString();
    String passwordSSRS = ConfigurationManager.AppSettings["passwordSSRS"].ToString();
    String domainSSRS = ConfigurationManager.AppSettings["domainSSRS"].ToString();


    protected void Page_Load(object sender, EventArgs e)
    {
        dnvListado.BindGridView += new QNET.Web.UI.Controls.DataNavigator.BindGridViewDelegate(BindGridLista);

        ScriptManager scriptManager = ScriptManager.GetCurrent(this.Page);
        scriptManager.RegisterPostBackControl(this.GrvListado);

        String tiempo = (Convert.ToInt32(GlobalEntity.Instancia.TiempoDuracionSesion.ToString()) * 1000 * 60).ToString();
        Session["Reset"] = true;
        if (!Page.IsPostBack)
        {
            txtLiquidacion.Attributes.Add("onkeypress", "javascript:return SoloEnteros(event);");
            if (GrvListado.Rows.Count == 0)
            {
               

                BE_Cliente oBE_Cliente = new BE_Cliente();
                oBE_Cliente.sIdCliente = GlobalEntity.Instancia.IdCliente.ToString();
                lblProveedor.Text = GlobalEntity.Instancia.NombreCliente.ToString();

            }
            ScriptManager.RegisterStartupScript(this, GetType(), "mensaje", "SessionExpireAlert(" + tiempo + ");", true);
        }
        else
            ScriptManager.RegisterStartupScript(this, GetType(), "mensaje", "myStopFunction();", true);

    }

    private void SCA_MsgInformacion(string strError)
    {
        MensajeScript(strError);
    }

    private void MensajeScript(string SMS)
    {
        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "", "<script>alert('" + SMS + "');</script>", false);
    }

    protected void btn_cerrar_Click(object sender, EventArgs e)
    {
        if (GlobalEntity.Instancia.CerrarExtranet == "S")
        {
            BL_Usuario oBL_Usuario = new BL_Usuario();
            oBL_Usuario.GrabaInicioSesion(GlobalEntity.Instancia.Usuario, "0");
            Session.Abandon();
            FormsAuthentication.SignOut();
            Response.Redirect("../../eFENIX_Login.aspx");
            Response.End();
        }
    }

    protected void ImgBtnBuscar_Click(object sender, EventArgs e)
    {
        dnvListado.InvokeBind();

        if (GrvListado.Rows.Count == 0)
        {
            List<BE_AcuerdoComision> oLista_Acuerdo = new List<BE_AcuerdoComision>();
            BE_AcuerdoComision oBE_AcuerdoComision = new BE_AcuerdoComision();
            // oLista_Acuerdo.Add(oBE_AcuerdoComision);
            //GrvListado.DataSource = oLista_Acuerdo;
            // /GrvListado.DataBind();
        }
    }

   
    DataNavigatorParams BindGridLista(object sender, EventArgs e)
    {//
        BE_Liquidacion oBE_Liquidacion;
        BL_Liquidacion oBL_Liquidacion;
        BE_LiquidacionList oLista_Liquidacion = new BE_LiquidacionList();
        oBL_Liquidacion = new BL_Liquidacion();
        oBE_Liquidacion = new BE_Liquidacion();

        oBE_Liquidacion.iIdCliente = Convert.ToInt32(GlobalEntity.Instancia.IdCliente);
        //oBE_AcuerdoComision.iEstado = Convert.ToInt32(DplEstado.SelectedValue);
        oBE_Liquidacion.sFechaAlmaIni = Request.Form["DatePickername"];
        oBE_Liquidacion.sFechaAlmaFin = Request.Form["DatePickerFinname"];
        oBE_Liquidacion.sLiquidacion = txtLiquidacion.Text;
        oBE_Liquidacion.iIdTipoLiquidacion_2 = 0;
        oBE_Liquidacion.iIdLineaNegocio = 89;        
        oBE_Liquidacion.iOficina = 1;
        oBE_Liquidacion.sNroVolante = TxtVolante.Text;
        if (TxtBL.Text == "")
        {
            oBE_Liquidacion.sNumeroDo = "";
        }
        else
        oBE_Liquidacion.sNumeroDo = TxtBL.Text;

        oBE_Liquidacion.sEstado = "";
        var Inicio = Request.Form["DatePickername"];
        var Fin = Request.Form["DatePickerFinname"];

        if (Inicio == "")
            Inicio = Convert.ToString(DateTime.Today);

        if (Fin == "")
            Fin = Convert.ToString(DateTime.Today);


        if (Convert.ToDateTime(Inicio) > Convert.ToDateTime(Fin))
        {
            SCA_MsgInformacion("Fecha Fin no puede ser inferior a Fecha Inicio.");
        }
        else
        {
            GrvListado.PageSize = 1000;
            oBE_Liquidacion.NPagina = dnvListado.CurrentPage;
            oBE_Liquidacion.NRegistros = GrvListado.PageSize;

            oLista_Liquidacion = oBL_Liquidacion.Listar_Liquidacion_2(oBE_Liquidacion);

            if (oLista_Liquidacion.Count == 0 || oLista_Liquidacion == null)
            {
                SCA_MsgInformacion("No hay registros encontrados");
            }

            ViewState["oBE_AcuerdoComisionList"] = oLista_Liquidacion;

            //GrvListado.Width                             = 1055;       
            dnvListado.Visible = (oLista_Liquidacion.Count() > 0);
            //upDocumentoOrigen.Update();
        }

        //return new DataNavigatorParams(oLista_Liquidacion, oBE_Liquidacion.NtotalRegistros);
        return new DataNavigatorParams(oLista_Liquidacion, oLista_Liquidacion.Count());
    }

    protected void GrvListado_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        Int32 idPreLiquidacion = 0;
        String NombreZip = String.Empty;
        String verLiq = urlPreLiquidacion;
        String password = passwordSSRS;
        String domain = domainSSRS;
        String usuario = GlobalEntity.Instancia.Usuario;

        if (e.CommandName == "OpenPreLiquidacion")
        {
            idPreLiquidacion = Convert.ToInt32(GrvListado.DataKeys[Convert.ToInt32(e.CommandArgument)].Values["iIdLiq"].ToString());
            NombreZip = "NroLiquidacion - " + GrvListado.DataKeys[Convert.ToInt32(e.CommandArgument)].Values["iIdLiq"].ToString();

            ReportExecutionService rs = new ReportExecutionService();
            rs.Credentials = new System.Net.NetworkCredential("userReport", "Reportes2019@", "FSAFARGOLINE107");
            rs.LoadReport(verLiq, null);

            List<ParameterValue> parameters = new List<ParameterValue>();
            parameters.Add(new ParameterValue { Name = "vi_IdLiquidacion", Value = idPreLiquidacion.ToString() });
            parameters.Add(new ParameterValue { Name = "vi_Usuario", Value = usuario.ToString() });
            rs.SetExecutionParameters(parameters.ToArray(), "en-US");
            String deviceInfo = "<DeviceInfo><Toolbar>False</Toolbar></DeviceInfo>";
            String mimeType;
            String encoding;
            String[] streamId;
            Warning[] warning;
            // Byte[] result = rs.Render("PDF", deviceInfo, out mimeType, out encoding, out encoding, out warning, out streamId);
            var result = rs.Render("PDF", deviceInfo, out mimeType, out encoding, out encoding, out warning, out streamId);



            Response.ClearHeaders();
            Response.Clear();
            Response.ContentType = "application/pdf";
            Response.AddHeader("content-disposition", "attachment;filename=" + NombreZip + ".pdf");
            Response.Buffer = true;
            Response.Cache.SetCacheability(System.Web.HttpCacheability.NoCache);
            Response.BinaryWrite(result);
            Response.Flush();
            HttpContext.Current.ApplicationInstance.CompleteRequest();
            Response.End();
            Response.Close();

        }


    }

    protected void GrvListado_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {

            decimal dAfecto = 0;
            decimal.TryParse(e.Row.Cells[8].Text, out dAfecto);
            e.Row.Cells[8].Text = dAfecto.ToString("N2");

            decimal dIgv = 0;
            decimal.TryParse(e.Row.Cells[9].Text, out dIgv);
            e.Row.Cells[9].Text = dIgv.ToString("N2");

            decimal dSubTotal = 0;
            decimal.TryParse(e.Row.Cells[10].Text, out dSubTotal);
            e.Row.Cells[10].Text = dSubTotal.ToString("N2");

           

        }
    }
}