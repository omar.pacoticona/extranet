﻿<%@ Page Language="C#"
    MasterPageFile="~/Extranet/Seguridad/eFENIX_Principal.master"
    AutoEventWireup="true"
    CodeFile="eFENIX_AutRet_Consulta.aspx.cs"
    Inherits="Extranet_Operaciones_eFENIX_AutRet_Consulta"
    Title="Consulta de Autorizaciones de Retiro" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="CntMaster" runat="Server">

    <table cellspacing="0" cellpadding="0" style="width: 100%;">

        <tr>
            <td class="form_titulo" style="width: 85%">CONSULTA DE Autorizaciones de retiro
            </td>
            <td align="right" class="form_titulo_verde" style="width: 15%">
                <ajax:UpdatePanel ID="UdpTotales" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <asp:TextBox ID="LblTotal" Text="Total de Registros:" Style="background-color: #0069ae; color: #FFFFFF" ReadOnly="true" BorderColor="#0069ae" runat="server"></asp:TextBox>
                    </ContentTemplate>
                </ajax:UpdatePanel>
            </td>
        </tr>

        <tr>
            <td valign="top" colspan="2">
                <table class="form_bandeja" style="margin-top: 5px; margin-bottom: 5px; width: 100%;">
                    <tr>
                        <td style="width: 14%;">Cliente
                        </td>
                        <td colspan="5">
                            <asp:TextBox ID="TxtCliente" runat="server" ReadOnly="true" BackColor="#ECE9D8" Style="text-transform: uppercase"
                                Width="514px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 14%;">AUT. RETIRO N°</td>
                        <td style="width: 25%;">&nbsp;<asp:TextBox ID="TxtNumAut" runat="server"
                            MaxLength="10" Style="text-transform: uppercase" Width="80px"></asp:TextBox></td>
                        <td style="width: 10%;">Volante
                        </td>
                        <td style="width: 15%;">
                            <asp:TextBox ID="TxtVolante" runat="server" MaxLength="15" Style="text-transform: uppercase"
                                Width="82px"></asp:TextBox>
                        </td>
                        <td style="width: 10%;">&nbsp;</td>
                        <td style="width: 25%;">&nbsp;</td>
                    </tr>
                    <tr>
                        <td style="width: 14%">BL</td>
                        <td>
                            <asp:TextBox ID="TxtDocumentoH" runat="server" MaxLength="25" Style="text-transform: uppercase"
                                Width="135px"></asp:TextBox>
                        </td>
                        <td>Contenedor/Chasis
                        </td>
                        <td style="width: 15%">
                            <asp:TextBox ID="TxtContenedor" runat="server" Style="text-transform: uppercase"
                                MaxLength="30" Width="120px"></asp:TextBox>
                        </td>
                        <td>&nbsp;
                        </td>
                        <td>
                            <asp:ImageButton ID="ImgBtnBuscar" runat="server" ImageUrl="~/Imagenes/Formulario/btn_consultar.JPG"
                                OnClick="ImgBtnBuscar_Click" OnClientClick="return ValidarFiltro();" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>


        <tr valign="top" style="height: 100%">
            <td colspan="5">
                <ajax:UpdatePanel ID="upDocumentoOrigen" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div style="overflow: auto; width: 100%; height: 100%">
                            <asp:GridView ID="GrvListado" runat="server" AutoGenerateColumns="False" DataKeyNames="IIdAutRet,iIdCliente,iIdAgencia,SDespachador,SNumAutRet,SFechaVigencia,SSituacion,SEstado,dFechaCreacion,dFechaHabilitado,dFechaMaxVigencia"
                                SkinID="GrillaConsulta" Width="100%" OnRowCommand="GrvListado_RowCommand">
                                <Columns>
                                    <asp:BoundField DataField="IIdAutRet" HeaderText="ID" Visible="false" />
                                    <asp:BoundField DataField="SNumAutRet" HeaderText="Numero" />
                                    <asp:BoundField DataField="SFechaVigencia" HeaderText="Fecha Vigencia" />
                                    <asp:BoundField DataField="iIdCliente" HeaderText="IdCliente" Visible="false" />
                                    <asp:BoundField DataField="SCliente" HeaderText="Cliente" />
                                    <asp:BoundField DataField="SDespachador" HeaderText="Despachador" />
                                    <asp:BoundField DataField="SDocumentoO" HeaderText="Doc. Origen" />
                                    <asp:BoundField DataField="SVolante" HeaderText="Volante" />
                                    <asp:BoundField DataField="SSituacion" HeaderText="Situacion" />
                                    <asp:BoundField DataField="SEstado" HeaderText="Estado" />
                                    <asp:BoundField DataField="iIdAgencia" HeaderText="IdAgencia" Visible="false" />
                                    <asp:BoundField DataField="SAgenciaAdu" HeaderText="Agencia Aduana">
                                        <ItemStyle Width="10%" Font-Size="XX-Small" />
                                    </asp:BoundField>
                                    <asp:ButtonField ButtonType="Image" CommandName="OpenHabilitar" HeaderText="Hab." ImageUrl="~/Script/Imagenes/IconButton/AutRetRenovacion_icon.png"
                                        Text="Habilitar Autorizacion de Retiro"></asp:ButtonField>
                                    <asp:ButtonField ButtonType="Image" CommandName="OpenActualizar" HeaderText="Act." ImageUrl="~/Script/Imagenes/IconButton/Edit_Pequeno.png"
                                        Text="Actualizar Contenedores en Autorizacion de Retiro"></asp:ButtonField>
                                    <asp:ButtonField ButtonType="Image" CommandName="OpenRenovacion" HeaderText="Ren." ImageUrl="~/Script/Imagenes/IconButton/renovar.png"
                                        Text="Solicitud de Renovacion"></asp:ButtonField>
                                </Columns>
                            </asp:GridView>
                        </div>
                    </ContentTemplate>
                    <Triggers>
                        <ajax:AsyncPostBackTrigger ControlID="GrvListado" EventName="SelectedIndexChanged" />
                        <ajax:AsyncPostBackTrigger ControlID="ImgBtnBuscar" EventName="Click" />
                        <ajax:AsyncPostBackTrigger ControlID="dnvListado" EventName="PageChanged" />
                        <ajax:AsyncPostBackTrigger ControlID="dnvListado" EventName="NextPage" />
                        <ajax:AsyncPostBackTrigger ControlID="dnvListado" EventName="PreviousPage" />
                        <ajax:AsyncPostBackTrigger ControlID="dnvListado" EventName="FirstPage" />
                        <ajax:AsyncPostBackTrigger ControlID="dnvListado" EventName="LastPage" />
                    </Triggers>
                </ajax:UpdatePanel>
            </td>
        </tr>
        <tr valign="top" style="height: 6%;">
            <td colspan="5">
                <ajax:UpdatePanel ID="upDnvListado" runat="server">
                    <ContentTemplate>
                        <FENIX:DataNavigator ID="dnvListado" runat="server" AllowCustomPaging="True" ButtonText="Ir"
                            ForeColor="White" Font-Size="11px" BackColor="#0069ae"
                            CurrentPage="1" CustomClientFunction="" EnableAskingAfterPage="False" EnableCustomScript="False"
                            GridViewId="GrvListado" ImagesPath="~/Imagenes/DN/" IsParentShowWindow="False"
                            MessageAskingAfterPage="" PageIndicatorFormat="Página {0} / {1}" Visible="False"
                            Width="100%" WidthButtonIr="" WidthTextBoxNumPagina="25px" />
                    </ContentTemplate>
                    <Triggers>
                        <ajax:AsyncPostBackTrigger ControlID="dnvListado" EventName="PageChanged" />
                        <ajax:AsyncPostBackTrigger ControlID="ImgBtnBuscar" EventName="Click" />
                    </Triggers>
                </ajax:UpdatePanel>
            </td>
        </tr>
    </table>

    <asp:Panel ID="pnlHabilitar" runat="server" CssClass="PanelPopup" Width="850px" BorderWidth="1" ScrollBars="Horizontal"
        BorderColor="#004b8e">

        <asp:UpdatePanel ID="UpdatePanel1" runat="server" style="overflow: auto; width: 850px; height: 650px; padding-left: 2px;">
            <ContentTemplate>
                 <br /> <br />
                <table cellspacing="0" cellpadding="0" border="0" style="height: 60px; width: 850px; background: url( '../../Imagenes/menu/menu_titulo6.png');">
                    <tr>
                        <td align="right" style="padding-bottom: 5px; padding-top: 10px; padding-right: 15px;">
                            <asp:Button ID="btnHabilitar" runat="server" OnClick="btnHabilitar_Click" Text="Habilitar" />
                            <asp:Button ID="BtnCerrar" runat="server" Text="Cerrar" OnClientClick="return ClosePopub();" />
                        </td>
                    </tr>
                </table>
                <br />
                <table border="0" style="width: 620px; height: 50px; background-color: #EFEFF1;" class="cuerpoComision">
                    <tr>
                        <td colspan="2">Aut. Retiro:
                            <asp:TextBox ID="txtAutPop" runat="server" Enabled="false"></asp:TextBox>
                        </td>
                        <td colspan="2">Fecha Vigencia:
                            <asp:TextBox ID="txtVigenciaPop" runat="server" Enabled="false"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">Estado:
                            <asp:TextBox ID="txtEstadoPop" runat="server" Enabled="false"></asp:TextBox>
                        </td>
                        <td colspan="2">Situacion:
                            <asp:TextBox ID="txtSituacionPop" runat="server" Enabled="false"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">Despachador que Tramito 
                        </td>
                        <td colspan="2">
                            <asp:TextBox ID="txtDesTraPop" runat="server" Width="270px" Enabled="False"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">Retira con Presencia
                        </td>
                        <td align="left">
                            <asp:RadioButton ID="rbtnPresenciaSi" runat="server" Text="SI" GroupName="rbtnPresencia" OnCheckedChanged="rbtnPresenciaSi_CheckedChanged"
                                AutoPostBack="true" />
                            <asp:RadioButton ID="rbtnPresenciaNo" runat="server" Text="NO" GroupName="rbtnPresencia" OnCheckedChanged="rbtnPresenciaNo_CheckedChanged"
                                AutoPostBack="true" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3">Despachadores que pueden retirar: 
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <hr />
                        </td>
                    </tr>
                    <tr>
                        <asp:UpdatePanel ID="UpdatePanel7" runat="server">
                            <ContentTemplate>
                                <td>DNI
                                    &nbsp;
                                    <asp:TextBox ID="txtIdDesPop" runat="server" Width="70px" Enabled="false"></asp:TextBox>
                                </td>
                                <td>Nombre
                                </td>
                                <td align="left">
                                    <asp:TextBox ID="txtDespPop" runat="server" Width="200px" Enabled="false"></asp:TextBox>
                                </td>
                                <td align="left">
                                    <asp:ImageButton ID="imbDespachador" runat="server" ImageUrl="~/Script/Imagenes/IconButton/Buscar.png"
                                        OnClientClick="return OpenPopupDespachador();" Width="30px" />
                                </td>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </tr>
                    <tr>
                        <td align="right">
                            <asp:Button ID="btnAgregarDesp" runat="server" CssClass="btnAcccionComision" Text="Agregar" Width="70px" OnClick="btnAgregarDesp_Click" Enabled="False" />
                        </td>
                        <td colspan="2" align="left">
                            <asp:Button ID="btnQuitarDesp" runat="server" CssClass="btnAcccionComision" Text="Quitar" Width="70px" OnClick="btnQuitarDesp_Click" Enabled="False" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3" align="center">
                            <asp:UpdatePanel ID="UpdatePanel8" runat="server">
                                <ContentTemplate>
                                    <asp:GridView ID="gvDespachadores" runat="server" AutoGenerateColumns="False" DataKeyNames="IIdRegistro,IIdDespachador,SDniDespachador,SDespachador"
                                        SkinID="GrillaConsulta" OnRowDataBound="gvDespachadores_OnRowDataBound" Width="100%">
                                        <Columns>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="chkSeleccionarDesp" runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="IIdRegistro" HeaderText="IIdRegistro" Visible="false" />
                                            <asp:BoundField DataField="IIdDespachador" HeaderText="IdDespachador" Visible="false" />
                                            <asp:BoundField DataField="SDniDespachador" HeaderText="DNI" />
                                            <asp:BoundField DataField="SDespachador" HeaderText="Despachador" />
                                        </Columns>
                                    </asp:GridView>
                                </ContentTemplate>
                                <Triggers>
                                </Triggers>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <hr />
                        </td>
                    </tr>
                    <tr>

                        <td>Placa
                        </td>
                        <td align="left">
                            <asp:TextBox ID="txtPlacaPop" runat="server" MaxLength="6"></asp:TextBox>
                        </td>
                        <td>DNI Chofer:
                        </td>
                        <td align="left">
                            <asp:TextBox ID="txtDocPop" runat="server" Width="100px" Height="16px" MaxLength="15"></asp:TextBox>

                        </td>
                        <td>
                            <asp:ImageButton ID="btnImgChoferDNI" OnClick="btnImgChoferDNI_Click" runat="server" ImageUrl="~/Imagenes/buscar.png" />
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 250px">Nombre Chofer
                        </td>
                        <td align="left">
                            <asp:TextBox ID="txtNomChoPop" runat="server" Height="16px" Width="190px" MaxLength="80" Enabled="false"></asp:TextBox>
                        </td>
                        <td>Brevete Chofer
                        </td>
                        <td align="left">
                            <asp:TextBox ID="txtBreChoPop" runat="server" Height="16px" Width="100px" MaxLength="15"></asp:TextBox>
                        </td>
                        <td>
                            <asp:ImageButton ID="btnImgBrevete" OnClick="btnImgBrevete_Click" runat="server" ImageUrl="~/Imagenes/buscar.png" />
                        </td>
                    </tr>
                    <tr>
                        <td align="center">Empadronar Chofer
                            
                        </td>
                        <td>
                            <asp:CheckBox ID="ChkEmpadronar" runat="server" AutoPostBack="true" OnCheckedChanged="ChkEmpadronar_OnCheckedChanged" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" align="right">
                            <asp:Button ID="btnAgregar" runat="server" Text="Agregar" CssClass="btnAcccionComision" Width="85px" OnClick="btnAgregar_Click" />
                        </td>
                        <td colspan="2" align="left" style="margin-left: 40px">
                            <asp:Button ID="btnQuitar" runat="server" Text="Quitar" CssClass="btnAcccionComision" OnClick="btnQuitar_Click" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4" align="center">
                            <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                <ContentTemplate>
                                    <div style="overflow: auto; width: 650px; padding-left: 2px;">
                                        <asp:GridView ID="gvListado" Width="600px" runat="server" SkinID="GrillaConsulta"
                                             DataKeyNames="IIdRegistro,SPlaca,SDoc,sChofer,sTicket,sFSalida"   ScrollBars="Vertical" Font-Bold="false"
                                            OnRowDataBound="gvListado_OnRowDataBound">                                           
                                            <Columns>
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="chkSeleccionar" runat="server" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="IIdRegistro" HeaderText="IIdRegistro" Visible="false" />
                                                <asp:BoundField DataField="SPlaca" HeaderText="PLACA" />
                                                <asp:BoundField DataField="SDoc" HeaderText="DOCUMENTO" />
                                                <asp:BoundField DataField="sChofer" HeaderText="Chofer" />
                                                <asp:BoundField DataField="sTicket" HeaderText="Ticket Balanza" />
                                                <asp:BoundField DataField="sFSalida" HeaderText="Fec. Sal. Balanza" />
                                            </Columns>                                            
                                        </asp:GridView>
                                    </div>
                                </ContentTemplate>                          
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                    <caption>
               
                        <tr>
                  
                            <td align="center" colspan="4">
                                      <br />
                        <br />
                        <br />
                                  Historial de Registros de VALIDACION- INGRESO A FARGOLINE
                                <asp:UpdatePanel ID="UpdatePanel12" runat="server">
                                    <ContentTemplate>
                                        <div id="divChoferesR" style="overflow: auto; width: 550px;  padding-left: 2px;">
                                            <asp:GridView ID="gvListadoR" runat="server" Font-Bold="false" SkinID="GrillaConsulta" Width="500px">
                                                <Columns>
                                                    <asp:BoundField DataField="dtFechaRegistro" HeaderText="Fecha" />
                                                    <asp:BoundField DataField="SDoc" HeaderText="DNI" />
                                                    <asp:BoundField DataField="sBrevete" HeaderText="Brevete" />
                                                    <asp:BoundField DataField="SPlaca" HeaderText="Placa" />
                                                    <asp:BoundField DataField="sSituacion" HeaderText="Situacion" />
                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                        </tr>
                        <tr>
                            <td align="right" colspan="4"></td>
                        </tr>
                    </caption>
                </table>
          
                <table border="0" style="width: 825px; height: 50px; background-color: #EFEFF1" class="cuerpoComision">
                    <tr>
                        <td colspan="4">
                            <hr />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">Retirar un contenedor en especifico:
                        </td>
                        <td colspan="2">
                            <asp:Panel ID="pnl1" runat="server">
                                <asp:UpdatePanel ID="UpdatePanel6" runat="server">
                                    <ContentTemplate>
                                        <asp:RadioButton ID="rbtnContenedorSi" runat="server" Text="SI" GroupName="rbtnCont" OnCheckedChanged="rbtnContenedorSi_CheckedChanged" AutoPostBack="true" />
                                        <asp:RadioButton ID="rbtnContenedorNo" runat="server" Text="NO" GroupName="rbtnCont" OnCheckedChanged="rbtnContenedorNo_CheckedChanged" AutoPostBack="true" />&nbsp;
                                    </ContentTemplate>
                                    <Triggers>
                                    </Triggers>
                                </asp:UpdatePanel>
                            </asp:Panel>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4" align="center">
                            <asp:UpdatePanel ID="UpdatePanel5" runat="server">
                                <ContentTemplate>
                                    <div id="divContenedores" style="overflow: auto; width: 550px; height: 205px; padding-left: 2px;">
                                        <asp:GridView ID="gridContenedores" Width="500px" runat="server" AllowPaging="True" SkinID="GrillaConsulta"
                                            AutoGenerateColumns="False" DataKeyNames="IdAutRetDet,SContenedor"
                                            Font-Bold="false" Visible="False" PageSize="9999">
                                            <%--DataKeyNames="iIdRegistro,sPlaca,sChofer" AutoGenerateColumns="false"  OnRowDataBound="gridContenedores_OnRowDataBound" --%>
                                            <Columns>
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="chkSeleccionarContenedor" runat="server" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="IdAutRetDet" HeaderText="Código" Visible="false">
                                                    <ItemStyle Width="5%" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="SChasis" HeaderText="Chasis"></asp:BoundField>
                                                <asp:BoundField DataField="SContenedor" HeaderText="Contenedor"></asp:BoundField>
                                                <%--<asp:BoundField DataField="SCantAte" HeaderText="Cant. Atendida">
                                            </asp:BoundField>
                                            <asp:BoundField DataField="SPesoAte" HeaderText="Peso Atendido">
                                            </asp:BoundField>
                                            <asp:BoundField DataField="SVolAte" HeaderText="Vol. Atendido">
                                            </asp:BoundField>--%>
                                            </Columns>
                                            <PagerSettings Visible="False" />
                                        </asp:GridView>
                                    </div>
                                </ContentTemplate>
                                <Triggers>
                                </Triggers>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                </table>
            </ContentTemplate>
            <Triggers>
            </Triggers>
        </asp:UpdatePanel>
    </asp:Panel>
    <ajaxToolkit:ModalPopupExtender ID="mpHabilitar" runat="server"
        CancelControlID="btnClosePopub" Enabled="True" PopupControlID="pnlHabilitar"
        TargetControlID="btnOpenPopub">
    </ajaxToolkit:ModalPopupExtender>
    <asp:Button ID="btnOpenPopub" Style="display: none;" runat="server" Text="[Open Proxy]" />
    <asp:Button ID="btnClosePopub" Style="display: none;" runat="server" Text="[Close Proxy]" />


    <asp:Panel ID="pnlDespachador" runat="server" CssClass="PanelPopup" Width="530px" BorderWidth="1"
        BorderColor="#004b8e">
        <asp:UpdatePanel ID="UpdatePanel3" runat="server">
            <ContentTemplate>
                <table cellspacing="0" cellpadding="0" border="0" style="height: 60px; width: 520px; background: url( '../../Imagenes/menu/menu_titulo6.png');">
                    <tr>
                        <td align="right" style="padding-bottom: 5px; padding-top: 10px; padding-right: 15px;">
                            <asp:Button ID="btnAceptar" runat="server" Text="Aceptar" CssClass="btnAcccionComision"
                                Width="70px" OnClick="btnAceptar_Click" />
                            <asp:Button ID="btnCerrarPopUp" runat="server" Text="Cerrar" CssClass="btnAcccionComision"
                                Width="70px" OnClick="btnCerrarPopUp_Click" />
                            <%--OnClientClick="return ClosePopubDespachador();"--%>
                        </td>
                    </tr>
                </table>
                <table border="0" cellpadding="2" cellspacing="1" style="width: 500px; height: 35px; margin-top: 5px; margin-left: 5px; padding: 5px; background-color: #EFEFF1"
                    class="cuerpoComision">
                    <tr>
                        <td>DNI
                            &nbsp;
                            <asp:TextBox ID="txtIdDespachadorPopUp" runat="server" Width="100px" MaxLength="10" SkinID="txtNormal" Style="text-transform: uppercase"></asp:TextBox>
                            &nbsp;
                            Nombre
                            &nbsp;
                            <asp:TextBox ID="txtDespachadorPopUp" runat="server" Width="200px" MaxLength="50" SkinID="txtNormal" Style="text-transform: uppercase"></asp:TextBox>
                            <asp:ImageButton ID="imgbBuscar" runat="server" ImageUrl="~/Script/Imagenes/IconButton/Buscar.png" OnClick="imgbBuscar_Click" />
                        </td>
                    </tr>
                </table>
                <table border="0" cellpadding="2" cellspacing="1" style="width: 480px; height: 290px; margin-top: 5px; margin-left: 5px; margin-right: 5px; margin-bottom: 5px; padding: 5px; background-color: #FFFFFF"
                    class="form_bandeja">
                    <tr style="vertical-align: top;">
                        <td colspan="3">
                            <div style="overflow: auto; width: 480px; height: 290px; padding-left: 1px; position: absolute;">
                                <ajax:UpdatePanel ID="UpdatePanel4" runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <asp:GridView ID="grvListadoDespachador" Width="480px" runat="server"
                                            AllowPaging="true" AllowSorting="true" SkinID="GrillaConsulta"
                                            AutoGenerateColumns="False" Font-Bold="false" DataKeyNames="IIdDespachador,SDniDespachador,SDespachador"
                                            OnRowDataBound="grvListadoDespachador_RowDataBound">
                                            <%--  DataKeyNames="iIdValor,sDescripcion,sDescripcion1,sDescripcion2,sDescripcion3,sDescripcion4,sDescripcion5,sDescripcion6" 
                            " 
                            OnSorting="GrvListado_Sorting" --%>
                                            <Columns>
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="ChkSeleccionarDespachador" runat="server" />
                                                    </ItemTemplate>
                                                    <ItemStyle Width="5%" />
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="IIdDespachador" HeaderText="Código">
                                                    <ItemStyle Width="10px" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="SDniDespachador" HeaderText="DNI"></asp:BoundField>
                                                <asp:BoundField DataField="SDespachador" HeaderText="Despachador"></asp:BoundField>
                                            </Columns>
                                            <PagerSettings Visible="False" />
                                        </asp:GridView>
                                    </ContentTemplate>
                                    <Triggers>
                                        <%-- <ajax:AsyncPostBackTrigger ControlID="GrvListado" EventName="Sorting"></ajax:AsyncPostBackTrigger>
                        <ajax:AsyncPostBackTrigger ControlID="GrvListado" EventName="PageIndexChanging"></ajax:AsyncPostBackTrigger>
                        <ajax:AsyncPostBackTrigger ControlID="GrvListado" EventName="RowDataBound"></ajax:AsyncPostBackTrigger>--%>
                                    </Triggers>
                                </ajax:UpdatePanel>
                            </div>
                        </td>
                    </tr>
                </table>
            </ContentTemplate>
            <Triggers>
            </Triggers>
        </asp:UpdatePanel>
    </asp:Panel>
    <ajaxToolkit:ModalPopupExtender ID="mpDespachador" runat="server"
        Enabled="True" PopupControlID="pnlDespachador" BackgroundCssClass="modalBackground"
        TargetControlID="btnOpenProxyInser" CancelControlID="btnCloseProxyInser">
    </ajaxToolkit:ModalPopupExtender>
    <asp:Button ID="btnOpenProxyInser" Style="display: none;" runat="server" Text="[Open Proxy]" />
    <asp:Button ID="btnCloseProxyInser" Style="display: none;" runat="server" Text="[Close Proxy]" />

    <!--    ADDED NEW PANEL TO CONTAINERS ENABLED AND DISABLED-->
    <asp:Panel ID="pnlActualizar" runat="server" CssClass="PanelPopup" Width="750px" BorderWidth="1"
        BorderColor="#004b8e">
        <asp:UpdatePanel ID="UpdatePanel9" runat="server" style="overflow: auto; width: 750px; padding-left: 2px;">
            <ContentTemplate>
                <table cellspacing="0" cellpadding="0" border="0" style="height: 60px; width: 720px; background: url( '../../Imagenes/menu/menu_titulo6.png');">
                    <tr>
                        <td align="right" style="padding-bottom: 5px; padding-top: 10px; padding-right: 15px;">
                            <asp:Button ID="btnActualizar" runat="server" OnClick="btnActualizar_Click" Text="Actualizar" />
                            <asp:Button ID="btnCerrarStatus" runat="server" Text="Cerrar" OnClientClick="return ClosePopup();" />
                        </td>
                    </tr>
                </table>
                <br />


                <table border="0" style="width: 720px; height: 50px; background-color: #EFEFF1" class="cuerpoComision">
                    <tr>
                        <td colspan="4" style="height: 21px"></td>
                    </tr>
                    <tr>
                        <td colspan="2" align="left">
                            <asp:Panel ID="Panel1" runat="server">
                                <asp:UpdatePanel ID="UpdatePanel10" runat="server">
                                    <ContentTemplate>
                                        &nbsp;&nbsp;&nbsp;Retirar un contenedor en especifico:
                            <asp:RadioButton ID="rbtnContStatusSI" runat="server" Text="SI" GroupName="rbtnCont" OnCheckedChanged="rbtnContStatusSI_CheckedChanged" AutoPostBack="true" />
                                        <asp:RadioButton ID="rbtnContStatusNO" runat="server" Text="NO" GroupName="rbtnCont" OnCheckedChanged="rbtnContStatusNO_CheckedChanged" AutoPostBack="true" />&nbsp;
                                    </ContentTemplate>
                                    <Triggers>
                                    </Triggers>
                                </asp:UpdatePanel>
                            </asp:Panel>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4" align="center">
                            <asp:UpdatePanel ID="UpdatePanel14" runat="server">
                                <ContentTemplate>
                                    <div id="divContAutRetiro" style="overflow: auto; width: 550px; height: 205px; padding-left: 2px;">
                                        <asp:GridView ID="gridContenedoresStatus" Width="500px" runat="server" AllowPaging="True" SkinID="GrillaConsulta"
                                            AutoGenerateColumns="False" DataKeyNames="IdAutRetDet,SContenedor,iEstado"
                                            Font-Bold="false" Visible="False" PageSize="9999">
                                            <%--DataKeyNames="iIdRegistro,sPlaca,sChofer" AutoGenerateColumns="false"  OnRowDataBound="gridContenedores_OnRowDataBound" --%>
                                            <Columns>
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="chkSelContActualizar" runat="server" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="IdAutRetDet" HeaderText="Código" Visible="false">
                                                    <ItemStyle Width="5%" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="iEstado" HeaderText="Estado" Visible="false">
                                                    <ItemStyle Width="5%" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="SChasis" HeaderText="Chasis"></asp:BoundField>
                                                <asp:BoundField DataField="SContenedor" HeaderText="Contenedor"></asp:BoundField>
                                            </Columns>
                                            <PagerSettings Visible="False" />
                                        </asp:GridView>
                                    </div>
                                </ContentTemplate>
                                <Triggers>
                                </Triggers>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                </table>
            </ContentTemplate>
            <Triggers>
            </Triggers>
        </asp:UpdatePanel>
    </asp:Panel>
    <ajaxToolkit:ModalPopupExtender ID="mpActualizar" runat="server"
        CancelControlID="btnClosePopup" Enabled="True" PopupControlID="pnlActualizar"
        TargetControlID="btnOpenPopup">
    </ajaxToolkit:ModalPopupExtender>
    <asp:Button ID="btnOpenPopup" Style="display: none;" runat="server" Text="[Open Proxy]" />
    <asp:Button ID="btnClosePopup" Style="display: none;" runat="server" Text="[Close Proxy]" />



    <!--SOLICITUD DE RENOVACION DE ARE-->

    <asp:Panel ID="pnlRenovacion" runat="server" CssClass="PanelPopup" Width="710px" BorderWidth="1"
        BorderColor="#004b8e">
        <asp:UpdatePanel ID="UpdatePanel11" runat="server" style="width: 710px; padding-left: 1px; align-content: center;">
             <ContentTemplate>
            <table border="0" style="width: 700px; height: 10px; background-color: #EFEFF1" class="cuerpoComision">
                <tr>
                    <td>
                        <table border="0" style="width: 700px; height: 50px; background-color: #EFEFF1;" class="cuerpoComision">
                            <tr style="background-color: #0069AE; color: #FFFFFF">
                                <td colspan="4" align="center" style="height: 21px">
                                    <strong>SOLICITUD DE RENOVACION</strong>
                                </td>
                                <td align="left" style="height: 21px">
                                    <asp:ImageButton ID="imgCerrarPopRenovac" runat="server"
                                        ImageUrl="~/Script/Imagenes/IconButton/Delete_Icon.png" OnClick="imgCerrarPopRenovac_Click" />
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 100px">AUT. RETIRO
                                </td>
                                <td>
                                    <asp:TextBox ID="TxtNumeroPO" runat="server" Width="110px" Enabled="False"></asp:TextBox>
                                    <asp:TextBox ID="TxtIdAutRet" Style="display: none" runat="server" MaxLength="10" Width="24px"></asp:TextBox>
                                </td>
                                <td>F. CREACION
                                </td>
                                <td>
                                    <asp:TextBox ID="txtFechaCreacion" runat="server" MaxLength="10" Width="110px"
                                        Enabled="False"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 80px">SITUACION
                                </td>
                                <td>
                                    <asp:TextBox ID="TxtSituacionPO" runat="server" Enabled="False" Width="110px"></asp:TextBox>

                                </td>

                                <td>F.ACTUAL HABILITACION</td>
                                <td>
                                    <asp:TextBox ID="txtFechaHabilitado" runat="server" MaxLength="10" Width="110px"
                                        Enabled="False"></asp:TextBox>

                                </td>
                            </tr>
                            <tr>
                                <td>ESTADO
                                </td>
                                <td>
                                    <asp:TextBox ID="TxtEstadoVigenciaPO" runat="server" Width="110px" Enabled="False"></asp:TextBox>
                                </td>
                                <td>F.MAX VIGENCIA</td>
                                <td>
                                    <asp:TextBox ID="TxtFechaVigenciaMaximaPop" runat="server" MaxLength="10" Width="110px"
                                        Enabled="False"></asp:TextBox>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table border="0" style="width: 700px; height: 50px; background-color: #EFEFF1;" class="cuerpoComision">
                            <tr>

                                <td style="width: 120px">MEDIO DE PAGO</td>
                                <td style="width: 220px">
                                    <asp:FileUpload ID="FileUploadMedioPago" runat="server" Width="380px" accept="application/pdf" />
                                </td>
                                <td style="width: 50px" align="center">
                                    <%-- <asp:ImageButton runat="server" ID="imgBtnUplMedioPago" ImageUrl="~/Imagenes/save.png" Enabled="true" />
                                        <asp:ImageButton runat="server" ID="imgBtnUplMedioPagoupd" ImageUrl="~/Imagenes/cambiar.png" Visible="false" />--%>
                                </td>

                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td align="center"></td>
                </tr>
            </table>

            </ContentTemplate>
        </asp:UpdatePanel>
        <table>
            <tr><td style="width:260px;">

                </td>
                <td>
                    <asp:Label runat="server" ID="lblUplMedioPago" Text=" "></asp:Label>
                    <asp:Button ID="btnSolicitarRenovacion" runat="server" Text="SOLICITAR RENOVACION" Height="30px" CssClass="bootonClas" OnClick="btnSolicitarRenovacion_Click" />
                </td>
            </tr>

        </table>
    </asp:Panel>

    <ajaxToolkit:ModalPopupExtender ID="mpRenovacion" runat="server"
        CancelControlID="btnClosePopupARE" Enabled="True" PopupControlID="pnlRenovacion"
        TargetControlID="btnOpenPopupARE">
    </ajaxToolkit:ModalPopupExtender>
    <asp:Button ID="btnClosePopupARE" Style="display: none;" runat="server" Text="[Open Proxy]" />
    <asp:Button ID="btnOpenPopupARE" Style="display: none;" runat="server" Text="[Close Proxy]" />
    <!-- END -SOLICITUD DE RENOVACION ARE-->


    <asp:HiddenField ID="vl_idSolicitudAreDet" runat="server" />

    <script language="javascript" type="text/javascript">

        var tiempo = 0;
        var seconds = 0;

        $(document).ready(function () {
            $('#rbtnContenedorSi').change(function () {
                if (this.checked)
                    $('#divContenedores').fadeIn('slow');
                else
                    $('#divContenedores').fadeOut('slow');

            });
        });

        var myVar = setInterval(function () {
            clearInterval(myVar);
        }, 1000);

        var myTiempo = setTimeout(function () {
            clearInterval(myTiempo);
        }, 1);

        function myStopFunction() {
            clearInterval(myVar);
            seconds = '<%=GlobalEntity.Instancia.TiempoDuracionSesion%>' * 60;
            tiempo = '<%=GlobalEntity.Instancia.TiempoDuracionSesion%>' * 1000 * 60;

            CuentaTiempo(tiempo);
        }

        function ClosePopub() {
            document.getElementById('<%=btnClosePopub.ClientID %>').click();
            return false;
        }

        function ClosePopup() {
            document.getElementById('<%=btnClosePopup.ClientID %>').click();
            return false;
        }

        function ClosePopubDespachador() {
            document.getElementById('<%=btnCloseProxyInser.ClientID %>').click();
            return false;
        }
        function OpenPopupDespachador(e, sObjeto) {
            document.getElementById('<%=btnOpenProxyInser.ClientID %>').click();
            return false;
        }

        function CuentaTiempo(tiempo) {
            clearTimeout(myTiempo);
            myTiempo = setTimeout(function () {
                document.getElementById('<%=btn_cerrar.ClientID%>').click();
                //window.location = "../Seguridad/eFENIX_Login.aspx";
            }, tiempo);
        }

        function SessionExpireAlert(timeout) {
            seconds = timeout / 1000;
            document.getElementsByName("seconds").innerHTML = seconds;

            myvar = setInterval(function () {
                seconds--;
                document.getElementById("seconds").innerHTML = seconds;
            }, 1000);

            CuentaTiempo(timeout);
        };


        var goodexit = false;
        window.onbeforeunload = confirmRegisterExit;
        function confirmRegisterExit(evt) {
            if (!goodexit) {
                //return "Si vas abandonar el sistema haz click en Cerrar Sesion!!";
                document.getElementById('<%=btn_cerrar.ClientID%>').click();
            }
        }

        var objFilaAnt = null;
        var backgroundColorFilaAnt = "";

        function fc_PressKeyDO() {
            if (event.keyCode == 13) {
                document.getElementById("<%=ImgBtnBuscar.ClientID%>").click();
                return false;
            }
            else {
                return true;
            }
        }

        function HabilitarUno(chk) {
            f = document.forms[0].elements;
            for (x = 0; x < f.length; x++) {
                obj = f[x];
                if (obj.type == 'checkbox') {
                    obj.checked = false;
                }
            }
            chk.enabled;
            chk.checked = true;
        }

        function fc_PressKeyDesp() {
            if (event.keyCode == 13) {
                document.getElementById("<%=imgbBuscar.ClientID%>").click();
                return false;
            }
            else {
                return true;
            }
        }
        function fc_PressKeyDNI() {
            if (event.keyCode == 13) {
                document.getElementById("<%=btnImgChoferDNI.ClientID%>").click();
                return false;
            }
            else {
                return true;
            }
        }
        function fc_PressKeyBrevete() {
            if (event.keyCode == 13) {
                document.getElementById("<%=btnImgBrevete.ClientID%>").click();
                return false;
            }
            else {
                return true;
            }
        }

    </script>
    <style type="text/css">
        .bootonClas {
            background-color: #0069AE !important;
            border-color: #0069AE !important;
            color: #fff !important;
            font-size: 15px;
            font-family: Arial,Helvetica,sans-serif;
            font-weight: bold;
        }
    </style>
</asp:Content>
<asp:Content ID="Content3" runat="server" ContentPlaceHolderID="ContentCab">
    <span class="texto_titulo">Tu Sesion Expirara en &nbsp; 
        <span class="texto_titulo" id="seconds"></span>
        <span>&nbsp;seg.</span>
    </span>
    <asp:Button ID="btn_cerrar" runat="server" Text="cerrar"
        OnClick="btn_cerrar_Click" Style="display: none;" />
</asp:Content>




