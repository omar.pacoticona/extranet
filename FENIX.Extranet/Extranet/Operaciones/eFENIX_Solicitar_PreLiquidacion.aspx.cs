﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FENIX.DataAccess;
using FENIX.Common;
using FENIX.BusinessEntity;
using FENIX.BusinessLogic;
using System.Web.Security;
using System.Text.RegularExpressions;

public partial class Extranet_Operaciones_eFENIX_Solicitar_PreLiquidacion : System.Web.UI.Page
{

    private void SCA_MsgInformacion(string strError)
    {
        MensajeScript(strError);
    }

    private void MensajeScript(string SMS)
    {
        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "", "<script>alert('" + SMS + "');</script>", false);
    }

    protected void Page_Load(object sender, EventArgs e)
    {

        String tiempo = (Convert.ToInt32(GlobalEntity.Instancia.TiempoDuracionSesion.ToString()) * 1000 * 60).ToString();
        Session["Reset"] = true;
        if (!Page.IsPostBack)
        {

            //if (GrvListado.Rows.Count == 0)
            //{
            //    BE_Cliente oBE_Cliente = new BE_Cliente();
            //    oBE_Cliente.sIdCliente = GlobalEntity.Instancia.IdCliente.ToString();
            //    idLblComisionista.Text = GlobalEntity.Instancia.NombreCliente.ToString();

            //}
            //// ImgBtnGrabar.Enabled = false;
            //btnAceptar.Enabled = false;


            if (GrvListado.Rows.Count == 0)
            {
                List<BE_AcuerdoComision> oLista_Acuerdo = new List<BE_AcuerdoComision>();
                BE_AcuerdoComision oBE_AcuerdoComision = new BE_AcuerdoComision();
                //oLista_Acuerdo.Add(oBE_AcuerdoComision);
                //GrvListado.DataSource = oLista_Acuerdo;
                //GrvListado.DataBind();
            }


            if (GrvListaContenedores.Rows.Count == 0)
            {
                //List<BE_AcuerdoComision> oLista_Contenedores = new List<BE_AcuerdoComision>();
                //BE_AcuerdoComision oBE_AcuerdoComision = new BE_AcuerdoComision();
                //oLista_Contenedores.Add(oBE_AcuerdoComision);
                //GrvListaContenedores.DataSource = oLista_Contenedores;
                //GrvListaContenedores.DataBind();
            }

            ScriptManager.RegisterStartupScript(this, GetType(), "mensaje", "SessionExpireAlert(" + tiempo + ");", true);
        }
        else
            ScriptManager.RegisterStartupScript(this, GetType(), "mensaje", "myStopFunction();", true);
    }


    protected void btn_cerrar_Click(object sender, EventArgs e)
    {
        if (GlobalEntity.Instancia.CerrarExtranet == "S")
        {
            BL_Usuario oBL_Usuario = new BL_Usuario();
            oBL_Usuario.GrabaInicioSesion(GlobalEntity.Instancia.Usuario, "0");
            Session.Abandon();
            FormsAuthentication.SignOut();
            Response.Redirect("../../eFENIX_Login.aspx");
            Response.End();
        }
    }






    protected void ImgBtnBuscar_Click(object sender, EventArgs e)
    {
        BE_Liquidacion oBE_Liquidacion = new BE_Liquidacion();
        BL_Liquidacion oBL_Liquidacion = new BL_Liquidacion();
        BE_LiquidacionList oLista_Liquidacion = new BE_LiquidacionList();
        BE_LiquidacionList oLista_Contenedores = new BE_LiquidacionList();

        Int32 tipoOperacion = 66; //IMPO
        Int32 LineaNegocio = 89; //D. TEMPORAL.
        Int32 MasterLCL = 0;
        String NroVolante = "";
        oBE_Liquidacion.sDocOrigen = txtDocumento.Text;
        oBE_Liquidacion.iIdOperacion = tipoOperacion;
        oBE_Liquidacion.iIdLineaNegocio = LineaNegocio;

        oLista_Liquidacion = oBL_Liquidacion.Listar_Info_DocumentoOrigen(oBE_Liquidacion);

        if (oLista_Liquidacion.Count > 0)
        {
            lblCliente.Text = oLista_Liquidacion[0].sCliente.ToString();
            lblAgntAduana.Text = oLista_Liquidacion[0].sDomentoAgeAdu.ToString();
            NroVolante = oLista_Liquidacion[0].sNroVolante;

            HdtxtIdVolante.Text = oLista_Liquidacion[0].iIdVolante.ToString();
            HdtxtIddocOri.Text = oLista_Liquidacion[0].iIdDocOri.ToString();
            txtIdTipoMoneda.Text = oLista_Liquidacion[0].sTipoMoneda;
            txtOperacion.Text = oLista_Liquidacion[0].iIdOperacion.ToString();
            HdtxtIdAcuerdo.Text = oLista_Liquidacion[0].iIdContrato.ToString();
            txtSuperNegocio.Text = "89";
            UpdatePanel2.Update();

            if (NroVolante != "")
            {
                oBE_Liquidacion.sNroVolante = NroVolante;
                oLista_Contenedores = oBL_Liquidacion.Listar_Contenedores_Liquidar(oBE_Liquidacion);
                if (oLista_Contenedores.Count > 0)
                {
                    GrvListaContenedores.DataSource = oLista_Contenedores;
                    GrvListaContenedores.DataBind();
                }
                else
                {

                }
            }
            else
            {

            }
        }
        else
        {

        }






    }



    protected void btnCakcular_Click(object sender, EventArgs e)
    {
        BE_Liquidacion oBE_Liquidacion = new BE_Liquidacion();
        BL_Liquidacion oBL_Liquidacion = new BL_Liquidacion();
        BE_Liquidacion oBE_Upd_Liquidacion = new BE_Liquidacion();
        BL_Liquidacion oBL_Upd_Liquidacion = new BL_Liquidacion();
        BE_LiquidacionList oBE_LiquidacionServicio = new BE_LiquidacionList();
        //BE_LiquidacionList oBE_LiquidacionAlmacenaje = new BE_LiquidacionList();
        String[] Resultado = null;
        int iRetorno = 2;

        foreach (GridViewRow GrvRow in GrvListaContenedores.Rows)
        {
            if ((GrvRow.FindControl("chkSeleccionarContenedor") as CheckBox).Checked)
            {
                TextBox txtFechaRetiro = GrvRow.FindControl("TxtSerie") as TextBox;
                // var match = this.match("/^ ([0 - 9]{ 2})\/ ([0 - 9]{ 2})\/ ([0 - 9]{ 4})$/");
                Regex reg = new Regex(@"[0-9]{2}/[0-9]{2}/[0-9]{4}");
                if (reg.IsMatch(txtFechaRetiro.Text) == false)
                {
                    SCA_MsgInformacion("Ingrese el formato correcto");
                    return;
                }
                if (txtFechaRetiro.Text == "")
                {
                    SCA_MsgInformacion("Obligario fecha de retiro para todos los contenedores");
                    return;
                }

            }
            else
            {
                SCA_MsgInformacion("Obligario fecha de retiro para todos los contenedores");
                return;
            }
        }
        if (txtOperacion.Text == "66")
        {
            oBE_Liquidacion.iIdOperacion = Convert.ToInt32(TipoOperacion.Importacion);
            //if (!ChkMasterLCL.Checked)
            oBE_Liquidacion.sNroVolante = txtDocumento.Text;
            //else
            //    oBE_Liquidacion.sNroVolante = null;
        }
        else if (txtOperacion.Text == "65")
            oBE_Liquidacion.iIdOperacion = Convert.ToInt32(TipoOperacion.Exportacion);

        if (txtOperacion.Text == "66")
        {
            var a = txtSuperNegocio.Text;

            //if (ChkMasterLCL.Checked)
            //   Resultado = oBL_Liquidacion.Listar_Recuperar_Contrato_2(Convert.ToInt32(DdlLineaNegocio.SelectedValue), null, iIdDocOri, Convert.ToInt32(TipoOperacion.Importacion), "1").Split('|');
            // else
            Resultado = oBL_Liquidacion.Listar_Recuperar_Contrato_2(Convert.ToInt32(txtSuperNegocio.Text), Convert.ToInt32(txtDocumento.Text), null, Convert.ToInt32(TipoOperacion.Importacion), "0").Split('|');
        }
        else if (txtOperacion.Text == "65")
            Resultado = oBL_Liquidacion.Listar_Recuperar_Contrato_2(Convert.ToInt32((txtSuperNegocio.Text)), null, Convert.ToInt32(HdtxtIddocOri.Text), Convert.ToInt32(TipoOperacion.Exportacion), "0").Split('|');


        if (txtOperacion.Text == "66")
        {
            //if (ChkMasterLCL.Checked)
            //    iRetorno = oBL_Liquidacion.RegistrarServicios_Mandatorios_2(89, Convert.ToInt32(TipoOperacion.Importacion), iIdDocOri, iIdAcuerdo, null, "2");
            //else
            Int32? iIdDocOri = Convert.ToInt32(HdtxtIddocOri.Text);
            Int32? iIdAcuerdo = Convert.ToInt32(HdtxtIdAcuerdo.Text);
            iRetorno = oBL_Liquidacion.RegistrarServicios_Mandatorios_2(89, Convert.ToInt32(TipoOperacion.Importacion), iIdDocOri, iIdAcuerdo, Convert.ToInt32(txtDocumento.Text), "0");
        }
        else if (txtOperacion.Text == "65")
            iRetorno = oBL_Liquidacion.RegistrarServicios_Mandatorios_2(89, Convert.ToInt32(TipoOperacion.Exportacion), Convert.ToInt32(HdtxtIddocOri.Text), Convert.ToInt32(HdtxtIdAcuerdo), null, "0");


        foreach (GridViewRow GrvRow in GrvListaContenedores.Rows)
        {
            if ((GrvRow.FindControl("chkSeleccionarContenedor") as CheckBox).Checked)
            {
                TextBox txtFechaRetiro = GrvRow.FindControl("TxtSerie") as TextBox;
                oBE_Liquidacion.iIdDocOriDet = Convert.ToInt32(GrvListaContenedores.DataKeys[GrvRow.RowIndex].Values["iIdDocOriDet"].ToString());
                oBE_Liquidacion.dtFechaRetiro = Convert.ToDateTime(txtFechaRetiro.Text);
                if (txtFechaRetiro.Text == "")
                    oBE_Liquidacion.dtFechaRetiro = DateTime.Now.Date;
                //  oBE_Liquidacion.dtFecha_Liquidacion = Convert.ToDateTime(TxtFechaAlma.Text + " " + DateTime.Now.Hour.ToString() + ":" + DateTime.Now.Minute.ToString() + ":" + DateTime.Now.Second.ToString());
                //int result = Convert.ToInt32(txt2.Text) * Convert.ToInt32(txt3.Text);

                // var xd = GrvListaContenedores.DataKeys[GrvRow.RowIndex].Values["dtFechaRetiro"].ToString();
                oBL_Upd_Liquidacion.Actualizar_FechaRetiroXContenedor(oBE_Liquidacion.iIdDocOriDet, oBE_Liquidacion.dtFechaRetiro);
                // var xd = GrvListaContenedores.DataKeys[GrvRow.RowIndex].Values["dtFechaRetiro"].ToString();
                //var xd = ctl00_CntMaster_GrvListaContenedores_ctl02_TxtSerie.text;
                //var xd = txtFechaRetiro.Text;


            }
        }


        oBE_Liquidacion.iIdLiq = 0;
        oBE_Liquidacion.iIdSuperNegocio_2 = 89;
        oBE_Liquidacion.sNroVolante = txtDocumento.Text;
        oBE_Liquidacion.iIdDocOri = Convert.ToInt32(HdtxtIddocOri.Text);
        oBE_Liquidacion.dtFecha_Liquidacion = DateTime.Now.Date;
        oBE_Liquidacion.iIdMoneda = Convert.ToInt32(txtIdTipoMoneda.Text);
        oBE_Liquidacion.iIdOperacion = Convert.ToInt32(txtOperacion.Text);
        oBE_Liquidacion.iIdAcuerdo = Convert.ToInt32(HdtxtIdAcuerdo.Text);
        oBE_Liquidacion.sMasterLCL = "0";
        oBE_LiquidacionServicio = oBL_Liquidacion.Listar_PreLiquidacion_2(oBE_Liquidacion);

        GrvListado.DataSource = oBE_LiquidacionServicio;
        GrvListado.DataBind();

    }

    protected void GrvListaContenedores_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            //DataKey dataKey;
            //dataKey = this.GrvListaContenedores.DataKeys[e.Row.RowIndex];
            BE_Liquidacion oitem = (e.Row.DataItem as BE_Liquidacion);
            System.Web.UI.WebControls.CheckBox chkSeleccion = (System.Web.UI.WebControls.CheckBox)e.Row.FindControl("chkSeleccionarContenedor");

            if (oitem.iIdDocOriDet != null)
            {
                for (int c = 1; c < e.Row.Cells.Count; c++)
                {
                    chkSeleccion.Checked = true;
                    chkSeleccion.Enabled = false;


                }
            }
        }
    }
}