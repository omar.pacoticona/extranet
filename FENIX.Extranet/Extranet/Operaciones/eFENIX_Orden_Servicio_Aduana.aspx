﻿<%@ Page Language="C#" 
MasterPageFile="~/Extranet/Seguridad/eFENIX_Principal.master"
AutoEventWireup="true" 
CodeFile="eFENIX_Orden_Servicio_Aduana.aspx.cs" 
Inherits="Extranet_Operaciones_eFENIX_Orden_Servicio_Aduana"
Title="Orden de Servicio" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="CntMaster" runat="Server">

    <table cellspacing="0" cellpadding="0" style="width: 100%;">
        
        <tr>
            <td class="form_titulo" style="width: 85%">
                ORDEN DE SERVICIO
            </td>
            <td align="right" class="form_titulo_verde" style="width: 15%" >
                <ajax:UpdatePanel ID="UdpTotales" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <asp:TextBox ID="LblTotal" Text="Total de Registros:" style="background-color: #0069ae; color: #FFFFFF" ReadOnly="true" BorderColor="#0069ae" runat="server"></asp:TextBox>
                    </ContentTemplate>
                </ajax:UpdatePanel>
            </td>               
        </tr>
        
        <tr>
            <td valign="top" colspan="2">
                <table class="form_bandeja" style="margin-top: 5px; margin-bottom: 5px; width: 100%;">                   
                 
                    <tr>
                        <td>
                            BL / BK
                        </td>
                        <td>
                            <asp:TextBox ID="TxtDocumentoM" runat="server" MaxLength="10" Style="text-transform: uppercase"
                                Width="135px"></asp:TextBox>
                        </td>
                        <td>
                            Volante
                        </td>
                        <td>
                            <asp:TextBox ID="TxtVolante" runat="server" MaxLength="15" Style="text-transform: uppercase"
                                Width="125px"></asp:TextBox>
                        </td>
                        <td>
                            &nbsp;
                        </td>
                        <td>
                            <asp:ImageButton ID="ImgBtnBuscar" runat="server" ImageUrl="~/Imagenes/Formulario/btn_consultar.JPG"
                                OnClick="ImgBtnBuscar_Click" OnClientClick="return ValidarFiltro();"/>
                            &nbsp;&nbsp;
                            <asp:ImageButton ID="ImgBtnNuevo" runat="server" ImageUrl="~/Imagenes/Formulario/btn_nuevo.JPG"
                                OnClick="ImgBtnNuevo_Click"/>
                        </td>
                        
                    </tr>
                </table>
             </td>   
        </tr>
    

        <tr valign="top" style="height: 100%">
            <td colspan="5">
                <ajax:UpdatePanel ID="upDocumentoOrigen" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div style="overflow: auto; float:left; width: 70%; height: 100%">
                            <tr><td>
                                &nbsp;
                                </td></tr>
                            <span>Seleccionar Carga</span>
                            <asp:GridView ID="GrvListado" runat="server" AutoGenerateColumns="False" DataKeyNames="iIdDocOriDet"
                                SkinID="GrillaConsulta" Width="70%" OnRowDataBound="GrvListado_RowDataBound" OnRowCommand="GrvListado_RowCommand" >
                                <Columns>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkSeleccionar" runat="server"/>
                                        </ItemTemplate>
                                        <HeaderStyle BackColor="#0069ae" Width="3%" />
                                    </asp:TemplateField>                                    
                                    <asp:BoundField DataField="sDocumentoHijo" HeaderText="BL" />
                                    <asp:BoundField DataField="iItem" HeaderText="ITEM" />
                                    <asp:BoundField DataField="sCarga" HeaderText="CNT/CHASIS" />
                                    <asp:BoundField DataField="sTamano" HeaderText="TNO" />
                                    <asp:BoundField DataField="sTipo" HeaderText="TIPO" />
                                    <asp:BoundField DataField="sCondicion" HeaderText="CONDICION" />
                                    <asp:BoundField DataField="iBultos" HeaderText="BULTOS" />
                                    <asp:BoundField DataField="dPeso" HeaderText="PESO" />                                   
                                </Columns>
                            </asp:GridView>
                        </div>
                        <div style="overflow: auto; float:right; width: 30%; height: 100%">
                            <table>

                            </table>
                        </div>
                    </ContentTemplate>
                    <Triggers>
                        <ajax:AsyncPostBackTrigger ControlID="GrvListado" EventName="SelectedIndexChanged" />
                        <ajax:AsyncPostBackTrigger ControlID="ImgBtnBuscar" EventName="Click" />
                        <ajax:AsyncPostBackTrigger ControlID="dnvListado" EventName="PageChanged" />
                        <ajax:AsyncPostBackTrigger ControlID="dnvListado" EventName="NextPage" />
                        <ajax:AsyncPostBackTrigger ControlID="dnvListado" EventName="PreviousPage" />
                        <ajax:AsyncPostBackTrigger ControlID="dnvListado" EventName="FirstPage" />  
                        <ajax:AsyncPostBackTrigger ControlID="dnvListado" EventName="LastPage" />     
<asp:AsyncPostBackTrigger ControlID="GrvListado" EventName="SelectedIndexChanged"></asp:AsyncPostBackTrigger>
<asp:AsyncPostBackTrigger ControlID="ImgBtnBuscar" EventName="Click"></asp:AsyncPostBackTrigger>
<asp:AsyncPostBackTrigger ControlID="dnvListado" EventName="PageChanged"></asp:AsyncPostBackTrigger>
<asp:AsyncPostBackTrigger ControlID="dnvListado" EventName="NextPage"></asp:AsyncPostBackTrigger>
<asp:AsyncPostBackTrigger ControlID="dnvListado" EventName="PreviousPage"></asp:AsyncPostBackTrigger>
<asp:AsyncPostBackTrigger ControlID="dnvListado" EventName="FirstPage"></asp:AsyncPostBackTrigger>
<asp:AsyncPostBackTrigger ControlID="dnvListado" EventName="LastPage"></asp:AsyncPostBackTrigger>
                    </Triggers>
                </ajax:UpdatePanel>
            </td>
        </tr>
        <tr valign="top" style="height: 6%;">
            <td colspan="5">
               <ajax:UpdatePanel ID="upDnvListado" runat="server">
                    <ContentTemplate>
                        <FENIX:DataNavigator ID="dnvListado" runat="server" AllowCustomPaging="True" ButtonText="Ir" 
                            ForeColor="White" Font-Size="11px" BackColor="#0069ae" 
                            CurrentPage="1" CustomClientFunction="" EnableAskingAfterPage="False" EnableCustomScript="False"
                            GridViewId="GrvListado" ImagesPath="~/Imagenes/DN/" IsParentShowWindow="False"
                            MessageAskingAfterPage="" PageIndicatorFormat="Página {0} / {1}" Visible="False"
                            Width="100%" WidthButtonIr="" WidthTextBoxNumPagina="25px" />
                    </ContentTemplate>
                    <Triggers>
                        <ajax:AsyncPostBackTrigger ControlID="dnvListado" EventName="PageChanged" />
                        <ajax:AsyncPostBackTrigger ControlID="ImgBtnBuscar" EventName="Click" />
                    </Triggers>
                </ajax:UpdatePanel>
            </td>
        </tr>
    </table>

    
    <asp:HiddenField ID="HdImbContenedor" runat="server" />


<%-- Popup - Nuevo Servicio--%>
    <asp:Panel ID="__PopPanel__" runat="server" CssClass="modalBackground" Style="left: 0px;
        top: 0px; display: none; position: fixed; z-index: 10000; display: none;" />

    <asp:Panel ID="pnlDetalle" runat="server" Style="display: none; width: 480px; height: 250px;
        position: fixed; z-index: 100002; border: dimgray 1px solid; background-color: White;" DefaultButton="btnCancelarVerDetalle">
        <ajax:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <table border="0" cellpadding="0" cellspacing="0" width="100%" style="padding: 5px;">
                    <tr valign="top" class="form_titulo" style="background-color: #0069ae; height: 15px; text-transform: none;">
                        <td style="padding: 5px;">
                            Registrar Nueva Orden de Servicio
                        </td>
                    </tr>
                    <tr>
                        <td valign="top" colspan="2">
                            <table class="form_bandeja" style="width: 100%;">
                                <tr>
                                    <td colspan="4">
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4">
                                        La orden de servicio se genera de Lunes a Viernes hasta las 17:00 horas y Sábado hasta las 11:00 
                                        No aplica si el día siguiente es feriado
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4">
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 15%;">
                                        &nbsp;Documento
                                    </td>
                                    <td style="width: 85%;" colspan="3">
                                        &nbsp;&nbsp;<asp:TextBox ID="TxtDocumentoNuevo" runat="server" MaxLength="30" Style="text-transform: uppercase" Width="125px"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        &nbsp;Servicio
                                    </td>
                                    <td style="width: 40%;">
                                        &nbsp;&nbsp;<asp:DropDownList ID="DplServicio" runat="server" Width="333px"></asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4">
                                        &nbsp;
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>

                    <tr style="height: 15px;">
                        <td valign="bottom" colspan="2">
                            <table style="width: 100%;">
                                <tr>
                                    <td style="text-align: right;">
                                        <asp:Button ID="btnGrabarOrdenServicio" runat="server" CssClass="botonAceptar" 
                                            style="border-width:0px;" 
                                            OnClick="btnGrabarOrdenServicio_Click" /> 

                                        <asp:Button ID="btnCancelarVerDetalle" runat="server" CssClass="botonCancelar"
                                            Style="border-width: 0px;" 
                                            OnClientClick="javascript: return fc_CerrarVerDetalle();" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </ContentTemplate>
            <Triggers>
                <ajax:AsyncPostBackTrigger ControlID="GrvListado" EventName="RowCommand" />
            </Triggers>
        </ajax:UpdatePanel>
    </asp:Panel>


    <script language="javascript" type="text/javascript">

        var tiempo = 0;
        var seconds = 0;

        var myVar = setInterval(function () {
            clearInterval(myVar);
        }, 1000);

        var myTiempo = setTimeout(function () {
            clearInterval(myTiempo);
        }, 1);

        function myStopFunction() {
            clearInterval(myVar);
            seconds = '<%=GlobalEntity.Instancia.TiempoDuracionSesion%>' * 60;
            tiempo = '<%=GlobalEntity.Instancia.TiempoDuracionSesion%>' * 1000 * 60;

            CuentaTiempo(tiempo);
        }

        function CuentaTiempo(tiempo) {
            clearTimeout(myTiempo);
            myTiempo = setTimeout(function () {
                document.getElementById('<%=btn_cerrar.ClientID%>').click();
                //window.location = "../Seguridad/eFENIX_Login.aspx";
            }, tiempo);
        }



        function SessionExpireAlert(timeout) {
            seconds = timeout / 1000;
            document.getElementsByName("seconds").innerHTML = seconds;

            myvar = setInterval(function () {
                seconds--;
                document.getElementById("seconds").innerHTML = seconds;
            }, 1000);

            CuentaTiempo(timeout);
        };

        var goodexit = false;
        window.onbeforeunload = confirmRegisterExit;
        function confirmRegisterExit(evt) {
            if (!goodexit) {
                //return "Si vas abandonar el sistema haz click en Cerrar Sesion!!";
                document.getElementById('<%=btn_cerrar.ClientID%>').click();
            }
        }



        var objFilaAnt = null;
        var backgroundColorFilaAnt = "";

        function fc_SeleccionaFilaSimple(objFila, objrowIndex, chkID) {
            try {

                if (objFilaAnt != null) {
                    objFilaAnt.style.backgroundColor = backgroundColorFilaAnt;
                }
                objFilaAnt = objFila;
                backgroundColorFilaAnt = objFila.style.backgroundColor;
                objFila.style.backgroundColor = "#c4e4ff";
            }

            catch (e) {
                error = e.message;
            }
        }

        function HabilitarUno(chk) {
            f = document.forms[0].elements;
            for (x = 0; x < f.length; x++) {
                obj = f[x];
                if (obj.type == 'checkbox') {
                    obj.checked = false;
                }
            }
            chk.enabled;
            chk.checked = true;
        }
        
        function fc_PressKeyDO() {
            if (event.keyCode == 13) {
                document.getElementById("<%=ImgBtnBuscar.ClientID%>").click();
                return false;
            }
            else {
                return true;
            }
        }
        
        
        function ValidarFiltro() {
           
            var sDocOrigenM = document.getElementById('<%=TxtDocumentoM.ClientID %>').value;
            var sVolante = document.getElementById('<%=TxtVolante.ClientID %>').value;
          

            var sCriterio = sManifiesto + sDocOrigenM + sVolante + sOrden;

            if ((fc_Trim(sCriterio) == "")) {
                alert('Debe indicar al menos un criterio de búsqueda');
                return false;
            }
            else {
                return true;
            }
        }

        function OpenPopub() {
            Mostrar('block');
        }

        function Mostrar(estilo) {
            document.getElementById('<%=pnlDetalle.ClientID%>').style.left = (screen.width - 550) / 2 + 'px';
            document.getElementById('<%=pnlDetalle.ClientID%>').style.top = (screen.height - 500) / 2 + 'px';

            document.getElementById('<%=__PopPanel__.ClientID%>').style.width = screen.width + '0px';
            document.getElementById('<%=__PopPanel__.ClientID%>').style.height = screen.height + '0px';

            document.getElementById('<%=__PopPanel__.ClientID%>').style.display = estilo;
            document.getElementById('<%=pnlDetalle.ClientID%>').style.display = estilo;
        }

        function fc_CerrarVerDetalle() {
            Mostrar('none');
            return false;
        }
    </script>

</asp:Content>
<asp:Content ID="Content3" runat="server" contentplaceholderid="ContentCab">
    <span class="texto_titulo" >Tu Sesion Expirara en &nbsp; 
        <span class="texto_titulo" id="seconds"> </span>
        <span>&nbsp;seg.</span>
     </span>
     <asp:Button ID="btn_cerrar" runat="server" Text="cerrar" 
        onclick="btn_cerrar_Click"  Style="display: none;" />
</asp:Content>
