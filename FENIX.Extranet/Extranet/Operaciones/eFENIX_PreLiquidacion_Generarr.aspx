﻿<%@ Page Language="C#" MasterPageFile="~/Extranet/Seguridad/eFENIX_Principal.master" AutoEventWireup="true" CodeFile="eFENIX_PreLiquidacion_Generarr.aspx.cs" Inherits="Extranet_Operaciones_eFENIX_PreLiquidacion_Generarr" %>

 <asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
      

      <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"/>
     <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
     <%-- <script src="https://code.jquery.com/jquery-1.12.4.js" type="text/javascript"></script>--%>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js" type="text/javascript"></script>
   

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
      <script src="../../Script/Java_Script/jsPreLiquidacion.js"></script>
     <link rel="stylesheet" href="../../Script/Hojas_EStilo/preLoad.css"/>


     <link href="https://cdn.jsdelivr.net/sweetalert2/4.2.4/sweetalert2.min.css" rel="stylesheet"/>
<script src="https://cdn.jsdelivr.net/sweetalert2/4.2.4/sweetalert2.min.js"></script>
     
<%--<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>--%>


    </asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="CntMaster" runat="Server">
    <!DOCTYPE html>
    
<html>
        <body>
    <table cellspacing="0" cellpadding="0" class="w-100">
        <tr>
            <td class="form_titulo" style="width: 85%">
               <%-- <asp:Button ID="Button1" runat="server" Text="" BackColor="#0069ae" BorderColor ="#0069ae" OnClick="ImgBtnGrabar_Click" Width="0px" Height="0px"/> --%>
                <asp:TextBox ID="LblTitulo" Text="Generar PreLiquidacion" ReadOnly="true" 
                    style="background-color: #0069ae; border: #0069ae; color: #FFFFFF; font-family: Calibri; font-size: 13px; font-weight: bold; text-transform: uppercase;" 
                    runat="server" Width="231px" ></asp:TextBox>
                 
            </td> 
        </tr>

    </table>
   
    <table class="form_bandeja" style=" width: 100%; margin-top:5px;">
        <tr>
                        <td style="width:90px;"> 
                               <ajax:UpdatePanel ID="UpdatePanel7" runat="server" UpdateMode="Conditional" >
                    <ContentTemplate>
                            <asp:Label ID="txtVolBook" runat="server">Documento </asp:Label>
                           </ContentTemplate>
                                   <Triggers>
                                       <asp:AsyncPostBackTrigger ControlID="ImgBtnBuscar" EventName="Click"></asp:AsyncPostBackTrigger>
                                        <asp:AsyncPostBackTrigger ControlID="ImgBtnGrabar" EventName="Click"></asp:AsyncPostBackTrigger>
                                   </Triggers>
                                 </ajax:UpdatePanel>
                        </td>
                        <td  style="width:220px;" >
                             <ajax:UpdatePanel ID="UpdatePanel4" runat="server" UpdateMode="Conditional" >
                    <ContentTemplate>
                           <asp:TextBox ID="txtDocumento" runat="server" Width="210px" Style="text-transform: uppercase;" autocomplete="off" Placeholder="VOLANTE O BOOKING"></asp:TextBox>
                        </ContentTemplate>
                                 </ajax:UpdatePanel>
                        </td>

                        <td style="width:70px;">                          
                            Operacion
                           
                        </td>
                 <td style="width:200px;">  
                     <ajax:UpdatePanel ID="UpdatePanel5" runat="server" UpdateMode="Conditional" >
                    <ContentTemplate>
               &nbsp; <%--<asp:DropDownList ID="DplOperacion" runat="server" Width="189px" Enabled="false"></asp:DropDownList>--%>
                         <%--<asp:Label ID="lblOperacion" runat="server"> -- </asp:Label>--%>
                        <asp:TextBox ID="lblOperacion" runat="server" Width="190px" Enabled="false" Style="text-transform: uppercase; margin-left: 27;" autocomplete="off"></asp:TextBox>
                        </ContentTemplate>
                          <Triggers>
                              <asp:AsyncPostBackTrigger ControlID="ImgBtnBuscar" EventName="Click"></asp:AsyncPostBackTrigger>
                               <asp:AsyncPostBackTrigger ControlID="ImgBtnGrabar" EventName="Click"></asp:AsyncPostBackTrigger>
                                   </Triggers>
                     </ajax:UpdatePanel>           
                      </td>

                        <td style="width:110px;">
                            <ajax:UpdatePanel ID="UpdatePanel8" runat="server" UpdateMode="Conditional" >
                    <ContentTemplate>
                             <asp:Label ID="lblNomFecha" runat="server"> Fecha de Retiro </asp:Label>
                        </ContentTemplate>
                                 <Triggers>
                              <asp:AsyncPostBackTrigger ControlID="ImgBtnBuscar" EventName="Click"></asp:AsyncPostBackTrigger>
                               <asp:AsyncPostBackTrigger ControlID="ImgBtnGrabar" EventName="Click"></asp:AsyncPostBackTrigger>
                                   </Triggers>
                                </ajax:UpdatePanel>
                        </td>
                        <td style="width:110px;">
                            <input id="txtFin_datep" type="text" readonly maxlength="10" name="DatePickerFinname" class="inputText_extranet" style="width:100px;" />
                        </td>

                        <td >                           
                           
                                 <asp:Button ID="ImgBtnBuscar" runat="server" Text="Consultar" class="btn btn-primary" BackColor="#0069ae" OnClick="ImgBtnBuscar_Click" />
                            &nbsp;
                         <%--  <asp:Button ID="ImgBtnGrabar" runat="server" Text="Grabar" class="btn btn-primary"  OnClick="ImgBtnGrabar_Click" BackColor="#0069ae" OnClientClick="return validar();" />--%>
                         
                        </td> 
            <%--OnClientClick="return validar();--%>

                    </tr>
                  
    </table>

     <table cellspacing="0" cellpadding="0" style="width: 100%; height:20px;">
         <tr style="height: 55px;">
            <td valign="top" colspan="2">
                <ajax:UpdatePanel ID="upCabecera" runat="server" UpdateMode="Conditional" >
                    <ContentTemplate>
                 <table class="form_bandeja" style=" width: 100%; height:50px;">
                   <tr>
                        
                        <td style="width:90px;" > Cliente
                        </td>
                        <td style="width:220px;" >
                             <asp:TextBox ID="txtCliente" runat="server" Width="210px" Enabled="false" Style="text-transform: uppercase; margin-left: 27;" autocomplete="off"></asp:TextBox>
                            <%--<asp:DropDownList ID="dplLineaNegocio" runat="server" Width="210px" Enabled="false" style="text-align:center;">
                            </asp:DropDownList>--%>
                        </td>
                       
                        <td style="width:76px;">
                            <asp:Label ID="lblDocOrigen" runat="server">BL </asp:Label> 
                        </td>
                        <td style="width:197px;">
                            <asp:TextBox ID="txtsDocOrigen" runat="server" Width="190px" Enabled="false" Style="text-transform: uppercase; margin-left: 27;" autocomplete="off"></asp:TextBox>
                        </td>
                        <td style="width:108px;" >
                             <asp:Label ID="lblMoneda" runat="server">Moneda </asp:Label> 
                        </td>
                        <td style="width:105px;">
                            <asp:DropDownList ID="DplMonedaGrabar" runat="server" Width="100px" style="text-align:center;">
                            </asp:DropDownList>
                           
                        </td>
                       <td>
                            &nbsp;
                              <asp:Button ID="ImgBtnGrabar" runat="server" Text="  Grabar   " class="btn btn-primary"  OnClick="ImgBtnGrabar_Click" BackColor="#0069ae" OnClientClick="return validar();" />
                       </td>
                       <td>
                            <asp:TextBox ID="txtClienteAcuerdo" runat="server" Width="50px" MaxLength="10" Enabled="false" Style="text-transform: uppercase; margin-left: 27;" autocomplete="off" Visible="false"></asp:TextBox>
                           <asp:TextBox ID="TxtNroAcuerdo" runat="server" Width="30px" Enabled="false" MaxLength="10" Style="text-transform: uppercase;" autocomplete="off" Visible="false"></asp:TextBox>
                            <asp:TextBox ID="TxtIdAcuerdo" runat="server" Width="20px" Visible="false"  MaxLength="10" Style="text-transform: uppercase;" autocomplete="off"></asp:TextBox>
                           <asp:TextBox ID="TxtIdMoneda" runat="server" Width="10px" Visible="false" MaxLength="10" Style="text-transform: uppercase;" autocomplete="off" ></asp:TextBox>
                           <asp:TextBox ID="TxtIdCliente" runat="server" Width="30px" Visible="false"  Style="text-transform: uppercase;" autocomplete="off"></asp:TextBox>
                           <asp:TextBox ID="TxtIdAgntAduanas" runat="server" Width="30px" Visible="false"  Style="text-transform: uppercase;" autocomplete="off"></asp:TextBox>
                           <asp:TextBox ID="txtNroVolante" runat="server" Width="30px" Visible="false"  Style="text-transform: uppercase;" autocomplete="off"></asp:TextBox>
                            <asp:TextBox ID="HdTxtIdOperacion" runat="server" Width="15px" Visible="false" MaxLength="10" Style="text-transform: uppercase;" autocomplete="off"></asp:TextBox>
                             <asp:TextBox ID="txtIdDocOri" runat="server" Width="60px" Visible="false"  Style="text-transform: uppercase;" autocomplete="off"></asp:TextBox>
                           <asp:HiddenField ID="vi_IdMoneda" runat="server" />
                             <asp:HiddenField ID="vi_Operacion" runat="server" />
                           <asp:HiddenField ID="vi_MonedaGrabar" runat="server" />
                           <asp:HiddenField ID="vi_MonedaGrabar2" runat="server" />
                       </td>
                    </tr>
                  <%--  <tr>
                        
                        <td >
                            <asp:TextBox ID="txtClienteAcuerdo" runat="server" Width="100px" Enabled="false" Style="text-transform: uppercase; margin-left: 27;" autocomplete="off" Visible="false"></asp:TextBox>
                        </td>
                        <td style="width:80px;"></td>
                        
                        <td>
                            <asp:TextBox ID="TxtNroAcuerdo" runat="server" Width="70px" Enabled="false" Style="text-transform: uppercase; margin-left: 27;" autocomplete="off" Visible="false"></asp:TextBox>
                            <asp:TextBox ID="TxtIdAcuerdo" runat="server" Width="40px" Visible="false" Style="text-transform: uppercase; margin-left: 27;" autocomplete="off"></asp:TextBox>
                            <asp:TextBox ID="TxtIdMoneda" runat="server" Width="40px" Visible="false" Style="text-transform: uppercase; margin-left: 27;" autocomplete="off" ></asp:TextBox>
                            <asp:TextBox ID="TxtIdCliente" runat="server" Width="40px" Visible="false" Style="text-transform: uppercase; margin-left: 27;" autocomplete="off"></asp:TextBox>
                             <asp:TextBox ID="TxtIdAgntAduanas" runat="server" Width="40px" Visible="false" Style="text-transform: uppercase; margin-left: 27;" autocomplete="off"></asp:TextBox>
                        </td>
                        <td >

                        </td>
                        <td></td>
                    </tr>--%>
                    
                    </table>
                        </ContentTemplate>
                    </ajax:UpdatePanel>


                </td>

         </tr>

     </table>
       <table class="form_bandeja" style=" width: 100%; height: 200px; margin-top:5px;">
           <tr>
            <td class="form_titulo" style="width: 85%">
                <asp:TextBox ID="TextBox10" Text="Detalle de servicios" ReadOnly="true" 
                    style="background-color: #0069ae; border: #0069ae; color: #FFFFFF; font-family: Calibri; font-size: 13px; font-weight: bold; text-transform: uppercase;" 
                    runat="server" Width="231px" ></asp:TextBox>
            </td> 
           <%-- <td  style="width: 85%">
                 <asp:Button ID="btnCakcular"  runat="server" Text="Ver detalles de servicios" class="form-control" BackColor="#0069ae" ForeColor="White" OnClick="btnCakcular_Click"/>
            </td> --%>
        </tr>
         
         <tr>
              <td colspan="5">
                <ajax:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional" >
                    <ContentTemplate>                         
                        <div style="overflow: auto; width: 100%; height: 250px;">
                            <asp:GridView ID="GvListaServicios" runat="server" AutoGenerateColumns="False"
                                SkinID="GrillaConsulta" Width="100%"  DataKeyNames="sDescripcionServicio,iIdOrdSer,iIdOrdSerDet,iIdTarifa,dCantidad,dPeso,dIgv,dAfecto,dInafecto,dMontoBruto,dMontoNeto,dDescuento,dDetraccion,sSituacion,
                                dMontoUnitario,dTotal,iIdDocOriDet,sOrigenPqteSLI,sMarcaSLI,dAfectoGrabar,dInafectoGrabar,dDescuentoGrabar,ddetraccionGrabar,dIgvGrabar,dMontoTotalGrabar,dUnitarioGrabar,sTipoMoneda,idMonedaGrabar,
                                dAfectoGrabar2,dInafectoGrabar2,dDescuentoGrabar2,ddetraccionGrabar2,dIgvGrabar2,dMontoTotalGrabar2,dUnitarioGrabar2,idMonedaGrabar2" OnRowDataBound="GvListaServicios_RowDataBound" >
                                <Columns>
                                    <asp:TemplateField>
                                                       <%-- <HeaderTemplate>
                                                            <asp:CheckBox runat="server" ID="chkAllQuitarGrp" onclick="SelectGroupChecks(this.id)"
                                                                AutoPostBack="True" />
                                                        </HeaderTemplate>--%>
                                                        <ItemTemplate>
                                                            <asp:CheckBox ID="chkSeleccionarServicio" runat="server"/>
                                                        </ItemTemplate>
                                                        <ItemStyle Width="1%" />
                                    </asp:TemplateField>
                                     <asp:BoundField DataField="iIdOrdSer" HeaderText="Nro OrdSer" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5px" Visible="false">                                        
                                    </asp:BoundField>
                                     <asp:BoundField DataField="iItem" HeaderText="Item" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5px" Visible="false">                                       
                                    </asp:BoundField>
                                    <asp:BoundField DataField="sDescripcionServicio" HeaderText="     Servicio     " ItemStyle-HorizontalAlign="left" ItemStyle-Width="200px">                                       
                                    </asp:BoundField>
                                    <asp:BoundField DataField="sContenedor" HeaderText=" Cont. / Chasis " ItemStyle-HorizontalAlign="Center" ItemStyle-Width="20px">                                       
                                    </asp:BoundField>
                                     <asp:BoundField DataField="sFechaIngreso" HeaderText="Fecha Ingreso" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5px">                                       
                                    </asp:BoundField> 
                                     <asp:BoundField DataField="sCondCarga" HeaderText="IMO" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5px">                                       
                                    </asp:BoundField>
                                     <asp:BoundField DataField="sTipoCarga" HeaderText="Carga" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5px">                                       
                                    </asp:BoundField>
                                    <asp:BoundField DataField="sTipoContenedor" HeaderText="Tipo Cont." ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5px">                                       
                                    </asp:BoundField>
                                     <asp:BoundField DataField="iIdTamanoContenedor" HeaderText="Tamaño" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5px">                                       
                                    </asp:BoundField>
                                     <asp:BoundField DataField="sEmbalaje" HeaderText="Emb." ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5px" Visible="false">                                       
                                    </asp:BoundField>
                                      <asp:BoundField DataField="sTipoMoneda" HeaderText="Moneda" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5px">                                       
                                    </asp:BoundField>
                                    <asp:BoundField DataField="dMontoUnitario" HeaderText="Tarifa" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5px">                                       
                                    </asp:BoundField>
                                     <asp:BoundField DataField="dCantidad" HeaderText="Cantidad" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5px">                                       
                                    </asp:BoundField>                                   
                                    <asp:BoundField DataField="dAfecto" HeaderText="Sub Total" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5px">                                       
                                    </asp:BoundField>
                                     <asp:BoundField DataField="dInafecto" HeaderText="Inafecto" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5px" Visible="false">                                       
                                    </asp:BoundField>
                                     <asp:BoundField DataField="dDescuento" HeaderText="Dscto." ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5px">                                       
                                    </asp:BoundField>
                                    <asp:BoundField DataField="dIgv" HeaderText="IGV" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5px" Visible="false">                                       
                                    </asp:BoundField>
                                     <asp:BoundField DataField="dTotal" HeaderText="Total" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5px">                                       
                                    </asp:BoundField>
                                     <asp:BoundField DataField="dDetraccion" HeaderText="Det." ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5px" Visible="false">                                       
                                    </asp:BoundField>
                                    <asp:BoundField DataField="sModalidad" HeaderText="Modalidad" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5px" Visible="false">                                       
                                    </asp:BoundField>
                                     <asp:BoundField DataField="sOrigen" HeaderText="Origen" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5px" Visible="false">                                       
                                    </asp:BoundField>
                                    
                                    <asp:BoundField DataField="iIdDocOriDet" HeaderText="IdDocOriDet" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5px" Visible="false">                                       
                                    </asp:BoundField> 

                                     <asp:BoundField DataField="dAfectoGrabar" HeaderText="" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5px" Visible="false">                                       
                                    </asp:BoundField>  
                                    <asp:BoundField DataField="dInafectoGrabar" HeaderText="" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5px" Visible="false">                                       
                                    </asp:BoundField>  
                                    <asp:BoundField DataField="dDescuentoGrabar" HeaderText="" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5px" Visible="false">                                       
                                    </asp:BoundField>  
                                    <asp:BoundField DataField="ddetraccionGrabar" HeaderText="" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5px" Visible="false">                                       
                                    </asp:BoundField>  
                                    <asp:BoundField DataField="dIgvGrabar" HeaderText="" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5px" Visible="false">                                       
                                    </asp:BoundField>  
                                    <asp:BoundField DataField="dMontoTotalGrabar" HeaderText="a" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5px" Visible="true">                                       
                                    </asp:BoundField>  
                                    <asp:BoundField DataField="dUnitarioGrabar" HeaderText="" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5px" Visible="false">                                       
                                    </asp:BoundField>  
                                     <asp:BoundField DataField="idMonedaGrabar" HeaderText="" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5px" Visible="false">                                       
                                    </asp:BoundField>  

                                      <asp:BoundField DataField="dAfectoGrabar2" HeaderText="" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5px" Visible="false">                                       
                                    </asp:BoundField>  
                                    <asp:BoundField DataField="dInafectoGrabar2" HeaderText="" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5px" Visible="false">                                       
                                    </asp:BoundField>  
                                    <asp:BoundField DataField="dDescuentoGrabar2" HeaderText="" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5px" Visible="false">                                       
                                    </asp:BoundField>  
                                    <asp:BoundField DataField="ddetraccionGrabar2" HeaderText="" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5px" Visible="false">                                       
                                    </asp:BoundField>  
                                    <asp:BoundField DataField="dIgvGrabar2" HeaderText="" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5px" Visible="false">                                       
                                    </asp:BoundField>  
                                    <asp:BoundField DataField="dMontoTotalGrabar2" HeaderText="" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5px" Visible="false">                                       
                                    </asp:BoundField>  
                                   
                                     <asp:BoundField DataField="idMonedaGrabar2" HeaderText="" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5px" Visible="false">                                       
                                    </asp:BoundField> 
                                    
                                </Columns>
                            </asp:GridView>
                        </div>
                    </ContentTemplate>    
                     <Triggers>                        
                     <ajax:PostBackTrigger ControlID="GvListaServicios" />            
                        <ajax:AsyncPostBackTrigger ControlID="ImgBtnBuscar" EventName="Click" />     
<asp:PostBackTrigger ControlID="GvListaServicios"></asp:PostBackTrigger>
<asp:AsyncPostBackTrigger ControlID="ImgBtnBuscar" EventName="Click"></asp:AsyncPostBackTrigger>
                         <%--<asp:AsyncPostBackTrigger ControlID="ImgBtnGrabar" EventName="Click"></asp:AsyncPostBackTrigger>--%>
                         <asp:AsyncPostBackTrigger ControlID="ImgBtnGrabar" EventName="Click"></asp:AsyncPostBackTrigger>
                    </Triggers>
                </ajax:UpdatePanel>
            </td>                        
                     </tr>
          </table>
          <%--   </td>
             </tr>

          </table>--%>

     <table class="form_bandeja" style=" width: 100%;">
           <tr>
            <td class="form_titulo" style="width: 85%">
                <asp:TextBox ID="TextBox11" Text="Detalle de servicios de almacenaje" ReadOnly="true" 
                    style="background-color: #0069ae; border: #0069ae; color: #FFFFFF; font-family: Calibri; font-size: 13px; font-weight: bold; text-transform: uppercase;" 
                    runat="server" Width="231px" ></asp:TextBox>
            </td> 
        </tr>
         
          <tr>
           <td colspan="5">
                <ajax:UpdatePanel ID="upDocumentoOrigen" runat="server" UpdateMode="Conditional" >
                    <ContentTemplate>                         
                        <div style="overflow: auto; width: 100%; height: 150px;">
                            <asp:GridView ID="GrvListadoAlmacenaje" runat="server" AutoGenerateColumns="False"
                                SkinID="GrillaConsulta" Width="100%"  DataKeyNames="iIdOrdSer,iIdOrdSerDet,iIdTarifa,sDescripcionServicio,dCantidad,dPeso,dIgv,dAfecto,dInafecto,dMontoBruto,dMontoNeto,dDescuento,
                                dDetraccion,sSituacion,dMontoUnitario,dTotal,iIdDocOriDet,sOrigenPqteSLI,sMarcaSLI,dAfectoGrabar,dInafectoGrabar,dDescuentoGrabar,ddetraccionGrabar,dIgvGrabar,dMontoTotalGrabar,dUnitarioGrabar,sTipoMoneda,idMonedaGrabar,
                                dAfectoGrabar2,dInafectoGrabar2,dDescuentoGrabar2,ddetraccionGrabar2,dIgvGrabar2,dMontoTotalGrabar2,dUnitarioGrabar2,idMonedaGrabar2" OnRowDataBound="GrvListadoAlmacenaje_RowDataBound" >
                                <Columns>
                                     <asp:TemplateField>
                                                       <%-- <HeaderTemplate>
                                                            <asp:CheckBox runat="server" ID="chkAllQuitarGrp" onclick="SelectGroupChecks(this.id)"
                                                                AutoPostBack="True" />
                                                        </HeaderTemplate>--%>
                                                        <ItemTemplate>
                                                            <asp:CheckBox ID="chkSeleccionarAlmacenaje" runat="server"/>
                                                        </ItemTemplate>
                                                        <ItemStyle Width="1%" />
                                    </asp:TemplateField>
                                     <asp:BoundField DataField="iIdOrdSer" HeaderText="Nro OrdSer" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5px" Visible="false">                                       
                                    </asp:BoundField>
                                     <asp:BoundField DataField="iItem" HeaderText="Item" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5px" Visible="false">                                       
                                    </asp:BoundField>
                                     <asp:BoundField DataField="sDescripcionServicio" HeaderText="     Servicio     " ItemStyle-HorizontalAlign="left" ItemStyle-Width="200px">                                       
                                    </asp:BoundField>
                                    <asp:BoundField DataField="sContenedor" HeaderText=" Cont. / Chasis " ItemStyle-HorizontalAlign="Center" ItemStyle-Width="20px"> 
                                        
                                    </asp:BoundField>
                                     <asp:BoundField DataField="sFechaIngreso" HeaderText="Fecha Ingreso" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5px">                                       
                                    </asp:BoundField> 
                                     <asp:BoundField DataField="sCondCarga" HeaderText="IMO" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5px">                                       
                                    </asp:BoundField>
                                     <asp:BoundField DataField="sTipoCarga" HeaderText="Carga" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5px">                                       
                                    </asp:BoundField>
                                    <asp:BoundField DataField="sTipoContenedor" HeaderText="Tipo Cont." ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5px">                                       
                                    </asp:BoundField>
                                     <asp:BoundField DataField="iIdTamanoContenedor" HeaderText="Tamaño" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5px">                                       
                                    </asp:BoundField>
                                     <asp:BoundField DataField="sEmbalaje" HeaderText="Emb." ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5px" Visible="false">                                       
                                    </asp:BoundField>
                                     <asp:BoundField DataField="sTipoMoneda" HeaderText="Moneda" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5px">                                       
                                    </asp:BoundField>
                                    <asp:BoundField DataField="dMontoUnitario" HeaderText="Tarifa" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5px">                                       
                                    </asp:BoundField>
                                     <asp:BoundField DataField="dCantidad" HeaderText="Cantidad" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5px">                                       
                                    </asp:BoundField>
                                    
                                    <asp:BoundField DataField="dAfecto" HeaderText="Sub Total" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5px">                                       
                                    </asp:BoundField>
                                     <asp:BoundField DataField="dInafecto" HeaderText="Inafecto" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5px" Visible="false">                                       
                                    </asp:BoundField>
                                     <asp:BoundField DataField="dDescuento" HeaderText="Dscto." ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5px">                                       
                                    </asp:BoundField>
                                    <asp:BoundField DataField="dIgv" HeaderText="IGV" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5px" Visible="false">                                       
                                    </asp:BoundField>
                                     <asp:BoundField DataField="dTotal" HeaderText="Total" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5px">                                       
                                    </asp:BoundField>
                                     <asp:BoundField DataField="dDetraccion" HeaderText="Det." ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5px" Visible="false">                                       
                                    </asp:BoundField>
                                    <asp:BoundField DataField="sModalidad" HeaderText="Modalidad" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5px" Visible="false">                                       
                                    </asp:BoundField>
                                     <asp:BoundField DataField="sOrigen" HeaderText="Origen" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5px" Visible="false" >                                       
                                    </asp:BoundField>
                                                                    
                                    <asp:BoundField DataField="iIdDocOriDet" HeaderText="IdDocOriDet" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5px" Visible="false">                                       
                                    </asp:BoundField>  
                                    
                                    <asp:BoundField DataField="dAfectoGrabar" HeaderText="" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5px" Visible="false">                                       
                                    </asp:BoundField>  
                                    <asp:BoundField DataField="dInafectoGrabar" HeaderText="" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5px" Visible="false">                                       
                                    </asp:BoundField>  
                                    <asp:BoundField DataField="dDescuentoGrabar" HeaderText="" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5px" Visible="false">                                       
                                    </asp:BoundField>  
                                    <asp:BoundField DataField="ddetraccionGrabar" HeaderText="" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5px" Visible="false">                                       
                                    </asp:BoundField>  
                                    <asp:BoundField DataField="dIgvGrabar" HeaderText="" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5px" Visible="false">                                       
                                    </asp:BoundField>  
                                    <asp:BoundField DataField="dMontoTotalGrabar" HeaderText="" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5px" Visible="true">                                       
                                    </asp:BoundField>  
                                    <asp:BoundField DataField="dUnitarioGrabar" HeaderText="" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5px" Visible="false">                                       
                                    </asp:BoundField>  
                                     <asp:BoundField DataField="idMonedaGrabar" HeaderText="" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5px" Visible="false">                                       
                                    </asp:BoundField>
                                     <asp:BoundField DataField="dUnitarioGrabar2" HeaderText="" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5px" Visible="false">                                       
                                    </asp:BoundField>  
                                    
                                     <asp:BoundField DataField="dAfectoGrabar2" HeaderText="" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5px" Visible="false">                                       
                                    </asp:BoundField>  
                                    <asp:BoundField DataField="dInafectoGrabar2" HeaderText="" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5px" Visible="false">                                       
                                    </asp:BoundField>  
                                    <asp:BoundField DataField="dDescuentoGrabar2" HeaderText="" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5px" Visible="false">                                       
                                    </asp:BoundField>  
                                    <asp:BoundField DataField="ddetraccionGrabar2" HeaderText="" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5px" Visible="false">                                       
                                    </asp:BoundField>  
                                    <asp:BoundField DataField="dIgvGrabar2" HeaderText="" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5px" Visible="false">                                       
                                    </asp:BoundField>  
                                    <asp:BoundField DataField="dMontoTotalGrabar2" HeaderText="" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5px" Visible="false">                                       
                                    </asp:BoundField>  
                                   
                                     <asp:BoundField DataField="idMonedaGrabar2" HeaderText="" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5px" Visible="false">                                       
                                    </asp:BoundField> 
                                </Columns>
                            </asp:GridView>
                        </div>
                    </ContentTemplate> 
                    <Triggers>
                     <ajax:PostBackTrigger ControlID="GrvListadoAlmacenaje" />            
                        <ajax:AsyncPostBackTrigger ControlID="ImgBtnBuscar" EventName="Click" />                      
                        <asp:PostBackTrigger ControlID="GrvListadoAlmacenaje"></asp:PostBackTrigger>
                        <asp:AsyncPostBackTrigger ControlID="ImgBtnBuscar" EventName="Click"></asp:AsyncPostBackTrigger>
                        <%--<asp:AsyncPostBackTrigger ControlID="ImgBtnGrabar" EventName="Click"></asp:AsyncPostBackTrigger>--%>
                        <asp:AsyncPostBackTrigger ControlID="ImgBtnGrabar" EventName="Click"></asp:AsyncPostBackTrigger>
                    </Triggers>
                </ajax:UpdatePanel>
            </td>
         </tr>
         </table>
    
     <table class="form_bandeja" style="width: 100%; margin-top: 3px;">
         <tr>           
             <td colspan="5">
                  <ajax:UpdatePanel ID="UpdatePanel3" runat="server" UpdateMode="Conditional" >
                    <ContentTemplate> 
                    <table class="form_bandeja" style="width: 90%; margin-top: 1px; margin-left:10px;">
                      
                      <tr>                         
                         <td style="width: 10%">  
                               <asp:TextBox ID="HdtxtAfecto" runat="server" MaxLength="4" Style="text-transform: uppercase"
                                Width="75px" Visible="false"></asp:TextBox>
                         <asp:TextBox ID="HdtxtDescuento" runat="server" MaxLength="4" Style="text-transform: uppercase"
                                Width="75px" Visible="false"></asp:TextBox>
                             <asp:TextBox ID="HdtxtIGV" runat="server" MaxLength="4" Style="text-transform: uppercase"
                                Width="75px" Visible="false"></asp:TextBox>
                              <asp:TextBox ID="txtTotalHd" runat="server" MaxLength="4" Style="text-transform: uppercase"
                                Width="75px" Visible="false"></asp:TextBox>
                         </td>
                         <td style="width: 20px;">
                          
                         </td>
                     </tr>
                     <tr>
                         <td style="width: 10%"> 
                         </td>
                         <td style="width: 13px;">
                            
                              
                         </td>
                     </tr>
                     <tr>
                         <td style="width: 10%"> 
                         </td>
                         <td style="width: 13px;">
                            
                         </td>
                     </tr>
                      <tr>
                         <td style="width: 9%">   
                         </td>
                         <td style="width: 20px;">
                           
                         </td>
                     </tr>
                     <tr>
                         <td style="width: 10%">   
                         </td>
                         <td style="width: 80px;">
                            
                         </td>
                     </tr>
                          
                      
                 </table>
                        </ContentTemplate>
                     </ajax:UpdatePanel>

              </td>
           <%--  <td style="width:10px;">
                    <div class="upTablaSubTotalSoles">
                 <ajax:UpdatePanel ID="upTablaSoles" runat="server" UpdateMode="Conditional" >
                    <ContentTemplate> 
                    <table class="form_bandeja" style="width: 100%; margin-top: 1px; border-collapse:separate; border:#0069AE 1px solid;">
                      <tbody style="border:black 1px solid;">
                       <tr style="border:black 1px solid;">                         
                         <td style="width: 98% ;">                            
                              <asp:Label ID="a" Width="90px" runat="server" Text=" Afecto  "
                                 Visible="true" CssClass="lblComisionistaa" Style="float:right;"></asp:Label>
                         </td>
                         <td style="width: 15%;">
                            <asp:Label ID="lblAfectoSoles" Width="90px" runat="server" Text=" - "
                                 Visible="true" CssClass="a" Style="float:left;"></asp:Label>
                         </td>
                     </tr>
                     <tr>
                         <td style="width: 10%">
                             <asp:Label ID="b" Width="90px" runat="server" Text=" Descuento "
                                 Visible="true" CssClass="aa" Style="float:right;"></asp:Label>
                         </td>
                         <td style="width: 13px;">
                              <asp:Label ID="lblDescuentoSoles"  Width="90px" runat="server" Text=" - "
                                 Visible="true" CssClass="aaa"></asp:Label>
                         </td>
                     </tr>
                     <tr>
                         <td style="width: 10%">
                             <asp:Label ID="c" Width="90px" runat="server" Text=" IGV "
                                 Visible="true" CssClass="aaaa" Style="float:right;"></asp:Label>
                         </td>
                         <td style="width: 13px;">
                             <asp:Label ID="lblIgvSoles"  Width="90px" runat="server" Text=" - "
                                 Visible="true" CssClass="aaaaa"></asp:Label>
                         </td>
                     </tr>
                      <tr>
                         <td style="width: 9%">
                             <asp:Label ID="d" Width="90px" runat="server" Text=" Sub Total S/ "
                                 Visible="true" CssClass="aaaaaa" Style="float:right;"></asp:Label>
                         </td>
                         <td style="width: 20px;">
                            <asp:Label ID="lblTotalSoles"  Width="90px" runat="server" Text=" - "
                                 Visible="true" CssClass="aaaaaaa"></asp:Label>
                         </td>
                     
                            <asp:HiddenField ID="Hd_Afecto" runat="server" />
                            <asp:HiddenField ID="Hd_Inafecto" runat="server" />
                            <asp:HiddenField ID="Hd_Igv" runat="server" />
                            <asp:HiddenField ID="Hd_Descuento" runat="server" />
                            <asp:HiddenField ID="Hd_Total" runat="server" />

                           <asp:HiddenField ID="Hd_AfectoTotalSoles" runat="server" />
                            <asp:HiddenField ID="Hd_InafectoTotalSoles" runat="server" />
                            <asp:HiddenField ID="Hd_IgvTotalSoles" runat="server" />
                            <asp:HiddenField ID="Hd_DescuentoTotalSoles" runat="server" />
                            <asp:HiddenField ID="Hd_TotalSoles" runat="server" />
                          
                     </tr>  
                      </tbody>
                 </table>
                        </ContentTemplate>
                     </ajax:UpdatePanel>
                 </div>
             </td>--%>
             <td style="width:10px;">
                 
                 <div class="upTablaSubTotalDolares">
                 <ajax:UpdatePanel ID="upSubTotalDolares" runat="server" UpdateMode="Conditional" >
                    <ContentTemplate> 
                    <table class="form_bandeja" style="width: 100%; margin-top: 1px; border-collapse:separate; border:#0069AE 1px solid;">
                      <tbody style="border:black 1px solid;">
                       <tr style="border:black 1px solid;">                         
                         <td style="width: 98% ;">
                            <%--  <asp:TextBox ID="HdtxtAfecto" runat="server" MaxLength="4" Style="text-transform: uppercase"
                                Width="75px" Visible="false"></asp:TextBox>
                         <asp:TextBox ID="HdtxtDescuento" runat="server" MaxLength="4" Style="text-transform: uppercase"
                                Width="75px" Visible="false"></asp:TextBox>
                             <asp:TextBox ID="HdtxtIGV" runat="server" MaxLength="4" Style="text-transform: uppercase"
                                Width="75px" Visible="false"></asp:TextBox>
                              <asp:TextBox ID="txtTotalHd" runat="server" MaxLength="4" Style="text-transform: uppercase"
                                Width="75px" Visible="false"></asp:TextBox>--%>
                              <asp:Label ID="lblAfect" Width="90px" runat="server" Text=" Afecto "
                                 Visible="true" CssClass="lblComisionistaa" Style="float:right;"></asp:Label>
                         </td>
                         <td style="width: 15%;">
                            <asp:Label ID="lblAfecto" Width="90px" runat="server" Text=" - "
                                 Visible="true" CssClass="lblComisionista" Style="float:left;"></asp:Label>
                         </td>
                     </tr>
                     <tr>
                         <td style="width: 10%">
                             <asp:Label ID="lbl" Width="90px" runat="server" Text=" Descuento  "
                                 Visible="true" CssClass="lblComisionista" Style="float:right;"></asp:Label>
                         </td>
                         <td style="width: 13px;">
                              <asp:Label ID="lblDescuento"  Width="90px" runat="server" Text=" - "
                                 Visible="true" CssClass="lblComisionista"></asp:Label>
                         </td>
                     </tr>
                     <tr>
                         <td style="width: 10%">
                             <asp:Label ID="lbla" Width="90px" runat="server" Text=" IGV "
                                 Visible="true" CssClass="lblComisionista" Style="float:right;"></asp:Label>
                         </td>
                         <td style="width: 13px;">
                             <asp:Label ID="lblIgv"  Width="90px" runat="server" Text=" - "
                                 Visible="true" CssClass="lblComisionista"></asp:Label>
                         </td>
                     </tr>
                      <tr>
                         <td style="width: 9%">
                             <asp:Label ID="lblTotalNombre" Width="90px" runat="server" Text="Total $ "
                                 Visible="true" CssClass="lblComisionista" Style="float:right; font-weight:bold;"></asp:Label>
                         </td>
                         <td style="width: 20px;">
                            <asp:Label ID="lblTotal1"  Width="90px" runat="server" Text=" -  "
                                 Visible="true"  Style="font-weight:bold;"></asp:Label>
                         </td>
                          <%--<td>
                              <button id="close_account">Show</button>
                          </td>--%>
                         
                     </tr>
                           <tr>
                         <td style="width: 9%">
                             <asp:Label ID="lblTotalNombre2" Width="90px" runat="server" Text=" Sub Total S/ "
                                 Visible="true" CssClass="lblComisionista" Style="float:right;"></asp:Label>
                         </td>
                         <td style="width: 20px;">
                            <asp:Label ID="lblTotal2"  Width="90px" runat="server" Text=" - "
                                 Visible="true" CssClass="lblComisionista"></asp:Label>
                         </td>

                               <asp:HiddenField ID="Hd_Afecto" runat="server" />
                            <asp:HiddenField ID="Hd_Inafecto" runat="server" />
                            <asp:HiddenField ID="Hd_Igv" runat="server" />
                            <asp:HiddenField ID="Hd_Descuento" runat="server" />
                            <asp:HiddenField ID="Hd_Total" runat="server" />

                           <asp:HiddenField ID="Hd_AfectoTotalSoles" runat="server" />
                            <asp:HiddenField ID="Hd_InafectoTotalSoles" runat="server" />
                            <asp:HiddenField ID="Hd_IgvTotalSoles" runat="server" />
                            <asp:HiddenField ID="Hd_DescuentoTotalSoles" runat="server" />
                            <asp:HiddenField ID="Hd_TotalSoles" runat="server" />
                          <%--<td>
                              <button id="close_account">Show</button>
                          </td>--%>
                         
                     </tr>
                 
                          
                          
                      </tbody>
                 </table>
                        </ContentTemplate>
                     </ajax:UpdatePanel>
                       </div>
             </td>

         </tr>
         <tr>
             <td colspan="5" style="height:50%;">
                  <ajax:UpdatePanel ID="UpdatePanel6" runat="server" UpdateMode="Conditional" >
                    <ContentTemplate> 
                 <div style="height:100%;">
                    <div style="position: relative;height:10%;">
                         <asp:Label ID="lblTipoCambioo" Width="170px" runat="server" Text="   "
                                 Visible="false" CssClass="f" Style="float:left; margin-left:30px;  margin-top: 14%;"></asp:Label>
                    </div>
                     </div>
                        </ContentTemplate>
                     </ajax:UpdatePanel>

              </td>
            
         </tr>
       
         </table>
    <table class="form_bandeja" style="width: 100%;">
         <tr>
            <td style="width:100%;">
                 <ajax:updatepanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional" >
                    <ContentTemplate> 
                 <table class="form_bandeja" style="width: 100%; height:15px;">
                     <tr>
                         <td style="width:400px;">
                              <asp:Label ID="lblOpcion" Width="130px" runat="server" Text="  - "
                                 Visible="true" CssClass="f" Style="float:left;"></asp:Label>
                         </td> 
                         <td style="width:400px;">
                              <asp:Label ID="lblTipoCambio" Width="130px" runat="server" Text="  - "
                                 Visible="true" CssClass="f" Style="float:right;"></asp:Label>
                         </td>                       
                     </tr>

                 </table>
                        </ContentTemplate>
                     </ajax:updatepanel>

                
            </td>
            <%--<td style="width:400px;">
                 Registros seleccionados : <asp:Label ID="lblRegistrosSelecc" runat="server" Text="0"  Visible="true"></asp:Label>
            </td>
            <td style="width:400px;">
                 Importe seleccionado : <asp:Label ID="lblImportSelec" runat="server" Text="0"  Visible="true"></asp:Label>
            </td>--%>
        </tr>
    </table>

    
    
    

    <%--<asp:Panel ID="pnlCondiciones" runat="server" CssClass="PanelPopup" Width="307px" Height="260px" BorderStyle="Groove">
              <div  class="panel-heading" runat="server"  style="width:300px" >      
            <ajax:UpdatePanel ID="udpCondiciones" UpdateMode="Conditional" runat="server">               
                <ContentTemplate>    
                 
                    <table style="width: 100%; border-radius:100px; background: rgb(255, 255, 255);" class="tablee"  >
                        <tr>                          
                            <td  style="width: 100%; border-radius:50px; background:#0069ae;" >
                                 <div class="divTitle">
                             <label  runat="server" style="background-color: #0069ae; color: #FFFFFF; font-family: Calibri; 
                                            font-size: 20px; font-weight: bold; text-transform: uppercase; text-align:center; " >IMPORTANTE </label>
                                    
                                     </div>
                                </td>
                        </tr>
                    </table>
                    <table  style="width: 100%;" class="table">
                        <tr class="info" style="width: 85%;height:50%;">                          
                            <td  style="width: 85%;height:50%;">   
                                <div class="swal2-content"> 
                                <p style="font-size:15px; margin-left:0%; font-family: Calibri; color:#004b8e;"> 
                                         Estimado usuario,
                                    de acuerdo a la operativa podrán cargarse servicios adicionales, que deberán ser oportunamente cancelados.
                                         De igual manera, debe verificar las tarifas presentadas y solo aceptar si es que está de acuerdo.                                     
                                      <br>

                                      </br>
                                    </div>
                                 <div class="divBon">
                                 <asp:Button ID="btnAceptarCondiciones" Style="font-family: Calibri; 
                                            font-size: 13px; text-align:center;"  runat="server" Text="Continuar" class="btn btn-primary active" BackColor="#0069ae"  OnClick="btnAceptarCondiciones_Click"/> 
                                    </div>
                                </p>
                              
                                
                            </td>
                         </tr>
                      
                    </table>
                 

                </ContentTemplate>
                 <Triggers>      
                     

                    </Triggers>
            </ajax:UpdatePanel>
              </div>
        </asp:Panel>--%>

        <%--<ajaxToolkit:ModalPopupExtender ID="mCondiciones" runat="server" DynamicServicePath=""
        Enabled="True" PopupControlID="pnlCondiciones" TargetControlID="btnCerrarCondicion" CancelControlID=""
        BackgroundCssClass="modalBackground">
    </ajaxToolkit:ModalPopupExtender>

       <asp:Button ID="btnCerrarCondicion" Style="display: none;" runat="server" Text="[Open Proxy]" />--%>

      

      <asp:HiddenField ID="vi_Validacion" runat="server" />

    </body>
        </html>

    
    <script language="javascript" type="text/javascript">

        var tiempo = 0;
        var seconds = 0;

//       function createButton(text, cb) {
//    return $('<asp:Button ID="Grabar"  runat="server" style="margin-left:20px;" Text="Aceptar" class="btn btn-primary active" BackColor="#0069ae"  OnClick="ImgBtnGrabar_Click" OnClientClick="return validar()" />').on('click', cb);
//    //
//        }

//    //        function createButton2(text, cb) {
//    //return $('<asp:Button ID="NoGrabar"  runat="server" style="margin-left:20px;" Text="No Acepto" class="btn btn-primary active"  OnClick="ImgBtnGrabar_Click" OnClientClick="return validar()" />').on('click', cb);
//    ////
//    //    }

//            function createButton3(text, cb) {
//    return $('<asp:Button ID="Cancel"  runat="server" style="margin-left:20px;" Text="Cancelar" class="btn btn-primary active"  BackColor="#0069ae"/>').on('click', cb);
//    //
//}

          
        var myVar = setInterval(function () {
            clearInterval(myVar);
        }, 1000);

        var myTiempo = setTimeout(function () {
            clearInterval(myTiempo);
        }, 1);

        function myStopFunction() {
            clearInterval(myVar);
            seconds = '<%=GlobalEntity.Instancia.TiempoDuracionSesion%>' * 60;
            tiempo = '<%=GlobalEntity.Instancia.TiempoDuracionSesion%>' * 1000 * 60;

            CuentaTiempo(tiempo);
        }

        function CuentaTiempo(tiempo) {
            clearTimeout(myTiempo);
            myTiempo = setTimeout(function () {
                document.getElementById('<%=btn_cerrar.ClientID%>').click();
                //window.location = "../Seguridad/eFENIX_Login.aspx";
            }, tiempo);
        }
     


        function SessionExpireAlert(timeout) {
            seconds = timeout / 1000;
            document.getElementsByName("seconds").innerHTML = seconds;

            myvar = setInterval(function () {
                seconds--;
                document.getElementById("seconds").innerHTML = seconds;
            }, 1000);

            CuentaTiempo(timeout);
        };


        var goodexit = false;
        window.onbeforeunload = confirmRegisterExit;
        function confirmRegisterExit(evt) {
            if (!goodexit) {
                //return "Si vas abandonar el sistema haz click en Cerrar Sesion!!";
                document.getElementById('<%=btn_cerrar.ClientID%>').click();
            }
        }

      <%--  function validar()
        {                     

            $("#ctl00_CntMaster_NoGrabar").click(function () {
             var a = "No";
            document.getElementById('<%= vi_Validacion.ClientID %>').value = a;
            document.getElementById('<%= Button1.ClientID %>').click();
                
            }); 

            $("#ctl00_CntMaster_Grabar").click(function () {
                    var a = "Si";
            document.getElementById('<%= vi_Validacion.ClientID %>').value = a;
            document.getElementById('<%= Button1.ClientID %>').click();
            });   

             $("#ctl00_CntMaster_Cancel").click(function () {
                    var a = "Cancel";
            document.getElementById('<%= vi_Validacion.ClientID %>').value = a;
            document.getElementById('<%= Button1.ClientID %>').click();
            });
            
        }--%>

     
    </script>



    </asp:Content>


<asp:Content ID="Content3" ContentPlaceHolderID="ContentCab" Runat="Server">
        
        <span class="texto_titulo" >Tu Sesion Expirara en &nbsp; 
            <span class="texto_titulo" id="seconds"> </span>
            <span>&nbsp;seg.</span>
         </span>
         <asp:Button ID="btn_cerrar" runat="server" Text="cerrar" 
            onclick="btn_cerrar_Click"  Style="display: none;" />
    </asp:Content>