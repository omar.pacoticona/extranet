﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using FENIX.BusinessLogic;
using FENIX.BusinessEntity;
using System.Web.UI.WebControls;
using System.Web.Security;
using System.Configuration;
using System.IO;
using System.Text.RegularExpressions;

public partial class Extranet_Operaciones_eFenix_Solicitud_Refrendo : System.Web.UI.Page
{

    public Int32 IdAgenAduana;
    protected void Page_Load(object sender, EventArgs e)
    {
        String tiempo = (Convert.ToInt32(GlobalEntity.Instancia.TiempoDuracionSesion.ToString()) * 1000 * 60).ToString();
        Session["Reset"] = true;

        ScriptManager scriptManager = ScriptManager.GetCurrent(this.Page);
        scriptManager.RegisterPostBackControl(this.imgBtnUplDua);

        scriptManager.RegisterPostBackControl(this.imgBtnUplBoking);
        scriptManager.RegisterPostBackControl(this.imgBtnUplTicekt);
        scriptManager.RegisterPostBackControl(this.imgBtnUplPackList);
        scriptManager.RegisterPostBackControl(this.imgBtnUplGuiaRem);
        scriptManager.RegisterPostBackControl(this.imgBtnUplTipoPago);
        IdAgenAduana = GlobalEntity.Instancia.IdCliente;

        //ClientScript.RegisterStartupScript(GetType(), "Mensaje", "Habilita()", true);


        if (!Page.IsPostBack)
        {
            IdAgenAduana = GlobalEntity.Instancia.IdCliente;
            listarDroplist();
            validarRUC();
            ListarDespachadores();


            ScriptManager.RegisterStartupScript(this, GetType(), "mensaje2", "HabilitaIsPostack();", true);
            DplTipoPago.Enabled = false;
            ScriptManager.RegisterStartupScript(this, GetType(), "mensaje", "SessionExpireAlert(" + tiempo + ");", true);
        }
        else
        {
            //ScriptManager.RegisterStartupScript(this, GetType(), "mensaje", "myStopFunction();", true);
            ScriptManager.RegisterStartupScript(this, GetType(), "mensaje", "SessionExpireAlert(" + tiempo + ");", true);
        }
    }

    private void SCA_MsgInformacion(string strError)
    {
        MensajeScript(strError);
    }

    private void MensajeScript(string SMS)
    {
        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "", "<script>alert('" + SMS + "');</script>", false);


    }

    protected void btn_cerrar_Click(object sender, EventArgs e)
    {
        if (GlobalEntity.Instancia.CerrarExtranet == "S")
        {
            BL_Usuario oBL_Usuario = new BL_Usuario();
            oBL_Usuario.GrabaInicioSesion(GlobalEntity.Instancia.Usuario, "0");
            Session.Abandon();
            FormsAuthentication.SignOut();
            Response.Redirect("../../eFENIX_Login.aspx");
            Response.End();
        }
    }

    Tuple<string, int> cargarArchivo(FileUpload fileUpload, string IdAgenAduana)
    {
        string ruta = ConfigurationManager.AppSettings["Refrendo_Solicitud"] + IdAgenAduana + "/";

        string filename = "";
        int estado = 0;
        string fecha = DateTime.Now.ToString("ddMMyyHmmss");
        try
        {
            if (fileUpload.HasFile)
            {
                if (fileUpload.PostedFile.ContentType == "application/pdf")
                {
                    //COMPROBAR SI EXISTE RUTA 
                    if (!File.Exists(ruta))
                    {
                        //SI NO EXISTE CREAR LA RUTA
                        DirectoryInfo directory = Directory.CreateDirectory(ruta);
                    }


                    //GUARDA INFORMACION
                    if (fileUpload.FileName.Length >= 100)
                        filename = fileUpload.FileName.Substring(0, 100);
                    else
                        filename = fileUpload.FileName;
                    filename = Path.GetFileName(fecha + "_" + filename);
                    fileUpload.SaveAs(ruta + filename);
                    //file.read();
                    estado = 1;
                }
                else
                {
                    estado = 2;
                }
            }

        }
        catch (Exception ex)
        {
            //MensajeScript("eror de upload");
            estado = 0;
        }

        return Tuple.Create(filename, estado);
    }

    protected void imgBtnUplDua_Click(object sender, ImageClickEventArgs e)
    {
        //ScriptManager scriptManager = ScriptManager.GetCurrent(this.Page);
        //scriptManager.RegisterPostBackControl(this.imgBtnUplDua);
        Int32 IdCliente = GlobalEntity.Instancia.IdCliente;
        BE_Refrendo oBE_Refrendo = new BE_Refrendo();
        BL_Refrendo oBL_Refrendo = new BL_Refrendo();
        var a = IdAgenAduana;

        if (GlobalEntity.Instancia.Usuario == null || GlobalEntity.Instancia.Usuario == "")
        {
            SCA_MsgInformacion("Acaba de perder la session. Vuelva loguearse.");
            return;
        }


        var result = cargarArchivo(FileUploadDua, IdCliente.ToString());

        if (result.Item2 == 1)
        {
            lblUplDua.Text = FileUploadDua.FileName;
            UploadIconoEnabled(1, 1);
            //Int32 IdCliente = GlobalEntity.Instancia.IdCliente;
            oBE_Refrendo.IdCliente = IdCliente;
            oBE_Refrendo.sNombreArchivo = FileUploadDua.FileName;
            oBE_Refrendo.sRutaArchivo = result.Item1;
            oBE_Refrendo.iIdusuario = 0;
            oBE_Refrendo.sUsuario = GlobalEntity.Instancia.Usuario;
            oBE_Refrendo.iIdTipoDocumento = 1; //DUA
            var res = oBL_Refrendo.Extranet_Ins_ArchivosRefrendo(oBE_Refrendo);

            lblUplBLID.Text = res.Item2;
            if (lblUplBLID.Text != "")
            {
                imgBtnUplDua.ImageUrl = "../../Script/Imagenes/checkTrueFactura.png";
                imgBtnUplDua.Width = 22;
                imgBtnUplDua.Enabled = false;
            }
            ScriptManager.RegisterStartupScript(this, GetType(), "mensaje2", "HabilitaPostPostack();", true);

        }
        else if (result.Item2 == 2)
        {
            MensajeScript("Ingrese un archivo en Formato PDF");
            ScriptManager.RegisterStartupScript(this, GetType(), "mensaje2", "HabilitaPostPostack();", true);
        }
    }

    protected void ImgBtnBuscar_Click(object sender, EventArgs e)
    {
        try
        {


            BE_Refrendo oBE_Refrendo = new BE_Refrendo();
            BL_Refrendo oBL_Refrendo = new BL_Refrendo();
            BE_Refrendo oBE_DocDetalle = new BE_Refrendo();
            List<BE_Refrendo> List_Refrendo = new List<BE_Refrendo>();
            List<BE_Refrendo> List_DocDetalle = new List<BE_Refrendo>();
            Int32 IdCliente = GlobalEntity.Instancia.IdCliente;

            if (txtBooking.Text == "")
            {
                SCA_MsgInformacion("Ingrese el booking.");
            }

            oBE_Refrendo.sBooking = txtBooking.Text;
            oBE_Refrendo.IdCliente = IdCliente;

            List_Refrendo = oBL_Refrendo.ListarDatosLiquidacion(oBE_Refrendo);

            if (List_Refrendo.Count() != 0)
            {
                txtIdExportador.Text = List_Refrendo[0].IdExportador.ToString();
                txtExportador.Text = List_Refrendo[0].sExportador.ToString();
                txtIdAgenciaAduanas.Text = List_Refrendo[0].IdAgenciaAguanas.ToString();
                txtAgeniaAduanas.Text = List_Refrendo[0].sAgenciaAduanas.ToString();
                txtNave.Text = List_Refrendo[0].Nave.ToString();
                txtIdNave.Text = List_Refrendo[0].IdNave.ToString();
                txtViaje.Text = List_Refrendo[0].sViaje.ToString();
                txtRumbo.Text = List_Refrendo[0].sRumbo.ToString();
                txtIdDocOri.Text = List_Refrendo[0].IidDocori.ToString();
                txtIdBooking.Text = List_Refrendo[0].IdBooking.ToString();
                txtDtCargaSeca.Text = List_Refrendo[0].dtCargaSeca.ToString();
                txtRefrigerante.Text = List_Refrendo[0].dtCargaRefrigerada.ToString();
                txtIdCondicionTransmitir.Text = List_Refrendo[0].iIdCondicionTransmitir.ToString();

                if (List_Refrendo[0].iIdCondicionTransmitir == 3)
                {
                    oBE_DocDetalle.sBooking = txtBooking.Text;
                    List_DocDetalle = oBL_Refrendo.ListarDetalleDocumento(oBE_DocDetalle);
                    if (List_DocDetalle.Count() > 0)
                    {
                        //GrvListadoDetalle.DataSource = List_DocDetalle;
                        //GrvListadoDetalle.DataBind();
                        //upDetalleDocOriDet.Update();
                        //ScriptManager.RegisterStartupScript(this, GetType(), "mensaje2", "MostrarDiv();", true); //MOSTRAR EL DETALLE. OcultarDiv

                        if (List_Refrendo[0].iIdCondicionTransmitir == 3) // FCL/FCL
                        {
                            GrvListadoDetalle.DataSource = List_DocDetalle;
                            GrvListadoDetalle.DataBind();
                            upDetalleDocOriDet.Update();
                            ScriptManager.RegisterStartupScript(this, GetType(), "mensaje2", "MostrarDiv();", true); //MOSTRAR EL DETALLE. OcultarDiv
                        }
                        else
                        {
                            List<BE_Refrendo> oLista_Refrendo = new List<BE_Refrendo>();
                            BE_Refrendo oBE_RefrendoV2 = new BE_Refrendo();
                            oLista_Refrendo = null;
                            GrvListadoDetalle.DataSource = oLista_Refrendo;
                            GrvListadoDetalle.DataBind();
                            upDetalleDocOriDet.Update();
                            ScriptManager.RegisterStartupScript(this, GetType(), "mensaje2", "OcultarDiv();", true); //OCULTAR EL DETALLE. MostrarDiv
                        }
                    }
                    else
                    {
                        List<BE_Refrendo> oLista_Refrendo = new List<BE_Refrendo>();
                        BE_Refrendo oBE_RefrendoV2 = new BE_Refrendo();
                        oLista_Refrendo = null;
                        //oLista_Refrendo.Add(oBE_RefrendoV2);
                        //oLista_Refrendo = 0.;              
                        GrvListadoDetalle.DataSource = oLista_Refrendo;
                        GrvListadoDetalle.DataBind();
                        upDetalleDocOriDet.Update();
                        ScriptManager.RegisterStartupScript(this, GetType(), "mensaje2", "OcultarDiv();", true); //OCULTAR EL DETALLE. MostrarDiv
                    }

                }
                else
                {
                    limpiardatos();
                    List<BE_Refrendo> oLista_Refrendo = new List<BE_Refrendo>();
                    BE_Refrendo oBE_RefrendoV2 = new BE_Refrendo();
                    oLista_Refrendo = null;
                    //oLista_Refrendo.Add(oBE_RefrendoV2);
                    //oLista_Refrendo = 0.;              
                    GrvListadoDetalle.DataSource = oLista_Refrendo;
                    GrvListadoDetalle.DataBind();
                    upDetalleDocOriDet.Update();
                    ScriptManager.RegisterStartupScript(this, GetType(), "mensaje2", "OcultarDiv();", true);
                    upDetalleDocOriDet.Update();
                    SCA_MsgInformacion("El booking tiene condicion de llenado. Su solicitud debe ser gestionada por correo (Proceso anterior).");

                }

            }
            else
            {
                limpiardatos();
                upDetalleDocOriDet.Update();
                SCA_MsgInformacion("No hay informacion encontrada");
                //  ScriptManager.RegisterStartupScript(this, GetType(), "mensaje3", "Alerta();", true); //OCULTAR EL DETALLE. MostrarDiv
            }
        }
        catch (Exception ex)
        {
            SCA_MsgInformacion("Error en el catch");
        }


    }

    void limpiardatos()
    {
        txtIdExportador.Text = "";
        txtExportador.Text = "";
        txtIdAgenciaAduanas.Text = "";
        txtAgeniaAduanas.Text = "";
        txtNave.Text = "";
        txtIdNave.Text = "";
        txtViaje.Text = "";
        txtRumbo.Text = "";
        txtIdDocOri.Text = "";
        txtIdBooking.Text = "";
        txtDtCargaSeca.Text = "";
        txtRefrigerante.Text = "";
        txtIdCondicionTransmitir.Text = "";
    }

    protected void BtnGrabar_Click(object sender, EventArgs e)
    {
        //SCA_MsgInformacion(txtExportador.Text.ToString());
        BE_Refrendo oBE_Refrendo = new BE_Refrendo();
        List<BE_Refrendo> oBE_RefrendoList = new List<BE_Refrendo>();
        BL_Refrendo oBL_Refrendo = new BL_Refrendo();
        Int32 IdCliente = GlobalEntity.Instancia.IdCliente;
        String Usuario = GlobalEntity.Instancia.Usuario;

        try
        {
            if (GlobalEntity.Instancia.Usuario == null || GlobalEntity.Instancia.Usuario == "")
            {
                SCA_MsgInformacion("Acaba de perder la session. Vuelva loguearse.");
                return;
            }
            if (lblUplDua.Text == "")
            {
                SCA_MsgInformacion("Debe guardar el documento DAM");
                return;
            }

            if (lblRucFacturar.Text == "")
            {
                SCA_MsgInformacion("Ingrese Ruc a facturar");
                return;
            }

            if (DplTipoPago.SelectedValue == "-1")
            {

                SCA_MsgInformacion("Seleccionar forma de pago");
                return;

            }

            if (DplDespachador.SelectedValue == "-1")
            {

                SCA_MsgInformacion("Seleccionar el despachador");
                return;

            }

            if (DplTipoPago.SelectedValue == "2") //contado
            {
                if (lblupPago.Text == "")
                {
                    SCA_MsgInformacion("Debe adjuntar documento de pago");
                    return;
                }
                if (DplFormaPago.SelectedValue == "-1")
                {
                    SCA_MsgInformacion("Debe seleccionar la forma de pago");
                    return;
                }
                if (DplBancoOrigen.SelectedValue == "-1")
                {
                    SCA_MsgInformacion("Debe seleccionar banco de origen");
                    return;
                }
                if (DplBancoDestino.SelectedValue == "-1")
                {
                    SCA_MsgInformacion("Debe seleccionar banco destino");
                    return;
                }
                if (txtMonto.Text == "")
                {
                    SCA_MsgInformacion("Debe ingresar el monto");
                    return;
                }
                if (txtOperacion.Text == "")
                {
                    SCA_MsgInformacion("Debe ingresar el numero de operacion");
                    return;
                }
                if (DplMoneda.SelectedValue == "-1")
                {
                    SCA_MsgInformacion("Debe seleccionar el tipo de moneda");
                    return;
                }
                if (txtIdLiquidacion.Text == "")
                {
                    SCA_MsgInformacion("El booking no tiene pre liquidacion generada. Comunicarse con ATC");
                    return;
                }
            }

            if (DplTipoPago.SelectedValue == "1") //Credito
            {
                if (lblRucFacturar.Text == "")
                {
                    SCA_MsgInformacion("Debe adjuntar documento de pago");
                    return;
                }
            }


            if (DplTipoPago.SelectedValue == "1") //CREDITO
            {
                oBE_Refrendo.IidDocori = Convert.ToInt32(txtIdDocOri.Text);
                oBE_Refrendo.IdBooking = Convert.ToInt32(txtIdBooking.Text);
                oBE_Refrendo.IdExportador = Convert.ToInt32(txtIdExportador.Text);
                oBE_Refrendo.IdAgenciaAguanas = Convert.ToInt32(txtIdAgenciaAduanas.Text);
                oBE_Refrendo.IdCliente = IdCliente; //Extranet.
                                                    //oBE_Refrendo.sRuc = txtRucFac.Text; txtRucFacNoVisible
                oBE_Refrendo.sRuc = txtRucFacNoVisible.Text;
                oBE_Refrendo.iIdTipoPago = Convert.ToInt32(DplTipoPago.SelectedValue);
                oBE_Refrendo.iIdFormaPago = null;
                oBE_Refrendo.sIdBancoOrigen = null;
                oBE_Refrendo.sIdBancoDestino = null;
                oBE_Refrendo.dMonto = null;
                oBE_Refrendo.sNroOperacion = null;
                oBE_Refrendo.iIdLiquidacion = null;
                oBE_Refrendo.iIdMoneda = null;
                oBE_Refrendo.sComentarios = txtObservaciones.Text;
                oBE_Refrendo.sUsuario = Usuario;
                oBE_Refrendo.iIdDespachador = Convert.ToInt32(DplDespachador.SelectedValue);
            }

            if (DplTipoPago.SelectedValue == "2")
            { //CONTADO

                oBE_Refrendo.IidDocori = Convert.ToInt32(txtIdDocOri.Text);
                oBE_Refrendo.IdBooking = Convert.ToInt32(txtIdBooking.Text);
                oBE_Refrendo.IdExportador = Convert.ToInt32(txtIdExportador.Text);
                oBE_Refrendo.IdAgenciaAguanas = Convert.ToInt32(txtIdAgenciaAduanas.Text);
                oBE_Refrendo.IdCliente = IdCliente;
                //oBE_Refrendo.sRuc = txtRucFac.Text;
                oBE_Refrendo.sRuc = txtRucFacNoVisible.Text;
                oBE_Refrendo.iIdTipoPago = Convert.ToInt32(DplTipoPago.SelectedValue);
                oBE_Refrendo.iIdFormaPago = Convert.ToInt32(DplFormaPago.SelectedValue);
                oBE_Refrendo.sIdBancoOrigen = DplBancoOrigen.SelectedValue;
                oBE_Refrendo.sIdBancoDestino = DplBancoDestino.SelectedValue;
                oBE_Refrendo.dMonto = Convert.ToDecimal(txtMonto.Text);
                oBE_Refrendo.sNroOperacion = txtOperacion.Text;
                oBE_Refrendo.iIdLiquidacion = Convert.ToInt32(txtIdLiquidacion.Text);
                oBE_Refrendo.iIdMoneda = Convert.ToInt32(DplMoneda.SelectedValue);
                oBE_Refrendo.sComentarios = txtObservaciones.Text;
                oBE_Refrendo.sUsuario = Usuario;
                oBE_Refrendo.iIdDespachador = Convert.ToInt32(DplDespachador.SelectedValue);
            }


            oBE_RefrendoList = oBL_Refrendo.ListarArchivosXClienteUsuario(oBE_Refrendo);

            if (oBE_RefrendoList.Count > 0)
            {
                String[] Mensaje = oBL_Refrendo.Insertar_RefrendoElectronico(oBE_Refrendo, oBE_RefrendoList).ToString().Split('|');
                if (Convert.ToInt32(Mensaje[0]) > 0)
                {
                    if (GrvListadoDetalle.Rows.Count == 0)
                    {
                        //ScriptManager.RegisterStartupScript(this, GetType(), "mensaje2", "HabilitaPostPostack();", true);
                    }
                    else
                    {
                        foreach (GridViewRow GrvRow in GrvListadoDetalle.Rows)
                        {
                            if ((GrvRow.FindControl("chkSeleccionarDetalle") as CheckBox).Checked)
                            {
                                TextBox txtBultos = GrvRow.FindControl("txtBultos") as TextBox;
                                TextBox txtPrencitoAduanas = GrvRow.FindControl("txtPrecintoAduanas") as TextBox;
                                TextBox txtPrencitoLinea = GrvRow.FindControl("txtPrecintoLinea") as TextBox;
                                var a = txtBultos.Text;
                                txtBultos.Text = txtBultos.Text.Trim();
                                oBE_Refrendo.iIdSolicitudRefr = Convert.ToInt32(Mensaje[0]);
                                oBE_Refrendo.IidDocori = Convert.ToInt32(txtIdDocOri.Text);
                                oBE_Refrendo.iIdDocOridet = Convert.ToInt32(GrvListadoDetalle.DataKeys[GrvRow.RowIndex].Values["iIdDocOridet"].ToString());
                                oBE_Refrendo.sUsuario = Usuario;
                                oBE_Refrendo.sBultos = txtBultos.Text;
                                oBE_Refrendo.sPrecintoAduanas = txtPrencitoAduanas.Text;
                                oBE_Refrendo.sPrecintoLinea = txtPrencitoLinea.Text;
                                //  String[] Rpt = oBL_Refrendo.Actualizar_BultosDetalleRefrendo(oBE_Refrendo).ToString().Split('|'); 
                                String[] Rpt = oBL_Refrendo.Insertar_BultosPrecintoDetalleRefrendo(oBE_Refrendo).ToString().Split('|');
                                if (Convert.ToInt32(Rpt[0]) > 0)
                                {
                                    //No hace nada
                                }
                                else
                                {
                                    SCA_MsgInformacion("No puede generar su solicitud");
                                    return;
                                }

                            }
                        }
                    }

                    txtExportador.Text = "";
                    txtIdExportador.Text = "";
                    txtIdDocOri.Text = "";
                    txtIdBooking.Text = "";
                    txtAgeniaAduanas.Text = "";
                    txtIdAgenciaAduanas.Text = "";
                    txtNave.Text = "";
                    txtViaje.Text = "";
                    txtRumbo.Text = "";
                    txtIdNave.Text = "";
                    txtRefrigerante.Text = "";
                    txtDtCargaSeca.Text = "";
                    txtRefrigerante.Text = "";
                    imgBtnUplDua.Visible = true;
                    imgBtnUplDua.Enabled = true;
                    imgBtnUplDua.ImageUrl = "~/Imagenes/save.png";
                    imgBtnUplDua.Width = 16;
                    imgBtnUplBoking.Visible = true;
                    imgBtnUplBoking.ImageUrl = "~/Imagenes/save.png";
                    imgBtnUplBoking.Width = 16;
                    imgBtnUplTicekt.Visible = true;
                    imgBtnUplTicekt.ImageUrl = "~/Imagenes/save.png";
                    imgBtnUplTicekt.Width = 16;
                    imgBtnUplPackList.Visible = true;
                    imgBtnUplPackList.ImageUrl = "~/Imagenes/save.png";
                    imgBtnUplPackList.Width = 16;
                    imgBtnUplGuiaRem.Visible = true;
                    imgBtnUplGuiaRem.ImageUrl = "~/Imagenes/save.png";
                    imgBtnUplGuiaRem.Width = 16;
                    imgBtnUplTipoPago.Visible = true;
                    imgBtnUplTipoPago.ImageUrl = "~/Imagenes/save.png";
                    imgBtnUplTipoPago.Width = 16;
                    imgBtnUplTipoPago.Enabled = true;
                    lblupPago.Text = "";
                    DplTipoPago.SelectedValue = "-1";
                    DplFormaPago.SelectedValue = "-1";
                    DplBancoOrigen.SelectedValue = "-1";
                    DplBancoDestino.SelectedValue = "-1";
                    DplMoneda.SelectedValue = "-1";
                    txtMonto.Text = "";
                    txtOperacion.Text = "";
                    txtObservaciones.Text = "";
                    lblUplDua.Text = "";
                    lblUplBooking.Text = "";
                    lblUplTicket.Text = "";
                    lblUplPackList.Text = "";
                    lblUplGuiaRem.Text = "";
                    txtBooking.Text = "";
                    txtRucFacNoVisible.Text = "";
                    lblRucFacturar.Text = "";
                    ScriptManager.RegisterStartupScript(this, GetType(), "mensaje2", "HabilitaIsIsPostack();", true);
                    SCA_MsgInformacion(Mensaje[1].ToString());
                    List<BE_Refrendo> oLista_Refrendo = new List<BE_Refrendo>();
                    BE_Refrendo oBE_RefrendoV2 = new BE_Refrendo();
                    oLista_Refrendo.Add(oBE_RefrendoV2);
                    GrvListadoDetalle.DataSource = oLista_Refrendo;
                    GrvListadoDetalle.DataBind();
                    txtRucFac.Text = "";


                    //=== LIMPIAR .
                    txtObservaciones.Text = "";
                    DplTipoPago.SelectedValue = "-1";
                    lblFormaPago.Visible = true;
                    DplFormaPago.SelectedValue = "-1";
                    DplFormaPago.Visible = true;
                    DplTipoPago.Visible = true;
                    lblBanco.Visible = true;
                    DplBancoOrigen.SelectedValue = "-1";
                    DplBancoOrigen.Visible = true;
                    lblBancoDestino.Visible = true;
                    DplBancoDestino.SelectedValue = "-1";
                    DplBancoDestino.Visible = true;
                    lblMonto.Visible = true;
                    txtMonto.Visible = true;
                    txtMonto.Text = "";
                    lblNroOperacion.Visible = true;
                    txtOperacion.Text = "";
                    txtOperacion.Visible = true;
                    lblMoneda.Visible = true;                    
                    DplMoneda.Visible = true;
                    DplMoneda.SelectedValue = "-1";
                    lblLiquidacion.Visible = true;
                    txtIdLiquidacion.Text = "";
                    txtIdLiquidacion.Visible = true;
                   // DplDespachador.SelectedValue = "-1";
                    validarRUC();
                    ListarDespachadores();
                    UpdatePanel2.Update();
                    upDetalleDocOriDet.Update();
                }
                else
                {

                }
            }
            else
            {

            }

        }
        catch (Exception ex)
        {
            SCA_MsgInformacion("Error en el catch. ");
        }



    }

    void listarDroplist()
    {

        BL_DocumentoPago oBL_DocumentoPago = new BL_DocumentoPago();
        List<BE_DocumentoPago> oBE_DocumentoPago = new List<BE_DocumentoPago>();
        List<BE_Tabla> olst_TablaTipo = new List<BE_Tabla>();

        int i = 0;

        olst_TablaTipo = null;
        i = 0;
        DplBancoOrigen.Items.Clear();
        olst_TablaTipo = oBL_DocumentoPago.ListarBanco(0);
        DplBancoOrigen.Items.Add(new ListItem("[Seleccionar]", "-1"));
        for (i = 0; i < olst_TablaTipo.Count; ++i)
        {
            DplBancoOrigen.Items.Add(new ListItem(olst_TablaTipo[i].sBanco_Descripcion, olst_TablaTipo[i].sIdBanco.ToString()));
        }
        DplBancoOrigen.SelectedIndex = -1;

        olst_TablaTipo = null;
        i = 0;
        DplBancoDestino.Items.Clear();
        olst_TablaTipo = oBL_DocumentoPago.ListarBanco(0);
        DplBancoDestino.Items.Add(new ListItem("[Seleccionar]", "-1"));
        for (i = 0; i < olst_TablaTipo.Count; ++i)
        {
            DplBancoDestino.Items.Add(new ListItem(olst_TablaTipo[i].sBanco_Descripcion, olst_TablaTipo[i].sIdBanco.ToString()));
        }
        DplBancoDestino.SelectedIndex = -1;



        olst_TablaTipo = null;
        i = 0;
        DplFormaPago.Items.Clear();
        olst_TablaTipo = oBL_DocumentoPago.ListarTablaFormagoPago();
        DplFormaPago.Items.Add(new ListItem("[Seleccionar]", "-1"));
        for (i = 0; i < olst_TablaTipo.Count; ++i)
        {
            DplFormaPago.Items.Add(new ListItem(olst_TablaTipo[i].sDescripcion, olst_TablaTipo[i].iIdValor.ToString()));
        }
        DplFormaPago.SelectedIndex = -1;



        olst_TablaTipo = null;
        i = 0;
        DplTipoPago.Items.Clear();
        olst_TablaTipo = oBL_DocumentoPago.ListarTipoPago();
        DplTipoPago.Items.Add(new ListItem("[Seleccionar]", "-1"));
        for (i = 0; i < olst_TablaTipo.Count; ++i)
        {
            if (olst_TablaTipo[i].iIdValor != 3)
            {
                DplTipoPago.Items.Add(new ListItem(olst_TablaTipo[i].sDescripcion, olst_TablaTipo[i].iIdValor.ToString()));
            }

        }
        DplTipoPago.SelectedIndex = -1;


    }

    void UploadIconoEnabled(int tipo, int accion)
    {
        if (tipo == 1)
        {
            if (accion == 1)
            {
                FileUploadBooking.Enabled = true;
                imgBtnUplBoking.Enabled = true;
                imgBtnUplBoking.Visible = true;
                DplTipoPago.Enabled = true;

                //FileUploadTipoPago.Enabled = true;
                imgBtnUplTipoPago.Enabled = true;
                imgBtnUplTipoPago.Visible = true;

            }

            if (accion == 2)
            {
                FileUploaTicket.Enabled = true;
                imgBtnUplTicekt.Enabled = true;
                imgBtnUplTicekt.Visible = true;
            }

            if (accion == 3)
            {
                FileUploadPackList.Enabled = true;
                imgBtnUplPackList.Enabled = true;
                imgBtnUplPackList.Visible = true;
            }

            if (accion == 4)
            {
                FileUploadsGuiaRem.Enabled = true;
                imgBtnUplGuiaRem.Enabled = true;
                imgBtnUplGuiaRem.Visible = true;
            }

            if (accion == 5)
            {
                FileUploadsGuiaRem.Enabled = true;
                imgBtnUplGuiaRem.Enabled = true;
                imgBtnUplGuiaRem.Visible = true;
                DplTipoPago.Enabled = true;
            }


        }
    }

    protected void imgBtnUplBoking_Click(object sender, ImageClickEventArgs e)
    {
        //ScriptManager scriptManager = ScriptManager.GetCurrent(this.Page);
        //scriptManager.RegisterPostBackControl(this.imgBtnUplDua);
        Int32 IdCliente = GlobalEntity.Instancia.IdCliente;
        BE_Refrendo oBE_Refrendo = new BE_Refrendo();
        BL_Refrendo oBL_Refrendo = new BL_Refrendo();

        if (GlobalEntity.Instancia.Usuario == null || GlobalEntity.Instancia.Usuario == "")
        {
            SCA_MsgInformacion("Acaba de perder la session. Vuelva loguearse.");
            return;
        }

        var result = cargarArchivo(FileUploadBooking, IdCliente.ToString());

        if (result.Item2 == 1)
        {
            lblUplBooking.Text = FileUploadBooking.FileName;
            UploadIconoEnabled(1, 2);
            oBE_Refrendo.IdCliente = IdCliente;
            oBE_Refrendo.sNombreArchivo = FileUploadBooking.FileName;
            oBE_Refrendo.sRutaArchivo = result.Item1;
            oBE_Refrendo.iIdusuario = 0;
            oBE_Refrendo.sUsuario = GlobalEntity.Instancia.Usuario;
            oBE_Refrendo.iIdTipoDocumento = 2; //BOOKING
            var res = oBL_Refrendo.Extranet_Ins_ArchivosRefrendo(oBE_Refrendo);

            lblUplBookingId.Text = res.Item2;
            if (lblUplBookingId.Text != "")
            {
                imgBtnUplBoking.ImageUrl = "../../Script/Imagenes/checkTrueFactura.png";
                imgBtnUplBoking.Width = 22;
                imgBtnUplBoking.Enabled = false;
            }
            ScriptManager.RegisterStartupScript(this, GetType(), "mensaje2", "HabilitaPostPostack();", true);

        }
        else if (result.Item2 == 2)
        {
            MensajeScript("Ingrese un archivo en Formato PDF");
            ScriptManager.RegisterStartupScript(this, GetType(), "mensaje2", "HabilitaPostPostack();", true);
        }

    }

    protected void imgBtnUplTicekt_Click(object sender, ImageClickEventArgs e)
    {

        if (GlobalEntity.Instancia.Usuario == null || GlobalEntity.Instancia.Usuario == "")
        {
            SCA_MsgInformacion("Acaba de perder la session. Vuelva loguearse.");
            return;
        }
        Int32 IdCliente = GlobalEntity.Instancia.IdCliente;
        BE_Refrendo oBE_Refrendo = new BE_Refrendo();
        BL_Refrendo oBL_Refrendo = new BL_Refrendo();

        var result = cargarArchivo(FileUploaTicket, IdCliente.ToString());

        if (result.Item2 == 1)
        {
            lblUplTicket.Text = FileUploaTicket.FileName;
            UploadIconoEnabled(1, 3);
            oBE_Refrendo.IdCliente = IdCliente;
            oBE_Refrendo.sNombreArchivo = FileUploaTicket.FileName;
            oBE_Refrendo.sRutaArchivo = result.Item1;
            oBE_Refrendo.iIdusuario = 0;
            oBE_Refrendo.sUsuario = GlobalEntity.Instancia.Usuario;
            oBE_Refrendo.iIdTipoDocumento = 3; //TICKET
            var res = oBL_Refrendo.Extranet_Ins_ArchivosRefrendo(oBE_Refrendo);

            lblUplTicketID.Text = res.Item2;
            if (lblUplTicketID.Text != "")
            {
                imgBtnUplTicekt.ImageUrl = "../../Script/Imagenes/checkTrueFactura.png";
                imgBtnUplTicekt.Width = 22;
                imgBtnUplTicekt.Enabled = false;
            }
            ScriptManager.RegisterStartupScript(this, GetType(), "mensaje2", "HabilitaPostPostack();", true);

        }
        else if (result.Item2 == 2)
        {
            MensajeScript("Ingrese un archivo en Formato PDF");
            ScriptManager.RegisterStartupScript(this, GetType(), "mensaje2", "HabilitaPostPostack();", true);
        }

    }

    protected void imgBtnUplPackList_Click(object sender, ImageClickEventArgs e)
    {
        Int32 IdCliente = GlobalEntity.Instancia.IdCliente;
        BE_Refrendo oBE_Refrendo = new BE_Refrendo();
        BL_Refrendo oBL_Refrendo = new BL_Refrendo();

        if (GlobalEntity.Instancia.Usuario == null || GlobalEntity.Instancia.Usuario == "")
        {
            SCA_MsgInformacion("Acaba de perder la session. Vuelva loguearse.");
            return;
        }

        var result = cargarArchivo(FileUploadPackList, IdCliente.ToString());

        if (result.Item2 == 1)
        {
            lblUplPackList.Text = FileUploadPackList.FileName;
            UploadIconoEnabled(1, 4);
            oBE_Refrendo.IdCliente = IdCliente;
            oBE_Refrendo.sNombreArchivo = FileUploadPackList.FileName;
            oBE_Refrendo.sRutaArchivo = result.Item1;
            oBE_Refrendo.iIdusuario = 0;
            oBE_Refrendo.sUsuario = GlobalEntity.Instancia.Usuario;
            oBE_Refrendo.iIdTipoDocumento = 4; //PACKIN LIST
            var res = oBL_Refrendo.Extranet_Ins_ArchivosRefrendo(oBE_Refrendo);

            lblUplPackListID.Text = res.Item2;
            if (lblUplPackListID.Text != "")
            {
                imgBtnUplPackList.ImageUrl = "../../Script/Imagenes/checkTrueFactura.png";
                imgBtnUplPackList.Width = 22;
                imgBtnUplPackList.Enabled = false;
            }
            ScriptManager.RegisterStartupScript(this, GetType(), "mensaje2", "HabilitaPostPostack();", true);

        }
        else if (result.Item2 == 2)
        {
            MensajeScript("Ingrese un archivo en Formato PDF");
            ScriptManager.RegisterStartupScript(this, GetType(), "mensaje2", "HabilitaPostPostack();", true);
        }

    }

    protected void imgBtnUplGuiaRem_Click(object sender, ImageClickEventArgs e)
    {
        Int32 IdCliente = GlobalEntity.Instancia.IdCliente;
        BE_Refrendo oBE_Refrendo = new BE_Refrendo();
        BL_Refrendo oBL_Refrendo = new BL_Refrendo();

        if (GlobalEntity.Instancia.Usuario == null || GlobalEntity.Instancia.Usuario == "")
        {
            SCA_MsgInformacion("Acaba de perder la session. Vuelva loguearse.");
            return;
        }

        var result = cargarArchivo(FileUploadsGuiaRem, IdCliente.ToString());

        if (result.Item2 == 1)
        {
            lblUplGuiaRem.Text = FileUploadsGuiaRem.FileName;
            UploadIconoEnabled(1, 5);
            oBE_Refrendo.IdCliente = IdCliente;
            oBE_Refrendo.sNombreArchivo = FileUploadsGuiaRem.FileName;
            oBE_Refrendo.sRutaArchivo = result.Item1;
            oBE_Refrendo.iIdusuario = 0;
            oBE_Refrendo.sUsuario = GlobalEntity.Instancia.Usuario;
            oBE_Refrendo.iIdTipoDocumento = 5; //PACKIN LIST
            var res = oBL_Refrendo.Extranet_Ins_ArchivosRefrendo(oBE_Refrendo);

            lblUplGuiaRemID.Text = res.Item2;
            if (lblUplGuiaRemID.Text != "")
            {
                imgBtnUplGuiaRem.ImageUrl = "../../Script/Imagenes/checkTrueFactura.png";
                imgBtnUplGuiaRem.Width = 22;
                imgBtnUplGuiaRem.Enabled = false;
            }
            ScriptManager.RegisterStartupScript(this, GetType(), "mensaje2", "HabilitaPostPostack();", true);

        }
        else if (result.Item2 == 2)
        {
            MensajeScript("Ingrese un archivo en Formato PDF");
            ScriptManager.RegisterStartupScript(this, GetType(), "mensaje2", "HabilitaPostPostack();", true);
        }

    }



    protected void GrvListadoDetalle_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            TextBox txtBultos = (TextBox)e.Row.FindControl("txtBultos");
            ImageButton btnRgistrarFactura = (ImageButton)e.Row.FindControl("ImgBGrabarDetalle");
            BE_Refrendo oitem = (e.Row.DataItem as BE_Refrendo);
            CheckBox chkSeleccion = (CheckBox)e.Row.FindControl("chkSeleccionarDetalle");

            chkSeleccion.Attributes.Add("onclick", "HabilitarUno(this);");
            chkSeleccion.Enabled = false;
            chkSeleccion.Checked = true;
        }
    }

    protected void imgBtnUplTipoPago_Click(object sender, ImageClickEventArgs e)
    {
        Int32 IdCliente = GlobalEntity.Instancia.IdCliente;
        BE_Refrendo oBE_Refrendo = new BE_Refrendo();
        BL_Refrendo oBL_Refrendo = new BL_Refrendo();

        if (GlobalEntity.Instancia.Usuario == null || GlobalEntity.Instancia.Usuario == "")
        {
            SCA_MsgInformacion("Acaba de perder la session. Vuelva loguearse.");
            return;
        }

        var result = cargarArchivo(FileUploadTipoPago, IdCliente.ToString());

        if (result.Item2 == 1)
        {
            lblupPago.Text = FileUploadTipoPago.FileName;
            //UploadIconoEnabled(1, 5);
            oBE_Refrendo.IdCliente = IdCliente;
            oBE_Refrendo.sNombreArchivo = FileUploadTipoPago.FileName;
            oBE_Refrendo.sRutaArchivo = result.Item1;
            oBE_Refrendo.iIdusuario = 0;
            oBE_Refrendo.sUsuario = GlobalEntity.Instancia.Usuario;
            oBE_Refrendo.iIdTipoDocumento = 6; //MEDIO DE PAGO
            var res = oBL_Refrendo.Extranet_Ins_ArchivosRefrendo(oBE_Refrendo);

            lblIdTipoPago.Text = res.Item2;
            if (lblIdTipoPago.Text != "")
            {
                imgBtnUplTipoPago.ImageUrl = "../../Script/Imagenes/checkTrueFactura.png";
                imgBtnUplTipoPago.Width = 22;
                imgBtnUplTipoPago.Enabled = false;
            }
            ScriptManager.RegisterStartupScript(this, GetType(), "mensaje2", "HabilitaPostPostack();", true);

        }
        else if (result.Item2 == 2)
        {
            MensajeScript("Ingrese un archivo en Formato PDF");
            ScriptManager.RegisterStartupScript(this, GetType(), "mensaje2", "HabilitaPostPostack();", true);
        }
    }

    protected void btnTab1_Click(object sender, EventArgs e)
    {
        String sUsuario = GlobalEntity.Instancia.Usuario;

        if (GlobalEntity.Instancia.Usuario == null || GlobalEntity.Instancia.Usuario == "")
        {
            SCA_MsgInformacion("Acaba de perder la session. Vuelva loguearse.");
            return;
        }
        if (txtExportador.Text == "")
        {
            SCA_MsgInformacion("Realice primero la consulta");
            return;
        }

        BE_Refrendo oBE_Refrendo = new BE_Refrendo();
        BL_Refrendo oBL_Refrendo = new BL_Refrendo();
        String[] Retorno;
        oBE_Refrendo.iIdDocori = Convert.ToInt32(txtIdDocOri.Text);
        Retorno = oBL_Refrendo.ValidarCarga(oBE_Refrendo).ToString().Split('|');

        if (txtIdCondicionTransmitir.Text != "3")
        {
            SCA_MsgInformacion("El booking tiene condicion de llenado. Su solicitud debe ser gestionada por correo (Proceso anterior)");
            return;
        }

        if (Retorno[0] == "1") //Enviado o tiene DAM GENERADA
        {
            SCA_MsgInformacion(Retorno[1]);
            return;
        }

        if (Retorno[0] == "2") //Aprobado
        {
            SCA_MsgInformacion(Retorno[1]);
            return;
        }

        if (Retorno[0] == "4") //En Proceso.
        {
            SCA_MsgInformacion(Retorno[1]);
            return;
        }

        if (Retorno[0] == "Parcial")
        {
            SCA_MsgInformacion(Retorno[1]);
            return;
        }

        if (Retorno[0] == "Total")
        {
            if (Retorno[1] == "0")
            {
                txtIdLiquidacion.Text = "";
                UpdatePanel2.Update();
            }
            else
            {
                txtIdLiquidacion.Text = Retorno[1];
                //txtIdLiquidacion.Text = "0";
                UpdatePanel2.Update();
            }

        }


        if (GrvListadoDetalle.Rows.Count == 0)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "mensaje2", "HabilitaPostPostack();", true);
        }
        else
        {
            foreach (GridViewRow GrvRow in GrvListadoDetalle.Rows)
            {
                if ((GrvRow.FindControl("chkSeleccionarDetalle") as CheckBox).Checked)
                {
                    TextBox txtBultos = GrvRow.FindControl("txtBultos") as TextBox;
                    TextBox txtPrencitoAduanas = GrvRow.FindControl("txtPrecintoAduanas") as TextBox;
                    var a = txtBultos.Text;
                    Int32 Conexion = Convert.ToInt32(GrvListadoDetalle.DataKeys[GrvRow.RowIndex].Values["iIConexionCarga"]);
                    // var match = this.match("/^ ([0 - 9]{ 2})\/ ([0 - 9]{ 2})\/ ([0 - 9]{ 4})$/");
                    Regex reg = new Regex(@"[0-9]+"); //^[0-9]+$/
                    txtBultos.Text = txtBultos.Text.Trim();
                    // Regex reg = new Regex(@"\d+\/[^a-z]+/"); //^[0-9]+$/                   
                    if (txtBultos.Text == "")
                    {
                        SCA_MsgInformacion("Ingresar cantidad de bultos");
                    }
                    if (reg.IsMatch(txtBultos.Text) == false)
                    {
                        SCA_MsgInformacion("Solamente numeros en el campo bultos");
                        return;
                    }
                    //if (txtPrencitoAduanas.Text == "")
                    //{
                    //    SCA_MsgInformacion("Precinto aduana obligatorio");
                    //    return;
                    //}

                }
            }
            ScriptManager.RegisterStartupScript(this, GetType(), "mensaje2", "HabilitaPostPostack();", true);
        }
    }



    protected void btnRucFac_Click(object sender, ImageClickEventArgs e)
    {
        BE_Refrendo oBE_Refrendo = new BE_Refrendo();
        BL_Refrendo oBL_Refrendo = new BL_Refrendo();
        BE_Refrendo oBE_DocDetalle = new BE_Refrendo();
        List<BE_Refrendo> List_RucREL = new List<BE_Refrendo>();


        oBE_Refrendo.sRuc = txtRucFac.Text;
        List_RucREL = oBL_Refrendo.ListarREL_Ruc(oBE_Refrendo);
        if (List_RucREL.Count > 0)
        {
            txtRucFacNoVisible.Text = txtRucFac.Text;
            lblRucFacturar.Text = List_RucREL[0].sRazSocial;
        }
        else
        {
            txtRucFacNoVisible.Text = "";
            lblRucFacturar.Text = ""; ;
        }
    }

    protected void DplTipoPago_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (DplTipoPago.SelectedValue == "1")
        {
            DplFormaPago.Visible = false;
            lblFormaPago.Visible = false;
            lblMonto.Visible = false;
            txtMonto.Visible = false;
            lblNroOperacion.Visible = false;
            txtOperacion.Visible = false;
            lblBanco.Visible = false;
            DplBancoOrigen.Visible = false;
            lblMoneda.Visible = false;
            DplMoneda.Visible = false; lblBancoDestino.Visible = false;
            DplBancoDestino.Visible = false;
            lblLiquidacion.Visible = false;
            txtIdLiquidacion.Visible = false;
            FileUploadTipoPago.Enabled = false;
            imgBtnUplTipoPago.Enabled = false;
            upTablaArchivo.Update();
        }
        if (DplTipoPago.SelectedValue == "2") //Contdo
        {
            DplFormaPago.Visible = true;
            lblFormaPago.Visible = true;
            lblMonto.Visible = true;
            txtMonto.Visible = true;
            lblNroOperacion.Visible = true;
            txtOperacion.Visible = true;
            lblBanco.Visible = true;
            DplBancoOrigen.Visible = true;
            lblMoneda.Visible = true;
            DplMoneda.Visible = true; lblBancoDestino.Visible = true;
            DplBancoDestino.Visible = true;
            lblLiquidacion.Visible = true;
            txtIdLiquidacion.Visible = true;
            FileUploadTipoPago.Enabled = true;
            imgBtnUplTipoPago.Enabled = true;
            upTablaArchivo.Update();
        }
    }

    public void validarRUC()
    {
        BE_Refrendo oBE_Refrendo = new BE_Refrendo();
        BL_Refrendo oBL_Refrendo = new BL_Refrendo();
        oBE_Refrendo.IdCliente = GlobalEntity.Instancia.IdCliente;
        String[] Retorno;
        String RUC;
        Retorno = oBL_Refrendo.ValidarRUCCliente_ParaDespachador(oBE_Refrendo).ToString().Split('|');
        if(Retorno[0] != "")
        {
            RUC = Retorno[0];
            vi_ValidacionRUC.Value = RUC.ToString();
        }
        else
            vi_ValidacionRUC.Value = "";

    }

    void ListarDespachadores()
    {
        BL_Refrendo oBL_Refrendo = new BL_Refrendo();
        List<BE_Refrendo> ListRefrendo = new List<BE_Refrendo>();
        var _CodigoRUC="";
        _CodigoRUC = vi_ValidacionRUC.Value;

        ListRefrendo = null;
        int i = 0;
        i = 0;
        DplDespachador.Items.Clear();
        ListRefrendo = oBL_Refrendo.Listar_Despachadores(_CodigoRUC);
        DplDespachador.Items.Add(new ListItem("[Seleccionar]", "-1"));
        for (i = 0; i < ListRefrendo.Count; ++i)
        {
            DplDespachador.Items.Add(new ListItem(ListRefrendo[i].sValor, ListRefrendo[i].iIdValor.ToString()));
        }
        DplDespachador.SelectedIndex = -1;

    }
}