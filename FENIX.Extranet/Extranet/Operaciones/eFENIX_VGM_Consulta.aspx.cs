﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using QNET.Web.UI.Controls;
using FENIX.BusinessEntity;
using FENIX.BusinessLogic;
using System.Text;
using FENIX.Common;
using System.Collections.Generic;
using System.Drawing;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.IO;
using System.Net;
using wsExtranet.SSRSws;

public partial class Extranet_Operaciones_eFENIX_VGM_Consulta : System.Web.UI.Page
{
    #region "Evento Pagina"

    BE_DocumentoOrigenListaList oBE_DocumentoOrigenListaList;
    String urlVGM = ConfigurationManager.AppSettings["urlVGM"].ToString();
    String userSSRS = ConfigurationManager.AppSettings["userSSRS"].ToString();
    String passwordSSRS = ConfigurationManager.AppSettings["passwordSSRS"].ToString();
    String domainSSRS = ConfigurationManager.AppSettings["domainSSRS"].ToString();

    protected void Page_Load(object sender, EventArgs e)
        {
            
            dnvListado.BindGridView += new QNET.Web.UI.Controls.DataNavigator.BindGridViewDelegate(BindGridLista);


        ScriptManager scriptManager = ScriptManager.GetCurrent(this.Page);
        scriptManager.RegisterPostBackControl(this.GrvListado);




        TxtAnoManifiesto.Attributes.Add("onkeydown", "javascript:return fc_PressKeyDO();");
            TxtNumManifiesto.Attributes.Add("onkeydown", "javascript:return fc_PressKeyDO();");
          //  TxtDocumentoH.Attributes.Add("onkeydown", "javascript:return fc_PressKeyDO();");
            TxtDocumentoM.Attributes.Add("onkeydown", "javascript:return fc_PressKeyDO();");
          //  TxtVolante.Attributes.Add("onkeydown", "javascript:return fc_PressKeyDO();");
          //  TxtContenedor.Attributes.Add("onkeydown", "javascript:return fc_PressKeyDO();");

            TxtAnoManifiesto.Attributes.Add("onkeypress", "javascript:return SoloEnteros(event);");
            TxtNumManifiesto.Attributes.Add("onkeypress", "javascript:return SoloEnteros(event);");
          //  TxtVolante.Attributes.Add("onkeypress", "javascript:return SoloEnteros(event);");

          //  TxtCliente.Text = GlobalEntity.Instancia.NombreCliente;
            

            String tiempo = (Convert.ToInt32(GlobalEntity.Instancia.TiempoDuracionSesion.ToString()) * 1000 * 60).ToString();
            Session["Reset"] = true;

            if (!Page.IsPostBack)
            {
                TxtAnoManifiesto.Text = Convert.ToString(DateTime.Now.Year);
                


                if(Request.QueryString["sDoc"] != null)
                {
                TxtDocumentoM.Text = Request.QueryString["sDoc"];
                }
                if(Request.QueryString["sPeriodoManif"] != null)
                {
                TxtAnoManifiesto.Text = Request.QueryString["sPeriodoManif"];
                }
                if(Request.QueryString["sNroManif"] != null)
                {
                TxtNumManifiesto.Text = Request.QueryString["sNroManif"];
                }

            if (TxtNumManifiesto.Text != "" || TxtDocumentoM.Text != ""){
                btnConsulta_Click(sender, e);
            }
            else
            {
                if (GrvListado.Rows.Count == 0)
                {
                    List<BE_DocumentoOrigen> lsBE_DocumentoOrigen = new List<BE_DocumentoOrigen>();
                    BE_DocumentoOrigen oBE_DocumentoOrigen = new BE_DocumentoOrigen();
                    //lsBE_DocumentoOrigen.Add(oBE_DocumentoOrigen);
                    GrvListado.DataSource = lsBE_DocumentoOrigen;
                    GrvListado.DataBind();
                }
            }
               

          

                ScriptManager.RegisterStartupScript(this, GetType(), "mensaje", "SessionExpireAlert(" + tiempo + ");", true);
            }
            else
                ScriptManager.RegisterStartupScript(this, GetType(), "mensaje", "myStopFunction();", true);
        }

    protected void ImgBtnBuscar_Click(object sender, ImageClickEventArgs e)
        {
            dnvListado.InvokeBind();

            if (GrvListado.Rows.Count == 0)
            {
                List<BE_DocumentoOrigen> lsBE_DocumentoOrigen = new List<BE_DocumentoOrigen>();
                BE_DocumentoOrigen oBE_DocumentoOrigen = new BE_DocumentoOrigen();
                lsBE_DocumentoOrigen.Add(oBE_DocumentoOrigen);
                GrvListado.DataSource = lsBE_DocumentoOrigen;
                GrvListado.DataBind();
            }
        }

    protected void GrvListado_RowDataBound(object sender, GridViewRowEventArgs e){

        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            DataKey dataKey;
            dataKey = this.GrvListado.DataKeys[e.Row.RowIndex];
            BE_DocumentoOrigen oItem = (e.Row.DataItem as BE_DocumentoOrigen);

            if(dataKey.Values["iIdDocOri"] != null)
            {
                CheckBox chkSeleccion = (CheckBox)e.Row.FindControl("chkSeleccionar");
                chkSeleccion.Attributes.Add("onclick", "HabilitarUno(this);");
                e.Row.Attributes["onclick"] = String.Format("javascript:return fc_SeleccionaFilaSimple(this);document.getElementById('{0}').value = '{1}';", HdIdDocOri.ClientID, dataKey.Values["iIdDocOri"].ToString());
                e.Row.Attributes["ondblclick"] = String.Format("AbrirPagina('{0}','{1}','{2}');", dataKey.Values["sNumeroDO"].ToString(), dataKey.Values["iIdDocOri"].ToString(), dataKey.Values["sConsignatario"].ToString());

                //if (e.Row.Cells[5].Controls.Count > 0)
                //{
                //    ImageButton ibtn = (ImageButton)e.Row.Cells[5].Controls[0];
                //    ibtn.OnClientClick = String.Format("AbrirPagina('{0}','{1}','{2}');", dataKey.Values["sNumeroDO"].ToString(), dataKey.Values["iIdDocOri"].ToString(), dataKey.Values["sConsignatario"].ToString());
                //}

                for (int c = 1; c < e.Row.Cells.Count; c++)
                {
                    e.Row.Cells[c].Attributes.Add("onclick", string.Format("HabilitarUno(document.getElementById('{0}'));", chkSeleccion.ClientID));
                }

                e.Row.Style["cursor"] = "pointer";
            }
            else
            {
                e.Row.Visible = false;
            }

        }
    }
    protected void GrvListado_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        //  Int32 iIdVolante = 0;
        Int32 iIdDocOri = 0;
        String NombreZip = String.Empty;
        String verVGM = urlVGM;
        String user = userSSRS;
        String password = passwordSSRS;
        String domain = domainSSRS;

        if (e.CommandName == "OpenVerVGM")
        {        
            iIdDocOri = Convert.ToInt32(GrvListado.DataKeys[Convert.ToInt32(e.CommandArgument)].Values["iIdDocOri"].ToString());
            NombreZip = GrvListado.DataKeys[Convert.ToInt32(e.CommandArgument)].Values["sNumeroDO"].ToString() + " - VGM";       
        
            ReportExecutionService rs = new ReportExecutionService();           
            rs.Credentials = new System.Net.NetworkCredential("userReport", "Reportes2019@", "FSAFARGOLINE107");           
            rs.LoadReport(verVGM, null);
       
                        List<ParameterValue> parameters = new List<ParameterValue>();
            parameters.Add(new ParameterValue { Name = "vi_DocOrigen",Value = iIdDocOri.ToString()});
            rs.SetExecutionParameters(parameters.ToArray(),"en-US");
            String deviceInfo = "<DeviceInfo><Toolbar>False</Toolbar></DeviceInfo>";
            String mimeType;
            String encoding;
            String[] streamId;
            Warning[] warning;
            var result = rs.Render("PDF", deviceInfo, out mimeType, out encoding, out encoding, out warning, out streamId);  

            Response.ClearHeaders();
            Response.Clear();
            Response.ContentType = "application/pdf";          
            Response.AddHeader("content-disposition", "attachment;filename="+ NombreZip + ".pdf");          
            Response.Buffer = true;
            Response.Cache.SetCacheability(System.Web.HttpCacheability.NoCache);
            Response.BinaryWrite(result);
            Response.Flush();
            HttpContext.Current.ApplicationInstance.CompleteRequest();
            Response.End();
            Response.Close();
        }
      
    }

   
        protected void btnConsulta_Click(object sender, EventArgs e)
        {
            dnvListado.InvokeBind();

            if (GrvListado.Rows.Count == 0)
            {
                List<BE_DocumentoOrigen> lsBE_DocumentoOrigen = new List<BE_DocumentoOrigen>();
                BE_DocumentoOrigen oBE_DocumentoOrigen = new BE_DocumentoOrigen();
                lsBE_DocumentoOrigen.Add(oBE_DocumentoOrigen);
                GrvListado.DataSource = lsBE_DocumentoOrigen;
                GrvListado.DataBind();
            }
        }

        protected void ImgBtnBuscar_Click(object sender, EventArgs e)
        {
            dnvListado.InvokeBind();

            if (GrvListado.Rows.Count == 0)
            {
                List<BE_DocumentoOrigen> lsBE_DocumentoOrigen = new List<BE_DocumentoOrigen>();
                BE_DocumentoOrigen oBE_DocumentoOrigen = new BE_DocumentoOrigen();
                lsBE_DocumentoOrigen.Add(oBE_DocumentoOrigen);
                GrvListado.DataSource = lsBE_DocumentoOrigen;
                GrvListado.DataBind();
            }
        }

        protected void btn_cerrar_Click(object sender, EventArgs e)
        {
            if (GlobalEntity.Instancia.CerrarExtranet == "S")
            {
                BL_Usuario oBL_Usuario = new BL_Usuario();
                oBL_Usuario.GrabaInicioSesion(GlobalEntity.Instancia.Usuario, "0");
                Session.Abandon();
                FormsAuthentication.SignOut();
                Response.Redirect("../../eFENIX_Login.aspx");
                Response.End();
            }
        }
    #endregion



    #region Metodo Transacciones
        DataNavigatorParams BindGridLista(object sender, EventArgs e)
        {
            BL_DocumentoOrigen oBL_DocumentoOrigen = new BL_DocumentoOrigen();
            BE_DocumentoOrigen oBE_DocumentoOrigen = new BE_DocumentoOrigen();

            oBE_DocumentoOrigen.iIdCliente = GlobalEntity.Instancia.IdCliente;
            oBE_DocumentoOrigen.sAnoManifiesto = TxtAnoManifiesto.Text;
            oBE_DocumentoOrigen.sNumManifiesto = TxtNumManifiesto.Text.Trim();
            oBE_DocumentoOrigen.sNumeroDO = TxtDocumentoM.Text.Trim();
        //       oBE_DocumentoOrigen.sAnoManifiesto = TxtAnoManifiesto.Text;
        //      oBE_DocumentoOrigen.sNumManifiesto = TxtNumManifiesto.Text;
            oBE_DocumentoOrigen.iIdLineaNegocio = 89; //DEP.TEMP.  GlobalEntity.Instancia.IdLineaNegocio;
            oBE_DocumentoOrigen.NPagina = 1;
            oBE_DocumentoOrigen.NRegistros = 50;
            oBE_DocumentoOrigen.iIdTipoOperacion = 65;
            oBE_DocumentoOrigenListaList = new BE_DocumentoOrigenListaList();
            oBE_DocumentoOrigenListaList = oBL_DocumentoOrigen.ListarDO(oBE_DocumentoOrigen);
            GrvListado.Width = 1055;
            dnvListado.Visible = (oBE_DocumentoOrigenListaList.Count() > 0);
            LblTotal.Text = "Total de Registros: " + Convert.ToString(oBE_DocumentoOrigenListaList.Count());
            UdpTotales.Update();

            return new DataNavigatorParams(oBE_DocumentoOrigenListaList, oBE_DocumentoOrigenListaList.Count());
        }

   
    #endregion
    
 }