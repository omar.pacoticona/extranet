﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using QNET.Web.UI.Controls;
using FENIX.BusinessEntity;
using FENIX.BusinessLogic;
using FENIX.DataAccess;
using System.Text;
using FENIX.Common;
using System.Collections.Generic;
using System.Drawing;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.IO;



public partial class Extranet_Operaciones_eFENIX_Volante_Solicitud : System.Web.UI.Page
{
    #region "Evento Pagina"
        private void SCA_MsgInformacion(string strError)
        {
            MensajeScript(strError);
        }

        private void MensajeScript(string SMS)
        {
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "", "<script>alert('" + SMS + "');</script>", false);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            dnvListado.BindGridView += new QNET.Web.UI.Controls.DataNavigator.BindGridViewDelegate(BindGridLista);
        ScriptManager scriptManager = ScriptManager.GetCurrent(this.Page);
        scriptManager.RegisterPostBackControl(this.GrvListado);

        TxtDocumentoM.Attributes.Add("onkeydown", "javascript:return fc_PressKeyDO();");
            TxtCliente.Text = GlobalEntity.Instancia.NombreCliente;
            LblAviso.Text = "Maximo " + GlobalEntity.Instancia.MaximoSolicitudVolantes.ToString() + " documentos";

            String tiempo = (Convert.ToInt32(GlobalEntity.Instancia.TiempoDuracionSesion.ToString()) * 1000 * 60).ToString();
            Session["Reset"] = true;

            if (!Page.IsPostBack)
            {
                if (GrvListado.Rows.Count == 0)
                {
                    List<BE_DocumentoOrigen> lsBE_DocumentoOrigen = new List<BE_DocumentoOrigen>();
                    BE_DocumentoOrigen oBE_DocumentoOrigen = new BE_DocumentoOrigen();
                    lsBE_DocumentoOrigen.Add(oBE_DocumentoOrigen);
                    GrvListado.DataSource = lsBE_DocumentoOrigen;
                    GrvListado.DataBind();

                    LlenaCombos();

                    BE_Cliente oBE_Cliente = new BE_Cliente();
                    oBE_Cliente.sIdCliente = GlobalEntity.Instancia.IdCliente.ToString();
                    oBE_Cliente.iIdRol = 42;

                    BL_Contrato oBL_Contrato = new BL_Contrato();
                    List<BE_TablaUc> Lista = oBL_Contrato.ListarClienteRol(oBE_Cliente);

                    if (Lista.Count > 0)  //Es un Agte.Aduana
                    {
                        DdlAgencia.SelectedValue = Lista[0].iIdValor.ToString();
                        DdlAgencia.Enabled = false;
                    }
                    else
                        DdlAgencia.Enabled = true;
                }

                ScriptManager.RegisterStartupScript(this, GetType(), "mensaje", "SessionExpireAlert(" + tiempo + ");", true);
            }
            else
                ScriptManager.RegisterStartupScript(this, GetType(), "mensaje", "myStopFunction();", true);
        }

        protected void ImgBtnAgregar_Click(object sender, ImageClickEventArgs e)
        {
            String strValidar = "0";
            if (LstDocumentos.Items.Count + 1 <= GlobalEntity.Instancia.MaximoSolicitudVolantes)
            {
                for (int x = 0; x < LstDocumentos.Items.Count; x++)
                {
                    if (TxtDocumentoM.Text.ToUpper() == LstDocumentos.Items[x].ToString().ToUpper())
                    {
                        strValidar = "1";
                        SCA_MsgInformacion("BL ya se encuentra en la lista");
                        break;
                    }
                }
                if (strValidar == "0")
                {
                    LstDocumentos.Items.Add(new System.Web.UI.WebControls.ListItem(TxtDocumentoM.Text.Trim().ToUpper(), TxtDocumentoM.Text.Trim().ToUpper()));
                    TxtDocumentoM.Text = String.Empty;
                    TxtDocumentoM.Focus();

                    LblTotal.Text = "Total de Registros: " + LstDocumentos.Items.Count.ToString();
                }
            }
            else
            {
                SCA_MsgInformacion("No puede agregar más documentos");
            }
        }

        protected void ImgBtnLimpiar_Click(object sender, ImageClickEventArgs e)
        {
            LstDocumentos.Items.Clear();
            LblTotal.Text = "Total de Registros: " + LstDocumentos.Items.Count.ToString();
            TxtDocumentoM.Text = String.Empty;
            TxtDocumentoM.Focus();
        }

        protected void ImgBtnBuscar_Click(object sender, ImageClickEventArgs e)
        {
            BL_DocumentoOrigen objNegocio = new BL_DocumentoOrigen();
            String Permiso = objNegocio.ValidarPermisos(GlobalEntity.Instancia.IdCliente.ToString());
            //if (Permiso == "1")
            //{
                if (DdlAgencia.SelectedValue == "0")
                {
                    MensajeScript("Seleccionar Agente de Aduana Por Favor");
                    DdlAgencia.Focus();
                    return;
                }

                dnvListado.InvokeBind();

                if (GrvListado.Rows.Count == 0)
                {
                    List<BE_DocumentoOrigen> lsBE_DocumentoOrigen = new List<BE_DocumentoOrigen>();
                    BE_DocumentoOrigen oBE_DocumentoOrigen = new BE_DocumentoOrigen();
                    lsBE_DocumentoOrigen.Add(oBE_DocumentoOrigen);
                    GrvListado.DataSource = lsBE_DocumentoOrigen;
                    GrvListado.DataBind();
                }
            //}
            //else
            //{
            //    Mensaje("Solamente pueden Solicitar Volantes las Agencias de Aduanas");
            //}
        }

        void Mensaje(String SMS)
        {
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "", "<script>alert('" + SMS + "');</script>", false);
        }

    //protected void GrvListado_RowDataBound(object sender, GridViewRowEventArgs e)
    //    {
    //        if (e.Row.RowType == DataControlRowType.DataRow)
    //        {
    //            DataKey dataKey;
    //            dataKey = this.GrvListado.DataKeys[e.Row.RowIndex];
    //            BE_DocumentoOrigen oitem = (e.Row.DataItem as BE_DocumentoOrigen);

    //            if (dataKey.Values["sComentario"] != null)
    //            {
    //                CheckBox chkSeleccion = (CheckBox)e.Row.FindControl("chkSeleccionar");
    //                chkSeleccion.Attributes.Add("onclick", "HabilitarUno(this);");

    //                for (int c = 1; c < e.Row.Cells.Count; c++)
    //                {
    //                    e.Row.Cells[c].Attributes.Add("onclick", string.Format("HabilitarUno(document.getElementById('{0}'));", chkSeleccion.ClientID));
    //                }                

    //            //  e.Row.Cells[4].Visible = false;
    //            if (e.Row.Cells[4].Controls.Count > 0)
    //                {
    //                    if (oitem.sComentario == "Documento Generado Correctamente")
    //                    {
    //                        //ImageButton ibtn = (ImageButton)e.Row.Cells[4].Controls[0];
    //                        //ibtn.OnClientClick = String.Format("VerVolante('{0}','{1}');", dataKey.Values["iIdDocOri"].ToString(), dataKey.Values["iIdVolante"].ToString());
    //                        e.Row.Cells[4].Visible = true;
    //                    }
    //                }

    //                if (oitem.sComentario == "Documento Generado Correctamente")
    //                {
    //                    e.Row.Cells[2].Font.Bold = true;
    //                    e.Row.Cells[2].Font.Size = 8;
    //                    e.Row.Cells[2].BackColor = Color.LightGreen;
    //                }

    //                e.Row.Style["cursor"] = "pointer";
    //            }
    //            else
    //            {
    //                e.Row.Visible = false;
    //            }
    //        }
    //    }

        protected void btn_cerrar_Click(object sender, EventArgs e)
        {
            if (GlobalEntity.Instancia.CerrarExtranet == "S")
            {
                BL_Usuario oBL_Usuario = new BL_Usuario();
                oBL_Usuario.GrabaInicioSesion(GlobalEntity.Instancia.Usuario, "0");
                Session.Abandon();
                FormsAuthentication.SignOut();
                Response.Redirect("../../eFENIX_Login.aspx");
                Response.End();
            }
        }

    protected void GrvListado_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        Int32 iIdVolante = 0;
        Int32 iIdDocOri = 0;
        String NombreZip = String.Empty;
        Byte[] bytesZip;


        if (e.CommandName == "OpenVerVolante")
        {
            //Crear Volantes
            iIdVolante = Convert.ToInt32(GrvListado.DataKeys[Convert.ToInt32(e.CommandArgument)].Values["iIdVolante"].ToString());
            iIdDocOri = Convert.ToInt32(GrvListado.DataKeys[Convert.ToInt32(e.CommandArgument)].Values["iIdDocOri"].ToString());
            NombreZip = GrvListado.DataKeys[Convert.ToInt32(e.CommandArgument)].Values["sVolante"].ToString();


            BL_DocumentoOrigen oBL_DocumentoOrigen = new BL_DocumentoOrigen();
            BE_DocumentoOrigen oBE_DocumentoOrigen = new BE_DocumentoOrigen();
            List<BE_DocumentoOrigen> oLis_BE_DocumentoOrigen = new List<BE_DocumentoOrigen>();

            oBE_DocumentoOrigen.iIdVolante = iIdVolante;
            oBE_DocumentoOrigen.sUsuario = GlobalEntity.Instancia.Usuario;
            oBE_DocumentoOrigen.iIdDocOri = iIdDocOri;

            oLis_BE_DocumentoOrigen = oBL_DocumentoOrigen.ListarDatosVolante(oBE_DocumentoOrigen);

            if ((oLis_BE_DocumentoOrigen != null) && (oLis_BE_DocumentoOrigen.Count > 0))
            {
                CreaPDFVolante(oLis_BE_DocumentoOrigen, oBE_DocumentoOrigen.sUsuario);
            }

            iTextSharp.text.Image imagen = iTextSharp.text.Image.GetInstance(Server.MapPath("~/Imagenes/logo.jpg"));


        }

    }

    protected void ImgBtnPdf_Click(object sender, ImageClickEventArgs e)
        {
            BL_DocumentoOrigen oBL_DocumentoOrigen = new BL_DocumentoOrigen();
            BE_DocumentoOrigen oBE_DocumentoOrigen = new BE_DocumentoOrigen();
            List<BE_DocumentoOrigen> oLis_BE_DocumentoOrigen = new List<BE_DocumentoOrigen>();

            oBE_DocumentoOrigen.iIdVolante = Convert.ToInt32(HdIdVolante.Value);
            oBE_DocumentoOrigen.sUsuario = GlobalEntity.Instancia.Usuario;
            oBE_DocumentoOrigen.iIdDocOri = Convert.ToInt32(HdIdDocOri.Value); 

            oLis_BE_DocumentoOrigen = oBL_DocumentoOrigen.ListarDatosVolante(oBE_DocumentoOrigen);

            if ((oLis_BE_DocumentoOrigen != null) && (oLis_BE_DocumentoOrigen.Count > 0))
            {
            //     BL_Tracking oBL_Tracking = new BL_Tracking();
            //   oBL_Tracking.CreaPDFVolante(oLis_BE_DocumentoOrigen, oBE_DocumentoOrigen.sUsuario);
                CreaPDFVolante(oLis_BE_DocumentoOrigen, oBE_DocumentoOrigen.sUsuario);
            }

             iTextSharp.text.Image imagen = iTextSharp.text.Image.GetInstance(Server.MapPath("~/Imagenes/logo.jpg"));
        //if ((oLis_BE_DocumentoOrigen != null) && (oLis_BE_DocumentoOrigen.Count > 0)) 
        //{
        //    Document pdfDoc = new Document(PageSize.A4, 10f, 10f, 10f, 10f);
        //    PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
        //    pdfDoc.Open();

        //    // Le colocamos el título y el autor
        //    pdfDoc.AddTitle("Volante de Extranet");
        //    pdfDoc.AddAuthor("Extranet Fargoline");

        //    iTextSharp.text.Font _sFontNormal = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 8, iTextSharp.text.Font.NORMAL, BaseColor.BLACK);
        //    iTextSharp.text.Font _sFontNormalNegrita = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 8, iTextSharp.text.Font.BOLD, BaseColor.BLACK);
        //    iTextSharp.text.Font _sFontTituloCentral = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 14, iTextSharp.text.Font.BOLD, BaseColor.BLACK);
        //    iTextSharp.text.Font _sFontTituloIzquierda = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 5, iTextSharp.text.Font.ITALIC, BaseColor.BLACK);
        //    iTextSharp.text.Font _sFontEstado = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 12, iTextSharp.text.Font.BOLD, BaseColor.BLACK);
        //    iTextSharp.text.Font _sFontDetalle = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 7, iTextSharp.text.Font.NORMAL, BaseColor.BLACK);
        //    iTextSharp.text.Font _sFontDetalleNegrita = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 7, iTextSharp.text.Font.BOLD, BaseColor.BLACK);


        //    //Genera la tabla 
        //    PdfPTable tblUno = new PdfPTable(3);
        //    tblUno.WidthPercentage = 100;


        //    // Creamos la imagen y le ajustamos el tamaño 
        //    iTextSharp.text.Image imagen = iTextSharp.text.Image.GetInstance(Server.MapPath("~/Imagenes/logo.jpg"));
        //    imagen.BorderWidth = 0;
        //    float percentage = 0.0f;
        //    percentage = (100 / imagen.Width) - 80;
        //    imagen.ScalePercent(percentage * 100);

        //    //Genera la celda
        //    PdfPCell clFila_1_1 = new PdfPCell(new Phrase("Fila_1_1", _sFontNormal));
        //    clFila_1_1 = new PdfPCell(new Phrase("Fargoline S.A.", _sFontNormal));
        //    clFila_1_1.BorderWidth = 0;
        //    clFila_1_1.HorizontalAlignment = 0;
        //    tblUno.AddCell(imagen);

        //    //Genera la celda
        //    PdfPCell clFila_1_2 = new PdfPCell(new Phrase("Fila_1_2", _sFontTituloCentral));
        //    clFila_1_2 = new PdfPCell(new Phrase("Volante Nro: " + oLis_BE_DocumentoOrigen[0].sVolante.Substring(5, 10), _sFontTituloCentral));
        //    clFila_1_2.BorderWidth = 0;
        //    clFila_1_2.HorizontalAlignment = 1;
        //    tblUno.AddCell(clFila_1_2);


        //    //Genera la tabla 
        //    PdfPTable tblDos = new PdfPTable(1);
        //    tblDos.WidthPercentage = 100;

        //    PdfPCell clFechaTitulo = new PdfPCell(new Phrase("FechaTitulo", _sFontTituloIzquierda));
        //    clFechaTitulo = new PdfPCell(new Phrase(DateTime.Now.ToString(), _sFontTituloIzquierda));
        //    clFechaTitulo.BorderWidth = 0;
        //    clFechaTitulo.HorizontalAlignment = 2;
        //    tblDos.AddCell(clFechaTitulo);

        //    PdfPCell clUsuarioTitulo = new PdfPCell(new Phrase("UsuarioTitulo", _sFontTituloIzquierda));
        //    clUsuarioTitulo = new PdfPCell(new Phrase("Impreso por: " + GlobalEntity.Instancia.Usuario, _sFontTituloIzquierda));
        //    clUsuarioTitulo.BorderWidth = 0;
        //    clUsuarioTitulo.HorizontalAlignment = 2;
        //    tblDos.AddCell(clUsuarioTitulo);

        //    PdfPCell clUsuarioGeneraTitulo = new PdfPCell(new Phrase("UsuarioTitulo", _sFontTituloIzquierda));
        //    clUsuarioGeneraTitulo = new PdfPCell(new Phrase("Generado por: " + oLis_BE_DocumentoOrigen[0].sUsuarioGenera, _sFontTituloIzquierda));
        //    clUsuarioGeneraTitulo.BorderWidth = 0;
        //    clUsuarioGeneraTitulo.HorizontalAlignment = 2;
        //    tblDos.AddCell(clUsuarioGeneraTitulo);

        //    //Relaciona la tabla dos en la uno
        //    PdfPCell clFila_1_3 = new PdfPCell(tblDos);
        //    clFila_1_3.BorderWidth = 0;
        //    tblUno.AddCell(clFila_1_3);


        //    PdfPCell clFila_2_1 = new PdfPCell(new Phrase("Fila_2_1"));
        //    clFila_2_1 = new PdfPCell(new Phrase(" ", _sFontTituloIzquierda));
        //    clFila_2_1.Colspan = 3;
        //    clFila_2_1.BorderWidth = 0;
        //    tblUno.AddCell(clFila_2_1);


        //    //Genera la tabla 
        //    PdfPTable tblTres = new PdfPTable(6);
        //    tblTres.TotalWidth = 830f;
        //    float[] widthsTres = new float[] { 100f, 200f, 110f, 200f, 120f, 100f };
        //    tblTres.SetWidths(widthsTres);
        //    tblTres.HorizontalAlignment = 0;
        //    tblTres.SpacingBefore = 10f;

        //    PdfPCell clManifiesto = new PdfPCell(new Phrase("Manifiesto", _sFontNormalNegrita));
        //    clManifiesto = new PdfPCell(new Phrase("Manifiesto:", _sFontNormalNegrita));
        //    clManifiesto.BorderWidth = 0;
        //    clManifiesto.HorizontalAlignment = 0;
        //    clManifiesto.BorderWidthTop = 1f;
        //    clManifiesto.BorderWidthLeft = 1f;
        //    clManifiesto.PaddingLeft = 8f;
        //    clManifiesto.PaddingTop = 5f;
        //    tblTres.AddCell(clManifiesto);

        //    PdfPCell clManifiestoDato = new PdfPCell(new Phrase("ManifiestoDato", _sFontNormal));
        //    clManifiestoDato = new PdfPCell(new Phrase(oLis_BE_DocumentoOrigen[0].sManifiesto, _sFontNormal));
        //    clManifiestoDato.BorderWidth = 0;
        //    clManifiestoDato.HorizontalAlignment = 0;
        //    clManifiestoDato.BorderWidthTop = 1f;
        //    clManifiestoDato.PaddingTop = 5f;
        //    tblTres.AddCell(clManifiestoDato);

        //    PdfPCell clDoHijo = new PdfPCell(new Phrase("DoHijo", _sFontNormalNegrita));
        //    clDoHijo = new PdfPCell(new Phrase("Do Hijo:", _sFontNormalNegrita));
        //    clDoHijo.BorderWidth = 0;
        //    clDoHijo.HorizontalAlignment = 0;
        //    clDoHijo.BorderWidthTop = 1f;
        //    clDoHijo.PaddingTop = 5f;
        //    tblTres.AddCell(clDoHijo);

        //    PdfPCell clDoHijoDato = new PdfPCell(new Phrase("DoHijoDato", _sFontNormal));
        //    clDoHijoDato = new PdfPCell(new Phrase(oLis_BE_DocumentoOrigen[0].sDocumentoHijo, _sFontNormal));
        //    clDoHijoDato.BorderWidth = 0;
        //    clDoHijoDato.HorizontalAlignment = 0;
        //    clDoHijoDato.BorderWidthTop = 1f;
        //    clDoHijoDato.PaddingTop = 5f;
        //    tblTres.AddCell(clDoHijoDato);

        //    PdfPCell clFechaLlegada = new PdfPCell(new Phrase("FechaLlegada", _sFontNormalNegrita));
        //    clFechaLlegada = new PdfPCell(new Phrase("Fecha Llegada:", _sFontNormalNegrita));
        //    clFechaLlegada.BorderWidth = 0;
        //    clFechaLlegada.HorizontalAlignment = 0;
        //    clFechaLlegada.BorderWidthTop = 1f;
        //    clFechaLlegada.PaddingTop = 5f;
        //    tblTres.AddCell(clFechaLlegada);

        //    PdfPCell clFechaLlegadaDato = new PdfPCell(new Phrase("FechaLlegadaDato", _sFontNormal));
        //    clFechaLlegadaDato = new PdfPCell(new Phrase(oLis_BE_DocumentoOrigen[0].sFechaLlegada, _sFontNormal));
        //    clFechaLlegadaDato.BorderWidth = 0;
        //    clFechaLlegadaDato.HorizontalAlignment = 0;
        //    clFechaLlegadaDato.BorderWidthTop = 1f;
        //    clFechaLlegadaDato.BorderWidthRight = 1f;
        //    clFechaLlegadaDato.PaddingTop = 5f;
        //    tblTres.AddCell(clFechaLlegadaDato);


        //    PdfPCell clNave = new PdfPCell(new Phrase("Nave", _sFontNormalNegrita));
        //    clNave = new PdfPCell(new Phrase("Nave:", _sFontNormalNegrita));
        //    clNave.BorderWidth = 0;
        //    clNave.HorizontalAlignment = 0;
        //    clNave.BorderWidthLeft = 1f;
        //    clNave.PaddingLeft = 8f;
        //    tblTres.AddCell(clNave);

        //    PdfPCell clNaveDato = new PdfPCell(new Phrase("NaveDato", _sFontNormal));
        //    clNaveDato = new PdfPCell(new Phrase(oLis_BE_DocumentoOrigen[0].sNave, _sFontNormal));
        //    clNaveDato.BorderWidth = 0;
        //    clNaveDato.HorizontalAlignment = 0;
        //    tblTres.AddCell(clNaveDato);

        //    PdfPCell clDoMadre = new PdfPCell(new Phrase("DoMadre", _sFontNormalNegrita));
        //    clDoMadre = new PdfPCell(new Phrase("Do Madre:", _sFontNormalNegrita));
        //    clDoMadre.BorderWidth = 0;
        //    clDoMadre.HorizontalAlignment = 0;
        //    tblTres.AddCell(clDoMadre);

        //    PdfPCell clDoMadreDato = new PdfPCell(new Phrase("DoMadreDato", _sFontNormal));
        //    clDoMadreDato = new PdfPCell(new Phrase(oLis_BE_DocumentoOrigen[0].sDocumentoMaster, _sFontNormal));
        //    clDoMadreDato.BorderWidth = 0;
        //    clDoMadreDato.HorizontalAlignment = 0;
        //    tblTres.AddCell(clDoMadreDato);

        //    PdfPCell clFechaTermino = new PdfPCell(new Phrase("FechaTermino", _sFontNormalNegrita));
        //    clFechaTermino = new PdfPCell(new Phrase("Fecha Termino:", _sFontNormalNegrita));
        //    clFechaTermino.BorderWidth = 0;
        //    clFechaTermino.HorizontalAlignment = 0;
        //    tblTres.AddCell(clFechaTermino);

        //    PdfPCell clFechaTerminoDato = new PdfPCell(new Phrase("FechaTerminoDato", _sFontNormal));
        //    clFechaTerminoDato = new PdfPCell(new Phrase(oLis_BE_DocumentoOrigen[0].sFechaTermino, _sFontNormal));
        //    clFechaTerminoDato.BorderWidth = 0;
        //    clFechaTerminoDato.HorizontalAlignment = 0;
        //    clFechaTerminoDato.BorderWidthRight = 1f;
        //    tblTres.AddCell(clFechaTerminoDato);


        //    PdfPCell clViaje = new PdfPCell(new Phrase("Viaje", _sFontNormalNegrita));
        //    clViaje = new PdfPCell(new Phrase("Viaje:", _sFontNormalNegrita));
        //    clViaje.BorderWidth = 0;
        //    clViaje.HorizontalAlignment = 0;
        //    clViaje.BorderWidthLeft = 1f;
        //    clViaje.PaddingLeft = 8f;
        //    tblTres.AddCell(clViaje);

        //    PdfPCell clViajeDato = new PdfPCell(new Phrase("ViajeDato", _sFontNormal));
        //    clViajeDato = new PdfPCell(new Phrase(oLis_BE_DocumentoOrigen[0].sViaje, _sFontNormal));
        //    clViajeDato.BorderWidth = 0;
        //    clViajeDato.HorizontalAlignment = 0;
        //    tblTres.AddCell(clViajeDato);

        //    PdfPCell clPtoEmbarque = new PdfPCell(new Phrase("PtoEmbarque", _sFontNormalNegrita));
        //    clPtoEmbarque = new PdfPCell(new Phrase("Pto Embarque:", _sFontNormalNegrita));
        //    clPtoEmbarque.BorderWidth = 0;
        //    clPtoEmbarque.HorizontalAlignment = 0;
        //    tblTres.AddCell(clPtoEmbarque);

        //    PdfPCell clPtoEmbarqueDato = new PdfPCell(new Phrase("PtoEmbarqueDato", _sFontNormal));
        //    clPtoEmbarqueDato = new PdfPCell(new Phrase(oLis_BE_DocumentoOrigen[0].sPtoEmbarque, _sFontNormal));
        //    clPtoEmbarqueDato.BorderWidth = 0;
        //    clPtoEmbarqueDato.HorizontalAlignment = 0;
        //    tblTres.AddCell(clPtoEmbarqueDato);

        //    PdfPCell clDetalleAduana = new PdfPCell(new Phrase("DetalleAduana", _sFontNormalNegrita));
        //    clDetalleAduana = new PdfPCell(new Phrase("Detalle Aduana:", _sFontNormalNegrita));
        //    clDetalleAduana.BorderWidth = 0;
        //    clDetalleAduana.HorizontalAlignment = 0;
        //    tblTres.AddCell(clDetalleAduana);

        //    PdfPCell clDetalleAduanaData = new PdfPCell(new Phrase("DetalleAduanaData", _sFontNormal));
        //    clDetalleAduanaData = new PdfPCell(new Phrase(oLis_BE_DocumentoOrigen[0].sDetalleAduana, _sFontNormal));
        //    clDetalleAduanaData.BorderWidth = 0;
        //    clDetalleAduanaData.HorizontalAlignment = 0;
        //    clDetalleAduanaData.BorderWidthRight = 1f;
        //    tblTres.AddCell(clDetalleAduanaData);


        //    PdfPCell clRumbo = new PdfPCell(new Phrase("Rumbo", _sFontNormalNegrita));
        //    clRumbo = new PdfPCell(new Phrase("Rumbo:", _sFontNormalNegrita));
        //    clRumbo.BorderWidth = 0;
        //    clRumbo.HorizontalAlignment = 0;
        //    clRumbo.BorderWidthLeft = 1f;
        //    clRumbo.PaddingLeft = 8f;
        //    tblTres.AddCell(clRumbo);

        //    PdfPCell clRumboDato = new PdfPCell(new Phrase("RumboDato", _sFontNormal));
        //    clRumboDato = new PdfPCell(new Phrase(oLis_BE_DocumentoOrigen[0].sRumbo, _sFontNormal));
        //    clRumboDato.BorderWidth = 0;
        //    clRumboDato.HorizontalAlignment = 0;
        //    tblTres.AddCell(clRumboDato);

        //    PdfPCell clPtoFinal = new PdfPCell(new Phrase("PtoFinal", _sFontNormalNegrita));
        //    clPtoFinal = new PdfPCell(new Phrase("Pto Final:", _sFontNormalNegrita));
        //    clPtoFinal.BorderWidth = 0;
        //    clPtoFinal.HorizontalAlignment = 0;
        //    tblTres.AddCell(clPtoFinal);

        //    PdfPCell clPtoFinalDato = new PdfPCell(new Phrase("PtoFinalDato", _sFontNormal));
        //    clPtoFinalDato = new PdfPCell(new Phrase(oLis_BE_DocumentoOrigen[0].sPtoFinal, _sFontNormal));
        //    clPtoFinalDato.BorderWidth = 0;
        //    clPtoFinalDato.HorizontalAlignment = 0;
        //    tblTres.AddCell(clPtoFinalDato);

        //    PdfPCell clTipoOperacion = new PdfPCell(new Phrase("TipoOperacion", _sFontNormalNegrita));
        //    clTipoOperacion = new PdfPCell(new Phrase("Tipo Operación:", _sFontNormalNegrita));
        //    clTipoOperacion.BorderWidth = 0;
        //    clTipoOperacion.HorizontalAlignment = 0;
        //    tblTres.AddCell(clTipoOperacion);

        //    PdfPCell clTipoOperacionDato = new PdfPCell(new Phrase("TipoOperacionDato", _sFontNormal));
        //    clTipoOperacionDato = new PdfPCell(new Phrase(oLis_BE_DocumentoOrigen[0].sOperacion, _sFontNormal));
        //    clTipoOperacionDato.BorderWidth = 0;
        //    clTipoOperacionDato.HorizontalAlignment = 0;
        //    clTipoOperacionDato.BorderWidthRight = 1f;
        //    tblTres.AddCell(clTipoOperacionDato);


        //    PdfPCell clEmision = new PdfPCell(new Phrase("Emision", _sFontNormalNegrita));
        //    clEmision = new PdfPCell(new Phrase("Emisión:", _sFontNormalNegrita));
        //    clEmision.BorderWidth = 0;
        //    clEmision.HorizontalAlignment = 0;
        //    clEmision.BorderWidthLeft = 1f;
        //    clEmision.BorderWidthBottom = 1f;
        //    clEmision.PaddingLeft = 8f;
        //    clEmision.PaddingBottom = 8f;
        //    tblTres.AddCell(clEmision);

        //    PdfPCell clEmisionDato = new PdfPCell(new Phrase("EmisionDato", _sFontNormal));
        //    clEmisionDato = new PdfPCell(new Phrase(oLis_BE_DocumentoOrigen[0].sFechaEmision, _sFontNormal));
        //    clEmisionDato.BorderWidth = 0;
        //    clEmisionDato.HorizontalAlignment = 0;
        //    clEmisionDato.BorderWidthBottom = 1f;
        //    clEmisionDato.PaddingBottom = 8f;
        //    tblTres.AddCell(clEmisionDato);

        //    PdfPCell clTermPorturario = new PdfPCell(new Phrase("TermPorturario", _sFontNormalNegrita));
        //    clTermPorturario = new PdfPCell(new Phrase("Term.Porturario:", _sFontNormalNegrita));
        //    clTermPorturario.BorderWidth = 0;
        //    clTermPorturario.HorizontalAlignment = 0;
        //    clTermPorturario.BorderWidthBottom = 1f;
        //    tblTres.AddCell(clTermPorturario);

        //    PdfPCell clTermPorturarioDato = new PdfPCell(new Phrase("TermPorturarioDato", _sFontNormal));
        //    clTermPorturarioDato = new PdfPCell(new Phrase(oLis_BE_DocumentoOrigen[0].sTerminalPortuario, _sFontNormal));
        //    clTermPorturarioDato.BorderWidth = 0;
        //    clTermPorturarioDato.HorizontalAlignment = 0;
        //    clTermPorturarioDato.BorderWidthBottom = 1f;
        //    tblTres.AddCell(clTermPorturarioDato);

        //    PdfPCell clFechaIngreso = new PdfPCell(new Phrase("FechaIngreso", _sFontNormalNegrita));
        //    clFechaIngreso = new PdfPCell(new Phrase("Fecha Ingreso DO:", _sFontNormalNegrita));
        //    clFechaIngreso.BorderWidth = 0;
        //    clFechaIngreso.HorizontalAlignment = 0;
        //    clFechaIngreso.BorderWidthBottom = 1f;
        //    tblTres.AddCell(clFechaIngreso);

        //    PdfPCell clFechaIngresoDato = new PdfPCell(new Phrase("FechaIngresoDato", _sFontNormal));
        //    clFechaIngresoDato = new PdfPCell(new Phrase(oLis_BE_DocumentoOrigen[0].sFecIngresoBL, _sFontNormal));
        //    clFechaIngresoDato.BorderWidth = 0;
        //    clFechaIngresoDato.HorizontalAlignment = 0;
        //    clFechaIngresoDato.BorderWidthRight = 1f;
        //    clFechaIngresoDato.BorderWidthBottom = 1f;
        //    tblTres.AddCell(clFechaIngresoDato);

        //    //Relaciona la tabla tres en la uno
        //    PdfPCell clFila_3_1 = new PdfPCell(tblTres);
        //    clFila_3_1.Colspan = 3;
        //    clFila_3_1.BorderWidth = 0;
        //    tblUno.AddCell(clFila_3_1);


        //    //Genera la tabla 
        //    PdfPTable tblCuatro = new PdfPTable(2);
        //    tblCuatro.TotalWidth = 830f;
        //    float[] widthsCuatro = new float[] { 150f, 680f };
        //    tblCuatro.SetWidths(widthsCuatro);
        //    tblCuatro.HorizontalAlignment = 0;
        //    tblCuatro.SpacingBefore = 10f;

        //    PdfPCell clLinea = new PdfPCell(new Phrase("Linea", _sFontNormalNegrita));
        //    clLinea = new PdfPCell(new Phrase("Línea:", _sFontNormalNegrita));
        //    clLinea.BorderWidth = 0;
        //    clLinea.HorizontalAlignment = 0;
        //    clLinea.BorderWidthTop = 1f;
        //    clLinea.BorderWidthLeft = 1f;
        //    clLinea.PaddingLeft = 8f;
        //    clLinea.PaddingTop = 5f;
        //    tblCuatro.AddCell(clLinea);

        //    PdfPCell clLineaDato = new PdfPCell(new Phrase("LineaDato", _sFontNormal));
        //    clLineaDato = new PdfPCell(new Phrase(oLis_BE_DocumentoOrigen[0].sLineaMaritima, _sFontNormal));
        //    clLineaDato.BorderWidth = 0;
        //    clLineaDato.HorizontalAlignment = 0;
        //    clLineaDato.BorderWidthTop = 1f;
        //    clLineaDato.BorderWidthRight = 1f;
        //    clLineaDato.PaddingTop = 5f;
        //    tblCuatro.AddCell(clLineaDato);

        //    PdfPCell clConsignatario = new PdfPCell(new Phrase("Consignatario", _sFontNormalNegrita));
        //    clConsignatario = new PdfPCell(new Phrase("Consignatario:", _sFontNormalNegrita));
        //    clConsignatario.BorderWidth = 0;
        //    clConsignatario.HorizontalAlignment = 0;
        //    clConsignatario.BorderWidthLeft = 1f;
        //    clConsignatario.PaddingLeft = 8f;
        //    tblCuatro.AddCell(clConsignatario);

        //    PdfPCell clConsignatarioDato = new PdfPCell(new Phrase("ConsignatarioDato", _sFontNormal));
        //    clConsignatarioDato = new PdfPCell(new Phrase(oLis_BE_DocumentoOrigen[0].sConsignatario, _sFontNormal));
        //    clConsignatarioDato.BorderWidth = 0;
        //    clConsignatarioDato.HorizontalAlignment = 0;
        //    clConsignatarioDato.BorderWidthRight = 1f;
        //    tblCuatro.AddCell(clConsignatarioDato);

        //    PdfPCell clCodCLiente = new PdfPCell(new Phrase("CodCLiente", _sFontNormalNegrita));
        //    clCodCLiente = new PdfPCell(new Phrase("Cod/Cliente:", _sFontNormalNegrita));
        //    clCodCLiente.BorderWidth = 0;
        //    clCodCLiente.HorizontalAlignment = 0;
        //    clCodCLiente.BorderWidthLeft = 1f;
        //    clCodCLiente.PaddingLeft = 8f;
        //    tblCuatro.AddCell(clCodCLiente);

        //    PdfPCell clCodClienteDato = new PdfPCell(new Phrase("CodClienteDato", _sFontNormal));
        //    clCodClienteDato = new PdfPCell(new Phrase(oLis_BE_DocumentoOrigen[0].sCliente, _sFontNormal));
        //    clCodClienteDato.BorderWidth = 0;
        //    clCodClienteDato.HorizontalAlignment = 0;
        //    clCodClienteDato.BorderWidthRight = 1f;
        //    tblCuatro.AddCell(clCodClienteDato);

        //    PdfPCell clAgenciaAduana = new PdfPCell(new Phrase("AgenciaAduana", _sFontNormalNegrita));
        //    clAgenciaAduana = new PdfPCell(new Phrase("Agencia Aduana:", _sFontNormalNegrita));
        //    clAgenciaAduana.BorderWidth = 0;
        //    clAgenciaAduana.HorizontalAlignment = 0;
        //    clAgenciaAduana.BorderWidthLeft = 1f;
        //    clAgenciaAduana.PaddingLeft = 8f;
        //    tblCuatro.AddCell(clAgenciaAduana);

        //    PdfPCell clAgenciaAduanaDato = new PdfPCell(new Phrase("AgenciaAduanaDato", _sFontNormal));
        //    clAgenciaAduanaDato = new PdfPCell(new Phrase(oLis_BE_DocumentoOrigen[0].sAgenciaAduana, _sFontNormal));
        //    clAgenciaAduanaDato.BorderWidth = 0;
        //    clAgenciaAduanaDato.HorizontalAlignment = 0;
        //    clAgenciaAduanaDato.BorderWidthRight = 1f;
        //    tblCuatro.AddCell(clAgenciaAduanaDato);

        //    PdfPCell clConsolidador = new PdfPCell(new Phrase("Consolidador", _sFontNormalNegrita));
        //    clConsolidador = new PdfPCell(new Phrase("Consolidador:", _sFontNormalNegrita));
        //    clConsolidador.BorderWidth = 0;
        //    clConsolidador.HorizontalAlignment = 0;
        //    clConsolidador.BorderWidthLeft = 1f;
        //    clConsolidador.PaddingLeft = 8f;
        //    tblCuatro.AddCell(clConsolidador);

        //    PdfPCell clConsolidadorDato = new PdfPCell(new Phrase("ConsolidadorDato", _sFontNormal));
        //    clConsolidadorDato = new PdfPCell(new Phrase(oLis_BE_DocumentoOrigen[0].sConsolidador, _sFontNormal));
        //    clConsolidadorDato.BorderWidth = 0;
        //    clConsolidadorDato.HorizontalAlignment = 0;
        //    clConsolidadorDato.BorderWidthRight = 1f;
        //    tblCuatro.AddCell(clConsolidadorDato);

        //    PdfPCell clNotificante = new PdfPCell(new Phrase("Notificante", _sFontNormalNegrita));
        //    clNotificante = new PdfPCell(new Phrase("Notificante:", _sFontNormalNegrita));
        //    clNotificante.BorderWidth = 0;
        //    clNotificante.HorizontalAlignment = 0;
        //    clNotificante.BorderWidthLeft = 1f;
        //    clNotificante.PaddingLeft = 8f;
        //    tblCuatro.AddCell(clNotificante);

        //    PdfPCell clNotificanteDato = new PdfPCell(new Phrase("NotificanteDato", _sFontNormal));
        //    clNotificanteDato = new PdfPCell(new Phrase(oLis_BE_DocumentoOrigen[0].sNotificante, _sFontNormal));
        //    clNotificanteDato.BorderWidth = 0;
        //    clNotificanteDato.HorizontalAlignment = 0;
        //    clNotificanteDato.BorderWidthRight = 1f;
        //    tblCuatro.AddCell(clNotificanteDato);

        //    PdfPCell clAgenciaMaritima = new PdfPCell(new Phrase("AgenciaMaritima", _sFontNormalNegrita));
        //    clAgenciaMaritima = new PdfPCell(new Phrase("Agencia Maritima:", _sFontNormalNegrita));
        //    clAgenciaMaritima.BorderWidth = 0;
        //    clAgenciaMaritima.HorizontalAlignment = 0;
        //    clAgenciaMaritima.BorderWidthLeft = 1f;
        //    clAgenciaMaritima.BorderWidthBottom = 1f;
        //    clAgenciaMaritima.PaddingLeft = 8f;
        //    clAgenciaMaritima.PaddingBottom = 5f;
        //    tblCuatro.AddCell(clAgenciaMaritima);

        //    PdfPCell clAgenciaMaritimaDato = new PdfPCell(new Phrase("AgenciaMaritimaDato", _sFontNormal));
        //    clAgenciaMaritimaDato = new PdfPCell(new Phrase(oLis_BE_DocumentoOrigen[0].sAgenciaMaritima, _sFontNormal));
        //    clAgenciaMaritimaDato.BorderWidth = 0;
        //    clAgenciaMaritimaDato.HorizontalAlignment = 0;
        //    clAgenciaMaritimaDato.BorderWidthBottom = 1f;
        //    clAgenciaMaritimaDato.BorderWidthRight = 1f;
        //    clAgenciaMaritimaDato.PaddingBottom = 5f;
        //    tblCuatro.AddCell(clAgenciaMaritimaDato);

        //    //Relaciona la tabla cuatro en la uno
        //    PdfPCell clFila_4_1 = new PdfPCell(tblCuatro);
        //    clFila_4_1.Colspan = 3;
        //    clFila_4_1.BorderWidth = 0;
        //    tblUno.AddCell(clFila_4_1);


        //    //Genera la tabla 
        //    PdfPTable tblCinco = new PdfPTable(6);
        //    tblCinco.TotalWidth = 830f;
        //    float[] widthsCinco = new float[] { 100f, 150f, 150f, 150f, 150f, 150f };
        //    tblCinco.SetWidths(widthsCinco);
        //    tblCinco.HorizontalAlignment = 0;
        //    tblCinco.SpacingBefore = 10f;

        //    PdfPCell clTotalGeneralTitulo = new PdfPCell(new Phrase("TotalGeneralTitulo", _sFontNormalNegrita));
        //    clTotalGeneralTitulo = new PdfPCell(new Phrase("Total General", _sFontNormalNegrita));
        //    clTotalGeneralTitulo.BorderWidth = 0.75f;
        //    clTotalGeneralTitulo.HorizontalAlignment = 0;
        //    clTotalGeneralTitulo.PaddingTop = 5f;
        //    clTotalGeneralTitulo.PaddingBottom = 5f;
        //    tblCinco.AddCell(clTotalGeneralTitulo);

        //    PdfPCell clManifestadoTitulo = new PdfPCell(new Phrase("ManifestadoTitulo", _sFontNormalNegrita));
        //    clManifestadoTitulo = new PdfPCell(new Phrase("Manifestado", _sFontNormalNegrita));
        //    clManifestadoTitulo.BorderWidth = 0.75f;
        //    clManifestadoTitulo.HorizontalAlignment = 1;
        //    clManifestadoTitulo.PaddingTop = 5f;
        //    clManifestadoTitulo.PaddingBottom = 5f;
        //    tblCinco.AddCell(clManifestadoTitulo);

        //    PdfPCell clActualTitulo = new PdfPCell(new Phrase("ActualTitulo", _sFontNormalNegrita));
        //    clActualTitulo = new PdfPCell(new Phrase("Actual", _sFontNormalNegrita));
        //    clActualTitulo.BorderWidth = 0.75f;
        //    clActualTitulo.HorizontalAlignment = 1;
        //    clActualTitulo.PaddingTop = 5f;
        //    clActualTitulo.PaddingBottom = 5f;
        //    tblCinco.AddCell(clActualTitulo);

        //    PdfPCell clMalosTitulo = new PdfPCell(new Phrase("MalosTitulo", _sFontNormalNegrita));
        //    clMalosTitulo = new PdfPCell(new Phrase("Malos", _sFontNormalNegrita));
        //    clMalosTitulo.BorderWidth = 0.75f;
        //    clMalosTitulo.HorizontalAlignment = 1;
        //    clMalosTitulo.PaddingTop = 5f;
        //    clMalosTitulo.PaddingBottom = 5f;
        //    tblCinco.AddCell(clMalosTitulo);

        //    PdfPCell clFaltantesTitulo = new PdfPCell(new Phrase("FaltantesTitulo", _sFontNormalNegrita));
        //    clFaltantesTitulo = new PdfPCell(new Phrase("Faltantes", _sFontNormalNegrita));
        //    clFaltantesTitulo.BorderWidth = 0.75f;
        //    clFaltantesTitulo.HorizontalAlignment = 1;
        //    clFaltantesTitulo.PaddingTop = 5f;
        //    clFaltantesTitulo.PaddingBottom = 5f;
        //    tblCinco.AddCell(clFaltantesTitulo);

        //    PdfPCell clSobrantesTitulo = new PdfPCell(new Phrase("SobrantesTitulo", _sFontNormalNegrita));
        //    clSobrantesTitulo = new PdfPCell(new Phrase("Sobrantes", _sFontNormalNegrita));
        //    clSobrantesTitulo.BorderWidth = 0.75f;
        //    clSobrantesTitulo.HorizontalAlignment = 1;
        //    clSobrantesTitulo.PaddingTop = 5f;
        //    clSobrantesTitulo.PaddingBottom = 5f;
        //    tblCinco.AddCell(clSobrantesTitulo);

        //    PdfPCell clBultosTitulo = new PdfPCell(new Phrase("BultosTitulo", _sFontNormalNegrita));
        //    clBultosTitulo = new PdfPCell(new Phrase("Bultos", _sFontNormalNegrita));
        //    clBultosTitulo.BorderWidth = 0.75f;
        //    clBultosTitulo.HorizontalAlignment = 0;
        //    clBultosTitulo.PaddingTop = 5f;
        //    clBultosTitulo.PaddingBottom = 5f;
        //    tblCinco.AddCell(clBultosTitulo);

        //    PdfPCell clBultosManifDato = new PdfPCell(new Phrase("BultosManifDato", _sFontNormal));
        //    clBultosManifDato = new PdfPCell(new Phrase(oLis_BE_DocumentoOrigen[0].dBultoManif.ToString(), _sFontNormal));
        //    clBultosManifDato.BorderWidth = 0.75f;
        //    clBultosManifDato.HorizontalAlignment = 2;
        //    clBultosManifDato.PaddingTop = 5f;
        //    clBultosManifDato.PaddingBottom = 5f;
        //    tblCinco.AddCell(clBultosManifDato);

        //    PdfPCell clBultosRecibDato = new PdfPCell(new Phrase("BultosRecibDato", _sFontNormal));
        //    clBultosRecibDato = new PdfPCell(new Phrase(oLis_BE_DocumentoOrigen[0].dBultoManif.ToString(), _sFontNormal));
        //    clBultosRecibDato.BorderWidth = 0.75f;
        //    clBultosRecibDato.HorizontalAlignment = 2;
        //    clBultosRecibDato.PaddingTop = 5f;
        //    clBultosRecibDato.PaddingBottom = 5f;
        //    tblCinco.AddCell(clBultosRecibDato);

        //    PdfPCell clBultosMaloDato = new PdfPCell(new Phrase("BultosMaloDato", _sFontNormal));
        //    clBultosMaloDato = new PdfPCell(new Phrase(oLis_BE_DocumentoOrigen[0].dBultoMalo.ToString(), _sFontNormal));
        //    clBultosMaloDato.BorderWidth = 0.75f;
        //    clBultosMaloDato.HorizontalAlignment = 2;
        //    clBultosMaloDato.PaddingTop = 5f;
        //    clBultosMaloDato.PaddingBottom = 5f;
        //    tblCinco.AddCell(clBultosMaloDato);

        //    PdfPCell clBultosFaltanteDato = new PdfPCell(new Phrase("BultosFaltanteDato", _sFontNormal));
        //    clBultosFaltanteDato = new PdfPCell(new Phrase(oLis_BE_DocumentoOrigen[0].dBultoFaltante.ToString(), _sFontNormal));
        //    clBultosFaltanteDato.BorderWidth = 0.75f;
        //    clBultosFaltanteDato.HorizontalAlignment = 2;
        //    clBultosFaltanteDato.PaddingTop = 5f;
        //    clBultosFaltanteDato.PaddingBottom = 5f;
        //    tblCinco.AddCell(clBultosFaltanteDato);

        //    PdfPCell clBultosSobranteDato = new PdfPCell(new Phrase("BultosSobranteDato", _sFontNormal));
        //    clBultosSobranteDato = new PdfPCell(new Phrase(oLis_BE_DocumentoOrigen[0].dBultoSobrante.ToString(), _sFontNormal));
        //    clBultosSobranteDato.BorderWidth = 0.75f;
        //    clBultosSobranteDato.HorizontalAlignment = 2;
        //    clBultosSobranteDato.PaddingTop = 5f;
        //    clBultosSobranteDato.PaddingBottom = 5f;
        //    tblCinco.AddCell(clBultosSobranteDato);

        //    PdfPCell clPesoTitulo = new PdfPCell(new Phrase("PesoTitulo", _sFontNormalNegrita));
        //    clPesoTitulo = new PdfPCell(new Phrase("Peso", _sFontNormalNegrita));
        //    clPesoTitulo.BorderWidth = 0.75f;
        //    clPesoTitulo.HorizontalAlignment = 0;
        //    clPesoTitulo.PaddingTop = 5f;
        //    clPesoTitulo.PaddingBottom = 5f;
        //    tblCinco.AddCell(clPesoTitulo);

        //    PdfPCell clPesoManifDato = new PdfPCell(new Phrase("PesoManifDato", _sFontNormal));
        //    clPesoManifDato = new PdfPCell(new Phrase(oLis_BE_DocumentoOrigen[0].dPesoManif.ToString(), _sFontNormal));
        //    clPesoManifDato.BorderWidth = 0.75f;
        //    clPesoManifDato.HorizontalAlignment = 2;
        //    clPesoManifDato.PaddingTop = 5f;
        //    clPesoManifDato.PaddingBottom = 5f;
        //    tblCinco.AddCell(clPesoManifDato);

        //    PdfPCell clPesoRecibDato = new PdfPCell(new Phrase("PesoRecibDato", _sFontNormal));
        //    clPesoRecibDato = new PdfPCell(new Phrase(oLis_BE_DocumentoOrigen[0].dBultoManif.ToString(), _sFontNormal));
        //    clPesoRecibDato.BorderWidth = 0.75f;
        //    clPesoRecibDato.HorizontalAlignment = 2;
        //    clPesoRecibDato.PaddingTop = 5f;
        //    clPesoRecibDato.PaddingBottom = 5f;
        //    tblCinco.AddCell(clPesoRecibDato);

        //    PdfPCell clPesoMaloDato = new PdfPCell(new Phrase("PesoMaloDato", _sFontNormal));
        //    clPesoMaloDato = new PdfPCell(new Phrase(oLis_BE_DocumentoOrigen[0].dPesoMalo.ToString(), _sFontNormal));
        //    clPesoMaloDato.BorderWidth = 0.75f;
        //    clPesoMaloDato.HorizontalAlignment = 2;
        //    clPesoMaloDato.PaddingTop = 5f;
        //    clPesoMaloDato.PaddingBottom = 5f;
        //    tblCinco.AddCell(clPesoMaloDato);

        //    PdfPCell clPesoFaltanteDato = new PdfPCell(new Phrase("PesoFaltanteDato", _sFontNormal));
        //    clPesoFaltanteDato = new PdfPCell(new Phrase(oLis_BE_DocumentoOrigen[0].dPesoFaltante.ToString(), _sFontNormal));
        //    clPesoFaltanteDato.BorderWidth = 0.75f;
        //    clPesoFaltanteDato.HorizontalAlignment = 2;
        //    clPesoFaltanteDato.PaddingTop = 5f;
        //    clPesoFaltanteDato.PaddingBottom = 5f;
        //    tblCinco.AddCell(clPesoFaltanteDato);

        //    PdfPCell clPesoSobranteDato = new PdfPCell(new Phrase("PesoSobranteDato", _sFontNormal));
        //    clPesoSobranteDato = new PdfPCell(new Phrase(oLis_BE_DocumentoOrigen[0].dPesoSobrante.ToString(), _sFontNormal));
        //    clPesoSobranteDato.BorderWidth = 0.75f;
        //    clPesoSobranteDato.HorizontalAlignment = 2;
        //    clPesoSobranteDato.PaddingTop = 5f;
        //    clPesoSobranteDato.PaddingBottom = 5f;
        //    tblCinco.AddCell(clPesoSobranteDato);

        //    //Relaciona la tabla cinco en la uno
        //    PdfPCell clFila_5_1 = new PdfPCell(tblCinco);
        //    clFila_5_1.Colspan = 3;
        //    clFila_5_1.BorderWidth = 0;
        //    tblUno.AddCell(clFila_5_1);


        //    //Genera la tabla 
        //    PdfPTable tblSeis = new PdfPTable(6);
        //    tblSeis.TotalWidth = 830f;
        //    float[] widthsSeis = new float[] { 100f, 150f, 150f, 150f, 150f, 150f };
        //    tblSeis.SetWidths(widthsSeis);
        //    tblSeis.HorizontalAlignment = 0;
        //    tblSeis.SpacingBefore = 20f;

        //    PdfPCell clDiceContenerTitulo = new PdfPCell(new Phrase("DiceContenerTitulo", _sFontNormal));
        //    clDiceContenerTitulo = new PdfPCell(new Phrase("Dice Contener", _sFontNormal));
        //    clDiceContenerTitulo.BorderWidth = 0;
        //    clDiceContenerTitulo.HorizontalAlignment = 0;
        //    tblSeis.AddCell(clDiceContenerTitulo);

        //    PdfPCell clDiceContenerDato = new PdfPCell(new Phrase("DiceContenerDato", _sFontNormal));
        //    clDiceContenerDato = new PdfPCell(new Phrase(Convert.ToInt32(oLis_BE_DocumentoOrigen[0].dDiceContener).ToString(), _sFontNormal));
        //    clDiceContenerDato.BorderWidth = 0;
        //    clDiceContenerDato.HorizontalAlignment = 0;
        //    tblSeis.AddCell(clDiceContenerDato);

        //    PdfPCell clEmpaquesTitulo = new PdfPCell(new Phrase("EmpaquesTituloTitulo", _sFontNormal));
        //    clEmpaquesTitulo = new PdfPCell(new Phrase("Empaques Recibidos", _sFontNormal));
        //    clEmpaquesTitulo.BorderWidth = 0;
        //    clEmpaquesTitulo.HorizontalAlignment = 0;
        //    tblSeis.AddCell(clEmpaquesTitulo);

        //    PdfPCell clEmpaquesDato = new PdfPCell(new Phrase("EmpaquesDato", _sFontNormal));
        //    clEmpaquesDato = new PdfPCell(new Phrase(Convert.ToInt32(oLis_BE_DocumentoOrigen[0].dEmpaqueRecibido).ToString(), _sFontNormal));
        //    clEmpaquesDato.BorderWidth = 0;
        //    clEmpaquesDato.HorizontalAlignment = 0;
        //    tblSeis.AddCell(clEmpaquesDato);

        //    PdfPCell clEstadoTitulo = new PdfPCell(new Phrase("EstadoTitulo", _sFontNormal));
        //    clEstadoTitulo = new PdfPCell(new Phrase("Estado Transmisión", _sFontNormal));
        //    clEstadoTitulo.BorderWidth = 0;
        //    clEstadoTitulo.HorizontalAlignment = 0;
        //    tblSeis.AddCell(clEstadoTitulo);

        //    PdfPCell clEstadoDato = new PdfPCell(new Phrase("EstadoDato", _sFontEstado));
        //    clEstadoDato = new PdfPCell(new Phrase(oLis_BE_DocumentoOrigen[0].sEstadoTransmitido, _sFontEstado));
        //    clEstadoDato.BorderWidth = 0;
        //    clEstadoDato.HorizontalAlignment = 0;
        //    tblSeis.AddCell(clEstadoDato);

        //    //Relaciona la tabla seis en la uno
        //    PdfPCell clFila_6_1 = new PdfPCell(tblSeis);
        //    clFila_6_1.Colspan = 3;
        //    clFila_6_1.BorderWidth = 0;
        //    tblUno.AddCell(clFila_6_1);


        //    //Genera la tabla 
        //    PdfPTable tblSiete = new PdfPTable(1);
        //    tblSiete.TotalWidth = 830f;
        //    float[] widthsSiete = new float[] { 830f };
        //    tblSiete.SetWidths(widthsSiete);
        //    tblSiete.HorizontalAlignment = 0;
        //    tblSiete.SpacingBefore = 10f;

        //    PdfPCell clMercaderiaTitulo = new PdfPCell(new Phrase("MercaderiaTitulo", _sFontNormalNegrita));
        //    clMercaderiaTitulo = new PdfPCell(new Phrase("Mercadería", _sFontNormalNegrita));
        //    clMercaderiaTitulo.BorderWidth = 0;
        //    clMercaderiaTitulo.HorizontalAlignment = 0;
        //    tblSiete.AddCell(clMercaderiaTitulo);

        //    PdfPCell clMercaderiaDato = new PdfPCell(new Phrase("MercaderiaDato", _sFontNormal));
        //    clMercaderiaDato = new PdfPCell(new Phrase(oLis_BE_DocumentoOrigen[0].sMercaderia, _sFontNormal));
        //    clMercaderiaDato.BorderWidth = 0;
        //    clMercaderiaDato.HorizontalAlignment = 0;
        //    tblSiete.AddCell(clMercaderiaDato);

        //    PdfPCell clVacio = new PdfPCell(new Phrase("Vacio", _sFontNormalNegrita));
        //    clVacio = new PdfPCell(new Phrase(" ", _sFontNormalNegrita));
        //    clVacio.BorderWidth = 0;
        //    clVacio.HorizontalAlignment = 0;
        //    tblSiete.AddCell(clVacio);

        //    PdfPCell clObservacionTitulo = new PdfPCell(new Phrase("ObservacionTitulo", _sFontNormalNegrita));
        //    clObservacionTitulo = new PdfPCell(new Phrase("Observación", _sFontNormalNegrita));
        //    clObservacionTitulo.BorderWidth = 0;
        //    clObservacionTitulo.HorizontalAlignment = 0;
        //    tblSiete.AddCell(clObservacionTitulo);

        //    PdfPCell clObservacionDato = new PdfPCell(new Phrase("ObservacionDato", _sFontNormal));
        //    clObservacionDato = new PdfPCell(new Phrase(oLis_BE_DocumentoOrigen[0].sObservacionAduana, _sFontNormal));
        //    clObservacionDato.BorderWidth = 0;
        //    clObservacionDato.HorizontalAlignment = 0;
        //    tblSiete.AddCell(clObservacionDato);

        //    //Relaciona la tabla seis en la uno
        //    PdfPCell clFila_7_1 = new PdfPCell(tblSiete);
        //    clFila_7_1.Colspan = 3;
        //    clFila_7_1.BorderWidth = 0;
        //    tblUno.AddCell(clFila_7_1);


        //    //Genera la tabla 
        //    PdfPTable tblOcho = new PdfPTable(12);
        //    tblOcho.TotalWidth = 830f;
        //    float[] widthsOcho = new float[] { 20f, 70f, 20f, 20f, 30f, 50f, 150f, 80f, 60f, 60f, 60f, 60f };
        //    tblOcho.SetWidths(widthsOcho);
        //    tblOcho.HorizontalAlignment = 0;
        //    tblOcho.SpacingBefore = 10f;

        //    PdfPCell clItemTitulo = new PdfPCell(new Phrase("ItemTitulo", _sFontDetalleNegrita));
        //    clItemTitulo = new PdfPCell(new Phrase("Id", _sFontDetalleNegrita));
        //    clItemTitulo.BorderWidth = 0.75f;
        //    clItemTitulo.HorizontalAlignment = 1;
        //    tblOcho.AddCell(clItemTitulo);

        //    PdfPCell clContenedorTitulo = new PdfPCell(new Phrase("ContenedorTitulo", _sFontDetalleNegrita));
        //    clContenedorTitulo = new PdfPCell(new Phrase("Contenedor", _sFontDetalleNegrita));
        //    clContenedorTitulo.BorderWidth = 0.75f;
        //    clContenedorTitulo.HorizontalAlignment = 1;
        //    tblOcho.AddCell(clContenedorTitulo);

        //    PdfPCell clTamanoCtnTitulo = new PdfPCell(new Phrase("TamanoTitulo", _sFontDetalleNegrita));
        //    clTamanoCtnTitulo = new PdfPCell(new Phrase("Tn", _sFontDetalleNegrita));
        //    clTamanoCtnTitulo.BorderWidth = 0.75f;
        //    clTamanoCtnTitulo.HorizontalAlignment = 1;
        //    tblOcho.AddCell(clTamanoCtnTitulo);

        //    PdfPCell clTipoCtnTitullo = new PdfPCell(new Phrase("TipoCtnTitulo", _sFontDetalleNegrita));
        //    clTipoCtnTitullo = new PdfPCell(new Phrase("Tip", _sFontDetalleNegrita));
        //    clTipoCtnTitullo.BorderWidth = 0.75f;
        //    clTipoCtnTitullo.HorizontalAlignment = 1;
        //    tblOcho.AddCell(clTipoCtnTitullo);

        //    PdfPCell clTaraTitulo = new PdfPCell(new Phrase("TaraTitulo", _sFontDetalleNegrita));
        //    clTaraTitulo = new PdfPCell(new Phrase("Tara", _sFontDetalleNegrita));
        //    clTaraTitulo.BorderWidth = 0.75f;
        //    clTaraTitulo.HorizontalAlignment = 1;
        //    tblOcho.AddCell(clTaraTitulo);

        //    PdfPCell clFechaIngresoDetTitulo = new PdfPCell(new Phrase("FechaIngresoTitulo", _sFontDetalleNegrita));
        //    clFechaIngresoDetTitulo = new PdfPCell(new Phrase("Fecha Ingreso", _sFontDetalleNegrita));
        //    clFechaIngresoDetTitulo.BorderWidth = 0.75f;
        //    clFechaIngresoDetTitulo.HorizontalAlignment = 1;
        //    tblOcho.AddCell(clFechaIngresoDetTitulo);

        //    PdfPCell clTipoCargaTitulo = new PdfPCell(new Phrase("TipoCargaTitulo", _sFontDetalleNegrita));
        //    clTipoCargaTitulo = new PdfPCell(new Phrase("Descripción", _sFontDetalleNegrita));
        //    clTipoCargaTitulo.BorderWidth = 0.75f;
        //    clTipoCargaTitulo.HorizontalAlignment = 1;
        //    tblOcho.AddCell(clTipoCargaTitulo);

        //    PdfPCell clObservacionDetTitulo = new PdfPCell(new Phrase("ObservacionDetTitulo", _sFontDetalleNegrita));
        //    clObservacionDetTitulo = new PdfPCell(new Phrase("Observación", _sFontDetalleNegrita));
        //    clObservacionDetTitulo.BorderWidth = 0.75f;
        //    clObservacionDetTitulo.HorizontalAlignment = 1;
        //    tblOcho.AddCell(clObservacionDetTitulo);

        //    PdfPCell clBultosManifDatoDetalleTitulo = new PdfPCell(new Phrase("BultosManifDatoDetalleTitulo", _sFontDetalleNegrita));
        //    clBultosManifDatoDetalleTitulo = new PdfPCell(new Phrase("Manifestado", _sFontDetalleNegrita));
        //    clBultosManifDatoDetalleTitulo.BorderWidth = 0.75f;
        //    clBultosManifDatoDetalleTitulo.HorizontalAlignment = 2;
        //    tblOcho.AddCell(clBultosManifDatoDetalleTitulo);

        //    PdfPCell clPesoManifDatoDetalleTitulo = new PdfPCell(new Phrase("PesoManifDatoDetalleTitulo", _sFontDetalleNegrita));
        //    clPesoManifDatoDetalleTitulo = new PdfPCell(new Phrase("Actual", _sFontDetalleNegrita));
        //    clPesoManifDatoDetalleTitulo.BorderWidth = 0.75f;
        //    clPesoManifDatoDetalleTitulo.HorizontalAlignment = 2;
        //    tblOcho.AddCell(clPesoManifDatoDetalleTitulo);

        //    PdfPCell clPrecintoManifTitulo = new PdfPCell(new Phrase("PrecintoManifTitulo", _sFontDetalleNegrita));
        //    clPrecintoManifTitulo = new PdfPCell(new Phrase("Manifestado", _sFontDetalleNegrita));
        //    clPrecintoManifTitulo.BorderWidth = 0.75f;
        //    clPrecintoManifTitulo.HorizontalAlignment = 2;
        //    tblOcho.AddCell(clPrecintoManifTitulo);

        //    PdfPCell clPrecintoRecibTitulo = new PdfPCell(new Phrase("PrecintoRecibTitulo", _sFontDetalleNegrita));
        //    clPrecintoRecibTitulo = new PdfPCell(new Phrase("Actual", _sFontDetalleNegrita));
        //    clPrecintoRecibTitulo.BorderWidth = 0.75f;
        //    clPrecintoRecibTitulo.HorizontalAlignment = 2;
        //    tblOcho.AddCell(clPrecintoRecibTitulo);

        //    for (int i = 0; i < oLis_BE_DocumentoOrigen.Count; i++)
        //    {
        //        PdfPCell clItem = new PdfPCell(new Phrase("Item", _sFontDetalle));
        //        clItem = new PdfPCell(new Phrase((i + 1).ToString(), _sFontDetalle));
        //        clItem.BorderWidth = 0.75f;
        //        clItem.HorizontalAlignment = 1;
        //        tblOcho.AddCell(clItem);

        //        PdfPCell clContenedor = new PdfPCell(new Phrase("Contenedor", _sFontDetalle));
        //        clContenedor = new PdfPCell(new Phrase(oLis_BE_DocumentoOrigen[i].sContenedor, _sFontDetalle));
        //        clContenedor.BorderWidth = 0.75f;
        //        clContenedor.HorizontalAlignment = 1;
        //        tblOcho.AddCell(clContenedor);

        //        PdfPCell clTamanoCtn = new PdfPCell(new Phrase("Tamano", _sFontDetalle));
        //        clTamanoCtn = new PdfPCell(new Phrase(oLis_BE_DocumentoOrigen[i].sTamano, _sFontDetalle));
        //        clTamanoCtn.BorderWidth = 0.75f;
        //        clTamanoCtn.HorizontalAlignment = 1;
        //        tblOcho.AddCell(clTamanoCtn);

        //        PdfPCell clTipoCtn = new PdfPCell(new Phrase("TipoCtn", _sFontDetalle));
        //        clTipoCtn = new PdfPCell(new Phrase(oLis_BE_DocumentoOrigen[i].sTipoCtn, _sFontDetalle));
        //        clTipoCtn.BorderWidth = 0.75f;
        //        clTipoCtn.HorizontalAlignment = 1;
        //        tblOcho.AddCell(clTipoCtn);

        //        PdfPCell clTara = new PdfPCell(new Phrase("Tara", _sFontDetalle));
        //        clTara = new PdfPCell(new Phrase(Convert.ToInt32(oLis_BE_DocumentoOrigen[i].dTara).ToString(), _sFontDetalle));
        //        clTara.BorderWidth = 0.75f;
        //        clTara.HorizontalAlignment = 2;
        //        tblOcho.AddCell(clTara);

        //        PdfPCell clFechaIngresoDet = new PdfPCell(new Phrase("FechaIngreso", _sFontDetalle));
        //        clFechaIngresoDet = new PdfPCell(new Phrase(oLis_BE_DocumentoOrigen[i].sFechaIngreso, _sFontDetalle));
        //        clFechaIngresoDet.BorderWidth = 0.75f;
        //        clFechaIngresoDet.HorizontalAlignment = 1;
        //        tblOcho.AddCell(clFechaIngresoDet);

        //        PdfPCell clTipoCarga = new PdfPCell(new Phrase("TipoCarga", _sFontDetalle));
        //        clTipoCarga = new PdfPCell(new Phrase(oLis_BE_DocumentoOrigen[i].sTipoCarga, _sFontDetalle));
        //        clTipoCarga.BorderWidth = 0.75f;
        //        clTipoCarga.HorizontalAlignment = 0;
        //        tblOcho.AddCell(clTipoCarga);

        //        PdfPCell clObservacionDet = new PdfPCell(new Phrase("ObservacionDet", _sFontDetalle));
        //        clObservacionDet = new PdfPCell(new Phrase(oLis_BE_DocumentoOrigen[i].sObservacion, _sFontDetalle));
        //        clObservacionDet.BorderWidth = 0.75f;
        //        clObservacionDet.HorizontalAlignment = 0;
        //        tblOcho.AddCell(clObservacionDet);

        //        PdfPCell clPesoManifDatoDetalle = new PdfPCell(new Phrase("PesoManifDatoDetalle", _sFontDetalle));
        //        clPesoManifDatoDetalle = new PdfPCell(new Phrase(oLis_BE_DocumentoOrigen[0].dPesoManif.ToString(), _sFontDetalle));
        //        clPesoManifDatoDetalle.BorderWidth = 0.75f;
        //        clPesoManifDatoDetalle.HorizontalAlignment = 2;
        //        tblOcho.AddCell(clPesoManifDatoDetalle);

        //        PdfPCell clPesoRecibDatoDetalle = new PdfPCell(new Phrase("PesoRecibDatoDetalle", _sFontDetalle));
        //        clPesoRecibDatoDetalle = new PdfPCell(new Phrase(oLis_BE_DocumentoOrigen[0].dPesoRecibido.ToString(), _sFontDetalle));
        //        clPesoRecibDatoDetalle.BorderWidth = 0.75f;
        //        clPesoRecibDatoDetalle.HorizontalAlignment = 2;
        //        tblOcho.AddCell(clPesoRecibDatoDetalle);

        //        PdfPCell clPrecintoManif = new PdfPCell(new Phrase("PrecintoManif", _sFontDetalle));
        //        clPrecintoManif = new PdfPCell(new Phrase(oLis_BE_DocumentoOrigen[i].sPrecintoManifestado, _sFontDetalle));
        //        clPrecintoManif.BorderWidth = 0.75f;
        //        clPrecintoManif.HorizontalAlignment = 0;
        //        tblOcho.AddCell(clPrecintoManif);

        //        PdfPCell clPrecintoRecib = new PdfPCell(new Phrase("PrecintoRecib", _sFontDetalle));
        //        clPrecintoRecib = new PdfPCell(new Phrase(oLis_BE_DocumentoOrigen[i].sPrecintoRecepcionado, _sFontDetalle));
        //        clPrecintoRecib.BorderWidth = 0.75f;
        //        clPrecintoRecib.HorizontalAlignment = 0;
        //        tblOcho.AddCell(clPrecintoRecib);
        //    }

        //    //Relaciona la tabla seis en la uno
        //    PdfPCell clFila_8_1 = new PdfPCell(tblOcho);
        //    clFila_8_1.Colspan = 3;
        //    clFila_8_1.BorderWidth = 0;
        //    tblUno.AddCell(clFila_8_1);


        //    //Muestra las tablas en el documento
        //    pdfDoc.Add(tblUno);
        //    pdfDoc.Close();


        //    Response.ContentType = "application/pdf";
        //    Response.AddHeader("content-disposition", "attachment;filename=Fargoline.pdf");
        //    ////////Response.Cache.SetCacheability(HttpCacheability.NoCache);
        //    Response.ContentEncoding = Encoding.Default;
        //    Response.Write(pdfDoc);
        //    Response.End();
        //}
    }

    public void CreaPDFVolante(List<BE_DocumentoOrigen> oLis_BE_DocumentoOrigen, String sUsuario)
    {
        //List<BE_DocumentoOrigen> oLis_BE_DocumentoOrigen = new List<BE_DocumentoOrigen>();
        //BE_DocumentoOrigen oBE_DocumentoOrigen = new BE_DocumentoOrigen();
        //BL_DocumentoOrigen oBL_DocumentoOrigen = new BL_DocumentoOrigen();

        //oBE_DocumentoOrigen.iIdVolante = iIdVolante;
        //oBE_DocumentoOrigen.sUsuario = sUsuario;
        //oBE_DocumentoOrigen.iIdDocOri = iIdDocOrigen;

        //oLis_BE_DocumentoOrigen = oBL_DocumentoOrigen.ListarDatosVolante(oBE_DocumentoOrigen);
        byte[] result;

        if ((oLis_BE_DocumentoOrigen != null) && (oLis_BE_DocumentoOrigen.Count > 0))
        {
            using (System.IO.MemoryStream ms = new System.IO.MemoryStream())
            {


                Document pdfDoc = new Document(PageSize.A4, 10f, 10f, 10f, 10f);
                //   PdfWriter.GetInstance(pdfDoc, System.Web.HttpContext.Current.Response.OutputStream);
                PdfWriter.GetInstance(pdfDoc, ms);
                pdfDoc.Open();

                // Le colocamos el título y el autor
                pdfDoc.AddTitle("Volante de Extranet");
                pdfDoc.AddAuthor("Extranet Fargoline");

                iTextSharp.text.Font _sFontNormal = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 8, iTextSharp.text.Font.NORMAL, BaseColor.BLACK);
                iTextSharp.text.Font _sFontNormalNegrita = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 8, iTextSharp.text.Font.BOLD, BaseColor.BLACK);
                iTextSharp.text.Font _sFontTituloCentral = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 14, iTextSharp.text.Font.BOLD, BaseColor.BLACK);
                iTextSharp.text.Font _sFontTituloIzquierda = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 5, iTextSharp.text.Font.ITALIC, BaseColor.BLACK);
                iTextSharp.text.Font _sFontEstado = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 12, iTextSharp.text.Font.BOLD, BaseColor.BLACK);
                iTextSharp.text.Font _sFontDetalle = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 7, iTextSharp.text.Font.NORMAL, BaseColor.BLACK);
                iTextSharp.text.Font _sFontDetalleNegrita = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 7, iTextSharp.text.Font.BOLD, BaseColor.BLACK);
                iTextSharp.text.Font _sFontInmovilizado = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 7, iTextSharp.text.Font.NORMAL, BaseColor.RED);
                iTextSharp.text.Font _sFontInmovilizadoNegrita = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 7, iTextSharp.text.Font.BOLD, BaseColor.RED);
                iTextSharp.text.Font _sFontInmovilizadoPequena = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 6, iTextSharp.text.Font.NORMAL, BaseColor.RED);
                iTextSharp.text.Font _sFontDetallePequena = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 6, iTextSharp.text.Font.NORMAL, BaseColor.BLACK);


                //Genera la tabla 
                PdfPTable tblUno = new PdfPTable(3);
                tblUno.WidthPercentage = 100;


                // Creamos la imagen y le ajustamos el tamaño 
                iTextSharp.text.Image imagen = iTextSharp.text.Image.GetInstance(System.Web.HttpContext.Current.Server.MapPath("~/Imagenes/logo.jpg"));
                imagen.BorderWidth = 0;
                float percentage = 0.0f;
                percentage = (100 / imagen.Width) - 80;
                imagen.ScalePercent(percentage * 100);

                //Genera la celda
                PdfPCell clFila_1_1 = new PdfPCell(new Phrase("Fila_1_1", _sFontNormal));
                clFila_1_1 = new PdfPCell(new Phrase("Fargoline S.A.", _sFontNormal));
                clFila_1_1.BorderWidth = 0;
                clFila_1_1.HorizontalAlignment = 0;
                tblUno.AddCell(imagen);

                //Genera la celda
                PdfPCell clFila_1_2 = new PdfPCell(new Phrase("Fila_1_2", _sFontTituloCentral));
                clFila_1_2 = new PdfPCell(new Phrase("Volante Nro: " + oLis_BE_DocumentoOrigen[0].sVolante.Substring(5, 10), _sFontTituloCentral));
                clFila_1_2.BorderWidth = 0;
                clFila_1_2.HorizontalAlignment = 1;
                tblUno.AddCell(clFila_1_2);


                //Genera la tabla 
                PdfPTable tblDos = new PdfPTable(1);
                tblDos.WidthPercentage = 100;

                PdfPCell clFechaTitulo = new PdfPCell(new Phrase("FechaTitulo", _sFontTituloIzquierda));
                clFechaTitulo = new PdfPCell(new Phrase(DateTime.Now.ToString(), _sFontTituloIzquierda));
                clFechaTitulo.BorderWidth = 0;
                clFechaTitulo.HorizontalAlignment = 2;
                tblDos.AddCell(clFechaTitulo);

                PdfPCell clUsuarioTitulo = new PdfPCell(new Phrase("UsuarioTitulo", _sFontTituloIzquierda));
                clUsuarioTitulo = new PdfPCell(new Phrase("Impreso por: " + sUsuario, _sFontTituloIzquierda));
                clUsuarioTitulo.BorderWidth = 0;
                clUsuarioTitulo.HorizontalAlignment = 2;
                tblDos.AddCell(clUsuarioTitulo);

                PdfPCell clUsuarioGeneraTitulo = new PdfPCell(new Phrase("UsuarioTitulo", _sFontTituloIzquierda));
                clUsuarioGeneraTitulo = new PdfPCell(new Phrase("Generado por: " + oLis_BE_DocumentoOrigen[0].sUsuarioGenera, _sFontTituloIzquierda));
                clUsuarioGeneraTitulo.BorderWidth = 0;
                clUsuarioGeneraTitulo.HorizontalAlignment = 2;
                tblDos.AddCell(clUsuarioGeneraTitulo);

                //Relaciona la tabla dos en la uno
                PdfPCell clFila_1_3 = new PdfPCell(tblDos);
                clFila_1_3.BorderWidth = 0;
                tblUno.AddCell(clFila_1_3);


                PdfPCell clFila_2_1 = new PdfPCell(new Phrase("Fila_2_1"));
                clFila_2_1 = new PdfPCell(new Phrase(" ", _sFontTituloIzquierda));
                clFila_2_1.Colspan = 3;
                clFila_2_1.BorderWidth = 0;
                tblUno.AddCell(clFila_2_1);


                //Genera la tabla 
                PdfPTable tblTres = new PdfPTable(6);
                tblTres.TotalWidth = 830f;
                float[] widthsTres = new float[] { 100f, 200f, 110f, 200f, 120f, 100f };
                tblTres.SetWidths(widthsTres);
                tblTres.HorizontalAlignment = 0;
                tblTres.SpacingBefore = 10f;

                PdfPCell clManifiesto = new PdfPCell(new Phrase("Manifiesto", _sFontNormalNegrita));
                clManifiesto = new PdfPCell(new Phrase("Manifiesto:", _sFontNormalNegrita));
                clManifiesto.BorderWidth = 0;
                clManifiesto.HorizontalAlignment = 0;
                clManifiesto.BorderWidthTop = 1f;
                clManifiesto.BorderWidthLeft = 1f;
                clManifiesto.PaddingLeft = 8f;
                clManifiesto.PaddingTop = 5f;
                tblTres.AddCell(clManifiesto);

                PdfPCell clManifiestoDato = new PdfPCell(new Phrase("ManifiestoDato", _sFontNormal));
                clManifiestoDato = new PdfPCell(new Phrase(oLis_BE_DocumentoOrigen[0].sManifiesto, _sFontNormal));
                clManifiestoDato.BorderWidth = 0;
                clManifiestoDato.HorizontalAlignment = 0;
                clManifiestoDato.BorderWidthTop = 1f;
                clManifiestoDato.PaddingTop = 5f;
                tblTres.AddCell(clManifiestoDato);

                PdfPCell clDoHijo = new PdfPCell(new Phrase("DoHijo", _sFontNormalNegrita));
                clDoHijo = new PdfPCell(new Phrase("Do Hijo:", _sFontNormalNegrita));
                clDoHijo.BorderWidth = 0;
                clDoHijo.HorizontalAlignment = 0;
                clDoHijo.BorderWidthTop = 1f;
                clDoHijo.PaddingTop = 5f;
                tblTres.AddCell(clDoHijo);

                PdfPCell clDoHijoDato = new PdfPCell(new Phrase("DoHijoDato", _sFontNormal));
                clDoHijoDato = new PdfPCell(new Phrase(oLis_BE_DocumentoOrigen[0].sDocumentoHijo, _sFontNormal));
                clDoHijoDato.BorderWidth = 0;
                clDoHijoDato.HorizontalAlignment = 0;
                clDoHijoDato.BorderWidthTop = 1f;
                clDoHijoDato.PaddingTop = 5f;
                tblTres.AddCell(clDoHijoDato);

                PdfPCell clFechaLlegada = new PdfPCell(new Phrase("FechaLlegada", _sFontNormalNegrita));
                clFechaLlegada = new PdfPCell(new Phrase("Fecha Llegada:", _sFontNormalNegrita));
                clFechaLlegada.BorderWidth = 0;
                clFechaLlegada.HorizontalAlignment = 0;
                clFechaLlegada.BorderWidthTop = 1f;
                clFechaLlegada.PaddingTop = 5f;
                tblTres.AddCell(clFechaLlegada);

                PdfPCell clFechaLlegadaDato = new PdfPCell(new Phrase("FechaLlegadaDato", _sFontNormal));
                clFechaLlegadaDato = new PdfPCell(new Phrase(oLis_BE_DocumentoOrigen[0].sFechaLlegada, _sFontNormal));
                clFechaLlegadaDato.BorderWidth = 0;
                clFechaLlegadaDato.HorizontalAlignment = 0;
                clFechaLlegadaDato.BorderWidthTop = 1f;
                clFechaLlegadaDato.BorderWidthRight = 1f;
                clFechaLlegadaDato.PaddingTop = 5f;
                tblTres.AddCell(clFechaLlegadaDato);


                PdfPCell clNave = new PdfPCell(new Phrase("Nave", _sFontNormalNegrita));
                clNave = new PdfPCell(new Phrase("Nave:", _sFontNormalNegrita));
                clNave.BorderWidth = 0;
                clNave.HorizontalAlignment = 0;
                clNave.BorderWidthLeft = 1f;
                clNave.PaddingLeft = 8f;
                tblTres.AddCell(clNave);

                PdfPCell clNaveDato = new PdfPCell(new Phrase("NaveDato", _sFontNormal));
                clNaveDato = new PdfPCell(new Phrase(oLis_BE_DocumentoOrigen[0].sNave, _sFontNormal));
                clNaveDato.BorderWidth = 0;
                clNaveDato.HorizontalAlignment = 0;
                tblTres.AddCell(clNaveDato);

                PdfPCell clDoMadre = new PdfPCell(new Phrase("DoMadre", _sFontNormalNegrita));
                clDoMadre = new PdfPCell(new Phrase("Do Madre:", _sFontNormalNegrita));
                clDoMadre.BorderWidth = 0;
                clDoMadre.HorizontalAlignment = 0;
                tblTres.AddCell(clDoMadre);

                PdfPCell clDoMadreDato = new PdfPCell(new Phrase("DoMadreDato", _sFontNormal));
                clDoMadreDato = new PdfPCell(new Phrase(oLis_BE_DocumentoOrigen[0].sDocumentoMaster, _sFontNormal));
                clDoMadreDato.BorderWidth = 0;
                clDoMadreDato.HorizontalAlignment = 0;
                tblTres.AddCell(clDoMadreDato);

                PdfPCell clFechaTermino = new PdfPCell(new Phrase("FechaTermino", _sFontNormalNegrita));
                clFechaTermino = new PdfPCell(new Phrase("Fecha Termino:", _sFontNormalNegrita));
                clFechaTermino.BorderWidth = 0;
                clFechaTermino.HorizontalAlignment = 0;
                tblTres.AddCell(clFechaTermino);

                PdfPCell clFechaTerminoDato = new PdfPCell(new Phrase("FechaTerminoDato", _sFontNormal));
                clFechaTerminoDato = new PdfPCell(new Phrase(oLis_BE_DocumentoOrigen[0].sFechaTermino, _sFontNormal));
                clFechaTerminoDato.BorderWidth = 0;
                clFechaTerminoDato.HorizontalAlignment = 0;
                clFechaTerminoDato.BorderWidthRight = 1f;
                tblTres.AddCell(clFechaTerminoDato);


                PdfPCell clViaje = new PdfPCell(new Phrase("Viaje", _sFontNormalNegrita));
                clViaje = new PdfPCell(new Phrase("Viaje:", _sFontNormalNegrita));
                clViaje.BorderWidth = 0;
                clViaje.HorizontalAlignment = 0;
                clViaje.BorderWidthLeft = 1f;
                clViaje.PaddingLeft = 8f;
                tblTres.AddCell(clViaje);

                PdfPCell clViajeDato = new PdfPCell(new Phrase("ViajeDato", _sFontNormal));
                clViajeDato = new PdfPCell(new Phrase(oLis_BE_DocumentoOrigen[0].sViaje, _sFontNormal));
                clViajeDato.BorderWidth = 0;
                clViajeDato.HorizontalAlignment = 0;
                tblTres.AddCell(clViajeDato);

                PdfPCell clPtoEmbarque = new PdfPCell(new Phrase("PtoEmbarque", _sFontNormalNegrita));
                clPtoEmbarque = new PdfPCell(new Phrase("Pto Embarque:", _sFontNormalNegrita));
                clPtoEmbarque.BorderWidth = 0;
                clPtoEmbarque.HorizontalAlignment = 0;
                tblTres.AddCell(clPtoEmbarque);

                PdfPCell clPtoEmbarqueDato = new PdfPCell(new Phrase("PtoEmbarqueDato", _sFontNormal));
                clPtoEmbarqueDato = new PdfPCell(new Phrase(oLis_BE_DocumentoOrigen[0].sPtoEmbarque, _sFontNormal));
                clPtoEmbarqueDato.BorderWidth = 0;
                clPtoEmbarqueDato.HorizontalAlignment = 0;
                tblTres.AddCell(clPtoEmbarqueDato);

                PdfPCell clDetalleAduana = new PdfPCell(new Phrase("DetalleAduana", _sFontNormalNegrita));
                clDetalleAduana = new PdfPCell(new Phrase("Detalle Aduana:", _sFontNormalNegrita));
                clDetalleAduana.BorderWidth = 0;
                clDetalleAduana.HorizontalAlignment = 0;
                tblTres.AddCell(clDetalleAduana);

                PdfPCell clDetalleAduanaData = new PdfPCell(new Phrase("DetalleAduanaData", _sFontNormal));
                clDetalleAduanaData = new PdfPCell(new Phrase(oLis_BE_DocumentoOrigen[0].sDetalleAduana, _sFontNormal));
                clDetalleAduanaData.BorderWidth = 0;
                clDetalleAduanaData.HorizontalAlignment = 0;
                clDetalleAduanaData.BorderWidthRight = 1f;
                tblTres.AddCell(clDetalleAduanaData);


                PdfPCell clRumbo = new PdfPCell(new Phrase("Rumbo", _sFontNormalNegrita));
                clRumbo = new PdfPCell(new Phrase("Rumbo:", _sFontNormalNegrita));
                clRumbo.BorderWidth = 0;
                clRumbo.HorizontalAlignment = 0;
                clRumbo.BorderWidthLeft = 1f;
                clRumbo.PaddingLeft = 8f;
                tblTres.AddCell(clRumbo);

                PdfPCell clRumboDato = new PdfPCell(new Phrase("RumboDato", _sFontNormal));
                clRumboDato = new PdfPCell(new Phrase(oLis_BE_DocumentoOrigen[0].sRumbo, _sFontNormal));
                clRumboDato.BorderWidth = 0;
                clRumboDato.HorizontalAlignment = 0;
                tblTres.AddCell(clRumboDato);

                PdfPCell clPtoFinal = new PdfPCell(new Phrase("PtoFinal", _sFontNormalNegrita));
                clPtoFinal = new PdfPCell(new Phrase("Pto Final:", _sFontNormalNegrita));
                clPtoFinal.BorderWidth = 0;
                clPtoFinal.HorizontalAlignment = 0;
                tblTres.AddCell(clPtoFinal);

                PdfPCell clPtoFinalDato = new PdfPCell(new Phrase("PtoFinalDato", _sFontNormal));
                clPtoFinalDato = new PdfPCell(new Phrase(oLis_BE_DocumentoOrigen[0].sPtoFinal, _sFontNormal));
                clPtoFinalDato.BorderWidth = 0;
                clPtoFinalDato.HorizontalAlignment = 0;
                tblTres.AddCell(clPtoFinalDato);

                PdfPCell clTipoOperacion = new PdfPCell(new Phrase("TipoOperacion", _sFontNormalNegrita));
                clTipoOperacion = new PdfPCell(new Phrase("Tipo Operación:", _sFontNormalNegrita));
                clTipoOperacion.BorderWidth = 0;
                clTipoOperacion.HorizontalAlignment = 0;
                tblTres.AddCell(clTipoOperacion);

                PdfPCell clTipoOperacionDato = new PdfPCell(new Phrase("TipoOperacionDato", _sFontNormal));
                clTipoOperacionDato = new PdfPCell(new Phrase(oLis_BE_DocumentoOrigen[0].sOperacion, _sFontNormal));
                clTipoOperacionDato.BorderWidth = 0;
                clTipoOperacionDato.HorizontalAlignment = 0;
                clTipoOperacionDato.BorderWidthRight = 1f;
                tblTres.AddCell(clTipoOperacionDato);


                PdfPCell clEmision = new PdfPCell(new Phrase("Emision", _sFontNormalNegrita));
                clEmision = new PdfPCell(new Phrase("Emisión:", _sFontNormalNegrita));
                clEmision.BorderWidth = 0;
                clEmision.HorizontalAlignment = 0;
                clEmision.BorderWidthLeft = 1f;
                clEmision.BorderWidthBottom = 1f;
                clEmision.PaddingLeft = 8f;
                clEmision.PaddingBottom = 8f;
                tblTres.AddCell(clEmision);

                PdfPCell clEmisionDato = new PdfPCell(new Phrase("EmisionDato", _sFontNormal));
                clEmisionDato = new PdfPCell(new Phrase(oLis_BE_DocumentoOrigen[0].sFechaEmision, _sFontNormal));
                clEmisionDato.BorderWidth = 0;
                clEmisionDato.HorizontalAlignment = 0;
                clEmisionDato.BorderWidthBottom = 1f;
                clEmisionDato.PaddingBottom = 8f;
                tblTres.AddCell(clEmisionDato);

                PdfPCell clTermPorturario = new PdfPCell(new Phrase("TermPorturario", _sFontNormalNegrita));
                clTermPorturario = new PdfPCell(new Phrase("Term.Porturario:", _sFontNormalNegrita));
                clTermPorturario.BorderWidth = 0;
                clTermPorturario.HorizontalAlignment = 0;
                clTermPorturario.BorderWidthBottom = 1f;
                tblTres.AddCell(clTermPorturario);

                PdfPCell clTermPorturarioDato = new PdfPCell(new Phrase("TermPorturarioDato", _sFontNormal));
                clTermPorturarioDato = new PdfPCell(new Phrase(oLis_BE_DocumentoOrigen[0].sTerminalPortuario, _sFontNormal));
                clTermPorturarioDato.BorderWidth = 0;
                clTermPorturarioDato.HorizontalAlignment = 0;
                clTermPorturarioDato.BorderWidthBottom = 1f;
                tblTres.AddCell(clTermPorturarioDato);

                PdfPCell clFechaIngreso = new PdfPCell(new Phrase("FechaIngreso", _sFontNormalNegrita));
                clFechaIngreso = new PdfPCell(new Phrase("Fecha Ingreso DO:", _sFontNormalNegrita));
                clFechaIngreso.BorderWidth = 0;
                clFechaIngreso.HorizontalAlignment = 0;
                clFechaIngreso.BorderWidthBottom = 1f;
                tblTres.AddCell(clFechaIngreso);

                PdfPCell clFechaIngresoDato = new PdfPCell(new Phrase("FechaIngresoDato", _sFontNormal));
                clFechaIngresoDato = new PdfPCell(new Phrase(oLis_BE_DocumentoOrigen[0].sFecIngresoBL, _sFontNormal));
                clFechaIngresoDato.BorderWidth = 0;
                clFechaIngresoDato.HorizontalAlignment = 0;
                clFechaIngresoDato.BorderWidthRight = 1f;
                clFechaIngresoDato.BorderWidthBottom = 1f;
                tblTres.AddCell(clFechaIngresoDato);

                //Relaciona la tabla tres en la uno
                PdfPCell clFila_3_1 = new PdfPCell(tblTres);
                clFila_3_1.Colspan = 3;
                clFila_3_1.BorderWidth = 0;
                tblUno.AddCell(clFila_3_1);


                //Genera la tabla 
                PdfPTable tblCuatro = new PdfPTable(2);
                tblCuatro.TotalWidth = 830f;
                float[] widthsCuatro = new float[] { 150f, 680f };
                tblCuatro.SetWidths(widthsCuatro);
                tblCuatro.HorizontalAlignment = 0;
                tblCuatro.SpacingBefore = 10f;

                PdfPCell clLinea = new PdfPCell(new Phrase("Linea", _sFontNormalNegrita));
                clLinea = new PdfPCell(new Phrase("Línea:", _sFontNormalNegrita));
                clLinea.BorderWidth = 0;
                clLinea.HorizontalAlignment = 0;
                clLinea.BorderWidthTop = 1f;
                clLinea.BorderWidthLeft = 1f;
                clLinea.PaddingLeft = 8f;
                clLinea.PaddingTop = 5f;
                tblCuatro.AddCell(clLinea);

                PdfPCell clLineaDato = new PdfPCell(new Phrase("LineaDato", _sFontNormal));
                clLineaDato = new PdfPCell(new Phrase(oLis_BE_DocumentoOrigen[0].sLineaMaritima, _sFontNormal));
                clLineaDato.BorderWidth = 0;
                clLineaDato.HorizontalAlignment = 0;
                clLineaDato.BorderWidthTop = 1f;
                clLineaDato.BorderWidthRight = 1f;
                clLineaDato.PaddingTop = 5f;
                tblCuatro.AddCell(clLineaDato);

                PdfPCell clConsignatario = new PdfPCell(new Phrase("Consignatario", _sFontNormalNegrita));
                clConsignatario = new PdfPCell(new Phrase("Consignatario:", _sFontNormalNegrita));
                clConsignatario.BorderWidth = 0;
                clConsignatario.HorizontalAlignment = 0;
                clConsignatario.BorderWidthLeft = 1f;
                clConsignatario.PaddingLeft = 8f;
                tblCuatro.AddCell(clConsignatario);

                PdfPCell clConsignatarioDato = new PdfPCell(new Phrase("ConsignatarioDato", _sFontNormal));
                clConsignatarioDato = new PdfPCell(new Phrase(oLis_BE_DocumentoOrigen[0].sConsignatario, _sFontNormal));
                clConsignatarioDato.BorderWidth = 0;
                clConsignatarioDato.HorizontalAlignment = 0;
                clConsignatarioDato.BorderWidthRight = 1f;
                tblCuatro.AddCell(clConsignatarioDato);

                PdfPCell clCodCLiente = new PdfPCell(new Phrase("CodCLiente", _sFontNormalNegrita));
                clCodCLiente = new PdfPCell(new Phrase("Cod/Cliente:", _sFontNormalNegrita));
                clCodCLiente.BorderWidth = 0;
                clCodCLiente.HorizontalAlignment = 0;
                clCodCLiente.BorderWidthLeft = 1f;
                clCodCLiente.PaddingLeft = 8f;
                tblCuatro.AddCell(clCodCLiente);

                PdfPCell clCodClienteDato = new PdfPCell(new Phrase("CodClienteDato", _sFontNormal));
                if (oLis_BE_DocumentoOrigen[0].sCliBloqueado == String.Empty)
                { clCodClienteDato = new PdfPCell(new Phrase(oLis_BE_DocumentoOrigen[0].sCliente, _sFontNormal)); }
                else
                { clCodClienteDato = new PdfPCell(new Phrase(oLis_BE_DocumentoOrigen[0].sCliente + " *** " + oLis_BE_DocumentoOrigen[0].sCliBloqueado, _sFontInmovilizado)); }
                clCodClienteDato.BorderWidth = 0;
                clCodClienteDato.HorizontalAlignment = 0;
                clCodClienteDato.BorderWidthRight = 1f;
                tblCuatro.AddCell(clCodClienteDato);

                PdfPCell clAgenciaAduana = new PdfPCell(new Phrase("AgenciaAduana", _sFontNormalNegrita));
                clAgenciaAduana = new PdfPCell(new Phrase("Agencia Aduana:", _sFontNormalNegrita));
                clAgenciaAduana.BorderWidth = 0;
                clAgenciaAduana.HorizontalAlignment = 0;
                clAgenciaAduana.BorderWidthLeft = 1f;
                clAgenciaAduana.PaddingLeft = 8f;
                tblCuatro.AddCell(clAgenciaAduana);

                PdfPCell clAgenciaAduanaDato = new PdfPCell(new Phrase("AgenciaAduanaDato", _sFontNormal));
                if (oLis_BE_DocumentoOrigen[0].sAgeBloqueado == String.Empty)
                { clAgenciaAduanaDato = new PdfPCell(new Phrase(oLis_BE_DocumentoOrigen[0].sAgenciaAduana, _sFontNormal)); }
                else
                { clAgenciaAduanaDato = new PdfPCell(new Phrase(oLis_BE_DocumentoOrigen[0].sAgenciaAduana + " *** " + oLis_BE_DocumentoOrigen[0].sAgeBloqueado, _sFontInmovilizado)); }
                clAgenciaAduanaDato.BorderWidth = 0;
                clAgenciaAduanaDato.HorizontalAlignment = 0;
                clAgenciaAduanaDato.BorderWidthRight = 1f;
                tblCuatro.AddCell(clAgenciaAduanaDato);

                PdfPCell clConsolidador = new PdfPCell(new Phrase("Consolidador", _sFontNormalNegrita));
                clConsolidador = new PdfPCell(new Phrase("Consolidador:", _sFontNormalNegrita));
                clConsolidador.BorderWidth = 0;
                clConsolidador.HorizontalAlignment = 0;
                clConsolidador.BorderWidthLeft = 1f;
                clConsolidador.PaddingLeft = 8f;
                tblCuatro.AddCell(clConsolidador);

                PdfPCell clConsolidadorDato = new PdfPCell(new Phrase("ConsolidadorDato", _sFontNormal));
                clConsolidadorDato = new PdfPCell(new Phrase(oLis_BE_DocumentoOrigen[0].sConsolidador, _sFontNormal));
                if (oLis_BE_DocumentoOrigen[0].sConsBloqueado == String.Empty)
                { clConsolidadorDato = new PdfPCell(new Phrase(oLis_BE_DocumentoOrigen[0].sConsolidador, _sFontNormal)); }
                else
                { clConsolidadorDato = new PdfPCell(new Phrase(oLis_BE_DocumentoOrigen[0].sConsolidador + " *** " + oLis_BE_DocumentoOrigen[0].sConsBloqueado, _sFontInmovilizado)); }
                clConsolidadorDato.BorderWidth = 0;
                clConsolidadorDato.HorizontalAlignment = 0;
                clConsolidadorDato.BorderWidthRight = 1f;
                tblCuatro.AddCell(clConsolidadorDato);

                PdfPCell clNotificante = new PdfPCell(new Phrase("Notificante", _sFontNormalNegrita));
                clNotificante = new PdfPCell(new Phrase("Notificante:", _sFontNormalNegrita));
                clNotificante.BorderWidth = 0;
                clNotificante.HorizontalAlignment = 0;
                clNotificante.BorderWidthLeft = 1f;
                clNotificante.PaddingLeft = 8f;
                tblCuatro.AddCell(clNotificante);

                PdfPCell clNotificanteDato = new PdfPCell(new Phrase("NotificanteDato", _sFontNormal));
                clNotificanteDato = new PdfPCell(new Phrase(oLis_BE_DocumentoOrigen[0].sNotificante, _sFontNormal));
                clNotificanteDato.BorderWidth = 0;
                clNotificanteDato.HorizontalAlignment = 0;
                clNotificanteDato.BorderWidthRight = 1f;
                tblCuatro.AddCell(clNotificanteDato);

                PdfPCell clAgenciaMaritima = new PdfPCell(new Phrase("AgenciaMaritima", _sFontNormalNegrita));
                clAgenciaMaritima = new PdfPCell(new Phrase("Agencia Maritima:", _sFontNormalNegrita));
                clAgenciaMaritima.BorderWidth = 0;
                clAgenciaMaritima.HorizontalAlignment = 0;
                clAgenciaMaritima.BorderWidthLeft = 1f;
                clAgenciaMaritima.BorderWidthBottom = 1f;
                clAgenciaMaritima.PaddingLeft = 8f;
                clAgenciaMaritima.PaddingBottom = 5f;
                tblCuatro.AddCell(clAgenciaMaritima);

                PdfPCell clAgenciaMaritimaDato = new PdfPCell(new Phrase("AgenciaMaritimaDato", _sFontNormal));
                clAgenciaMaritimaDato = new PdfPCell(new Phrase(oLis_BE_DocumentoOrigen[0].sAgenciaMaritima, _sFontNormal));
                clAgenciaMaritimaDato.BorderWidth = 0;
                clAgenciaMaritimaDato.HorizontalAlignment = 0;
                clAgenciaMaritimaDato.BorderWidthBottom = 1f;
                clAgenciaMaritimaDato.BorderWidthRight = 1f;
                clAgenciaMaritimaDato.PaddingBottom = 5f;
                tblCuatro.AddCell(clAgenciaMaritimaDato);

                //Relaciona la tabla cuatro en la uno
                PdfPCell clFila_4_1 = new PdfPCell(tblCuatro);
                clFila_4_1.Colspan = 3;
                clFila_4_1.BorderWidth = 0;
                tblUno.AddCell(clFila_4_1);


                //Genera la tabla 
                PdfPTable tblCinco = new PdfPTable(6);
                tblCinco.TotalWidth = 830f;
                float[] widthsCinco = new float[] { 100f, 150f, 150f, 150f, 150f, 150f };
                tblCinco.SetWidths(widthsCinco);
                tblCinco.HorizontalAlignment = 0;
                tblCinco.SpacingBefore = 10f;

                PdfPCell clTotalGeneralTitulo = new PdfPCell(new Phrase("TotalGeneralTitulo", _sFontNormalNegrita));
                clTotalGeneralTitulo = new PdfPCell(new Phrase("Total General", _sFontNormalNegrita));
                clTotalGeneralTitulo.BorderWidth = 0.75f;
                clTotalGeneralTitulo.HorizontalAlignment = 0;
                clTotalGeneralTitulo.PaddingTop = 5f;
                clTotalGeneralTitulo.PaddingBottom = 5f;
                tblCinco.AddCell(clTotalGeneralTitulo);

                PdfPCell clManifestadoTitulo = new PdfPCell(new Phrase("ManifestadoTitulo", _sFontNormalNegrita));
                clManifestadoTitulo = new PdfPCell(new Phrase("Manifestado", _sFontNormalNegrita));
                clManifestadoTitulo.BorderWidth = 0.75f;
                clManifestadoTitulo.HorizontalAlignment = 1;
                clManifestadoTitulo.PaddingTop = 5f;
                clManifestadoTitulo.PaddingBottom = 5f;
                tblCinco.AddCell(clManifestadoTitulo);

                PdfPCell clActualTitulo = new PdfPCell(new Phrase("ActualTitulo", _sFontNormalNegrita));
                clActualTitulo = new PdfPCell(new Phrase("Actual", _sFontNormalNegrita));
                clActualTitulo.BorderWidth = 0.75f;
                clActualTitulo.HorizontalAlignment = 1;
                clActualTitulo.PaddingTop = 5f;
                clActualTitulo.PaddingBottom = 5f;
                tblCinco.AddCell(clActualTitulo);

                PdfPCell clMalosTitulo = new PdfPCell(new Phrase("MalosTitulo", _sFontNormalNegrita));
                clMalosTitulo = new PdfPCell(new Phrase("Malos", _sFontNormalNegrita));
                clMalosTitulo.BorderWidth = 0.75f;
                clMalosTitulo.HorizontalAlignment = 1;
                clMalosTitulo.PaddingTop = 5f;
                clMalosTitulo.PaddingBottom = 5f;
                tblCinco.AddCell(clMalosTitulo);

                PdfPCell clFaltantesTitulo = new PdfPCell(new Phrase("FaltantesTitulo", _sFontNormalNegrita));
                clFaltantesTitulo = new PdfPCell(new Phrase("Faltantes", _sFontNormalNegrita));
                clFaltantesTitulo.BorderWidth = 0.75f;
                clFaltantesTitulo.HorizontalAlignment = 1;
                clFaltantesTitulo.PaddingTop = 5f;
                clFaltantesTitulo.PaddingBottom = 5f;
                tblCinco.AddCell(clFaltantesTitulo);

                PdfPCell clSobrantesTitulo = new PdfPCell(new Phrase("SobrantesTitulo", _sFontNormalNegrita));
                clSobrantesTitulo = new PdfPCell(new Phrase("Sobrantes", _sFontNormalNegrita));
                clSobrantesTitulo.BorderWidth = 0.75f;
                clSobrantesTitulo.HorizontalAlignment = 1;
                clSobrantesTitulo.PaddingTop = 5f;
                clSobrantesTitulo.PaddingBottom = 5f;
                tblCinco.AddCell(clSobrantesTitulo);

                decimal totBultosManifestado = 0;
                decimal totBultosRecibido = 0;
                decimal totBultosMalo = 0;
                decimal totBultosFaltante = 0;
                decimal totBultosSobrante = 0;

                decimal totPesoManif = 0;
                decimal totPesoRecibido = 0;
                decimal totPesoMalo = 0;
                decimal totPesoFaltante = 0;
                decimal totPesoSobrante = 0;
                decimal totDiceContener = 0;
                decimal totEmpqRecibido = 0;

                for (int i = 0; i < oLis_BE_DocumentoOrigen.Count; i++)
                {
                    totBultosManifestado += oLis_BE_DocumentoOrigen[i].dBultoManif - oLis_BE_DocumentoOrigen[i].dBultosRecCs;
                    totBultosRecibido += oLis_BE_DocumentoOrigen[i].dBultosRecib - oLis_BE_DocumentoOrigen[i].dBultosRecCs;
                    totBultosMalo += oLis_BE_DocumentoOrigen[i].dBultoMalo;


                    totPesoManif += oLis_BE_DocumentoOrigen[i].dPesoManif - oLis_BE_DocumentoOrigen[i].dPesosRecCs;
                    totPesoRecibido += oLis_BE_DocumentoOrigen[i].dPesoRecibido - oLis_BE_DocumentoOrigen[i].dPesosRecCs;
                    totPesoMalo += oLis_BE_DocumentoOrigen[i].dPesoMalo;

                    totDiceContener += oLis_BE_DocumentoOrigen[i].dDiceContener - oLis_BE_DocumentoOrigen[i].dBultosRecCs;
                    totEmpqRecibido += oLis_BE_DocumentoOrigen[i].dEmpaqueRecibido;
                }

                if (totBultosManifestado - totBultosRecibido > 0)
                    totBultosFaltante += totBultosManifestado - totBultosRecibido;
                else
                    // Cambios Volante Erick
                    //totBultosSobrante += totBultosRecibido - totBultosManifestado;
                     totBultosSobrante += totBultosManifestado - totBultosRecibido;


                if (totPesoManif - totPesoRecibido > 0)
                    totPesoFaltante += totPesoManif - totPesoRecibido;
                else
                    // Cambios Volante  Erick
                    totPesoSobrante += totPesoRecibido - totPesoManif;
                    //totPesoSobrante += totPesoManif - totPesoRecibido;



                PdfPCell clBultosTitulo = new PdfPCell(new Phrase("BultosTitulo", _sFontNormalNegrita));
                clBultosTitulo = new PdfPCell(new Phrase("Bultos", _sFontNormalNegrita));
                clBultosTitulo.BorderWidth = 0.75f;
                clBultosTitulo.HorizontalAlignment = 0;
                clBultosTitulo.PaddingTop = 5f;
                clBultosTitulo.PaddingBottom = 5f;
                tblCinco.AddCell(clBultosTitulo);

                PdfPCell clBultosManifDato = new PdfPCell(new Phrase("BultosManifDato", _sFontNormal));
                //clBultosManifDato = new PdfPCell(new Phrase(oLis_BE_DocumentoOrigen[0].dBultoManif.ToString(), _sFontNormal));
                clBultosManifDato = new PdfPCell(new Phrase(totBultosManifestado.ToString(), _sFontNormal));
                clBultosManifDato.BorderWidth = 0.75f;
                clBultosManifDato.HorizontalAlignment = 2;
                clBultosManifDato.PaddingTop = 5f;
                clBultosManifDato.PaddingBottom = 5f;
                tblCinco.AddCell(clBultosManifDato);

                PdfPCell clBultosRecibDato = new PdfPCell(new Phrase("BultosRecibDato", _sFontNormal));
                //clBultosRecibDato = new PdfPCell(new Phrase(oLis_BE_DocumentoOrigen[0].dBultoManif.ToString(), _sFontNormal));
                clBultosRecibDato = new PdfPCell(new Phrase(totBultosRecibido.ToString(), _sFontNormal));
                clBultosRecibDato.BorderWidth = 0.75f;
                clBultosRecibDato.HorizontalAlignment = 2;
                clBultosRecibDato.PaddingTop = 5f;
                clBultosRecibDato.PaddingBottom = 5f;
                tblCinco.AddCell(clBultosRecibDato);

                PdfPCell clBultosMaloDato = new PdfPCell(new Phrase("BultosMaloDato", _sFontNormal));
                //clBultosMaloDato = new PdfPCell(new Phrase(oLis_BE_DocumentoOrigen[0].dBultoMalo.ToString(), _sFontNormal));
                clBultosMaloDato = new PdfPCell(new Phrase(totBultosMalo.ToString(), _sFontNormal));
                clBultosMaloDato.BorderWidth = 0.75f;
                clBultosMaloDato.HorizontalAlignment = 2;
                clBultosMaloDato.PaddingTop = 5f;
                clBultosMaloDato.PaddingBottom = 5f;
                tblCinco.AddCell(clBultosMaloDato);

                PdfPCell clBultosFaltanteDato = new PdfPCell(new Phrase("BultosFaltanteDato", _sFontNormal));
                //clBultosFaltanteDato = new PdfPCell(new Phrase(oLis_BE_DocumentoOrigen[0].dBultoFaltante.ToString(), _sFontNormal));
                clBultosFaltanteDato = new PdfPCell(new Phrase(totBultosFaltante.ToString(), _sFontNormal));
                clBultosFaltanteDato.BorderWidth = 0.75f;
                clBultosFaltanteDato.HorizontalAlignment = 2;
                clBultosFaltanteDato.PaddingTop = 5f;
                clBultosFaltanteDato.PaddingBottom = 5f;
                tblCinco.AddCell(clBultosFaltanteDato);

                PdfPCell clBultosSobranteDato = new PdfPCell(new Phrase("BultosSobranteDato", _sFontNormal));
                //clBultosSobranteDato = new PdfPCell(new Phrase(oLis_BE_DocumentoOrigen[0].dBultoSobrante.ToString(), _sFontNormal));
                clBultosSobranteDato = new PdfPCell(new Phrase(totBultosSobrante.ToString(), _sFontNormal));
                clBultosSobranteDato.BorderWidth = 0.75f;
                clBultosSobranteDato.HorizontalAlignment = 2;
                clBultosSobranteDato.PaddingTop = 5f;
                clBultosSobranteDato.PaddingBottom = 5f;
                tblCinco.AddCell(clBultosSobranteDato);

                PdfPCell clPesoTitulo = new PdfPCell(new Phrase("PesoTitulo", _sFontNormalNegrita));
                clPesoTitulo = new PdfPCell(new Phrase("Peso", _sFontNormalNegrita));
                clPesoTitulo.BorderWidth = 0.75f;
                clPesoTitulo.HorizontalAlignment = 0;
                clPesoTitulo.PaddingTop = 5f;
                clPesoTitulo.PaddingBottom = 5f;
                tblCinco.AddCell(clPesoTitulo);

                PdfPCell clPesoManifDato = new PdfPCell(new Phrase("PesoManifDato", _sFontNormal));
                //clPesoManifDato = new PdfPCell(new Phrase(oLis_BE_DocumentoOrigen[0].dPesoManif.ToString(), _sFontNormal));
                clPesoManifDato = new PdfPCell(new Phrase(totPesoManif.ToString(), _sFontNormal));
                clPesoManifDato.BorderWidth = 0.75f;
                clPesoManifDato.HorizontalAlignment = 2;
                clPesoManifDato.PaddingTop = 5f;
                clPesoManifDato.PaddingBottom = 5f;
                tblCinco.AddCell(clPesoManifDato);

                PdfPCell clPesoRecibDato = new PdfPCell(new Phrase("PesoRecibDato", _sFontNormal));
                //clPesoRecibDato = new PdfPCell(new Phrase(oLis_BE_DocumentoOrigen[0].dBultoManif.ToString(), _sFontNormal));
                clPesoRecibDato = new PdfPCell(new Phrase(totPesoRecibido.ToString(), _sFontNormal));
                clPesoRecibDato.BorderWidth = 0.75f;
                clPesoRecibDato.HorizontalAlignment = 2;
                clPesoRecibDato.PaddingTop = 5f;
                clPesoRecibDato.PaddingBottom = 5f;
                tblCinco.AddCell(clPesoRecibDato);

                PdfPCell clPesoMaloDato = new PdfPCell(new Phrase("PesoMaloDato", _sFontNormal));
                //clPesoMaloDato = new PdfPCell(new Phrase(oLis_BE_DocumentoOrigen[0].dPesoMalo.ToString(), _sFontNormal));
                clPesoMaloDato = new PdfPCell(new Phrase(totPesoMalo.ToString(), _sFontNormal));
                clPesoMaloDato.BorderWidth = 0.75f;
                clPesoMaloDato.HorizontalAlignment = 2;
                clPesoMaloDato.PaddingTop = 5f;
                clPesoMaloDato.PaddingBottom = 5f;
                tblCinco.AddCell(clPesoMaloDato);

                PdfPCell clPesoFaltanteDato = new PdfPCell(new Phrase("PesoFaltanteDato", _sFontNormal));
                //clPesoFaltanteDato = new PdfPCell(new Phrase(oLis_BE_DocumentoOrigen[0].dPesoFaltante.ToString(), _sFontNormal));
                clPesoFaltanteDato = new PdfPCell(new Phrase(totPesoFaltante.ToString(), _sFontNormal));
                clPesoFaltanteDato.BorderWidth = 0.75f;
                clPesoFaltanteDato.HorizontalAlignment = 2;
                clPesoFaltanteDato.PaddingTop = 5f;
                clPesoFaltanteDato.PaddingBottom = 5f;
                tblCinco.AddCell(clPesoFaltanteDato);

                PdfPCell clPesoSobranteDato = new PdfPCell(new Phrase("PesoSobranteDato", _sFontNormal));
                //clPesoSobranteDato = new PdfPCell(new Phrase(oLis_BE_DocumentoOrigen[0].dPesoSobrante.ToString(), _sFontNormal));
                clPesoSobranteDato = new PdfPCell(new Phrase(totPesoSobrante.ToString(), _sFontNormal));
                clPesoSobranteDato.BorderWidth = 0.75f;
                clPesoSobranteDato.HorizontalAlignment = 2;
                clPesoSobranteDato.PaddingTop = 5f;
                clPesoSobranteDato.PaddingBottom = 5f;
                tblCinco.AddCell(clPesoSobranteDato);

                //Relaciona la tabla cinco en la uno
                PdfPCell clFila_5_1 = new PdfPCell(tblCinco);
                clFila_5_1.Colspan = 3;
                clFila_5_1.BorderWidth = 0;
                tblUno.AddCell(clFila_5_1);


                //Genera la tabla 
                PdfPTable tblSeis = new PdfPTable(6);
                tblSeis.TotalWidth = 830f;
                float[] widthsSeis = new float[] { 100f, 150f, 150f, 150f, 150f, 150f };
                tblSeis.SetWidths(widthsSeis);
                tblSeis.HorizontalAlignment = 0;
                tblSeis.SpacingBefore = 20f;

                PdfPCell clDiceContenerTitulo = new PdfPCell(new Phrase("DiceContenerTitulo", _sFontNormal));
                clDiceContenerTitulo = new PdfPCell(new Phrase("Dice Contener", _sFontNormal));
                clDiceContenerTitulo.BorderWidth = 0;
                clDiceContenerTitulo.HorizontalAlignment = 0;
                tblSeis.AddCell(clDiceContenerTitulo);

                PdfPCell clDiceContenerDato = new PdfPCell(new Phrase("DiceContenerDato", _sFontNormal));
                //clDiceContenerDato = new PdfPCell(new Phrase(Convert.ToInt32(oLis_BE_DocumentoOrigen[0].dDiceContener).ToString(), _sFontNormal));
                clDiceContenerDato = new PdfPCell(new Phrase(totDiceContener.ToString(), _sFontNormal));
                clDiceContenerDato.BorderWidth = 0;
                clDiceContenerDato.HorizontalAlignment = 0;
                tblSeis.AddCell(clDiceContenerDato);

                PdfPCell clEmpaquesTitulo = new PdfPCell(new Phrase("EmpaquesTituloTitulo", _sFontNormal));
                clEmpaquesTitulo = new PdfPCell(new Phrase("Empaques Recibidos", _sFontNormal));
                clEmpaquesTitulo.BorderWidth = 0;
                clEmpaquesTitulo.HorizontalAlignment = 0;
                tblSeis.AddCell(clEmpaquesTitulo);

                PdfPCell clEmpaquesDato = new PdfPCell(new Phrase("EmpaquesDato", _sFontNormal));
                //clEmpaquesDato = new PdfPCell(new Phrase(Convert.ToInt32(oLis_BE_DocumentoOrigen[0].dEmpaqueRecibido).ToString(), _sFontNormal));
                clEmpaquesDato = new PdfPCell(new Phrase(totEmpqRecibido.ToString(), _sFontNormal));
                clEmpaquesDato.BorderWidth = 0;
                clEmpaquesDato.HorizontalAlignment = 0;
                tblSeis.AddCell(clEmpaquesDato);

                PdfPCell clEstadoTitulo = new PdfPCell(new Phrase("EstadoTitulo", _sFontNormal));
                clEstadoTitulo = new PdfPCell(new Phrase("Estado Transmisión", _sFontNormal));
                clEstadoTitulo.BorderWidth = 0;
                clEstadoTitulo.HorizontalAlignment = 0;
                tblSeis.AddCell(clEstadoTitulo);

                PdfPCell clEstadoDato = new PdfPCell(new Phrase("EstadoDato", _sFontEstado));
                clEstadoDato = new PdfPCell(new Phrase(oLis_BE_DocumentoOrigen[0].sEstadoTransmitido, _sFontEstado));
                clEstadoDato.BorderWidth = 0;
                clEstadoDato.HorizontalAlignment = 0;
                tblSeis.AddCell(clEstadoDato);

                //Relaciona la tabla seis en la uno
                PdfPCell clFila_6_1 = new PdfPCell(tblSeis);
                clFila_6_1.Colspan = 3;
                clFila_6_1.BorderWidth = 0;
                tblUno.AddCell(clFila_6_1);


                //Genera la tabla 
                PdfPTable tblSiete = new PdfPTable(1);
                tblSiete.TotalWidth = 830f;
                float[] widthsSiete = new float[] { 830f };
                tblSiete.SetWidths(widthsSiete);
                tblSiete.HorizontalAlignment = 0;
                tblSiete.SpacingBefore = 10f;

                PdfPCell clMercaderiaTitulo = new PdfPCell(new Phrase("MercaderiaTitulo", _sFontNormalNegrita));
                clMercaderiaTitulo = new PdfPCell(new Phrase("Mercadería", _sFontNormalNegrita));
                clMercaderiaTitulo.BorderWidth = 0;
                clMercaderiaTitulo.HorizontalAlignment = 0;
                tblSiete.AddCell(clMercaderiaTitulo);

                PdfPCell clMercaderiaDato = new PdfPCell(new Phrase("MercaderiaDato", _sFontTituloIzquierda));
                clMercaderiaDato = new PdfPCell(new Phrase(oLis_BE_DocumentoOrigen[0].sMercaderia, _sFontTituloIzquierda));
                clMercaderiaDato.BorderWidth = 0;
                clMercaderiaDato.HorizontalAlignment = 0;
                tblSiete.AddCell(clMercaderiaDato);

                PdfPCell clVacio = new PdfPCell(new Phrase("Vacio", _sFontNormalNegrita));
                clVacio = new PdfPCell(new Phrase(" ", _sFontNormalNegrita));
                clVacio.BorderWidth = 0;
                clVacio.HorizontalAlignment = 0;
                tblSiete.AddCell(clVacio);

                //////////////////////////////////////// Concatenar Observacio ////////////////////////////////
                string ObservacionTitulo = string.Empty;
                for (int i = 0; i < oLis_BE_DocumentoOrigen.Count; i++)
                {
                    if (!string.IsNullOrEmpty(oLis_BE_DocumentoOrigen[i].sObservacion))
                    {
                        ObservacionTitulo += oLis_BE_DocumentoOrigen[i].sObservacion;
                    }
                }

                PdfPCell clObservacionTitulo = new PdfPCell(new Phrase("ObservacionTitulo", _sFontNormalNegrita));
                clObservacionTitulo = new PdfPCell(new Phrase("Observación: " , _sFontNormalNegrita));
                clObservacionTitulo.BorderWidth = 0;
                clObservacionTitulo.HorizontalAlignment = 0;
                tblSiete.AddCell(clObservacionTitulo);

                PdfPCell clObservacionDato = new PdfPCell(new Phrase("ObservacionDato", _sFontNormal));
                clObservacionDato = new PdfPCell(new Phrase(oLis_BE_DocumentoOrigen[0].sObservacion, _sFontNormal));
                clObservacionDato.BorderWidth = 0;
                clObservacionDato.HorizontalAlignment = 0;
                tblSiete.AddCell(clObservacionDato);

                clVacio = new PdfPCell(new Phrase(" ", _sFontNormalNegrita));
                clVacio.BorderWidth = 0;
                clVacio.HorizontalAlignment = 0;
                tblSiete.AddCell(clVacio);

                PdfPCell clPesoInmovilizadoDato = new PdfPCell(new Phrase("PesoInmovilizadoDato", _sFontInmovilizadoNegrita));
                clPesoInmovilizadoDato = new PdfPCell(new Phrase(oLis_BE_DocumentoOrigen[0].sPesoInvLib, _sFontInmovilizadoNegrita));
                clPesoInmovilizadoDato.BorderWidth = 0;
                clPesoInmovilizadoDato.HorizontalAlignment = 0;
                tblSiete.AddCell(clPesoInmovilizadoDato);

                clVacio = new PdfPCell(new Phrase(" ", _sFontNormalNegrita));
                clVacio.BorderWidth = 0;
                clVacio.HorizontalAlignment = 0;
                tblSiete.AddCell(clVacio);

                //Relaciona la tabla siete en la uno
                PdfPCell clFila_7_1 = new PdfPCell(tblSiete);
                clFila_7_1.Colspan = 3;
                clFila_7_1.BorderWidth = 0;
                tblUno.AddCell(clFila_7_1);


                int iFila = 0;
                String sCondicion = "";

                //Genera la tabla para los FCL
                PdfPTable tblOcho = new PdfPTable(13);
                tblOcho.TotalWidth = 830f;
                float[] widthsOcho = new float[] { 20f, 70f, 20f, 20f, 30f, 50f, 150f, 80f, 60f, 60f, 60f, 60f, 30f };
                tblOcho.SetWidths(widthsOcho);
                tblOcho.HorizontalAlignment = 0;
                tblOcho.SpacingBefore = 10f;

                //Genera la tabla para la CS
                PdfPTable tblNueve = new PdfPTable(7);
                tblNueve.TotalWidth = 830f;
                float[] widthsNueve = new float[] { 20f, 60f, 500f, 60f, 60f, 60f, 60f };
                tblNueve.SetWidths(widthsNueve);
                tblNueve.HorizontalAlignment = 0;
                tblNueve.SpacingBefore = 10f;

                //Genera la tabla para los LCL
                PdfPTable tblDiez = new PdfPTable(12);
                tblDiez.TotalWidth = 830f;
                float[] widthsDiez = new float[] { 20f, 70f, 20f, 20f, 30f, 50f, 200f, 60f, 50f, 100f, 20f, 30f };
                tblDiez.SetWidths(widthsDiez);
                tblDiez.HorizontalAlignment = 0;
                tblDiez.SpacingBefore = 10f;


                for (int i = 0; i < oLis_BE_DocumentoOrigen.Count; i++)
                {
                    if (sCondicion != oLis_BE_DocumentoOrigen[i].sCondicion)
                    {
                        iFila = 0;
                    }

                    if (oLis_BE_DocumentoOrigen[i].sCondicion == "APE")
                    {
                        if (iFila == 0)
                        {
                            PdfPCell clCondicCargaDatoLCL = new PdfPCell(new Phrase("CondicCargaDato", _sFontDetalleNegrita));
                            clCondicCargaDatoLCL = new PdfPCell(new Phrase(oLis_BE_DocumentoOrigen[i].sCondicCarga, _sFontDetalleNegrita));
                            clCondicCargaDatoLCL.BorderWidth = 0;
                            clCondicCargaDatoLCL.HorizontalAlignment = 0;
                            clCondicCargaDatoLCL.Colspan = 12;
                            tblDiez.AddCell(clCondicCargaDatoLCL);

                            PdfPCell clItemTitulo = new PdfPCell(new Phrase("ItemTitulo", _sFontDetalleNegrita));
                            clItemTitulo = new PdfPCell(new Phrase("Id", _sFontDetalleNegrita));
                            clItemTitulo.BorderWidth = 0.75f;
                            clItemTitulo.HorizontalAlignment = 1;
                            tblDiez.AddCell(clItemTitulo);

                            PdfPCell clContenedorTitulo = new PdfPCell(new Phrase("ContenedorTitulo", _sFontDetalleNegrita));
                            clContenedorTitulo = new PdfPCell(new Phrase("Contenedor", _sFontDetalleNegrita));
                            clContenedorTitulo.BorderWidth = 0.75f;
                            clContenedorTitulo.HorizontalAlignment = 1;
                            tblDiez.AddCell(clContenedorTitulo);

                            PdfPCell clTamanoCtnTitulo = new PdfPCell(new Phrase("TamanoTitulo", _sFontDetalleNegrita));
                            clTamanoCtnTitulo = new PdfPCell(new Phrase("Tn", _sFontDetalleNegrita));
                            clTamanoCtnTitulo.BorderWidth = 0.75f;
                            clTamanoCtnTitulo.HorizontalAlignment = 1;
                            tblDiez.AddCell(clTamanoCtnTitulo);

                            PdfPCell clTipoCtnTitullo = new PdfPCell(new Phrase("TipoCtnTitulo", _sFontDetalleNegrita));
                            clTipoCtnTitullo = new PdfPCell(new Phrase("Tip", _sFontDetalleNegrita));
                            clTipoCtnTitullo.BorderWidth = 0.75f;
                            clTipoCtnTitullo.HorizontalAlignment = 1;
                            tblDiez.AddCell(clTipoCtnTitullo);

                            PdfPCell clTaraTitulo = new PdfPCell(new Phrase("TaraTitulo", _sFontDetalleNegrita));
                            clTaraTitulo = new PdfPCell(new Phrase("Tara", _sFontDetalleNegrita));
                            clTaraTitulo.BorderWidth = 0.75f;
                            clTaraTitulo.HorizontalAlignment = 1;
                            tblDiez.AddCell(clTaraTitulo);

                            PdfPCell clFechaIngresoDetTitulo = new PdfPCell(new Phrase("FechaIngresoTitulo", _sFontDetalleNegrita));
                            clFechaIngresoDetTitulo = new PdfPCell(new Phrase("Fecha Ingreso", _sFontDetalleNegrita));
                            clFechaIngresoDetTitulo.BorderWidth = 0.75f;
                            clFechaIngresoDetTitulo.HorizontalAlignment = 1;
                            tblDiez.AddCell(clFechaIngresoDetTitulo);

                            PdfPCell clTipoCargaTitulo = new PdfPCell(new Phrase("TipoCargaTitulo", _sFontDetalleNegrita));
                            clTipoCargaTitulo = new PdfPCell(new Phrase("Descripción", _sFontDetalleNegrita));
                            clTipoCargaTitulo.BorderWidth = 0.75f;
                            clTipoCargaTitulo.HorizontalAlignment = 1;
                            tblDiez.AddCell(clTipoCargaTitulo);

                            PdfPCell clPesoActualDetalleTitulo = new PdfPCell(new Phrase("PesoActualDatoDetalleTitulo", _sFontDetalleNegrita));
                            clPesoActualDetalleTitulo = new PdfPCell(new Phrase("Peso Actual", _sFontDetalleNegrita));
                            clPesoActualDetalleTitulo.BorderWidth = 0.75f;
                            clPesoActualDetalleTitulo.HorizontalAlignment = 2;
                            tblDiez.AddCell(clPesoActualDetalleTitulo);

                            PdfPCell clBultosActualDetalleTitulo = new PdfPCell(new Phrase("BultosActualDetalleTitulo", _sFontDetalleNegrita));
                            clBultosActualDetalleTitulo = new PdfPCell(new Phrase("Bultos Actual", _sFontDetalleNegrita));
                            clBultosActualDetalleTitulo.BorderWidth = 0.75f;
                            clBultosActualDetalleTitulo.HorizontalAlignment = 2;
                            tblDiez.AddCell(clBultosActualDetalleTitulo);

                            PdfPCell clPrecintoManifTitulo = new PdfPCell(new Phrase("PrecintoManifTitulo", _sFontDetalleNegrita));
                            clPrecintoManifTitulo = new PdfPCell(new Phrase("Precinto", _sFontDetalleNegrita));
                            clPrecintoManifTitulo.BorderWidth = 0.75f;
                            clPrecintoManifTitulo.HorizontalAlignment = 2;
                            tblDiez.AddCell(clPrecintoManifTitulo);

                            PdfPCell clReestibaTitulo = new PdfPCell(new Phrase("RestibaTitulo", _sFontDetalleNegrita));
                            clReestibaTitulo = new PdfPCell(new Phrase("RB", _sFontDetalleNegrita));
                            clReestibaTitulo.BorderWidth = 0.75f;
                            clReestibaTitulo.HorizontalAlignment = 2;
                            tblDiez.AddCell(clReestibaTitulo);

                            PdfPCell clSiniTitulo = new PdfPCell(new Phrase("SiniTitulo", _sFontDetalleNegrita));
                            clSiniTitulo = new PdfPCell(new Phrase("Sini", _sFontDetalleNegrita));
                            clSiniTitulo.BorderWidth = 0.75f;
                            clSiniTitulo.HorizontalAlignment = 1;
                            tblDiez.AddCell(clSiniTitulo);

                            iFila = 1;
                        }
                        sCondicion = oLis_BE_DocumentoOrigen[i].sCondicion;

                        PdfPCell clItem = new PdfPCell(new Phrase("Item", _sFontDetalle));
                        clItem = new PdfPCell(new Phrase((i + 1).ToString(), _sFontDetallePequena));
                        clItem.BorderWidth = 0.75f;
                        clItem.HorizontalAlignment = 1;
                        if (oLis_BE_DocumentoOrigen[0].sPesoInvLib != String.Empty) { clItem.BackgroundColor = new iTextSharp.text.BaseColor(255, 0, 0); }
                        tblDiez.AddCell(clItem);

                        PdfPCell clContenedor = new PdfPCell(new Phrase("Contenedor", _sFontDetalle));
                        clContenedor = new PdfPCell(new Phrase(oLis_BE_DocumentoOrigen[i].sContenedor, _sFontDetallePequena));
                        clContenedor.BorderWidth = 0.75f;
                        clContenedor.HorizontalAlignment = 1;
                        if (oLis_BE_DocumentoOrigen[0].sPesoInvLib != String.Empty) { clContenedor.BackgroundColor = new iTextSharp.text.BaseColor(255, 0, 0); }
                        tblDiez.AddCell(clContenedor);

                        PdfPCell clTamanoCtn = new PdfPCell(new Phrase("Tamano", _sFontDetalle));
                        clTamanoCtn = new PdfPCell(new Phrase(oLis_BE_DocumentoOrigen[i].sTamano, _sFontDetallePequena));
                        clTamanoCtn.BorderWidth = 0.75f;
                        clTamanoCtn.HorizontalAlignment = 1;
                        if (oLis_BE_DocumentoOrigen[0].sPesoInvLib != String.Empty) { clTamanoCtn.BackgroundColor = new iTextSharp.text.BaseColor(255, 0, 0); }
                        tblDiez.AddCell(clTamanoCtn);

                        PdfPCell clTipoCtn = new PdfPCell(new Phrase("TipoCtn", _sFontDetalle));
                        clTipoCtn = new PdfPCell(new Phrase(oLis_BE_DocumentoOrigen[i].sTipoCtn, _sFontDetallePequena));
                        clTipoCtn.BorderWidth = 0.75f;
                        clTipoCtn.HorizontalAlignment = 1;
                        if (oLis_BE_DocumentoOrigen[0].sPesoInvLib != String.Empty) { clTipoCtn.BackgroundColor = new iTextSharp.text.BaseColor(255, 0, 0); }
                        tblDiez.AddCell(clTipoCtn);

                        PdfPCell clTara = new PdfPCell(new Phrase("Tara", _sFontDetalle));
                        clTara = new PdfPCell(new Phrase(Convert.ToInt32(oLis_BE_DocumentoOrigen[i].dTara).ToString(), _sFontDetallePequena));
                        clTara.BorderWidth = 0.75f;
                        clTara.HorizontalAlignment = 2;
                        if (oLis_BE_DocumentoOrigen[0].sPesoInvLib != String.Empty) { clTara.BackgroundColor = new iTextSharp.text.BaseColor(255, 0, 0); }
                        tblDiez.AddCell(clTara);

                        PdfPCell clFechaIngresoDet = new PdfPCell(new Phrase("FechaIngreso", _sFontDetalle));
                        clFechaIngresoDet = new PdfPCell(new Phrase(oLis_BE_DocumentoOrigen[i].sFechaIngreso, _sFontDetallePequena));
                        clFechaIngresoDet.BorderWidth = 0.75f;
                        clFechaIngresoDet.HorizontalAlignment = 1;
                        if (oLis_BE_DocumentoOrigen[0].sPesoInvLib != String.Empty) { clFechaIngresoDet.BackgroundColor = new iTextSharp.text.BaseColor(255, 0, 0); }
                        tblDiez.AddCell(clFechaIngresoDet);

                        PdfPCell clTipoCarga = new PdfPCell(new Phrase("TipoCarga", _sFontDetalle));
                        clTipoCarga = new PdfPCell(new Phrase(oLis_BE_DocumentoOrigen[i].sTipoCarga, _sFontDetallePequena));
                        clTipoCarga.BorderWidth = 0.75f;
                        clTipoCarga.HorizontalAlignment = 0;
                        if (oLis_BE_DocumentoOrigen[0].sPesoInvLib != String.Empty) { clTipoCarga.BackgroundColor = new iTextSharp.text.BaseColor(255, 0, 0); }
                        tblDiez.AddCell(clTipoCarga);

                        PdfPCell clPesoActualDatoDetalle = new PdfPCell(new Phrase("PesoActualDatoDetalle", _sFontDetalle));
                        clPesoActualDatoDetalle = new PdfPCell(new Phrase(oLis_BE_DocumentoOrigen[i].dPesoRecibido.ToString(), _sFontDetallePequena));
                        clPesoActualDatoDetalle.BorderWidth = 0.75f;
                        clPesoActualDatoDetalle.HorizontalAlignment = 2;
                        if (oLis_BE_DocumentoOrigen[0].sPesoInvLib != String.Empty) { clPesoActualDatoDetalle.BackgroundColor = new iTextSharp.text.BaseColor(255, 0, 0); }
                        tblDiez.AddCell(clPesoActualDatoDetalle);

                        PdfPCell clBultosActualDatoDetalle = new PdfPCell(new Phrase("BultosActualDatoDetalle", _sFontDetalle));
                        clBultosActualDatoDetalle = new PdfPCell(new Phrase(oLis_BE_DocumentoOrigen[i].dBultosRecib.ToString(), _sFontDetallePequena));
                        clBultosActualDatoDetalle.BorderWidth = 0.75f;
                        clBultosActualDatoDetalle.HorizontalAlignment = 2;
                        if (oLis_BE_DocumentoOrigen[0].sPesoInvLib != String.Empty) { clBultosActualDatoDetalle.BackgroundColor = new iTextSharp.text.BaseColor(255, 0, 0); }
                        tblDiez.AddCell(clBultosActualDatoDetalle);

                        PdfPCell clPrecintoDatoDetalle = new PdfPCell(new Phrase("PrecintoDatoDetalle", _sFontDetalle));
                        clPrecintoDatoDetalle = new PdfPCell(new Phrase(oLis_BE_DocumentoOrigen[i].sPrecintoRecepcionado, _sFontDetallePequena));
                        clPrecintoDatoDetalle.BorderWidth = 0.75f;
                        clPrecintoDatoDetalle.HorizontalAlignment = 0;
                        if (oLis_BE_DocumentoOrigen[0].sPesoInvLib != String.Empty) { clPrecintoDatoDetalle.BackgroundColor = new iTextSharp.text.BaseColor(255, 0, 0); }
                        tblDiez.AddCell(clPrecintoDatoDetalle);

                        PdfPCell clReestibaDatoDetalle = new PdfPCell(new Phrase("ReestibaDatoDetalle", _sFontDetalle));
                        clReestibaDatoDetalle = new PdfPCell(new Phrase(oLis_BE_DocumentoOrigen[i].sReestiba, _sFontDetallePequena));
                        clReestibaDatoDetalle.BorderWidth = 0.75f;
                        clReestibaDatoDetalle.HorizontalAlignment = 0;
                        if (oLis_BE_DocumentoOrigen[0].sPesoInvLib != String.Empty) { clReestibaDatoDetalle.BackgroundColor = new iTextSharp.text.BaseColor(255, 0, 0); }
                        tblDiez.AddCell(clReestibaDatoDetalle);

                        PdfPCell clSini = new PdfPCell(new Phrase("Sini", _sFontDetalle));
                        clSini = new PdfPCell(new Phrase(oLis_BE_DocumentoOrigen[i].sSini, _sFontDetallePequena));
                        clSini.BorderWidth = 0.75f;
                        clSini.HorizontalAlignment = 0;
                        if (oLis_BE_DocumentoOrigen[0].sPesoInvLib != String.Empty) { clSini.BackgroundColor = new iTextSharp.text.BaseColor(255, 0, 0); }
                        tblDiez.AddCell(clSini);
                    }

                    if (oLis_BE_DocumentoOrigen[i].sCondicion == "CNT")
                    {
                        if (iFila == 0)
                        {
                            PdfPCell clCondicCargaDatoCNT = new PdfPCell(new Phrase("CondicCargaDato", _sFontDetalleNegrita));
                            clCondicCargaDatoCNT = new PdfPCell(new Phrase(oLis_BE_DocumentoOrigen[i].sCondicCarga, _sFontDetalleNegrita));
                            clCondicCargaDatoCNT.BorderWidth = 0;
                            clCondicCargaDatoCNT.HorizontalAlignment = 0;
                            clCondicCargaDatoCNT.Colspan = 13;
                            tblOcho.AddCell(clCondicCargaDatoCNT);

                            PdfPCell clItemTitulo = new PdfPCell(new Phrase("ItemTitulo", _sFontDetalleNegrita));
                            clItemTitulo = new PdfPCell(new Phrase("Id", _sFontDetalleNegrita));
                            clItemTitulo.BorderWidth = 0.75f;
                            clItemTitulo.HorizontalAlignment = 1;
                            tblOcho.AddCell(clItemTitulo);

                            PdfPCell clContenedorTitulo = new PdfPCell(new Phrase("ContenedorTitulo", _sFontDetalleNegrita));
                            clContenedorTitulo = new PdfPCell(new Phrase("Contenedor", _sFontDetalleNegrita));
                            clContenedorTitulo.BorderWidth = 0.75f;
                            clContenedorTitulo.HorizontalAlignment = 1;
                            tblOcho.AddCell(clContenedorTitulo);

                            PdfPCell clTamanoCtnTitulo = new PdfPCell(new Phrase("TamanoTitulo", _sFontDetalleNegrita));
                            clTamanoCtnTitulo = new PdfPCell(new Phrase("Tn", _sFontDetalleNegrita));
                            clTamanoCtnTitulo.BorderWidth = 0.75f;
                            clTamanoCtnTitulo.HorizontalAlignment = 1;
                            tblOcho.AddCell(clTamanoCtnTitulo);

                            PdfPCell clTipoCtnTitullo = new PdfPCell(new Phrase("TipoCtnTitulo", _sFontDetalleNegrita));
                            clTipoCtnTitullo = new PdfPCell(new Phrase("Tip", _sFontDetalleNegrita));
                            clTipoCtnTitullo.BorderWidth = 0.75f;
                            clTipoCtnTitullo.HorizontalAlignment = 1;
                            tblOcho.AddCell(clTipoCtnTitullo);

                            PdfPCell clTaraTitulo = new PdfPCell(new Phrase("TaraTitulo", _sFontDetalleNegrita));
                            clTaraTitulo = new PdfPCell(new Phrase("Tara", _sFontDetalleNegrita));
                            clTaraTitulo.BorderWidth = 0.75f;
                            clTaraTitulo.HorizontalAlignment = 1;
                            tblOcho.AddCell(clTaraTitulo);

                            PdfPCell clFechaIngresoDetTitulo = new PdfPCell(new Phrase("FechaIngresoTitulo", _sFontDetalleNegrita));
                            clFechaIngresoDetTitulo = new PdfPCell(new Phrase("Fecha Ingreso", _sFontDetalleNegrita));
                            clFechaIngresoDetTitulo.BorderWidth = 0.75f;
                            clFechaIngresoDetTitulo.HorizontalAlignment = 1;
                            tblOcho.AddCell(clFechaIngresoDetTitulo);

                            PdfPCell clTipoCargaTitulo = new PdfPCell(new Phrase("TipoCargaTitulo", _sFontDetalleNegrita));
                            clTipoCargaTitulo = new PdfPCell(new Phrase("Descripción", _sFontDetalleNegrita));
                            clTipoCargaTitulo.BorderWidth = 0.75f;
                            clTipoCargaTitulo.HorizontalAlignment = 1;
                            tblOcho.AddCell(clTipoCargaTitulo);

                            PdfPCell clObservacionDetTitulo = new PdfPCell(new Phrase("ObservacionDetTitulo", _sFontDetalleNegrita));
                            clObservacionDetTitulo = new PdfPCell(new Phrase("Observación", _sFontDetalleNegrita));
                            clObservacionDetTitulo.BorderWidth = 0.75f;
                            clObservacionDetTitulo.HorizontalAlignment = 1;
                            tblOcho.AddCell(clObservacionDetTitulo);

                            PdfPCell clPesoManifDetalleTitulo = new PdfPCell(new Phrase("PesoManifDetalleTitulo", _sFontDetalleNegrita));
                            clPesoManifDetalleTitulo = new PdfPCell(new Phrase("Peso Manifestado", _sFontDetalleNegrita));
                            clPesoManifDetalleTitulo.BorderWidth = 0.75f;
                            clPesoManifDetalleTitulo.HorizontalAlignment = 2;
                            tblOcho.AddCell(clPesoManifDetalleTitulo);

                            PdfPCell clPesoActualDetalleTitulo = new PdfPCell(new Phrase("PesoActualDetalleTitulo", _sFontDetalleNegrita));
                            clPesoActualDetalleTitulo = new PdfPCell(new Phrase("Peso Actual", _sFontDetalleNegrita));
                            clPesoActualDetalleTitulo.BorderWidth = 0.75f;
                            clPesoActualDetalleTitulo.HorizontalAlignment = 2;
                            tblOcho.AddCell(clPesoActualDetalleTitulo);

                            PdfPCell clPrecintoManifTitulo = new PdfPCell(new Phrase("PrecintoManifTitulo", _sFontDetalleNegrita));
                            clPrecintoManifTitulo = new PdfPCell(new Phrase("Precinto Manif.", _sFontDetalleNegrita));
                            clPrecintoManifTitulo.BorderWidth = 0.75f;
                            clPrecintoManifTitulo.HorizontalAlignment = 2;
                            tblOcho.AddCell(clPrecintoManifTitulo);

                            PdfPCell clPrecintoRecibTitulo = new PdfPCell(new Phrase("PrecintoRecibTitulo", _sFontDetalleNegrita));
                            clPrecintoRecibTitulo = new PdfPCell(new Phrase("Precinto Actual", _sFontDetalleNegrita));
                            clPrecintoRecibTitulo.BorderWidth = 0.75f;
                            clPrecintoRecibTitulo.HorizontalAlignment = 2;
                            tblOcho.AddCell(clPrecintoRecibTitulo);

                            PdfPCell clSiniTitulo = new PdfPCell(new Phrase("SiniTitulo", _sFontDetalleNegrita));
                            clSiniTitulo = new PdfPCell(new Phrase("Sini", _sFontDetalleNegrita));
                            clSiniTitulo.BorderWidth = 0.75f;
                            clSiniTitulo.HorizontalAlignment = 1;
                            tblOcho.AddCell(clSiniTitulo);

                            iFila = 1;
                        }
                        sCondicion = oLis_BE_DocumentoOrigen[i].sCondicion;

                        PdfPCell clItem = new PdfPCell(new Phrase("Item", _sFontDetalle));
                        clItem = new PdfPCell(new Phrase((i + 1).ToString(), _sFontDetallePequena));
                        clItem.BorderWidth = 0.75f;
                        clItem.HorizontalAlignment = 1;
                        if (oLis_BE_DocumentoOrigen[0].sPesoInvLib != String.Empty) { clItem.BackgroundColor = new iTextSharp.text.BaseColor(255, 0, 0); }
                        tblOcho.AddCell(clItem);

                        PdfPCell clContenedor = new PdfPCell(new Phrase("Contenedor", _sFontDetalle));
                        clContenedor = new PdfPCell(new Phrase(oLis_BE_DocumentoOrigen[i].sContenedor, _sFontDetallePequena));
                        clContenedor.BorderWidth = 0.75f;
                        clContenedor.HorizontalAlignment = 1;
                        if (oLis_BE_DocumentoOrigen[0].sPesoInvLib != String.Empty) { clContenedor.BackgroundColor = new iTextSharp.text.BaseColor(255, 0, 0); }
                        tblOcho.AddCell(clContenedor);

                        PdfPCell clTamanoCtn = new PdfPCell(new Phrase("Tamano", _sFontDetalle));
                        clTamanoCtn = new PdfPCell(new Phrase(oLis_BE_DocumentoOrigen[i].sTamano, _sFontDetallePequena));
                        clTamanoCtn.BorderWidth = 0.75f;
                        clTamanoCtn.HorizontalAlignment = 1;
                        if (oLis_BE_DocumentoOrigen[0].sPesoInvLib != String.Empty) { clTamanoCtn.BackgroundColor = new iTextSharp.text.BaseColor(255, 0, 0); }
                        tblOcho.AddCell(clTamanoCtn);

                        PdfPCell clTipoCtn = new PdfPCell(new Phrase("TipoCtn", _sFontDetalle));
                        clTipoCtn = new PdfPCell(new Phrase(oLis_BE_DocumentoOrigen[i].sTipoCtn, _sFontDetallePequena));
                        clTipoCtn.BorderWidth = 0.75f;
                        clTipoCtn.HorizontalAlignment = 1;
                        if (oLis_BE_DocumentoOrigen[0].sPesoInvLib != String.Empty) { clTipoCtn.BackgroundColor = new iTextSharp.text.BaseColor(255, 0, 0); }
                        tblOcho.AddCell(clTipoCtn);

                        PdfPCell clTara = new PdfPCell(new Phrase("Tara", _sFontDetalle));
                        clTara = new PdfPCell(new Phrase(Convert.ToInt32(oLis_BE_DocumentoOrigen[i].dTara).ToString(), _sFontDetallePequena));
                        clTara.BorderWidth = 0.75f;
                        clTara.HorizontalAlignment = 2;
                        if (oLis_BE_DocumentoOrigen[0].sPesoInvLib != String.Empty) { clTara.BackgroundColor = new iTextSharp.text.BaseColor(255, 0, 0); }
                        tblOcho.AddCell(clTara);

                        PdfPCell clFechaIngresoDet = new PdfPCell(new Phrase("FechaIngreso", _sFontDetalle));
                        clFechaIngresoDet = new PdfPCell(new Phrase(oLis_BE_DocumentoOrigen[i].sFechaIngreso, _sFontDetallePequena));
                        clFechaIngresoDet.BorderWidth = 0.75f;
                        clFechaIngresoDet.HorizontalAlignment = 1;
                        if (oLis_BE_DocumentoOrigen[0].sPesoInvLib != String.Empty) { clFechaIngresoDet.BackgroundColor = new iTextSharp.text.BaseColor(255, 0, 0); }
                        tblOcho.AddCell(clFechaIngresoDet);

                        PdfPCell clTipoCarga = new PdfPCell(new Phrase("TipoCarga", _sFontDetalle));
                        clTipoCarga = new PdfPCell(new Phrase(oLis_BE_DocumentoOrigen[i].sTipoCarga, _sFontDetallePequena));
                        clTipoCarga.BorderWidth = 0.75f;
                        clTipoCarga.HorizontalAlignment = 0;
                        if (oLis_BE_DocumentoOrigen[0].sPesoInvLib != String.Empty) { clTipoCarga.BackgroundColor = new iTextSharp.text.BaseColor(255, 0, 0); }
                        tblOcho.AddCell(clTipoCarga);

                        PdfPCell clObservacionDet = new PdfPCell(new Phrase("ObservacionDet", _sFontDetalle));
                        clObservacionDet = new PdfPCell(new Phrase(oLis_BE_DocumentoOrigen[i].sObservacionEir, _sFontDetallePequena));
                        clObservacionDet.BorderWidth = 0.75f;
                        clObservacionDet.HorizontalAlignment = 0;
                        if (oLis_BE_DocumentoOrigen[0].sPesoInvLib != String.Empty) { clObservacionDet.BackgroundColor = new iTextSharp.text.BaseColor(255, 0, 0); }
                        tblOcho.AddCell(clObservacionDet);

                        PdfPCell clPesoManifDatoDetalle = new PdfPCell(new Phrase("PesoManifDatoDetalle", _sFontDetalle));
                        clPesoManifDatoDetalle = new PdfPCell(new Phrase(oLis_BE_DocumentoOrigen[i].dPesoManif.ToString(), _sFontDetallePequena));
                        clPesoManifDatoDetalle.BorderWidth = 0.75f;
                        clPesoManifDatoDetalle.HorizontalAlignment = 2;
                        if (oLis_BE_DocumentoOrigen[0].sPesoInvLib != String.Empty) { clPesoManifDatoDetalle.BackgroundColor = new iTextSharp.text.BaseColor(255, 0, 0); }
                        tblOcho.AddCell(clPesoManifDatoDetalle);

                        PdfPCell clPesoRecibDatoDetalle = new PdfPCell(new Phrase("PesoRecibDatoDetalle", _sFontDetalle));
                        clPesoRecibDatoDetalle = new PdfPCell(new Phrase(oLis_BE_DocumentoOrigen[i].dPesoRecibido.ToString(), _sFontDetallePequena));
                        clPesoRecibDatoDetalle.BorderWidth = 0.75f;
                        clPesoRecibDatoDetalle.HorizontalAlignment = 2;
                        if (oLis_BE_DocumentoOrigen[0].sPesoInvLib != String.Empty) { clPesoRecibDatoDetalle.BackgroundColor = new iTextSharp.text.BaseColor(255, 0, 0); }
                        tblOcho.AddCell(clPesoRecibDatoDetalle);

                        PdfPCell clPrecintoManif = new PdfPCell(new Phrase("PrecintoManif", _sFontDetalle));
                        clPrecintoManif = new PdfPCell(new Phrase(oLis_BE_DocumentoOrigen[i].sPrecintoManifestado, _sFontDetallePequena));
                        clPrecintoManif.BorderWidth = 0.75f;
                        clPrecintoManif.HorizontalAlignment = 0;
                        if (oLis_BE_DocumentoOrigen[0].sPesoInvLib != String.Empty) { clPrecintoManif.BackgroundColor = new iTextSharp.text.BaseColor(255, 0, 0); }
                        tblOcho.AddCell(clPrecintoManif);

                        PdfPCell clPrecintoRecib = new PdfPCell(new Phrase("PrecintoRecib", _sFontDetalle));
                        clPrecintoRecib = new PdfPCell(new Phrase(oLis_BE_DocumentoOrigen[i].sPrecintoRecepcionado, _sFontDetallePequena));
                        clPrecintoRecib.BorderWidth = 0.75f;
                        clPrecintoRecib.HorizontalAlignment = 0;
                        if (oLis_BE_DocumentoOrigen[0].sPesoInvLib != String.Empty) { clPrecintoRecib.BackgroundColor = new iTextSharp.text.BaseColor(255, 0, 0); }
                        tblOcho.AddCell(clPrecintoRecib);

                        PdfPCell clSini = new PdfPCell(new Phrase("Sini", _sFontDetalle));
                        clSini = new PdfPCell(new Phrase(oLis_BE_DocumentoOrigen[i].sSini, _sFontDetallePequena));
                        clSini.BorderWidth = 0.75f;
                        clSini.HorizontalAlignment = 0;
                        if (oLis_BE_DocumentoOrigen[0].sPesoInvLib != String.Empty) { clSini.BackgroundColor = new iTextSharp.text.BaseColor(255, 0, 0); }
                        tblOcho.AddCell(clSini);
                    }


                    if (oLis_BE_DocumentoOrigen[i].sCondicion == "CS" || oLis_BE_DocumentoOrigen[i].sCondicion == "VEH")
                    {
                        if (iFila == 0)
                        {
                            PdfPCell clCondicCargaDatoCS = new PdfPCell(new Phrase("CondicCargaDato", _sFontDetalleNegrita));
                            clCondicCargaDatoCS = new PdfPCell(new Phrase(oLis_BE_DocumentoOrigen[i].sCondicCarga, _sFontDetalleNegrita));
                            clCondicCargaDatoCS.BorderWidth = 0;
                            clCondicCargaDatoCS.HorizontalAlignment = 0;
                            clCondicCargaDatoCS.Colspan = 7;
                            tblNueve.AddCell(clCondicCargaDatoCS);

                            PdfPCell clItemTitulo = new PdfPCell(new Phrase("ItemTitulo", _sFontDetalleNegrita));
                            clItemTitulo = new PdfPCell(new Phrase("Id", _sFontDetalleNegrita));
                            clItemTitulo.BorderWidth = 0.75f;
                            clItemTitulo.HorizontalAlignment = 1;
                            tblNueve.AddCell(clItemTitulo);

                            PdfPCell clFechaIngresoDetTitulo = new PdfPCell(new Phrase("FechaIngresoTitulo", _sFontDetalleNegrita));
                            clFechaIngresoDetTitulo = new PdfPCell(new Phrase("Fecha Ingreso", _sFontDetalleNegrita));
                            clFechaIngresoDetTitulo.BorderWidth = 0.75f;
                            clFechaIngresoDetTitulo.HorizontalAlignment = 1;
                            tblNueve.AddCell(clFechaIngresoDetTitulo);

                            PdfPCell clTipoCargaTitulo = new PdfPCell(new Phrase("TipoCargaTitulo", _sFontDetalleNegrita));
                            clTipoCargaTitulo = new PdfPCell(new Phrase("Descripción", _sFontDetalleNegrita));
                            clTipoCargaTitulo.BorderWidth = 0.75f;
                            clTipoCargaTitulo.HorizontalAlignment = 1;
                            tblNueve.AddCell(clTipoCargaTitulo);

                            PdfPCell clPesoActualDatoDetalleTitulo = new PdfPCell(new Phrase("PesoActualDatoDetalleTitulo", _sFontDetalleNegrita));
                            clPesoActualDatoDetalleTitulo = new PdfPCell(new Phrase("Peso Actual", _sFontDetalleNegrita));
                            clPesoActualDatoDetalleTitulo.BorderWidth = 0.75f;
                            clPesoActualDatoDetalleTitulo.HorizontalAlignment = 2;
                            tblNueve.AddCell(clPesoActualDatoDetalleTitulo);

                            PdfPCell clBultosActualDatoDetalleTitulo = new PdfPCell(new Phrase("BultosActualDatoDetalleTitulo", _sFontDetalleNegrita));
                            clBultosActualDatoDetalleTitulo = new PdfPCell(new Phrase("Bultos Actual", _sFontDetalleNegrita));
                            clBultosActualDatoDetalleTitulo.BorderWidth = 0.75f;
                            clBultosActualDatoDetalleTitulo.HorizontalAlignment = 2;
                            tblNueve.AddCell(clBultosActualDatoDetalleTitulo);

                            PdfPCell clPesoInventarioDatoDetalleTitulo = new PdfPCell(new Phrase("PesoInventarioDatoDetalleTitulo", _sFontDetalleNegrita));
                            clPesoInventarioDatoDetalleTitulo = new PdfPCell(new Phrase("Peso Inv", _sFontDetalleNegrita));
                            clPesoInventarioDatoDetalleTitulo.BorderWidth = 0.75f;
                            clPesoInventarioDatoDetalleTitulo.HorizontalAlignment = 2;
                            tblNueve.AddCell(clPesoInventarioDatoDetalleTitulo);

                            PdfPCell clBultosInventarioDatoDetalleTitulo = new PdfPCell(new Phrase("BultosInventarioDatoDetalleTitulo", _sFontDetalleNegrita));
                            clBultosInventarioDatoDetalleTitulo = new PdfPCell(new Phrase("Bultos Inv", _sFontDetalleNegrita));
                            clBultosInventarioDatoDetalleTitulo.BorderWidth = 0.75f;
                            clBultosInventarioDatoDetalleTitulo.HorizontalAlignment = 2;
                            tblNueve.AddCell(clBultosInventarioDatoDetalleTitulo);

                            iFila = 1;
                        }
                        sCondicion = oLis_BE_DocumentoOrigen[i].sCondicion;

                        PdfPCell clItem = new PdfPCell(new Phrase("Item", _sFontDetalle));
                        clItem = new PdfPCell(new Phrase((i + 1).ToString(), _sFontDetallePequena));
                        clItem.BorderWidth = 0.75f;
                        clItem.HorizontalAlignment = 1;
                        if (oLis_BE_DocumentoOrigen[0].sPesoInvLib != String.Empty) { clItem.BackgroundColor = new iTextSharp.text.BaseColor(255, 0, 0); }
                        tblNueve.AddCell(clItem);

                        PdfPCell clFechaIngresoDet = new PdfPCell(new Phrase("FechaIngreso", _sFontDetalle));
                        clFechaIngresoDet = new PdfPCell(new Phrase(oLis_BE_DocumentoOrigen[i].sFechaIngreso, _sFontDetallePequena));
                        clFechaIngresoDet.BorderWidth = 0.75f;
                        clFechaIngresoDet.HorizontalAlignment = 1;
                        if (oLis_BE_DocumentoOrigen[0].sPesoInvLib != String.Empty) { clFechaIngresoDet.BackgroundColor = new iTextSharp.text.BaseColor(255, 0, 0); }
                        tblNueve.AddCell(clFechaIngresoDet);

                        PdfPCell clTipoCarga = new PdfPCell(new Phrase("TipoCarga", _sFontDetalle));
                        clTipoCarga = new PdfPCell(new Phrase(oLis_BE_DocumentoOrigen[i].sTipoCarga, _sFontDetallePequena));
                        clTipoCarga.BorderWidth = 0.75f;
                        clTipoCarga.HorizontalAlignment = 0;
                        if (oLis_BE_DocumentoOrigen[0].sPesoInvLib != String.Empty) { clTipoCarga.BackgroundColor = new iTextSharp.text.BaseColor(255, 0, 0); }
                        tblNueve.AddCell(clTipoCarga);

                        PdfPCell clPesoRecibidoDatoDetalle = new PdfPCell(new Phrase("PesoRecibidoDatoDetalle", _sFontDetalle));
                        clPesoRecibidoDatoDetalle = new PdfPCell(new Phrase(oLis_BE_DocumentoOrigen[i].dPesoRecibido.ToString(), _sFontDetallePequena));
                        clPesoRecibidoDatoDetalle.BorderWidth = 0.75f;
                        clPesoRecibidoDatoDetalle.HorizontalAlignment = 2;
                        if (oLis_BE_DocumentoOrigen[0].sPesoInvLib != String.Empty) { clPesoRecibidoDatoDetalle.BackgroundColor = new iTextSharp.text.BaseColor(255, 0, 0); }
                        tblNueve.AddCell(clPesoRecibidoDatoDetalle);

                        PdfPCell clBultoRecibidoDatoDetalle = new PdfPCell(new Phrase("BultoReciboDatoDetalle", _sFontDetalle));
                        clBultoRecibidoDatoDetalle = new PdfPCell(new Phrase(oLis_BE_DocumentoOrigen[i].dBultoRecib2.ToString(), _sFontDetallePequena));
                        clBultoRecibidoDatoDetalle.BorderWidth = 0.75f;
                        clBultoRecibidoDatoDetalle.HorizontalAlignment = 2;
                        if (oLis_BE_DocumentoOrigen[0].sPesoInvLib != String.Empty) { clBultoRecibidoDatoDetalle.BackgroundColor = new iTextSharp.text.BaseColor(255, 0, 0); }
                        tblNueve.AddCell(clBultoRecibidoDatoDetalle);

                        PdfPCell clPesoInventariadoDatoDetalle = new PdfPCell(new Phrase("PesoInventariadoDatoDetalle", _sFontDetalle));
                        clPesoInventariadoDatoDetalle = new PdfPCell(new Phrase(String.Empty, _sFontDetallePequena));
                        clPesoInventariadoDatoDetalle.BorderWidth = 0.75f;
                        clPesoInventariadoDatoDetalle.HorizontalAlignment = 2;
                        if (oLis_BE_DocumentoOrigen[0].sPesoInvLib != String.Empty) { clPesoInventariadoDatoDetalle.BackgroundColor = new iTextSharp.text.BaseColor(255, 0, 0); }
                        tblNueve.AddCell(clPesoInventariadoDatoDetalle);

                        PdfPCell clBultoInventariadoDatoDetalle = new PdfPCell(new Phrase("BultoInventariadoDatoDetalle", _sFontDetalle));
                        clBultoInventariadoDatoDetalle = new PdfPCell(new Phrase(String.Empty, _sFontDetallePequena));
                        clBultoInventariadoDatoDetalle.BorderWidth = 0.75f;
                        clBultoInventariadoDatoDetalle.HorizontalAlignment = 2;
                        if (oLis_BE_DocumentoOrigen[0].sPesoInvLib != String.Empty) { clBultoInventariadoDatoDetalle.BackgroundColor = new iTextSharp.text.BaseColor(255, 0, 0); }
                        tblNueve.AddCell(clBultoInventariadoDatoDetalle);
                    }
                }

                //Relaciona la tabla ocho en la uno
                PdfPCell clFila_8_1 = new PdfPCell(tblOcho);
                clFila_8_1.Colspan = 3;
                clFila_8_1.BorderWidth = 0;
                tblUno.AddCell(clFila_8_1);

                //Relaciona la tabla nueve en la uno
                PdfPCell clFila_9_1 = new PdfPCell(tblNueve);
                clFila_9_1.Colspan = 3;
                clFila_9_1.BorderWidth = 0;
                tblUno.AddCell(clFila_9_1);

                //Relaciona la tabla diez en la uno
                PdfPCell clFila_10_1 = new PdfPCell(tblDiez);
                clFila_10_1.Colspan = 3;
                clFila_10_1.BorderWidth = 0;
                tblUno.AddCell(clFila_10_1);


                //Genera la tabla 
                PdfPTable tblDoce = new PdfPTable(1);
                tblDoce.TotalWidth = 830f;
                float[] widthsDoce = new float[] { 830f };
                tblDoce.SetWidths(widthsDoce);
                tblDoce.HorizontalAlignment = 0;
                tblDoce.SpacingBefore = 10f;

                clVacio = new PdfPCell(new Phrase(" ", _sFontNormalNegrita));
                clVacio.BorderWidth = 0;
                clVacio.HorizontalAlignment = 0;
                tblDoce.AddCell(clVacio);

                clVacio = new PdfPCell(new Phrase(" ", _sFontNormalNegrita));
                clVacio.BorderWidth = 0;
                clVacio.HorizontalAlignment = 0;
                tblDoce.AddCell(clVacio);

                clVacio = new PdfPCell(new Phrase(" ", _sFontNormalNegrita));
                clVacio.BorderWidth = 0;
                clVacio.HorizontalAlignment = 0;
                tblDoce.AddCell(clVacio);

                clVacio = new PdfPCell(new Phrase(" ", _sFontNormalNegrita));
                clVacio.BorderWidth = 0;
                clVacio.HorizontalAlignment = 0;
                tblDoce.AddCell(clVacio);

                clVacio = new PdfPCell(new Phrase(" ", _sFontNormalNegrita));
                clVacio.BorderWidth = 0;
                clVacio.HorizontalAlignment = 0;
                tblDoce.AddCell(clVacio);

                PdfPCell clNombreDocumento = new PdfPCell(new Phrase("NombreDocumento", _sFontTituloIzquierda));
                clNombreDocumento = new PdfPCell(new Phrase("R01/SIG-IN-OP-006", _sFontTituloIzquierda));
                clNombreDocumento.BorderWidth = 0;
                clNombreDocumento.HorizontalAlignment = 0;
                tblDoce.AddCell(clNombreDocumento);

                PdfPCell clVersionDocumento = new PdfPCell(new Phrase("VersionDocumento", _sFontTituloIzquierda));
                clVersionDocumento = new PdfPCell(new Phrase("Rev 00", _sFontTituloIzquierda));
                clVersionDocumento.BorderWidth = 0;
                clVersionDocumento.HorizontalAlignment = 0;
                tblDoce.AddCell(clVersionDocumento);

                //Relaciona la tabla siete en la uno
                PdfPCell clFila_12_1 = new PdfPCell(tblDoce);
                clFila_12_1.Colspan = 3;
                clFila_12_1.BorderWidth = 0;
                tblUno.AddCell(clFila_12_1);


                //Muestra las tablas en el documento
                pdfDoc.Add(tblUno);
                pdfDoc.Close();
                result = ms.ToArray();
                ms.Close();
            }


            Response.ClearHeaders();
            Response.Clear();
            Response.ContentType = "application/pdf";
            // Response.ContentType = "application/x-zip-compressed";
            Response.AddHeader("content-disposition", "attachment;filename=Fargoline.pdf");
            //Response.AddHeader("content-disposition", String.Format("attachment;filename={0}", "Fargoline.zip"));
            //Response.ContentEncoding = Encoding.Default;
            Response.Buffer = true;
            Response.Cache.SetCacheability(System.Web.HttpCacheability.NoCache);
            Response.BinaryWrite(result);
            Response.Flush();

            HttpContext.Current.ApplicationInstance.CompleteRequest();
            Response.End();
            Response.Close();
        }
    }


    protected void LlenaCombos()
        {
            BE_Cliente oBE_Cliente = new BE_Cliente();
            oBE_Cliente.iIdRol = 42;
            oBE_Cliente.NPagina = 1;
            oBE_Cliente.NRegistros = 999999;

            DA_Contrato oDA_Contrato = new DA_Contrato();
            //IList<BE_TablaUc> lista = null;

            DdlAgencia.DataSource = oDA_Contrato.ListarCliente(oBE_Cliente);
            DdlAgencia.DataValueField = "iIdValor";
            DdlAgencia.DataTextField = "sDescripcion";
            DdlAgencia.DataBind();
            DdlAgencia.SelectedValue = String.Empty;
        }
    #endregion



    #region Metodo Transacciones
        DataNavigatorParams BindGridLista(object sender, EventArgs e)
        {
            BL_DocumentoOrigen oBL_DocumentoOrigen = new BL_DocumentoOrigen();
            BE_DocumentoOrigen oBE_DocumentoOrigen = new BE_DocumentoOrigen();

            String lsDocumentos = String.Empty;

            for (int i = 0; i < LstDocumentos.Items.Count; i++)
            {
                lsDocumentos = lsDocumentos + LstDocumentos.Items[i].ToString() + "|";
            }

            oBE_DocumentoOrigen.sDocumentoHijo = lsDocumentos;
            oBE_DocumentoOrigen.iIdCliente = Convert.ToInt32(DdlAgencia.SelectedValue);  //GlobalEntity.Instancia.IdCliente;
            oBE_DocumentoOrigen.sUsuario = GlobalEntity.Instancia.Usuario;
            oBE_DocumentoOrigen.sNombrePc = GlobalEntity.Instancia.NombrePc;

            GrvListado.PageSize = 999;
            oBE_DocumentoOrigen.NPagina = dnvListado.CurrentPage;
            oBE_DocumentoOrigen.NRegistros = GrvListado.PageSize;

            IList<BE_DocumentoOrigen> lista = oBL_DocumentoOrigen.ListarBLsparaVolante(oBE_DocumentoOrigen);
            dnvListado.Visible = (oBE_DocumentoOrigen.NTotalRegistros > oBE_DocumentoOrigen.NRegistros);
            LblTotal.Text = "Total de Registros: " + Convert.ToString(oBE_DocumentoOrigen.NTotalRegistros);
            UdpTotales.Update();

            return new DataNavigatorParams(lista, oBE_DocumentoOrigen.NTotalRegistros);
        }
    #endregion



    #region "Metodo Controles"
    
    
    #endregion
}