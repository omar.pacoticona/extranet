﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using QNET.Web.UI.Controls;
using FENIX.BusinessEntity;
using FENIX.BusinessLogic;
using System.Text;
using FENIX.Common;
using System.Collections.Generic;
using System.Drawing;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.IO;
using System.Net;
using wsExtranet.SSRSws;

public partial class Extranet_Operaciones_eFENIX_VGM_Consulta_Detalle : System.Web.UI.Page
{
    #region "Evento Pagina"

    BE_DocumentoOrigenListaList oBE_DocumentoOrigenListaList;
    String urlTicketBalanza = ConfigurationManager.AppSettings["urlTicketBalanza"].ToString();
    String userSSRS = ConfigurationManager.AppSettings["userSSRS"].ToString();
    String passwordSSRS = ConfigurationManager.AppSettings["passwordSSRS"].ToString();
    String domainSSRS = ConfigurationManager.AppSettings["domainSSRS"].ToString();

    protected void Page_Load(object sender, EventArgs e)
        {
        
        dnvListado.BindGridView += new QNET.Web.UI.Controls.DataNavigator.BindGridViewDelegate(BindGridLista);


        ScriptManager scriptManager = ScriptManager.GetCurrent(this.Page);
        scriptManager.RegisterPostBackControl(this.GrvListado);        
            TxtDocumentoM.Attributes.Add("onkeydown", "javascript:return fc_PressKeyDO();");

            String tiempo = (Convert.ToInt32(GlobalEntity.Instancia.TiempoDuracionSesion.ToString()) * 1000 * 60).ToString();
            Session["Reset"] = true;

            if (!Page.IsPostBack)
            {
            TxtDocumentoM.Text = Request.QueryString["sNumeroDO"];
            if(TxtDocumentoM.Text != "")
            {
                dnvListado.InvokeBind();

                if (GrvListado.Rows.Count == 0)
                {
                    List<BE_DocumentoOrigen> lsBE_DocumentoOrigen = new List<BE_DocumentoOrigen>();
                    BE_DocumentoOrigen oBE_DocumentoOrigen = new BE_DocumentoOrigen();
                    lsBE_DocumentoOrigen.Add(oBE_DocumentoOrigen);
                    GrvListado.DataSource = lsBE_DocumentoOrigen;
                    GrvListado.DataBind();
                }
            }


                ViewState["Doc"] = Request.QueryString["sNumeroDO"];
                ViewState["Cliente"] = Request.QueryString["sCliente"];                 
                ViewState["IdDocOri"] = Request.QueryString["sIdDocOri"];
                ViewState["sNroManif"] = Request.QueryString["sNroManif"];
                ViewState["sPeriodoManif"] = Request.QueryString["sPeriodoManif"];
            if (GrvListado.Rows.Count == 0)
                {
                    List<BE_DocumentoOrigen> lsBE_DocumentoOrigen = new List<BE_DocumentoOrigen>();
                    BE_DocumentoOrigen oBE_DocumentoOrigen = new BE_DocumentoOrigen();
                   //lsBE_DocumentoOrigen.Add(oBE_DocumentoOrigen);
                    GrvListado.DataSource = lsBE_DocumentoOrigen;
                    GrvListado.DataBind();
                }

                ScriptManager.RegisterStartupScript(this, GetType(), "mensaje", "SessionExpireAlert(" + tiempo + ");", true);
            }
            else
                ScriptManager.RegisterStartupScript(this, GetType(), "mensaje", "myStopFunction();", true);
        }

        protected void ImgBtnBuscar_Click(object sender, ImageClickEventArgs e)
        {
            dnvListado.InvokeBind();

            if (GrvListado.Rows.Count == 0)
            {
                List<BE_DocumentoOrigen> lsBE_DocumentoOrigen = new List<BE_DocumentoOrigen>();
                BE_DocumentoOrigen oBE_DocumentoOrigen = new BE_DocumentoOrigen();
                lsBE_DocumentoOrigen.Add(oBE_DocumentoOrigen);
                GrvListado.DataSource = lsBE_DocumentoOrigen;
                GrvListado.DataBind();
            }
        }

    protected void GrvListado_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        //if (e.Row.RowType == DataControlRowType.DataRow)
        //{
        //    if(Int32.Parse(e.Row.Cells[4].Text) < 1)
        //    {
        //        e.Row.Cells[4].Visible = false;
        //    }
        //    else
        //    {
        //        e.Row.Cells[4].Visible = true;
        //    }

        //    if (Int32.Parse(e.Row.Cells[5].Text) < 1)
        //    {
        //        e.Row.Cells[4].Visible = false;
        //    }
        //    else
        //    {
        //        e.Row.Cells[4].Visible = true;
        //    }
        //}
    }

        protected void GrvListado_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        //  Int32 iIdVolante = 0;
        Int32 iIdMovBalIngreso = 0;
        Int32 iIdMovBalSalida = 0;
        String usuarioImp = "";
        String NombreZip = String.Empty;
        String verTicketBalanza = urlTicketBalanza;
        String user = userSSRS;
        String password = passwordSSRS;
        String domain = domainSSRS;


        usuarioImp = GlobalEntity.Instancia.Usuario;

        if (e.CommandName == "Ticket Ingreso")
        {
            iIdMovBalIngreso = Convert.ToInt32(GrvListado.DataKeys[Convert.ToInt32(e.CommandArgument)].Values["iIdMovBalIngreso"].ToString());
            if (iIdMovBalIngreso <= 0)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "mensaje", "No existe ticket de ingreso", true);
                return;
            }
            NombreZip = "Ticket Ingreso - "+GrvListado.DataKeys[Convert.ToInt32(e.CommandArgument)].Values["iTicketIngreso"].ToString();       
        
            ReportExecutionService rs = new ReportExecutionService();           
            rs.Credentials = new System.Net.NetworkCredential("userReport", "Reportes2019@", "FSAFARGOLINE107");           
            rs.LoadReport(verTicketBalanza, null);
       
                        List<ParameterValue> parameters = new List<ParameterValue>();
            parameters.Add(new ParameterValue { Name = "vi_IdMovBal",Value = iIdMovBalIngreso.ToString()});
            parameters.Add(new ParameterValue { Name = "vi_Usuario", Value = usuarioImp.ToString() });
            rs.SetExecutionParameters(parameters.ToArray(),"en-US");
            String deviceInfo = "<DeviceInfo><Toolbar>False</Toolbar></DeviceInfo>";
            String mimeType;
            String encoding;
            String[] streamId;
            Warning[] warning;
            var result = rs.Render("PDF", deviceInfo, out mimeType, out encoding, out encoding, out warning, out streamId);  

            Response.ClearHeaders();
            Response.Clear();
            Response.ContentType = "application/pdf";          
            Response.AddHeader("content-disposition", "attachment;filename="+ NombreZip + ".pdf");          
            Response.Buffer = true;
            Response.Cache.SetCacheability(System.Web.HttpCacheability.NoCache);
            Response.BinaryWrite(result);
            Response.Flush();
            HttpContext.Current.ApplicationInstance.CompleteRequest();
            Response.End();
            Response.Close();
        }
        if (e.CommandName == "Ticket Embarque")
        {
            iIdMovBalSalida = Convert.ToInt32(GrvListado.DataKeys[Convert.ToInt32(e.CommandArgument)].Values["iIdMovBalSalida"].ToString());
            if (iIdMovBalSalida <= 0)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "mensaje", "No existe ticket de salida", true);
                return;
            }
            NombreZip = "Ticket Embarque - " + GrvListado.DataKeys[Convert.ToInt32(e.CommandArgument)].Values["iTicketSalida"].ToString();

            ReportExecutionService rs = new ReportExecutionService();
            rs.Credentials = new System.Net.NetworkCredential("userReport", "Reportes2019@", "FSAFARGOLINE107");
            rs.LoadReport(verTicketBalanza, null);

            List<ParameterValue> parameters = new List<ParameterValue>();
            parameters.Add(new ParameterValue { Name = "vi_IdMovBal", Value = iIdMovBalSalida.ToString() });
            parameters.Add(new ParameterValue { Name = "vi_Usuario", Value = usuarioImp.ToString() });
            rs.SetExecutionParameters(parameters.ToArray(), "en-US");
            String deviceInfo = "<DeviceInfo><Toolbar>False</Toolbar></DeviceInfo>";
            String mimeType;
            String encoding;
            String[] streamId;
            Warning[] warning;
            var result = rs.Render("PDF", deviceInfo, out mimeType, out encoding, out encoding, out warning, out streamId);

            Response.ClearHeaders();
            Response.Clear();
            Response.ContentType = "application/pdf";
            Response.AddHeader("content-disposition", "attachment;filename=" + NombreZip + ".pdf");
            Response.Buffer = true;
            Response.Cache.SetCacheability(System.Web.HttpCacheability.NoCache);
            Response.BinaryWrite(result);
            Response.Flush();
            HttpContext.Current.ApplicationInstance.CompleteRequest();
            Response.End();
            Response.Close();
        }

    }

   
        protected void btnConsulta_Click(object sender, EventArgs e)
        {
            dnvListado.InvokeBind();

            if (GrvListado.Rows.Count == 0)
            {
                List<BE_DocumentoOrigen> lsBE_DocumentoOrigen = new List<BE_DocumentoOrigen>();
                BE_DocumentoOrigen oBE_DocumentoOrigen = new BE_DocumentoOrigen();
                lsBE_DocumentoOrigen.Add(oBE_DocumentoOrigen);
                GrvListado.DataSource = lsBE_DocumentoOrigen;
                GrvListado.DataBind();
            }
        }

        protected void ImgBtnBuscar_Click(object sender, EventArgs e)
        {
            dnvListado.InvokeBind();

            if (GrvListado.Rows.Count == 0)
            {
                List<BE_DocumentoOrigen> lsBE_DocumentoOrigen = new List<BE_DocumentoOrigen>();
                BE_DocumentoOrigen oBE_DocumentoOrigen = new BE_DocumentoOrigen();
                lsBE_DocumentoOrigen.Add(oBE_DocumentoOrigen);
                GrvListado.DataSource = lsBE_DocumentoOrigen;
                GrvListado.DataBind();
            }
        }

        protected void btn_cerrar_Click(object sender, EventArgs e)
        {
            if (GlobalEntity.Instancia.CerrarExtranet == "S")
            {
                BL_Usuario oBL_Usuario = new BL_Usuario();
                oBL_Usuario.GrabaInicioSesion(GlobalEntity.Instancia.Usuario, "0");
                Session.Abandon();
                FormsAuthentication.SignOut();
                Response.Redirect("../../eFENIX_Login.aspx");
                Response.End();
            }
        }
    #endregion

 
    protected void ImgBtnItems_Click(object sender, ImageClickEventArgs e)
    {
        dnvListado.InvokeBind();
        if (GrvListado.Rows.Count == 0)
        {
            List<BE_DocumentoOrigenDetalle> lsBE_DocumentoOrigenDetalle = new List<BE_DocumentoOrigenDetalle>();
            BE_DocumentoOrigenDetalle oBE_DocumentoOrigen = new BE_DocumentoOrigenDetalle();
            lsBE_DocumentoOrigenDetalle.Add(oBE_DocumentoOrigen);
            GrvListado.DataSource = lsBE_DocumentoOrigenDetalle;
            GrvListado.DataBind();
        }
    }
    #region Metodo Transacciones
    DataNavigatorParams BindGridLista(object sender, EventArgs e)
        {
            BL_DocumentoOrigen oBL_DocumentoOrigen = new BL_DocumentoOrigen();
            BE_DocumentoOrigen oBE_DocumentoOrigen = new BE_DocumentoOrigen();      
        BE_DocumentoOrigenDetalleListaList oBE_DocumentoOrigenDetalleListaList = new BE_DocumentoOrigenDetalleListaList();

        oBE_DocumentoOrigen.iIdCliente = GlobalEntity.Instancia.IdCliente;
        oBE_DocumentoOrigen.sNumeroDO = TxtDocumentoM.Text.Trim();
        GrvListado.PageSize = 10;
        oBE_DocumentoOrigen.NPagina = dnvListado.CurrentPage;
        oBE_DocumentoOrigen.NRegistros = GrvListado.PageSize;
        oBE_DocumentoOrigenDetalleListaList = oBL_DocumentoOrigen.ListarDODetalle(oBE_DocumentoOrigen);
        //    oBE_DocumentoOrigen.iIdLineaNegocio = 89; //DEP.TEMP.  GlobalEntity.Instancia.IdLineaNegocio;
        //    oBE_DocumentoOrigen.NPagina = 1;
        //    oBE_DocumentoOrigen.NRegistros = 50;
        //    oBE_DocumentoOrigen.iIdTipoOperacion = 65;
        //oBE_DocumentoOrigenListaList = new BE_DocumentoOrigenListaList();
        //oBE_DocumentoOrigenListaList = oBL_DocumentoOrigen.ListarDO(oBE_DocumentoOrigen);
        

        GrvListado.Width = 1055;
            dnvListado.Visible = (oBE_DocumentoOrigenDetalleListaList.Count() > 0);
        //LblTotal.Text = "Total de Registros: " + Convert.ToString(oBE_DocumentoOrigenDetalleListaList.Count());
        LblTotal.Text = "Total de Registros: " + Convert.ToString(oBE_DocumentoOrigen.NtotalRegistros);
        UdpTotales.Update();

        //return new DataNavigatorParams(oBE_DocumentoOrigenDetalleListaList, oBE_DocumentoOrigenDetalleListaList.Count());
        return new DataNavigatorParams(oBE_DocumentoOrigenDetalleListaList, oBE_DocumentoOrigen.NtotalRegistros);
    }

   
    #endregion
    
 }