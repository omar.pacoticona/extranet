﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="eFENIX_Tracking_Carga_Imagenes.aspx.cs" Inherits="Extranet_Documentacion_AutorizacionRetiroPDF" %>

<!DOCTYPE html>
<html>
<head>
    <title>Imagenes - Tarja</title>
    <style>
        .auto-style1 {
            height: 37px;
        }
    </style>


    <link href="../../Script/Hojas_Estilo/jquery.rondell.css" rel="stylesheet" />

    <script src="../../Script/jquery/modernizr-2.0.6.min.js"></script>
    <script src="../../Script/jquery/jquery-1.10.2.min.js"></script>
    <script src="../../Script/jquery/jquery.mousewheel-3.0.6.min.js"></script>
    <script src="../../Script/jquery/jquery.rondell.min.js"></script>


    <link href="../../Script/Hojas_Estilo/ccFenix.css" rel="stylesheet" type="text/css" />
    <link href="../../Script/Hojas_Estilo/dialog_box.css" rel="stylesheet" type="text/css" />
    <link href="../../Script/Hojas_Estilo/eFenix.css" rel="stylesheet" type="text/css" />
</head>


<body>
    <form id="form1" runat="server">
    <table id="sidebar" style="width: 100%;" align="center" border="0" cellpadding="0" cellspacing="0">
        <tr style="width: 100%; vertical-align: middle">
            <td colspan="2" style="text-align: left;">
                <table style="height: 100%; width: 100%;" border="0px" cellpadding="0" cellspacing="0">
                    <tr>
                        <td style="width: 70%;">
                            <img alt="" src="../../Imagenes/barra.jpg" style="width: 100%; height: 105px" />
                        </td>
                        <td style="text-align: right;">
                            <img alt="" src="../../Imagenes/logo.jpg" style="height: 80px; width: 250px;" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="form_titulo">DETALLE DEL TRACKING A LA CARGA -  IMAGENES EN EL ITEM (TARJA)
            </td>
        </tr>
        <tr>
            <td class="auto-style1">
                <table  align="center">
                    <tr>
                        <td>
                         <%--   <div style="width: 100%; margin-left: auto; margin-right: auto;align-content: center">
                                <form id="form1" runat="server" style="align-content: center" >--%>
                                    <div id="rondellCarousel">
                                        <asp:Repeater ID="Repeater1" runat="server">
                                            <ItemTemplate>
                                                <a href='<%#Eval("Ruta") %>' title='<%#Eval("Titulo") %>'>
                                                    <img src='<%#Eval("Ruta") %>' />
                                                </a>

                                            </ItemTemplate>
                                        </asp:Repeater>

                                    </div>
                         <%--       </form>
                            </div>--%>
                        </td>
                    </tr>
                </table>
            </td>

        </tr>
        <tr>
            <td class="form_titulo">
                <asp:Button runat="server" Text="descargar" ID="btnDescargarImg" OnClick="btn_cerrar_Click" />
            </td>
        </tr>
    </table>

    </form>

</body>
</html>
<script>
    //window.onload = function () {
    //    myStopFunction();
    //}

    $(function () {
        $("#rondellCarousel").rondell({
            preset: "carousel",
        });
    });


    var tiempo = 0;
    var seconds = 0;

    var myVar = setInterval(function () {
        clearInterval(myVar);
    }, 1000);

    var myTiempo = setTimeout(function () {
        clearInterval(myTiempo);
    }, 1);

    function myStopFunction() {
        clearInterval(myVar);
        seconds = '<%=GlobalEntity.Instancia.TiempoDuracionSesion%>' * 60;
        tiempo = '<%=GlobalEntity.Instancia.TiempoDuracionSesion%>' * 1000 * 60;

        CuentaTiempo(tiempo);
    }

    function CuentaTiempo(tiempo) {
        clearTimeout(myTiempo);
        myTiempo = setTimeout(function () {
            // window.location.close();//= "../Seguridad/eFENIX_Login.aspx";
            window.close();
        }, tiempo);
    }



    function SessionExpireAlert(timeout) {
        seconds = timeout / 1000;
        //document.getElementsByName("seconds").innerHTML = seconds;

        myvar = setInterval(function () {
            seconds--;
            //  document.getElementById("seconds").innerHTML = seconds;
        }, 1000);

        CuentaTiempo(timeout);
    };

    //function inhabilitar() {
    //    alert("Esta función está inhabilitada.\n\nPerdone las molestias");
    //    return false;
    //}

    //document.oncontextmenu = inhabilitar;

</script>


