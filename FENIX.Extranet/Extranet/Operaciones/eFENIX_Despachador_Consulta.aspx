﻿<%@ Page Language="C#" 
MasterPageFile="~/Extranet/Seguridad/eFENIX_Principal.master"
AutoEventWireup="true" 
CodeFile="eFENIX_Despachador_Consulta.aspx.cs" 
Inherits="Extranet_Operaciones_eFENIX_Volante_Consulta"
Title="Consulta de Volantes" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="CntMaster" runat="Server">

    <table cellspacing="0" cellpadding="0" style="width: 100%;">
        
        <tr>
            <td class="form_titulo" style="width: 85%">
                CONSULTA DE DESPACHADORES
            </td>
            <td align="right" class="form_titulo_verde" style="width: 15%" >
                <ajax:UpdatePanel ID="UdpTotales" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <asp:TextBox ID="LblTotal" Text="Total de Registros:" style="background-color: #0069ae; color: #FFFFFF" ReadOnly="true" BorderColor="#0069ae" runat="server"></asp:TextBox>
                    </ContentTemplate>
                </ajax:UpdatePanel>
            </td>               
        </tr>
        
        <tr>
            <td valign="top" colspan="2">
                <table class="form_bandeja" style="margin-top: 5px; margin-bottom: 5px; width: 100%;">
                    <tr>
                        <td style="width: 10%;">
                            Cliente
                        </td>
                        <td colspan="6">
                            <asp:TextBox ID="TxtCliente" runat="server" ReadOnly="true" BackColor="#ECE9D8" Style="text-transform: uppercase"
                                Width="514px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 10%;">
                            CODIGO</td>
                        <td style="width: 25%;">
                            <asp:TextBox ID="txtCodigo" runat="server" MaxLength="4" Style="text-transform: uppercase"
                                Width="100px"></asp:TextBox>&nbsp;</td>
                        <td style="width: 10%;">
                            NOMBRES</td>
                        <td style="width: 20%;">
                            <asp:TextBox ID="txtNombre" runat="server" MaxLength="25" Style="text-transform: uppercase"
                                Width="250px"></asp:TextBox>
                        </td>
                        <td>
                            &nbsp;
                        </td>
                        <td style="width: 10%;">
                            &nbsp;</td>
                        <td style="width: 25%;">
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td>
                             &nbsp;
                        </td>
                        <td>
                             &nbsp;
                        </td>
                        <td>
                             &nbsp;
                        </td>
                        <td>
                             &nbsp;
                        </td>
                        <td>
                             &nbsp;
                        </td>
                        <td>
                             &nbsp;
                        </td>
                        <td>
                             &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>
                            DNI</td>
                        <td>
                            <asp:TextBox ID="txtDNI" runat="server" MaxLength="25" Style="text-transform: uppercase"
                                Width="100px"></asp:TextBox>
                        </td>
                        <td>
                            APELLIDOS</td>
                        <td style="width: 20%">
                            <asp:TextBox ID="txtApellido" runat="server" Style="text-transform: uppercase"
                                MaxLength="30" Width="250px"></asp:TextBox>
                        </td>
                        <td>
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        </td>
                        <td>
                            <asp:ImageButton ID="ImgBtnBuscar" runat="server" ImageUrl="~/Imagenes/Formulario/btn_consultar.JPG"
                                OnClick="ImgBtnBuscar_Click"/>
                            </td>
                        <td>
                            <asp:ImageButton ID="ImgBtnNuevo" runat="server" ImageUrl="~/Imagenes/Formulario/btn_nuevo.JPG" OnClick="ImgBtnNuevo_Click"/>
                        </td>
                    </tr>
                </table>
             </td>   
        </tr>
    

        <tr valign="top" style="height: 100%">
            <td colspan="5">
                <ajax:UpdatePanel ID="upDocumentoOrigen" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div style="overflow: auto; width: 100%; height: 100%">
                            <asp:GridView ID="GrvListado" runat="server" AutoGenerateColumns="False" DataKeyNames="Codigo,DNI,Nombres,Apellidos,Carnet,FechaVenc,FechaVencIni,Situacion"
                                SkinID="GrillaConsulta" Width="100%" OnRowCommand="GrvListado_RowCommand">
                                <Columns>
                                    <asp:BoundField DataField="Codigo" HeaderText="CODIGO" />
                                    <asp:BoundField DataField="DNI" HeaderText="DNI" />
                                    <asp:BoundField DataField="Nombres" HeaderText="NOMRES" />
                                    <asp:BoundField DataField="Apellidos" HeaderText="APELLIDOS" />
                                    <asp:ButtonField ButtonType="Image" CommandName="OpenDespachador" HeaderText="Editar" ImageUrl="~/Script/Imagenes/IconButton/Edit_Pequeno.png"
                                            Text="Editar"></asp:ButtonField>
                                    <asp:ButtonField ButtonType="Image" CommandName="EliminarDespachador" HeaderText="Eliminar" ImageUrl="~/Script/Imagenes/IconButton/Delete_Icon.png"
                                            Text="Eliminar"></asp:ButtonField>
                                </Columns>
                            </asp:GridView>
                        </div>
                    </ContentTemplate>
                    <Triggers>
                        <ajax:AsyncPostBackTrigger ControlID="GrvListado" EventName="SelectedIndexChanged" />
                        <ajax:AsyncPostBackTrigger ControlID="ImgBtnBuscar" EventName="Click" />
                        <ajax:AsyncPostBackTrigger ControlID="dnvListado" EventName="PageChanged" />
                        <ajax:AsyncPostBackTrigger ControlID="dnvListado" EventName="NextPage" />
                        <ajax:AsyncPostBackTrigger ControlID="dnvListado" EventName="PreviousPage" />
                        <ajax:AsyncPostBackTrigger ControlID="dnvListado" EventName="FirstPage" />  
                        <ajax:AsyncPostBackTrigger ControlID="dnvListado" EventName="LastPage" />     
                    </Triggers>
                </ajax:UpdatePanel>
            </td>
        </tr>
        <tr valign="top" style="height: 6%;">
            <td colspan="5">
               <ajax:UpdatePanel ID="upDnvListado" runat="server">
                    <ContentTemplate>
                        <FENIX:DataNavigator ID="dnvListado" runat="server" AllowCustomPaging="True" ButtonText="Ir" 
                            ForeColor="White" Font-Size="11px" BackColor="#0069ae" 
                            CurrentPage="1" CustomClientFunction="" EnableAskingAfterPage="False" EnableCustomScript="False"
                            GridViewId="GrvListado" ImagesPath="~/Imagenes/DN/" IsParentShowWindow="False"
                            MessageAskingAfterPage="" PageIndicatorFormat="Página {0} / {1}" Visible="False"
                            Width="100%" WidthButtonIr="" WidthTextBoxNumPagina="25px" />
                    </ContentTemplate>
                    <Triggers>
                        <ajax:AsyncPostBackTrigger ControlID="dnvListado" EventName="PageChanged" />
                        <ajax:AsyncPostBackTrigger ControlID="ImgBtnBuscar" EventName="Click" />
                    </Triggers>
                </ajax:UpdatePanel>
            </td>
        </tr>
    </table>


    <asp:Panel ID="pnlRegistrarDespachador"  runat="server" CssClass="PanelPopup" Width="800px" BorderWidth="1"
        BorderColor="#004b8e">
        <asp:UpdatePanel ID="UpdatePanel3" runat="server">
            <ContentTemplate>
                <table cellspacing="0" cellpadding="0" border="0" style="height: 60px; width: 770px;
                    background: url( '../../Imagenes/menu/menu_titulo6.png');">
                    <tr>
                        <td align="right" style="padding-bottom: 5px; padding-top: 10px; padding-right: 15px;">
                             <asp:Button ID="btnAceptar" runat="server" Text="Grabar" CssClass="btnAcccionComision"
                                            Width="70px" OnClick="btnAceptar_Click"/>
                            <asp:Button ID="btnCerrarPopUp" runat="server" Text="Cerrar" CssClass="btnAcccionComision"
                                 Width="70px" OnClientClick="return ClosePopUpDespachador();" />
                        </td>
                    </tr>
                </table>
                <table border="0" cellpadding="2" cellspacing="1" style="width: 770px; height: 35px;
                    margin-top: 5px; margin-left: 5px; padding: 5px; background-color: #EFEFF1"
                    class="cuerpoComision">
                    <tr>
                        <td >
                           Nombres
                        </td>
                        <td>
                            <asp:TextBox ID="txtNombrePopUp" runat="server" Width="250px" SkinID="txtNormal" Style="text-transform: uppercase">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            </asp:TextBox>
                        </td>
                        <td>
                            Apellidos
                        </td>
                        <td>
                            <asp:TextBox ID="txtApellidoPopUp" runat="server" Width="250px" SkinID="txtNormal" Style="text-transform: uppercase">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            </asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            DNI
                        </td>
                        <td>
                            <asp:TextBox ID="txtDNIPopUp" runat="server" Width="100px" SkinID="txtNormal" Style="text-transform: uppercase">
                            </asp:TextBox>
                        </td>
                        <td>
                            &nbsp;</td>
                        <td>
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td colspan="7">
                            <asp:Label ID="lblRespuesta" runat="server" Text=""></asp:Label>
                        </td>
                    </tr>
                </table>
            </ContentTemplate>
            <Triggers>

            </Triggers>
        </asp:UpdatePanel>
    </asp:Panel>
    <ajaxToolkit:ModalPopupExtender ID="mpRegistrarDespachador" runat="server"
        Enabled="True" PopupControlID="pnlRegistrarDespachador" BackgroundCssClass="modalBackground"
        TargetControlID="btnOpenProxyInser" CancelControlID="btnCloseProxyInser">
    </ajaxToolkit:ModalPopupExtender>
     <asp:Button ID="btnOpenProxyInser" Style="display: none;" runat="server" Text="[Open Proxy]" />
    <asp:Button ID="btnCloseProxyInser" Style="display: none;" runat="server" Text="[Close Proxy]" />
    <script language="javascript" type="text/javascript">

        var tiempo = 0;
        var seconds = 0;

        var myVar = setInterval(function () {
            clearInterval(myVar);
        }, 1000);

        var myTiempo = setTimeout(function () {
            clearInterval(myTiempo);
        }, 1);

        function myStopFunction() {
            clearInterval(myVar);
            seconds = '<%=GlobalEntity.Instancia.TiempoDuracionSesion%>' * 60;
            tiempo = '<%=GlobalEntity.Instancia.TiempoDuracionSesion%>' * 1000 * 60;

            CuentaTiempo(tiempo);
        }

        function CuentaTiempo(tiempo) {
            clearTimeout(myTiempo);
            myTiempo = setTimeout(function () {
                document.getElementById('<%=btn_cerrar.ClientID%>').click();
                //window.location = "../Seguridad/eFENIX_Login.aspx";
            }, tiempo);
        }



        function SessionExpireAlert(timeout) {
            seconds = timeout / 1000;
            document.getElementsByName("seconds").innerHTML = seconds;

            myvar = setInterval(function () {
                seconds--;
                document.getElementById("seconds").innerHTML = seconds;
            }, 1000);

            CuentaTiempo(timeout);
        };


        var goodexit = false;
        window.onbeforeunload = confirmRegisterExit;
        function confirmRegisterExit(evt) {
            if (!goodexit) {
                //return "Si vas abandonar el sistema haz click en Cerrar Sesion!!";
                document.getElementById('<%=btn_cerrar.ClientID%>').click();
            }
        }

        var objFilaAnt = null;
        var backgroundColorFilaAnt = "";

        function fc_SeleccionaFilaSimple(objFila, objrowIndex, chkID) {
            try {

                if (objFilaAnt != null) {
                    objFilaAnt.style.backgroundColor = backgroundColorFilaAnt;
                }
                objFilaAnt = objFila;
                backgroundColorFilaAnt = objFila.style.backgroundColor;
                objFila.style.backgroundColor = "#c4e4ff";
            }

            catch (e) {
                error = e.message;
            }
        }

        function HabilitarUno(chk) {
            f = document.forms[0].elements;
            for (x = 0; x < f.length; x++) {
                obj = f[x];
                if (obj.type == 'checkbox') {
                    obj.checked = false;
                }
            }
            chk.enabled;
            chk.checked = true;
        }
        
        function fc_PressKeyDO() {
            if (event.keyCode == 13) {
                document.getElementById("<%=ImgBtnBuscar.ClientID%>").click();
                return false;
            }
            else {
                return true;
            }
        }

        function ClosePopUpDespachador() {
            document.getElementById('<%=btnCloseProxyInser.ClientID %>').click();
            return false;
        }

        function OpenPopUpDespachador(e, sObjeto) {
                document.getElementById('<%=btnOpenProxyInser.ClientID %>').click();
            return false;
        }

        <%--function VerVolante(sIdDocOri, sIdVolante) {
            document.getElementById('<%=HdIdDocOri.ClientID %>').value = sIdDocOri;
            document.getElementById('<%=HdIdVolante.ClientID %>').value = sIdVolante;

            document.getElementById("<%=ImgBtnPdf.ClientID%>").click();
        }
        
        function ValidarFiltro() {
            var sManifiesto = document.getElementById('<%=TxtNumManifiesto.ClientID %>').value;
            var sDocOrigenM = document.getElementById('<%=TxtDocumentoM.ClientID %>').value;
            var sVolante = document.getElementById('<%=TxtVolante.ClientID %>').value;
            var sDocOrigenH = document.getElementById('<%=TxtDocumentoH.ClientID %>').value;
            var sContenedor = document.getElementById('<%=TxtContenedor.ClientID %>').value;

            var sCriterio = sManifiesto + sDocOrigenM + sVolante + sDocOrigenH + sContenedor;

            if ((fc_Trim(sCriterio) == "")) {
                alert('Debe indicar al menos un criterio de búsqueda');
                return false;
            }
            else {
                return true;
            }
        }--%>
    </script>

</asp:Content>
<asp:Content ID="Content3" runat="server" contentplaceholderid="ContentCab">
    <span class="texto_titulo" >Tu Sesion Expirara en &nbsp; 
        <span class="texto_titulo" id="seconds"> </span>
        <span>&nbsp;seg.</span>
     </span>
     <asp:Button ID="btn_cerrar" runat="server" Text="cerrar" 
        onclick="btn_cerrar_Click"  Style="display: none;" />
</asp:Content>
