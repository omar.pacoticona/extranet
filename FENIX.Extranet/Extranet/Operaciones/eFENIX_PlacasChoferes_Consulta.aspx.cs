﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using QNET.Web.UI.Controls;
using FENIX.BusinessEntity;
using FENIX.BusinessLogic;
using System.Text;
using FENIX.Common;
using System.Collections.Generic;
using System.Drawing;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.IO;



public partial class Extranet_Operaciones_eFENIX_PlacasChoferes_Consulta : System.Web.UI.Page
{
    #region "Evento Pagina"
    protected void Page_Load(object sender, EventArgs e)
    {

        txtNumAut.Attributes.Add("onkeydown", "javascript:return fc_PressKeyDO();");
        TxtVolante.Attributes.Add("onkeydown", "javascript:return fc_PressKeyDO();");
        TxtDocumentoH.Attributes.Add("onkeydown", "javascript:return fc_PressKeyDO();");
        TxtContenedor.Attributes.Add("onkeydown", "javascript:return fc_PressKeyDO();");
        TxtCliente.Attributes.Add("onkeydown", "javascript:return fc_PressKeyDO();");
        txtNumAut.Attributes.Add("onkeypress", "javascript:return SoloEnteros(event);");
        TxtVolante.Attributes.Add("onkeypress", "javascript:return SoloEnteros(event);");
        txtPlacaAgre.Attributes.Add("onkeypress", "javascript: return AlphaNumericOnly(event);");
        txtIdDespachadorPopUp.Attributes.Add("onkeydown", "javascript:return fc_PressKeyDesp();");
        txtDespachadorPopUp.Attributes.Add("onkeydown", "javascript:return fc_PressKeyDesp();");

        txtDNIAgre.Attributes.Add("onkeydown", "javascript:return fc_PressKeyDNI();");
        txtBrevete.Attributes.Add("onkeydown", "javascript:return fc_PressKeyBrevete();");
        
        //chkSeleccion.Attributes.Add("onclick", "HabilitarUno(this);");

        String tiempo = (Convert.ToInt32(GlobalEntity.Instancia.TiempoDuracionSesion.ToString()) * 1000 * 60).ToString();
        Session["Reset"] = true;
        if (!Page.IsPostBack)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "mensaje", "SessionExpireAlert(" + tiempo + ");", true);
        }
        else
            ScriptManager.RegisterStartupScript(this, GetType(), "mensaje", "myStopFunction();", true);
    }

    protected void ImgBtnBuscar_Click(object sender, ImageClickEventArgs e)
    {
        LlenarGrilla();
    }

    void LlenarGrilla()
    {
        try
        {
            //BE_PlacaChofer objEntidad = new BE_PlacaChofer();
            BE_AutorizacionRetiroList lista = new BE_AutorizacionRetiroList();
            String NumAut = txtNumAut.Text;
            String Volante = TxtVolante.Text;
            String BL = TxtDocumentoH.Text;
            String Cliente = TxtCliente.Text;
            String Contenedor = TxtContenedor.Text;
            String Agencia = GlobalEntity.Instancia.NombreCliente;

            BL_DocumentoOrigen objNegocio = new BL_DocumentoOrigen();
            lista = objNegocio.ListarAutRetiro2(NumAut, Volante, BL, Cliente, Contenedor, Agencia);
            GrvListado.DataSource = lista;
            GrvListado.DataBind();
            //GrvListado.EmptyDataText = Resources.Resource.MsgRolNull;
            if (GrvListado.Rows.Count == 0 || GrvListado.DataSource == null)
            {
                Mensaje("No tiene Placas ni Choferes Registrados");
                GrvListado.Visible = false;
            }

            dnvListado.Visible = true;
            LblTotal.Text = "Total de Registros: " + Convert.ToString(GrvListado.Rows.Count);
            UdpTotales.Update();

            new DataNavigatorParams(lista, GrvListado.Rows.Count);
        }
        catch (Exception e)
        {
            String mensaje = e.ToString();
            Mensaje(mensaje);
        }
    }

    void Mensaje(String SMS)
    {
        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "", "<script>alert('" + SMS + "');</script>", false);
    }

    protected void GrvListado_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        String NumAut = String.Empty;
        String IdAutRet = String.Empty;
        int Tipo = 0;
        if (e.CommandName == "OpenModificar")
        {
            NumAut = GrvListado.DataKeys[Convert.ToInt32(e.CommandArgument)].Values["SNumAutRet"].ToString();
            IdAutRet = GrvListado.DataKeys[Convert.ToInt32(e.CommandArgument)].Values["IIdAutRet"].ToString();

            GlobalEntity.Instancia.IIdAutRet = Convert.ToInt32(IdAutRet);
            BuscarAutAgre(NumAut);
            LlenarPlacasChofer();
            LlenarDespachadores();
            listarRegistrosApp(GlobalEntity.Instancia.IIdAutRet);
            mpAgrePlacaChofer.Show();
        }
    }

        protected void btn_cerrar_Click(object sender, EventArgs e)
        {
            if (GlobalEntity.Instancia.CerrarExtranet == "S")
            {
                BL_Usuario oBL_Usuario = new BL_Usuario();
                oBL_Usuario.GrabaInicioSesion(GlobalEntity.Instancia.Usuario, "0");
                Session.Abandon();
                FormsAuthentication.SignOut();
                Response.Redirect("../../eFENIX_Login.aspx");
                Response.End();
            }
        }
    #endregion
    String AlertaCodigo(String NumAut,String Placa,String TipoDoc,String Doc,String Usuario)
    {
        String CodigoCliente = String.Empty;
        String Rspta = String.Empty;
        String Resultado = String.Empty;
        CodigoCliente = Codigo();
        BL_DocumentoOrigen objNegocio = new BL_DocumentoOrigen();
        Rspta = objNegocio.AlertaCodigo(NumAut, Placa, TipoDoc, Doc, Usuario, CodigoCliente,GlobalEntity.Instancia.IdCliente,GlobalEntity.Instancia.IIdAutRet);
        if (Rspta == "1")
        {
            Resultado = ConfigurationManager.AppSettings["MensajeCodigoOk"].ToString();
        }
        else if(Rspta=="-1")
        {
            Resultado = ConfigurationManager.AppSettings["MensajeCodigoDuplicado"].ToString();
        }
        else
        {
            Resultado = ConfigurationManager.AppSettings["MensajeCodigoNo"].ToString();
        }
        return Resultado;
    }

    String Codigo()
    {
        Random obj = new Random();
        string posibles = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
        int longitud = posibles.Length;
        char letra;
        int longitudnuevacadena = 5;
        string nuevacadena = "";
        for (int i = 0; i < longitudnuevacadena; i++)
        {
            letra = posibles[obj.Next(longitud)];
            nuevacadena += letra.ToString();
        }
        return nuevacadena;
    }

    void BuscarAutAgre(String NumAut)
    {
        if (NumAut == String.Empty)
        {
            Mensaje("Ingrese una N° de Autorizacion de Retiro");
        }
        BL_DocumentoOrigen objNegocio = new BL_DocumentoOrigen();
        BE_AutorizacionRetiro objEntidad = new BE_AutorizacionRetiro();
        objEntidad = objNegocio.ListarAutRetAgre(NumAut);

        txtAutRetAgre.Text = NumAut;
        txtFecha.Text = objEntidad.SFechaVigencia;
        txtSituacion.Text = objEntidad.SSituacion;
        txtEstado.Text = objEntidad.SEstado;
        txtDespachador.Text = objEntidad.SDespachador;


    }

    void LimpiarPopAgregar()
    {
        txtAutRetAgre.Text = String.Empty;
        txtFecha.Text = String.Empty;
        txtSituacion.Text = String.Empty;
        txtEstado.Text = String.Empty;
        txtDespachador.Text = String.Empty;
        txtPlacaAgre.Text = String.Empty;
        txtNombreChofer.Text = String.Empty;
        txtDNIAgre.Text = String.Empty;
        txtBrevete.Text = String.Empty;
    }

    void LlenarPlacasChofer()
    {
        try
        {
            BL_DocumentoOrigen oBLDua = new BL_DocumentoOrigen();
            BE_PlacaChoferList objEntidadLista = new BE_PlacaChoferList();
            objEntidadLista = oBLDua.ListarPlacaChofer(GlobalEntity.Instancia.IIdAutRet);
            gvListadoAgre.DataSource = objEntidadLista;
            gvListadoAgre.DataBind();
            //gvListadoAgre.EmptyDataText = Resources.Resource.MsgRolNull;
            if (gvListadoAgre.DataSource == null || gvListadoAgre.Rows.Count == 0)
            {
                //Mensaje("No tiene Placas ni Choferes Registrados");
                gvListadoAgre.Visible = false;
            }
            else
            {
                gvListadoAgre.Visible = true;
            }
        }
        catch (Exception e)
        {
            new ResultadoInterfase(ResultadoInterfase.TipoResultado.Error, e);
        }
    }

    protected void gvListadoAgre_OnRowDataBound(object sender, GridViewRowEventArgs e)
    {

        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            DataKey dataKey = this.gvListadoAgre.DataKeys[e.Row.RowIndex];
            BE_PlacaChofer oitem = (e.Row.DataItem as BE_PlacaChofer);

            if (oitem.SPlaca == null)
            {
                e.Row.Visible = false;

                return;
            }
            else
            {
                CheckBox chkSeleccion = (CheckBox)e.Row.FindControl("chkSeleccionar");
                chkSeleccion.Attributes.Add("onclick", "HabilitarUno(this);");
                e.Row.Style["cursor"] = "pointer";
                //e.Row.Attributes["onclick"] = String.Format("javascript: fc_SeleccionaFilaSimple(this)");
            }
        }
    }

    protected void btnQuitar_Click(object sender, EventArgs e)
    {
        Int32 vl_sCodigo = 0;
        String Usuario = String.Empty;
        String NombrePc = String.Empty;
        Usuario = GlobalEntity.Instancia.Usuario;
        NombrePc = "Extranet";
        String Ticket = String.Empty;
        int cont = 0;

        foreach (GridViewRow GrvRow in gvListadoAgre.Rows)
        {
            if ((GrvRow.FindControl("chkSeleccionar") as CheckBox).Checked)
            {
                //vl_sCodigo = vl_sCodigo + gvListado.DataKeys[GrvRow.RowIndex].Values["iIdRegistro"].ToString() + ",";
                vl_sCodigo = Convert.ToInt32(gvListadoAgre.DataKeys[GrvRow.RowIndex].Values["IIdRegistro"].ToString());
                Ticket = gvListadoAgre.DataKeys[GrvRow.RowIndex].Values["sTicket"].ToString();
                cont = cont + 1;
            }
        }
        if (cont == 0)
        {
            Mensaje("Debe Seleccionar un Registro");
            return;
        }

        if (!String.IsNullOrEmpty(Ticket))
        {
            Mensaje("No puede Quitar una Placa con Movimiento de Balanza");
            return;
        }

        if (vl_sCodigo.ToString().Trim().Length != 0 || vl_sCodigo != 0)
        {
            BL_DocumentoOrigen oBL_Dua = new BL_DocumentoOrigen();
            oBL_Dua.EliminarPlacaChofer(vl_sCodigo, Usuario, NombrePc);
            LlenarPlacasChofer();
        }
        else
        {
            Mensaje("Seleccione registro(s) a quitar");
            return;
        }
    }
    
    protected void btnAgregar_Click(object sender, EventArgs e)
    {
        int accion = 0;// 1 busqueda por Brevete || 2 busqueda por DNI|| 0 NINGUNO
        /*Validar si el DNI existe  en la BD*/
        if (ChkEmpadronar.Checked == false)
        {
            buscarChofer();
            accion = 0;
        }
        else
        {
            accion = 1;
        }

        /*---*/
        BE_PlacaChofer objEntidad = new BE_PlacaChofer();
        String Respuesta = String.Empty;
        String DNI = String.Empty;
        String Brevete = String.Empty;
        String NomChofer = String.Empty;

        if (String.IsNullOrEmpty(txtDNIAgre.Text))
        {
            Respuesta = "Ingrese DNI";
            Mensaje(Respuesta);
            return;
        }
        if (String.IsNullOrEmpty(txtNombreChofer.Text))
        {
            Respuesta = "Ingrese Chofer";
            Mensaje(Respuesta);
            return;
        }

        objEntidad.SPlaca = txtPlacaAgre.Text;
        objEntidad.SChofer = txtNombreChofer.Text;
        objEntidad.SDoc = txtDNIAgre.Text;
        objEntidad.sBrevete = txtBrevete.Text;
        objEntidad.SUsuario = GlobalEntity.Instancia.Usuario;
        objEntidad.SPC = "Extranet";
        objEntidad.IIdAutRet = GlobalEntity.Instancia.IIdAutRet;

        Int32 iCodigo = 0;
        String vl_sMessage = String.Empty;
        int tipo = 1;//busqueda x DNI Y BREVETE ||1 InsertarPlacaChofer
        
        BL_DocumentoOrigen objNegocio = new BL_DocumentoOrigen();

        String[] sResultado = objNegocio.InsertarPlacaChofer(objEntidad,tipo,accion).Split('|');

        for (int i = 0; i < sResultado.Length; i++)
        {
            if (i == 0)
            {
                iCodigo = Convert.ToInt32(sResultado[i]);
            }
            else if (i == 1)
            {
                vl_sMessage += sResultado[i];
            }

        }

        if (iCodigo < 1)
        {

            Mensaje(vl_sMessage);
        }
        else
        {
            txtPlacaAgre.Text = String.Empty;
            txtNombreChofer.Text = String.Empty;
            txtDNIAgre.Text = String.Empty;
            txtBrevete.Text = String.Empty;
            LlenarPlacasChofer();
            gvListadoAgre.Visible = true;
        }
    }

    protected void btnAceptar_Click(object sender, EventArgs e)
    {
        Int32 vl_sCodigo = 0;
        String Despachador = String.Empty;
        String DniDespachador = String.Empty;

        foreach (GridViewRow GrvRow in grvListadoDespachador.Rows)
        {
            if ((GrvRow.FindControl("ChkSeleccionarDespachador") as CheckBox).Checked)
            {
                //vl_sCodigo = vl_sCodigo + gvListado.DataKeys[GrvRow.RowIndex].Values["iIdRegistro"].ToString() + ",";
                vl_sCodigo = Convert.ToInt32(grvListadoDespachador.DataKeys[GrvRow.RowIndex].Values["IIdDespachador"].ToString());
                Despachador = grvListadoDespachador.DataKeys[GrvRow.RowIndex].Values["SDespachador"].ToString();
                DniDespachador = grvListadoDespachador.DataKeys[GrvRow.RowIndex].Values["SDniDespachador"].ToString();
            }
        }
        if (vl_sCodigo != 0)
        {
            GlobalEntity.Instancia.sIdDespachador = vl_sCodigo.ToString();
            txtDniDesp.Text = DniDespachador;
            txtDespNom.Text = Despachador;
            txtIdDespachadorPopUp.Text = "";
            txtDespachadorPopUp.Text = "";
            mpDespachador.Hide();
        }

        else
        {
            Mensaje("Seleccione registro(s) a quitar");
            return;
        }
    }

    protected void imgbBuscar_Click(object sender, ImageClickEventArgs e)
    {
        BE_AutorizacionRetiroList objEntidadList = new BE_AutorizacionRetiroList();
        String Resp = "Seleccione un criterio de busqueda";

        String Id = txtIdDespachadorPopUp.Text;
        String desp = txtDespachadorPopUp.Text;

        //if (Id == String.Empty)
        //{
        //    if (desp == String.Empty)
        //    {
        //        Mensaje(Resp);
        //        return;
        //    }
        //}
        //else if (desp == String.Empty)
        //{
        //    if (Id == String.Empty)
        //    {
        //        Mensaje(Resp);
        //        return;
        //    }
        //}

        BL_DocumentoOrigen objNegocio = new BL_DocumentoOrigen();

        objEntidadList = objNegocio.ListarDespachador(Id, desp, GlobalEntity.Instancia.IdCliente);

        if (objEntidadList.Count == 0 || objEntidadList == null)
        {
            objEntidadList = new BE_AutorizacionRetiroList();
            objEntidadList.Add(new BE_AutorizacionRetiro());
        }

        ViewState["oBE_ServicioList"] = objEntidadList;
        grvListadoDespachador.DataSource = objEntidadList;
        grvListadoDespachador.DataBind();
    }

    protected void grvListadoDespachador_RowDataBound(object sender, GridViewRowEventArgs e)
    {

        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            DataKey dataKey = this.grvListadoDespachador.DataKeys[e.Row.RowIndex];
            BE_AutorizacionRetiro oitem = (e.Row.DataItem as BE_AutorizacionRetiro);

            if (String.IsNullOrEmpty(oitem.SDespachador))
            {
                e.Row.Visible = false;

                return;
            }
            else
            {
                CheckBox chkSeleccion = (CheckBox)e.Row.FindControl("ChkSeleccionarDespachador");
                chkSeleccion.Attributes.Add("onclick", "HabilitarUno(this);");
                e.Row.Style["cursor"] = "pointer";
                //e.Row.Attributes["onclick"] = String.Format("javascript: fc_SeleccionaFilaSimple(this)");
            }
        }
    }

    void LimpiarGrillaDespachador()
    {
        BE_AutorizacionRetiroList objEntidadLista = new BE_AutorizacionRetiroList();
        BE_AutorizacionRetiro objEntidad = new BE_AutorizacionRetiro();
        objEntidadLista.Add(objEntidad);
        grvListadoDespachador.DataSource = objEntidadLista;
        grvListadoDespachador.DataBind();
    }

    void LlenarDespachadores()
    {
        try
        {
            BL_DocumentoOrigen oBLDua = new BL_DocumentoOrigen();
            BE_AutorizacionRetiroList objEntidadLista = new BE_AutorizacionRetiroList();
            objEntidadLista = oBLDua.ListarAutDespachadores(GlobalEntity.Instancia.IIdAutRet);
            gvDespachadores.DataSource = objEntidadLista;
            gvDespachadores.DataBind();
            //gvDespachadores.EmptyDataText = Resources.Resource.MsgRolNull;
            if (gvDespachadores.DataSource == null || gvDespachadores.Rows.Count == 0)
            {
                //Mensaje("No tiene Placas ni Choferes Registrados");
                gvDespachadores.Visible = false;
            }
            else
            {
                gvDespachadores.Visible = true;
            }
        }
        catch (Exception e)
        {
            new ResultadoInterfase(ResultadoInterfase.TipoResultado.Error, e);
        }
    }

    protected void gvDespachadores_OnRowDataBound(object sender, GridViewRowEventArgs e)
    {

        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            DataKey dataKey = this.gvDespachadores.DataKeys[e.Row.RowIndex];
            BE_AutorizacionRetiro oitem = (e.Row.DataItem as BE_AutorizacionRetiro);

            if (String.IsNullOrEmpty(oitem.SDespachador))
            {
                e.Row.Visible = false;

                return;
            }
            else
            {
                CheckBox chkSeleccion = (CheckBox)e.Row.FindControl("chkSeleccionarDesp");
                chkSeleccion.Attributes.Add("onclick", "HabilitarUno(this);");
                e.Row.Style["cursor"] = "pointer";
                //e.Row.Attributes["onclick"] = String.Format("javascript: fc_SeleccionaFilaSimple(this)");
            }
        }
    }


    protected void btnAgregarDesp_Click(object sender, EventArgs e)
    {
        BE_AutorizacionRetiro objEntidad = new BE_AutorizacionRetiro();
        String Respuesta = String.Empty;
        String DNI = String.Empty;
        String NomDesp = String.Empty;
        if (String.IsNullOrEmpty(txtDniDesp.Text))
        {
            Respuesta = "Ingrese DNI";
            Mensaje(Respuesta);
            return;
        }

        if (String.IsNullOrEmpty(txtDespNom.Text))
        {
            Respuesta = "Ingrese Nombre del Despachador";
            Mensaje(Respuesta);
            return;
        }

        objEntidad.SDespachador = txtDespNom.Text;
        objEntidad.IIdDespachador = Convert.ToInt32(GlobalEntity.Instancia.sIdDespachador);
        objEntidad.SDniDespachador = txtDniDesp.Text;
        objEntidad.SUsuario = GlobalEntity.Instancia.Usuario;
        objEntidad.SPC = "Extranet";
        objEntidad.IIdAutRet = GlobalEntity.Instancia.IIdAutRet;

        Int32 iCodigo = 0;
        String vl_sMessage = String.Empty;

        BL_DocumentoOrigen objNegocio = new BL_DocumentoOrigen();

        String[] sResultado = objNegocio.InsertarDespachadorAut(objEntidad).Split('|');

        for (int i = 0; i < sResultado.Length; i++)
        {
            if (i == 0)
            {
                iCodigo = Convert.ToInt32(sResultado[i]);
            }
            else if (i == 1)
            {
                vl_sMessage += sResultado[i];
            }

        }

        if (iCodigo < 1)
        {

            Mensaje(vl_sMessage);
        }
        else
        {
            txtDniDesp.Text = String.Empty;
            txtDespNom.Text = String.Empty;
            LlenarDespachadores();
            grvListadoDespachador.Visible = true;
        }
    }

    protected void btnQuitarDesp_Click(object sender, EventArgs e)
    {
        Int32 vl_sCodigo = 0;
        String Usuario = String.Empty;
        String NombrePc = String.Empty;
        Usuario = GlobalEntity.Instancia.Usuario;
        NombrePc = "Extranet";
        int cont = 0;

        foreach (GridViewRow GrvRow in gvDespachadores.Rows)
        {
            if ((GrvRow.FindControl("chkSeleccionarDesp") as CheckBox).Checked)
            {
                //vl_sCodigo = vl_sCodigo + gvListado.DataKeys[GrvRow.RowIndex].Values["iIdRegistro"].ToString() + ",";
                vl_sCodigo = Convert.ToInt32(gvDespachadores.DataKeys[GrvRow.RowIndex].Values["IIdRegistro"].ToString());
                cont = cont + 1;
            }
        }
        if (cont == 0)
        {
            Mensaje("Debe Seleccionar un Registro");
            return;
        }
        //if (cont > 1)
        //{
        //    Mensaje("Debe Seleccionar solo un registro a la vez");
        //    return;
        //}
        if (vl_sCodigo.ToString().Trim().Length != 0 || vl_sCodigo != 0)
        {
            BL_DocumentoOrigen oBL_Despachador = new BL_DocumentoOrigen();
            oBL_Despachador.EliminarDespachador2(vl_sCodigo, Usuario, NombrePc);
            LlenarDespachadores();
        }
        else
        {
            Mensaje("Seleccione registro(s) a quitar");
            return;
        }
    }

    protected void btnCerrarPopUp_Click(object sender, EventArgs e)
    {
        LimpiarGrillaDespachador();
        mpDespachador.Hide();
    }

    //--------------------consultar x bevete y dni a la bd
    protected void btnImgBrevete_Click(object sender, EventArgs e)
    {
        String Respuesta = String.Empty;
        Int32 iCodigo = 0;
        String vl_sNombre = String.Empty;
        String vl_svalueDNI = String.Empty;
        BE_PlacaChofer objEntidad = new BE_PlacaChofer();


        if (String.IsNullOrEmpty(txtBrevete.Text))
        {
            Respuesta = "Ingrese Brevete";
            Mensaje(Respuesta);
            return;
        }

        objEntidad.sBrevete = txtBrevete.Text;
        objEntidad.SChofer = "";
        int tipo = 2;//busqueda x DNI Y BREVETE
        int accion = 1;// 1 busqueda por Brevete || 2 busqueda por DNI

        BL_DocumentoOrigen objNegocio = new BL_DocumentoOrigen();

        String[] sResultado = objNegocio.InsertarPlacaChofer(objEntidad, tipo, accion).Split('|');
        for (int i = 0; i < sResultado.Length; i++)
        {
            if (i == 0)
            {
                iCodigo = Convert.ToInt32(sResultado[i]);
            }
            else if (i == 1)
            {
                vl_sNombre = sResultado[i];
            }
            else if (i == 2)
            {
                vl_svalueDNI = sResultado[i];
            }

        }

        if (iCodigo == 0)
        {
            txtDNIAgre.Text = "";
            txtBrevete.Text = "";
            txtNombreChofer.Text = "";
            string msj = vl_sNombre;
            Mensaje(vl_sNombre);
            return;
        }

        txtDNIAgre.Text = vl_svalueDNI;
        txtNombreChofer.Text = vl_sNombre;

    }
    protected void btnImgChoferDNI_Click(object sender, EventArgs e)
    {
        buscarChofer();
    }

    void buscarChofer()
    {
        String Respuesta = String.Empty;
        Int32 iCodigo = 0;
        String vl_sNombre = String.Empty;
        String vl_svalueBrebete = String.Empty;

        BE_PlacaChofer objEntidad = new BE_PlacaChofer();

        if (String.IsNullOrEmpty(txtDNIAgre.Text))
        {
            Respuesta = "Ingrese DNI";
            Mensaje(Respuesta);
            return;
        }

        objEntidad.SDoc = txtDNIAgre.Text;
        objEntidad.SChofer = "";
        int _tipo = 2;//busqueda x DNI Y BREVETE
        int _accion = 2;// 1 busqueda por Brevete || 2 busqueda por DNI

        BL_DocumentoOrigen objNegocio = new BL_DocumentoOrigen();

        String[] sResultado = objNegocio.InsertarPlacaChofer(objEntidad, _tipo, _accion).Split('|');


        for (int i = 0; i < sResultado.Length; i++)
        {
            if (i == 0)
            {
                iCodigo = Convert.ToInt32(sResultado[i]);
            }
            else if (i == 1)
            {
                vl_sNombre = sResultado[i];
            }
            else if (i == 2)
            {
                vl_svalueBrebete = sResultado[i];
            }

        }


        if (iCodigo == 0)
        {
            txtDNIAgre.Text = "";
            txtBrevete.Text = "";
            txtNombreChofer.Text = "";
            Mensaje(vl_sNombre);
            return;
        }


        txtBrevete.Text = vl_svalueBrebete;
        txtNombreChofer.Text = vl_sNombre;
    }
    protected void ChkEmpadronar_OnCheckedChanged(object sender, EventArgs e)
    {
        //CheckBox cb = new CheckBox();
        if (ChkEmpadronar.Checked == true)
        {
            txtDNIAgre.Text = "";
            txtBrevete.Text = "";
            txtNombreChofer.Text = "";
            txtNombreChofer.Enabled = true;
        }
        else
        {
            txtDNIAgre.Text = "";
            txtBrevete.Text = "";
            txtNombreChofer.Text = "";
            txtNombreChofer.Enabled = false;
        }

    }
    void listarRegistrosApp(int id)
    {
        BL_DocumentoOrigen objNegocio = new BL_DocumentoOrigen();
        BE_AutorizacionRetiro oBE_AutorizacionRetiro = new BE_AutorizacionRetiro();
        BE_PlacaChoferList oBE_PlacaChoferList = new BE_PlacaChoferList();

        oBE_PlacaChoferList = objNegocio.ListarPlacaChoferApp(id, 2);


        gvListadoR.DataSource = oBE_PlacaChoferList;
        gvListadoR.DataBind();
    }


}