﻿using System.Web;
/// <summary>
/// Clase que va mantener los datos de la sesion del usuario
/// </summary>
public class GlobalEntity
{
    #region " Propiedades "
    private string _Nombre;
    private string _Usuario;
    private string _Ejecutivos;
    private int _SoloLectura;
    private string _NombrePc;
    private string _DireccionIP;
    private int _IdLineaNegocio;
    private int _IdGrupo;
    private int _IdCliente;
    private string _NombreCliente;
    private int _TiempoSesion;
    private int _MaximoSolicitudVolantes;
    private string _iCerrar;
    private int _iIdAutRet;
    private string _sPresencia;
    private string _sAccion;
    private string _sIdDespachador;

    public int iIdGrupo
    {
        get { return _IdGrupo; }
        set { _IdGrupo = value; }
    }

    public string sIdDespachador
    {
        get { return _sIdDespachador; }
        set { _sIdDespachador = value; }
    }

    public int IdLineaNegocio
    {
        get { return _IdLineaNegocio; }
        set { _IdLineaNegocio = value; }
    }

    public string DireccionIP
    {
        get { return _DireccionIP; }
        set { _DireccionIP = value; }
    }

    public string sAccion
    {
        get { return _sAccion; }
        set { _sAccion = value; }
    }
    public string NombrePc
    {
        get { return _NombrePc; }
        set { _NombrePc = value; }
    }
    public string Nombre
    {
        get { return _Nombre; }
        set { _Nombre = value; }
    }
    public string Usuario
    {
        get { return _Usuario; }
        set { _Usuario = value; }
    }
    public string Ejecutivos
    {
        get { return _Ejecutivos; }
        set { _Ejecutivos = value; }
    }
    public int SoloLectura
    {
        get { return _SoloLectura; }
        set { _SoloLectura = value; }
    }
    public int IdCliente
    {
        get { return _IdCliente; }
        set { _IdCliente = value; }
    }

    public string NombreCliente
    {
        get { return _NombreCliente; }
        set { _NombreCliente = value; }
    }

    public int TiempoDuracionSesion
    {
        get { return _TiempoSesion; }
        set { _TiempoSesion = value; }
    }

    public int MaximoSolicitudVolantes
    {
        get { return _MaximoSolicitudVolantes; }
        set { _MaximoSolicitudVolantes = value; }
    }

    public string CerrarExtranet
    {
        get { return _iCerrar; }
        set { _iCerrar = value; }
    }

    #endregion

    public static GlobalEntity Instancia
    {
        get
        {
            if (HttpContext.Current.Session["__Global__"] == null)
                HttpContext.Current.Session["__Global__"] = new GlobalEntity();
            GlobalEntity obj_Global = (HttpContext.Current.Session["__Global__"] as GlobalEntity);
            return obj_Global;
        }
    }
    public static void SignOut()
    {
        HttpContext.Current.Session["__Global__"]=null;
    }
    /// <summary>
    /// retorna el codigo de ejecutivo si solo pertenece a un ejecutivo sino lo retorna separado por comas
    /// </summary>
    public static string MisEjecutivos(string pEjecutivos)
    {
        string Codigos = string.Empty;
        foreach (string s in pEjecutivos.Split('|'))
        {
            if (!string.IsNullOrEmpty(s))
            {
                Codigos = string.Format("{0},{1}", Codigos, s);
            }
        }
        if (Codigos.Length > 0)
            Codigos = Codigos.Substring(1);
        return Codigos;
    }
    public static bool EsNulo
    {
        get
        {
            return (HttpContext.Current.Session["__Global__"]==null);
        }
    }

    public int IIdAutRet
    {
        get
        {
            return _iIdAutRet;
        }

        set
        {
            _iIdAutRet = value;
        }
    }

    public string SPresencia
    {
        get
        {
            return _sPresencia;
        }

        set
        {
            _sPresencia = value;
        }
    }
}


