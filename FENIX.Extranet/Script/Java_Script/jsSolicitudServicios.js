﻿$(document).ready(function () {
    //$(document).keydown(function (event) {
    //    if (event.ctrlKey == true && (event.which == '118' || event.which == '86')) {
    //        alert("Esta función está inhabilitada.\n\nPerdone las molestias");
    //        event.preventDefault();
    //    }
    //});
    LoadAdditionalData();

    //document.getElementById("ctl00_CntMaster_btnTab2P").addEventListener("click", function (event) {
    //    event.preventDefault()
    //});


    //document.getElementById("ctl00_CntMaster_btnTab1").addEventListener("click", function (event) {
    //    event.preventDefault()
    //});
});


var lstCargaSeleccionada = [];
var lstCargaConfirmada = [];
var lstServiciosConfirmados = [];
var dateToday = new Date();
dateToday.setDate(dateToday.getDate() + 1);

$(function () {
    $('#fsMotivo').hide();
    $('#fsTipoCarga').hide();
    $('#fsDev').hide();
    $('#ctl00_CntMaster_txtDetalleServicio').hide();
    $('#ctl00_CntMaster_lblDAM').hide();




    $('#ctl00_CntMaster_dplServicio').change(function () {
        var selectedVal = $('#ctl00_CntMaster_dplServicio').val();
        /*
         143 PREVIO
         144 AFORO
         145 SENASA
         158 TRASEGADO
         */
        if (selectedVal == -1) {
            $('#fsMotivo').hide();
            $('#fsTipoCarga').hide();
            $('#fsDev').hide();
            $('#ctl00_CntMaster_lblDAM').hide();
            $('#ctl00_CntMaster_txtDetalleServicio').hide();
        }

        if (selectedVal == 143) {
            $('#fsMotivo').hide();
            $('#fsTipoCarga').hide();
            $('#fsDev').hide();
            $('#ctl00_CntMaster_lblDAM').hide();
            $('#ctl00_CntMaster_txtDetalleServicio').hide();
        }
        else if (selectedVal == 144) {
            $('#fsMotivo').hide();
            $('#fsTipoCarga').hide();
            $('#fsDev').hide();
            $('#ctl00_CntMaster_lblDAM').show();
            $('#ctl00_CntMaster_txtDetalleServicio').show();
        }
        else if (selectedVal == 145) {
            $('#fsMotivo').show();
            $('#fsTipoCarga').hide();
            $('#fsDev').hide();
            $('#ctl00_CntMaster_lblDAM').hide();
            $('#ctl00_CntMaster_txtDetalleServicio').hide();
        }
        else if (selectedVal == 158) {
            $('#fsMotivo').hide();
            $('#fsTipoCarga').show();
            $('#fsDev').show();
            $('#ctl00_CntMaster_lblDAM').hide();
            $('#ctl00_CntMaster_txtDetalleServicio').hide();

        }
    });



});

$.datepicker.regional['es'] = {
    closeText: 'Cerrar',
    prevText: '< Ant',
    nextText: 'Sig >',
    currentText: 'Hoy',
    minDate: dateToday,
    monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
    monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
    dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
    dayNamesShort: ['Dom', 'Lun', 'Mar', 'Mié', 'Juv', 'Vie', 'Sáb'],
    dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sá'],
    weekHeader: 'Sm',
    dateFormat: 'dd/mm/yy',
    firstDay: 1,
    isRTL: false,
    showMonthAfterYear: false,
    yearSuffix: '',
    beforeShowDay: function (date) {
        var day = date.getDay();
        return [(day != 0), ''];
    }
};

$.datepicker.setDefaults($.datepicker.regional['es']);
$(function () {
    $("input[id$='datepicker']").datepicker();
});


function LoadAdditionalData() {
    var selectedVal = $('#ctl00_CntMaster_dplServicio').val();
    /*
     143 PREVIO
     144 AFORO
     145 SENASA
     158 TRASEGADO
     */
    if (selectedVal == -1) {
        $('#fsMotivo').hide();
        $('#fsTipoCarga').hide();
        $('#fsDev').hide();
        $('#ctl00_CntMaster_lblDAM').hide();
        $('#ctl00_CntMaster_txtDetalleServicio').hide();
    }

    if (selectedVal == 143) {
        $('#fsMotivo').hide();
        $('#fsTipoCarga').hide();
        $('#fsDev').hide();
        $('#ctl00_CntMaster_lblDAM').hide();
        $('#ctl00_CntMaster_txtDetalleServicio').hide();
    }
    else if (selectedVal == 144) {
        $('#fsMotivo').hide();
        $('#fsTipoCarga').hide();
        $('#fsDev').hide();
        $('#ctl00_CntMaster_lblDAM').show();
        $('#ctl00_CntMaster_txtDetalleServicio').show();
    }
    else if (selectedVal == 145) {
        $('#fsMotivo').show();
        $('#fsTipoCarga').hide();
        $('#fsDev').hide();
        $('#ctl00_CntMaster_lblDAM').hide();
        $('#ctl00_CntMaster_txtDetalleServicio').hide();
    }
    else if (selectedVal == 158) {
        $('#fsMotivo').hide();
        $('#fsTipoCarga').show();
        $('#fsDev').show();
        $('#ctl00_CntMaster_lblDAM').hide();
        $('#ctl00_CntMaster_txtDetalleServicio').hide();

    }
}

function habiliteTabs() {
    //$("#tabs-container").tabs();
    //$("#tabs-container").tabs("option", "disabled", [1, 2]);
    if (ValidarNextPage1()) {
        $(".btnNext").click(function () {
            $("#tabs-container").tabs("option", "disabled", [0, 2]);
            $("#tabs-container").tabs("option", "active", $("#tabs-container").tabs('option', 'active') + 1);
            $("#ctl00_CntMaster_btnNextTab1").click();
            return false;
        });
    }

    $(".btnNext1").click(function () {
        $("#tabs-container").tabs("option", "disabled", [0, 1]);
        $("#tabs-container").tabs("option", "active", $("#tabs-container").tabs('option', 'active') + 1);
        return false;
    });

    $(".btnPrev").click(function () {
        $("#tabs-container").tabs("option", "disabled", [1, 2]);
        $("#tabs-container").tabs("option", "active", $("#tabs-container").tabs('option', 'active') - 1);
        return false;
    });
    $(".btnPrev1").click(function () {
        $("#tabs-container").tabs("option", "disabled", [0, 2]);
        $("#tabs-container").tabs("option", "active", $("#tabs-container").tabs('option', 'active') - 1);
        return false;
    });
    $(".btnConfirm").click(function () {
        alert("Solicitud de servicio registrada. Imprima el documento.")
        return false;
    });
}


$(function () {
    $("#tabs-container").tabs();
    $("#tabs-container").tabs("option", "disabled", [1, 2]);



    $(".btnNext").click(function () {
        if (ValidarNextPage1()) {
            $("#tabs-container").tabs("option", "disabled", [0, 2]);
            $("#tabs-container").tabs("option", "active", $("#tabs-container").tabs('option', 'active') + 1);
            //  $("#ctl00_CntMaster_btnNextTab1").click();
            return false;
        }
    });


    $(".btnNext1").click(function () {
        $("#tabs-container").tabs("option", "disabled", [0, 1]);
        $("#tabs-container").tabs("option", "active", $("#tabs-container").tabs('option', 'active') + 1);
        return false;
    });

    $(".btnPrev").click(function () {
        $("#tabs-container").tabs("option", "disabled", [1, 2]);
        $("#tabs-container").tabs("option", "active", $("#tabs-container").tabs('option', 'active') - 1);
        return false;
    });
    $(".btnPrev1").click(function () {
        $("#tabs-container").tabs("option", "disabled", [0, 2]);
        $("#tabs-container").tabs("option", "active", $("#tabs-container").tabs('option', 'active') - 1);
        return false;
    });
    $(".btnConfirm").click(function () {
        alert("Solicitud de servicio registrada. Sirvase imprimir la solicitud para su respectiva atención")
        return false;
    });

});

//var chkVent = document.getElementById("ctl00_CntMaster_chkVentilacion");
//var chkFumig = document.getElementById("ctl00_CntMaster_chkFumigacion");
//var chkInspec = document.getElementById("ctl00_CntMaster_chkInspeccion");

function ckChange() {
    var chkVent = document.getElementById("ctl00_CntMaster_chkVentilacion");
    var chkFumig = document.getElementById("ctl00_CntMaster_chkFumigacion");
    var chkInspec = document.getElementById("ctl00_CntMaster_chkInspeccion");


    if (chkVent.checked) {
        chkFumig.disabled = true;
        chkVent.disabled = false;
    }
    else if (chkFumig.checked) {

        chkVent.disabled = true;
        chkFumig.disabled = false;
    }
    else {
        chkVent.disabled = false;
        chkFumig.disabled = false;
    }
}


function isNumberKey(evt) {
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;
    return true;
}

function GoNextTab1() {
    $("#tabs-container").tabs("select", "tabs-2");
    $("#tabs-container").tabs("option", "disabled", [0, 2]);
}
function GoNextTab2() {
    $("#tabs-container").tabs("select", "tabs-3");
    $("#tabs-container").tabs("option", "disabled", [0, 1]);
}

function ValidarFiltro() {
    //var sDoc = document.getElementById('<%=TxtDocumento.ClientID %>').value;

    var e = document.getElementById("ctl00_CntMaster_dplTipoOperacion");
    var sTipoOperacion = e.options[e.selectedIndex].value;

    var sDoc = document.getElementById("ctl00_CntMaster_TxtDocumentoM").value;
    var sVol = document.getElementById("ctl00_CntMaster_TxtVolante").value;

    var sCriterio = sDoc + sVol;

    if ((fc_Trim(sTipoOperacion) == "-1")) {

        alert("Seleccione Operacion")
        return false;
    }
    else if ((fc_Trim(sCriterio) == "")) {
        alert(msgDatosObligatorios);
        return false;
    }
    else {
        return true;
    }
}

function ValidarNextPage1() {


    if ($("#ctl00_CntMaster_GrvListado >tbody >tr >td:first-child > input[type=checkbox]:checked").length <= 0) {
        alert("Debe seleccionar elementos del listado");
        return false;
    }

    var e = document.getElementById("ctl00_CntMaster_dplServicio");
    var sServicio = e.options[e.selectedIndex].value;
    var eDate = $("#ctl00_CntMaster_txtP_datepicker").val();

    if (fc_Trim(eDate) == "") {
        alert("Ingrese fecha de programación del servicio")

        $("#ctl00_CntMaster_txtP_datepicker").val($("#ctl00_CntMaster_txtP_datepicker").val());

        return false;
    }

    if ((fc_Trim(sServicio) == "-1")) {
        alert("Seleccione Servicio que desea Solicitar")
        return false;
    }

    if ((fc_Trim(sServicio) == "144")) {

        var sDAM = $("#ctl00_CntMaster_txtDetalleServicio").val();

        if ((fc_Trim(sDAM) == "")) {
            alert("Movilización de Aforo: Debe ingresar Nro. DAM");
            $('#ctl00_CntMaster_lblDAM').show();
            $('#ctl00_CntMaster_txtDetalleServicio').show();
            return false;
        }

    }


    else {
        var selectedDate = $("#ctl00_CntMaster_txtP_datepicker").val();
        $('#ctl00_CntMaster_datepickerSeleccionado').val(selectedDate);

        var tableControl = document.getElementById('ctl00_CntMaster_GrvListado');
        var selectedDO;
        var selectedVO;
        var selectedCO;
        var selectedIdServicio;
        var selectedTxtServicio;

        var lstDOSelected = [];
        var lstCOSelected = [];

        var DOduplicated = [];
        var COduplicated = [];

        $('input:checkbox:checked', tableControl).each(function () {
            selectedDO = ($(this).closest('tr').find('td:eq(1)').text());
            selectedVO = ($(this).closest('tr').find('td:eq(2)').text());
            selectedCO = ($(this).closest('tr').find('td:eq(7)').text());
            selectedIdServicio = $("#ctl00_CntMaster_dplServicio").val();
            selectedTxtServicio = $("#ctl00_CntMaster_dplServicio option:selected").text();

            lstDOSelected.push(selectedDO);
            lstCOSelected.push(selectedCO);
        }).get();

        $.each(lstDOSelected, function (key, value) {
            if ($.inArray(value, DOduplicated) == -1) {
                DOduplicated.push(value);
            }
        });

        $.each(lstCOSelected, function (key, value) {
            if ($.inArray(value, COduplicated) == -1) {
                COduplicated.push(value);
            }
        });

        if (DOduplicated.length > 1) {
            alert("Debe seleccionar carga de un solo documento");
            return false;
        }

        if (COduplicated.length > 1) {
            alert("La condición de la carga seleccionada debe ser la misma");
            return false;
        }

        $("#ctl00_CntMaster_dplServicioSeleccionado").append(new Option(selectedTxtServicio, selectedIdServicio));
        $("#ctl00_CntMaster_dplServicioSeleccionado").prop("disabled", true);
        $('#ctl00_CntMaster_txtDOSeleccionado').val(selectedDO);
        $('#ctl00_CntMaster_txtVOSeleccionado').val(selectedVO);


        return true;
    }

}

function ValidarServicioAdicional() {

    var checksConfirmed = $("#tblServiciosAdicionales >tbody >tr >td >div >span:first-child > input[type=checkbox]:checked").length;

    if (checksConfirmed <= 0) {
        alert("Debe seleccionar al menos un servicio operativo");
    }
    else {
        var tableControl = document.getElementById('ctl00_CntMaster_GrvListado');
        var confirmedDate;
        var selectedDO2;
        var selectedVO2;
        var selectedIdServicio2;
        var selectedTxtServicio2;

        confirmedDate = $("#ctl00_CntMaster_datepickerSeleccionado").val();
        selectedIdServicio2 = $("#ctl00_CntMaster_dplServicioSeleccionado").val();
        selectedTxtServicio2 = $("#ctl00_CntMaster_dplServicioSeleccionado option:selected").text();
        selectedDO2 = $('#ctl00_CntMaster_txtDOSeleccionado').val();
        selectedVO2 = $('#ctl00_CntMaster_txtVOSeleccionado').val();

        $('#ctl00_CntMaster_datepickerConfirmado').val(confirmedDate);

        $("#ctl00_CntMaster_dplServicioConfirmado").append(new Option(selectedTxtServicio2, selectedIdServicio2));
        $("#ctl00_CntMaster_dplServicioConfirmado").prop("disabled", true);
        $('#ctl00_CntMaster_txtDOConfirmado').val(selectedDO2);
        $('#ctl00_CntMaster_txtVolanteConfirmado').val(selectedVO2);
    }
}


function ListarCargaConfirmada() {

    var confirmedTableControl = document.getElementById('tblCargaSeleccionada');
    var confirmedServicesTableControl = document.getElementById('tblServiciosAdicionales');

    var sIdDocOriDetC;
    var sCargaC;
    var sTamanoC;
    var sTipoC;
    var sCondicionCargaC;
    var sBultosC;
    var sPesoC;
    var sIdServicioOficinaLineaNegocioC;
    var sServicioC;
    var servicioConfirmado;


    //  $("#tblServiciosAdicionales >tbody >tr >td >div >span:first-child > input[type=checkbox]:checked").each(
    // $('#tblServiciosAdicionales').find('input[type=checkbox]:checked').each(
    $('input:checkbox:checked', confirmedServicesTableControl).each(
        function () {
            sIdServicioOficinaLineaNegocioC = $(this).val(); //this.value;

            alert(sIdServicioOficinaLineaNegocioC);

            var sServicio = $("label[for='chk" + sIdServicioOficinaLineaNegocioC + "']").text();

            servicioConfirmado = {
                IdServicioOficinaLineaNegocio: sIdServicioOficinaLineaNegocioC,
                descripcionServicio: sServicioC
            }

            lstServiciosConfirmados.push(servicioConfirmado);
        }
    ).get();

    var confirmedServices = lstServiciosConfirmados;
    var confirmedDetail;

    $('input:checkbox:checked', confirmedTableControl).each(function () {
        sIdDocOriDetC = $(this).closest('tr').find("input[id='chkSeleccionAsignada']").val();
        sCargaC = ($(this).closest('tr').find('td:eq(1)').text());
        sTamanoC = ($(this).closest('tr').find('td:eq(2)').text());
        sTipoC = ($(this).closest('tr').find('td:eq(3)').text());
        sCondicionCargaC = ($(this).closest('tr').find('td:eq(4)').text());
        sBultosC = ($(this).closest('tr').find('td:eq(5)').text());
        sPesoC = ($(this).closest('tr').find('td:eq(6)').text());


        confirmedDetail =
            {
                idDocOriDet: sIdDocOriDetC,
                carga: sCargaC,
                tamano: sTamanoC,
                tipo: sTipoC,
                condicion: sCondicionCargaC,
                bultos: sBultosC,
                peso: sPesoC,
                servicios: confirmedServices
            };

        lstCargaConfirmada.push(confirmedDetail);
    }).get();


    if (lstCargaConfirmada.length > 0) {
        returnListarCargaServConfirmada(lstCargaConfirmada);
    } else {
        alert('Empty');
    }


}

function ListarServicioAdicional() {

    if (ValidarFiltro()) {
        var urlMethod = "eFenix_Solicitud_Servicio.aspx/ListarServicioAdicional";
        var IdTipoOperacion = $("#ctl00_CntMaster_dplTipoOperacion").val();
        var IdOficina = 1; //Gambetta
        var IdSuperNegocio = 89; //Dep. Temporal

        var tableControl = document.getElementById('ctl00_CntMaster_GrvListado');
        var sCondicionCarga;
        var e = document.getElementById("ctl00_CntMaster_dplTipoOperacion");
        var sTipoOperacion = e.options[e.selectedIndex].value;

        var IdLineaNegocio;

        var sIdDocOriDet;
        var sCarga;
        var sTamano;
        var sTipo;
        var sBultos;
        var sPeso;
        var detalle;

        $('input:checkbox:checked', tableControl).each(function () {
            sIdDocOriDet = $(this).closest('tr').find("input[id='chkSeleccionar']").val();
            sCarga = ($(this).closest('tr').find('td:eq(4)').text());
            sTamano = ($(this).closest('tr').find('td:eq(5)').text());
            sTipo = ($(this).closest('tr').find('td:eq(6)').text());
            sCondicionCarga = ($(this).closest('tr').find('td:eq(7)').text());
            sBultos = ($(this).closest('tr').find('td:eq(8)').text());
            sPeso = ($(this).closest('tr').find('td:eq(9)').text());

            detalle =
                {
                    idDocOriDet: sIdDocOriDet,
                    carga: sCarga,
                    tamano: sTamano,
                    tipo: sTipo,
                    condicion: sCondicionCarga,
                    bultos: sBultos,
                    peso: sPeso
                };

            lstCargaSeleccionada.push(detalle);
        }).get();


        if (sTipoOperacion == 66) { //IMPO
            IdLineaNegocio = (sCondicionCarga == "FCL") ? 80 : (sCondicionCarga == "LCL") ? 82 : (sCondicionCarga == "CARGA SUELTA") ? 84 : 86;

        }
        else if (sTipoOperacion == 65) { //EXPO
            IdLineaNegocio = (sCondicionCarga == "FCL") ? 81 : (sCondicionCarga == "LCL") ? 83 : (sCondicionCarga == "CARGA SUELTA") ? 85 : 87;
        }

        var jsonData = JSON.stringify({ iIdTipoOperacion: IdTipoOperacion, iIdOficina: IdOficina, iIdSuperNegocio: IdSuperNegocio, iIdLineaNegocio: IdLineaNegocio });

        lstCargaSeleccionada.forEach(ReturnListarCargaSeleccionada);


        SendAjax(urlMethod, jsonData, ReturnListarServicioAdicional);
    }

}

//Ajax
function SendAjax(urlMethod, jsonData, returnFunction) {
    $.ajax({
        method: "POST",
        contentType: "application/json; charset=utf-8",
        url: urlMethod,
        data: jsonData,
        dataType: "json",
        success: function (msg) {
            // Do something interesting here.
            if (msg != null) {
                returnFunction(msg);
            }
        },
        error: function (xhr, status, error) {
            // Boil the ASP.NET AJAX error down to JSON.
            var err = eval("(" + xhr.responseText + ")");

            // Display the specific error raised by the server
            alert(err.Message);
        }
    });
}

function returnListarCargaServConfirmada(confirmedServices) {
    var lstConfirmedCarga = "";
    var count = 0;
    var countServices = 0;

    var service;

    lstConfirmedCarga = "";

    $.each(confirmedServices.d, function (key, val) {
        count++;
        lstConfirmedCarga = "<div id='dv" + count + "'>";

        lstConfirmedCarga += "<input type='checkbox' value='" + val.idDocOriDet + "' id='chkCargaConfirmada" + val.idDocOriDet + "' style='vertical-align:middle' />";
        lstConfirmedCarga += "<label for='chkCargaConfirmada+'" + val.idDocOriDet + "'>" + val.carga + ", " + val.tamano + ", " + val.tipo + ", " + val.condicion + ", " + val.bultos + ", " + val.peso + "</label";

        lstConfirmedCarga += "<fieldset style='width:95%;float:right;'>";

        var servicios = val.servicios;

        if (servicios.length > 0) {
            lstConfirmedCarga += "<legend style='background-color:#0069AE;font-weight:700;padding:0;width: 100%;'>Servicios</legend>";

            $.each(servicios.d, function (key, val) {

                lstConfirmedCarga += "<div>";
                lstConfirmedCarga += "<input type='checkbox' value='" + val.IdServicioOficinaLineaNegocio + "' id='chkDet" + val.IdServicioOficinaLineaNegocio + "' style='vertical-align:middle'/>";
                lstConfirmedCarga += "<label for='chkDet" + val.IdServicioOficinaLineaNegocio + "'/>" + val.descripcionServicio + "</label>";
                lstConfirmedCarga += "</div>";
            });
        }

        lstConfirmedCarga += "</fieldset>";





    });


    $("#dvCargaConfirmada").append(lstConfirmedCarga);
}

function ReturnListarCargaSeleccionada(det) {
    //var striped = det.length;    


    var listCarga = "";

    //if (striped % 2 == 0) {
    listCarga = "<tr class='textogrilla' style='height:23px;'>";
    //} else {
    //listCarga = "<tr class='textogrilla' style='background-color: #E6E6E6; height: 23px;'>";
    //}
    listCarga += "<td style='width:20px;'>" +
        "<input id='chkSeleccionAsignada' type='checkbox' value='" + det.idDocOriDet + "'>"
        + "</td>";
    listCarga += "<td>" + det.carga + "</td>";
    listCarga += "<td>" + det.tamano + "</td>";
    listCarga += "<td>" + det.tipo + "</td>";
    listCarga += "<td>" + det.condicion + "</td>";
    listCarga += "<td>" + det.bultos + "</td>";
    listCarga += "<td>" + det.peso + "</td>";
    listCarga += "</tr>";

    $("#tbdCargaSeleccionada").append(listCarga);
}



function ReturnListarServicioAdicional(msg) {
    var listItems = "";
    var acc = 0;
    var nLength = msg.d.length;


    listItems += "<td>";
    $.each(msg.d, function (key, val) {
        acc++;
        listItems += "<div>" +
            "<span style='width: 100px; vertical-align: middle'>" +
            "<input id='chk" + val.iIdServicioOficinaLineaNegocio + "' type='checkbox' name='chk" + val.iIdServicioOficinaLineaNegocio + "' value='" + val.iIdServicioOficinaLineaNegocio + "'>" +
            "<label for='chk" + val.iIdServicioOficinaLineaNegocio + "'>" + val.sServicio + "</label>" +
            "</span>" +
            "</div>";

        if (acc % 2 == 0) {
            listItems += "</td>" +
                "<td>";
        }

        if (acc == nLength) {
            listItems += "</td>";
        }
    }
    );
    $("#TrServiciosAdicionales").html(listItems);

}



function GoBack() {
    var g;
    g = confirm("Se perderan los cambios realizados. ¿Desea continuar?")
    if (g) {
        window.location.href = window.location.href;
    } else {
        return false;
    }
}


