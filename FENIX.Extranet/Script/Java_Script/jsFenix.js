﻿/// <reference name="MicrosoftAjax.js"/>

Type.registerNamespace("FENIX.web");

FENIX.web.jsFenix = function(element) {
    FENIX.web.jsFenix.initializeBase(this, [element]);
}

FENIX.web.jsFenix.prototype = {
    initialize: function() {
        FENIX.web.jsFenix.callBaseMethod(this, 'initialize');
        
        // Agregar inicialización personalizada aquí
    },
    dispose: function() {        
        //Agregar acciones de eliminación personalizadas aquí
        FENIX.web.jsFenix.callBaseMethod(this, 'dispose');
    }
}
FENIX.web.jsFenix.registerClass('FENIX.web.jsFenix', Sys.UI.Control);

if (typeof(Sys) !== 'undefined') Sys.Application.notifyScriptLoaded();

// Archivo JScript
var Mes = ["ENERO", "FEBRERO", "MARZO", "ABRIL", "MAYO", "JUNIO", "JULIO", "AGOSTO", "SEPTIEMBRE", "OCTUBRE", "NOVIEMBRE", "DICIEMBRE"];
var firstAnio = 2008;
var FechaHoy = new Date();
function getNombreMes(pMes) {
    alert(pMes)
    return Mes[pMes];
}
function setComboMes(pCboMes) {
    for (i = 0; i < Mes.length; i++) {
        pCboMes.options[i] = new Option(Mes[i], (i + 1))
    }
    pCboMes.selectedIndex = FechaHoy.getMonth();
}

//aumenta una determinada cantidad de horas a la actual
function ola()
{
    alert('holaaa');
}
function MasHoras(CantHora) {
    Ahorita = MiFecha();
    NuevaHora = Ahorita.substring(0, Ahorita.indexOf(':'));
    elResto = Ahorita.substring(Ahorita.indexOf(':'));
    NuevaHora = (NuevaHora * 1) + CantHora;
    if (NuevaHora >= 60) NuevaHora = NuevaHora - 60;
    NuevaHora = NuevaHora + elResto;
    return NuevaHora;
}
//retorna la fecha en formato dd MMM yyyy HH:mm
function MiFecha() {
    Fecha = new Date();
    Fecha = '' + Right('00' + Fecha.getHours(), 2) + ':' + Right('00' + Fecha.getMinutes(), 2) + ':' + Right('00' + Fecha.getSeconds(), 2);
    return Fecha;
    /*Right('00'+Fecha.getDate(),2) + ' ' + Mes[Fecha.getMonth()].substring(0,3) + ' ' + Fecha.getFullYear() + ' ' + */
}
//llenar un SELECT con años
function setComboAnio(pCboAnio) {
    endAnio = FechaHoy.getYear();
    for (i = firstAnio; i < (endAnio + 1); i++) {
        pCboAnio.options[i - firstAnio] = new Option(i, i);
    }
    pCboAnio.selectedIndex = pCboAnio.options.length - 1;
}
/*  TipoError:  0:correcto  1:vacio     2:noextension  */
function testFile(id) {

    var file = document.getElementById(id);
    var fileName = file.value;

    fileName = fileName.slice(fileName.lastIndexOf("\\") + 1);
    tipoError = -1;
    if (fileName.length > 0) {
        var ext = (fileName.toLowerCase().substring(fileName.lastIndexOf(".")))
        var listado = new Array(".xls");
        var ok = false;
        for (var i = 0; i < listado.length; i++) {
            if (listado[i] == ext) {
                ok = true;
                break;
            }
        }
        if (ok) {
            tipoError = 0;
        } else {
            tipoError = 2;
        }
    }
    else {
        tipoError = 1;
    }
    return tipoError;
}

function DisplayElementos(pTag, pClass) {
    for (i = 0; i < document.all.tags(pTag).length; i++) {
        obj = document.all.tags(pTag)[i];
        if (obj != null)
            document.getElementById(obj.id).className = pClass;
    }
}
//Selecciona un Grupo de Checks segun el ID de un padre
function SelectGroupChecks(ParentID) {
    idCheck = ParentID.replace('All', '');
    frm = document.forms[0];
    for (i = 0; i < frm.elements.length; i++) {
        obj = frm.elements[i];
        if (obj.type == 'checkbox') {
            if (obj.id.replace(idCheck, '').length != obj.id.length) {
                obj.checked = document.getElementById(ParentID).checked;
            }
        }
    }
}
//Verifica si se ha seleccionado algun check de un determinado grupo
function IfChecked(ID, pMensaje) {
    //alert(pMensaje.length)
    ok = false;
    frm = document.forms[0];
    for (i = 0; i < frm.elements.length; i++) {
        obj = frm.elements[i];
        if (obj.type == 'checkbox') {
            if (obj.id.replace(ID, '').length != obj.id.length) {
                if (obj.checked) {
                    ok = true;
                    break;
                }
            }
        }
    }
    if (!ok && pMensaje.length > 0) {
        alert(pMensaje);
    }
    return ok;
}

/* FUNCIONES PARA EL SELECT */
function ReturnIndex(ddl, Value) {
    Elementos = ddl.options;
    indice = -1;
    for (i = 0; i < Elementos.length; i++) {
        if (Elementos[i].value == Value) {
            indice = i;
        }
    }
    return indice;
}
/* FUNCIONES DE VALIDACION */
function IsEntero(pValor) {
    var strValidChars = "0123456789";
    var strChar;
    var Find = true;

    if (pValor.length == 0) return false;
    if (pValor.length > 10) return false; //longitud maxima de un entero
    for (i = 0; i < pValor.length && Find == true; i++) {
        strChar = pValor.charAt(i);
        if (strValidChars.indexOf(strChar) == -1)
            Find = false;
    }
    return Find;
}

/* FUNCIONES DE CADENA */
function Left(str, n) {
    if (n <= 0)
        return "";
    else if (n > String(str).length)
        return str;
    else
        return String(str).substring(0, n);
}
function Right(str, n) {
    if (n <= 0)
        return "";
    else if (n > String(str).length)
        return str;
    else {
        var iLen = String(str).length;
        return String(str).substring(iLen, iLen - n);
    }
}
function IsEmpty(Texto) {
    TextoSinEnter = Texto.replace(/\r\n/gi, '');
    TextoSinEnter = TextoSinEnter.replace(/ /gi, '');
    return (TextoSinEnter.length == 0);
}
/*  FUNCION INGRESO DE DATOS    */
function SoloEnteros() {
    k = event.keyCode;
    if (k >= 48 && k <= 57) { }
    else { event.keyCode = 0; }
}


function AlphaNumericOnly(e) {
    var evt = (e.which) ? e.which : event.keyCode
    if (evt > 31 && (evt < 48 || evt > 57) && (evt < 65 || evt > 90) && (evt < 97 || evt > 122)) {
        return false;
    }
    else {

    }
}

/*  FUNCION EFECTOS     */
function changeto(highlightcolor) {
    source = event.srcElement
    if (source.tagName == "TR")
        return
    while (source.tagName != "TR")
        source = source.parentElement
    if (source.style.backgroundColor != highlightcolor && source.id != "ignore")
        source.style.backgroundColor = highlightcolor
}
function changeback(originalcolor) {
    if (event.fromElement.contains(event.toElement) || source.contains(event.toElement) || source.id == "ignore")
        return
    if (event.toElement != source)
        source.style.backgroundColor = originalcolor
}
function DisplayElementos(pTag, pClass) {
    for (i = 0; i < document.all.tags(pTag).length; i++) {
        obj = document.all.tags(pTag)[i];
        if (obj != null)
            document.getElementById(obj.id).className = pClass;
    }
}
function getValue(name) {
    var type = '', value = '', n = 0;

    // get the type of variable 
    try {
        type = eval('typeof ' + name);
    } catch (e) {
        type = e.message;
    }

    try {
        if (eval('typeof ' + name) != 'object')
        // get the value of simple variable
            value = eval(name);
        else {
            // for-in loop to get the names and values of children of a object
            for (var child in eval(name)) {
                try {
                    value += '.' + child + '=' + eval(name + '.' + chid) + '; ';
                } catch (e) {
                    value += '.' + child + '=error:' + e.message + '; ';
                }

                // only return the first 20 children if it is a object variable
                if (n++ >= 20) break;
            }
        }
    } catch (ex) {
        value = 'Error: ' + ex.message;
    }

    // return '<td>[simple vaiable name]</td><td>[value]</td>[type]</td>' or
    // return '<td>[object name]</td><td>.[child name][value]</td>object</td>'
    return '<td>' + name + '</td><td>' + ((n < 20) ? '' : '<font color=\"gray\">First 20 properties:</font><br />') + value + '</td><td>' + type + '</td>';
}







