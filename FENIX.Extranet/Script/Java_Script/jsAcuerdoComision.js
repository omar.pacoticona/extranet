﻿$(document).ready(function () {


    $("#ctl00_CntMaster_txtLiquidacionn").keydown(function (event) {
        if (event.shiftKey) {
            event.preventDefault();
        }

        if (event.keyCode == 46 || event.keyCode == 8) {
        }
        else {
            if (event.keyCode < 95) {
                if (event.keyCode < 48 || event.keyCode > 57 ) {
                    event.preventDefault();
                }
                
            }
            else {
                if (event.keyCode < 96 || event.keyCode > 105 ) {
                    event.preventDefault();
                }
            }
        }
        
    });

    $("#ctl00_CntMaster_txtLiquidacion").empty();
    cargarFecha(); 

    
});

var dateToday = new Date();
dateToday.setDate(dateToday.getDate() + 1);


$.datepicker.regional['es'] = {
    closeText: 'Cerrar',
    //prevText: '< Ant',
    nextText: 'Sig >',
    //currentText: 'Hoy',
    //minDate: dateToday,
    monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
    //monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
    dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
    dayNamesShort: ['Dom', 'Lun', 'Mar', 'Mié', 'Juv', 'Vie', 'Sáb'],
    dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sá'],
    //weekHeader: 'Sm',
    dateFormat: 'dd/mm/yy',
    firstDay: 1,
    
    //isRTL: false,
    //showMonthAfterYear: false,
    //yearSuffix: '',
    //beforeShowDay: function (date) {
    //    var day = date.getDay();
    //    return [(day != 0), ''];
    //}
};

$.datepicker.setDefaults($.datepicker.regional['es']);
$(function () {
    $("input[id$='datepicker']").datepicker();

});

function ValidarCheck() {
    
    var isChecked = $("input:checkbox").is(':checked');
    if (!isChecked) {
        alert("Debe Seleccionar al menos un registro");
        return false;    
    }
    else {
        return confirm('¿Seguro de generar la liquidación?');
    }
    
}

function RegistrarFacturaLiq() {


            var isChecked = $("input:checkbox").is(':checked');
    if (!isChecked)
    {
                alert("Debe Seleccionar la fila del registro que va asociar la factura");
                return false;
            }
    else
    {
                    
                    var urlMethod = "eFENIX_LiquidacionesXProveedor.aspx/RegistrarFactura_Liquidacion";
                    var mensaje = "";
                    var IdLiquidacion;
                    var Serie;
                    var NroFactura;
                    var ValidarSerie;

                    $("input:checkbox:checked").each(function () {

                        IdLiquidacion = ($(this).closest('tr').find('td:eq(1)').text());
                        Serie = ($(this).closest('tr').find('input:text:eq(0)').val());                        
                        NroFactura = ($(this).closest('tr').find('input:text:eq(1)').val());                  


        });
                if (Serie.length <4) {
                    alert("Numero de serie 4 digitos.");
                    return false;
                   }
                 if (!NroFactura.match(/^[0-9]+$/)) {
                     alert("Numero de factura solo numeros.");
                     return false;
                }


                if (Serie == "") {
                    alert("Ingresar numero de serie");
                    return false;
                }
                else if (NroFactura == "") {
                    alert("Ingresar numero de factura");
                    return false;
                }
                else {
                    var jsonData = JSON.stringify({ iLiquidacion: IdLiquidacion, sNroSerie: Serie, sNroFactura: NroFactura, smensaje: mensaje });
                    SendAjax(urlMethod, jsonData);
                }
                
            }
    
    
}

function SendAjax(urlMethod, jsonData) {

    $.ajax({
        method: "POST",
        contentType: "application/json; charset=utf-8",
        url: urlMethod,

        data: jsonData,
        dataType: "json",        
        success: function (response) {
           
            var names = response.d;
            alert(names);
        },
        error: function (xhr, status, error) {
            // Boil the ASP.NET AJAX error down to JSON.
            var err = eval("(" + xhr.responseText + ")");

            // Display the specific error raised by the server
            alert(err.Message);
        }
    });

   
    
    //});
}

function validarNumeroFactura() {



    $(this).closest('tr').find('input:text:eq(1)').keydown(function (event) {
        if (event.shiftKey) {
            event.preventDefault();
        }

        if (event.keyCode == 46 || event.keyCode == 8) {
        }
        else {
            if (event.keyCode < 95) {
                if (event.keyCode < 48 || event.keyCode > 57) {
                    event.preventDefault();
                }
            }
            else {
                if (event.keyCode < 96 || event.keyCode > 105) {
                    event.preventDefault();
                }
            }
        }



    });
   
}

function SoloEnteros(e) {
    var tecla = document.all ? tecla = e.keyCode : tecla = e.which;
    return ((tecla > 47 && tecla < 58) || tecla == 44 || tecla == 46);
}


//eFenix_Factura_Consulta.

//$(function () {
//    //$('#txtIni_datep').datepicker();
//    //$('#txtFin_datep').datepicker();

//    //$("#txtIni_datep").on("dp.change", function (e) {
//    //    $('#txtFin_datep').data("DateTimePicker").minDate(e.date);
//    //});
//    //$("#txtFin_datep").on("dp.change", function (e) {
//    //    $('#txtIni_datep').data("DateTimePicker").maxDate(e.date);
//    //});
//    $('#txtIni_datep').datetimepicker();
    
//});



function cargarFecha() {
    var date = new Date();
    var mes = date.getMonth() + 1;
    var dia = date.getDate();


           
    if (mes < 10) {
        mes = "0" + mes;
    }
    var fechaFin = dia + "/" + mes + "/" + date.getFullYear();
    var FechaInicio = "01" + "/" + mes + "/" + date.getFullYear();

    $('#txtIni_datep').val(FechaInicio);
    $('#txtFin_datep').val(fechaFin);

    
}

$(function () {
    $("#txtIni_datep").datepicker({
        defaultDate: dateToday,
        value : 'xd'
        

    });

    $("#txtFin_datep").datepicker({
        defaultDate: dateToday,
        useCurrent: false

    });
    
});






