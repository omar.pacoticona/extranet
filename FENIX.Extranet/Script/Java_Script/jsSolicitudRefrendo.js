﻿$(document).ready(function () {
   // LoadAdditionalData();
    //$('#ctl00_CntMaster_DplFormaPago').hide();
    //$('#ctl00_CntMaster_DplBanco').hide();
    //$('#ctl00_CntMaster_txtMonto').hide();
    //$('#ctl00_CntMaster_txtOperacion').hide();
    //$('#ctl00_CntMaster_DplMoneda').hide();
    //$('#ctl00_CntMaster_lblFormaPago').hide();
    //$('#ctl00_CntMaster_lblBanco').hide();
    //$('#ctl00_CntMaster_lblMonto').hide();
    //$('#ctl00_CntMaster_lblNroOperacion').hide();
    //$('#ctl00_CntMaster_lblMoneda').hide();

    cargarFecha();
    OcultarDiv();
});



function habiliteTabs() {
    //$("#tabs-container").tabs();
    //$("#tabs-container").tabs("option", "disabled", [1, 2]);

    if (ValidarNextPage1()) {
        $(".btnNext").click(function () {
            $("#tabs-container").tabs("option", "disabled", [0, 2]);
            $("#tabs-container").tabs("option", "active", $("#tabs-container").tabs('option', 'active') + 1);
            $("#ctl00_CntMaster_btnNextTab1").click();
            return false;
        });
    }

    //$(".btnNext1").click(function () {
    //    $("#tabs-container").tabs("option", "disabled", [0, 1]);
    //    $("#tabs-container").tabs("option", "active", $("#tabs-container").tabs('option', 'active') + 1);
    //    return false;
    //});

    //$(".btnPrev").click(function () {
    //    $("#tabs-container").tabs("option", "disabled", [1, 2]);
    //    $("#tabs-container").tabs("option", "active", $("#tabs-container").tabs('option', 'active') - 1);
    //    return false;
    //});
    //$(".btnPrev1").click(function () {
    //    $("#tabs-container").tabs("option", "disabled", [0, 2]);
    //    $("#tabs-container").tabs("option", "active", $("#tabs-container").tabs('option', 'active') - 1);
    //    return false;
    //});
    //$(".btnConfirm").click(function () {
    //    alert("Solicitud de servicio registrada. Imprima el documento.")
    //    return false;
    //});
}

$(function () {
    $("#tabs-container").tabs();
    $("#tabs-container").tabs("option", "disabled", [1]);

    //$(".btnNext").click(function () {
    //    if (ValidarNextPage1()) {
           
    //        $("#tabs-container").tabs("option", "disabled", [0]);
    //        $("#tabs-container").tabs("option", "active", $("#tabs-container").tabs('option', 'active') + 1);
                    
    //        return false;
    //    }
    //});


});

var dateToday = new Date();
dateToday.setDate(dateToday.getDate() + 1);

$.datepicker.regional['es'] = {
    closeText: 'Cerrar',
    //prevText: '< Ant',
    nextText: 'Sig >',
    //currentText: 'Hoy',
    //minDate: dateToday,
    monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
    //monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
    dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
    dayNamesShort: ['Dom', 'Lun', 'Mar', 'Mié', 'Juv', 'Vie', 'Sáb'],
    dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sá'],
    //weekHeader: 'Sm',
    dateFormat: 'dd/mm/yy',
    firstDay: 1,

    //isRTL: false,
    //showMonthAfterYear: false,
    //yearSuffix: '',
    //beforeShowDay: function (date) {
    //    var day = date.getDay();
    //    return [(day != 0), ''];
    //}
};

$.datepicker.setDefaults($.datepicker.regional['es']);
$(function () {
    $("input[id$='datepicker']").datepicker();

});

function cargarFecha() {
    var date = new Date();
    var mes = date.getMonth() + 1;
    var dia = date.getDate();



    if (mes < 10) {
        mes = "0" + mes;
    }
    var fechaFin = dia + "/" + mes + "/" + date.getFullYear();
    var FechaInicio = "01" + "/" + mes + "/" + date.getFullYear();

    $('#txtIni_datep').val(FechaInicio);
    $('#txtFin_datep').val(fechaFin);


}

$(function () {
    $("#txtIni_datep").datepicker({
        defaultDate: dateToday,
        value: 'xd'


    });

    $("#txtFin_datep").datepicker({
        defaultDate: dateToday,
        useCurrent: false

    });

});

$(function () {
   
    
    //$('#ctl00_CntMaster_DplTipoPago').change(function () {
    //    var selectedVal = $('#ctl00_CntMaster_DplTipoPago').val();
        
    //    if (selectedVal == 1) { //CREDITO
    //        $('#ctl00_CntMaster_DplFormaPago').hide();
    //        $('#ctl00_CntMaster_DplBancoOrigen').hide();
    //        $('#ctl00_CntMaster_DplBancoDestino').hide(); //lblBancoDestino
    //        $('#ctl00_CntMaster_txtMonto').hide();
    //        $('#ctl00_CntMaster_txtOperacion').hide();
    //        $('#ctl00_CntMaster_DplMoneda').hide();

    //        $('#ctl00_CntMaster_lblFormaPago').hide();
    //        $('#ctl00_CntMaster_lblBanco').hide();
    //        $('#ctl00_CntMaster_lblMonto').hide();
    //        $('#ctl00_CntMaster_lblNroOperacion').hide();
    //        $('#ctl00_CntMaster_lblMoneda').hide();
    //        $('#ctl00_CntMaster_lblBancoDestino').hide();
    //        $("#ctl00_CntMaster_FileUploadTipoPago").prop('disabled', true);
            

    //    }

    //    if (selectedVal == 2) { //CONTADO
    //        $('#ctl00_CntMaster_DplFormaPago').show();
    //        $('#ctl00_CntMaster_DplBancoOrigen').show();
    //        $('#ctl00_CntMaster_DplBancoDestino').show();
    //        $('#ctl00_CntMaster_txtMonto').show();
    //        $('#ctl00_CntMaster_txtOperacion').show();
    //        $('#ctl00_CntMaster_DplMoneda').show();

    //        $('#ctl00_CntMaster_lblFormaPago').show();
    //        $('#ctl00_CntMaster_lblBanco').show();
    //        $('#ctl00_CntMaster_lblMonto').show();
    //        $('#ctl00_CntMaster_lblNroOperacion').show();
    //        $('#ctl00_CntMaster_lblMoneda').show();
    //        $('#ctl00_CntMaster_lblBancoDestino').show();
    //        $("#ctl00_CntMaster_FileUploadTipoPago").prop('disabled', false);

    //    }

      
    //});



});



function ValidarNextPage1() {

    return true;

    //if ($("#ctl00_CntMaster_GrvListado >tbody >tr >td:first-child > input[type=checkbox]:checked").length <= 0) {
    //    alert("Debe seleccionar elementos del listado");
    //    return false;
    //}

    //var e = document.getElementById("ctl00_CntMaster_dplServicio");
    //var sServicio = e.options[e.selectedIndex].value;
    //var eDate = $("#ctl00_CntMaster_txtP_datepicker").val();

    //if (fc_Trim(eDate) == "") {
    //    alert("Ingrese fecha de programación del servicio")

    //    $("#ctl00_CntMaster_txtP_datepicker").val($("#ctl00_CntMaster_txtP_datepicker").val());

    //    return false;
    //}

    //if ((fc_Trim(sServicio) == "-1")) {
    //    alert("Seleccione Servicio que desea Solicitar")
    //    return false;
    //}

    //if ((fc_Trim(sServicio) == "144")) {

    //    var sDAM = $("#ctl00_CntMaster_txtDetalleServicio").val();

    //    if ((fc_Trim(sDAM) == "")) {
    //        alert("Movilización de Aforo: Debe ingresar Nro. DAM");
    //        $('#ctl00_CntMaster_lblDAM').show();
    //        $('#ctl00_CntMaster_txtDetalleServicio').show();
    //        return false;
    //    }

    //}


    //else {
    //    var selectedDate = $("#ctl00_CntMaster_txtP_datepicker").val();
    //    $('#ctl00_CntMaster_datepickerSeleccionado').val(selectedDate);

    //    var tableControl = document.getElementById('ctl00_CntMaster_GrvListado');
    //    var selectedDO;
    //    var selectedVO;
    //    var selectedCO;
    //    var selectedIdServicio;
    //    var selectedTxtServicio;

    //    var lstDOSelected = [];
    //    var lstCOSelected = [];

    //    var DOduplicated = [];
    //    var COduplicated = [];

    //    $('input:checkbox:checked', tableControl).each(function () {
    //        selectedDO = ($(this).closest('tr').find('td:eq(1)').text());
    //        selectedVO = ($(this).closest('tr').find('td:eq(2)').text());
    //        selectedCO = ($(this).closest('tr').find('td:eq(7)').text());
    //        selectedIdServicio = $("#ctl00_CntMaster_dplServicio").val();
    //        selectedTxtServicio = $("#ctl00_CntMaster_dplServicio option:selected").text();

    //        lstDOSelected.push(selectedDO);
    //        lstCOSelected.push(selectedCO);
    //    }).get();

    //    $.each(lstDOSelected, function (key, value) {
    //        if ($.inArray(value, DOduplicated) == -1) {
    //            DOduplicated.push(value);
    //        }
    //    });

    //    $.each(lstCOSelected, function (key, value) {
    //        if ($.inArray(value, COduplicated) == -1) {
    //            COduplicated.push(value);
    //        }
    //    });

    //    if (DOduplicated.length > 1) {
    //        alert("Debe seleccionar carga de un solo documento");
    //        return false;
    //    }

    //    if (COduplicated.length > 1) {
    //        alert("La condición de la carga seleccionada debe ser la misma");
    //        return false;
    //    }

    //    //$("#ctl00_CntMaster_dplServicioSeleccionado").append(new Option(selectedTxtServicio, selectedIdServicio));
    //    //$("#ctl00_CntMaster_dplServicioSeleccionado").prop("disabled", true);
    //    //$('#ctl00_CntMaster_txtDOSeleccionado').val(selectedDO);
    //    //$('#ctl00_CntMaster_txtVOSeleccionado').val(selectedVO);


    //    return true;
    //}

}

function saveFileUpl(vi_uplNombre) {

    if (vi_uplNombre == 'imgBtnUplDua') {
        var tienearchivo = $("#ctl00_CntMaster_FileUploadDua").val();
        if (tienearchivo == null || tienearchivo == '') {
            alert("Seleccione un archivo DAM");
            return false;
        }
    }
    if (vi_uplNombre == 'imgBtnUplBoking') {
        var tienearchivo = $("#ctl00_CntMaster_FileUploadBooking").val();
        if (tienearchivo == null || tienearchivo == '') {
            alert("Seleccione un archivo Booking");
            return false;
        }
    }
    if (vi_uplNombre == 'imgBtnUplTicekt') {
        var tienearchivo = $("#ctl00_CntMaster_FileUploaTicket").val();
        if (tienearchivo == null || tienearchivo == '') {
            alert("Seleccione un archivo Ticket");
            return false;
        }
    }
    if (vi_uplNombre == 'imgBtnUplPackList') {
        var tienearchivo = $("#ctl00_CntMaster_FileUploadPackList").val();
        if (tienearchivo == null || tienearchivo == '') {
            alert("Seleccione un archivo PackList");
            return false;
        }
    }
    if (vi_uplNombre == 'imgBtnUplGuiaRem') {
        var tienearchivo = $("#ctl00_CntMaster_FileUploadsGuiaRem").val();
        if (tienearchivo == null || tienearchivo == '') {
            alert("Seleccione un archivo GuiaRemision");
            return false;
        }
    }
    if (vi_uplNombre == 'imgBtnUplTipoPago') {
        var tienearchivo = $("#ctl00_CntMaster_FileUploadTipoPago").val();
        if (tienearchivo == null || tienearchivo == '') {
            alert("Seleccione un archivo Documento de pago");
            return false;
        }
    }
    //if (vi_uplNombre == 'FileUploadBooking') {
    //    document.getElementById("<%=imgBtnUplBoking.ClientID%>").click();
    //}
    //if (vi_uplNombre == 'FileUploaTicket') {
    //    document.getElementById("<%=imgBtnUplTicekt.ClientID%>").click();
    //}
    //if (vi_uplNombre == 'FileUploadPackList') {
    //    document.getElementById("<%=imgBtnUplPackList.ClientID%>").click();
    //}
    //if (vi_uplNombre == 'FileUploadsGuiaRem') {
    //    document.getElementById("<%=imgBtnUplGuiaRem.ClientID%>").click();
    //}

    //if (vi_uplNombre == 'FileUploadTipoPago') {
    //    document.getElementById("<%=imgBtnUplTipoPago.ClientID%>").click();
    //}
}

function HabilitaIsPostack() {
    $("#tabs-container").tabs();
    $("#tabs-container").tabs("option", "disabled", [1]);
    //$("#tabs-container").tabs("option", "active", $("#tabs-container").tabs('option', 'active') - 2);
}


function HabilitaPostPostack() {
    $("#tabs-container").tabs();
    $("#tabs-container").tabs("option", "disabled", [0]);
    $("#tabs-container").tabs("option", "active", $("#tabs-container").tabs('option', 'active') + 1);
   // $("#tabs-container").tabs("option", "active", [0]);
}

function HabilitaIsIsPostack() {
    $("#tabs-container").tabs();
    $("#tabs-container").tabs("option", "disabled", [1]);
    $("#tabs-container").tabs("option", "active", $("#tabs-container").tabs('option', 'active') - 1);
}

function OcultarDiv() {
    $("#IdDetalle").hide();
   // $("#divDetalles3").hide();
    
}

function OcultarDivDet() {   
    $("#divDetalles3").hide();
}

function MostrarDiv() {
    $("#IdDetalle").show();
    $("#divDetalles3").show();
}

function Alerta() {
    var alert = alertify.alert('Mensaje', 'Error').set('label', 'Aceptar');
    alert.set({ transition: 'zoom' }); //slide, zoom, flipx, flipy, fade, pulse (default)
    alert.set('modal', false);  //al pulsar fuera del dialog se cierra o no	
}