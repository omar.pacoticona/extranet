﻿
/*Funciones para cabeceras de TAB's */

var strClassNameOffForm = "";
var strImgIzqOffForm = "";
var strImgDerOffForm = "";

function setTabCabeceraOffForm(strIndiceTab)
{
    try
    {
        
        objTableTab = document.getElementById("tblHeader" + strIndiceTab);
        if (objTableTab != null)
        {
            objTdCentro = objTableTab.rows[0].cells[0];
            if ( objTdCentro != null ) objTdCentro.className = "TabCabeceraOffForm";
        }
    }    
    catch(e)
    {
        strMsg = e.message;
    }
}

function setTabCabeceraOnForm(strIndiceTab)
{
    try
    {
        objTableTab = document.getElementById("tblHeader" + strIndiceTab);
        if (objTableTab != null)
        {
            objTdCentro = objTableTab.rows[0].cells[0];
            if ( objTdCentro != null ) objTdCentro.className = "TabCabeceraOnForm";
        }
    }    
    catch(e)
    {
        strMsg = e.message;
    }
}



function onTabCabeceraOutForm(strIndiceTab)
{
    try
    {
        objTableTab = document.getElementById("tblHeader" + strIndiceTab);

        if (objTableTab != null)
        {
            objTdCentro = objTableTab.rows[0].cells[0];      
            
            if ( objTdCentro != null && strClassNameOffForm != "" ) objTdCentro.className = strClassNameOffForm;
        }
    }    
    catch(e)
    {
        strMsg = e.message;
    }
}

function onTabCabeceraOverForm(strIndiceTab)
{
    try
    {
        objTableTab = document.getElementById("tblHeader" + strIndiceTab);
        if (objTableTab != null)
        {
            objTdCentro = objTableTab.rows[0].cells[0];
            if ( objTdCentro != null ) objTdCentro.className = "TabCabeceraOnForm";
        }
    }    
    catch(e)
    {
        strMsg = e.message;
    }
}

