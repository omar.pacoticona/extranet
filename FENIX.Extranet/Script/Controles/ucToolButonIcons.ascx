<%@ Control Language="C#" AutoEventWireup="true"    CodeFile="ucToolButonIcons.ascx.cs" Inherits="WebUserControlIcons" %>
<table class="fondo_celeste" >
    <tr>
        <td >
            <asp:ImageButton ID="imgB_Nuevo"  OnClientClick = "IndicadorNuevo();" 
                runat="server" 
                ImageUrl= "~/Script/Imagenes/IconButton/ToolButton_New.png" 
                ToolTip="Nuevo Registro" />
        </td>
       
        <td >
            <asp:ImageButton ID="imgB_Grabar" Enabled="false" 
                OnClientClick="IndicadorGrabar();" runat="server" 
                ImageUrl="~/Script/Imagenes/IconButton/ToolButton_Save.png" 
                ToolTip="Grabar Registro"/>
        </td>
       
        <td >
            <asp:ImageButton ID="imgB_Editar" runat="server" 
                OnClientClick="return IndicadorEditar();" 
                ImageUrl="~/Script/Imagenes/IconButton/ToolButton_Edit.png" 
                 ToolTip="Editar Registro" />
        </td>
       
        <td >
            <asp:ImageButton ID="imgB_Eliminar" runat="server" 
                OnClientClick="return IndicadorEliminar();" 
                ImageUrl="~/Script/Imagenes/IconButton/ToolButton_Delete.png" 
                 ToolTip="Eliminar"/>
        </td>
       
        <td >
            <asp:ImageButton ID="imgB_Listado" OnClientClick="IndicadorListar();"  
                CommandName = "prueba"  runat="server" 
                ImageUrl="~/Script/Imagenes/IconButton/ToolButton_Export.png"  
                OnClick="imgB_Listado_Click" ToolTip="Exportar" />
        </td>
       
        <td >
            <asp:ImageButton ID="imgB_Imprimir" runat="server"  
                OnClientClick="IndicadorImprimir();" 
                ImageUrl="~/Script/Imagenes/IconButton/ToolButton_Print.png" 
                ToolTip="Imprimir" />
        </td>
       
        <td >
            <asp:ImageButton ID="imgB_Cancelar" runat="server"  
                OnClientClick="IndicadorCancelar();" 
                ImageUrl="~/Script/Imagenes/IconButton/ToolButton_Return.png" 
                ImageAlign="Bottom" ToolTip="Cancelar" />
        </td>
       
    </tr>    
    
    <script language="javascript"   type="text/javascript">



//        Nuevo = 1,
//            Grabar = 2,
//            Editar = 3,
//            Eliminar = 4,
//            Imprimir = 5,
//            Listar = 6,
//            Salir = 7,
            
        function IndicadorListar() {
            document.getElementById('<%=hdBoton.ClientID%>').value = 9;
        
            
        }
        function IndicadorNuevo() 
        {
            document.getElementById('<%=hdBoton.ClientID%>').value = 1;
        }
        function IndicadorEditar() {
            chk = false;
            f = document.forms[0].elements
            for (x = 0; x < f.length; x++) {
                obj = f[x];
                if (obj.type == 'checkbox') {
                    if (obj.checked) {
                        chk = true;
                        document.getElementById('<%=hdBoton.ClientID%>').value = 3;                     
                        return true;
                        break;
                    }
                }
            }
            if (!chk) {
                alert('Seleccione un registro');

                return false;
            }
        }
        function IndicadorEliminar() {
            chk = false;
            f = document.forms[0].elements
            for (x = 0; x < f.length; x++) {
                obj = f[x];
                if (obj.type == 'checkbox') {
                    if (obj.checked) {
                        if (confirm('�Desea Eliminar el registro Seleccionado?')) {
                        }
                        else {
                            return false;
                        }
                        chk = true;
                        document.getElementById('<%=hdBoton.ClientID%>').value = 4;
                        return true;
                        break;
                    }
                }
            }
            if (!chk) {
                alert('Seleccione un registro');             
                return false;
            }
        }
        function IndicadorImprimir() {
            document.getElementById('<%=hdBoton.ClientID%>').value = 5;            
        }
        function IndicadorGrabar() {
            document.getElementById('<%=hdBoton.ClientID%>').value = 2;           
        }
        function IndicadorListar() {
            document.getElementById('<%=hdBoton.ClientID%>').value = 6;            
        }
        function IndicadorCancelar() {
            document.getElementById('<%=hdBoton.ClientID%>').value = 7;           
            
        }
        
     </script>  
        
</table>
 <asp:HiddenField ID="hdBoton" runat="server" />

    
