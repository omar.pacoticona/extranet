﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ctrlCalendar.ascx.cs" Inherits="Controles_ctrlCalendar" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:TextBox ID="txtFecha" runat="server" Columns="11" MaxLength="10"></asp:TextBox>
<asp:Image ID="btnFecha" runat="server" ImageUrl="~/Imagenes/calendario.gif" ImageAlign="AbsMiddle" ToolTip="Calendario"  />
<cc1:CalendarExtender ID="ceFecha" runat="server" Format="dd/MM/yyyy" 
    TargetControlID="txtFecha" PopupButtonID="btnFecha" />
<cc1:MaskedEditExtender ID="meFecha" runat="server"
        MaskType="Date" Mask="99/99/9999" UserDateFormat="DayMonthYear"
        ClearMaskOnLostFocus="true" ErrorTooltipEnabled="true" MessageValidatorTip="true"
        ClearTextOnInvalid="true" TargetControlID="txtFecha" >
</cc1:MaskedEditExtender>

<%--<asp:TextBox ID="txtFecha"  runat="server" Width="80px"  CssClass = "CssTextBox"></asp:TextBox>
<ajaxToolkit:MaskedEditExtender ID="maskFecha" runat="server"  MaskType="Date"  
 Mask="99/99/9999" UserDateFormat="DayMonthYear" TargetControlID="txtFecha">    
</ajaxToolkit:MaskedEditExtender>
<img alt="" src= "<% =sRuta  %>"   
align="absmiddle"
style="border-color: inherit; border-width: 0px; cursor:hand;" 
id="imgFecha2" onclick="popUpCalendar(this, <%=txtFecha.ClientID %>, 'dd/mm/yyyy');" >
<asp:RangeValidator ID="rv" runat="server" ControlToValidate="txtFecha" 
Display="Dynamic"
ErrorMessage="(dd/MM/yyyy)" MaximumValue="31/12/2020"  MinimumValue="01/01/1980" 
SetFocusOnError="True" Type="Date"></asp:RangeValidator>

<asp:HiddenField ID="hEnable" runat="server"   Value = "0"/>
<asp:HiddenField ID="hReload" runat="server"   Value = "0"/>--%>