<%@ Control Language="C#" 
AutoEventWireup="true" 
CodeFile="ucToolButon.ascx.cs"
Inherits="WebUserControl" %>    
    
<table cellpadding="0" cellspacing="0" align="right" style="background-color:Transparent;">
    <tr>
        <td id="td_nuevo" runat="server" >
            <img src="../../Imagenes/botones_accion/linea_espacio.jpg" />
        </td>
        <td>
            <asp:ImageButton ID="imgB_Nuevo" OnClientClick="IndicadorNuevo();" runat="server"
                ImageUrl="~/Imagenes/botones_accion/btn_nuevo.jpg" ToolTip="Nuevo Registro" />
        </td>
        <td id="td_grabar" runat="server" >
            <img src="../../Imagenes/botones_accion/linea_espacio.jpg" />
        </td>
        <td>
            <asp:ImageButton ID="imgB_Grabar" Enabled="false" OnClientClick="IndicadorGrabar();"
                runat="server" ImageUrl="~/Imagenes/botones_accion/btn_guardar.jpg" ToolTip="Grabar Registro" />
        </td>
        <td id="td_actualizar" runat="server" >
            <img src="../../Imagenes/botones_accion/linea_espacio.jpg" />
        </td>
        <td>
            <asp:ImageButton ID="imgB_Editar" runat="server" OnClientClick="return IndicadorEditar();"
                ImageUrl="~/Imagenes/botones_accion/btn_actualizar.jpg" ToolTip="Editar Registro" />
        </td>
        <td id="td_eliminar" runat="server" >
            <img src="../../Imagenes/botones_accion/linea_espacio.jpg" />
        </td>
        <td>
            <asp:ImageButton ID="imgB_Eliminar" runat="server" OnClientClick="return IndicadorEliminar();"
                ImageUrl="~/Imagenes/botones_accion/btn_eliminar.jpg" ToolTip="Eliminar" />
        </td>
        <td id="td_exportar" runat="server" >
            <img src="../../Imagenes/botones_accion/linea_espacio.jpg" />
        </td>
        <td>
            <asp:ImageButton ID="imgB_Listado" OnClientClick="IndicadorListar();" CommandName="prueba"
                runat="server" ImageUrl="~/Imagenes/botones_accion/btn_Exportar.JPG" OnClick="imgB_Listado_Click"
                ToolTip="Exportar" />
        </td>
        <td id="td_imprimir" runat="server" >
            <img src="../../Imagenes/botones_accion/linea_espacio.jpg" />
        </td>
        <td>
            <asp:ImageButton ID="imgB_Imprimir" runat="server" OnClientClick="IndicadorImprimir();"
                ImageUrl="~/Imagenes/botones_accion/btn_imprimir.jpg" ToolTip="Imprimir" />
        </td>
        <td id="td_salir" runat="server" >
            <img src="../../Imagenes/botones_accion/linea_espacio.jpg" />
        </td>
        <td>
            <asp:ImageButton ID="imgB_Cancelar" runat="server" OnClientClick="IndicadorCancelar();"
                ImageUrl="~/Imagenes/botones_accion/btn_salir.jpg" ImageAlign="Bottom" ToolTip="Cancelar" />
        </td>
        <td >
            <img src="../../Imagenes/botones_accion/linea_espacio.jpg" />
        </td>
    </tr>

    <asp:HiddenField ID="hdBoton" runat="server" />

    <script language="javascript" type="text/javascript">



//        Nuevo = 1,
//            Grabar = 2,
//            Editar = 3,
//            Eliminar = 4,
//            Imprimir = 5,
//            Listar = 6,
//            Salir = 7,
            
        function IndicadorListar() {
            document.getElementById('<%=hdBoton.ClientID%>').value = 9;
        
            
        }
        function IndicadorNuevo() 
        {
            document.getElementById('<%=hdBoton.ClientID%>').value = 1;
        }
        function IndicadorEditar() {
            chk = false;
            f = document.forms[0].elements
            for (x = 0; x < f.length; x++) {
                obj = f[x];
                if (obj.type == 'checkbox') {
                    if (obj.checked) {
                        chk = true;
                        document.getElementById('<%=hdBoton.ClientID%>').value = 3;                     
                        return true;
                        break;
                    }
                }
            }
            if (!chk) {
                alert('Seleccione un registro');

                return false;
            }
        }
        function IndicadorEliminar() {
            chk = false;
            f = document.forms[0].elements
            for (x = 0; x < f.length; x++) {
                obj = f[x];
                if (obj.type == 'checkbox') {
                    if (obj.checked) {
                        if (confirm('�Desea Eliminar el registro Seleccionado?')) {
                        }
                        else {
                            return false;
                        }
                        chk = true;
                        document.getElementById('<%=hdBoton.ClientID%>').value = 4;
                        return true;
                        break;
                    }
                }
            }
            if (!chk) {
                alert('Seleccione un registro');             
                return false;
            }
        }
        function IndicadorImprimir() {
            chk = false;
            f = document.forms[0].elements
            for (x = 0; x < f.length; x++) {
                obj = f[x];
                if (obj.type == 'checkbox') {

                    chk = true;
                    document.getElementById('<%=hdBoton.ClientID%>').value = 5;
                    return true;
                    break;

                }
            }
            if (!chk) {
                alert('Seleccione un registro');
                return false;
            }

        }
        function IndicadorGrabar() {
            document.getElementById('<%=hdBoton.ClientID%>').value = 2;           
        }
        function IndicadorListar() {
            document.getElementById('<%=hdBoton.ClientID%>').value = 6;            
        }
        function IndicadorCancelar() {
            document.getElementById('<%=hdBoton.ClientID%>').value = 7;           
            
        }
        
    </script>

</table>

