﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucTarifa.ascx.cs" Inherits="Script_Controles_ucTarifa" %>

 <table class="TabMantenimiento" style="width: 100%;">
        <tr>
                        <td class="BarraControl" colspan="5">
                            <strong>REGISTRAR TARIFA </strong>
                        </td>
                    </tr>
        <tr>
                <td>
                    <fieldset id="F0001" title="Servicio" class="inputFile">
                    <legend>Servicio</legend>
                        <div class="double">
                        <ajax:UpdatePanel ID="upServicio" runat="server">
                            <ContentTemplate> 
                                <table style="width: 66%;">
                                <tr>
                                    <td>
                                        Código
                                    </td>
                                    <td colspan="2">
                                        <asp:TextBox ID="TxtIdTarifaIns" runat="server" Width="141px" BackColor="#CCCCCC"></asp:TextBox>
                                    </td>
                                    <td>
                                        &nbsp;
                                    </td>
                                    <td>
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Servicio
                                    </td>
                                    <td>
                                        <asp:TextBox ID="TxtIdServicioIns" runat="server" Width="41px"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="TxtServicioDescripIns" runat="server" Width="249px" BackColor="#CCCCCC"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:ImageButton ID="ImbServicio" OnClientClick="OpenPopupServicio()" runat="server"
                                            ImageUrl="~/Script/Imagenes/b-obs.png" />
                                    </td>
                                    <td>
                                        <asp:RequiredFieldValidator ID="rvIdServicio" runat="server" ControlToValidate="TxtIdServicioIns"
                                            ErrorMessage="(*)" ValidationGroup="vgGrabar"></asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Operación
                                    </td>
                                    <td colspan="3">
                                        <asp:DropDownList ID="DplOperacionIns" runat="server" Height="26px" Width="293px">
                                        </asp:DropDownList>
                                    </td>
                                    <td>
                                        <asp:RequiredFieldValidator ID="rvIdOperacion" runat="server" ControlToValidate="DplOperacionIns"
                                            ErrorMessage="(*)" ValidationGroup="vgGrabar"></asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                            </table>
                            </ContentTemplate>
                            <Triggers>
                                <ajax:AsyncPostBackTrigger ControlID="BtnGrabar" EventName="Click" />
                                <ajax:AsyncPostBackTrigger ControlID="BtnNuevo" EventName="Click" />
                                <ajax:AsyncPostBackTrigger ControlID="BtnActualizar" EventName="Click" />
                            </Triggers>
                        </ajax:UpdatePanel>    
                        </div>
                    </fieldset>
                </td>
                <td>
                    <fieldset id="F7" title="Datos de la carga" class="inputFile">
                    <legend>Contrato</legend>
                        <div id="divLimites1" class="double3">
                        <ajax:UpdatePanel ID="UpContrato" runat="server">
                            <ContentTemplate>
                            <table style="width: 89%;">
                                <tr>
                                    <td>
                                        &nbsp;
                                    </td>
                                    <td>
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Tipo Cntrato
                                    </td>
                                    <td>
                                        <asp:TextBox ID="TxtTipoContrato" runat="server" Width="92px" BackColor="#CCCCCC"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Contrato
                                    </td>
                                    <td>
                                        <asp:TextBox ID="TxtIdContrato" runat="server" Width="92px" BackColor="#CCCCCC"></asp:TextBox>
                                    </td>
                                </tr>
                            </table>
                            </ContentTemplate>
                        </ajax:UpdatePanel>    
                        </div>
                    </fieldset>
                </td>
        </tr>
        <tr>
            <td>
                <fieldset id="F0002" title="Carga" class="inputFile">
                <legend>Datos de la Carga</legend>
                    <div class="double">
                    <ajax:UpdatePanel ID="upCarga" runat="server">
                          <ContentTemplate>
                            <table style="width: 97%;">
                            <tr>
                                <td>
                                    Condicion Carga
                                </td>
                                <td>
                                    <asp:DropDownList ID="DplCondicionCargaIns" runat="server" Height="26px" Width="293px">
                                    </asp:DropDownList>
                                </td>
                                <td>
                                    <asp:RequiredFieldValidator ID="rvCondicionCarga" runat="server" ControlToValidate="DplCondicionCargaIns"
                                        ErrorMessage="(*)" InitialValue="-1" ValidationGroup="vgGrabar"></asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Tño Cnt
                                </td>
                                <td>
                                    <asp:DropDownList ID="DplTamanoCntIns" runat="server" Height="26px" Width="293px">
                                    </asp:DropDownList>
                                </td>
                                <td>
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Ter.Port.
                                </td>
                                <td>
                                    <asp:DropDownList ID="DplTerminalPortuarioIns" runat="server" Height="20px" Width="293px">
                                    </asp:DropDownList>
                                </td>
                                <td>
                                    <asp:RequiredFieldValidator ID="rvTerminalPortuario" runat="server" ControlToValidate="DplTerminalPortuarioIns"
                                        ErrorMessage="(*)" InitialValue="-1" ValidationGroup="vgGrabar"></asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3">
                                    <div class="double">
                                        <table style="width: 100%;">
                                            <tr>
                                                <td>
                                                    <asp:CheckBox ID="ChkCargaPeligrosaIns" runat="server" Text="Carga Peligrosa" />
                                                </td>
                                                <td>
                                                    <asp:CheckBox ID="ChkObligatorioIns" runat="server" Text="Obligatorio" />
                                                </td>
                                                <td>
                                                    <asp:CheckBox ID="ChkTarifaSliIns" runat="server" Text="Tarifa SLI" />
                                                </td>
                                                <td>
                                                    <asp:CheckBox ID="ChkAlmacenajeIns" runat="server" Text="Almacenaje" />
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </td>
                            </tr>
                        </table>
                           </ContentTemplate>
                          <Triggers>
                              <ajax:AsyncPostBackTrigger ControlID="BtnGrabar" EventName="Click" />
                              <ajax:AsyncPostBackTrigger ControlID="BtnActualizar" EventName="Click" />
                              <ajax:AsyncPostBackTrigger ControlID="BtnNuevo" EventName="Click" />
                          </Triggers>
                     </ajax:UpdatePanel>
                    </div>
                </fieldset>
                <br />
                    <ajax:UpdatePanel ID="upCosto" runat="server">
                          <ContentTemplate>
                        <table style="width: 66%;">
                            <tr>
                                <td>
                                    Descripcion &nbsp;
                                </td>
                                <td>
                                    &nbsp;<asp:TextBox ID="TxTarifaDescripIns" runat="server" Height="58px" 
                                        TextMode="MultiLine" Width="289px"></asp:TextBox></td>
                            </tr>
                        </table>
                        </ContentTemplate>
                          <Triggers>
                              <ajax:AsyncPostBackTrigger ControlID="BtnGrabar" EventName="Click" />
                              <ajax:AsyncPostBackTrigger ControlID="BtnActualizar" EventName="Click" />
                              <ajax:AsyncPostBackTrigger ControlID="BtnNuevo" EventName="Click" />
                          </Triggers>
                     </ajax:UpdatePanel>
            </td>
            <td rowspan="3">
                <fieldset id="F4" title="Configuración Tarifa" class="inputFile">
                <legend>Configuración</legend>
                    <div class="double">
                    <ajax:UpdatePanel ID="upTarifa" runat="server">
                          <ContentTemplate>
                        <table style="width: 100%;">
                            <tr>
                                <td>
                                    Modalidad
                                </td>
                                <td>
                                    <asp:DropDownList ID="DplModalidadIns" runat="server" Height="26px" Width="293px">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Moneda
                                </td>
                                <td>
                                    <asp:DropDownList ID="DplMonedaIns" runat="server" Height="26px" Width="293px">
                                    </asp:DropDownList>
                                    <asp:RequiredFieldValidator ID="rvIdMoneda" runat="server" ControlToValidate="DplMonedaIns"
                                        ErrorMessage="(*)" ValidationGroup="vgGrabar" InitialValue="-1"></asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Monto
                                </td>
                                <td>
                                    <asp:TextBox ID="TxtMontoIns" onkeyup="validaFloat();" runat="server" Width="92px"></asp:TextBox>
                                    <%--       <ajaxToolkit:MaskedEditExtender ID="meMonto" runat="server" 
                                                CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" 
                                                CultureDateFormat="" CultureDatePlaceholder="" CultureDecimalPlaceholder="" 
                                                CultureThousandsPlaceholder="" CultureTimePlaceholder="" Enabled="True" 
                                                Mask="99999.99" MaskType="Number" TargetControlID="TxtMontoIns" 
                                                AutoComplete="False">
                                            </ajaxToolkit:MaskedEditExtender>--%>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Dias Lib.
                                </td>
                                <td>
                                    <asp:TextBox ID="TxtDiasLibresIns" runat="server" Width="92px"></asp:TextBox>
                                    <ajaxToolkit:MaskedEditExtender ID="meDiasLibres" runat="server" CultureAMPMPlaceholder=""
                                        CultureCurrencySymbolPlaceholder="" CultureDateFormat="" CultureDatePlaceholder=""
                                        CultureDecimalPlaceholder="" CultureThousandsPlaceholder="" CultureTimePlaceholder=""
                                        Enabled="True" Mask="99999" MaskType="Number" TargetControlID="TxtDiasLibresIns"
                                        AutoComplete="False">
                                    </ajaxToolkit:MaskedEditExtender>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Costo
                                </td>
                                <td>
                                    <asp:TextBox ID="TxtCostoIns" onkeyup="validaFloat();" runat="server" Width="92px"></asp:TextBox>
                                </td>
                            </tr>
                        </table>
                        </ContentTemplate>
                          <Triggers>
                              <ajax:AsyncPostBackTrigger ControlID="BtnActualizar" EventName="Click" />
                              <ajax:AsyncPostBackTrigger ControlID="BtnGrabar" EventName="Click" />
                              <ajax:AsyncPostBackTrigger ControlID="BtnNuevo" EventName="Click" />
                          </Triggers>
                    </ajax:UpdatePanel>
                    </div>
                </fieldset>
                <br />
                <fieldset id="F5" title="Limites ">
                <legend>Límites</legend>
                    <div id="divLimites">
                    <ajax:UpdatePanel ID="upLimites" runat="server">
                          <ContentTemplate>
                        <table style="width: 401px">
                            <tr>
                                <td>
                                    Limites
                                </td>
                                <td>
                                    <asp:CheckBox ID="ChkLimiteIns" onclick="ValidaMontos()" runat="server" />
                                </td>
                                <td>
                                    &nbsp;
                                </td>
                                <td>
                                    Lim.Neg
                                </td>
                                <td>
                                    <asp:CheckBox ID="ChkLimiteNegoIns" runat="server" onclick="ValidaMontosNegociado()" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Maximo
                                </td>
                                <td>
                                    <asp:TextBox ID="TxtMontoMaximoIns" onkeyup="validaFloat();" runat="server" BackColor="#CCCCCC"
                                        Width="92px" ValidationGroup="vgLimites"></asp:TextBox>
                                </td>
                                <td>
                                    <asp:RequiredFieldValidator ID="rvMontoMaximo" runat="server" ErrorMessage="(*)"
                                        ValidationGroup="vgLimites" ControlToValidate="TxtMontoMaximoIns"></asp:RequiredFieldValidator>
                                </td>
                                <td>
                                    Maximo
                                </td>
                                <td>
                                    <asp:TextBox ID="TxtMontoMaximoNegIns" onkeyup="validaFloat();" runat="server" BackColor="#CCCCCC"
                                        ValidationGroup="vgLimites" Width="92px"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Minimo
                                </td>
                                <td>
                                    <asp:TextBox ID="TxtMontoMinimoIns" onkeyup="validaFloat();" runat="server" BackColor="#CCCCCC"
                                        Width="92px" ValidationGroup="vgLimites"></asp:TextBox>
                                </td>
                                <td>
                                    <asp:RequiredFieldValidator ID="rvMontoMinimo" runat="server" ErrorMessage="(*)"
                                        ValidationGroup="vgLimites" ControlToValidate="TxtMontoMinimoIns"></asp:RequiredFieldValidator>
                                </td>
                                <td>
                                    Minimo
                                </td>
                                <td>
                                    <asp:TextBox ID="TxtMontoMinimoNegIns" onkeyup="validaFloat();" runat="server" BackColor="#CCCCCC"
                                        ValidationGroup="vgLimites" Width="92px"></asp:TextBox>
                                </td>
                            </tr>
                        </table>
                        </ContentTemplate>
                          <Triggers>
                              <ajax:AsyncPostBackTrigger ControlID="BtnActualizar" EventName="Click" />
                              <ajax:AsyncPostBackTrigger ControlID="BtnGrabar" EventName="Click" />
                              <ajax:AsyncPostBackTrigger ControlID="BtnNuevo" EventName="Click" />
                          </Triggers>
                        </ajax:UpdatePanel>
                    </div>
                </fieldset>
                <br />
                <fieldset id="F6" title="Rango dias">
                <legend>Rango Días</legend>
                    <div id="divLimites0">
                    <ajax:UpdatePanel ID="upRangoDias" runat="server">
                          <ContentTemplate>
                        <table style="width: 82%;">
                            <tr>
                                <td>
                                    R. Dias
                                </td>
                                <td colspan="3">
                                    <asp:CheckBox ID="ChkRangoDiasIns" onclick="validarRangoDias()" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Dias
                                </td>
                                <td>
                                    <asp:TextBox ID="TxtRangoDias1Ins" runat="server" BackColor="#CCCCCC" Width="69px"></asp:TextBox>
                                </td>
                                <td>
                                    <asp:TextBox ID="TxtRangoDias2Ins" runat="server" BackColor="#CCCCCC" Width="69px"></asp:TextBox>
                                </td>
                                <td>
                                    <asp:TextBox ID="TxtRangoDias3Ins" runat="server" BackColor="#CCCCCC" Width="69px"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Monto
                                </td>
                                <td>
                                    <asp:TextBox ID="TxtRangoMonto1Ins" onkeyup="validaFloat();" runat="server" BackColor="#CCCCCC"
                                        Width="69px"></asp:TextBox>
                                </td>
                                <td>
                                    <asp:TextBox ID="TxtRangoMonto2Ins" onkeyup="validaFloat();" runat="server" BackColor="#CCCCCC"
                                        Width="69px"></asp:TextBox>
                                </td>
                                <td>
                                    <asp:TextBox ID="TxtRangoMonto3Ins" onkeyup="validaFloat();" runat="server" BackColor="#CCCCCC"
                                        Width="69px"></asp:TextBox>
                                </td>
                            </tr>
                        </table>
                              <ajaxToolkit:MaskedEditExtender ID="meDiasLibres1" runat="server" 
                                  AutoComplete="False" CultureAMPMPlaceholder="" 
                                  CultureCurrencySymbolPlaceholder="" CultureDateFormat="" 
                                  CultureDatePlaceholder="" CultureDecimalPlaceholder="" 
                                  CultureThousandsPlaceholder="" CultureTimePlaceholder="" Enabled="True" 
                                  Mask="99999" MaskType="Number" TargetControlID="TxtRangoDias1Ins">
                              </ajaxToolkit:MaskedEditExtender>
                              <ajaxToolkit:MaskedEditExtender ID="meDiasLibres2" runat="server" 
                                  AutoComplete="False" CultureAMPMPlaceholder="" 
                                  CultureCurrencySymbolPlaceholder="" CultureDateFormat="" 
                                  CultureDatePlaceholder="" CultureDecimalPlaceholder="" 
                                  CultureThousandsPlaceholder="" CultureTimePlaceholder="" Enabled="True" 
                                  Mask="99999" MaskType="Number" TargetControlID="TxtRangoDias2Ins">
                              </ajaxToolkit:MaskedEditExtender>
                              <ajaxToolkit:MaskedEditExtender ID="meDiasLibres3" runat="server" 
                                  AutoComplete="False" CultureAMPMPlaceholder="" 
                                  CultureCurrencySymbolPlaceholder="" CultureDateFormat="" 
                                  CultureDatePlaceholder="" CultureDecimalPlaceholder="" 
                                  CultureThousandsPlaceholder="" CultureTimePlaceholder="" Enabled="True" 
                                  Mask="99999" MaskType="Number" TargetControlID="TxtRangoDias3Ins">
                              </ajaxToolkit:MaskedEditExtender>
                        </ContentTemplate>
                          <Triggers>
                              <ajax:AsyncPostBackTrigger ControlID="BtnActualizar" EventName="Click" />
                              <ajax:AsyncPostBackTrigger ControlID="BtnGrabar" EventName="Click" />
                              <ajax:AsyncPostBackTrigger ControlID="BtnNuevo" EventName="Click" />
                          </Triggers>
                        </ajax:UpdatePanel>
                    </div>
                </fieldset>
                <br />
            </td>
        </tr>
        <tr>
            <td align="center">
                            <ajax:UpdatePanel ID="upGrabar" runat="server">
                    <ContentTemplate>
                        <asp:Button ID="BtnGrabar" runat="server" OnClick="BtnGrabar_Click" 
                            OnClientClick="ActivarGrabar();" Text="Grabar" />
                        &nbsp;<asp:Button ID="BtnSalir" runat="server" Text="Cerrar" OnClientClick="ValidarCerrar();" />
                    </ContentTemplate>
                    <Triggers>
                        <ajax:AsyncPostBackTrigger ControlID="BtnGrabar" EventName="Click">
                        </ajax:AsyncPostBackTrigger>
                        <ajax:AsyncPostBackTrigger ControlID="BtnActualizar" EventName="Click">
                        </ajax:AsyncPostBackTrigger>
                    </Triggers>
                </ajax:UpdatePanel>
                
                
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                <br />
            </td>
            <td>
                
                
                <asp:HiddenField ID="hdImbCosto" runat="server" />
                <asp:HiddenField ID="hdImbEmbalaje" runat="server" />
                <asp:HiddenField ID="hdImbServicio" runat="server" />
                <asp:HiddenField ID="hdImbTipoContenedor" runat="server" />
                <asp:HiddenField ID="hdBtnTipoCarga" runat="server" />
                <asp:HiddenField ID="hdBtnCerrar" runat="server" />
                <asp:HiddenField ID="hdBtnGrabar" runat="server" />
            </td>
        </tr>
        <asp:Button ID="btnOpenProxyInser" Style="display: none;" runat="server" Text="[Open Proxy]" />
    </table>

    <script language="javascript" type="text/javascript">

        function ActivarNuevo() {

            document.getElementById("<%=BtnGrabar.ClientID%>").disabled = '';


        }

        function ActivarActualizar() {

          

        }

    

        function ValidarCerrar() {


            document.getElementById("<%=hdBtnCerrar.ClientID%>").value = '0';           


        }



        function ValidarCheck() {
            if (document.getElementById("<%= ChkLimiteIns.ClientID%>").checked == true) {
                if ((document.getElementById("<%=TxtMontoMinimoIns.ClientID%>").value = '') ||
                    (document.getElementById("<%=TxtMontoMinimoIns.ClientID%>").value = '')) {
                    alert('Ingrese los valores Minimos o Maximos');
                    document.getElementById("<%=TxtMontoMinimoIns.ClientID%>").focus();
                    return false;
                }
            }

            if (document.getElementById("<%= ChkLimiteNegoIns.ClientID%>").checked == true) {
                if ((document.getElementById("<%=TxtMontoMinimoNegIns.ClientID%>").value = '') ||
                    (document.getElementById("<%=TxtMontoMinimoNegIns.ClientID%>").value = '')) {
                    alert('Ingrese los valores Minimo o Maximo Negociado');
                    document.getElementById("<%=TxtMontoMinimoNegIns.ClientID%>").focus();
                    return false;
                }
            }


        }
        function ValidaMontos() {

            if (document.getElementById("<%= ChkLimiteIns.ClientID%>").checked == true) {

                document.getElementById("<%=TxtMontoMinimoIns.ClientID%>").disabled = '';
                document.getElementById("<%=TxtMontoMaximoIns.ClientID%>").disabled = '';
                document.getElementById("<%=TxtMontoMinimoIns.ClientID%>").style.backgroundColor = '';
                document.getElementById("<%=TxtMontoMaximoIns.ClientID%>").style.backgroundColor = '';
                document.getElementById("<%=TxtMontoMaximoIns.ClientID%>").focus();

            } else {
                document.getElementById("<%=TxtMontoMinimoIns.ClientID%>").style.backgroundColor = '#CCCCCC';
                document.getElementById("<%=TxtMontoMaximoIns.ClientID%>").style.backgroundColor = '#CCCCCC';
                document.getElementById("<%=TxtMontoMinimoIns.ClientID%>").disabled = '1';
                document.getElementById("<%=TxtMontoMaximoIns.ClientID%>").disabled = '1';

                document.getElementById("<%=TxtMontoMinimoIns.ClientID%>").value = '';
                document.getElementById("<%=TxtMontoMaximoIns.ClientID%>").value = '';

            }

        }

        function ValidaMontosNegociado() {

            if (document.getElementById("<%= ChkLimiteNegoIns.ClientID%>").checked == true) {

                document.getElementById("<%=TxtMontoMaximoNegIns.ClientID%>").disabled = '';
                document.getElementById("<%=TxtMontoMinimoNegIns.ClientID%>").disabled = '';
                document.getElementById("<%=TxtMontoMaximoNegIns.ClientID%>").style.backgroundColor = '';
                document.getElementById("<%=TxtMontoMinimoNegIns.ClientID%>").style.backgroundColor = '';
                document.getElementById("<%=TxtMontoMaximoNegIns.ClientID%>").focus();

            } else {
                document.getElementById("<%=TxtMontoMinimoNegIns.ClientID%>").style.backgroundColor = '#CCCCCC';
                document.getElementById("<%=TxtMontoMaximoNegIns.ClientID%>").style.backgroundColor = '#CCCCCC';
                document.getElementById("<%=TxtMontoMinimoNegIns.ClientID%>").disabled = '1';
                document.getElementById("<%=TxtMontoMaximoNegIns.ClientID%>").disabled = '1';

                document.getElementById("<%=TxtMontoMinimoNegIns.ClientID%>").value = '';
                document.getElementById("<%=TxtMontoMaximoNegIns.ClientID%>").value = '';

            }

        }

        function validarRangoDias() {
            if (document.getElementById("<%= ChkRangoDiasIns.ClientID%>").checked == true) {

                document.getElementById("<%=TxtRangoDias1Ins.ClientID%>").disabled = '';
                document.getElementById("<%=TxtRangoDias2Ins.ClientID%>").disabled = '';
                document.getElementById("<%=TxtRangoDias3Ins.ClientID%>").disabled = '';

                document.getElementById("<%=TxtRangoMonto1Ins.ClientID%>").disabled = '';
                document.getElementById("<%=TxtRangoMonto2Ins.ClientID%>").disabled = '';
                document.getElementById("<%=TxtRangoMonto3Ins.ClientID%>").disabled = '';

                document.getElementById("<%=TxtRangoDias1Ins.ClientID%>").style.backgroundColor = '';
                document.getElementById("<%=TxtRangoDias2Ins.ClientID%>").style.backgroundColor = '';
                document.getElementById("<%=TxtRangoDias3Ins.ClientID%>").style.backgroundColor = '';
                document.getElementById("<%=TxtRangoMonto1Ins.ClientID%>").style.backgroundColor = '';
                document.getElementById("<%=TxtRangoMonto2Ins.ClientID%>").style.backgroundColor = '';
                document.getElementById("<%=TxtRangoMonto3Ins.ClientID%>").style.backgroundColor = '';




                document.getElementById("<%=TxtRangoDias1Ins.ClientID%>").focus();

            } else {
                document.getElementById("<%=TxtRangoDias1Ins.ClientID%>").disabled = '1';
                document.getElementById("<%=TxtRangoDias2Ins.ClientID%>").disabled = '1';
                document.getElementById("<%=TxtRangoDias3Ins.ClientID%>").disabled = '1';

                document.getElementById("<%=TxtRangoMonto1Ins.ClientID%>").disabled = '1';
                document.getElementById("<%=TxtRangoMonto2Ins.ClientID%>").disabled = '1';
                document.getElementById("<%=TxtRangoMonto3Ins.ClientID%>").disabled = '1';

                document.getElementById("<%=TxtRangoDias1Ins.ClientID%>").style.backgroundColor = '#CCCCCC';
                document.getElementById("<%=TxtRangoDias2Ins.ClientID%>").style.backgroundColor = '#CCCCCC';
                document.getElementById("<%=TxtRangoDias3Ins.ClientID%>").style.backgroundColor = '#CCCCCC';
                document.getElementById("<%=TxtRangoMonto1Ins.ClientID%>").style.backgroundColor = '#CCCCCC';
                document.getElementById("<%=TxtRangoMonto2Ins.ClientID%>").style.backgroundColor = '#CCCCCC';
                document.getElementById("<%=TxtRangoMonto3Ins.ClientID%>").style.backgroundColor = '#CCCCCC';

                document.getElementById("<%=TxtRangoDias1Ins.ClientID%>").value = '';
                document.getElementById("<%=TxtRangoDias2Ins.ClientID%>").value = '';
                document.getElementById("<%=TxtRangoDias3Ins.ClientID%>").value = '';
                document.getElementById("<%=TxtRangoMonto1Ins.ClientID%>").value = '';
                document.getElementById("<%=TxtRangoMonto2Ins.ClientID%>").value = '';
                document.getElementById("<%=TxtRangoMonto3Ins.ClientID%>").svalue = '';


            }
        }


        function validaFloat() {
            numero = document.getElementById("<%=TxtMontoIns.ClientID%>").value;
            if (!/^([0-9])*[.]?[0-9]*$/.test(numero)) {

                alert("El valor " + numero + " no es un número");

            }
        }

        function ActivarGrabar() {
            
            document.getElementById("<%=hdBtnGrabar.ClientID%>").value = '1';    
        }


    
    </script>