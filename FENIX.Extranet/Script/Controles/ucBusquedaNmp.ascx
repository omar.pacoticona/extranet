<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucBusquedaNmp.ascx.cs" Inherits="ucBusquedaNmp" %>
<%@ Register Assembly="QNET.Web.UI" Namespace="QNET.Web.UI.Controls"  TagPrefix="FENIX" %>


<table  class="TabMantenimiento"  cellpadding="0" cellspacing="0" style="width: 100%;">

 <tr class="PanelPopupBusquedaBarra">
       
        <td colspan="2" >
            BUSCAR INFORMACI�N      
        </td>
        <td  align="right">
            <asp:ImageButton ID="imgPopBusqueda" runat="server"  OnClientClick="return Cancelar();" ImageUrl="~/Script/Imagenes/IconButton/Delete_Icon.png" />
          </td>
    </tr>
    
    <tr>
        <td>
        
            <asp:Label ID="LblDescripcion" runat="server" Text="Descripci�n"></asp:Label>
        </td>
        <td >
            <asp:TextBox ID="TxtManifiesto" runat="server" Width="58px"></asp:TextBox>
            <asp:TextBox ID="TxtDescripcion" runat="server"></asp:TextBox>
            <asp:ImageButton ID="imgbBuscar"  OnClientClick="return Consultar();" 
                runat="server" ImageUrl="~/Script/Imagenes/b-obs.png" 
                OnClick="imgbBuscar_Click" />
        </td>
        <td >
            <asp:Label ID="LblTitulo" Text="Titulo" runat="server" Visible="False"></asp:Label>
            <asp:HiddenField ID="hdDescripcion" runat="server" />
            </td>
    </tr>
    <tr>
        <td>
            <asp:Label ID="LblCodigo" runat="server" Text="C�digo"></asp:Label>
        </td>
        <td >
            <asp:TextBox ID="TxtCodigo" runat="server"></asp:TextBox>
        </td>
        <td >
            <asp:HiddenField ID="hdBuscar" runat="server" />
            <asp:HiddenField ID="hdConsultar" runat="server" />
            <asp:HiddenField ID="hdCodigo"  runat="server"/>
            
            <asp:HiddenField ID="hdDescripcion1" runat="server" />
            <asp:HiddenField ID="hdDescripcion2" runat="server" />
            <asp:HiddenField ID="hdDescripcion3" runat="server" />
            <asp:HiddenField ID="hdDescripcion4" runat="server" />
            <asp:HiddenField ID="hdDescripcion5" runat="server" />
            
        </td>
    </tr>
    <tr>
        <td colspan="3">
            <asp:Panel ID="plScrol"  runat="server" BackColor="White" Height="250px" ScrollBars="Vertical"
                Width="100%">
                <asp:GridView Width="100%" ID="GrvListado" SkinID="GrdListadoTabUc" runat="server" DataKeyNames="iIdValor,sDescripcion,sDescripcion1,sDescripcion2,sDescripcion3,sDescripcion4,sDescripcion5"
                     CellPadding="4" 
                    AutoGenerateColumns="False" OnRowDataBound="GrvListado_RowDataBound" >
                   
                    <Columns>                        
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:CheckBox ID="ChkSeleccionar" runat="server" />
                            </ItemTemplate>  
                          <ItemStyle Width="5%" />                         
                        </asp:TemplateField>
                        <asp:BoundField DataField="iIdValor" HeaderText="C�digo">
                        <ItemStyle Width="10px" />
                        </asp:BoundField>
                        <asp:BoundField DataField="sDescripcion" HeaderText="Descripci�n">
                        </asp:BoundField>
                        <asp:BoundField  DataField="sDescripcion1" HeaderText="Desc1">
                        </asp:BoundField>
                        <asp:BoundField DataField="sDescripcion2" HeaderText="Desc2">
                        </asp:BoundField>
                        <asp:BoundField DataField="sDescripcion3" HeaderText="Desc3">
                        </asp:BoundField>
                        <asp:BoundField DataField="sDescripcion4" HeaderText="Desc4">
                        </asp:BoundField>
                        <asp:BoundField DataField="sDescripcion5" HeaderText="Desc5">
                        </asp:BoundField>
                    </Columns>                                   
                </asp:GridView>                                                                             
            </asp:Panel>             
        </td>
    </tr>
    <tr>
        <td colspan="3" style="height: 26px">
            <asp:Button ID="BtnCancelar" runat="server" Style="position: relative; left: 164px;
                top: 0px;" Text="Cancelar" OnClientClick="Cancelar();" />
            <asp:Button ID="btnConsulta" runat="server" Style="position: relative; left: 14px;
                top: 0px;" Text="Aceptar" OnClientClick="Aceptar();" OnClick="btnConsulta_Click"  />
        </td>
    </tr>

    <script language="javascript" type="text/javascript">
        function fc_PressKeyBus() {
            if (event.keyCode == 13) {
                document.getElementById("<%=imgbBuscar.ClientID%>").click();
                return false;
            }
            else {
                return true;
            }
        }

        function fc_PressKeyBusGrid() {
            if (event.keyCode == 13) {
                document.getElementById("<%=btnConsulta.ClientID%>").click();
                return false;
            }
            else {
                return true;
            }
        }
    
        function Cancelar() {
            document.getElementById('<%=hdBuscar.ClientID%>').value = '0';
        }

        function Aceptar() {
            chk = false;
            f = document.forms[0].elements
            for (x = 0; x < f.length; x++) {
                obj = f[x];
                if (obj.type == 'checkbox') {
                    if (obj.checked) {
                        document.getElementById('<%=hdBuscar.ClientID%>').value = '1';                                           
                        return true;
                        break;
                    }
                }
            }
//            if (!chk) {
//                alert('Seleccione un registro');
//                return false;
//            }
            
        }
              
        var objFilaAnt = null;
        var backgroundColorFilaAnt = "";

        function fc_SeleccionaFilaSimpleUc(objFila, chkID, p1, p2, p3, p4, p5, p6, p7) {
            try {
                if (objFilaAnt != null) {
                    objFilaAnt.style.backgroundColor = backgroundColorFilaAnt;
                }
                objFilaAnt = objFila;
                backgroundColorFilaAnt = objFila.style.backgroundColor;
                objFila.style.backgroundColor = "#c4e4ff";

                document.getElementById('<%=hdCodigo.ClientID%>').value = p1;
                document.getElementById('<%=hdDescripcion.ClientID%>').value = p2;
                document.getElementById('<%=hdDescripcion1.ClientID%>').value = p3;
                document.getElementById('<%=hdDescripcion2.ClientID%>').value = p4;
                document.getElementById('<%=hdDescripcion3.ClientID%>').value = p5;
                document.getElementById('<%=hdDescripcion4.ClientID%>').value = p6;
                document.getElementById('<%=hdDescripcion5.ClientID%>').value = p7;
                f = document.forms[0].elements;
                for (x = 0; x < f.length; x++) {
                    obj = f[x];
                    if (obj.type == 'checkbox' && obj.id.replace("<%=GrvListado.ClientID %>", "") != obj.id) {
                        if (obj.id != chkID)
                            obj.checked = false;
                    }
                }
                chkID.checked = true;
            }
            catch (e) {
                error = e.message;
            }
        }



        function fc_SeleccionaFilaSimpleUc_DblClik(rowIndex, chkID, p1, p2, p3, p4, p5, p6, p7) {
            oTabla = document.getElementById("<%=GrvListado.ClientID %>");
            rowIndex = rowIndex * 1;
            rowIndex = rowIndex + 1;
            for (i = 1; i < oTabla.rows.length; i++) {
                objFila = oTabla.rows[i];
                if (i == rowIndex) {
                    objFila.style.backgroundColor = "#c4e4ff";
                    chk = document.getElementById(chkID);
                    chk.checked = !chk.checked;
                    //pValue=rowIndex+"";
                    if (!chk.checked) {
                        objFila.style.backgroundColor = "white";
                        pValue = p1 = p2 = p3 = p4 = p5 = p6 = p7 = "";
                    }

                    /*previsualizar los datos*/
                    document.getElementById('<%=hdCodigo.ClientID%>').value = p1;
                    document.getElementById('<%=hdDescripcion.ClientID%>').value = p2;
                    document.getElementById('<%=hdDescripcion1.ClientID%>').value = p3;
                    document.getElementById('<%=hdDescripcion2.ClientID%>').value = p4;
                    document.getElementById('<%=hdDescripcion3.ClientID%>').value = p5;
                    document.getElementById('<%=hdDescripcion4.ClientID%>').value = p6;
                    document.getElementById('<%=hdDescripcion5.ClientID%>').value = p7;
                    /*cerramos popub*/
                    document.getElementById('<%=hdBuscar.ClientID%>').value = '0';
                    
                    /*inactivamos todos los checks*/
                    f = document.forms[0].elements;
                    for (x = 0; x < f.length; x++) {
                        obj = f[x];
                        if (obj.type == 'checkbox' && obj.id.replace("<%=GrvListado.ClientID %>", "") != obj.id) {
                            if (obj.id != chkID)
                                obj.checked = false;
                        }
                    }
                } else {
                    objFila.style.backgroundColor = "white";
                }
            }
        }
        
      
        function Consultar() {
            document.getElementById('<%=hdConsultar.ClientID%>').value = 1;
        }
       

  
        

        
    </script>
 
</table>


