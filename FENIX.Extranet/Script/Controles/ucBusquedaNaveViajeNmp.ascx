<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucBusquedaNaveViajeNmp.ascx.cs" Inherits="ucBusquedaNavViaNmp" %>


<table class="TabMantenimiento" cellpadding="0" cellspacing="0"  
    style="width: 100%; height:100%;">

    <tr class="PanelPopupBusquedaBarra" >
       <td colspan="3" align="left" >
        BUSCAR NAVE - VIAJE - RUMBO
             </td>
        <td  align="right" >
        <asp:ImageButton ID="imgPopBusqueda" runat="server"  OnClientClick="return Cancelar_Nvr();"  ImageUrl="~/Script/Imagenes/IconButton/Delete_Icon.png" />
             </td>
    </tr>

    <tr style="border:1px;" >
       
        <td style="border:1px;" colspan="4">
            <asp:Label ID="lblTitulo" Text="Titulo" runat="server"></asp:Label>
            <asp:HiddenField ID="hdDescripcion" runat="server" />
        </td>
    </tr>
    <tr>
        <td>
            Nave/Viaje/Rumbo</td>
        <td >
            <asp:TextBox ID="TxtNave" runat="server"></asp:TextBox>
            <asp:TextBox ID="TxtViaje" runat="server" Width="30px"> </asp:TextBox>
            <asp:TextBox ID="TxtRumbo" runat="server" Width="30px"></asp:TextBox>
        </td>
        <td >
            <asp:ImageButton ID="imgbBuscar"  OnClientClick="return ConsultarNvr();" 
                runat="server" ImageUrl="~/Script/Imagenes/b-obs.png" 
                OnClick="imgbBuscar_Click" />
            <asp:HiddenField ID="hdCodigo"  runat="server"/>
            
        </td>
    </tr>
    <tr>
        <td>
            Manifiesto</td>
        <td >
            <asp:TextBox ID="TxtAnoManifiesto"   runat="server" Width="41px"></asp:TextBox>
            <asp:TextBox ID="TxtNumeroManifiesto" runat="server" Width="84px"></asp:TextBox>
        </td>
        <td >
            <asp:HiddenField ID="hdBuscar" runat="server" />
            <asp:HiddenField ID="hdConsultar" runat="server" />
            <asp:HiddenField ID="hdDescripcion1" runat="server" />
            <asp:HiddenField ID="hdDescripcion2" runat="server" />
            <asp:HiddenField ID="hdDescripcion3" runat="server" />
            <asp:HiddenField ID="hdDescripcion4" runat="server" />
            <asp:HiddenField ID="hdDescripcion5" runat="server" />
            
        </td>
    </tr>
    <tr>
        <td colspan="4">
            &nbsp;<asp:Panel CssClass="CssBordeGridPop" ID="plScrol" runat="server" Height="300px" ScrollBars="Vertical"
                Width="450px">
                <asp:GridView Width="80%" ID="GrvListado" runat="server" DataKeyNames="iIdValor,sDescripcion,sDescripcion1,sDescripcion2,sDescripcion3,sDescripcion4,sDescripcion5"
                     CellPadding="4" 
                    AutoGenerateColumns="False" OnRowDataBound="GrvListado_RowDataBound" >
                   
                    <Columns>                        
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:CheckBox ID="ChkSeleccionar_Nvr" runat="server" />
                            </ItemTemplate>  
                          <ItemStyle Width="5%" />                         
                        </asp:TemplateField>
                        <asp:BoundField DataField="iIdValor" HeaderText="C�digo">
                        <ItemStyle Width="10px" />
                        </asp:BoundField>
                        <asp:BoundField DataField="sDescripcion" HeaderText="Descripci�n">
                        </asp:BoundField>
                        <asp:BoundField  DataField="sDescripcion1" HeaderText="Desc1">
                        </asp:BoundField>
                        <asp:BoundField DataField="sDescripcion3" HeaderText="Desc3">
                        </asp:BoundField>
                        <asp:BoundField DataField="sDescripcion2" HeaderText="Desc2">
                        </asp:BoundField>
                        <asp:BoundField DataField="sDescripcion4" HeaderText="Desc4">
                        </asp:BoundField>
                        <asp:BoundField DataField="sDescripcion5" HeaderText="Desc5">
                        </asp:BoundField>
                    </Columns>
                    <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="false" ForeColor="#333333" />                    
                </asp:GridView>                                                                             
            </asp:Panel>
        </td>
    </tr>
    <tr align="center">
        <td  colspan="4">
            <asp:Button ID="btnConsulta" runat="server" Text="Aceptar" 
                OnClientClick="return Aceptar_Nvr();" OnClick="btnConsulta_Click" Width="90px"  />
        &nbsp;<asp:Button ID="BtnCancelar" runat="server" Text="Cancelar" 
                OnClientClick="Cancelar_Nvr();" Width="90px" />
        </td>
    </tr>

    <script language="javascript" type="text/javascript">
        function fc_PressKeyNav() {
            if (event.keyCode == 13) {
                document.getElementById("<%=imgbBuscar.ClientID%>").click();
                return false;
            }
            else {
                return true;
            }
        }

        function fc_PressKeyNavGrid(event,sObjeto) {
            if (event.keyCode == 13) {
                if (sObjeto == 'TxtNave') {
                    document.getElementById("<%=btnConsulta.ClientID%>").click();
                    document.getElementById("<%=TxtViaje.ClientID%>").focus();
                }
                else if (sObjeto == 'TxtViaje') {
                    document.getElementById("<%=btnConsulta.ClientID%>").click();
                    document.getElementById("<%=TxtRumbo.ClientID%>").focus();
                }
                else if (sObjeto == 'TxtRumbo') {
                    document.getElementById("<%=btnConsulta.ClientID%>").click();
                    document.getElementById("<%=TxtViaje.ClientID%>").focus();
                }
                else if (sObjeto == 'TxtAnoManifiesto') {
                    document.getElementById("<%=btnConsulta.ClientID%>").click();
                    document.getElementById("<%=TxtAnoManifiesto.ClientID%>").focus();
                }
                else if (sObjeto == 'TxtNumeroManifiesto') {
                    document.getElementById("<%=btnConsulta.ClientID%>").click();
                    document.getElementById("<%=TxtNumeroManifiesto.ClientID%>").focus();
                }

                return false;
            }
            else {
                return false;
            }
        }
    
        function Cancelar_Nvr() {
            document.getElementById('<%=hdBuscar.ClientID%>').value = '0';
        }

        function Aceptar_Nvr() {
            chk = false;
            f = document.forms[0].elements
            for (x = 0; x < f.length; x++) {
                obj = f[x];
                if (obj.type == 'checkbox') {
                    if (obj.checked) {
                        document.getElementById('<%=hdBuscar.ClientID%>').value = '1';
                        return true;
                        break;
                    }
                }
            }
            if (!chk) {
                alert('Seleccione un registro');
                return false;
            }
            
        }
              
        var objFilaAnt = null;
        var backgroundColorFilaAnt = "";

        function fc_SeleccionaFilaSimpleUc_Nvr(rowIndex, chkID, p1, p2, p3, p4, p5, p6, p7) {
            oTabla = document.getElementById("<%=GrvListado.ClientID %>");
            rowIndex = rowIndex * 1;
            rowIndex = rowIndex + 1;
            for (i = 1; i < oTabla.rows.length; i++) {
                objFila = oTabla.rows[i];
                if (i == rowIndex) {
                    objFila.style.backgroundColor = "#c4e4ff";
                    chk = document.getElementById(chkID);
                    chk.checked = !chk.checked;
                    //pValue=rowIndex+"";
                    if (!chk.checked) {
                        objFila.style.backgroundColor = "white";
                        pValue = p1 = p2 = p3 = p4 = p5 = p6 = p7 = "";
                    }
                  
                    /*previsualizar los datos*/
                    document.getElementById('<%=hdCodigo.ClientID%>').value = p1;
                    document.getElementById('<%=hdDescripcion.ClientID%>').value = p2;
                    document.getElementById('<%=hdDescripcion1.ClientID%>').value = p3;
                    document.getElementById('<%=hdDescripcion2.ClientID%>').value = p4;
                    document.getElementById('<%=hdDescripcion3.ClientID%>').value = p5;
                    document.getElementById('<%=hdDescripcion4.ClientID%>').value = p6;
                    document.getElementById('<%=hdDescripcion5.ClientID%>').value = p7;
                    

                    /*inactivamos todos los checks*/
                    f = document.forms[0].elements;
                    for (x = 0; x < f.length; x++) {
                        obj = f[x];
                        if (obj.type == 'checkbox' && obj.id.replace("<%=GrvListado.ClientID %>", "") != obj.id) {
                            if (obj.id != chkID)
                                obj.checked = false;
                        }
                    }
                } else {
                    objFila.style.backgroundColor = "white";
                }
            }
        }
        function entro() {
            alert('entro');
        }
        function HabilitarUno_Nvr(chk) {
          
            f = document.forms[0].elements;
            for (x = 0; x < f.length; x++) {
                obj = f[x];
                if (obj.type == 'checkbox') {
                    obj.checked = false;
                }
            }
            chk.enabled;
            chk.checked = true;
        }
        function ConsultarNvr() {

            document.getElementById('<%= hdConsultar.ClientID %>').value = '1';
            //alert(document.getElementById('<%=hdConsultar.ClientID%>').value);
            return true;
        }
        
       
  
               
    </script>
 
</table>


