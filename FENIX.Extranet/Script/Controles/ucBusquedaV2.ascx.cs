using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using FENIX.Common;
using FENIX.DataAccess;
using FENIX.BusinessEntity;
using QNET.Web.UI.Controls;
using System.Text;



using System.Data.SqlClient;

public partial class ucBusquedaV2 : System.Web.UI.UserControl
{
    public delegate void ucGrvListadoSeleccionEventCommandHandler(ucGrvListadoSeleccionCommandEventArgs e);
    public event ucGrvListadoSeleccionEventCommandHandler ucSeleccionCambiada;

    public class ucGrvListadoSeleccionCommandEventArgs
    {
        String _DescripcionText_val = string.Empty;
        String _DescripcionText1_val = string.Empty;
        String _DescripcionText2_val = string.Empty;
        String _DescripcionText3_val = string.Empty;
        String _DescripcionText4_val = string.Empty;
        String _DescripcionText5_val = string.Empty;
        String _Codigo_val = string.Empty;

        public string sDescripcion_val
        {
            get { return _DescripcionText_val; }
            set { _DescripcionText_val = value; }
        }
        public string sDescripcion1_val
        {
            get { return _DescripcionText1_val; }
            set { _DescripcionText1_val = value; }
        }
        public string sDescripcion2_val
        {
            get { return _DescripcionText2_val; }
            set { _DescripcionText2_val = value; }
        }
        public string sDescripcion3_val
        {
            get { return _DescripcionText3_val; }
            set { _DescripcionText3_val = value; }
        }
        public string sDescripcion4_val
        {
            get { return _DescripcionText4_val; }
            set { _DescripcionText4_val = value; }
        }
        public string sDescripcion5_val
        {
            get { return _DescripcionText5_val; }
            set { _DescripcionText5_val = value; }
        }
        public string sCodigo_val
        {
            get { return _Codigo_val; }
            set { _Codigo_val = value; }
        }

        public ucGrvListadoSeleccionCommandEventArgs(String sCodigo, String sDescripcion, String sDescripcion1, String sDescripcion2, String sDescripcion3, String sDescripcion4, String sDescripcion5)
        {
            this.sCodigo_val = sCodigo;
            this.sDescripcion_val = sDescripcion;
            this.sDescripcion1_val = sDescripcion1;
            this.sDescripcion2_val = sDescripcion2;
            this.sDescripcion3_val = sDescripcion3;
            this.sDescripcion4_val = sDescripcion4;
            this.sDescripcion5_val = sDescripcion5;        
        }
    }

    #region "Propiedades"

    int _ColumnaVisible = 0;
  
    string _CodigoSecundario2 = string.Empty;

    int _CodigoSecundario = 0;
    int _CodigoSecundario1 = 0;
    int _IdNavVia = 0;
    int _IdOperacion = 0;
    String _Titulo = string.Empty;  
    Boolean _bTxt_CodigoVisible = true;
    Boolean _bTxt_DescripcionVisible = true;
    Boolean _bLbl_CodigoVisible=true;
    Boolean _bLbl_DescripcionVisible=true;
    String _sLbl_Descripcion_Bus_Caption;
    String _sLbl_Codigo_Bus_Caption;
    String _Col_DescripcionText = string.Empty;
    String _Col1_DescripcionText = string.Empty;
    String _Col2_DescripcionText = string.Empty;
    String _Col3_DescripcionText = string.Empty;
    String _Col4_DescripcionText = string.Empty;
    String _Col5_DescripcionText = string.Empty;
    String _Codigo_DescripcionText = string.Empty;

   
    Comun.Busqueda _tipoBusqueda;


    public string sLbl_Descripcion_Bus_Caption
    {
        get { return _sLbl_Descripcion_Bus_Caption; }
        set { _sLbl_Descripcion_Bus_Caption = value; }
    }
    public string sLbl_Codigo_Bus_Caption
    {
        get { return _sLbl_Codigo_Bus_Caption; }
        set { _sLbl_Codigo_Bus_Caption = value; }
    }
    public string sTitulo
    {
        get { return _Titulo; }
        set { _Titulo = value; }
    }
   
    public Boolean bTxt_CodigoVisible
    {
        get { return _bTxt_CodigoVisible; }
        set { _bTxt_CodigoVisible = value; }
    }
    public Boolean bTxt_DescripcionVisible
    {
        get { return _bTxt_DescripcionVisible; }
        set { _bTxt_DescripcionVisible = value; }
    }

    public Boolean bLbl_DescripcionVisible
    {
        get { return _bLbl_DescripcionVisible; }
        set { _bLbl_DescripcionVisible = value; }
    }

    public Boolean bLbl_CodigoVisible
    {
        get { return _bLbl_CodigoVisible; }
        set { _bLbl_CodigoVisible = value; }
    }

    public int IdOperacion
    {
        get { return _IdOperacion; }
        set { _IdOperacion = value; }
    }
    public int IdNavVia
    {
        get { return _IdNavVia; }
        set { _IdNavVia = value; }
    }   
    public string Descripcion
    {
        get { return TxtDescripcion.Text; }

    }

    public int CodigoSecundario
    {
        get { return _CodigoSecundario; }
        set { _CodigoSecundario = value; }
    }

    public int CodigoSecundario1
    {
        get { return _CodigoSecundario1; }
        set { _CodigoSecundario1 = value; }
    }
    public string CodigoSecundario2
    {
        get { return _CodigoSecundario2; }
        set { _CodigoSecundario2 = value; }
    }
    public string Codigo
    {
        get { return TxtCodigo.Text; }

    }
    public Comun.Busqueda tipoBusqueda
    {
        set { _tipoBusqueda = value; }
        get { return _tipoBusqueda; }
    }
    public int ColumnaVisible
    {
        set { _ColumnaVisible = value; }
        get { return _ColumnaVisible; }
    }

    public string Col_DescripcionText
    {
        get { return _Col_DescripcionText; }
        set { _Col_DescripcionText = value; }
    }
    public string Col1_DescripcionText
    {
        get { return _Col1_DescripcionText; }
        set { _Col1_DescripcionText = value; }
    }
    public string Col2_DescripcionText
    {
        get { return _Col2_DescripcionText; }
        set { _Col2_DescripcionText = value; }
    }
    public string Col3_DescripcionText
    {
        get { return _Col3_DescripcionText; }
        set { _Col3_DescripcionText = value; }
    }
    public string Col4_DescripcionText
    {
        get { return _Col4_DescripcionText; }
        set { _Col4_DescripcionText = value; }
    }
    public string Col5_DescripcionText
    {
        get { return _Col5_DescripcionText; }
        set { _Col5_DescripcionText = value; }
    }
    public string Codigo_DescripcionText
    {
        get { return _Codigo_DescripcionText; }
        set { _Codigo_DescripcionText = value; }
    }
    #endregion


    #region Metodos

    protected void Page_Init(object sender, EventArgs e)
    {
        
    }

    #endregion


    #region Metodo Eventos
   

    protected void imgbBuscar_Click(object sender, ImageClickEventArgs e)
    {
        dnvListado.InvokeBind();                           
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        dnvListado.BindGridView += new QNET.Web.UI.Controls.DataNavigator.BindGridViewDelegate(BindGridLista);
        
        if (!Page.IsPostBack)
        {
            List<BE_TablaUc> oBE_TablaUc_Lis = new List<BE_TablaUc>();
            BE_TablaUc oBE_TablaUc = new BE_TablaUc();
            oBE_TablaUc_Lis.Add(oBE_TablaUc);
            GrvUcListado.DataSource = oBE_TablaUc_Lis;
            GrvUcListado.DataBind();           
        }

        LblTitulo.Text = sTitulo;
        TxtCodigo.Visible = bTxt_CodigoVisible;
        TxtDescripcion.Visible = bTxt_DescripcionVisible;

        LblCodigo.Text = sLbl_Codigo_Bus_Caption;
        LblDescripcion.Text = sLbl_Descripcion_Bus_Caption;
        LblCodigo.Visible = LblCodigo.Visible;
        LblDescripcion.Text = sLbl_Descripcion_Bus_Caption;
        LblDescripcion.Visible = bLbl_DescripcionVisible;
        TxtDescripcion.Focus();

       
        TxtDescripcion.Attributes.Add("onkeydown", "javascript:return fc_PressKeyBus();");
        TxtCodigo.Attributes.Add("onkeydown", "javascript:return fc_PressKeyBus();");
        GrvUcListado.Attributes.Add("onkeydown", "javascript:return fc_PressKeyBusGrid();");                                
    }

    DataNavigatorParams BindGridLista(object sender, EventArgs e)
    {
        //DA_ConsultasComunes oDA_ConsultasComunes = new DA_ConsultasComunes();
        //BE_Tabla oBE_Tabla = new BE_Tabla();
        //DA_Contrato oDA_Contrato = new DA_Contrato();
        //BE_Cliente oBE_Cliente = new BE_Cliente();
        //IList<BE_TablaUc> lista = null;

        //switch (tipoBusqueda)
        //{
        //    case Comun.Busqueda.Contenedor:
        //        {
        //            oBE_Tabla.sIdValor = Codigo;
        //            oBE_Tabla.sDescripcion = Descripcion;
        //            oBE_Tabla.NPagina = dnvListado.CurrentPage;
        //            oBE_Tabla.NRegistros = GrvUcListado.PageSize;
        //            lista = oDA_ConsultasComunes.ListarTablaUsercControlContenedor(oBE_Tabla);
        //            dnvListado.Visible = (oBE_Tabla.NTotalRegistros > oBE_Tabla.NRegistros);
        //            return new DataNavigatorParams(lista, oBE_Tabla.NTotalRegistros);
        //            break;
        //        }

        //    case Comun.Busqueda.Inspector:
        //        {
        //            oBE_Tabla.sIdValor = Codigo;
        //            oBE_Tabla.sDescripcion = Descripcion;
        //            oBE_Tabla.NPagina = dnvListado.CurrentPage;
        //            oBE_Tabla.NRegistros = GrvUcListado.PageSize;
        //            lista = oDA_ConsultasComunes.ListarTablaUsercControlInspector(oBE_Tabla);
        //            dnvListado.Visible = (oBE_Tabla.NTotalRegistros > oBE_Tabla.NRegistros);
        //            return new DataNavigatorParams(lista, oBE_Tabla.NTotalRegistros);
        //            break;
        //        }

        //    case Comun.Busqueda.ContenedorNaveViaje_Eir:
        //        {
        //            oBE_Tabla.sIdValor = Codigo;
        //            oBE_Tabla.sDescripcion = Descripcion;
        //            oBE_Tabla.NPagina = dnvListado.CurrentPage;
        //            oBE_Tabla.NRegistros = GrvUcListado.PageSize;
        //            oBE_Tabla.iCodigoSecundario = CodigoSecundario;
        //            oBE_Tabla.iIdOperacion = IdOperacion;
        //            lista = oDA_ConsultasComunes.ListarTablaUsercControlContenedorNaveViaje_EIR(oBE_Tabla);
        //            dnvListado.Visible = (oBE_Tabla.NTotalRegistros > oBE_Tabla.NRegistros);
        //            return new DataNavigatorParams(lista, oBE_Tabla.NTotalRegistros);
        //            break;
        //        }
        //         case Comun.Busqueda.ContenedorNaveViaje_EirRetiro:
        //        {
        //            oBE_Tabla.sIdValor = Codigo;
        //            oBE_Tabla.sDescripcion = Descripcion;
        //            oBE_Tabla.NPagina = dnvListado.CurrentPage;
        //            oBE_Tabla.NRegistros = GrvUcListado.PageSize;
        //            oBE_Tabla.iCodigoSecundario = CodigoSecundario;
        //            oBE_Tabla.iIdOperacion = IdOperacion;
        //            lista = oDA_ConsultasComunes.ListarTablaUsercControlContenedorNaveViaje_EIR_Retiro(oBE_Tabla);
        //            dnvListado.Visible = (oBE_Tabla.NTotalRegistros > oBE_Tabla.NRegistros);
        //            return new DataNavigatorParams(lista, oBE_Tabla.NTotalRegistros);
        //            break;
        //        }

        //    case Comun.Busqueda.ContenedorNaveViaje:
        //        {
        //            oBE_Tabla.sIdValor = Codigo;
        //            oBE_Tabla.sDescripcion = Descripcion;
        //            oBE_Tabla.NPagina = dnvListado.CurrentPage;
        //            oBE_Tabla.NRegistros = GrvUcListado.PageSize;
        //            oBE_Tabla.iCodigoSecundario = CodigoSecundario;
        //            lista = oDA_ConsultasComunes.ListarTablaUsercControlContenedorNaveViaje(oBE_Tabla);
        //            dnvListado.Visible = (oBE_Tabla.NTotalRegistros > oBE_Tabla.NRegistros);
        //            return new DataNavigatorParams(lista, oBE_Tabla.NTotalRegistros);
        //            break;
        //        }

        //    case Comun.Busqueda.Puerto:
        //        {
        //            oBE_Tabla.sIdValor = Codigo;
        //            oBE_Tabla.sDescripcion = Descripcion;
        //            oBE_Tabla.NPagina = dnvListado.CurrentPage;
        //            oBE_Tabla.NRegistros = GrvUcListado.PageSize;
        //            lista = oDA_ConsultasComunes.ListarTablaUsercControlPuerto(oBE_Tabla);
        //            dnvListado.Visible = (oBE_Tabla.NTotalRegistros > oBE_Tabla.NRegistros);
        //            return new DataNavigatorParams(lista, oBE_Tabla.NTotalRegistros);

        //            break;
        //        }


        //    case Comun.Busqueda.DocumentoOrigen_Bookin:
        //        {
        //            oBE_Tabla.sIdValor = Codigo;
        //            oBE_Tabla.sDescripcion = Descripcion;
        //            oBE_Tabla.iCodigoSecundario = CodigoSecundario;
        //            oBE_Tabla.sTipoBooking = CodigoSecundario2;
        //            oBE_Tabla.NPagina = dnvListado.CurrentPage;
        //            oBE_Tabla.NRegistros = GrvUcListado.PageSize;
        //            lista = oDA_ConsultasComunes.ListarTablaUserControlDocumentoOrigen_Bookin(oBE_Tabla);
        //            dnvListado.Visible = (oBE_Tabla.NTotalRegistros > oBE_Tabla.NRegistros);
        //            return new DataNavigatorParams(lista, oBE_Tabla.NTotalRegistros);

        //            break;
        //        }

        //    case Comun.Busqueda.DocumentoOrigen_Bookin_Roleo:
        //        {
        //            oBE_Tabla.sIdValor = Codigo;
        //            oBE_Tabla.sDescripcion = Descripcion;
        //            oBE_Tabla.sTipoBooking = CodigoSecundario2;
        //            oBE_Tabla.NPagina = dnvListado.CurrentPage;
        //            oBE_Tabla.NRegistros = GrvUcListado.PageSize;
        //            lista = oDA_ConsultasComunes.ListarTablaUserControlDocumentoOrigen_Bookin_Roleo(oBE_Tabla);
        //            dnvListado.Visible = (oBE_Tabla.NTotalRegistros > oBE_Tabla.NRegistros);
        //            return new DataNavigatorParams(lista, oBE_Tabla.NTotalRegistros);

        //            break;
        //        }
        //    case Comun.Busqueda.DocumentoOrigen_BookingDet_CS:
        //        {
        //            oBE_Tabla.sIdValor = Codigo;
        //            oBE_Tabla.sDescripcion = Descripcion;
        //            oBE_Tabla.iCodigoSecundario = CodigoSecundario;
        //            oBE_Tabla.NPagina = dnvListado.CurrentPage;
        //            oBE_Tabla.NRegistros = GrvUcListado.PageSize;
        //            lista = oDA_ConsultasComunes.ListarTablaUserControlDocumentoOrigen_BookingDet_CS(oBE_Tabla);
        //            dnvListado.Visible = (oBE_Tabla.NTotalRegistros > oBE_Tabla.NRegistros);
        //            return new DataNavigatorParams(lista, oBE_Tabla.NTotalRegistros);

        //            break;
        //        }

        //    case Comun.Busqueda.DocumentoOrigen:
        //        {
        //            oBE_Tabla.sIdValor = Codigo;
        //            oBE_Tabla.sDescripcion = Descripcion;
        //            oBE_Tabla.NPagina = dnvListado.CurrentPage;
        //            oBE_Tabla.NRegistros = GrvUcListado.PageSize;
        //            oBE_Tabla.iIdTablaPadre = CodigoSecundario;
        //            lista = oDA_ConsultasComunes.ListarTablaUserControlDocumentoOrigen(oBE_Tabla);
        //            dnvListado.Visible = (oBE_Tabla.NTotalRegistros > oBE_Tabla.NRegistros);
        //            return new DataNavigatorParams(lista, oBE_Tabla.NTotalRegistros);

        //            break;
        //        }


        //    case Comun.Busqueda.DocumentoOrigenDesglose:
        //        {
        //            oBE_Tabla.sIdValor = Codigo;
        //            oBE_Tabla.sDescripcion = Descripcion;
        //            oBE_Tabla.NPagina = dnvListado.CurrentPage;
        //            oBE_Tabla.NRegistros = GrvUcListado.PageSize;
        //            oBE_Tabla.iIdTablaPadre = CodigoSecundario;
        //            oBE_Tabla.iIdOperacion = IdOperacion;
        //            oBE_Tabla.iIdNavVia = IdNavVia;

        //            //TxtManifiesto.Visible = bTxt_ManifiestoVisible;
        //            //TxtDescripcion.Visible = bTxt_DescripcionVisible;
        //            //TxtCodigo.Visible = bTxt_CodigoVisible;
        //            //LblCodigo.Text = sLbl_Codigo_Bus_Caption;
        //            //LblDescripcion.Text = sLbl_Descripcion_Bus_Caption;

        //            lista = oDA_ConsultasComunes.ListarTablaUserControlDocumentoOrigen_Desglose(oBE_Tabla);
        //            dnvListado.Visible = (oBE_Tabla.NTotalRegistros > oBE_Tabla.NRegistros);
        //            return new DataNavigatorParams(lista, oBE_Tabla.NTotalRegistros);

        //            break;
        //        }

        //    case Comun.Busqueda.DocumentoOrigen_Previo:
        //        {
        //            oBE_Tabla.sIdValor = Codigo;
        //            oBE_Tabla.sDescripcion = Descripcion;
        //            oBE_Tabla.NPagina = dnvListado.CurrentPage;
        //            oBE_Tabla.NRegistros = GrvUcListado.PageSize;
        //            oBE_Tabla.iIdTablaPadre = CodigoSecundario; //IdNaveViaje
        //            oBE_Tabla.iIdTabla = CodigoSecundario1; //IdOperacion
        //            lista = oDA_ConsultasComunes.ListarTablaUserControlDocumentoOrigen_Previo(oBE_Tabla);
        //            dnvListado.Visible = (oBE_Tabla.NTotalRegistros > oBE_Tabla.NRegistros);
        //            return new DataNavigatorParams(lista, oBE_Tabla.NTotalRegistros);

        //            break;
        //        }

        //    case Comun.Busqueda.DocumentoOrigen_ProcesoEsp:
        //        {
        //            oBE_Tabla.sIdValor = Codigo;
        //            oBE_Tabla.sDescripcion = Descripcion;
        //            oBE_Tabla.NPagina = dnvListado.CurrentPage;
        //            oBE_Tabla.NRegistros = GrvUcListado.PageSize;
        //            oBE_Tabla.iIdTablaPadre = CodigoSecundario; //IdNaveViaje
        //            oBE_Tabla.iIdTabla = CodigoSecundario1; //IdOperacion
        //            lista = oDA_ConsultasComunes.ListarTablaUserControlDocumentoOrigen_ProcesoEsp(oBE_Tabla);
        //            dnvListado.Visible = (oBE_Tabla.NTotalRegistros > oBE_Tabla.NRegistros);
        //            return new DataNavigatorParams(lista, oBE_Tabla.NTotalRegistros);

        //            break;
        //        }

        //    case Comun.Busqueda.DocumentoOrigen_Volante:
        //        {
        //            oBE_Tabla.sIdValor = Codigo;
        //            oBE_Tabla.sDescripcion = Descripcion;
        //            oBE_Tabla.NPagina = dnvListado.CurrentPage;
        //            oBE_Tabla.NRegistros = GrvUcListado.PageSize;
        //            oBE_Tabla.iIdTablaPadre = CodigoSecundario; //IdNaveViaje
        //            oBE_Tabla.iIdTabla = CodigoSecundario1; //IdOperacion
        //            lista = oDA_ConsultasComunes.ListarTablaUserControlDocumentoOrigen_Volante(oBE_Tabla);
        //            dnvListado.Visible = (oBE_Tabla.NTotalRegistros > oBE_Tabla.NRegistros);
        //            return new DataNavigatorParams(lista, oBE_Tabla.NTotalRegistros);

        //            break;
        //        }

        //    case Comun.Busqueda.DocumentoOrigenDet_Previo:
        //        {
        //            oBE_Tabla.sIdValor = Codigo;
        //            oBE_Tabla.sDescripcion = Descripcion;
        //            oBE_Tabla.NPagina = dnvListado.CurrentPage;
        //            oBE_Tabla.NRegistros = GrvUcListado.PageSize;
        //            oBE_Tabla.iIdTablaPadre = CodigoSecundario;
        //            lista = oDA_ConsultasComunes.ListarTablaUserControlDocumentoOrigenDet_Previo(oBE_Tabla);
        //            dnvListado.Visible = (oBE_Tabla.NTotalRegistros > oBE_Tabla.NRegistros);
        //            return new DataNavigatorParams(lista, oBE_Tabla.NTotalRegistros);

        //            break;
        //        }

        //    case Comun.Busqueda.DocumentoOrigen_InvLib:
        //        {
        //            oBE_Tabla.sIdValor = Codigo;
        //            oBE_Tabla.sDescripcion = Descripcion;
        //            oBE_Tabla.NPagina = dnvListado.CurrentPage;
        //            oBE_Tabla.NRegistros = GrvUcListado.PageSize;
        //            oBE_Tabla.iIdTablaPadre = CodigoSecundario;
        //            lista = oDA_ConsultasComunes.ListarTablaUserControlDocumentoOrigen_InvLib(oBE_Tabla);
        //            dnvListado.Visible = (oBE_Tabla.NTotalRegistros > oBE_Tabla.NRegistros);
        //            return new DataNavigatorParams(lista, oBE_Tabla.NTotalRegistros);

        //            break;
        //        }

        //    case Comun.Busqueda.DocumentoOrigenDet_InvLib:
        //        {
        //            oBE_Tabla.sIdValor = Codigo;
        //            oBE_Tabla.sDescripcion = Descripcion;
        //            oBE_Tabla.NPagina = dnvListado.CurrentPage;
        //            oBE_Tabla.NRegistros = GrvUcListado.PageSize;
        //            oBE_Tabla.iIdTablaPadre = CodigoSecundario;
        //            lista = oDA_ConsultasComunes.ListarTablaUserControlDocumentoOrigenDet_InvLib(oBE_Tabla);
        //            dnvListado.Visible = (oBE_Tabla.NTotalRegistros > oBE_Tabla.NRegistros);
        //            return new DataNavigatorParams(lista, oBE_Tabla.NTotalRegistros);

        //            break;
        //        }

        //    case Comun.Busqueda.DocumentoOrigenDet_InvLibPrec:
        //        {
        //            oBE_Tabla.sIdValor = Codigo;
        //            oBE_Tabla.sDescripcion = Descripcion;
        //            oBE_Tabla.NPagina = dnvListado.CurrentPage;
        //            oBE_Tabla.NRegistros = GrvUcListado.PageSize;
        //            oBE_Tabla.iIdTablaPadre = CodigoSecundario;
        //            oBE_Tabla.iIdTabla = CodigoSecundario1;
        //            lista = oDA_ConsultasComunes.ListarTablaUserControlDocumentoOrigenDet_InvLibPrec(oBE_Tabla);
        //            dnvListado.Visible = (oBE_Tabla.NTotalRegistros > oBE_Tabla.NRegistros);
        //            return new DataNavigatorParams(lista, oBE_Tabla.NTotalRegistros);

        //            break;
        //        }

        //    case Comun.Busqueda.Inmovilizacion_Precedente:
        //        {
        //            oBE_Tabla.sIdValor = Codigo;
        //            oBE_Tabla.sDescripcion = Descripcion;
        //            oBE_Tabla.NPagina = dnvListado.CurrentPage;
        //            oBE_Tabla.NRegistros = GrvUcListado.PageSize;
        //            oBE_Tabla.iIdTablaPadre = CodigoSecundario;
        //            lista = oDA_ConsultasComunes.ListarTablaUserControlInmovilizacion_Precedente(oBE_Tabla);
        //            dnvListado.Visible = (oBE_Tabla.NTotalRegistros > oBE_Tabla.NRegistros);
        //            return new DataNavigatorParams(lista, oBE_Tabla.NTotalRegistros);

        //            break;
        //        }

        //    case Comun.Busqueda.DocumentoOrigenDet_MalEstado:
        //        {
        //            oBE_Tabla.sIdValor = Descripcion ;
        //            oBE_Tabla.sDescripcion = Descripcion;
        //            oBE_Tabla.NPagina = dnvListado.CurrentPage;
        //            oBE_Tabla.NRegistros = GrvUcListado.PageSize;
        //            oBE_Tabla.iIdTablaPadre = CodigoSecundario;
        //            lista = oDA_ConsultasComunes.ListarTablaUserControlDocumentoOrigenDet_MalEstado(oBE_Tabla);
        //            dnvListado.Visible = (oBE_Tabla.NTotalRegistros > oBE_Tabla.NRegistros);
        //            return new DataNavigatorParams(lista, oBE_Tabla.NTotalRegistros);

        //            break;
        //        }


        //    case Comun.Busqueda.DocumentoOrigenDetalleCargaSuelta:
        //        {
        //            oBE_Tabla.sIdValor = Descripcion ;
        //            oBE_Tabla.sDescripcion = Codigo;
        //            oBE_Tabla.IdMovimiento = CodigoSecundario1; // IdMovimiento
        //            oBE_Tabla.NPagina = dnvListado.CurrentPage;
        //            oBE_Tabla.NRegistros = GrvUcListado.PageSize;
        //            oBE_Tabla.iIdTablaPadre = CodigoSecundario;
        //            lista = oDA_ConsultasComunes.ListarTablaUserControlDocumentoOrigenTarjaCS(oBE_Tabla);
        //            dnvListado.Visible = (oBE_Tabla.NTotalRegistros > oBE_Tabla.NRegistros);
        //            return new DataNavigatorParams(lista, oBE_Tabla.NTotalRegistros);

        //            break;
        //        }

        //    case Comun.Busqueda.DocumentoOrigenDetalleTarjaVehicular:
        //        {
        //            oBE_Tabla.sIdValor = Codigo;
        //            oBE_Tabla.sDescripcion = Descripcion;
        //            oBE_Tabla.NPagina = dnvListado.CurrentPage;
        //            oBE_Tabla.NRegistros = GrvUcListado.PageSize;
        //            oBE_Tabla.iIdTablaPadre = CodigoSecundario;
        //            oBE_Tabla.iIdOperacion = IdOperacion;
        //            lista = oDA_ConsultasComunes.ListarTablaUserControlDocumentoOrigenTarjaVEH(oBE_Tabla);
        //            dnvListado.Visible = (oBE_Tabla.NTotalRegistros > oBE_Tabla.NRegistros);
        //            return new DataNavigatorParams(lista, oBE_Tabla.NTotalRegistros);

        //            break;
        //        }


        //    case Comun.Busqueda.DocumentoOrigenDetalleTarjaVehicular_Retiro:
        //        {
        //            oBE_Tabla.sIdValor = Codigo;
        //            oBE_Tabla.sDescripcion = Descripcion;
        //            oBE_Tabla.NPagina = dnvListado.CurrentPage;
        //            oBE_Tabla.NRegistros = GrvUcListado.PageSize;
        //            oBE_Tabla.iIdTablaPadre = CodigoSecundario;
        //            oBE_Tabla.iIdOperacion = IdOperacion;
        //            lista = oDA_ConsultasComunes.ListarTablaUserControlDocumentoOrigenTarjaVEH_Retiro(oBE_Tabla);
        //            dnvListado.Visible = (oBE_Tabla.NTotalRegistros > oBE_Tabla.NRegistros);
        //            return new DataNavigatorParams(lista, oBE_Tabla.NTotalRegistros);

        //            break;
        //        }

        //    case Comun.Busqueda.Dua:
        //        {
        //            return null;
        //            break;
        //        }

        //    case Comun.Busqueda.NaveViajeRumbo:
        //        {
        //            BE_NaveViaje oBE_NaveViaje = new BE_NaveViaje();
        //            DA_NaveViaje oDA_NaveViaje = new DA_NaveViaje();
        //            oBE_NaveViaje.NPagina = dnvListado.CurrentPage;
        //            oBE_NaveViaje.NRegistros = GrvUcListado.PageSize;
        //            IList<BE_TablaUc> listaNvr = oDA_NaveViaje.ListarManifiestosParaInforme(oBE_NaveViaje);
        //            dnvListado.Visible = (oBE_NaveViaje.NTotalRegistros > oBE_NaveViaje.NRegistros);
        //            return new DataNavigatorParams(listaNvr, oBE_NaveViaje.NTotalRegistros);
        //            break;
        //        }

        //    case Comun.Busqueda.NaveViajeRumboExportacion:
        //        {
        //            BE_NaveViaje oBE_NaveViaje = new BE_NaveViaje();
        //            DA_NaveViaje oDA_NaveViaje = new DA_NaveViaje();
        //            oBE_NaveViaje.NPagina = dnvListado.CurrentPage;
        //            oBE_NaveViaje.NRegistros = GrvUcListado.PageSize;
        //            IList<BE_TablaUc> listaNvr = oDA_NaveViaje.ListarManifiestosPorOperacion(oBE_NaveViaje,"E");
        //            dnvListado.Visible = (oBE_NaveViaje.NTotalRegistros > oBE_NaveViaje.NRegistros);
        //            return new DataNavigatorParams(listaNvr, oBE_NaveViaje.NTotalRegistros);
        //            break;
        //        }


        //    case Comun.Busqueda.Tarifa:
        //        {
        //            oBE_Tabla.iIdTablaPadre = 0;
        //            oBE_Tabla.iIdTabla = CodigoSecundario;
        //            oBE_Tabla.sIdValor = Codigo;
        //            oBE_Tabla.sDescripcion = Descripcion;
        //            oBE_Tabla.NPagina = 1;
        //            oBE_Tabla.NRegistros = 100;
        //            lista = oDA_Contrato.ListarTarifaMaster(oBE_Tabla);
        //            dnvListado.Visible = (oBE_Tabla.NTotalRegistros > oBE_Tabla.NRegistros);
        //            return new DataNavigatorParams(lista, oBE_Tabla.NTotalRegistros);
        //        }

        //    case Comun.Busqueda.Cliente:
        //        {
        //            oBE_Cliente.sRazon_Social = Descripcion;
        //            oBE_Cliente.sNumDocumento = Codigo;
        //            oBE_Cliente.iIdRol = CodigoSecundario;
        //            oBE_Cliente.NPagina = dnvListado.CurrentPage;
        //            oBE_Cliente.NRegistros = GrvUcListado.PageSize;

        //            if (CodigoSecundario == 0)
        //                lista = oDA_ConsultasComunes.ListarClienteTodos(oBE_Cliente);
        //            else
        //                lista = oDA_Contrato.ListarCliente(oBE_Cliente);

        //            dnvListado.Visible = (oBE_Cliente.NTotalRegistros > oBE_Cliente.NRegistros);
        //            return new DataNavigatorParams(lista, oBE_Cliente.NTotalRegistros);
        //        }

        //    case Comun.Busqueda.RolCliente:
        //        oBE_Tabla.iIdTablaPadre = 0;
        //        oBE_Tabla.iIdTabla = 33;
        //        oBE_Tabla.sIdValor = Codigo;
        //        oBE_Tabla.sDescripcion = Descripcion;
        //        oBE_Tabla.NPagina = dnvListado.CurrentPage;
        //        oBE_Tabla.NRegistros = GrvUcListado.PageSize;
        //        lista = oDA_ConsultasComunes.ListarTablaUsercControl(oBE_Tabla);
        //        dnvListado.Visible = (oBE_Tabla.NTotalRegistros > oBE_Tabla.NRegistros);
        //        return new DataNavigatorParams(lista, oBE_Tabla.NTotalRegistros);


        //    case Comun.Busqueda.Servicio:

        //        oBE_Tabla.sIdValor = Codigo;
        //        oBE_Tabla.sDescripcion = Descripcion;
        //        oBE_Tabla.NPagina = dnvListado.CurrentPage;
        //        oBE_Tabla.NRegistros = GrvUcListado.PageSize;
        //        oBE_Tabla.iIdTablaPadre = 0;
        //        lista = oDA_ConsultasComunes.ListarTablaUsercControlServicio(oBE_Tabla);
        //        dnvListado.Visible = (oBE_Tabla.NTotalRegistros > oBE_Tabla.NRegistros);
        //        return new DataNavigatorParams(lista, oBE_Tabla.NTotalRegistros);


        //    case Comun.Busqueda.TipoContenedor:

        //        oBE_Tabla.sIdValor = Codigo;
        //        oBE_Tabla.iIdTabla = 17;
        //        oBE_Tabla.sDescripcion = Descripcion;
        //        oBE_Tabla.NPagina = dnvListado.CurrentPage;
        //        oBE_Tabla.NRegistros = GrvUcListado.PageSize;
        //        lista = oDA_ConsultasComunes.ListarTablaUsercControl(oBE_Tabla);
        //        dnvListado.Visible = (oBE_Tabla.NTotalRegistros > oBE_Tabla.NRegistros);
        //        return new DataNavigatorParams(lista, oBE_Tabla.NTotalRegistros);


        //    case Comun.Busqueda.TipoCarga:

        //        oBE_Tabla.sIdValor = Codigo;
        //        oBE_Tabla.iIdTabla = 37;
        //        oBE_Tabla.sDescripcion = Descripcion;
        //        oBE_Tabla.NPagina = dnvListado.CurrentPage;
        //        oBE_Tabla.NRegistros = GrvUcListado.PageSize;
        //        lista = oDA_ConsultasComunes.ListarTablaUsercControlTipoCarga(oBE_Tabla);
        //        dnvListado.Visible = (oBE_Tabla.NTotalRegistros > oBE_Tabla.NRegistros);
        //        return new DataNavigatorParams(lista, oBE_Tabla.NTotalRegistros);

        //    case Comun.Busqueda.Carga:

        //        oBE_Tabla.sIdValor = Codigo;
        //        oBE_Tabla.iIdTabla = CodigoSecundario;
        //        oBE_Tabla.sDescripcion = Descripcion;
        //        oBE_Tabla.NPagina = dnvListado.CurrentPage;
        //        oBE_Tabla.NRegistros = GrvUcListado.PageSize;
        //        lista = oDA_ConsultasComunes.ListarTablaUsercControlCarga(oBE_Tabla);
        //        dnvListado.Visible = (oBE_Tabla.NTotalRegistros > oBE_Tabla.NRegistros);
        //        return new DataNavigatorParams(lista, oBE_Tabla.NTotalRegistros);




        //    case Comun.Busqueda.Embalaje:

        //        oBE_Tabla.sIdValor = Codigo;
        //        oBE_Tabla.iIdTabla = 6;
        //        oBE_Tabla.sDescripcion = Descripcion;
        //        oBE_Tabla.NPagina = dnvListado.CurrentPage;
        //        oBE_Tabla.NRegistros = GrvUcListado.PageSize;
        //        lista = oDA_ConsultasComunes.ListarTablaUsercControlEmbalaje(oBE_Tabla);
        //        dnvListado.Visible = (oBE_Tabla.NTotalRegistros > oBE_Tabla.NRegistros);
        //        return new DataNavigatorParams(lista, oBE_Tabla.NTotalRegistros);


        //    case Comun.Busqueda.Costo:

        //        oBE_Tabla.sIdValor = Codigo;
        //        oBE_Tabla.iIdTabla = 41;
        //        oBE_Tabla.sDescripcion = Descripcion;
        //        oBE_Tabla.NPagina = dnvListado.CurrentPage;
        //        oBE_Tabla.NRegistros = GrvUcListado.PageSize;
        //        lista = oDA_ConsultasComunes.ListarTablaUsercControl(oBE_Tabla);
        //        dnvListado.Visible = (oBE_Tabla.NTotalRegistros > oBE_Tabla.NRegistros);
        //        return new DataNavigatorParams(lista, oBE_Tabla.NTotalRegistros);


        //    case Comun.Busqueda.Vendedor:
        //        oBE_Tabla.sDescripcion = Descripcion;
        //        oBE_Tabla.sIdValor = Codigo;
        //        oBE_Tabla.NPagina = dnvListado.CurrentPage;
        //        oBE_Tabla.NRegistros = GrvUcListado.PageSize;
        //        lista = oDA_Contrato.ListarVendedor(oBE_Tabla);
        //        dnvListado.Visible = (oBE_Cliente.NTotalRegistros > oBE_Cliente.NRegistros);
        //        return new DataNavigatorParams(lista, oBE_Cliente.NTotalRegistros);


        //    case Comun.Busqueda.Direccionante:
        //        {
        //            oBE_Tabla.sIdValor = Codigo;
        //            oBE_Tabla.sDescripcion = Descripcion;
        //            oBE_Tabla.NPagina = dnvListado.CurrentPage;
        //            oBE_Tabla.NRegistros = GrvUcListado.PageSize;
        //            lista = oDA_ConsultasComunes.ListarTablaUserControlDireccionante(oBE_Tabla);
        //            dnvListado.Visible = (oBE_Tabla.NTotalRegistros > oBE_Tabla.NRegistros);
        //            return new DataNavigatorParams(lista, oBE_Tabla.NTotalRegistros);
        //            break;
        //        }


        //    case Comun.Busqueda.LineaMaritima:
        //        {
        //            oBE_Tabla.sIdValor = Codigo;
        //            oBE_Tabla.sDescripcion = Descripcion;
        //            oBE_Tabla.NPagina = dnvListado.CurrentPage;
        //            oBE_Tabla.NRegistros = GrvUcListado.PageSize;
        //            lista = oDA_ConsultasComunes.ListarTablaUserControlLineaMaritima(oBE_Tabla);
        //            dnvListado.Visible = (oBE_Tabla.NTotalRegistros > oBE_Tabla.NRegistros);
        //            return new DataNavigatorParams(lista, oBE_Tabla.NTotalRegistros);
        //            break;
        //        }

        //    case Comun.Busqueda.NaveViajeRumboProvisional:
        //        {
        //            BE_NaveViajeProvisional oBE_NaveViajeProvisional = new BE_NaveViajeProvisional();
        //            DA_NaveViajeProvisional oDA_NaveViajeProvisional = new DA_NaveViajeProvisional();
        //            oBE_NaveViajeProvisional.NPagina = dnvListado.CurrentPage;
        //            oBE_NaveViajeProvisional.NRegistros = GrvUcListado.PageSize;
        //            IList<BE_TablaUc> listaNvr = oDA_NaveViajeProvisional.ListarManifiestosParaDocumento(oBE_NaveViajeProvisional);
        //            dnvListado.Visible = (oBE_NaveViajeProvisional.NTotalRegistros > oBE_NaveViajeProvisional.NRegistros);
        //            return new DataNavigatorParams(listaNvr, oBE_NaveViajeProvisional.NTotalRegistros);
        //            break;
        //        }
        //    case Comun.Busqueda.UnidadMedida:
        //        {
        //            oBE_Tabla.sIdValor = Codigo;
        //            oBE_Tabla.iIdTabla = 5;
        //            oBE_Tabla.sDescripcion = Descripcion;
        //            oBE_Tabla.NPagina = dnvListado.CurrentPage;
        //            oBE_Tabla.NRegistros = GrvUcListado.PageSize;
        //            lista = oDA_ConsultasComunes.ListarTablaUsercControl(oBE_Tabla);
        //            dnvListado.Visible = (oBE_Tabla.NTotalRegistros > oBE_Tabla.NRegistros);
        //            return new DataNavigatorParams(lista, oBE_Tabla.NTotalRegistros);
        //        }

        //    case Comun.Busqueda.PlacaVehiculo:
        //        {
        //            oBE_Tabla.sIdValor = Codigo;
        //            oBE_Tabla.sDescripcion = Descripcion;
        //            oBE_Tabla.NPagina = dnvListado.CurrentPage;
        //            oBE_Tabla.NRegistros = GrvUcListado.PageSize;
        //            lista = oDA_ConsultasComunes.ListarTablaUserControlPlacaVehiculo(oBE_Tabla);
        //            dnvListado.Visible = (oBE_Tabla.NTotalRegistros > oBE_Tabla.NRegistros);
        //            return new DataNavigatorParams(lista, oBE_Tabla.NTotalRegistros);
        //            break;
        //        }

        //    case Comun.Busqueda.Chofer:
        //        {
        //            oBE_Tabla.sIdValor = Codigo;
        //            oBE_Tabla.sDescripcion = Descripcion;
        //            oBE_Tabla.NPagina = dnvListado.CurrentPage;
        //            oBE_Tabla.NRegistros = GrvUcListado.PageSize;
        //            lista = oDA_ConsultasComunes.ListarTablaUserControlChofer(oBE_Tabla);
        //            dnvListado.Visible = (oBE_Tabla.NTotalRegistros > oBE_Tabla.NRegistros);
        //            return new DataNavigatorParams(lista, oBE_Tabla.NTotalRegistros);
        //            break;
        //        }

        //    case Comun.Busqueda.DocumentoOrigen_MovBalanza:
        //        {
        //            oBE_Tabla.sIdValor = Codigo;
        //            oBE_Tabla.sDescripcion = Descripcion;
        //            oBE_Tabla.iIdNavVia = CodigoSecundario;
        //            oBE_Tabla.NPagina = dnvListado.CurrentPage;
        //            oBE_Tabla.NRegistros = GrvUcListado.PageSize;
        //            lista = oDA_ConsultasComunes.ListarTablaUserControlDocumentoOrigen_MovBalanza(oBE_Tabla);
        //            dnvListado.Visible = (oBE_Tabla.NTotalRegistros > oBE_Tabla.NRegistros);
        //            return new DataNavigatorParams(lista, oBE_Tabla.NTotalRegistros);
        //            break;
        //        }


        //    case Comun.Busqueda.DocumentoOrigen_Dua:
        //        {
        //            oBE_Tabla.sIdValor = Codigo;
        //            oBE_Tabla.sDescripcion = Descripcion;
        //            oBE_Tabla.iCodigoSecundario = CodigoSecundario;
        //            oBE_Tabla.NPagina = dnvListado.CurrentPage;
        //            oBE_Tabla.NRegistros = GrvUcListado.PageSize;
        //            lista = oDA_ConsultasComunes.ListarTablaUserControlDocumentoOrigen_Dua(oBE_Tabla);
        //            dnvListado.Visible = (oBE_Tabla.NTotalRegistros > oBE_Tabla.NRegistros);
        //            return new DataNavigatorParams(lista, oBE_Tabla.NTotalRegistros);
        //            break;
        //        }
        //    case Comun.Busqueda.DocumentoOrigen_Liquidacion:
        //        {
        //            oBE_Tabla.sIdValor = Codigo;
        //            oBE_Tabla.sIdValor = Descripcion;
        //            oBE_Tabla.NPagina = dnvListado.CurrentPage;
        //            oBE_Tabla.NRegistros = GrvUcListado.PageSize;
        //            oBE_Tabla.iCodigoSecundario = CodigoSecundario;
        //            lista = oDA_ConsultasComunes.ListarTablaUserControlDocumentoOrigen_Liquidacion(oBE_Tabla);
        //            dnvListado.Visible = (oBE_Tabla.NTotalRegistros > oBE_Tabla.NRegistros);
        //            return new DataNavigatorParams(lista, oBE_Tabla.NTotalRegistros);
        //            break;
        //        }

        //    case Comun.Busqueda.PlacaTarja:
        //        {
        //            oBE_Tabla.sIdValor = Codigo;
        //            oBE_Tabla.iCodigoSecundario = CodigoSecundario;
        //            oBE_Tabla.sDescripcion = Descripcion;
        //            oBE_Tabla.NPagina = dnvListado.CurrentPage;
        //            oBE_Tabla.NRegistros = GrvUcListado.PageSize;

        //            lista = oDA_ConsultasComunes.ListarTablaUserControlPlacaTarja(oBE_Tabla);

        //            dnvListado.Visible = (oBE_Tabla.NTotalRegistros > oBE_Tabla.NRegistros);
        //            return new DataNavigatorParams(lista, oBE_Tabla.NTotalRegistros);
        //        }

        //    case Comun.Busqueda.Despachador:
        //        {
        //            oBE_Tabla.sIdValor = Codigo;
        //            oBE_Tabla.sDescripcion = Descripcion;
        //            oBE_Tabla.NPagina = dnvListado.CurrentPage;
        //            oBE_Tabla.NRegistros = GrvUcListado.PageSize;

        //            lista = oDA_ConsultasComunes.ListarTablaUsercControlDespachador(oBE_Tabla);

        //            dnvListado.Visible = (oBE_Tabla.NTotalRegistros > oBE_Tabla.NRegistros);
        //            return new DataNavigatorParams(lista, oBE_Tabla.NTotalRegistros);
        //        }

        //    case Comun.Busqueda.AutorizacionRetiro:
        //        {
        //            oBE_Tabla.sIdValor = Codigo;
        //            oBE_Tabla.sDescripcion = Descripcion;
        //            oBE_Tabla.NPagina = dnvListado.CurrentPage;
        //            oBE_Tabla.NRegistros = GrvUcListado.PageSize;

        //            lista = oDA_ConsultasComunes.ListarTablaUsercControlAutRetiro(oBE_Tabla);

        //            dnvListado.Visible = (oBE_Tabla.NTotalRegistros > oBE_Tabla.NRegistros);
        //            return new DataNavigatorParams(lista, oBE_Tabla.NTotalRegistros);
        //        }
        //    case Comun.Busqueda.AutorizacionRetiro_PesajeCs:
        //        {
        //            oBE_Tabla.sIdValor = Codigo;
        //            oBE_Tabla.sDescripcion = Descripcion;
        //            oBE_Tabla.NPagina = dnvListado.CurrentPage;
        //            oBE_Tabla.NRegistros = GrvUcListado.PageSize;

        //            lista = oDA_ConsultasComunes.ListarTablaUsercControlAutRetiro_PesajeCS(oBE_Tabla);

        //            dnvListado.Visible = (oBE_Tabla.NTotalRegistros > oBE_Tabla.NRegistros);
        //            return new DataNavigatorParams(lista, oBE_Tabla.NTotalRegistros);
        //        }
        //    case Comun.Busqueda.AutorizacionRetiro_MovBal:
        //        {
        //            oBE_Tabla.sIdValor = Codigo;
        //            oBE_Tabla.sDescripcion = Descripcion;
        //            oBE_Tabla.NPagina = dnvListado.CurrentPage;
        //            oBE_Tabla.NRegistros = GrvUcListado.PageSize;

        //            lista = oDA_ConsultasComunes.ListarTablaUsercControlAutRetiro_MovBal(oBE_Tabla);

        //            dnvListado.Visible = (oBE_Tabla.NTotalRegistros > oBE_Tabla.NRegistros);
        //            return new DataNavigatorParams(lista, oBE_Tabla.NTotalRegistros);
        //        }

        //    case Comun.Busqueda.Zona:
        //        {
        //            oBE_Tabla.sIdValor = Codigo;
        //            oBE_Tabla.sDescripcion = Descripcion;
        //            oBE_Tabla.NPagina = dnvListado.CurrentPage;
        //            oBE_Tabla.NRegistros = GrvUcListado.PageSize;
        //            lista = oDA_ConsultasComunes.ListarTablaUsercControlZona(oBE_Tabla);
        //            dnvListado.Visible = (oBE_Tabla.NTotalRegistros > oBE_Tabla.NRegistros);
        //            return new DataNavigatorParams(lista, oBE_Tabla.NTotalRegistros);
        //            break;
        //        }

        //    case Comun.Busqueda.Nave:
        //        {
        //            oBE_Tabla.sIdValor = Codigo;
        //            oBE_Tabla.sDescripcion = Descripcion;
        //            oBE_Tabla.NPagina = dnvListado.CurrentPage;
        //            oBE_Tabla.NRegistros = GrvUcListado.PageSize;
        //            lista = oDA_ConsultasComunes.ListarTablaUsercControlNave(oBE_Tabla);
        //            dnvListado.Visible = (oBE_Tabla.NTotalRegistros > oBE_Tabla.NRegistros);
        //            return new DataNavigatorParams(lista, oBE_Tabla.NTotalRegistros);

        //            break;
        //        }
              
        //}
        return null;
    }

    protected void GrvListado_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {

            DataKey dataKey;
            dataKey = this.GrvUcListado.DataKeys[e.Row.RowIndex];
            BE_TablaUc oitem = (e.Row.DataItem as BE_TablaUc);

            CheckBox chkSeleccion = (CheckBox)e.Row.FindControl("chkSeleccionar");
            e.Row.Style["cursor"] = "pointer";
            e.Row.Attributes["onclick"] = String.Format("javascript:fc_SeleccionaFilaSimpleUc(this,'{0}','{1}');", e.Row.RowIndex, chkSeleccion.ClientID);
            chkSeleccion.Attributes["onclick"] = "javascript:HabilitarUno(this);";          
            e.Row.Attributes["ondblclick"] = Page.ClientScript.GetPostBackClientHyperlink(GrvUcListado, "Select$" + e.Row.RowIndex.ToString());

            for (int c = 1; c < e.Row.Cells.Count; c++)
                e.Row.Cells[c].Attributes.Add("onclick", string.Format("HabilitarUno(document.getElementById('{0}'));", chkSeleccion.ClientID));


            if (e.Row.Cells[0].Controls.Count > 0)
            {
                LinkButton hpSeleccionar = (LinkButton)e.Row.Cells[0].Controls[0];
                hpSeleccionar.Text = string.Empty;

            }
            for (int c = ColumnaVisible; c < e.Row.Cells.Count; c++)
                e.Row.Cells[c].Visible = false;
        }

       
        if (e.Row.RowType == DataControlRowType.Header)
        {
            e.Row.Cells[1].Text = Codigo_DescripcionText;
            e.Row.Cells[2].Text = Col_DescripcionText;
            e.Row.Cells[3].Text = Col1_DescripcionText;
            e.Row.Cells[4].Text = Col2_DescripcionText;
            e.Row.Cells[5].Text = Col3_DescripcionText;
            e.Row.Cells[6].Text = Col4_DescripcionText;
            e.Row.Cells[7].Text = Col5_DescripcionText;
            e.Row.Visible = true;
             for (int c = ColumnaVisible; c < e.Row.Cells.Count; c++)
                e.Row.Cells[c].Visible = false;

          
        }

    }

     protected void GrvListado_SelectedIndexChanged(object sender, EventArgs e)
        {
            String sCodigo = GrvUcListado.SelectedRow.Cells[1].Text;
            String sDescripcionText = Convert.ToString(GrvUcListado.SelectedRow.Cells[2].Text);
            String sDescripcionText1 = Convert.ToString(GrvUcListado.SelectedRow.Cells[3].Text);
            String sDescripcionText2 = Convert.ToString(GrvUcListado.SelectedRow.Cells[4].Text);
            String sDescripcionText3 = Convert.ToString(GrvUcListado.SelectedRow.Cells[5].Text);
            String sDescripcionText4 = Convert.ToString(GrvUcListado.SelectedRow.Cells[6].Text);
            String sDescripcionText5 = Convert.ToString(GrvUcListado.SelectedRow.Cells[7].Text);
                        

            if (ucSeleccionCambiada != null)
                ucSeleccionCambiada(new ucGrvListadoSeleccionCommandEventArgs(sCodigo, sDescripcionText, sDescripcionText1, sDescripcionText2, sDescripcionText3, sDescripcionText4, sDescripcionText5));
            
        }

    protected void btnAceptar_Click(object sender, EventArgs e)
    {
        
    }
    #endregion



    protected void BtnCancelar_Click(object sender, EventArgs e)
    {

    }
}
