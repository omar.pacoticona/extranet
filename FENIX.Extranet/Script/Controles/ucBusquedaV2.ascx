<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucBusquedaV2.ascx.cs" Inherits="ucBusquedaV2" %>
<%@ Register Assembly="QNET.Web.UI" Namespace="QNET.Web.UI.Controls"  TagPrefix="FENIX" %>


<table  class="TabMantenimiento"  cellpadding="0" cellspacing="0" style="width: 100%;">

 <tr class="PanelPopupBusquedaBarra">
       
        <td colspan="2" >
            <asp:Label ID="LblTitulo" Text="Titulo" runat="server" Visible="true"></asp:Label>
        </td>
        <td  align="right">
            <asp:ImageButton ID="imgbCerrar" runat="server"  
                OnClientClick="return Cancelar();" 
                ImageUrl="~/Script/Imagenes/IconButton/Delete_Icon.png" />
          </td>
    </tr>
    
    <tr>
       
        <td >
            &nbsp;</td>
        <td >
            &nbsp;</td>
        <td >
            &nbsp;</td>
    </tr>
    <tr>
        <td>
        
            <asp:Label ID="LblDescripcion" Text="Titulo" runat="server"></asp:Label>
        </td>
        <td >
            <asp:TextBox ID="TxtDescripcion" runat="server" style="width: 128px" 
                Width="205px"></asp:TextBox>
            <asp:ImageButton ID="imgbBuscar"  OnClientClick="return Consultar();" 
                runat="server" ImageUrl="~/Script/Imagenes/b-obs.png" 
                OnClick="imgbBuscar_Click" />
            <asp:HyperLink ID="HyperLink1" runat="server">HyperLink</asp:HyperLink>
        </td>
        <td >
            &nbsp;</td>
    </tr>
    <tr>
        <td>
            <asp:Label ID="LblCodigo" runat="server" Text="C�digo"></asp:Label>
        </td>
        <td >
            <asp:TextBox ID="TxtCodigo" runat="server" Width="77px"></asp:TextBox>
        </td>
        <td >
            &nbsp;</td>
    </tr>
    <tr>
        <td colspan="3">
            &nbsp;<asp:Panel ID="plScrol"  runat="server" CssClass="CssBordeGridPop"  Height="250px" ScrollBars="Vertical"
                Width="99%">
         <%--       <ajax:UpdatePanel ID="upGrvListado" runat="server" UpdateMode="Conditional">
                <ContentTemplate>--%>
                
               
                <asp:GridView Width="99%" ID="GrvUcListado" runat="server" DataKeyNames="iIdValor,sDescripcion,sDescripcion1,sDescripcion2,sDescripcion3,sDescripcion4,sDescripcion5"
                     CellPadding="4" 
                    AutoGenerateColumns="False" OnRowDataBound="GrvListado_RowDataBound" 
                    AutoGenerateSelectButton="True"  
                    OnSelectedIndexChanged="GrvListado_SelectedIndexChanged" >
                   
                    <Columns>                        
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:CheckBox ID="ChkSeleccionar" runat="server" />
                            </ItemTemplate>
                            <ItemStyle Width="5%" />
                        </asp:TemplateField>
                        <asp:BoundField DataField="iIdValor" HeaderText="C�digo">
                        <ItemStyle Width="10px" />
                        </asp:BoundField>
                        <asp:BoundField DataField="sDescripcion" HeaderText="Descripci�n" />
                        <asp:BoundField DataField="sDescripcion1" HeaderText="Desc1" />
                        <asp:BoundField DataField="sDescripcion2" HeaderText="Desc2" />
                        <asp:BoundField DataField="sDescripcion3" HeaderText="Desc3" />
                        <asp:BoundField DataField="sDescripcion4" HeaderText="Desc4" />
                        <asp:BoundField DataField="sDescripcion5" HeaderText="Desc5" />
                    </Columns>
                    <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="false" ForeColor="#333333" />                    
                </asp:GridView>      
                
                <%-- </ContentTemplate>
                    <Triggers>
                        <ajax:AsyncPostBackTrigger ControlID="imgbBuscar" EventName="Click" />
                    </Triggers>
                </ajax:UpdatePanel>--%>
                                                                                       
            </asp:Panel>
              <FENIX:DataNavigator  ID="dnvListado" runat="server" AllowCustomPaging="true" WidthTextBoxNumPagina="25px"
                    Width="90%" GridViewId="GrvUcListado" ButtonText="Ir" PageIndicatorFormat="P�gina {0} / {1}" Visible="false" />
        </td>
    </tr>
    <tr>
        <td colspan="3" style="height: 26px">
            <asp:Button ID="BtnCancelar" runat="server" Style="position: relative; left: 164px;
                top: 0px;" Text="Cancelar" OnClientClick="Cancelar();" />
            <asp:Button ID="btnAceptar" runat="server" Style="position: relative; left: 14px;
                top: 0px;" Text="Aceptar" OnClientClick="Aceptar();" onclick="btnAceptar_Click" 
                 />
        </td>
    </tr>

    <script language="javascript" type="text/javascript">
        function fc_PressKeyBus() {
            if (event.keyCode == 13) {
                document.getElementById("<%=imgbBuscar.ClientID%>").click();
                return false;
            }
            else {
                return true;
            }
        }

        function fc_PressKeyBusGrid() {
            if (event.keyCode == 13) {
                document.getElementById("<%=imgbBuscar.ClientID%>").click();
                return false;
            }
            else {
                return true;
            }
        }
    
        function Cancelar() {
           
        }

        function Aceptar() {
            chk = false;
            f = document.forms[0].elements
            for (x = 0; x < f.length; x++) {
                obj = f[x];
                if (obj.type == 'checkbox') {
                    if (obj.checked) {
                                                       
                        return true;
                        break;
                    }
                }
            }
            if (!chk) {
                alert('Seleccione un registro');
                return false;
            }
            
        }
              
        var objFilaAnt = null;
        var backgroundColorFilaAnt = "";      
        function fc_SeleccionaFilaSimpleUc(objFila, objrowIndex, chkID) {

            try {

                if (objFilaAnt != null) {
                    objFilaAnt.style.backgroundColor = backgroundColorFilaAnt;
                }
                objFilaAnt = objFila;
                backgroundColorFilaAnt = objFila.style.backgroundColor;
                objFila.style.backgroundColor = "#c4e4ff";

               
            }

            catch (e) {
                error = e.message;
            }

        }

    

        function HabilitarUno(chk) {
            f = document.forms[0].elements;
            for (x = 0; x < f.length; x++) {
                obj = f[x];
                if (obj.type == 'checkbox') {
                    obj.checked = false;
                }
            }
            chk.enabled;
            chk.checked = true;
        } 
        function Consultar() {
            sCodigo = document.getElementById('<%=TxtCodigo.ClientID%>').value;
            sDescripcion = document.getElementById('<%=TxtDescripcion.ClientID%>').value;
            if ((sCodigo == "") && (sDescripcion == "")) {
                alert("Debe Ingresar algun criterio de busqueda");
                document.getElementById('<%=TxtDescripcion.ClientID%>').focus();
                return false;
            } else {               
                return true;
            }

        }
        
  
        

        
    </script>
 
</table>


