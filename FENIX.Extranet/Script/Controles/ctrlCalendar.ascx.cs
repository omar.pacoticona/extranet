﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using FENIX.Common;

public partial class Controles_ctrlCalendar : System.Web.UI.UserControl
{

     Boolean _Enabled=false;
    public string RetornarFechas
    {
        get { return txtFecha.Text; }
        set { txtFecha.Text = value; }
    }

    public string RetornarFecha_Ini
    {
      

        get { 
            if (txtFecha.Text.Trim().Length>0)
            {
            return   txtFecha.Text + " 00:00:00";
            }
            else { return null; }                   
        }
        set { txtFecha.Text = value; }
    }

    public string RetornarFecha_Fin
    {
        get
        {
            if (txtFecha.Text.Trim().Length > 0)
            {
                return txtFecha.Text + " 23:59:59";
            }
            else { return null; }
        }
        set { txtFecha.Text = value; }
    }
    public string TextBoxClientID
    {
        get { return txtFecha.ClientID; }
    }
    public string sRuta
    {
        get { return Constantes.RutaCalendario;}      
    }

    public Boolean Enabled
    {
        get { return txtFecha.Enabled; }
        set
        {
            txtFecha.Enabled = value;         
        }
    }

    public Unit Width
    {
        get { return txtFecha.Width; }
        set
        {
            txtFecha.Width = Width;
        }
    }
  




    protected void Page_Load(object sender, EventArgs e)
    {

    }
}
