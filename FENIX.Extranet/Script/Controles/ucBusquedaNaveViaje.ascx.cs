using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using FENIX.Common;
using FENIX.DataAccess;
using FENIX.BusinessEntity;
using QNET.Web.UI.Controls;
using System.Text;



using System.Data.SqlClient;

public partial class ucBusquedaNavVia : System.Web.UI.UserControl
{

    #region "Propiedades"

    int _ColumnaVisible = 0;
    String _Col_DescripcionText = string.Empty;
    String _Col1_DescripcionText= string.Empty;
    String _Col2_DescripcionText= string.Empty;
    String _Col3_DescripcionText = string.Empty;
    String _Col4_DescripcionText = string.Empty;
    String _Col5_DescripcionText = string.Empty;

   


    String _Titulo = string.Empty;

    Comun.Busqueda _tipoBusqueda;


    public string Titulo
    {
        get { return _Titulo; }
        set { _Titulo = value; }
    }
    public string Col_DescripcionText
    {
        get { return _Col_DescripcionText; }
        set { _Col_DescripcionText = value; }
    }
    public string Col5_DescripcionText
    {
        get { return _Col5_DescripcionText; }
        set { _Col5_DescripcionText = value; }
    }
    public string Col4_DescripcionText
    {
        get { return _Col4_DescripcionText; }
        set { _Col4_DescripcionText= value; }
    }
    public string Col3_DescripcionText
    {
        get { return _Col3_DescripcionText; }
        set { _Col3_DescripcionText = value; }
    }
    public string Col2_DescripcionText
    {
        get { return _Col2_DescripcionText; }
        set {  _Col2_DescripcionText = value; }
    }
    public string Col1_DescripcionText
    {
        get { return _Col1_DescripcionText; }
        set { _Col1_DescripcionText = value; }
    }


    public string sAnioActual
    {
        get { return Constantes.sAnioActual; }
    }

    public string Nave
    {
        get { return TxtNave.Text; }
    }

    public string AnoManifiesto
    {
        get { return TxtAnoManifiesto.Text; }       
    }

    public String NumeroManifiesto
    {
        get { return TxtNumeroManifiesto.Text; }
        
    }

    public string Viaje
    {
        get { return TxtViaje.Text; }

    }

    public string Rumbo
    {
        get { return TxtRumbo.Text; }

    }  
    public Comun.Busqueda tipoBusqueda
    {
        set { _tipoBusqueda = value; }
        get { return _tipoBusqueda; }
    }
    public int ColumnaVisible
    {
        set { _ColumnaVisible = value; }
        get { return _ColumnaVisible; }
    }

    #endregion


    #region Metodos

    protected void Page_Init(object sender, EventArgs e)
    {
       
    }

    #endregion


    #region Metodo Eventos
    protected void btnConsulta_Click(object sender, EventArgs e)
    {

    }

    protected void imgbBuscar_Click(object sender, ImageClickEventArgs e)
    {      
        dnvListado.InvokeBind();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        dnvListado.BindGridView += new QNET.Web.UI.Controls.DataNavigator.BindGridViewDelegate(BindGridLista);
        GrvListado.EmptyDataText = "No Existen Coincidencias!";
        lblTitulo.Text = Titulo;

        TxtNave.Attributes.Add("onkeydown", "javascript:return fc_PressKeyNav(event,'TxtNave');");
        TxtViaje.Attributes.Add("onkeydown", "javascript:return fc_PressKeyNav(event,'TxtViaje');");
        TxtRumbo.Attributes.Add("onkeydown", "javascript:return fc_PressKeyNav(event,'TxtRumbo');");
        TxtAnoManifiesto.Attributes.Add("onkeydown", "javascript:return fc_PressKeyNav(event,'TxtAnoManifiesto');");
        TxtNumeroManifiesto.Attributes.Add("onkeydown", "javascript:return fc_PressKeyNav(event,'TxtNumeroManifiesto');");

        GrvListado.Attributes.Add("onkeydown", "javascript:return fc_PressKeyNavGrid();");
        if (!Page.IsPostBack)
        {
            TxtAnoManifiesto.Text = Constantes.sAnioActual;
            TxtAnoManifiesto.Text = DateTime.Now.Year.ToString();
            TxtNumeroManifiesto.Focus();
           
        }
        
    }

    DataNavigatorParams BindGridLista(object sender, EventArgs e)
    {
              
        //switch (tipoBusqueda)
        //{

        //    case Comun.Busqueda.NaveViajeRumbo:
        //        {
        //            BE_NaveViaje oBE_NaveViaje = new BE_NaveViaje();
        //            DA_NaveViaje oDA_NaveViaje = new DA_NaveViaje();
        //            oBE_NaveViaje.sAnoManifiesto = AnoManifiesto;
        //            oBE_NaveViaje.sNumManifiesto = NumeroManifiesto;
        //            oBE_NaveViaje.sDesNave = Nave;
        //            oBE_NaveViaje.sDesVia = Viaje;
        //            oBE_NaveViaje.sDesRumbo = Rumbo;
        //            oBE_NaveViaje.NPagina = dnvListado.CurrentPage;
        //            oBE_NaveViaje.NRegistros = GrvListado.PageSize;
        //            IList<BE_TablaUc> listaNvr = oDA_NaveViaje.ListarManifiestosParaPop(oBE_NaveViaje);
        //            dnvListado.Visible = (oBE_NaveViaje.NTotalRegistros > oBE_NaveViaje.NRegistros);
        //            return new DataNavigatorParams(listaNvr, oBE_NaveViaje.NTotalRegistros);
        //            break;
        //        }

        //    case Comun.Busqueda.NaveViajeRumboExportacion:
        //        {
        //            BE_NaveViaje oBE_NaveViaje = new BE_NaveViaje();
        //            DA_NaveViaje oDA_NaveViaje = new DA_NaveViaje();
        //            oBE_NaveViaje.sAnoManifiesto = AnoManifiesto;
        //            oBE_NaveViaje.sNumManifiesto = NumeroManifiesto;
        //            oBE_NaveViaje.sDesNave = Nave;
        //            oBE_NaveViaje.sDesVia = Viaje;
        //            oBE_NaveViaje.sDesRumbo = Rumbo;
        //            oBE_NaveViaje.NPagina = dnvListado.CurrentPage;
        //            oBE_NaveViaje.NRegistros = GrvListado.PageSize;
        //            IList<BE_TablaUc> listaNvr = oDA_NaveViaje.ListarManifiestosPopup(oBE_NaveViaje,"E");
        //            dnvListado.Visible = (oBE_NaveViaje.NTotalRegistros > oBE_NaveViaje.NRegistros);
        //            return new DataNavigatorParams(listaNvr, oBE_NaveViaje.NTotalRegistros);
        //            break;
        //        }

        //        btnConsulta.Focus();
        //}
        return null;
    }

    protected void GrvListado_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {

            DataKey dataKey;
            dataKey = this.GrvListado.DataKeys[e.Row.RowIndex];
            BE_TablaUc oitem = (e.Row.DataItem as BE_TablaUc);

            CheckBox chkSeleccion = (CheckBox)e.Row.FindControl("chkSeleccionar_Nvr");
            e.Row.Style["cursor"] = "pointer";
            //chkSeleccion.Attributes["onclick"] = String.Format("javascript:return HabilitarUno_Nvr('{0}');", chkSeleccion.ClientID);
            //e.Row.Attributes["onclick"] = String.Format("javascript:fc_SeleccionaFilaSimpleUc_Nvr('{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}');",
            //    e.Row.RowIndex, chkSeleccion.ClientID, oitem.iIdValor, oitem.sDescripcion,
            //    oitem.sDescripcion1, oitem.sDescripcion2, oitem.sDescripcion3,
            //    oitem.sDescripcion4, oitem.sDescripcion5);

            e.Row.Attributes["onclick"] = String.Format("javascript:fc_SeleccionaFilaSimpleUc_Nvr(this,document.getElementById('{0}'),'{1}','{2}','{3}','{4}','{5}','{6}','{7}');",
               chkSeleccion.ClientID, oitem.iIdValor, oitem.sDescripcion,
               oitem.sDescripcion1, oitem.sDescripcion2, oitem.sDescripcion3,
               oitem.sDescripcion4, oitem.sDescripcion5);


            //for (int c = ColumnaVisible; c < e.Row.Cells.Count; c++)
            //    e.Row.Cells[c].Visible = false;
        }

        if (e.Row.RowType == DataControlRowType.Header)
        {
            e.Row.Cells[2].Text = Col_DescripcionText;
            e.Row.Cells[3].Text = Col1_DescripcionText;
            e.Row.Cells[4].Text = Col2_DescripcionText;
            e.Row.Cells[5].Text = Col3_DescripcionText;
            e.Row.Cells[6].Text = "Operacion";
            e.Row.Cells[7].Text = "";
        }

        //if (e.Row.RowType == DataControlRowType.Header)
        //{
        //    for (int c = ColumnaVisible; c < e.Row.Cells.Count; c++)
        //        e.Row.Cells[c].Visible = false;
            
        //}
    }

    protected void Dofocus(Control control)
    {
        ScriptManager scm = ScriptManager.GetCurrent(this.Page);
        scm.SetFocus(control);
    }

#endregion
    protected void TxtViaje_TextChanged(object sender, EventArgs e)
    {

    }
}
