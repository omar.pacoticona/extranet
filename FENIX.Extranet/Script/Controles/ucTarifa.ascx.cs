﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FENIX.Common;
using FENIX.BusinessEntity;


public partial class Script_Controles_ucTarifa : System.Web.UI.UserControl
{
    //#region "Eventos Pagina"
    //protected void Page_Load(object sender, EventArgs e)
    //{
    //    if (!Page.IsPostBack)
    //    {
    //        ListarDropDowList();           
    //    }

    //    ValidarHabilitacionCheck();
    //}

    protected void BtnGrabar_Click(object sender, EventArgs e)
    {
       // Actualizar();


    }

  
    //#endregion



    //#region "Metodo Controles"
    //protected void ValidarHabilitacionCheck()
    //{
    //    if (ChkRangoDiasIns.Checked)
    //    {
    //        TxtRangoDias1Ins.BackColor = System.Drawing.Color.White;
    //        TxtRangoDias3Ins.BackColor = System.Drawing.Color.White;
    //        TxtRangoMonto1Ins.BackColor = System.Drawing.Color.White;
    //        TxtRangoMonto2Ins.BackColor = System.Drawing.Color.White;
    //        TxtRangoMonto3Ins.BackColor = System.Drawing.Color.White;
    //    }
    //    else
    //    {
    //        TxtRangoDias1Ins.BackColor = System.Drawing.Color.Silver;
    //        TxtRangoDias3Ins.BackColor = System.Drawing.Color.Silver;
    //        TxtRangoMonto1Ins.BackColor = System.Drawing.Color.Silver;
    //        TxtRangoMonto2Ins.BackColor = System.Drawing.Color.Silver;
    //        TxtRangoMonto3Ins.BackColor = System.Drawing.Color.Silver;


    //    }

    //    if (ChkLimiteIns.Checked)
    //    {
    //        TxtMontoMinimoIns.BackColor = System.Drawing.Color.White;
    //        TxtMontoMaximoIns.BackColor = System.Drawing.Color.White;
    //    }
    //    else
    //    {

    //        TxtMontoMinimoIns.BackColor = System.Drawing.Color.Silver;
    //        TxtMontoMaximoIns.BackColor = System.Drawing.Color.Silver;
    //    }

    //    if (ChkLimiteNegoIns.Checked)
    //    {
    //        TxtMontoMinimoNegIns.BackColor = System.Drawing.Color.White;
    //        TxtMontoMaximoNegIns.BackColor = System.Drawing.Color.White;
    //    }
    //    else
    //    {
    //        TxtMontoMinimoNegIns.BackColor = System.Drawing.Color.Silver;
    //        TxtMontoMaximoNegIns.BackColor = System.Drawing.Color.Silver;

    //    }

    //}
    //protected Boolean ValidarCheck()
    //{

    //    if (ChkLimiteIns.Checked)
    //    {
    //        if ((TxtMontoMinimoIns.Text.Trim().Length == 0) ||
    //           (TxtMontoMaximoIns.Text.Trim().Length == 0))
    //        {
    //            TxtMontoMinimoIns.Focus();
    //            ScriptManager.RegisterStartupScript(TxtIdTarifaIns, GetType(), "mensaje", Resources.Resource.MsgLimiteMonto, true);
    //            return false;
    //        }
    //        if (!EsNumero(TxtMontoMaximoIns.Text))
    //        {
    //            TxtMontoMaximoIns.Focus();
    //            ScriptManager.RegisterStartupScript(TxtIdTarifaIns, GetType(), "mensaje", Resources.Resource.MsgMontoNoNumero, true);
    //            return false;
    //        }

    //        if (!EsNumero(TxtMontoMinimoIns.Text))
    //        {
    //            TxtMontoMinimoIns.Focus();
    //            ScriptManager.RegisterStartupScript(TxtIdTarifaIns, GetType(), "mensaje", Resources.Resource.MsgMontoNoNumero, true);
    //            return false;
    //        }

    //    }

    //    if (ChkLimiteNegoIns.Checked)
    //    {
    //        if ((TxtMontoMinimoNegIns.Text.Trim().Length == 0) ||
    //           (TxtMontoMaximoNegIns.Text.Trim().Length == 0))
    //        {
    //            TxtMontoMinimoNegIns.Focus();
    //            ScriptManager.RegisterStartupScript(TxtIdTarifaIns, GetType(), "mensaje", Resources.Resource.MsgLimiteMontoNeg, true);
    //            return false;
    //        }
    //        if (!EsNumero(TxtMontoMaximoNegIns.Text))
    //        {
    //            TxtMontoMaximoNegIns.Focus();
    //            ScriptManager.RegisterStartupScript(TxtIdTarifaIns, GetType(), "mensaje", Resources.Resource.MsgMontoNoNumero, true);
    //            return false;
    //        }
    //        if (!EsNumero(TxtMontoMinimoNegIns.Text))
    //        {
    //            TxtMontoMinimoNegIns.Focus();
    //            ScriptManager.RegisterStartupScript(TxtIdTarifaIns, GetType(), "mensaje", Resources.Resource.MsgMontoNoNumero, true);
    //            return false;
    //        }
    //    }

    //    if (ChkRangoDiasIns.Checked)
    //    {
    //        if ((TxtRangoDias1Ins.Text.Trim().Length == 0))
    //        {
    //            TxtRangoDias1Ins.Focus();
    //            ScriptManager.RegisterStartupScript(TxtIdTarifaIns, GetType(), "mensaje", Resources.Resource.MsgRangoDias1, true);
    //            return false;
    //        }
    //        if ((TxtRangoDias2Ins.Text.Trim().Length == 0))
    //        {
    //            TxtRangoDias2Ins.Focus();
    //            ScriptManager.RegisterStartupScript(TxtIdTarifaIns, GetType(), "mensaje", Resources.Resource.MsgRangoDias2, true);
    //            return false;
    //        }

    //        if ((TxtRangoDias3Ins.Text.Trim().Length == 0))
    //        {
    //            TxtRangoDias3Ins.Focus();
    //            ScriptManager.RegisterStartupScript(TxtIdTarifaIns, GetType(), "mensaje", Resources.Resource.MsgRangoDias3, true);
    //            return false;
    //        }

    //        if ((TxtRangoMonto1Ins.Text.Trim().Length == 0))
    //        {
    //            TxtRangoMonto1Ins.Focus();
    //            ScriptManager.RegisterStartupScript(TxtIdTarifaIns, GetType(), "mensaje", Resources.Resource.MsgRangoMonto1, true);
    //            return false;
    //        }
    //        if ((TxtRangoMonto2Ins.Text.Trim().Length == 0))
    //        {
    //            TxtRangoMonto2Ins.Focus();
    //            ScriptManager.RegisterStartupScript(TxtIdTarifaIns, GetType(), "mensaje", Resources.Resource.MsgRangoMonto2, true);
    //            return false;
    //        }

    //        if ((TxtRangoMonto3Ins.Text.Trim().Length == 0))
    //        {
    //            TxtRangoMonto3Ins.Focus();
    //            ScriptManager.RegisterStartupScript(TxtIdTarifaIns, GetType(), "mensaje", Resources.Resource.MsgRangoMonto3, true);
    //            return false;
    //        }
    //    }


    //    if (!EsNumero(TxtMontoIns.Text))
    //    {
    //        TxtMontoIns.Focus();
    //        ScriptManager.RegisterStartupScript(TxtIdTarifaIns, GetType(), "mensaje", Resources.Resource.MsgMontoNoNumero, true);
    //        return false;
    //    }
    //    if (!EsNumero(TxtCostoIns.Text))
    //    {
    //        TxtCostoIns.Focus();
    //        ScriptManager.RegisterStartupScript(TxtIdTarifaIns, GetType(), "mensaje", Resources.Resource.MsgMontoNoNumero, true);
    //        return false;
    //    }


    //    return true;

    //}
    //private bool EsNumero(string cadena)
    //{
    //    //Sencillamente, si se logra hacer la conversión, entonces es número
    //    try
    //    {
    //        decimal resp = Convert.ToDecimal(cadena);
    //        return true;
    //    }
    //    catch //caso contrario, es falso.
    //    {
    //        return false;
    //    }

    //}   
    //protected void HabilitarControles(Boolean pValor)
    //{
    //    TxtIdServicioIns.Enabled = pValor;
    //    TxtServicioDescripIns.Enabled = pValor;
    //    DplOperacionIns.Enabled = pValor;
    //    DplCondicionCargaIns.Enabled = pValor;
    //    DplTamanoCntIns.Enabled = pValor;
    //    //TxtTipoCntDescripIns.Enabled = pValor;
    //    //TxtIdTipoCntIns.Enabled = pValor;
    //    //TxtIdCargaIns.Enabled = pValor;
    //    //TxtCargaDescripIns.Enabled = pValor;
    //    //TxtIdEmbalajeIns.Enabled = pValor;
    //    //TxtEmbalajeDescripIns.Enabled = pValor;
    //    DplTerminalPortuarioIns.Enabled = pValor;
    //    ChkCargaPeligrosaIns.Enabled = pValor;
    //    ChkObligatorioIns.Enabled = pValor;
    //    ChkTarifaSliIns.Enabled = pValor;
    //    ChkAlmacenajeIns.Enabled = pValor;
    //    //TxtIdCostoIns.Enabled = pValor;
    //    //TxtCostoDescripIns.Enabled = pValor;
    //    TxTarifaDescripIns.Enabled = pValor;
    //    TxtMontoMaximoIns.Enabled = pValor;
    //    TxtMontoMinimoIns.Enabled = pValor;
    //    DplModalidadIns.Enabled = pValor;
    //    DplMonedaIns.Enabled = pValor;
    //    TxtMontoIns.Enabled = pValor;
    //    TxtDiasLibresIns.Enabled = pValor;
    //    TxtCostoIns.Enabled = pValor;
    //    ChkLimiteIns.Enabled = pValor;
    //    TxtMontoMaximoIns.Enabled = pValor;
    //    TxtMontoMinimoIns.Enabled = pValor;
    //    ChkRangoDiasIns.Enabled = pValor;
    //    TxtRangoDias1Ins.Enabled = pValor;
    //    TxtRangoDias2Ins.Enabled = pValor;
    //    TxtRangoDias3Ins.Enabled = pValor;
    //    TxtRangoMonto1Ins.Enabled = pValor;
    //    TxtRangoMonto2Ins.Enabled = pValor;
    //    TxtRangoMonto3Ins.Enabled = pValor;
    //    //ImbCosto.Enabled = pValor;
    //    //ImbEmbalaje.Enabled = pValor;
    //    ImbServicio.Enabled = pValor;
    //    //ImbTipoContenedor.Enabled = pValor;
    //    //BtnTipoCarga.Enabled = pValor;
    //    ChkLimiteNegoIns.Enabled = pValor;
    //    TxtMontoMaximoNegIns.Enabled = pValor;
    //    TxtMontoMinimoNegIns.Enabled = pValor;


    //}

    //#endregion
    //#region "Metodo Transacciones"
    //protected void Actualizar()
    //{
    //    try
    //    {

    //        BL_Tarifa oBL_Tarifa = new BL_Tarifa();
    //        BE_Tarifa oBE_Tarifa = new BE_Tarifa();
    //        ResultadoTransaccionTx oResultadoTransaccionTx = null;

    //        if (ValidarCheck() != true)
    //        {
    //            return;
    //        }

    //        oBE_Tarifa.iIdTarifa = Convert.ToInt32(TxtIdTarifaIns.Text);
    //        oBE_Tarifa.iIdServicio = Convert.ToInt32(TxtIdServicioIns.Text);
    //        oBE_Tarifa.sDescripcionServicio = TxtServicioDescripIns.Text;
    //        oBE_Tarifa.iIdTipoOperacion = Convert.ToInt32(DplOperacionIns.SelectedValue);
    //        oBE_Tarifa.iIdCondicionCarga = Convert.ToInt32(DplCondicionCargaIns.SelectedValue);
    //        oBE_Tarifa.iIdTamanoCnt = Convert.ToInt32(DplTamanoCntIns.SelectedValue);
    //        //oBE_Tarifa.sDescripcionTcontenedor = TxtTipoCntDescripIns.Text;
    //        //oBE_Tarifa.iIdTipoCnt = Convert.ToInt32(TxtIdTipoCntIns.Text);
    //        //oBE_Tarifa.iIdCarga = Convert.ToInt32(TxtIdCargaIns.Text);
    //        //oBE_Tarifa.sDescripcionTcarga = TxtCargaDescripIns.Text;
    //        //oBE_Tarifa.iIdEmbalaje = Convert.ToInt32(TxtIdEmbalajeIns.Text);
    //        //oBE_Tarifa.sDescripcionTembalaje = TxtEmbalajeDescripIns.Text;
    //        oBE_Tarifa.iIdTerminalPorturario = Convert.ToInt32(DplTerminalPortuarioIns.SelectedValue);
    //        //oBE_Tarifa.iIdCentroCosto = Convert.ToInt32(TxtIdCostoIns.Text);

    //        if (ChkCargaPeligrosaIns.Checked)
    //            oBE_Tarifa.sFlagTarifaPeligrosa = "1";
    //        else
    //            oBE_Tarifa.sFlagTarifaPeligrosa = "0";

    //        if (ChkObligatorioIns.Checked)
    //            oBE_Tarifa.sFlagMandatorio = "1";
    //        else
    //            oBE_Tarifa.sFlagMandatorio = "0";

    //        if (ChkTarifaSliIns.Checked)
    //            oBE_Tarifa.sFlagTarifaSLI = "1";
    //        else
    //            oBE_Tarifa.sFlagTarifaSLI = "0";

    //        if (ChkAlmacenajeIns.Checked)
    //            oBE_Tarifa.sFlagAlmacenajeRetroactivo = "1";
    //        else
    //            oBE_Tarifa.sFlagAlmacenajeRetroactivo = "0";

    //        if (ChkLimiteIns.Checked)
    //        {
    //            oBE_Tarifa.sFlagLimite = "1";
    //            oBE_Tarifa.dMontoMaximo = Convert.ToDecimal(TxtMontoMaximoIns.Text);
    //            oBE_Tarifa.dMontoMinimo = Convert.ToDecimal(TxtMontoMinimoIns.Text);
    //        }
    //        else
    //            oBE_Tarifa.sFlagLimite = "0";


    //        if (ChkLimiteNegoIns.Checked)
    //        {
    //            oBE_Tarifa.sFlagLimiteNego = "1";
    //            oBE_Tarifa.dMontoMaximoNeg = Convert.ToDecimal(TxtMontoMaximoNegIns.Text);
    //            oBE_Tarifa.dMontoMinimoNeg = Convert.ToDecimal(TxtMontoMinimoNegIns.Text);
    //        }
    //        else
    //            oBE_Tarifa.sFlagLimiteNego = "0";

    //        if (ChkRangoDiasIns.Checked)
    //        {
    //            oBE_Tarifa.sFlagRangoDias = "1";
    //            oBE_Tarifa.iRangoDias1 = Convert.ToInt32(TxtRangoDias1Ins.Text);
    //            oBE_Tarifa.iRangoDias2 = Convert.ToInt32(TxtRangoDias2Ins.Text);
    //            oBE_Tarifa.iRangoDias3 = Convert.ToInt32(TxtRangoDias3Ins.Text);
    //            oBE_Tarifa.dRangoMonto1 = Convert.ToDecimal(TxtRangoMonto1Ins.Text);
    //            oBE_Tarifa.dRangoMonto2 = Convert.ToDecimal(TxtRangoMonto2Ins.Text);
    //            oBE_Tarifa.dRangoMonto3 = Convert.ToDecimal(TxtRangoMonto3Ins.Text);
    //        }
    //        else
    //            oBE_Tarifa.sFlagRangoDias = "0";
    //        oBE_Tarifa.sDescripcion = TxTarifaDescripIns.Text;

    //        oBE_Tarifa.iIdModalidad = Convert.ToInt32(DplModalidadIns.SelectedValue);
    //        oBE_Tarifa.iIdMoneda = Convert.ToInt32(DplMonedaIns.SelectedValue);
    //        oBE_Tarifa.dMonto = Convert.ToDecimal(TxtMontoIns.Text);
    //        oBE_Tarifa.iDiasLibresAlmacenaje = Convert.ToInt32(TxtDiasLibresIns.Text);
    //        oBE_Tarifa.dCosto = Convert.ToDecimal(TxtCostoIns.Text);


    //        oBE_Tarifa.sNombrePc = "";
    //        oBE_Tarifa.sUsuario = "";

    //        oResultadoTransaccionTx = oBL_Tarifa.Actualizar(oBE_Tarifa);

    //        if (oResultadoTransaccionTx.EnuTipoResultado == ResultadoTransaccionTx.TipoResultado.Exito)
    //        {
    //            ScriptManager.RegisterStartupScript(TxtIdTarifaIns, GetType(), "mensaje", Resources.Resource.MsgAlertActuazaOk, true);
              
    //            BtnGrabar.Enabled = false;
              
    //        }
    //        else
    //        {
    //            ScriptManager.RegisterStartupScript(TxtIdTarifaIns, GetType(), "mensaje", Resources.Resource.MsgAlertActuazaError, true);
    //        }
    //    }
    //    catch (Exception ex)
    //    {
    //        new ResultadoInterfase(ResultadoInterfase.TipoResultado.Error, ex);
    //    }

    //}
    
    //public void ConsultarTarifa(int idTarifa)
    //{
        
    //    BE_Tarifa oBE_Tarifa = new BE_Tarifa();
    //    BL_Tarifa oBL_Tarifa = new BL_Tarifa();

    //    HabilitarControles(true);
    //    BtnGrabar.Enabled = true;

    //    oBE_Tarifa = oBL_Tarifa.ListarPorId(idTarifa);

    //    TxtIdTarifaIns.Text = Convert.ToString(oBE_Tarifa.iIdTarifa);
    //    TxtIdServicioIns.Text = Convert.ToString(oBE_Tarifa.iIdServicio);
    //    TxtServicioDescripIns.Text = oBE_Tarifa.sDescripcionServicio;
    //    if (oBE_Tarifa.iIdTipoOperacion != 0)
    //    {
    //        DplOperacionIns.SelectedValue = Convert.ToString(oBE_Tarifa.iIdTipoOperacion);
    //    }
    //    if (oBE_Tarifa.iIdCondicionCarga > 0)
    //        DplCondicionCargaIns.SelectedValue = Convert.ToString(oBE_Tarifa.iIdCondicionCarga);

    //    DplTamanoCntIns.SelectedValue = Convert.ToString(oBE_Tarifa.iIdTamanoCnt);        
    //    if (oBE_Tarifa.sFlagTarifaPeligrosa == "1")
    //        ChkCargaPeligrosaIns.Checked = true;
    //    else
    //        ChkCargaPeligrosaIns.Checked = false;

    //    if (oBE_Tarifa.sFlagMandatorio == "1")
    //        ChkObligatorioIns.Checked = true;
    //    else
    //        ChkObligatorioIns.Checked = false;

    //    if (oBE_Tarifa.sFlagTarifaSLI == "1")
    //        ChkTarifaSliIns.Checked = true;
    //    else
    //        ChkTarifaSliIns.Checked = false;

    //    if (oBE_Tarifa.sFlagAlmacenajeRetroactivo == "1")
    //        ChkAlmacenajeIns.Checked = true;
    //    else
    //        ChkAlmacenajeIns.Checked = false;

    //    if (oBE_Tarifa.sFlagLimite == "1")
    //        ChkLimiteIns.Checked = true;
    //    else
    //        ChkLimiteIns.Checked = false;

    //    if (oBE_Tarifa.sFlagLimiteNego == "1")
    //        ChkLimiteNegoIns.Checked = true;
    //    else
    //        ChkLimiteNegoIns.Checked = false;

    //    if (oBE_Tarifa.sFlagRangoDias == "1")
    //        ChkRangoDiasIns.Checked = true;
    //    else
    //        ChkRangoDiasIns.Checked = false;
       
        
    //    TxTarifaDescripIns.Text = oBE_Tarifa.sDescripcion;
    //    TxtMontoMaximoIns.Text = Convert.ToString(oBE_Tarifa.dMontoMaximo);
    //    TxtMontoMinimoIns.Text = Convert.ToString(oBE_Tarifa.dMontoMinimo);
    //    DplModalidadIns.SelectedValue = Convert.ToString(oBE_Tarifa.iIdModalidad);
    //    DplMonedaIns.SelectedValue = Convert.ToString(oBE_Tarifa.iIdMoneda);
    //    TxtMontoIns.Text = Convert.ToString(oBE_Tarifa.dMonto);
    //    TxtDiasLibresIns.Text = Convert.ToString(oBE_Tarifa.iDiasLibresAlmacenaje);
    //    TxtCostoIns.Text = Convert.ToString(oBE_Tarifa.dCosto);
    //    TxtMontoMaximoNegIns.Text = Convert.ToString(oBE_Tarifa.dMontoMaximoNeg);
    //    TxtMontoMinimoNegIns.Text = Convert.ToString(oBE_Tarifa.dMontoMinimoNeg);
    //    TxtRangoDias1Ins.Text = Convert.ToString(oBE_Tarifa.iRangoDias1);
    //    TxtRangoDias2Ins.Text = Convert.ToString(oBE_Tarifa.iRangoDias2);
    //    TxtRangoDias3Ins.Text = Convert.ToString(oBE_Tarifa.iRangoDias3);
    //    TxtRangoMonto1Ins.Text = Convert.ToString(oBE_Tarifa.dRangoMonto1);
    //    TxtRangoMonto2Ins.Text = Convert.ToString(oBE_Tarifa.dRangoMonto2);
    //    TxtRangoMonto3Ins.Text = Convert.ToString(oBE_Tarifa.dRangoMonto3);
    //    TxtIdContrato.Text = Convert.ToString(oBE_Tarifa.iIdContrato);
    //    TxtTipoContrato.Text = oBE_Tarifa.sDescripcionTipoTarifa;
    //    if (oBE_Tarifa.iIdTerminalPorturario!= 0)
    //    {
    //        DplTerminalPortuarioIns.SelectedValue = Convert.ToString(oBE_Tarifa.iIdTerminalPorturario);
    //    }
    //}

    //public void ListarDropDowList()
    //{
    //    try
    //    {

    //        BL_Tarifa oBL_Tarifa = new BL_Tarifa();

    //        List<BE_Tabla> olst_TablaTipo = new List<BE_Tabla>();

    //        int i = 0;
    //        DplOperacionIns.Items.Clear();

    //        olst_TablaTipo = oBL_Tarifa.ListarTipoOperacion();
    //        DplOperacionIns.Items.Add(new ListItem("[Seleccionar]", "-1"));
    //        for (i = 0; i < olst_TablaTipo.Count; ++i)
    //        {
    //            DplOperacionIns.Items.Add(new ListItem(olst_TablaTipo[i].sDescripcion.ToString(), olst_TablaTipo[i].iIdValor.ToString()));
    //        }
    //        DplOperacionIns.SelectedIndex = -1;

    //        olst_TablaTipo = null;

    //        i = 0;
    //        DplCondicionCargaIns.Items.Clear();

    //        olst_TablaTipo = oBL_Tarifa.ListarCondicionCarga();
    //        DplCondicionCargaIns.Items.Add(new ListItem("[Seleccionar]", "-1"));
    //        for (i = 0; i < olst_TablaTipo.Count; ++i)
    //        {
    //            DplCondicionCargaIns.Items.Add(new ListItem(olst_TablaTipo[i].sDescripcion.ToString(), olst_TablaTipo[i].iIdValor.ToString()));
    //        }
    //        DplCondicionCargaIns.SelectedIndex = -1;

    //        olst_TablaTipo = null;

    //        i = 0;
    //        DplTamanoCntIns.Items.Clear();

    //        olst_TablaTipo = oBL_Tarifa.ListarTamanoContenedor();
    //        DplTamanoCntIns.Items.Add(new ListItem("", "0"));
    //        for (i = 0; i < olst_TablaTipo.Count; ++i)
    //        {
    //            DplTamanoCntIns.Items.Add(new ListItem(olst_TablaTipo[i].sDescripcion.ToString(), olst_TablaTipo[i].iIdValor.ToString()));
    //        }
    //        DplTamanoCntIns.SelectedIndex = -1;

    //        olst_TablaTipo = null;

    //        i = 0;
    //        DplTerminalPortuarioIns.Items.Clear();

    //        olst_TablaTipo = oBL_Tarifa.ListarTerminalPortuario();
    //        DplTerminalPortuarioIns.Items.Add(new ListItem("[Seleccionar]", "-1"));
    //        for (i = 0; i < olst_TablaTipo.Count; ++i)
    //        {
    //            DplTerminalPortuarioIns.Items.Add(new ListItem(olst_TablaTipo[i].sDescripcion.ToString(), olst_TablaTipo[i].iIdValor.ToString()));
    //        }
    //        DplTerminalPortuarioIns.SelectedIndex = -1;

    //        olst_TablaTipo = null;


    //        i = 0;
    //        DplModalidadIns.Items.Clear();

    //        olst_TablaTipo = oBL_Tarifa.ListarModalidad();
    //        DplModalidadIns.Items.Add(new ListItem("", "0"));
    //        for (i = 0; i < olst_TablaTipo.Count; ++i)
    //        {
    //            DplModalidadIns.Items.Add(new ListItem(olst_TablaTipo[i].sDescripcion.ToString(), olst_TablaTipo[i].iIdValor.ToString()));
    //        }
    //        DplModalidadIns.SelectedIndex = -1;

    //        olst_TablaTipo = null;

    //        i = 0;
    //        DplMonedaIns.Items.Clear();

    //        olst_TablaTipo = oBL_Tarifa.ListarTipoMoneda();
    //        DplMonedaIns.Items.Add(new ListItem("[Seleccionar]", "-1"));
    //        for (i = 0; i < olst_TablaTipo.Count; ++i)
    //        {
    //            DplMonedaIns.Items.Add(new ListItem(olst_TablaTipo[i].sDescripcion.ToString(), olst_TablaTipo[i].iIdValor.ToString()));
    //        }
    //        DplMonedaIns.SelectedIndex = -1;

    //        olst_TablaTipo = null;

    //    }
    //    catch (Exception e)
    //    {
    //        new ResultadoInterfase(ResultadoInterfase.TipoResultado.Error, e);
    //    }

    //}
    //#endregion

   
}
