﻿using System;
using System.Collections.Generic;
using System.Text;
using FENIX.BusinessEntity;
using FENIX.DataAccess;

namespace FENIX.BusinessLogic
{
    public class BL_Liquidacion
    {
        #region "Transaccion"
        public String EliminarLiquidacion(int iIdLiquidacion, string sUsuario, string sNombrePc)
        {
            DA_Liquidacion oDA_Liquidacion = new DA_Liquidacion();
            return oDA_Liquidacion.EliminarLiquidacion(iIdLiquidacion, sUsuario, sNombrePc);
        }
        public int RegistrarServicios_Mandatorios(int? idContrato, Int32? iIdVolante, int? iIdDocOri, int IdOperacion)
        {
            DA_Liquidacion oDA_Liquidacion = new DA_Liquidacion();
            return oDA_Liquidacion.RegistrarServicios_Mandatorios(idContrato, iIdVolante, iIdDocOri, IdOperacion);
        }
        public void RegistrarDescuento(int iTipo, decimal iValor, int IdOrdSer, int IdOrdSerDet, decimal dMontoAfecto)
        {
            DA_Liquidacion oDA_Liquidacion = new DA_Liquidacion();
            oDA_Liquidacion.RegistrarDescuento(iTipo, iValor, IdOrdSer, IdOrdSerDet, dMontoAfecto);
        }
        public string Listar_Contrato(Int32? iIdVolante, int? iIdDocOri, int iIdOperacion)
        {
            DA_Liquidacion oDA_Liquidacion = new DA_Liquidacion();
            return oDA_Liquidacion.Listar_Contrato(iIdVolante, iIdDocOri, iIdOperacion);
        }

        public String RegistrarLiquidacion(BE_Liquidacion oBE_Liquidacion, BE_LiquidacionList oBE_LiquidacionLis)
        {
            DA_Liquidacion oDA_Liquidacion = new DA_Liquidacion();
            return oDA_Liquidacion.RegistrarLiquidacion(oBE_Liquidacion, oBE_LiquidacionLis);
        }

        public String ActualizarLiquidacion(Int32 iIdLiq, Int32 iIdClienteFacturar)
        {
            DA_Liquidacion oDA_Liquidacion = new DA_Liquidacion();
            return oDA_Liquidacion.ActualizarLiquidacion(iIdLiq, iIdClienteFacturar);
        }

        public void ActualizarNroCuenta(Int32 IdDocOri, String sNroCuenta)
        {
            DA_Liquidacion oDA_Liquidacion = new DA_Liquidacion();
            oDA_Liquidacion.ActualizarNroCuenta(IdDocOri, sNroCuenta);

        }

        public String Insertar_PreLiquidacion(BE_Liquidacion oBE_Liquidacion, List<BE_Liquidacion> lst_BE_Liquidacion)
        {
            DA_Liquidacion oDA_Liquidacion = new DA_Liquidacion();
            return oDA_Liquidacion.Insertar_PreLiquidacion(oBE_Liquidacion, lst_BE_Liquidacion);
        }
        #endregion

        #region "Listado"

        public string Validar_NumeroDo(String sNumeroDo)
        {
            DA_Liquidacion oDA_Liquidacion = new DA_Liquidacion();
            return oDA_Liquidacion.Validar_NumeroDo(sNumeroDo);

        }

        //public BE_DuaList Listar_Dua_DocOrigen_Registrado(Int32 iIdDocOri)
        //{
        //    DA_Liquidacion oDA_Liquidacion = new DA_Liquidacion();
        //    return oDA_Liquidacion.Listar_Dua_DocOrigen_Registrado(iIdDocOri);
        //}

        public BE_LiquidacionList Listar_Linquidacion_Servicios_Registrado(int idLiquidacion)
        {
            DA_Liquidacion oDA_Liquidacion = new DA_Liquidacion();
            return oDA_Liquidacion.Listar_Linquidacion_Servicios_Registrado(idLiquidacion);
        }

        public BE_LiquidacionList Listar_Linquidacion_Servicios(BE_Liquidacion oBE_Liquidacion)
        {
            DA_Liquidacion oDA_Liquidacion = new DA_Liquidacion();
            return oDA_Liquidacion.Listar_Linquidacion_Servicios(oBE_Liquidacion);
        }
        public BE_LiquidacionList Listar_Buscar_Info_Liquidacion(Int32 iIdCodigo, Int32 iIdOperacion)
        {
            DA_Liquidacion oDA_Liquidacion = new DA_Liquidacion();
            return oDA_Liquidacion.Listar_Buscar_Info_Liquidacion(iIdCodigo, iIdOperacion);
        }

        //public BE_DuaList Listar_Dua_DocOrigen(Int32 iIdDocOri)
        //{
        //    DA_Liquidacion oDA_Liquidacion = new DA_Liquidacion();
        //    return oDA_Liquidacion.Listar_Dua_DocOrigen(iIdDocOri);
        //}

        //public BE_DuaList Listar_Dua_Detalle(String sIdCodigo, Int32 iIdOperacion)
        //{
        //    DA_Liquidacion oDA_Liquidacion = new DA_Liquidacion();
        //    return oDA_Liquidacion.Listar_Dua_Detalle(sIdCodigo, iIdOperacion);
        //}

        public BE_Liquidacion Listar_PreLiquidacion_Info_Cabecera(String sNumeroVolante, Int32 iIdOperacion, Int32? iIdDocOri, Int32? iIdNavVia)
        {
            DA_Liquidacion oDA_Liquidacion = new DA_Liquidacion();
            return oDA_Liquidacion.Listar_PreLiquidacion_Info_Cabecera(sNumeroVolante, iIdOperacion, iIdDocOri, iIdNavVia);
        }
        public BE_LiquidacionList Listar_PreLinquidacion(BE_Liquidacion oBE_Liquidacion)
        {
            DA_Liquidacion oDA_Liquidacion = new DA_Liquidacion();
            return oDA_Liquidacion.Listar_PreLinquidacion(oBE_Liquidacion);
        }
        public List<BE_Tabla> ListarMoneda()
        {
            DA_ConsultasComunes oDA_ConsultasComunes = new DA_ConsultasComunes();
            return oDA_ConsultasComunes.ListarTabla(30, 0);
        }
        public BE_LiquidacionList Listar_Linquidacion(BE_Liquidacion oBE_Liquidacion)
        {
            DA_Liquidacion oDA_Liquidacion = new DA_Liquidacion();
            return oDA_Liquidacion.Listar_Linquidacion(oBE_Liquidacion);
        }

        public BE_LiquidacionList Listar_Info_DocumentoOrigen(BE_Liquidacion oBE_Liquidacion)
        {
            DA_Liquidacion oDA_Liquidacion = new DA_Liquidacion();
            return oDA_Liquidacion.Listar_Info_DocumentoOrigen(oBE_Liquidacion);
        }

        public BE_LiquidacionList Listar_Contenedores_Liquidar(BE_Liquidacion oBE_Liquidacion)
        {
            DA_Liquidacion oDA_Liquidacion = new DA_Liquidacion();
            return oDA_Liquidacion.Listar_Contenedores_Liquidar(oBE_Liquidacion);
        }
        public String Listar_Recuperar_Contrato_2(Int32? iIdNegocio, Int32? iIdVolante, int? iIdDocOri, int iIdOperacion, String sMasterLCL)
        {
            return new DA_Liquidacion().Listar_Recuperar_Contrato_2(iIdNegocio, iIdVolante, iIdDocOri, iIdOperacion, sMasterLCL);
        }

        public BE_LiquidacionList Listar_Linquidacion_Pendiente(BE_Liquidacion oBE_Liquidacion)
        {
            DA_Liquidacion oDA_Liquidacion = new DA_Liquidacion();
            return oDA_Liquidacion.Listar_Linquidacion_Pendiente(oBE_Liquidacion);
        }
        public Int32 RegistrarServicios_Mandatorios_2(Int32? iIdLineaNegocio, Int32? iIdOperacion, Int32? iIdDocOri, Int32? iIdAcuerdo, Int32? iIdVolante, String sMasterLCL)
        {
            return new DA_Liquidacion().RegistrarServicios_Mandatorios_2(iIdLineaNegocio, iIdOperacion, iIdDocOri, iIdAcuerdo, iIdVolante, sMasterLCL);
        }

        public String Actualizar_FechaRetiroXContenedor(Int32? IdDocOriDet, DateTime? FechaRetiro)
        {
            return new DA_Liquidacion().Actualizar_FechaRetiroXContenedor(IdDocOriDet, FechaRetiro);
        }
        
        public BE_LiquidacionList Listar_PreLiquidacion_2(BE_Liquidacion oBE_Liquidacion)
        {
            return new DA_Liquidacion().Listar_PreLiquidacion_2(oBE_Liquidacion);
        }

        public BE_LiquidacionList Listar_Estados_ParaAlertas(BE_Liquidacion oBE_Liquidacion)
        {
            return new DA_Liquidacion().Listar_Estados_ParaAlertas(oBE_Liquidacion);
        }
        
        public BE_LiquidacionList Listar_Liquidacion_PorId(Int32 idLiq)
        {
            DA_Liquidacion oDA_Liquidacion = new DA_Liquidacion();
            return oDA_Liquidacion.Listar_Liquidacion_PorId(idLiq);
        }
        public String ListarRegistroPorID(String sCodigo, String sTabla, String sCampoCondicion, String sCampoMostrar)
        {
            DA_ConsultasComunes oDA_ConsultasComunes = new DA_ConsultasComunes();
            return oDA_ConsultasComunes.ListarRegistroPorID(sCodigo, sTabla, sCampoCondicion, sCampoMostrar);
        }
        public String Validar_TipoCambio()
        {
            DA_Liquidacion oDA_Liquidacion = new DA_Liquidacion();
            return oDA_Liquidacion.Validar_TipoCambio();
        }


        /// LIQUIDACION - FRANK 
        public BE_Liquidacion Listar_PreLiquidacion_Info_Cabecera_2(Int32 iIdLineaNegocio, Int32 iIdOperacion, String sNumeroDocumento, String sMasterLCL, Int32 iOficina, Int32 IdCliente)
        {
            return new DA_Liquidacion().Listar_PreLiquidacion_Info_Cabecera_2(iIdLineaNegocio, iIdOperacion, sNumeroDocumento, sMasterLCL, iOficina,IdCliente);
        }

        public Int32 ConsultarEstadoAcuerdo_2(Int32? iIdAcuerdo)
        {
            return new DA_Liquidacion().ConsultarEstadoAcuerdo_2(iIdAcuerdo);
        }
        public Decimal IGV()
        {
            DA_Liquidacion oDA_IGV = new DA_Liquidacion();
            return oDA_IGV.IGV();
        }

        public String Operacion(String Documento)
        {
            DA_Liquidacion oDA_Operacion = new DA_Liquidacion();
            return oDA_Operacion.Operacion(Documento);
        }

        public Decimal TipoCambio()
        {
            DA_Liquidacion oDA_TipoCambio = new DA_Liquidacion();
            return oDA_TipoCambio.TipoCambio();
        }

        public String Insertar_PreLiquidacion_2(BE_Liquidacion oBE_Liquidacion, BE_LiquidacionList oBE_LiquidacionLis)
        {
            return new DA_Liquidacion().Insertar_PreLiquidacion_2(oBE_Liquidacion, oBE_LiquidacionLis);
        }
        public BE_LiquidacionList Listar_Liquidacion_2(BE_Liquidacion oBE_Liquidacion)
        {
            return new DA_Liquidacion().Listar_Liquidacion_2(oBE_Liquidacion);
        }

        public String PreLiquidacion_EnviarAlertas(BE_Liquidacion oBE_Liquidacion)
        {
            return new DA_Liquidacion().PreLiquidacion_EnviarAlertas(oBE_Liquidacion);
        }

        public String PreLiquidacion_Ins_SolicitudPreLiquidacion(BE_Liquidacion oBE_Liquidacion)
        {
            return new DA_Liquidacion().PreLiquidacion_Ins_SolicitudPreLiquidacion(oBE_Liquidacion);
        }

        public String PreLiquidacion_Ins_Validaciones(BE_Liquidacion oBE_Liquidacion)
        {
            return new DA_Liquidacion().PreLiquidacion_Ins_Validaciones(oBE_Liquidacion);
        }

        public String PreLiquidacion_Ins_ServiciosPreValidacion(BE_Liquidacion oBE_Liquidacion)
        {
            return new DA_Liquidacion().PreLiquidacion_Ins_ServiciosPreValidacion(oBE_Liquidacion);
        }

        public String PreLiquidacion_Lis_ValidacionesServicioPendiente(BE_Liquidacion oBE_Liquidacion)
        {
            return new DA_Liquidacion().PreLiquidacion_Lis_ValidacionesServicioPendiente(oBE_Liquidacion);
        }

        





        #endregion
    }
}
