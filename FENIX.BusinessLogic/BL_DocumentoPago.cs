﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FENIX.Common;
using FENIX.DataAccess;
using FENIX.BusinessEntity;
using System.Data;

namespace FENIX.BusinessLogic
{
    public class BL_DocumentoPago
    {

        public BE_DocumentoPagoList ListarDocumentoPago(BE_DocumentoPago oBE_DocumentoPago)
        {
            DA_DocumentoPago oDA_DocumentoPago = new DA_DocumentoPago();
            return oDA_DocumentoPago.ListarDocumentoPago(oBE_DocumentoPago);
        }

        public List<BE_LineaNegocio> ListarNegocio()
        {
            DA_DocumentoPago oDA_DocumentoPago = new DA_DocumentoPago();
            return oDA_DocumentoPago.ListarLineaNegocio();
        }
        public List<BE_TipoDocumentoPago> ListarTipoDocumentoPago()
        {
            DA_DocumentoPago oDA_DocumentoPago = new DA_DocumentoPago();
            return oDA_DocumentoPago.ListarTipoDocumentoPago();
        }

        public List<BE_Tabla> ListarTipoPago()
        {
            DA_ConsultasComunes oDA_ListarMoneda = new DA_ConsultasComunes();
            return oDA_ListarMoneda.ListarTabla(111, 0);
        }

        public List<BE_Tabla> ListarFormaDePago()
        {
            DA_ConsultasComunes oDA_ListarMoneda = new DA_ConsultasComunes();
            return oDA_ListarMoneda.ListarTabla(93, 0);
        }

        public List<BE_Tabla> ListarTablaFormagoPago()
        {
            DA_ConsultasComunes oDA_ListarMoneda = new DA_ConsultasComunes();
            return oDA_ListarMoneda.ListarTablaFormagoPago(93, 0,2);
        }
        
        public List<BE_Tabla> ListarBanco(int idBanco)
        {
            DA_ConsultasComunes oDA_ListarMoneda = new DA_ConsultasComunes();
            return oDA_ListarMoneda.ListarBanco(idBanco);
        }


        public List<BE_Tabla> ListarTipoMoneda()
        {
            DA_ConsultasComunes oDA_Bitacora = new DA_ConsultasComunes();
            return oDA_Bitacora.ListarTabla(30, 0);
        }

        public List<BE_Tabla> ListarRegimen()
        {

            DA_ConsultasComunes oDA_Bitacora = new DA_ConsultasComunes();
            return oDA_Bitacora.ListarRegimen();
        }

    }
}
