﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FENIX.DataAccess;
using FENIX.BusinessEntity;

namespace FENIX.BusinessLogic
{
   public class BL_Refrendo
    {
        public List<BE_Refrendo> ListarDatosLiquidacion(BE_Refrendo oBE_Refrendo)
        {
            return new DA_Refrendo().ListarDatosXBooking_Refrendo(oBE_Refrendo);
        }
        public List<BE_Refrendo> ListarDetalleDocumento(BE_Refrendo oBE_Refrendo)
        {
            return new DA_Refrendo().ListarDetalleDocumento(oBE_Refrendo);
        }


        public List<BE_Refrendo> ListarArchivosXClienteUsuario(BE_Refrendo oBE_Refrendo)
        {
            return new DA_Refrendo().ListarArchivosXClienteUsuario(oBE_Refrendo);
        }

        public List<BE_Refrendo> Listar_Despachadores(String _codigoRUC)
        {
            return new DA_Refrendo().Listar_Despachadores(_codigoRUC);
        }

        

        public String ValidarRUCCliente_ParaDespachador(BE_Refrendo oBE_Refrendo)
        {
            return new DA_Refrendo().ValidarRUCCliente_ParaDespachador(oBE_Refrendo);
        }
        
        public List<BE_Refrendo> ListarSolicitudRefrendo(BE_Refrendo oBE_Refrendo)
        {
            return new DA_Refrendo().ListarSolicitudRefrendo(oBE_Refrendo);
        }
        
        public List<BE_Refrendo> ListarDetalleSolicitudRefrendo(BE_Refrendo oBE_Refrendo)
        {
            return new DA_Refrendo().ListarDetalleSolicitudRefrendo(oBE_Refrendo);
        }
        public List<BE_Refrendo> ListarDetalleBultosPrecintosIngresadosCliente(BE_Refrendo oBE_Refrendo)
        {
            return new DA_Refrendo().ListarDetalleBultosPrecintosIngresadosCliente(oBE_Refrendo);
        }
        public List<BE_Refrendo> ListarREL_Ruc(BE_Refrendo oBE_Refrendo)
        {
            return new DA_Refrendo().ListarREL_Ruc(oBE_Refrendo);
        }


        
        public List<BE_Refrendo> ListarEstadosRefrendo(BE_Refrendo oBE_Refrendo)
        {
            return new DA_Refrendo().ListarEstadosRefrendo(oBE_Refrendo);
        }

        



        public String Insertar_RefrendoElectronico(BE_Refrendo oBE_Refrendo, List<BE_Refrendo> oBE_RefrendoList)
        {
            return new DA_Refrendo().Insertar_RefrendoElectronico(oBE_Refrendo, oBE_RefrendoList);
        }

        public String Actualizar_BultosDetalleRefrendo(BE_Refrendo oBE_Refrendo)
        {
            return new DA_Refrendo().Actualizar_BultosDetalleRefrendo(oBE_Refrendo);
        }


        public String ValidarCarga(BE_Refrendo oBE_Refrendo)
        {
            return new DA_Refrendo().ValidarCarga(oBE_Refrendo);
        }
        

        public String Insertar_BultosPrecintoDetalleRefrendo(BE_Refrendo oBE_Refrendo)
        {
            return new DA_Refrendo().Insertar_BultosPrecintoDetalleRefrendo(oBE_Refrendo);
        }
        

        public Tuple<List<BE_Refrendo>, string> Extranet_Ins_ArchivosRefrendo(BE_Refrendo oBE_Refrendo)
        {
            DA_Refrendo oDA_Refrendo= new DA_Refrendo();
            var resul = oDA_Refrendo.Extranet_Ins_ArchivosRefrendo(oBE_Refrendo);

            return Tuple.Create(resul.Item1, resul.Item2);
        }


    }
}
