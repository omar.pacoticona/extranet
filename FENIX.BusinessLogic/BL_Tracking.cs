﻿using System;
using System.Collections.Generic;
using System.Text;
using FENIX.Common;
using FENIX.DataAccess;
using FENIX.BusinessEntity;

namespace FENIX.BusinessLogic
{
    public class BL_Tracking
    {
        #region "Transaccion"

        #endregion



        #region "Listado"
            public List<BE_Tracking> Listar(BE_Tracking oBE_Tracking)
            {
                DA_Tracking oDA_Tracking = new DA_Tracking();
                return oDA_Tracking.Listar(oBE_Tracking);
            }

            public List<BE_Tracking> ListarItems(Int32 pIdCliente, String pDocumentoOrigen)
            {
                DA_Tracking oDA_Tracking = new DA_Tracking();
                return oDA_Tracking.ListarItems(pIdCliente, pDocumentoOrigen);
            }

            public List<BE_Tracking> ListarSeguimiento(BE_Tracking oBE_Tracking)
            {
                DA_Tracking oDA_Tracking = new DA_Tracking();
                return oDA_Tracking.ListarSeguimiento(oBE_Tracking);
            }

            public void CreaPDFVolante(List<BE_DocumentoOrigen> oLis_BE_DocumentoOrigen, String sUsuario)
            {
                DA_Tracking oDA_Tracking = new DA_Tracking();
                oDA_Tracking.CreaPDFVolante(oLis_BE_DocumentoOrigen, sUsuario);
            }
        #endregion
    }
}

