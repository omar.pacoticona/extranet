﻿using System;
using System.Collections.Generic;
using System.Text;
using FENIX.Common;
using FENIX.DataAccess;
using FENIX.BusinessEntity;

namespace FENIX.BusinessLogic
{
    public class BL_Indicador
    {
        #region "Transaccion"

        #endregion



        #region "Listado"
            public List<BE_Indicador> Listar(BE_Indicador oBE_Indicador)
            {
                DA_Indicador oDA_Indicador = new DA_Indicador();
                return oDA_Indicador.Listar(oBE_Indicador);
            }

            public List<BE_Indicador> ListarAnual(BE_Indicador oBE_Indicador)
            {
                DA_Indicador oDA_Indicador = new DA_Indicador();
                return oDA_Indicador.ListarAnual(oBE_Indicador);
            }

            public List<BE_Indicador> ListarDetalle(BE_Indicador oBE_Indicador)
            {
                DA_Indicador oDA_Indicador = new DA_Indicador();
                return oDA_Indicador.ListarDetalle(oBE_Indicador);
            }

            public String AprobarDesaprobar(BE_Indicador oBE_Indicador)
            {
                DA_Indicador oDA_Indicador = new DA_Indicador();
                return oDA_Indicador.AprobarDesaprobar(oBE_Indicador);
            }

            public List<BE_Indicador> ListarGraficoTotal(BE_Indicador oBE_Indicador)
            {
                DA_Indicador oDA_Indicador = new DA_Indicador();
                return oDA_Indicador.ListarGraficoTotal(oBE_Indicador);
            }

            public List<BE_Indicador> ListarGrafico(BE_Indicador oBE_Indicador)
            {
                DA_Indicador oDA_Indicador = new DA_Indicador();
                return oDA_Indicador.ListarGrafico(oBE_Indicador);
            }
        #endregion
    }
}

