﻿using System;
using System.Collections.Generic;
using System.Text;
using FENIX.Common;
using FENIX.DataAccess;
using FENIX.BusinessEntity;
using System.Data;

namespace FENIX.BusinessLogic
{
    public class BL_DocumentoOrigen
    {
        #region "Transaccion"

        public String GrabarAutorizacDscto(BE_DocumentoOrigen oBE_DocumentoOrigen)
        {
            DA_DocumentoOrigen oDA_DocumentoOrigen = new DA_DocumentoOrigen();
            return oDA_DocumentoOrigen.GrabarAutorizacDscto(oBE_DocumentoOrigen);
        }

        #endregion



        #region "Listado"
            public List<BE_DocumentoOrigen> ListarVolantesWeb(BE_DocumentoOrigen oBE_DocumentoOrigen)
            {
                DA_DocumentoOrigen oDA_DocumentoOrigen = new DA_DocumentoOrigen();
                return oDA_DocumentoOrigen.ListarVolantesWeb(oBE_DocumentoOrigen);
            }

            public List<BE_DocumentoOrigen> ListarVolantesParaServicio(BE_DocumentoOrigen oBE_DocumentoOrigen)
            {
                DA_DocumentoOrigen oDA_DocumentoOrigen = new DA_DocumentoOrigen();
                return oDA_DocumentoOrigen.ListarVolantesParaServicio(oBE_DocumentoOrigen);
            }

            public List<BE_DocumentoOrigen> ListarBLsparaVolante(BE_DocumentoOrigen oBE_DocumentoOrigen)
            {
                DA_DocumentoOrigen oDA_DocumentoOrigen = new DA_DocumentoOrigen();
                return oDA_DocumentoOrigen.ListarBLsparaVolante(oBE_DocumentoOrigen);
            }

            public List<BE_DocumentoOrigen> ListarDatosVolante(BE_DocumentoOrigen oBE_DocumentoOrigen)
            {
                DA_DocumentoOrigen oDA_DocumentoOrigen = new DA_DocumentoOrigen();
                return oDA_DocumentoOrigen.ListarDatosVolante(oBE_DocumentoOrigen);
            }
            public List<BE_DocumentoOrigen> BuscaDatosDsctoAlmac(Int32 iIdDscto)
            {
                DA_DocumentoOrigen oDA_DocumentoOrigen = new DA_DocumentoOrigen();
                return oDA_DocumentoOrigen.BuscaDatosDsctoAlmac(iIdDscto);
            }

        public BE_AutorizacionRetiroList ListarAutRetiro(BE_AutorizacionRetiro objEntidad)
        {
            DA_DocumentoOrigen objDatos = new DA_DocumentoOrigen();
            return objDatos.ListarAutRetiro(objEntidad);
        }

        public BE_AutorizacionRetiro ConsultarAutRetId(Int32 iIdAutRet)
        {
            DA_DocumentoOrigen objDatos = new DA_DocumentoOrigen();
            return objDatos.ConsultarAutRetId(iIdAutRet);
        }

        public String InsertarPlacaChofer(BE_PlacaChofer objEntidad, int tipo, int accion)
        {
            DA_DocumentoOrigen objDatos = new DA_DocumentoOrigen();
            return objDatos.InsertarPlacaChofer(objEntidad, tipo, accion);
        }

        public BE_PlacaChoferList ListarPlacaChofer(int IdAutRet)
        {
            return new DA_DocumentoOrigen().ListarPlacaChofer(IdAutRet);
        }
        public BE_PlacaChoferList ListarPlacaChoferApp(int IdAutRet,int accion)
        {
            return new DA_DocumentoOrigen().ListarPlacaChoferApp(IdAutRet,accion);
        }
                
        public ResultadoTransaccionTx EliminarPlacaChofer(Int32 IdRegistro, String Usuario, String NombrePc)
        {
            DA_DocumentoOrigen oDA_Cliente = new DA_DocumentoOrigen();
            return oDA_Cliente.EliminarPlacaChofer(IdRegistro, Usuario, NombrePc);
        }

        public BE_AutorizacionRetiroList ListarAutorizacionRetiroDetPorID(BE_AutorizacionRetiro objEntidad)
        {
            DA_DocumentoOrigen objDatos = new DA_DocumentoOrigen();
            return objDatos.ListarAutorizacionRetiroDetPorID(objEntidad);
        }
        public BE_AutorizacionRetiroList ListarAutorizacionRetiroDetStatusPorID(BE_AutorizacionRetiro objEntidad)
        {
            DA_DocumentoOrigen objDatos = new DA_DocumentoOrigen();
            return objDatos.ListarAutorizacionRetiroDetStatusPorID(objEntidad);
        }

        public BE_AutorizacionRetiroList ListarDespachador(String Codigo,String Despachador, int IdCliente)
        {
            DA_DocumentoOrigen objDatos = new DA_DocumentoOrigen();
            return objDatos.ListarDespachador(Codigo,Despachador, IdCliente);
        }

        public String HabilitarAutRet(int IdAutRet,DateTime Vigencia,int dia, String Usuario,String PC)
        {
            DA_DocumentoOrigen objDatos = new DA_DocumentoOrigen();
            return objDatos.HabilitarAutRet(IdAutRet,Vigencia,dia,Usuario,PC);
        }

        public String ValidarPresencia(int IdCliente, int IdAutRet)
        {
            DA_DocumentoOrigen objDatos = new DA_DocumentoOrigen();
            return objDatos.ValidarPresencia(IdCliente, IdAutRet);
        }

        public BE_PlacaChoferList ListarPlacaChofer2(String AutRet,String Placa, String tipoDoc, String Doc,String Cliente)
        {
            return new DA_DocumentoOrigen().ListarPlacaChofer2(AutRet,Placa,tipoDoc,Doc, Cliente);
        }

        public String ModificarPlacaChofer(int IdRegistro, String AutRet,String Placa,String Tipo,String Doc,String Usuario,int Cliente)
        {
            DA_DocumentoOrigen objDatos = new DA_DocumentoOrigen();
            return objDatos.ModificarPlacaChofer(IdRegistro, AutRet, Placa, Tipo, Doc,Usuario,Cliente);
        }

        public String AlertaCodigo(String NumAut, String Placa, String Tipo, String Doc, String Usuario, String CodigoCliente, int IdCliente, int IdRegistro)
        {
            DA_DocumentoOrigen objDatos = new DA_DocumentoOrigen();
            return objDatos.AlertaCodigo(NumAut, Placa, Tipo, Doc, Usuario, CodigoCliente, IdCliente, IdRegistro);
        }

        public BE_DespachadorList ListarDespachador2(String Cliente, String Codigo, String Nombre, String Apellido, String DNI)
        {
            return new DA_DocumentoOrigen().ListarDespachador2(Cliente, Codigo, Nombre, Apellido, DNI);
        }

        public String GrabarDespachador(String Nombre, String Apellido, String DNI, Int32 IdCliente,String Usuario)
        {
            DA_DocumentoOrigen objDatos = new DA_DocumentoOrigen();
            return objDatos.GrabarDespachador(Nombre, Apellido, DNI, IdCliente,Usuario);
        }

        public String ACtualizarDespachador(String Codigo, String Nombre, String Apellido, String DNI, String IdCliente, String Usuario)
        {
            DA_DocumentoOrigen objDatos = new DA_DocumentoOrigen();
            return objDatos.ACtualizarDespachador(Codigo, Nombre, Apellido, DNI, IdCliente, Usuario);
        }

        public String EliminarDespachador(String Codigo)
        {
            DA_DocumentoOrigen objDatos = new DA_DocumentoOrigen();
            return objDatos.EliminarDespachador(Codigo);
        }

        public String ValidarCodigo(String Codigo)
        {
            DA_DocumentoOrigen objDatos = new DA_DocumentoOrigen();
            return objDatos.ValidarCodigo(Codigo);
        }
        public String ValidarDNI(String DNI)
        {
            DA_DocumentoOrigen objDatos = new DA_DocumentoOrigen();
            return objDatos.ValidarDNI(DNI);
        }
        public String ValidarPlaca(String Placa)
        {
            DA_DocumentoOrigen objDatos = new DA_DocumentoOrigen();
            return objDatos.ValidarPlaca(Placa);
        }

        public String ValidarPermisos(String Codigo)
        {
            DA_DocumentoOrigen objDatos = new DA_DocumentoOrigen();
            return objDatos.ValidarPermisos(Codigo);
        }

        public BE_AutorizacionRetiro ListarAutRetAgre(String AutRet)
        {
            DA_DocumentoOrigen objDatos = new DA_DocumentoOrigen();
            return objDatos.ListarAutRetAgre(AutRet);
        }

        public BE_AutorizacionRetiroList ListarAutRetiro2(String NumAut,String Volante,String BL, String Cliente, String Contenedor, String Agencia)
        {
            DA_DocumentoOrigen objDatos = new DA_DocumentoOrigen();
            return objDatos.ListarAutRetiro2(NumAut, Volante, BL, Cliente, Contenedor, Agencia);
        }

        public ResultadoTransaccionTx EliminarDespachador2(Int32 IdRegistro, String Usuario, String NombrePc)
        {
            DA_DocumentoOrigen oDA_Cliente = new DA_DocumentoOrigen();
            return oDA_Cliente.EliminarDespachador2(IdRegistro, Usuario, NombrePc);
        }

        public String InsertarDespachadorAut(BE_AutorizacionRetiro objEntidad)
        {
            DA_DocumentoOrigen objDatos = new DA_DocumentoOrigen();
            return objDatos.InsertarDespachadorAut(objEntidad);
        }

        public BE_AutorizacionRetiroList ListarAutDespachadores(int IdAutRet)
        {
            return new DA_DocumentoOrigen().ListarAutDespachadores(IdAutRet);
        }

        public String InsContSelAutRet(int IdAutRet, DateTime Vigencia,int dia, String Presencia, String SelCont, int IdAutRetDet,
            String Usuario, String PC)
        {
            DA_DocumentoOrigen objDatos = new DA_DocumentoOrigen();
            return objDatos.InsContSelAutRet(IdAutRet, Vigencia, dia, Presencia, SelCont, IdAutRetDet, Usuario, PC);
        }

        public void InsLstContSelAutRet(List<BE_Contenedor> lstContainers, Int32 AutRetiro)
        {
            DA_DocumentoOrigen objDatos = new DA_DocumentoOrigen();
            objDatos.InsLstContSelAutRet(lstContainers, AutRetiro);
        }


        public void SendContainers(List<String> lstContainers, Int32 AutRetiro)
        {
            DA_DocumentoOrigen objDatos = new DA_DocumentoOrigen();
            objDatos.SendContainers(lstContainers, AutRetiro);
        }

        public void UpdateSentContainers(List<String> lstcontainers, Int32 AutRetiro)
        {
            DA_DocumentoOrigen objDatos = new DA_DocumentoOrigen();
            objDatos.UpdateSentContainers(lstcontainers,AutRetiro);
        }


        public BE_DocumentoOrigenListaList ListarDO(BE_DocumentoOrigen oBE_DocumentoOrigen)
        {
            DA_DocumentoOrigen oDA_DocumentoOrigen = new DA_DocumentoOrigen();
            return oDA_DocumentoOrigen.ListarDO(oBE_DocumentoOrigen);

        }

        public BE_DocumentoOrigenDetalleListaList ListarDODetalle(BE_DocumentoOrigen oBE_DocumentoOrigen)
        {
            DA_DocumentoOrigen oDA_DocumentoOrigen = new DA_DocumentoOrigen();
            return oDA_DocumentoOrigen.ListarDODetalle(oBE_DocumentoOrigen);

        }
        #endregion


        #region ARE
        public List<BE_DocumentoOrigen> ListarDO_AR(BE_DocumentoOrigen oBE_DocOrigen, int accion)
        {
            DA_DocumentoOrigen oDA_DocumentoOrigen = new DA_DocumentoOrigen();
            return oDA_DocumentoOrigen.ListarDO_AR(oBE_DocOrigen, accion);
        }
        #endregion

    }
}

