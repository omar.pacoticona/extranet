﻿using FENIX.BusinessEntity;
using FENIX.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FENIX.BusinessLogic
{
    public class BL_AutorizacionRetiro
    {

        public Tuple<List<BE_AutorizacionRetiro>, string> Ext_SO_AutorizacionRetiro(BE_AutorizacionRetiro oBE_AutorizacionRetiro, int accion)
        {
            DA_AutorizacionRetiro oDA_AutorizacionRetiro = new DA_AutorizacionRetiro();
            var resul = oDA_AutorizacionRetiro.Ext_SO_AutorizacionRetiro(oBE_AutorizacionRetiro, accion);

            return Tuple.Create(resul.Item1, resul.Item2);
        }
        public Tuple<List<BE_AutorizacionRetiro>, string> Ext_SO_AutorizacionRetiroDet(BE_AutorizacionRetiro oBE_AutorizacionRetiro, int accion)
        {
            DA_AutorizacionRetiro oDA_AutorizacionRetiro = new DA_AutorizacionRetiro();
            var resul = oDA_AutorizacionRetiro.Ext_SO_AutorizacionRetiroDet(oBE_AutorizacionRetiro, accion);

            return Tuple.Create(resul.Item1, resul.Item2);
        }
        public BE_AutorizacionRetiro ListarProcesoPorID(String NroAutRet,int IdAgente)
        {
            DA_AutorizacionRetiro oDA_AutorizacionRetiro = new DA_AutorizacionRetiro();
            return oDA_AutorizacionRetiro.ListarProcesoPorID(NroAutRet, IdAgente);
        }

        public BE_AutorizacionRetiro ListarAutorizacionRetiroPorID_Rep(Int32 IdAutRet)
        {
            DA_AutorizacionRetiro oDA_AutorizacionRetiro = new DA_AutorizacionRetiro();
            return oDA_AutorizacionRetiro.ListarAutorizacionRetiroPorID_Rep(IdAutRet);
        }

        public List<BE_AutorizacionRetiro> ListarAutorizacionRetiroPorID_Det_Rep(Int32 IdAutRet)
        {
            DA_AutorizacionRetiro oDA_AutorizacionRetiro = new DA_AutorizacionRetiro();
            return oDA_AutorizacionRetiro.ListarAutorizacionRetiroPorID_Det_Rep(IdAutRet);
        }
    }
}
