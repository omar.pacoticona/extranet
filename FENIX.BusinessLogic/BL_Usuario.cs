﻿using System;
using System.Collections.Generic;
using System.Text;
using FENIX.BusinessEntity;
using FENIX.DataAccess;

namespace FENIX.BusinessLogic
{
    public class BL_Usuario
    {
        public String ValidaUsuario(BE_Usuario oBE_Usuario, String sMaquina, String sIPMaquina)
        {
            DA_Usuario oDA_Usuario = new DA_Usuario();
            return oDA_Usuario.ValidaUsuario(oBE_Usuario,sMaquina,sIPMaquina);
        }

        public String ModificaClave(BE_Usuario oBE_Usuario)
        {
            DA_Usuario oDA_Usuario = new DA_Usuario();
            return oDA_Usuario.ModificaClave(oBE_Usuario);
        }

        public void GrabaInicioSesion(string sUsuario, String sVarSesion)
        {
            DA_Usuario oDA_Usuario = new DA_Usuario();
            oDA_Usuario.GrabaInicioSesion(sUsuario, sVarSesion);
        }

        public List<BE_Usuario> AccesoMenu(BE_Usuario oBE_Usuario)
        {
            DA_Usuario oDA_Usuario = new DA_Usuario();
            return oDA_Usuario.AccesoMenu(oBE_Usuario);
        }

        //public String ReiniciaClave(BE_Usuario oBE_Usuario)
        //{
        //    DA_Usuario oDA_Usuario = new DA_Usuario();
        //    return oDA_Usuario.ReiniciaClave(oBE_Usuario);
        //}

        public List<BE_Usuario> ListaConfSeguridad()
        {
            DA_Usuario oDa_Usuario = new DA_Usuario();
            return oDa_Usuario.ListaConfSeguridad();
        }

        public List<BE_Usuario> ListaDatosUsuario(Int32 IdUsuario, string Usuario)
        {
            DA_Usuario oDa_Usuario = new DA_Usuario();
            return oDa_Usuario.ListaDatosUsuario(IdUsuario, Usuario);
        }

        public void EnviaCorreoNewClave(BE_Usuario oBE_Usuario)
        {
            DA_Usuario oDa_Usuario = new DA_Usuario();
            oDa_Usuario.EnviaCorreoNewClave(oBE_Usuario);
        }

        public List<BE_Usuario> ListarUsuarios(String sCliente, String sUsuario,string dni)
        {
            DA_Usuario oDa_Usuario = new DA_Usuario();
            return oDa_Usuario.ListarUsuarios(sCliente, sUsuario,dni);
        }

        public String GrabarUsuario(BE_Usuario oBE_Usuario)
        {
            DA_Usuario oDA_Usuario = new DA_Usuario();
            return oDA_Usuario.GrabarUsuario(oBE_Usuario);
        }

        public String AnularUsuario(BE_Usuario oBE_Usuario)
        {
            DA_Usuario oDA_Usuario = new DA_Usuario();
            return oDA_Usuario.AnularUsuario(oBE_Usuario);
        }

        public String VerificaAccesoProcesoEspecial(String sProceso, String sUsuario)
        {
            DA_Usuario oDA_Usuario = new DA_Usuario();
            return oDA_Usuario.VerificaAccesoProcesoEspecial(sProceso, sUsuario);
        }

        public void HabilitarUsuario(Int32 IdUsuario)
        {
            DA_Usuario oDa_Usuario = new DA_Usuario();
            oDa_Usuario.HabilitarUsuario(IdUsuario);
        }
    }
}
