﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FENIX.BusinessEntity;
using FENIX.DataAccess;

namespace FENIX.BusinessLogic
{
    public class BL_Presupuesto
    {
        public List<BE_Tabla> ListarLineaNegocio()
        {
            DA_ConsultasComunes oDA_Tabla = new DA_ConsultasComunes();
            return oDA_Tabla.ListarTabla(154, 0);
        }

        public List<BE_Presupuesto> ListarVendedor()
        {
            DA_Presupuesto oDA_Presupuesto = new DA_Presupuesto();
            return oDA_Presupuesto.ListarVendedor();
        }

        public List<BE_Presupuesto> ListarCliente()
        {
            DA_Presupuesto oDA_Presupuesto = new DA_Presupuesto();
            return oDA_Presupuesto.ListarCliente();
        }

        public List<BE_Presupuesto> ListarPresupuesto(string strPeriodo, string strTipo)
        {
            DA_Presupuesto oDA_Presupuesto = new DA_Presupuesto();
            return oDA_Presupuesto.ListarPresupuesto(strPeriodo, strTipo);
        }

        public List<BE_Tabla> ListarDatosTabla(Int32 IdTabla)
        {
            DA_ConsultasComunes oDA_Tabla = new DA_ConsultasComunes();
            return oDA_Tabla.ListarTabla(IdTabla, 0);
        }

        public List<BE_Presupuesto> ListarVtasXTipoCli(BE_Presupuesto oBE_Presupuesto)
        {
            DA_Presupuesto oDA_Presupuesto = new DA_Presupuesto();
            return oDA_Presupuesto.ListarVtasXTipoCli(oBE_Presupuesto);
        }

        public List<BE_Presupuesto> ListarVtasXTipoCarga(BE_Presupuesto oBE_Presupuesto)
        {
            DA_Presupuesto oDA_Presupuesto = new DA_Presupuesto();
            return oDA_Presupuesto.ListarVtasXTipoCarga(oBE_Presupuesto);
        }
        

        public List<BE_Presupuesto> ListarVtasXPeriodo(BE_Presupuesto oBE_Presupuesto)
        {
            DA_Presupuesto oDA_Presupuesto = new DA_Presupuesto();
            return oDA_Presupuesto.ListarVtasXPeriodo(oBE_Presupuesto);
        }

        public List<BE_Presupuesto> ListarVtasXLinea(BE_Presupuesto oBE_Presupuesto)
        {
            DA_Presupuesto oDA_Presupuesto = new DA_Presupuesto();
            return oDA_Presupuesto.ListarVtasXLinea(oBE_Presupuesto);
        }

        public List<BE_Presupuesto> ListarVtasXCliente(BE_Presupuesto oBE_Presupuesto)
        {
            DA_Presupuesto oDA_Presupuesto = new DA_Presupuesto();
            return oDA_Presupuesto.ListarVtasXCliente(oBE_Presupuesto);
        }

        public List<BE_Presupuesto> ListarVtasXVendedor(BE_Presupuesto oBE_Presupuesto)
        {
            DA_Presupuesto oDA_Presupuesto = new DA_Presupuesto();
            return oDA_Presupuesto.ListarVtasXVendedor(oBE_Presupuesto);
        }

        public List<BE_Presupuesto> ListarVtasXServicio(BE_Presupuesto oBE_Presupuesto)
        {
            DA_Presupuesto oDA_Presupuesto = new DA_Presupuesto();
            return oDA_Presupuesto.ListarVtasXServicio(oBE_Presupuesto);
        }

        public String InsertarPresupuesto(BE_Presupuesto oBE_Presupuesto)
        {
            DA_Presupuesto oDA_Presupuesto = new DA_Presupuesto();
            return oDA_Presupuesto.InsertarPresupuesto(oBE_Presupuesto);
        }

        public String EliminarPresupuesto(BE_Presupuesto oBE_Presupuesto)
        {
            DA_Presupuesto oDA_Presupuesto = new DA_Presupuesto();
            return oDA_Presupuesto.EliminarPresupuesto(oBE_Presupuesto);
        }

        
    }
}
