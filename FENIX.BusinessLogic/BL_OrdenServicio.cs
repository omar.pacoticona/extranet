﻿using System;
using System.Collections.Generic;
using System.Text;
using FENIX.BusinessEntity;
using FENIX.DataAccess;
using FENIX.Common;


namespace FENIX.BusinessLogic
{
    public class BL_OrdenServicio
    {
        public String Insertar_OrdenServicio(BE_OrdenServicio oBE_OrdenServicio, Int32 pIdServicioOficinaLineaNegocio)
        {
            DA_OrdenServicio oDA_OrdenServicio = new DA_OrdenServicio();
            return oDA_OrdenServicio.Insertar_OrdenServicio(oBE_OrdenServicio, pIdServicioOficinaLineaNegocio);
        }
        public String Actualizar(BE_OrdenServicio oBE_OrdenServicio)
        {
            DA_OrdenServicio oDA_OrdenServicio = new DA_OrdenServicio();
            return oDA_OrdenServicio.Actualizar(oBE_OrdenServicio);
        }
        public String Eliminar(BE_OrdenServicio oBE_OrdenServicio)
        {
            DA_OrdenServicio oDA_OrdenServicio = new DA_OrdenServicio();
            return oDA_OrdenServicio.Eliminar(oBE_OrdenServicio);
        }

        public Int32 EliminarDetalleFisico(int iidOrdSer)
        {
            DA_OrdenServicio oDA_OrdenServicio = new DA_OrdenServicio();
            return oDA_OrdenServicio.EliminarDetalleFisico(iidOrdSer);
        }

        public string ActualizarEstadoDetalle(BE_OrdenServicioDetalle oBE_OrdenServicioDetalle)
        {
            DA_OrdenServicio oDA_OrdenServicio = new DA_OrdenServicio();
            return oDA_OrdenServicio.ActualizarEstadoDetalle(oBE_OrdenServicioDetalle);
        }
        public string ActualizarEstadoDetalleVarios(BE_OrdenServicioDetalle oBE_OrdenServicioDetalle)
        {
            DA_OrdenServicio oDA_OrdenServicio = new DA_OrdenServicio();
            return oDA_OrdenServicio.ActualizarEstadoDetalleVarios(oBE_OrdenServicioDetalle);
        }

        public String EliminarDetalleOS(BE_OrdenServicio oBE_OrdenServicio)
        {
            DA_OrdenServicio oDA_OrdenServicio = new DA_OrdenServicio();
            return oDA_OrdenServicio.EliminarDetalleOS(oBE_OrdenServicio);
        }
        public String Insertar_OrdenServicioDetalle(BE_OrdenServicioDetalle oBE_OrdenServicioDetalle)
        {
            DA_OrdenServicio oDA_OrdenServicio = new DA_OrdenServicio();
            return oDA_OrdenServicio.Insertar_OrdenServicioDetalle(oBE_OrdenServicioDetalle);
        }
        public List<BE_OrdenServicio> Listar(BE_OrdenServicio oBE_OrdenServicio)
        {
            DA_OrdenServicio oDA_OrdenServicio = new DA_OrdenServicio();
            return oDA_OrdenServicio.Listar(oBE_OrdenServicio);
        }


        public BE_OrdenServicioLis ListarServiciosTransporte(BE_OrdenServicio ent)
        {
            return new DA_OrdenServicio().ListarServiciosTransporte(ent);
        }

        public BE_OrdenServicioDetalleLis ListarOrdenServicioAtencion(BE_OrdenServicio oBE_OrdenServicio)
        {
            DA_OrdenServicio oDA_OrdenServicio = new DA_OrdenServicio();
            return oDA_OrdenServicio.ListarOrdenServicioAtencion(oBE_OrdenServicio);
        }
        public List<BE_OrdenServicioDetalle> ListarOrdenServicioDetalle(int iIdOrdSer)
        {
            DA_OrdenServicio oDA_OrdenServicio = new DA_OrdenServicio();
            return oDA_OrdenServicio.ListarOrdenServicioDetalle(iIdOrdSer);

        }
        public List<BE_OrdenServicioDetalle> ListarDocumentoOrigenDetalle(int iIdDocOri)
        {
            DA_OrdenServicio oDA_OrdenServicio = new DA_OrdenServicio();
            return oDA_OrdenServicio.ListarDocumentoOrigenDetalle(iIdDocOri);
        }
        public BE_OrdenServicio ListarPorId(int IdOrdSer)
        {
            DA_OrdenServicio oDA_OrdenServicio = new DA_OrdenServicio();
            return oDA_OrdenServicio.ListarPorId(IdOrdSer);
        }
        public BE_OrdenServicio ListarPorVolante(String NumVolante)
        {
            DA_OrdenServicio oDA_OrdenServicio = new DA_OrdenServicio();
            return oDA_OrdenServicio.ListarPorVolante(NumVolante);
        }


        public String Orden_Servicio_Valida_SLI(Int32 iIdServicio, Int32 iIdDocOriDet)
        {
            DA_OrdenServicio oDA_OrdenServicio = new DA_OrdenServicio();
            return oDA_OrdenServicio.Orden_Servicio_Valida_SLI(iIdServicio, iIdDocOriDet);
        }

        public BE_OrdenServicioLis ListarReporteOrdenServicio(BE_OrdenServicio oBE_OrdenServicio)
        {
            DA_OrdenServicio oDA_OrdenServicio = new DA_OrdenServicio();
            return oDA_OrdenServicio.ListarReporteOrdenServicio(oBE_OrdenServicio);
        }

        public List<BE_Tabla> ListarCondicionCarga()
        {
            DA_ConsultasComunes oDA_ConsultasComunes = new DA_ConsultasComunes();
            return oDA_ConsultasComunes.ListarTabla(7, 0);
        }

        public List<BE_Tabla> ListarServicios_Traslado()
        {
            DA_OrdenServicio oDA_OrdenServicio = new DA_OrdenServicio();
            return oDA_OrdenServicio.ListarServicios_Traslado();
        }

        public List<BE_OrdenServicioDetalle> ListarDocumentoOrigenDetalle_Traslado(BE_OrdenServicioDetalle oBE_OrdenServicioDetalle)
        {
            DA_OrdenServicio oDA_OrdenServicio = new DA_OrdenServicio();
            return oDA_OrdenServicio.ListarDocumentoOrigenDetalle_Traslado(oBE_OrdenServicioDetalle);
        }
        public String Actualizar_FechaFinalCobroAlmacenaje(Int32 iIdDocOri, Int32 iIdDocOriDet, DateTime dtFechaFinal, String sUsuario, String sNombrePc)
        {
            DA_OrdenServicio oDA_OrdenServicio = new DA_OrdenServicio();
            return oDA_OrdenServicio.Actualizar_FechaFinalCobroAlmacenaje(iIdDocOri, iIdDocOriDet, dtFechaFinal, sUsuario, sNombrePc);
        }
        public String Actualizar_CodigoCM(Int32 iIdDocOri, Int32 iIdDocOriDet, String iCodigoCmNew, String sUsuario, String sNombrePc)
        {
            DA_OrdenServicio oDA_OrdenServicio = new DA_OrdenServicio();
            return oDA_OrdenServicio.Actualizar_CodigoCm(iIdDocOri, iIdDocOriDet, iCodigoCmNew, sUsuario, sNombrePc);
        }

        public Int32 AtenderOSTransporte(BE_OrdenServicio oBE_OrdenServicio)
        {
            DA_OrdenServicio oDA_OrdenServicio = new DA_OrdenServicio();
            return oDA_OrdenServicio.AtenderOSTransporte(oBE_OrdenServicio);
        }

        public Int32 EnviaraFacturarOSTransporte(BE_OrdenServicio oBE_OrdenServicio)
        {
            DA_OrdenServicio oDA_OrdenServicio = new DA_OrdenServicio();
            return oDA_OrdenServicio.EnviaraFacturarOSTransporte(oBE_OrdenServicio);
        }

        public Int32 AdicionarOSTransporte(BE_OrdenServicio oBE_OrdenServicio)
        {
            DA_OrdenServicio oDA_OrdenServicio = new DA_OrdenServicio();
            return oDA_OrdenServicio.AdicionarOSTransporte(oBE_OrdenServicio);
        }

        public List<BE_OrdenServicioDetalle> ListarServicioAduanero()
        {
            DA_OrdenServicio oDA_OrdenServicio = new DA_OrdenServicio();
            return oDA_OrdenServicio.ListarServicioAduanero();
        }



        #region "FIFA
        //////public List<BE_TablaUc> ListarLineaNegocio_2()
        //////{
        //////    DA_ConsultasComunes oDA_Bitacora = new DA_ConsultasComunes();
        //////    BE_Tabla oTabla = new BE_Tabla();
        //////    oTabla.NPagina = 1;
        //////    oTabla.NRegistros = 999999;
        //////    return oDA_Bitacora.ListarTablaUserControlLineaNegocio_2(oTabla);
        //////}

        public List<BE_Tabla> ListarTablasComunes(int pTabla)
        {
            DA_ConsultasComunes oDA_Bitacora = new DA_ConsultasComunes();
            return oDA_Bitacora.ListarTabla(pTabla, 0);
        }

        public BE_OrdenServicioLis ListarOrdenesServicio(BE_OrdenServicio ent)
        {
            return new DA_OrdenServicio().ListarOrdenesServicio(ent);
        }

        public BE_OrdenServicioDetalleLis ListarOrdenesServicioAtencion(BE_OrdenServicio ent)
        {
            return new DA_OrdenServicio().ListarOrdenesServicioAtencion(ent);
        }

        public BE_OrdenServicioDetalleLis ListarDocumentoOrigenDetalleNuevaOrden(BE_OrdenServicioDetalle ent)
        {
            return new DA_OrdenServicio().ListarDocumentoOrigenDetalleNuevaOrden(ent);
        }

        public BE_OrdenServicioDetalleLis ListarOrdenServicioDetalle(BE_OrdenServicioDetalle ent)
        {
            return new DA_OrdenServicio().ListarOrdenServicioDetalle(ent);
        }

        public BE_OrdenServicio ListarOrdenServicioPorId_2(Int32 IdOrdSer)
        {
            return new DA_OrdenServicio().ListarOrdenServicioPorId_2(IdOrdSer);
        }

        public String ActualizarSituacion_2(BE_OrdenServicioDetalle oBE_OrdenServicioDetalle)
        {
            return new DA_OrdenServicio().ActualizarSituacion_2(oBE_OrdenServicioDetalle);
        }

        public String EliminarCabeceraOS(BE_OrdenServicio oBE_OrdenServicio)
        {
            DA_OrdenServicio oDA_OrdenServicio = new DA_OrdenServicio();
            return oDA_OrdenServicio.EliminarCabeceraOS(oBE_OrdenServicio);
        }
        #endregion
    }
}
