﻿using System;
using System.Collections.Generic;
using System.Text;
using FENIX.DataAccess;
using FENIX.BusinessEntity;
using FENIX.Common;

namespace FENIX.BusinessLogic
{
    public class BL_Contrato
    {

        #region "Transacciones"
        public String Insertar(BE_Contrato oBE_Contrato)
        {
            DA_Contrato oDA_Contrato = new DA_Contrato();
            return oDA_Contrato.Insertar(oBE_Contrato);

        }
        public int InsertarContratoDetalle(BE_Contrato oBE_Contrato, BE_Tarifa oBE_Tarifa)
        {
            DA_Contrato oDA_Contrato = new DA_Contrato();
            return oDA_Contrato.InsertarContratoDetalle(oBE_Contrato, oBE_Tarifa);
        }

        public ResultadoTransaccionTx Actualizar(BE_Contrato oBE_Contrato, BE_Tarifa oBE_Tarifa)
        {
            DA_Contrato oDA_Contrato = new DA_Contrato();
            return oDA_Contrato.Actualizar(oBE_Contrato, oBE_Tarifa);
        }

        public ResultadoTransaccionTx Eliminar(BE_Contrato oBE_Contrato)
        {
            DA_Contrato oDA_Contrato = new DA_Contrato();
            return oDA_Contrato.Eliminar(oBE_Contrato);
        }

        public ResultadoTransaccionTx EliminarContratoDetalle(BE_Contrato oBE_Contrato)
        {
            DA_Contrato oDA_Contrato = new DA_Contrato();
            return oDA_Contrato.EliminarContratoDetalle(oBE_Contrato);
        }
        public ResultadoTransaccionTx AprobarDesaprobarContrato(Int32 iIdContrato, string sSituacion)
        {
            DA_Contrato oDA_Contrato = new DA_Contrato();
            return oDA_Contrato.AprobarDesaprobarContrato(iIdContrato, sSituacion);
        }

        public String VerificaAccesoProcesoEspecial(String sProceso, String sUsuario)
        {
            DA_Contrato oDA_Contrato = new DA_Contrato();
            return oDA_Contrato.VerificaAccesoProcesoEspecial(sProceso, sUsuario);
        }

        public String CopiarContrato(BE_Contrato oBE_Contrato)
        {
            DA_Contrato oDA_Contrato = new DA_Contrato();
            return oDA_Contrato.CopiarContrato(oBE_Contrato);

        }

        public String InsDireccionarContrato(Int32 iIdContrato, Int32 iIdDocOri)
        {
            DA_Contrato oDA_Contrato = new DA_Contrato();
            return oDA_Contrato.InsDireccionarContrato(iIdContrato, iIdDocOri);

        }
        public ResultadoTransaccionTx ReasignarContrato(Int32 sIdContrato, Int32 iIdvendedor, string sIdUsuario)
        {
            DA_Contrato oDA_Contrato = new DA_Contrato();
            return oDA_Contrato.ReasignarContrato(sIdContrato, iIdvendedor, sIdUsuario);
        }
        #endregion
        #region "Listado"
        public BE_TarifaList ListarContratoDetalle(int iIdContrato)
        {
            DA_Contrato oDA_Contrato = new DA_Contrato();
            return oDA_Contrato.ListarContratoDetalle(iIdContrato);
        }
        public List<BE_Contrato> Listar(BE_Contrato oBE_Contrato)
        {
            DA_Contrato oDA_Contrato = new DA_Contrato();
            return oDA_Contrato.Listar(oBE_Contrato);
        }
        public List<BE_TablaUc> ListarTarifaMaster(BE_Tabla oBE_Tabla)
        {
            DA_Contrato oDA_Contrato = new DA_Contrato();
            return oDA_Contrato.ListarTarifaMaster(oBE_Tabla);
        }
        public List<BE_Tabla> ListarTipoContrato()
        {
            DA_ConsultasComunes oDA_ConsultasComunes = new DA_ConsultasComunes();
            return oDA_ConsultasComunes.ListarTabla(28, 0);
        }
        public List<BE_Tabla> ListarRol()
        {
            DA_ConsultasComunes oDA_ConsultasComunes = new DA_ConsultasComunes();
            return oDA_ConsultasComunes.ListarTabla(33, 0);
        }
        public List<BE_Tabla> ListarLineaNegocio()
        {
            DA_ConsultasComunes oDA_ConsultasComunes = new DA_ConsultasComunes();
            return oDA_ConsultasComunes.ListarTabla(23, 0);
        }

        public List<BE_Tabla> ListarTipoOrdenServicio()
        {
            DA_ConsultasComunes oDA_ConsultasComunes = new DA_ConsultasComunes();
            return oDA_ConsultasComunes.ListarTabla(47, 0);
        }

        public List<BE_Tabla> ListarSituacionOrdenServicioDetalle()
        {
            DA_ConsultasComunes oDA_ConsultasComunes = new DA_ConsultasComunes();
            return oDA_ConsultasComunes.ListarTabla(44, 0);
        }


        public BE_Contrato ListarContratoPorId(int iIdContrato)
        {
            DA_Contrato oDA_Contrato = new DA_Contrato();
            return oDA_Contrato.ListarContratoPorId(iIdContrato);
        }
        //public List<BE_DireccionamientoContrato> ListarContratoPorBl(BE_DocumentoOrigen oBE_DocumentoOrigen)
        //{
        //    DA_Contrato oDA_Contrato = new DA_Contrato();
        //    return oDA_Contrato.ListarContratoPorBl(oBE_DocumentoOrigen);
        //}
        public List<BE_Contrato> ListarReasignacionContrato(BE_Contrato oBE_Contrato)
        {
            DA_Contrato oDA_Contrato = new DA_Contrato();
            return oDA_Contrato.ListarReasignacionContrato(oBE_Contrato);
        }

        //public List<BE_DireccionamientoContrato> ListarReasignacionContratoHistorico(Int32 iIdContrato)
        //{
        //    DA_Contrato oDA_Contrato = new DA_Contrato();
        //    return oDA_Contrato.ListarReasignacionContratoHistorico(iIdContrato);
        //}

        public List<BE_Tabla> ListarTipoDireccionado()
        {
            DA_ConsultasComunes oDA_ListarSituacion = new DA_ConsultasComunes();
            return oDA_ListarSituacion.ListarTabla(95, 0);
        }

        public List<BE_TablaUc> ListarClienteRol(BE_Cliente oBE_Cliente)
        {
            DA_Contrato oDA_Cliente = new DA_Contrato();
            return oDA_Cliente.ListarClienteRol(oBE_Cliente);
        }

        #endregion
    }
}