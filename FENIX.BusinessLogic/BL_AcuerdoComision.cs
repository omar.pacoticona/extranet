﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FENIX.DataAccess;
using FENIX.BusinessEntity;
using FENIX.Common;


namespace FENIX.BusinessLogic
{
    public class BL_AcuerdoComision
    {


        public Int32 GrabarLiquidacion(BE_AcuerdoComision ent)
        {
            return new DA_AcuerdoComision().GrabarLiquidacion(ent);
        }

        public Int32 GrabarLiquidacionDetalle(BE_AcuerdoComision ent)
        {
            return new DA_AcuerdoComision().GrabarLiquidacionDetalle(ent);
        }

        public Int32 Ins_AceptaCondiciones(BE_AcuerdoComision ent)
        {
            return new DA_AcuerdoComision().Ins_AceptaCondiciones(ent);
        }
               

        public String GrabaDocsComisionista(BE_AcuerdoComision ent)
        {
            return new DA_AcuerdoComision().GrabaDocsComisionista(ent);
        }

        public String AutorizarLiquidacion(BE_AcuerdoComision ent)
        {
            return new DA_AcuerdoComision().AutorizarLiquidacion(ent);
        }
        

        public List<BE_AcuerdoComision> ListarDocumentoParaLiquidarComision(BE_AcuerdoComision ent) 
        {
            return new DA_AcuerdoComision().ListarDocumentoParaLiquidarComision(ent);
        }

        public List<BE_AcuerdoComision> ListarDatosLiquidacion(Int32 iIdLiquidacion)
        {
            return new DA_AcuerdoComision().ListarDatosLiquidacion(iIdLiquidacion);
        }
        
        public List<BE_AcuerdoComision> Listar_LiquidacionXComisionista(BE_AcuerdoComision ent)
        {
            return new DA_AcuerdoComision().Listar_LiquidacionXComisionista(ent);
        }

        public List<BE_AcuerdoComision> Listar_EstadosXLiquidacion()
        {
            return new DA_AcuerdoComision().Listar_EstadosXLiquidacion();
        }

        public List<BE_AcuerdoComision> ListarDocsComisionista(Int32 iIdLiquidacion)
        {
            return new DA_AcuerdoComision().ListarDocsComisionista(iIdLiquidacion);
        }
    }
}
