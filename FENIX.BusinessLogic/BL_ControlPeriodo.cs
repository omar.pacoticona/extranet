﻿using System;
using System.Collections.Generic;
using System.Text;
using FENIX.BusinessEntity;
using FENIX.DataAccess;

namespace FENIX.BusinessLogic
{
    public class BL_ControlPeriodo
    {
        #region "Transacciones"

        public String Insertar(BE_ControlPeriodo oBE_ControlPeriodo)
        {
            DA_ControlPeriodo oDA_ControlPeriodo = new DA_ControlPeriodo();
            return oDA_ControlPeriodo.Insertar(oBE_ControlPeriodo);
        }

        public String Actualizar(BE_ControlPeriodo oBE_ControlPeriodo)
        {
            DA_ControlPeriodo oDA_ControlPeriodo = new DA_ControlPeriodo();
            return oDA_ControlPeriodo.Actualizar(oBE_ControlPeriodo);
        }

        public String CerrarPeriodo(BE_ControlPeriodo oBE_ControlPeriodo)
        {
            DA_ControlPeriodo oDA_ControlPeriodo = new DA_ControlPeriodo();
            return oDA_ControlPeriodo.CerrarPeriodo(oBE_ControlPeriodo);
        }

        #endregion

        #region "Listado"

        public List<BE_ControlPeriodo> Listar(BE_ControlPeriodo oBE_ControlPeriodo)
        {
            DA_ControlPeriodo oDA_ControlPeriodo = new DA_ControlPeriodo();
            return oDA_ControlPeriodo.Listar(oBE_ControlPeriodo);
        }

        public List<BE_ControlPeriodo> ListarLog(BE_ControlPeriodo oBE_ControlPeriodo)
        {
            DA_ControlPeriodo oDA_ControlPeriodo = new DA_ControlPeriodo();
            return oDA_ControlPeriodo.ListarLog(oBE_ControlPeriodo);
        }

        #endregion
    }
}
