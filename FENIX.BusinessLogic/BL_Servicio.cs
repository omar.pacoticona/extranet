﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FENIX.BusinessEntity;
using FENIX.DataAccess;
using FENIX.Common;

namespace FENIX.BusinessLogic
{
    public class BL_Servicio
    {

        public List<BE_Detalle> listarDetalleServicio(BE_eOrdenServicio oBE_eOrdenServicio)
        {
            DA_Servicio oDA_Servicio = new DA_Servicio();
            return oDA_Servicio.ListarDocumentoDetalle(oBE_eOrdenServicio);
        }
        public List<BE_ServicioAdicional> ListarServiciosAdicionales(Int32 iIdTipoOperacion, Int32 iIdOficina, Int32 iIdSuperNegocio, Int32 iIdLineaNegocio)
        {
            DA_Servicio oDA_Servicio = new DA_Servicio();
            return oDA_Servicio.ListarServiciosAdicionales(iIdTipoOperacion,iIdOficina,iIdSuperNegocio,iIdLineaNegocio);
        }

        public List<BE_Operacion> ListarItems()
        {
            DA_Servicio oDA_Servicio = new DA_Servicio();
            return oDA_Servicio.ListarItems();
        }

        public List<BE_Servicio> ListarServicios()
        {
            DA_Servicio oDA_Servicio = new DA_Servicio();
            return oDA_Servicio.ListarServicios();
        }
    }
}
